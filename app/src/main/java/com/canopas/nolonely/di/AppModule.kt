package com.canopas.nolonely.di

import android.app.AlarmManager
import android.app.NotificationManager
import android.content.Context
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.nolonely.BuildConfig
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.mixpanel.android.mpmetrics.MixpanelAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideAlarmManager(@ApplicationContext context: Context): AlarmManager {
        return context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    }

    @Provides
    @Singleton
    fun provideNotificationManager(@ApplicationContext context: Context): NotificationManager {
        return context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    @Provides
    @Singleton
    fun provideAppAnalytic(@ApplicationContext context: Context): AppAnalytics {
        val mixpanel = MixpanelAPI.getInstance(context, "ddc5f59443973de70a2834aec3828c6c", true)
        return AppAnalytics(Firebase.analytics, mixpanel)
    }

    @Provides
    @Singleton
    @Named("app_version_code")
    fun provideAppVersionCode(): Long {
        return BuildConfig.VERSION_CODE.toLong()
    }
}
