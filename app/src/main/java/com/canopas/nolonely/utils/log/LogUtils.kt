package com.canopas.nolonely.utils.log

import android.content.Context
import java.io.File

object LogUtils {

    @JvmStatic
    fun getLogFile(context: Context): File {
        return File(context.filesDir, "log.txt")
    }
}
