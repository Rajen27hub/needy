package com.canopas.nolonely.utils

import android.content.Context
import android.content.SharedPreferences
import com.canopas.nolonely.BuildConfig.VERSION_CODE
import timber.log.Timber

enum class AppVersionUpgradeNotifier {
    INSTANCE;

    private val TAG = "AppVersionUpdateManager"

    private val PREFERENCES_APP_VERSION = "pref_app_version_upgrade"
    private val KEY_LAST_VERSION = "last_version"

    lateinit var sharedPreferences: SharedPreferences
    private lateinit var versionUpdateListener: VersionUpdateListener
    private var isInitialized = false

    @Synchronized
    fun init(context: Context?, versionUpdateListener: VersionUpdateListener?) {
        if (context == null || versionUpdateListener == null) {
            throw IllegalArgumentException("$TAG : Context or VersionUpdateListener is null")
        }
        if (!INSTANCE.isInitialized) {
            INSTANCE.initInternal(context, versionUpdateListener)
        } else {
            Timber.w("Init called twice, ignoring...")
        }
    }

    private fun initInternal(context: Context, versionUpdateListener: VersionUpdateListener) {
        sharedPreferences = context.getSharedPreferences(
            PREFERENCES_APP_VERSION, Context.MODE_PRIVATE
        )
        this.versionUpdateListener = versionUpdateListener
        this.isInitialized = true
        checkVersionUpdate()
    }

    private fun checkVersionUpdate() {
        val lastVersion: Long = getLastVersion()
        val currentVersion: Long = getCurrentVersion()
        if (lastVersion < currentVersion) {
            if (versionUpdateListener.onVersionUpdate(currentVersion, lastVersion)) {
                upgradeLastVersionToCurrent()
            }
        }
    }

    private fun getLastVersion(): Long {
        return sharedPreferences.getLong(KEY_LAST_VERSION, 0)
    }

    private fun getCurrentVersion(): Long {
        return VERSION_CODE.toLong()
    }

    private fun upgradeLastVersionToCurrent() {
        sharedPreferences.edit().putLong(
            KEY_LAST_VERSION,
            getCurrentVersion()
        ).apply()
    }

    interface VersionUpdateListener {
        fun onVersionUpdate(newVersion: Long, oldVersion: Long): Boolean
    }
}
