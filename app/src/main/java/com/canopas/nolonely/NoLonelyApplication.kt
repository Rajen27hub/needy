package com.canopas.nolonely

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.res.Configuration.UI_MODE_NIGHT_MASK
import android.content.res.Configuration.UI_MODE_NIGHT_NO
import android.content.res.Configuration.UI_MODE_NIGHT_UNDEFINED
import android.content.res.Configuration.UI_MODE_NIGHT_YES
import android.os.Build
import androidx.hilt.work.HiltWorkerFactory
import androidx.lifecycle.DefaultLifecycleObserver
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.work.Configuration
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.auth.AuthStateChangeListener
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.repository.ABTestingRepository
import com.canopas.data.rest.NoLonelyService
import com.canopas.nolonely.fcm.FCMRegisterWorker.Companion.startService
import com.canopas.nolonely.fcm.NOTIFICATION_CHANNEL_NOLONELY
import com.canopas.nolonely.utils.AppVersionUpgradeNotifier
import com.canopas.nolonely.utils.log.FileLoggingTree
import com.canopas.nolonely.utils.log.LogUtils
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.TimeZone
import javax.inject.Inject

@HiltAndroidApp
class NoLonelyApplication :
    Application(),
    AuthStateChangeListener,
    DefaultLifecycleObserver,
    AppVersionUpgradeNotifier.VersionUpdateListener,
    Configuration.Provider {
    private val scope = CoroutineScope(Dispatchers.Main)

    @Inject
    lateinit var notificationManager: NotificationManager

    @Inject
    lateinit var authManager: AuthManager

    @Inject
    lateinit var preferencesDataStore: NoLonelyPreferences

    @Inject
    lateinit var appAnalytics: AppAnalytics

    @Inject
    lateinit var workerFactory: HiltWorkerFactory

    @Inject
    lateinit var service: NoLonelyService

    override fun onCreate() {
        super<Application>.onCreate()

        if (BuildConfig.DEBUG || BuildConfig.VERSION_NAME == "1.0.0") {
            Timber.plant(Timber.DebugTree())
            appAnalytics.setAnalyticsCollectionEnabled(false)
        } else {
            FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
            appAnalytics.setAnalyticsCollectionEnabled(true)
        }

        authManager.addListener(this)
        ABTestingRepository.INSTANCE.init(this)
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
        AppVersionUpgradeNotifier.INSTANCE.init(this, this)
        setNotificationChannel()

        Timber.plant(FileLoggingTree(LogUtils.getLogFile(this)))

        setFirebaseAnalyticsUser()

        setInitialDarkModePref()

        checkCurrentTimeZone()
    }

    private fun checkCurrentTimeZone() = scope.launch {
        val currentTimeZone = TimeZone.getDefault().id
        authManager.currentUser?.let { userAccount ->
            if (!userAccount.isAnonymous) {
                if (userAccount.timezone.isNullOrEmpty() || userAccount.timezone != currentTimeZone) {
                    userAccount.timezone = currentTimeZone
                    withContext(Dispatchers.IO) {
                        service.updateUserById(userAccount.id, userAccount).onSuccess {
                            authManager.currentUser = it
                            Timber.d("User timezone updated successfully:$it")
                        }.onFailure {
                            Timber.e("Error updating user timezone:$it")
                        }
                    }
                }
            }
        }
    }

    private fun setInitialDarkModePref() = scope.launch {
        if (!preferencesDataStore.getIsDarkModeSet()) {
            when (resources?.configuration?.uiMode?.and(UI_MODE_NIGHT_MASK)) {
                UI_MODE_NIGHT_YES, UI_MODE_NIGHT_UNDEFINED -> {
                    preferencesDataStore.setIsDarkModeEnabled(true)
                }
                UI_MODE_NIGHT_NO -> {
                    preferencesDataStore.setIsDarkModeEnabled(false)
                }
            }
        }
    }

    private fun setFirebaseAnalyticsUser() {
        val userType = when (authManager.authState) {
            is AuthState.ANONYMOUS -> "anonymous"
            AuthState.UNAUTHORIZED -> "unauthorized"
            is AuthState.VERIFIED -> "verified"
        }
        appAnalytics.setUserProperty(userType, authManager.currentUser?.id?.toString())
    }

    private fun setNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val reminderChannel = NotificationChannel(
                NOTIFICATION_CHANNEL_NOLONELY,
                getString(R.string.title_notification_channel_reminder),
                NotificationManager.IMPORTANCE_HIGH
            )

            reminderChannel.description =
                getString(R.string.description_notification_channel_reminder)
            reminderChannel.enableLights(true)
            notificationManager.createNotificationChannel(reminderChannel)
        }
    }

    override fun onStart(owner: LifecycleOwner) {
        super.onStart(owner)
        scope.launch {
            withContext(Dispatchers.IO) {
                if (authManager.authState.isAuthorised) {
                    authManager.currentUser?.let { userAccount ->
                        service.getUserDetailById(userAccount.id).onSuccess {
                            authManager.currentUser = it
                        }.onFailure {
                            Timber.e("Error fetching user:$it")
                        }
                    }
                }
            }
            if (authManager.authState.isAuthorised && !preferencesDataStore.isFCMRegistered()) {
                startService(applicationContext)
            }
        }
    }

    override fun onAuthStateChanged(authState: AuthState) {
        scope.launch {
            if (authManager.authState.isAuthorised && !preferencesDataStore.isFCMRegistered()) {
                startService(applicationContext)
            }
            setFirebaseAnalyticsUser()
        }
    }

    override fun onVersionUpdate(newVersion: Long, oldVersion: Long): Boolean {
        scope.launch {
            preferencesDataStore.setIsFCMRegistered(false)

            if (authManager.authState.isAuthorised) {
                startService(applicationContext)
            }
        }
        return true
    }

    override fun getWorkManagerConfiguration() =
        Configuration.Builder()
            .setWorkerFactory(workerFactory)
            .build()
}
