package com.canopas.nolonely.fcm

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.CoroutineWorker
import androidx.work.Data
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkerParameters
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.RegisterDeviceRequest
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.rest.AuthServices
import com.canopas.base.data.utils.Device
import com.google.firebase.messaging.FirebaseMessaging
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.async
import timber.log.Timber
import java.util.concurrent.CountDownLatch
import javax.inject.Inject

const val FCM_SERVICE = "FCMRegisterService"

@HiltWorker
class FCMRegisterWorker @AssistedInject constructor(@Assisted context: Context, @Assisted workerParameters: WorkerParameters) : CoroutineWorker(context, workerParameters) {

    @Inject
    lateinit var authManager: AuthManager

    @Inject
    lateinit var preferencesDataStore: NoLonelyPreferences

    @Inject
    lateinit var authServices: AuthServices

    @Inject
    lateinit var device: Device

    private val job = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.Main + job)

    override suspend fun doWork(): Result {
        if (authManager.authState.isAuthorised && !preferencesDataStore.isFCMRegistered()) {
            val deviceToken = getFCMToken()
            try {
                val job = scope.async {
                    val registerDeviceRequest = RegisterDeviceRequest(
                        deviceToken,
                        device.getAppVersionCode(),
                        device.deviceName(),
                        device.getDeviceOsVersion()
                    )
                    authServices.registerDevice(registerDeviceRequest).onSuccess {
                        preferencesDataStore.setIsFCMRegistered(true)
                    }.onFailure {
                        Timber.e(it)
                    }
                }
                job.await()
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
        return Result.success(inputData)
    }

    private fun getFCMToken(): String {
        val countDownLatch = CountDownLatch(1)
        var token = ""
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                task.result?.let {
                    token = it
                }
            } else {
                Timber.e(task.exception, "FCM token fetch failed")
            }
            countDownLatch.countDown()
        }
        countDownLatch.await()
        return token
    }

    companion object {
        fun startService(context: Context) {
            val data = Data.Builder().putString(FCM_SERVICE, "FCMRegisterService").build()
            val request = OneTimeWorkRequestBuilder<FCMRegisterWorker>()
                .setInputData(data)
                .build()
            val workManager = WorkManager.getInstance(context)
            workManager.enqueue(request)
        }
    }
}
