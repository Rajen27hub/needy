package com.canopas.nolonely.fcm

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.nolonely.R
import com.canopas.nolonely.broadcastreceiver.NotificationBroadcastReceiver
import com.canopas.nolonely.broadcastreceiver.NotificationBroadcastReceiver.Companion.EXTRA_COMMUNITY_FLAG
import com.canopas.nolonely.broadcastreceiver.NotificationBroadcastReceiver.Companion.EXTRA_SUBSCRIPTION_ID
import com.canopas.nolonely.fcm.FCMRegisterWorker.Companion.startService
import com.canopas.ui.home.HomeActivity
import com.canopas.ui.home.explore.notificationSummary.NotificationSummaryActivity
import com.canopas.ui.main.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.Calendar
import javax.inject.Inject

const val NOTIFICATION_CHANNEL_NOLONELY = "notification_channel_nolonely"
const val NOTIFICATION_TITLE = "title"
const val NOTIFICATION_BODY = "body"
const val NOTIFICATION_TYPE = "notification_type"
const val TYPE_NOTIFICATION_ACTIVITY_REMINDER = 1
const val TYPE_NOTIFICATION_SUBSCRIPTION_SUMMARY = 2
const val TYPE_NOTIFICATION_COMMUNITY = 3
const val TYPE_NOTIFICATION_INACTIVE_REMINDER = 4
const val TYPE_COMMUNITY_REQUEST_ACCEPTED = 5
private const val REMINDER_NOTIFICATION_ID = 11
const val WEEKLY_SUMMARY_START_DATE = "start_date"
const val WEEKLY_SUMMARY_END_DATE = "end_date"
const val ACCEPT_COMMUNITY_ID = "community_id"

@AndroidEntryPoint
class NoLonelyFcmService : FirebaseMessagingService() {
    private val scope = CoroutineScope(Dispatchers.IO)

    @Inject
    lateinit var authManager: AuthManager

    @Inject
    lateinit var preferencesDataStore: NoLonelyPreferences

    @Inject
    lateinit var notificationManager: NotificationManager

    @Inject
    lateinit var appAnalytics: AppAnalytics

    override fun onNewToken(token: String) {
        super.onNewToken(token)
        try {
            Timber.d("Refreshed token: $token")
            scope.launch {
                preferencesDataStore.setIsFCMRegistered(false)

                if (authManager.authState.isAuthorised) {
                    startService(applicationContext)
                }
            }
        } catch (e: Exception) {
            Timber.e(e, "Token refresh error")
        }
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        if (remoteMessage.notification == null) {
            Timber.d("Received notification data:%s", remoteMessage.data)
            val notificationType = remoteMessage.data[NOTIFICATION_TYPE]
            if (notificationType != null) {
                when (notificationType.toInt()) {
                    TYPE_NOTIFICATION_ACTIVITY_REMINDER -> {
                        appAnalytics.logEvent("received_daily_activity_notification")
                        val title = remoteMessage.data[NOTIFICATION_TITLE]
                        val body = remoteMessage.data[NOTIFICATION_BODY]
                        val subscriptionId = remoteMessage.data[EXTRA_SUBSCRIPTION_ID]
                        sendDailyActivityReminder(this, title!!, body!!, subscriptionId!!)
                    }
                    TYPE_NOTIFICATION_SUBSCRIPTION_SUMMARY -> {
                        appAnalytics.logEvent("received_weekly_summary_notification")
                        val title = remoteMessage.data[NOTIFICATION_TITLE]
                        val body = remoteMessage.data[NOTIFICATION_BODY]
                        val startDate = remoteMessage.data[WEEKLY_SUMMARY_START_DATE]
                        val endDate = remoteMessage.data[WEEKLY_SUMMARY_END_DATE]
                        sendWeeklySummary(this, title!!, body!!, startDate, endDate)
                    }
                    TYPE_NOTIFICATION_COMMUNITY -> {
                        appAnalytics.logEvent("received_community_join_notification")
                        val title = remoteMessage.data[NOTIFICATION_TITLE]
                        val body = remoteMessage.data[NOTIFICATION_BODY]
                        if (title != null && body != null) {
                            sendCommunityJoinNotification(this, title, body)
                        }
                    }
                    TYPE_NOTIFICATION_INACTIVE_REMINDER -> {
                        appAnalytics.logEvent("received_inactive_reminder_notification")
                        val title = remoteMessage.data[NOTIFICATION_TITLE]
                        val body = remoteMessage.data[NOTIFICATION_BODY]
                        if (title != null && body != null) {
                            sendNotification(this, title, body, TYPE_NOTIFICATION_INACTIVE_REMINDER)
                        }
                    }
                    TYPE_COMMUNITY_REQUEST_ACCEPTED -> {
                        appAnalytics.logEvent("received_community_accepted_notification")
                        val title = remoteMessage.data[NOTIFICATION_TITLE]
                        val body = remoteMessage.data[NOTIFICATION_BODY]
                        val communityId = remoteMessage.data[ACCEPT_COMMUNITY_ID]
                        if (title != null && body != null && communityId != null) {
                            acceptCommunityRequestNotification(this, title, body, communityId)
                        }
                    }
                }
            }
        } else {
            Timber.d("Received notification object:%s", remoteMessage.notification)
            val notification = remoteMessage.notification
            notification?.let {
                val title = notification.title
                val body = notification.body
                if (title != null && body != null) {
                    sendNotification(this, title, body, 0)
                }
            }
        }
    }

    private fun sendWeeklySummary(
        context: Context,
        title: String,
        body: String,
        startDate: String?,
        endDate: String?,
    ) {
        val activityActionIntent = Intent(context, NotificationSummaryActivity::class.java).apply {
            putExtra(WEEKLY_SUMMARY_START_DATE, startDate)
            putExtra(WEEKLY_SUMMARY_END_DATE, endDate)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntentFlag = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_IMMUTABLE else PendingIntent.FLAG_UPDATE_CURRENT
        val activityActionPendingIntent: PendingIntent = PendingIntent.getActivity(
            context,
            TYPE_NOTIFICATION_SUBSCRIPTION_SUMMARY,
            activityActionIntent,
            pendingIntentFlag
        )

        val nBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_NOLONELY)
                .setSmallIcon(R.drawable.ic_nolonely_logo)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.black))
                .setContentIntent(activityActionPendingIntent)
        notificationManager.notify(REMINDER_NOTIFICATION_ID, nBuilder.build())
    }

    private fun sendNotification(
        context: Context,
        title: String,
        body: String,
        requestCode: Int
    ) {
        val activityActionIntent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        val pendingIntentFlag = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_IMMUTABLE else PendingIntent.FLAG_UPDATE_CURRENT
        val activityActionPendingIntent: PendingIntent = PendingIntent.getActivity(
            context,
            requestCode,
            activityActionIntent,
            pendingIntentFlag
        )

        val nBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_NOLONELY)
                .setSmallIcon(R.drawable.ic_nolonely_logo)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.black))
                .setContentIntent(activityActionPendingIntent)
                .setTimeoutAfter(getNotificationTimeOutMillis())

        notificationManager.notify(REMINDER_NOTIFICATION_ID, nBuilder.build())
    }

    private fun sendCommunityJoinNotification(context: Context, title: String, body: String) {
        val activityActionIntent = Intent(context, HomeActivity::class.java).apply {
            this.putExtra(EXTRA_COMMUNITY_FLAG, true)
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        val pendingIntentFlag = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_IMMUTABLE else PendingIntent.FLAG_UPDATE_CURRENT
        val activityActionPendingIntent: PendingIntent = PendingIntent.getActivity(
            context,
            TYPE_NOTIFICATION_COMMUNITY,
            activityActionIntent,
            pendingIntentFlag
        )
        val nBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_NOLONELY)
                .setSmallIcon(R.drawable.ic_nolonely_logo)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.black))
                .setContentIntent(activityActionPendingIntent)

        notificationManager.notify(REMINDER_NOTIFICATION_ID, nBuilder.build())
    }
    private fun acceptCommunityRequestNotification(
        context: Context,
        title: String,
        body: String,
        communityId: String
    ) {
        val activityActionIntent = Intent(context, HomeActivity::class.java).apply {
            this.putExtra(ACCEPT_COMMUNITY_ID, communityId)
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        val pendingIntentFlag = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_IMMUTABLE else PendingIntent.FLAG_UPDATE_CURRENT
        val activityActionPendingIntent: PendingIntent = PendingIntent.getActivity(
            context,
            TYPE_COMMUNITY_REQUEST_ACCEPTED,
            activityActionIntent,
            pendingIntentFlag
        )
        val nBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_NOLONELY)
                .setSmallIcon(R.drawable.ic_nolonely_logo)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.black))
                .setContentIntent(activityActionPendingIntent)

        notificationManager.notify(REMINDER_NOTIFICATION_ID, nBuilder.build())
    }

    private fun sendDailyActivityReminder(context: Context, title: String, body: String, subsId: String) {
        val activityActionIntent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        val pendingIntentFlag = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) PendingIntent.FLAG_IMMUTABLE else PendingIntent.FLAG_UPDATE_CURRENT
        val activityActionPendingIntent: PendingIntent = PendingIntent.getActivity(
            context,
            TYPE_NOTIFICATION_ACTIVITY_REMINDER,
            activityActionIntent,
            pendingIntentFlag
        )

        val broadcastIntent = Intent(context, NotificationBroadcastReceiver::class.java).apply {
            putExtra(EXTRA_SUBSCRIPTION_ID, subsId)
            flags = Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP
        }
        val broadcastPendingIntent: PendingIntent =
            PendingIntent.getBroadcast(
                context,
                subsId.toInt().plus(10000),
                broadcastIntent,
                pendingIntentFlag
            )

        val nBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(context, NOTIFICATION_CHANNEL_NOLONELY)
                .setSmallIcon(R.drawable.ic_nolonely_logo)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(context, R.color.black))
                .setContentIntent(activityActionPendingIntent)
                .addAction(
                    R.drawable.ic_complete,
                    getString(R.string.notification_complete_button_text),
                    broadcastPendingIntent
                )
                .setTimeoutAfter(getNotificationTimeOutMillis())

        notificationManager.notify(subsId.toInt().plus(10000), nBuilder.build())
    }
}

fun getNotificationTimeOutMillis(): Long {
    val cal = Calendar.getInstance()
    cal[Calendar.HOUR_OF_DAY] = 0
    cal.clear(Calendar.MINUTE)
    cal.add(Calendar.DAY_OF_YEAR, 1)
    return (cal.timeInMillis - System.currentTimeMillis())
}
