package com.canopas.nolonely.broadcastreceiver

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.data.model.CompletionBody
import com.canopas.data.rest.NoLonelyService
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.IOException
import java.util.TimeZone
import javax.inject.Inject

@AndroidEntryPoint
class NotificationBroadcastReceiver : BroadcastReceiver() {

    @Inject
    lateinit var services: NoLonelyService

    @Inject
    lateinit var notificationManager: NotificationManager

    @Inject
    lateinit var appAnalytics: AppAnalytics

    private val scope = CoroutineScope(Dispatchers.Main)

    override fun onReceive(context: Context?, intent: Intent?) {
        val subscriptionId = intent?.getStringExtra(EXTRA_SUBSCRIPTION_ID) ?: ""
        val subscriptionBundle = Bundle()
        subscriptionBundle.putString("subscription_id", subscriptionId)
        appAnalytics.logEvent("tap_daily_activity_reminder_notification", subscriptionBundle)

        if (subscriptionId.isNotEmpty()) {
            val completionData = CompletionBody(
                subscription_id = subscriptionId.toInt(),
                completion_time = (System.currentTimeMillis() / 1000L),
                timeZone = TimeZone.getDefault().id
            )
            scope.launch {
                withContext(Dispatchers.IO) {
                    services.saveDailyCompletionActivity(completionData).onSuccess {
                        Timber.d("Activity with id-($subscriptionId), completed successfully")
                    }.onFailure { e ->
                        if (e !is IOException) {
                            Timber.e("Notification complete button exception for id-($subscriptionId}):$e")
                            val bundle = Bundle()
                            bundle.putString("exception", e.message)
                            appAnalytics.logEvent("error_reminder_notification_$subscriptionId ", bundle)
                        }
                    }
                }
                notificationManager.cancel(subscriptionId.toInt().plus(10000))
            }
        } else {
            Timber.e("Notification complete button error: empty subscription id!")
            appAnalytics.logEvent("error_reminder_notification_empty_sub_id")
        }
    }

    companion object {
        const val EXTRA_SUBSCRIPTION_ID = "subscription_id"
        const val EXTRA_COMMUNITY_FLAG = "is_community_request"
    }
}
