package com.canopas.feature_points_data.di

import com.canopas.feature_points_data.rest.PointService
import com.google.firebase.database.FirebaseDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RestModule {

    @Singleton
    @Provides
    fun provideService(@Named("ApiRetrofit") retrofit: Retrofit): PointService =
        retrofit.create(PointService::class.java)

    @Singleton
    @Provides
    fun provideFirebaseDatabase(): FirebaseDatabase =
        FirebaseDatabase.getInstance()
}
