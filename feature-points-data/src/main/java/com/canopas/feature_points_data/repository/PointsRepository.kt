package com.canopas.feature_points_data.repository

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class PointsRepository @Inject constructor(
    private val database: FirebaseDatabase
) {
    suspend fun getPoints(uniqueId: String): Flow<Int> {
        return callbackFlow {
            val ref = database.getReference("points").child(uniqueId).child("points")
            val listener = object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    dataSnapshot.value?.let {
                        trySendBlocking((it as Long).toInt())
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {
                    Timber.e(
                        databaseError.toException(),
                        "PointsRepository: Got error while listening to points data"
                    )
                }
            }

            ref.addValueEventListener(listener)

            awaitClose {
                ref.removeEventListener(listener)
            }
        }
    }
}
