package com.canopas.feature_points_data.rest

import com.canopas.feature_points_data.model.UserPoints
import retrofit2.http.GET
import retrofit2.http.Query

interface PointService {

    @GET("user/v1/leader-board")
    suspend fun getPoints(@Query("duration") duration: String): Result<List<UserPoints>>
}
