package com.canopas.feature_points_data

import com.canopas.base.data.model.Notifications
import com.canopas.base.data.model.Session
import com.canopas.base.data.model.UserAccount
import com.canopas.feature_points_data.model.UserPoints

class TestUtils {
    companion object {

        val session = Session(
            1, 1, deviceType = 1, "1", "MD1",
            "", 20000, "", "1", oldRefreshToken = "",
            limitedAccess = false, lastAccessedOn = 1641987105
        )
        val notification = Notifications(
            pushNotification = true, mail = false, inactivityMail = true, inactivityNotification = false
        )

        val user = UserAccount(
            1, 1, 1, 1, "abc", "",
            "userName", "12345", "dummy@gmail.com", "",
            isActive = true, isAnonymous = true,
            createdAt = "123", updatedAt = "456", gender = 1, userType = 0, 1, true,
            "", session, notification
        )

        val userPoints = UserPoints(
            userId = 1, rank = 1, total_points = 140, user
        )
    }
}
