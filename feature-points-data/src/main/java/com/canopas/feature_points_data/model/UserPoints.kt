package com.canopas.feature_points_data.model

import com.canopas.base.data.model.UserAccount
import com.google.gson.annotations.SerializedName

data class UserPoints(

    @SerializedName("user_id")
    val userId: Int,

    @SerializedName("rank")
    val rank: Int,

    @SerializedName("total_points")
    val total_points: Int,

    @SerializedName("user")
    var user: UserAccount
)
