package com.canopas.feature_points_data.model

enum class DurationType {
    DURATION_WEEK, DURATION_MONTH, DURATION_ALL;

    val title: String
        get() {
            return when (this) {
                DURATION_WEEK -> "week"
                DURATION_MONTH -> "month"
                DURATION_ALL -> "all"
            }
        }
}
