package com.canopas.feature_community_ui.communityinfo

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.UserAccount
import com.canopas.base.ui.Event
import com.canopas.feature_community_data.di.CommunityCache
import com.canopas.feature_community_data.model.AddCommunity
import com.canopas.feature_community_data.model.Community
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import timber.log.Timber
import java.io.File
import javax.inject.Inject

@HiltViewModel
class CommunityInfoViewModel @Inject constructor(
    private val service: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val navManager: CommunityNavManager,
    private val authManager: AuthManager,
    private val communityCache: CommunityCache,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    private var community: Community? = null

    val state = MutableStateFlow<InfoState>(InfoState.LOADING)
    val openLeaveCommunityDialog = MutableStateFlow(Event(false))
    private var updateCommunityProfileImage = MutableStateFlow<File?>(null)
    val openCommunityImageChooserEvent = MutableStateFlow(Event(false))

    fun fetchCommunityInfo(communityId: String, userId: String) {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("view_community_info")
                val communityData = communityCache.getCommunity(communityId)
                if (communityData != null) {
                    val userData = communityData.users.first {
                        it.user_id == userId.toInt()
                    }
                    community = communityData
                    state.tryEmit(InfoState.SUCCESS(communityData, userData))
                } else {
                    service.getCommunity(communityId.toInt()).onSuccess {
                        val userData = it.users.first { user ->
                            user.user_id == userId.toInt()
                        }
                        community = it
                        state.tryEmit(InfoState.SUCCESS(it, userData))
                    }.onFailure { e ->
                        Timber.e(e.localizedMessage)
                        state.tryEmit(InfoState.FAILURE(e.toUserError(resources)))
                    }
                }
            }
        }
    }

    fun managePopBack() {
        navManager.popBackStack()
    }

    fun navigateToChangeNameScreen(communityId: String, communityName: String) {
        appAnalytics.logEvent("tap_community_info_change_name")
        navManager.navigateToAddCommunityWithId(communityId, communityName)
    }

    fun navigateToAddMembers(communityId: Int) {
        appAnalytics.logEvent("tap_community_info_add_member")
        navManager.navigateToAddCommunityMembers(communityId, false)
    }

    fun navigateToPendingInvitations(communityId: Int) {
        appAnalytics.logEvent("tap_community_info_Pending_invitations")
        navManager.navigateToPendingInvitation(communityId)
    }

    fun navigateToShareActivities(communityId: String) {
        appAnalytics.logEvent("tap_community_info_share_activities")
        navManager.navigateToShareSubscriptions(
            communityId.toInt(),
            authManager.currentUser?.id.toString()
        )
    }

    fun openLeaveCommunityDialog() {
        appAnalytics.logEvent("tap_community_info_leave_community")
        openLeaveCommunityDialog.value = Event(true)
    }

    fun closeLeaveCommunityDialog() {
        appAnalytics.logEvent("tap_community_info_leave_community_no")
        openLeaveCommunityDialog.value = Event(false)
    }

    fun openCommunityImageChanger() {
        appAnalytics.logEvent("tap_community_info_change_profile")
        openCommunityImageChooserEvent.value = Event(true)
    }

    fun uploadCommunityImage(image: File, userId: String) {
        updateCommunityProfileImage.tryEmit(image)
        uploadCommunityProfile(userId)
    }

    fun removeCommunityProfile(userId: String) {
        community?.image_url = ""
        updateCommunityProfileImage.tryEmit(null)
        uploadCommunityProfile(userId)
    }

    fun leaveCommunity(communityId: Int) = viewModelScope.launch {
        withContext(appDispatcher.IO) {
            appAnalytics.logEvent("tap_community_info_leave_community_yes")
            service.leaveCommunity(communityId).onSuccess {
                state.tryEmit(InfoState.LEAVE)
            }.onFailure { e ->
                Timber.e(e)
                state.tryEmit(InfoState.FAILURE(e.toUserError(resources)))
            }
        }
        if (state.value == InfoState.LEAVE) {
            popBackToCommunityListScreen()
        }
    }

    private fun updateCommunity(communityId: Int, userId: String) {
        val community = community ?: return
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                val communityDetails = AddCommunity(
                    name = community.name,
                    description = community.description,
                    image_url = community.image_url
                )
                service.updateCommunity(communityId, communityDetails).onSuccess { communityData ->
                    val userData = communityData.users.first {
                        it.user_id == userId.toInt()
                    }
                    state.tryEmit(InfoState.SUCCESS(communityData, userData))
                }.onFailure {
                    state.tryEmit(InfoState.FAILURE(it.toUserError(resources)))
                    Timber.e(it.localizedMessage)
                }
            }
        }
    }

    private fun uploadCommunityProfile(userId: String) {
        val hasCommunityProfileUpload = updateCommunityProfileImage.value != null
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                state.tryEmit(InfoState.LOADING)

                updateCommunityProfileImage.value?.let { communityImage ->
                    val multipart: MultipartBody.Part = MultipartBody.Part.createFormData(
                        "file", communityImage.name,
                        communityImage.asRequestBody("*/*".toMediaTypeOrNull())
                    )
                    service.uploadCommunityProfile(multipart).onSuccess { communityImageUrl ->
                        community?.image_url = communityImageUrl.image_url
                        val userData = community?.users?.first {
                            it.user_id == userId.toInt()
                        }

                        updateCommunity(community?.id!!, userData?.user_id.toString())
                    }.onFailure { e ->
                        Timber.e(e)
                        state.value = InfoState.FAILURE(e.toUserError(resources))
                        return@withContext
                    }
                }

                if (!hasCommunityProfileUpload)
                    updateCommunity(community?.id!!, userId)
            }
        }
    }

    private fun popBackToCommunityListScreen() {
        navManager.navigateToCommunityListScreen()
    }
}

sealed class InfoState {
    object START : InfoState()
    object LOADING : InfoState()
    object LEAVE : InfoState()
    data class SUCCESS(
        val community: Community,
        val userAccount: UserAccount
    ) : InfoState()

    data class FAILURE(val message: String) : InfoState()
}
