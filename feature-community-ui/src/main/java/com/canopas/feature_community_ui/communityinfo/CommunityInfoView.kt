package com.canopas.feature_community_ui.communityinfo

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.model.UserAccount
import com.canopas.base.data.utils.Utils.Companion.encodeToBase64
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.DividedLineInfoView
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.feature_community_data.model.Community
import com.canopas.feature_community_ui.R
import id.zelory.compressor.Compressor
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

const val ADMIN_ROLE = 1

@Composable
fun CommunityInfoView(communityId: String, userId: String) {

    val viewModel = hiltViewModel<CommunityInfoViewModel>()
    val infoState by viewModel.state.collectAsState()
    val context = LocalContext.current
    val scope = rememberCoroutineScope()

    val openCommunityImageChooserEvent by viewModel.openCommunityImageChooserEvent.collectAsState()
    val openCommunityImageChooser = openCommunityImageChooserEvent.getContentIfNotHandled() ?: false

    val launcher = rememberLauncherForActivityResult(
        contract =
        ActivityResultContracts.GetContent()
    ) { uri: Uri? ->
        uri?.let {
            val fileName = "IMG_${System.currentTimeMillis()}.jpg"
            val imageFile = File(context.filesDir, fileName)
            scope.launch {
                imageFile.copyFrom(context, it)
                val compressedImageFile = Compressor.compress(context, imageFile)
                viewModel.uploadCommunityImage(compressedImageFile, userId)
            }
        }
    }

    val cameraLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicturePreview()
    ) { bitmap ->
        bitmap?.let {
            val fileName = "IMG_${System.currentTimeMillis()}.jpg"
            val imageFile = File(context.filesDir, fileName)
            try {
                val out = FileOutputStream(imageFile)
                it.compress(Bitmap.CompressFormat.JPEG, 100, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            viewModel.uploadCommunityImage(imageFile, userId)
        }
    }

    if (openCommunityImageChooser) {
        androidx.appcompat.app.AlertDialog
            .Builder(context)
            .setItems(R.array.community_profile_image_sources) { _, which ->
                when (which) {
                    0 -> {
                        cameraLauncher.launch()
                    }
                    1 -> {
                        launcher.launch("image/*")
                    }
                    2 -> {
                        viewModel.removeCommunityProfile(userId)
                    }
                }
            }
            .show()
    }

    LaunchedEffect(key1 = communityId, key2 = userId, block = {
        viewModel.fetchCommunityInfo(communityId, userId)
    })

    infoState.let { state ->
        when (state) {
            InfoState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(colors.background)
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }

            is InfoState.SUCCESS -> {
                val community = state.community
                val user = state.userAccount
                InfoSuccessView(community, user, userId.toInt(), viewModel)
            }

            is InfoState.FAILURE -> {
                val message = (infoState as InfoState.FAILURE).message
                showBanner(context, message)
            }
            else -> {}
        }
    }
}

@Composable
fun InfoSuccessView(
    community: Community,
    user: UserAccount,
    userId: Int,
    viewModel: CommunityInfoViewModel
) {
    val configuration = LocalConfiguration.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.Start
    ) {
        IconButton(
            onClick = {
                viewModel.managePopBack()
            }
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_arrow_down),
                contentDescription = null,
                tint = colors.textPrimary,
                modifier = Modifier
                    .padding(horizontal = 20.dp, vertical = 28.dp)
                    .size(width = 18.dp, height = 24.dp)

            )
        }
        Box(
            modifier = Modifier
                .align(Alignment.CenterHorizontally)
                .motionClickEvent {
                    if (user.role == ADMIN_ROLE && userId == user.user_id) {
                        viewModel.openCommunityImageChanger()
                    }
                }
        ) {
            val imageHeight = (configuration.screenHeightDp * 0.18).dp

            ProfileImageView(
                modifier = Modifier
                    .padding(top = 64.dp)
                    .size(imageHeight)
                    .border(2.dp, colors.textPrimary, CircleShape)
                    .clip(CircleShape)
                    .background(colors.background),
                data = community.image_url,
                char = community.name.trim().ifEmpty { "?" }.first().uppercase()
            )

            if (user.role == ADMIN_ROLE && userId == user.user_id) {
                Image(
                    painter = painterResource(id = R.drawable.ic_image_edit),
                    contentDescription = null,
                    modifier = Modifier
                        .align(Alignment.BottomEnd)
                        .size((imageHeight.value * 0.30).dp)
                        .padding(end = 8.dp)
                )
            }
        }

        Text(
            text = community.name.ifEmpty { "" },
            modifier = Modifier
                .padding(top = 20.dp)
                .align(Alignment.CenterHorizontally),
            style = AppTheme.typography.h1TextStyle,
            color = colors.textPrimary,
        )

        Spacer(modifier = Modifier.height(120.dp))

        if (user.role == ADMIN_ROLE && userId == user.user_id) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .motionClickEvent {
                        viewModel.navigateToChangeNameScreen(
                            community.id.toString(),
                            encodeToBase64(community.name)
                        )
                    }
                    .fillMaxWidth()
                    .padding(horizontal = 24.dp, vertical = 16.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_change_name),
                    contentDescription = "rename Image",
                    tint = colors.textPrimary,
                    modifier = Modifier
                        .size(20.dp)
                )

                Text(
                    text = stringResource(R.string.community_info_change_name_text),
                    style = AppTheme.typography.bodyTextStyle2,
                    color = colors.textPrimary,
                    modifier = Modifier
                        .padding(start = 20.dp)
                        .weight(1f)
                )
            }
            DividedLineInfoView()
        }

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .motionClickEvent {
                    viewModel.navigateToShareActivities(community.id.toString())
                }
                .fillMaxWidth()
                .padding(horizontal = 24.dp, vertical = 16.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_share),
                contentDescription = "share Image",
                tint = colors.textPrimary,
                modifier = Modifier
                    .size(20.dp)
            )

            Text(
                text = stringResource(R.string.community_info_share_activities_text),
                style = AppTheme.typography.bodyTextStyle2,
                color = colors.textPrimary,
                modifier = Modifier
                    .padding(start = 20.dp)
                    .weight(1f)
            )
        }
        DividedLineInfoView()

        if (user.role == ADMIN_ROLE && userId == user.user_id) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .motionClickEvent {
                        viewModel.navigateToAddMembers(community.id)
                    }
                    .fillMaxWidth()
                    .padding(horizontal = 24.dp, vertical = 16.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_add_member),
                    contentDescription = "add Image",
                    tint = colors.textPrimary,
                    modifier = Modifier
                        .size(20.dp)
                )

                Text(
                    text = stringResource(R.string.community_info_add_member_text),
                    style = AppTheme.typography.bodyTextStyle2,
                    color = colors.textPrimary,
                    modifier = Modifier
                        .padding(start = 20.dp)
                        .weight(1f)
                )
            }
            DividedLineInfoView()

            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .motionClickEvent {
                        viewModel.navigateToPendingInvitations(community.id)
                    }
                    .fillMaxWidth()
                    .padding(horizontal = 24.dp, vertical = 16.dp)
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_pending_invitation_icon),
                    contentDescription = null,
                    tint = colors.textPrimary,
                    modifier = Modifier
                        .size(20.dp)
                )

                Text(
                    text = "Pending invitations",
                    style = AppTheme.typography.bodyTextStyle2,
                    color = colors.textPrimary,
                    modifier = Modifier
                        .padding(start = 20.dp)
                        .weight(1f)
                )
            }
            DividedLineInfoView()
        }

        val openLeaveCommunityDialogEvent by viewModel.openLeaveCommunityDialog.collectAsState()
        val openLeaveCommunityDialog =
            openLeaveCommunityDialogEvent.getContentIfNotHandled() ?: false

        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .motionClickEvent {
                    viewModel.openLeaveCommunityDialog()
                }
                .fillMaxWidth()
                .padding(horizontal = 24.dp, vertical = 16.dp)
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_leave_community),
                contentDescription = "leave Image",
                tint = colors.textPrimary,
                modifier = Modifier
                    .size(20.dp)
            )

            Text(
                text = stringResource(R.string.community_info_leave_community_text),
                style = AppTheme.typography.bodyTextStyle2,
                color = colors.textPrimary,
                modifier = Modifier
                    .padding(start = 20.dp)
                    .weight(1f)
            )
        }

        if (openLeaveCommunityDialog) {
            ShowLeaveCommunityDialog(viewModel, community.id)
        }
        DividedLineInfoView()
    }
}

@Composable
fun ShowLeaveCommunityDialog(viewModel: CommunityInfoViewModel, communityId: Int) {
    CustomAlertDialog(
        title = null,
        subTitle = stringResource(R.string.leave_community_dialog_message),
        confirmBtnText = stringResource(R.string.leave_community_dialog_yes_button),
        dismissBtnText = stringResource(R.string.leave_community_dialog_no_button),
        onConfirmClick = { viewModel.leaveCommunity(communityId) },
        onDismissClick = { viewModel.closeLeaveCommunityDialog() },
        confirmBtnColor = yesTextColor
    )
}

fun File.copyFrom(context: Context, imageUri: Uri) {
    try {
        val descriptor = context.contentResolver.openFileDescriptor(imageUri, "r")
        FileInputStream(
            descriptor!!.fileDescriptor
        ).use { input ->
            this.outputStream().use { output ->
                input.copyTo(output)
            }
        }
        descriptor.close()
    } catch (e: IOException) {
        Timber.e("Exception while fetching selected file:-$e")
    }
}

@Preview
@Composable
fun PreviewCommunityInfo() {
    CommunityInfoView("12", "")
}

private val yesTextColor = Color(0xFFCA2F27)
