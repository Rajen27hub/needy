package com.canopas.feature_community_ui.community

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.auth.AuthStateChangeListener
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.model.Community
import com.canopas.feature_community_data.model.CommunityJoinRequest
import com.canopas.feature_community_data.model.CommunityRequest
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.communityrequest.REQUEST_ACCEPT
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CommunityViewModel @Inject constructor(
    private val services: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val authManager: AuthManager,
    private val navManager: CommunityNavManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel(), AuthStateChangeListener {

    val state = MutableStateFlow<CommunityState>(CommunityState.LOADING)
    val requestState = MutableStateFlow<RequestState>(RequestState.START)
    val joinState = MutableStateFlow<JoinState>(JoinState.IDLE)
    val requestList = MutableStateFlow(listOf<CommunityRequest>())
    val loaderIndex = MutableStateFlow(0)

    fun getCommunityRequestList() = viewModelScope.launch {
        appAnalytics.logEvent("community_request_section_view", null)
        if (requestState.value !is RequestState.SUCCESS) {
            requestState.tryEmit(RequestState.LOADING)
        }
        withContext(appDispatcher.IO) {
            services.getCommunityRequests().onSuccess {
                requestList.value = it
                requestState.value = RequestState.SUCCESS
            }.onFailure { e ->
                Timber.e(e)
                e.localizedMessage?.let {
                    requestState.value = RequestState.FAILURE(it)
                }
            }
        }
    }

    fun acceptOrClearRequest(value: Int, communityId: Int, senderId: Int) {
        loaderIndex.value = communityId

        val request = if (value == REQUEST_ACCEPT) {
            appAnalytics.logEvent("accept_community_join_request_section", null)
            CommunityJoinRequest(
                communityId = communityId, senderId = senderId, status = true
            )
        } else {
            appAnalytics.logEvent("decline_community_join_request_section", null)
            CommunityJoinRequest(
                communityId = communityId, senderId = senderId, status = false
            )
        }
        updateCommunityJoinRequest(request)
    }

    private fun updateCommunityJoinRequest(request: CommunityJoinRequest) =
        viewModelScope.launch {
            joinState.value = JoinState.LOADING
            withContext(appDispatcher.IO) {
                services.updateCommunityRequest(request).onSuccess {
                    joinState.value = JoinState.FINISH
                    displayCommunityList()
                }.onFailure { e ->
                    Timber.e(e)
                    e.localizedMessage?.let {
                        joinState.value = JoinState.FAILURE(it)
                    }
                }
            }
        }

    fun resetJoinState() {
        joinState.value = JoinState.IDLE
    }

    fun resetRequestState() {
        requestState.value = RequestState.START
    }

    init {
        authManager.addListener(this)
    }

    override fun onCleared() {
        authManager.removeListener(this)
        super.onCleared()
    }

    private fun displayCommunityList() = viewModelScope.launch {
        if (state.value !is CommunityState.SUCCESS) {
            state.value = CommunityState.LOADING
        }

        withContext(appDispatcher.IO) {
            if (!authManager.authState.isVerified) {
                state.tryEmit(CommunityState.SUCCESS(emptyList()))
                return@withContext
            }
            services.getCommunities().onSuccess {
                getCommunityRequestList()
                state.tryEmit(CommunityState.SUCCESS(it))
            }.onFailure { e ->
                Timber.e(e)
                state.value = CommunityState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    fun getCommunities() {
        appAnalytics.logEvent("view_tab_community")
        displayCommunityList()
    }

    override fun onAuthStateChanged(authState: AuthState) {
        displayCommunityList()
    }

    fun resetState() {
        state.value = CommunityState.START
    }

    fun navigateToAddCommunity() {
        appAnalytics.logEvent("tap_add_community")
        if (authManager.authState.isVerified) {
            navManager.navigateToAddCommunity()
        } else {
            navManager.navigateToLogin()
        }
    }

    fun navigateToCommunityRequestsScreen() {
        navManager.navigateToCommunityRequestScreen()
    }

    fun navigateToCommunityDetailScreen(communityId: Int) {
        appAnalytics.logEvent("tap_community_list_item")
        navManager.navigateToCommunityDetailScreen(communityId.toString())
    }
}

sealed class RequestState {
    object START : RequestState()
    object LOADING : RequestState()
    object SUCCESS : RequestState()
    data class FAILURE(val message: String) : RequestState()
}

sealed class JoinState {
    object IDLE : JoinState()
    object LOADING : JoinState()
    object FINISH : JoinState()
    data class FAILURE(val message: String) : JoinState()
}

sealed class CommunityState {
    object START : CommunityState()
    object LOADING : CommunityState()
    data class SUCCESS(
        val communities: List<Community>
    ) : CommunityState()

    data class FAILURE(val message: String, val isNetworkError: Boolean) : CommunityState()
}
