package com.canopas.feature_community_ui.community

import android.annotation.SuppressLint
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterItalicFont
import com.canopas.base.ui.customview.EmptyCommunityView
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.feature_community_data.TestUtils.Companion.communities
import com.canopas.feature_community_data.TestUtils.Companion.communityRequest
import com.canopas.feature_community_data.model.Community
import com.canopas.feature_community_data.model.CommunityRequest
import com.canopas.feature_community_ui.R
import com.canopas.feature_community_ui.communityrequest.REQUEST_ACCEPT
import com.canopas.feature_community_ui.communityrequest.REQUEST_DELETE

@Composable
fun CommunityView() {
    val viewModel = hiltViewModel<CommunityViewModel>()
    val state by viewModel.state.collectAsState()
    val requestData by viewModel.requestList.collectAsState()
    val joinState by viewModel.joinState.collectAsState()
    val context = LocalContext.current
    val requestState by viewModel.requestState.collectAsState()
    val communityListState = rememberLazyListState()

    LaunchedEffect(key1 = Unit) {
        viewModel.getCommunities()
    }

    LaunchedEffect(key1 = joinState) {
        joinState.let {
            if (joinState is JoinState.FAILURE) {
                val message = (joinState as JoinState.FAILURE).message
                showBanner(context, message)
                viewModel.resetJoinState()
            }
        }
    }
    LaunchedEffect(key1 = requestState) {
        requestState.let {
            if (requestState is RequestState.FAILURE) {
                val message = (requestState as RequestState.FAILURE).message
                showBanner(context, message)
                viewModel.resetRequestState()
            }
        }
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
    ) {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(id = R.string.community_heading_text),
                    actions = {
                        IconButton(onClick = {
                            viewModel.navigateToAddCommunity()
                        }) {
                            Image(
                                painter = painterResource(id = R.drawable.ic_add_community),
                                contentDescription = null,
                                modifier = Modifier.size(14.dp)
                            )
                        }
                    }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )

        state.let { state ->
            when (state) {
                is CommunityState.LOADING -> {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(colors.background),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }

                is CommunityState.FAILURE -> {
                    if (state.isNetworkError) {
                        NetworkErrorView {
                            viewModel.getCommunities()
                        }
                    } else {
                        ServerErrorView {
                            viewModel.getCommunities()
                        }
                    }
                }

                is CommunityState.SUCCESS -> {
                    val communities = state.communities
                    CommunitySuccessView(communities, communityListState, requestData, viewModel)
                }
                else -> {}
            }
        }
    }
}

@Composable
fun CommunitySuccessView(
    communities: List<Community>,
    communityListState: LazyListState,
    requestList: List<CommunityRequest>,
    viewModel: CommunityViewModel,
) {
    val listOfRequest = requestList.take(2)

    if (communities.isEmpty() && requestList.isEmpty()) {
        EmptyCommunityView {
            viewModel.navigateToAddCommunity()
        }
    } else {
        LazyColumn(
            state = communityListState
        ) {

            if (requestList.isNotEmpty()) {

                itemsIndexed(listOfRequest) { index, item ->
                    val isLastIndex = listOfRequest.lastIndex == index
                    RequestView(viewModel, item, isLastIndex)
                }

                item {
                    if (requestList.size > 2) {
                        SeeAllRequestView(viewModel, requestList)
                    }
                }
            }
            item {
                if (requestList.isNotEmpty() && communities.isNotEmpty()) {
                    YouCommunityText()
                }
            }

            itemsIndexed(communities) { _, item ->
                CommunityListView(viewModel, item)
            }
        }
    }
}

@Composable
fun CommunityListView(viewModel: CommunityViewModel, listItem: Community) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 20.dp, start = 20.dp, end = 20.dp)
            .motionClickEvent {
                viewModel.navigateToCommunityDetailScreen(listItem.id)
            },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = rememberAsyncImagePainter(
                ImageRequest.Builder(LocalContext.current).data(
                    data = listItem.image_url.ifEmpty { null }
                ).apply(block = {
                    placeholder(R.drawable.ic_community_image)
                    fallback(R.drawable.ic_community_image)
                }).build()
            ),
            modifier = Modifier
                .size(42.dp)
                .border(1.dp, colors.textSecondary, CircleShape)
                .clip(CircleShape),
            contentScale = ContentScale.Crop,
            contentDescription = "CommunityImage"
        )
        Text(
            text = listItem.name,
            style = AppTheme.typography.subTitleTextStyle1,
            color = colors.textPrimary,
            lineHeight = 18.sp,
            modifier = Modifier.padding(start = 16.dp)
        )
    }
    Divider(
        Modifier
            .fillMaxWidth()
            .padding(top = 16.dp),
        thickness = 1.dp,
        color = if (isDarkMode) darkDividerColor else lightDividerColor
    )
}

@Composable
fun RequestView(
    viewModel: CommunityViewModel,
    requestItem: CommunityRequest,
    isLastIndex: Boolean,
) {
    Box(
        modifier = Modifier
            .fillMaxWidth(),
        contentAlignment = Alignment.TopCenter
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 20.dp, end = 20.dp, top = 12.dp)
            ) {
                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(LocalContext.current).data(
                            data = requestItem.imageUrl.ifEmpty { null }
                        ).apply(block = {
                            placeholder(R.drawable.ic_community_image)
                            fallback(R.drawable.ic_community_image)
                        }).build()
                    ),
                    modifier = Modifier
                        .size(56.dp)
                        .clip(CircleShape),
                    contentScale = ContentScale.Crop,
                    contentDescription = null
                )
                CommunityReqInfoText(viewModel = viewModel, requestItem = requestItem)
            }
            if (!isLastIndex) {
                Divider(
                    Modifier
                        .fillMaxWidth()
                        .padding(top = 20.dp, bottom = 8.dp),
                    thickness = 1.dp,
                    color = if (isDarkMode) darkDividerColor else lightDividerColor
                )
            } else {
                Spacer(modifier = Modifier.height(20.dp))
            }
        }
    }
}

@Composable
fun SeeAllRequestView(viewModel: CommunityViewModel, requestList: List<CommunityRequest>) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp, end = 20.dp),
        contentAlignment = Alignment.CenterEnd
    ) {
        Text(
            text = stringResource(id = R.string.community_view_see_all_requests_text) + " (${requestList.size})",
            style = AppTheme.typography.buttonStyle, color = colors.primary,
            modifier = Modifier
                .motionClickEvent { viewModel.navigateToCommunityRequestsScreen() }
        )
    }
}

@Composable
fun YouCommunityText() {
    Text(
        text = stringResource(R.string.community_view_your_communities_text),
        style = AppTheme.typography.yourCommunityText,
        color = colors.textPrimary,
        textAlign = TextAlign.Start,
        modifier = Modifier
            .padding(start = 20.dp, top = 40.dp)
            .fillMaxWidth()
    )
}

@SuppressLint("ResourceType")
@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun CommunityReqInfoText(viewModel: CommunityViewModel, requestItem: CommunityRequest) {
    val fullName = remember {
        "${requestItem.sender.firstName.trim()} ${requestItem.sender.lastName?.trim() ?: ""}"
    }
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 16.dp)
    ) {
        val text = buildAnnotatedString {
            withStyle(
                style = SpanStyle(
                    color = colors.textPrimary,
                )
            ) {
                append(fullName)
            }
            append(stringResource(R.string.community_request_sender_detail_text))
            withStyle(
                style = SpanStyle(
                    color = colors.textPrimary,
                    fontFamily = InterItalicFont,
                    fontWeight = FontWeight.SemiBold
                )
            ) {
                append(" ${requestItem.name} ")
            }
            append(stringResource(R.string.community_request_text))
        }
        Text(
            text = text,
            style = AppTheme.typography.monthDayTextStyle,
            color = colors.textSecondary,
            textAlign = TextAlign.Start,
            modifier = Modifier.fillMaxWidth(),
        )
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = context.resources.getQuantityString(
                    R.plurals.plurals_member,
                    requestItem.memberCount,
                    requestItem.memberCount
                ),
                style = AppTheme.typography.buttonStyle,
                color = colors.textSecondary,
                lineHeight = 18.sp
            )
            AcceptOrDeleteRequestBox(viewModel = viewModel, requestItem)
        }
    }
}

@Composable
fun AcceptOrDeleteRequestBox(viewModel: CommunityViewModel, requestItem: CommunityRequest) {
    val joinState by viewModel.joinState.collectAsState()
    val isLoading = joinState is JoinState.LOADING
    val loaderIndex by viewModel.loaderIndex.collectAsState()

    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.End
    ) {
        if (isLoading && requestItem.id == loaderIndex) {
            CircularProgressIndicator(
                modifier = Modifier
                    .padding(top = 4.dp, end = 30.dp)
                    .size(40.dp),
                color = colors.primary
            )
        } else {
            IconButton(
                onClick = {
                    viewModel.acceptOrClearRequest(
                        REQUEST_ACCEPT,
                        requestItem.id,
                        requestItem.sender.id
                    )
                },
                modifier = Modifier
                    .padding(start = 8.dp, top = 4.dp)
                    .size(40.dp)
                    .background(colors.primary, CircleShape)
                    .clip(CircleShape)
            ) {
                Icon(
                    painterResource(id = R.drawable.ic_request_check_icon),
                    contentDescription = null,
                    tint = Color.White
                )
            }
            IconButton(
                onClick = {
                    viewModel.acceptOrClearRequest(
                        REQUEST_DELETE,
                        requestItem.id,
                        requestItem.sender.id
                    )
                },
                modifier = Modifier
                    .padding(start = 8.dp, top = 4.dp)
                    .size(40.dp)
                    .border(1.dp, shape = CircleShape, color = colors.textSecondary.copy(0.4f))
                    .clip(shape = CircleShape)
            ) {
                Icon(
                    painterResource(id = R.drawable.ic_request_clear_icon),
                    contentDescription = null,
                    tint = if (isDarkMode) colors.textSecondary else colors.textSecondary.copy(0.4f)
                )
            }
        }
    }
}

@Preview
@Composable
fun PreviewCommunitySuccessView() {
    val viewModel = hiltViewModel<CommunityViewModel>()
    val communityListState = rememberLazyListState()
    CommunitySuccessView(
        communities = listOf(communities),
        communityListState,
        requestList = listOf(communityRequest),
        viewModel = viewModel
    )
}

private val darkDividerColor = Color(0xFf313131)
private val lightDividerColor = Color(0x141C191F)
