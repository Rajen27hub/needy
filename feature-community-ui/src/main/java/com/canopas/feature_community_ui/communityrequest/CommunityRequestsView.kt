package com.canopas.feature_community_ui.communityrequest

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterItalicFont
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.feature_community_data.model.CommunityRequest
import com.canopas.feature_community_ui.R

const val REQUEST_ACCEPT = 1
const val REQUEST_DELETE = 2

@Composable
fun CommunityRequestsView() {
    val viewModel = hiltViewModel<CommunityRequestsViewModel>()
    val state by viewModel.state.collectAsState()
    val joinState by viewModel.joinState.collectAsState()
    val listState = rememberLazyListState()
    val context = LocalContext.current

    LaunchedEffect(key1 = Unit) {
        viewModel.getCommunityRequestList()
    }
    LaunchedEffect(key1 = joinState) {
        joinState.let {
            if (joinState is JoinState.FAILURE) {
                val message = (joinState as JoinState.FAILURE).message
                showBanner(context, message)
                viewModel.resetJoinState()
            }
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = stringResource(R.string.community_request_top_bar_text),
                        navigationIconOnClick = {
                            viewModel.managePopBack()
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        state.let { state ->
            when (state) {
                is RequestState.LOADING -> {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(it)
                            .background(colors.background),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is RequestState.FAILURE -> {
                    if (state.isNetworkError) {
                        NetworkErrorView {
                            viewModel.onStart()
                        }
                    } else {
                        ServerErrorView {
                            viewModel.onStart()
                        }
                    }
                }
                is RequestState.SUCCESS -> {
                    val requestList = state.request
                    RequestListView(viewModel, requestList, listState)
                }
            }
        }
    }
}

@Composable
fun RequestListView(
    viewModel: CommunityRequestsViewModel,
    requestList: List<CommunityRequest>,
    listState: LazyListState
) {
    if (requestList.isEmpty()) {
        viewModel.managePopBack()
    } else {
        LazyColumn(state = listState) {
            itemsIndexed(requestList) { index, item ->
                val isLastIndex = requestList.lastIndex == index
                RequestView(viewModel, item, isLastIndex)
            }
        }
    }
}

@Composable
fun RequestView(
    viewModel: CommunityRequestsViewModel,
    requestItem: CommunityRequest,
    isLastIndex: Boolean
) {
    Box(
        modifier = Modifier
            .fillMaxWidth(),
        contentAlignment = Alignment.TopCenter
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 20.dp, end = 20.dp, top = 12.dp)
            ) {
                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(LocalContext.current).data(
                            data = requestItem.imageUrl.ifEmpty { null }
                        ).apply(block = {
                            placeholder(R.drawable.ic_community_image)
                            fallback(R.drawable.ic_community_image)
                        }).build()
                    ),
                    modifier = Modifier
                        .size(56.dp)
                        .clip(CircleShape),
                    contentScale = ContentScale.Crop,
                    contentDescription = null
                )
                CommunityRequestInfoText(viewModel = viewModel, requestItem = requestItem)
            }
            if (!isLastIndex) {
                Divider(
                    Modifier
                        .fillMaxWidth()
                        .padding(top = 20.dp, bottom = 8.dp),
                    thickness = 1.dp,
                    color = if (isDarkMode) darkDividerColor else dividerColor,
                )
            } else {
                Spacer(modifier = Modifier.height(20.dp))
            }
        }
    }
}

@Composable
fun CommunityRequestInfoText(viewModel: CommunityRequestsViewModel, requestItem: CommunityRequest) {
    val fullName = remember {
        "${requestItem.sender.firstName.trim()} ${requestItem.sender.lastName?.trim() ?: ""}"
    }
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 16.dp)
    ) {
        val text = buildAnnotatedString {
            withStyle(
                style = SpanStyle(
                    color = colors.textPrimary,
                )
            ) {
                append(fullName)
            }
            append(stringResource(R.string.community_request_sender_detail_text))
            withStyle(
                style = SpanStyle(
                    color = colors.textPrimary,
                    fontFamily = InterItalicFont,
                    fontWeight = FontWeight.SemiBold
                )
            ) {
                append(" ${requestItem.name} ")
            }
            append(stringResource(R.string.community_request_text))
        }
        Text(
            text = text,
            style = AppTheme.typography.monthDayTextStyle,
            color = colors.textSecondary,
            textAlign = TextAlign.Start,
            modifier = Modifier.fillMaxWidth(),
        )
        Row(
            modifier = Modifier.fillMaxWidth()
        ) {
            Text(
                text = context.resources.getQuantityString(
                    R.plurals.plurals_member,
                    requestItem.memberCount,
                    requestItem.memberCount
                ),
                style = AppTheme.typography.buttonStyle,
                color = colors.textSecondary,
                lineHeight = 18.sp
            )
            AcceptOrDeleteRequestBox(viewModel = viewModel, requestItem)
        }
    }
}

@Composable
fun AcceptOrDeleteRequestBox(viewModel: CommunityRequestsViewModel, requestItem: CommunityRequest) {
    val joinState by viewModel.joinState.collectAsState()
    val isLoading = joinState is JoinState.LOADING
    val loaderIndex by viewModel.loaderIndex.collectAsState()
    val enableBtn = !(isLoading && requestItem.id == loaderIndex)

    Row(
        modifier = Modifier.fillMaxWidth(),
        horizontalArrangement = Arrangement.End
    ) {
        IconButton(
            onClick = {
                viewModel.acceptOrClearRequest(
                    REQUEST_ACCEPT,
                    requestItem.id,
                    requestItem.sender.id
                )
            },
            enabled = enableBtn,
            modifier = Modifier
                .padding(start = 8.dp, top = 4.dp)
                .size(40.dp)
                .background(
                    if (enableBtn) colors.primary else colors.primary.copy(0.6f),
                    CircleShape
                )
                .clip(CircleShape)
        ) {
            Icon(
                painterResource(id = R.drawable.ic_request_check_icon),
                contentDescription = null,
                tint = Color.White
            )
        }
        IconButton(
            onClick = {
                viewModel.acceptOrClearRequest(
                    REQUEST_DELETE,
                    requestItem.id,
                    requestItem.sender.id
                )
            },
            enabled = enableBtn,
            modifier = Modifier
                .padding(start = 8.dp, top = 4.dp)
                .size(40.dp)
                .border(1.dp, shape = CircleShape, color = colors.textSecondary.copy(0.4f))
                .clip(shape = CircleShape)
        ) {
            Icon(
                painterResource(id = R.drawable.ic_request_clear_icon),
                contentDescription = null,
                tint = if (isDarkMode) colors.textSecondary else colors.textSecondary.copy(0.4f)
            )
        }
    }
}

private val dividerColor = Color(0xF000000)
private val darkDividerColor = Color(0xFf313131)
