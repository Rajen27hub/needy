package com.canopas.feature_community_ui.communityrequest

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.model.CommunityJoinRequest
import com.canopas.feature_community_data.model.CommunityRequest
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CommunityRequestsViewModel @Inject constructor(
    private val navManager: CommunityNavManager,
    private val services: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<RequestState>(RequestState.LOADING)
    val joinState = MutableStateFlow<JoinState>(JoinState.NONE)
    val loaderIndex = MutableStateFlow(0)

    fun getCommunityRequestList() = viewModelScope.launch {
        appAnalytics.logEvent("community_request_view", null)
        if (state.value !is RequestState.SUCCESS) {
            state.tryEmit(RequestState.LOADING)
        }
        withContext(appDispatcher.IO) {
            services.getCommunityRequests().onSuccess { requestList ->
                state.value = RequestState.SUCCESS(requestList)
            }.onFailure { e ->
                Timber.e(e)
                state.value = RequestState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    fun acceptOrClearRequest(value: Int, communityId: Int, senderId: Int) {
        loaderIndex.value = communityId

        val request = if (value == REQUEST_ACCEPT) {
            appAnalytics.logEvent("accept_community_join_request", null)
            CommunityJoinRequest(
                communityId = communityId, senderId = senderId, status = true
            )
        } else {
            appAnalytics.logEvent("decline_community_join_request", null)
            CommunityJoinRequest(
                communityId = communityId, senderId = senderId, status = false
            )
        }
        updateCommunityJoinRequest(request)
    }

    private fun updateCommunityJoinRequest(request: CommunityJoinRequest) =
        viewModelScope.launch {
            joinState.value = JoinState.LOADING
            withContext(appDispatcher.IO) {
                services.updateCommunityRequest(request).onSuccess {
                    joinState.value = JoinState.FINISH
                    onStart()
                }.onFailure { e ->
                    Timber.e(e)
                    joinState.value = JoinState.FAILURE(e.toUserError(resources))
                }
            }
        }

    fun resetJoinState() {
        joinState.value = JoinState.NONE
    }

    fun onStart() {
        getCommunityRequestList()
    }

    fun managePopBack() {
        navManager.popBackStack()
    }
}

sealed class RequestState {
    object LOADING : RequestState()
    data class SUCCESS(val request: List<CommunityRequest>) : RequestState()
    data class FAILURE(val message: String, val isNetworkError: Boolean) : RequestState()
}

sealed class JoinState {
    object NONE : JoinState()
    object LOADING : JoinState()
    object FINISH : JoinState()
    data class FAILURE(val message: String) : JoinState()
}
