package com.canopas.feature_community_ui.pendinginvitation

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterBoldFont
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.feature_community_data.model.PendingInvitation
import com.canopas.feature_community_ui.R

@Composable
fun PendingInvitationsView(communityID: String) {
    val viewModel = hiltViewModel<PendingInvitationsViewModel>()
    val state by viewModel.state.collectAsState()
    val deleteState by viewModel.deleteState.collectAsState()
    val listState = rememberLazyListState()
    val context = LocalContext.current

    LaunchedEffect(key1 = communityID) {
        viewModel.onStart(communityID.toInt())
    }

    LaunchedEffect(key1 = deleteState) {
        if (deleteState is DeleteState.FAILURE) {
            val message = (deleteState as DeleteState.FAILURE).message
            showBanner(context, message)
            viewModel.resetDeleteState()
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = stringResource(R.string.pending_invitation_top_bar_text),
                        navigationIconOnClick = {
                            viewModel.navigatePopBack()
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        state.let { state ->
            when (state) {
                is InvitationState.LOADING -> {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(colors.background)
                            .padding(it),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is InvitationState.FAILURE -> {
                    if (state.isNetworkError) {
                        NetworkErrorView {}
                    } else {
                        ServerErrorView {}
                    }
                }
                is InvitationState.SUCCESS -> {
                    val pendingList = state.pendingList
                    PendingListView(viewModel, pendingList, listState)
                }
            }
        }
    }
}

@Composable
fun PendingListView(
    viewModel: PendingInvitationsViewModel,
    pendingList: List<PendingInvitation>,
    listState: LazyListState
) {
    if (pendingList.isEmpty()) {
        Column(
            modifier = Modifier
                .fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            Text(
                stringResource(R.string.community_pending_invitation_empty_list_text),
                color = colors.textSecondary
            )
        }
    } else {
        Column(
            modifier = Modifier.fillMaxSize(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            LazyColumn(state = listState) {
                itemsIndexed(pendingList) { index, item ->
                    val isLastIndex = pendingList.lastIndex == index
                    PendingListItemsView(viewModel, item, isLastIndex)
                }
            }
        }
    }
}

@Composable
fun PendingListItemsView(
    viewModel: PendingInvitationsViewModel,
    pendingItem: PendingInvitation,
    isLastIndex: Boolean
) {
    val deleteState by viewModel.deleteState.collectAsState()
    val isLoading = deleteState is DeleteState.LOADING
    val loadingIndex by viewModel.loaderIndex.collectAsState()
    val fullName = "${pendingItem.first_name.trim()} ${pendingItem.last_name?.trim() ?: ""}"
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 12.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            ProfileImageView(
                data = pendingItem.profileUrl,
                modifier = Modifier
                    .size(56.dp)
                    .border(
                        1.dp,
                        shape = CircleShape,
                        color = if (isDarkMode) colors.textSecondary else lightBorderColor
                    ),
                char = pendingItem.first_name.trim().first().uppercase()
            )
            Text(
                text = buildAnnotatedString {
                    withStyle(
                        style = SpanStyle(
                            color = colors.textPrimary,
                            fontFamily = InterBoldFont
                        )
                    ) {
                        append(stringResource(R.string.pending_invitation_you_text))
                    }
                    append(stringResource(R.string.pending_invitation_common_text))
                    withStyle(
                        style = SpanStyle(
                            color = colors.textPrimary,
                            fontFamily = InterBoldFont
                        )
                    ) {
                        append(" $fullName ")
                    }
                    append(stringResource(R.string.pending_invitation_common_community_text))
                },
                style = AppTheme.typography.tabTextStyle,
                lineHeight = 18.sp,
                color = colors.textSecondary,
                modifier = Modifier
                    .padding(start = 16.dp)
                    .weight(1f)
            )
            if (isLoading && loadingIndex == pendingItem.id) {
                CircularProgressIndicator(
                    modifier = Modifier.padding(start = 12.dp)
                        .size(40.dp),
                    color = colors.primary
                )
            } else {
                IconButton(
                    onClick = { viewModel.deletePendingInvitation(pendingItem.id) },
                    modifier = Modifier
                        .padding(start = 12.dp)
                        .size(40.dp)
                        .border(1.dp, shape = CircleShape, color = colors.textSecondary.copy(0.4f))
                        .clip(CircleShape)
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_pending_delete_icon),
                        contentDescription = null,
                        tint = if (isDarkMode) colors.textSecondary else colors.textSecondary.copy(0.4f)
                    )
                }
            }
        }
    }
    if (!isLastIndex) {
        Divider(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 20.dp, bottom = 8.dp),
            thickness = 1.dp,
            color = if (isDarkMode) darkDividerColor else lightDividerColor,
        )
    } else {
        Spacer(modifier = Modifier.height(20.dp))
    }
}

private val lightBorderColor = Color(0x66000000)
private val darkDividerColor = Color(0xFf313131)
private val lightDividerColor = Color(0x141C191F)
