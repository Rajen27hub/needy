package com.canopas.feature_community_ui.pendinginvitation

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.model.PendingInvitation
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class PendingInvitationsViewModel @Inject constructor(
    private val navManager: CommunityNavManager,
    private val service: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<InvitationState>(InvitationState.LOADING)
    val deleteState = MutableStateFlow<DeleteState>(DeleteState.NONE)
    val communityId = MutableStateFlow(0)
    val loaderIndex = MutableStateFlow(0)

    fun onStart(id: Int) {
        communityId.value = id
        getPendingInvitation(communityId = id)
    }

    fun getPendingInvitation(communityId: Int) = viewModelScope.launch {
        if (state.value !is InvitationState.SUCCESS) {
            state.value = InvitationState.LOADING
        }
        withContext(appDispatcher.IO) {
            service.getPendingInvitation(communityId).onSuccess { invitationList ->
                state.value = InvitationState.SUCCESS(invitationList)
            }.onFailure { e ->
                Timber.e(e)
                state.value = InvitationState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    private fun deleteInvitation(receiverId: Int) = viewModelScope.launch {
        deleteState.value = DeleteState.LOADING
        withContext(appDispatcher.IO) {
            service.deletePendingInvitation(communityId.value, receiverId).onSuccess {
                deleteState.value = DeleteState.DELETE
                getPendingInvitation(communityId.value)
            }.onFailure { e ->
                Timber.e(e)
                deleteState.value = DeleteState.FAILURE(e.toUserError(resources))
            }
        }
    }

    fun deletePendingInvitation(receiverId: Int) {
        appAnalytics.logEvent("tap_delete_pending_invitation", null)
        loaderIndex.value = receiverId
        deleteInvitation(receiverId)
    }

    fun resetDeleteState() {
        deleteState.value = DeleteState.NONE
    }

    fun navigatePopBack() {
        navManager.popBackStack()
    }
}

sealed class InvitationState {
    object LOADING : InvitationState()
    data class SUCCESS(val pendingList: List<PendingInvitation>) : InvitationState()
    data class FAILURE(val message: String, val isNetworkError: Boolean) : InvitationState()
}

sealed class DeleteState {
    object NONE : DeleteState()
    object LOADING : DeleteState()
    object DELETE : DeleteState()
    data class FAILURE(val message: String) : DeleteState()
}
