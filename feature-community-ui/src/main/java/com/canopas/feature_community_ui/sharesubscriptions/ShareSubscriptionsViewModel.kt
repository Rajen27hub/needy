package com.canopas.feature_community_ui.sharesubscriptions

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.Subscription
import com.canopas.feature_community_data.model.ShareSubscriptions
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ShareSubscriptionsViewModel @Inject constructor(
    private val service: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val navManager: CommunityNavManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<ShareSubscriptionState>(ShareSubscriptionState.LOADING)
    val selectedSubscriptions = MutableStateFlow(mutableListOf<Int>())
    val sharedSubscriptions = MutableStateFlow(mutableListOf<Int>())

    fun getUserSubscriptions(showSkipButton: Boolean, communityId: String, userId: String) = viewModelScope.launch {
        state.tryEmit(ShareSubscriptionState.LOADING)
        withContext(appDispatcher.IO) {
            appAnalytics.logEvent("view_community_share_activities")
            val fetchSharedSubscriptions = async { service.getSharedSubscriptionsList(communityId, userId) }
            val fetchSubscriptionsToShare = async { service.getSubscriptionsToShare() }
            if (!showSkipButton) {
                fetchSharedSubscriptions.await().onSuccess { subscriptions ->
                    val list = ArrayList(sharedSubscriptions.value)
                    subscriptions.asIterable().forEach {
                        list.add(it.id)
                    }
                    sharedSubscriptions.tryEmit(list)
                    selectedSubscriptions.tryEmit(list)
                }.onFailure { e ->
                    Timber.e(e)
                    state.tryEmit(ShareSubscriptionState.FAILURE(e.toUserError(resources)))
                }
            }
            fetchSubscriptionsToShare.await().onSuccess { subscriptions ->
                state.tryEmit(ShareSubscriptionState.SUCCESS(subscriptions))
            }.onFailure { e ->
                Timber.e(e)
                state.tryEmit(ShareSubscriptionState.FAILURE(e.toUserError(resources)))
            }
        }
    }

    fun toggleSubscriptionsSelection(subscriptionId: Int) {
        appAnalytics.logEvent("tap_share_activities_activity")
        val list = ArrayList(selectedSubscriptions.value)
        if (list.contains(subscriptionId)) {
            list.remove(subscriptionId)
        } else {
            list.add(subscriptionId)
        }
        selectedSubscriptions.tryEmit(list)
    }

    fun shareSubscriptions(communityId: String) {
        viewModelScope.launch {
            state.tryEmit(ShareSubscriptionState.LOADING)
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("tap_share_activities_share_button")
                service.shareSubscriptionsWithCommunity(communityId.toInt(), ShareSubscriptions(selectedSubscriptions.value)).onSuccess {
                    state.tryEmit(ShareSubscriptionState.SHARESUCCESS)
                }.onFailure { e ->
                    Timber.e(e.localizedMessage)
                    state.tryEmit(ShareSubscriptionState.FAILURE(e.toUserError(resources)))
                }
            }
        }
    }

    fun navigateToCommunityDetailScreen(communityId: String) {
        appAnalytics.logEvent("tap_share_activities_skip_button")
        navManager.popCommunityScreens()
        navManager.navigateToCommunityDetailScreenForFirstTime(communityId)
    }

    fun popBack() {
        navManager.popBackStack()
    }
}

sealed class ShareSubscriptionState {
    object LOADING : ShareSubscriptionState()
    data class SUCCESS(val subscriptions: List<Subscription>) : ShareSubscriptionState()
    object SHARESUCCESS : ShareSubscriptionState()
    data class FAILURE(val message: String) : ShareSubscriptionState()
}
