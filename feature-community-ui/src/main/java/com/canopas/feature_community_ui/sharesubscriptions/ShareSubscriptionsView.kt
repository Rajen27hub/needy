package com.canopas.feature_community_ui.sharesubscriptions

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.FabPosition
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.CheckBox
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.DisabledButton
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.Subscription
import com.canopas.feature_community_ui.R

@Composable
fun ShareSubscriptionsView(communityId: String, userId: String = "") {
    val viewModel = hiltViewModel<ShareSubscriptionsViewModel>()
    val state by viewModel.state.collectAsState()
    val subscriptionsListState = rememberLazyListState()
    val selectedSubscriptions by viewModel.selectedSubscriptions.collectAsState()
    val sharedSubscriptions by viewModel.sharedSubscriptions.collectAsState()
    val showSkipButton = userId == ""

    if (showSkipButton) {
        BackHandler(enabled = true, onBack = {
            // Do Nothing
            // Block user from using System's Back Action
        })
    }

    val buttonText =
        if (sharedSubscriptions.isEmpty()) stringResource(R.string.share_subscriptions_share_btn_text) else stringResource(
            R.string.share_subscriptions_update_btn_text
        )

    LaunchedEffect(key1 = Unit, block = {
        viewModel.getUserSubscriptions(showSkipButton, communityId, userId)
    })

    val context = LocalContext.current
    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = stringResource(R.string.share_activities_title_text),
                        navigationIconOnClick = { viewModel.popBack() },
                        enableNavigationIcon = !showSkipButton,
                        actions = {
                            if (showSkipButton) {
                                TextButton(
                                    onClick = {
                                        viewModel.navigateToCommunityDetailScreen(
                                            communityId
                                        )
                                    },
                                    modifier = Modifier.motionClickEvent { }
                                ) {
                                    Text(
                                        text = stringResource(R.string.share_subscriptions_skip_action_text),
                                        style = AppTheme.typography.topBarActionTextStyle,
                                        color = colors.textSecondary
                                    )
                                }
                            }
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        },
        backgroundColor = colors.background,
        floatingActionButton = {
            PrimaryButton(
                onClick = {
                    viewModel.shareSubscriptions(communityId)
                },
                enabled = selectedSubscriptions != sharedSubscriptions,
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .motionClickEvent { }
                    .fillMaxWidth(),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = colors.primary,
                    contentColor = Color.White,
                    disabledBackgroundColor = DisabledButton
                ),
                text = buttonText
            )
        },
        floatingActionButtonPosition = FabPosition.Center
    ) {
        state.let { state ->
            when (state) {
                is ShareSubscriptionState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(colors.background)
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is ShareSubscriptionState.FAILURE -> {
                    val message = state.message
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                    }
                }
                is ShareSubscriptionState.SUCCESS -> {
                    val subscriptions = state.subscriptions
                    ShareSubscriptionSuccessState(subscriptions, viewModel, subscriptionsListState)
                }
                is ShareSubscriptionState.SHARESUCCESS -> {
                    if (showSkipButton) {
                        viewModel.navigateToCommunityDetailScreen(communityId)
                    } else {
                        viewModel.popBack()
                    }
                }
            }
        }
    }
}

@Composable
fun ShareSubscriptionSuccessState(
    subscriptions: List<Subscription>,
    viewModel: ShareSubscriptionsViewModel,
    subscriptionsListState: LazyListState,
) {
    val selectedSubscriptions by viewModel.selectedSubscriptions.collectAsState()

    if (subscriptions.isEmpty()) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(colors.background),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = stringResource(R.string.share_subscriptions_empty_subscription_text),
                style = AppTheme.typography.h3TextStyle,
                color = colors.primary
            )
        }
    } else {
        val unSelectedActivity = if (isDarkMode) darkActivityCard else lightActivityCard
        LazyColumn(
            state = subscriptionsListState,
            contentPadding = PaddingValues(horizontal = 20.dp, vertical = 48.dp)
        ) {
            items(subscriptions) { item ->
                Box(
                    modifier = Modifier.fillMaxWidth(),
                    contentAlignment = Alignment.Center
                ) {
                    Box(
                        modifier = Modifier
                            .padding(vertical = 8.dp)
                            .widthIn(max = 600.dp)
                            .motionClickEvent {
                                viewModel.toggleSubscriptionsSelection(item.id)
                            }
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .clip(shape = RoundedCornerShape(15.dp))
                            .background(
                                color = if (item.id in selectedSubscriptions) if (isDarkMode) darkBackground else lightBackground
                                else unSelectedActivity,
                                shape = RoundedCornerShape(16.dp)
                            )
                    ) {
                        Row(
                            modifier = Modifier
                                .align(Alignment.CenterStart)
                                .fillMaxWidth(0.8f),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Start
                        ) {
                            item.activity.image_url2?.let {
                                AsyncImage(
                                    model = ImageRequest.Builder(LocalContext.current)
                                        .data(it.ifEmpty { item.activity.image_url })
                                        .decoderFactory(SvgDecoder.Factory())
                                        .build(),
                                    modifier = Modifier
                                        .padding(horizontal = 16.dp, vertical = 4.dp)
                                        .size(50.dp),
                                    contentDescription = null
                                )
                            }
                            Text(
                                text = item.activity.name,
                                style = AppTheme.typography.bodyTextStyle1,
                                color = if (item.id in selectedSubscriptions) Color.Black else colors.textPrimary,
                            )
                        }
                        Icon(
                            Icons.Filled.CheckBox,
                            contentDescription = null,
                            modifier = Modifier
                                .padding(12.dp)
                                .size(26.dp)
                                .align(Alignment.CenterEnd),
                            tint = if (item.id in selectedSubscriptions) colors.primary else if (isDarkMode) darkCheckBox else lightCheckBox
                        )
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun PreviewShareSubscriptions() {
    ShareSubscriptionsView(communityId = "20", "")
}

private val darkBackground = Color(0xffFCD8C3)
private val lightBackground = Color(0x4DF67C37)
private val lightActivityCard = Color(0xD1C191F)
private val darkActivityCard = Color(0xFF222222)
private val darkCheckBox = Color(0x33A6A2A2)
private val lightCheckBox = Color(0x331C191F)
