package com.canopas.feature_community_ui.addcommunitymembers

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.UserAccount
import com.canopas.feature_community_data.di.CommunityCache
import com.canopas.feature_community_data.model.JoinRequest
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

const val DEFAULT_DEBOUNCE_DELAY = 600L

@OptIn(FlowPreview::class)
@HiltViewModel
class AddCommunityMembersViewModel @Inject constructor(
    private val service: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val navManager: CommunityNavManager,
    private val communityCache: CommunityCache,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val showButtonLoader = MutableStateFlow(false)
    val searchString = MutableStateFlow("")
    val state = MutableStateFlow<AddMembersState>(AddMembersState.START)
    val searchResult = MutableStateFlow<List<UserAccount>>(emptyList())
    val selectedMembers = MutableStateFlow(mutableListOf<UserAccount>())
    val communityMembers = MutableStateFlow(mutableListOf<UserAccount>())
    val showInviteButton = MutableStateFlow(false)
    val communityId = MutableStateFlow("")

    init {
        viewModelScope.launch {
            searchString.debounce(DEFAULT_DEBOUNCE_DELAY).flowOn(appDispatcher.IO)
                .collectLatest { searchStr ->
                    if (searchStr.isNotEmpty()) {
                        withContext(appDispatcher.IO) {
                            service.searchMembers(search = searchStr, communityId.value).onSuccess { usersList ->
                                state.tryEmit(AddMembersState.SEARCHSUCCESS)
                                searchResult.tryEmit(usersList)
                                if (searchResult.value.isEmpty()) showInviteButton.tryEmit(true)
                                else showInviteButton.tryEmit(false)
                            }.onFailure {
                                Timber.e(it.localizedMessage)
                                state.tryEmit(AddMembersState.FAILURE(it.toUserError(resources)))
                            }
                        }
                    } else {
                        searchResult.value = listOf()
                        state.tryEmit(AddMembersState.START)
                    }
                }
        }
    }

    fun onSearchValueChange(value: String) {
        searchString.value = value
        if (searchString.value.isEmpty()) showInviteButton.tryEmit(false)
        state.tryEmit(AddMembersState.LOADING)
    }

    fun onUserSelected(user: UserAccount) {
        val list = ArrayList(selectedMembers.value)
        if (!list.contains(user)) {
            list.add(user)
            selectedMembers.tryEmit(list)
        }
    }

    fun removeUser(user: UserAccount) {
        val list = ArrayList(selectedMembers.value)
        list.remove(user)
        selectedMembers.tryEmit(list)
    }

    fun manageNavigation(communityId: String, showSkipButton: Boolean) {
        if (showSkipButton) {
            navManager.navigateToShareSubscriptions(communityId.toInt(), "")
        } else {
            managePopBack()
        }
    }

    fun sendCommunityJoinRequest(communityId: String) {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                showButtonLoader.tryEmit(true)
                appAnalytics.logEvent("tap_add_member_add_button")
                service.sendCommunityJoinRequest(JoinRequest(communityId.toInt(), selectedMembers.value.map { it.id })).onSuccess {
                    state.tryEmit(AddMembersState.SUCCESS)
                    showButtonLoader.tryEmit(false)
                }.onFailure {
                    Timber.e(it)
                    state.tryEmit(AddMembersState.FAILURE(it.toUserError(resources)))
                    showButtonLoader.tryEmit(false)
                }
            }
        }
    }

    fun onSkipButtonClicked() {
        appAnalytics.logEvent("tap_add_member_skip_button")
        state.tryEmit(AddMembersState.SUCCESS)
    }

    fun onStart(communityId: String) {
        this.communityId.value = communityId
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("view_community_add_members")
                state.tryEmit(AddMembersState.LOADING)
                val cachedCommunity = communityCache.getCommunity(communityId)
                if (cachedCommunity != null) {
                    state.tryEmit(AddMembersState.FOUNDUSERS)
                    if (cachedCommunity.users.isNotEmpty()) {
                        communityMembers.tryEmit(cachedCommunity.users as MutableList)
                    }
                } else {
                    service.getCommunity(communityId.toInt()).onSuccess { community ->
                        state.tryEmit(AddMembersState.FOUNDUSERS)
                        if (community.users.isNotEmpty()) {
                            communityMembers.tryEmit(community.users as MutableList)
                        }
                    }.onFailure { e ->
                        Timber.e(e)
                        state.value = AddMembersState.FAILURE(e.toUserError(resources))
                    }
                }
            }
        }
    }

    fun managePopBack() {
        navManager.popBackStack()
    }

    fun logShareInviteEvent() {
        appAnalytics.logEvent("tap_add_member_share_invite_link")
    }
}

sealed class AddMembersState {
    object START : AddMembersState()
    object LOADING : AddMembersState()
    object SEARCHSUCCESS : AddMembersState()
    object SUCCESS : AddMembersState()
    object FOUNDUSERS : AddMembersState()
    data class FAILURE(val message: String) : AddMembersState()
}
