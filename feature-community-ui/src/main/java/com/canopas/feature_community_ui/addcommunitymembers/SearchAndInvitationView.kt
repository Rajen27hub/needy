package com.canopas.feature_community_ui.addcommunitymembers

import android.content.Intent
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material.icons.sharp.Cancel
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusManager
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.data.model.UserAccount
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.DisabledButton
import com.canopas.base.ui.InterSemiBoldFont
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.feature_community_ui.R

@Suppress("FunctionName")
fun SearchBarView(
    viewModel: AddCommunityMembersViewModel,
    searchValue: String,
    selectedMembers: MutableList<UserAccount>,
    focusManager: FocusManager,
    lazyListScope: LazyListScope
) {
    lazyListScope.item {
        CompositionLocalProvider(LocalTextSelectionColors provides TextSelectionColors(ComposeTheme.colors.primary, ComposeTheme.colors.primary)) {
            TextField(
                value = searchValue,
                placeholder = {
                    Text(
                        stringResource(R.string.add_community_member_search_member_placeholder_text),
                        color = ComposeTheme.colors.textSecondary,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                },
                onValueChange = { value ->
                    viewModel.onSearchValueChange(value)
                },
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        start = 20.dp,
                        end = 20.dp,
                        bottom = 40.dp,
                        top = if (selectedMembers.isEmpty()) 20.dp else 4.dp
                    ),
                textStyle = AppTheme.typography.bodyTextStyle1,
                leadingIcon = {
                    Icon(
                        Icons.Default.Search,
                        contentDescription = null,
                        tint = ComposeTheme.colors.textPrimary
                    )
                },
                trailingIcon = {
                    if (searchValue.isNotEmpty()) {
                        IconButton(onClick = {
                            viewModel.onSearchValueChange("")
                        }) {
                            Icon(
                                Icons.Sharp.Cancel,
                                contentDescription = null,
                                tint = ComposeTheme.colors.textPrimary
                            )
                        }
                    }
                },
                shape = RoundedCornerShape(corner = CornerSize(20.dp)),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Search
                ),
                keyboardActions = KeyboardActions(onSearch = { focusManager.clearFocus() }),
                colors = TextFieldDefaults.textFieldColors(
                    textColor = ComposeTheme.colors.textPrimary,
                    cursorColor = ComposeTheme.colors.primary,
                    leadingIconColor = ComposeTheme.colors.textPrimary,
                    backgroundColor = if (ComposeTheme.isDarkMode) darkSearchBar else profileBorder,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent
                )
            )
        }
    }
}

@Suppress("FunctionName")
fun InvitationView(
    viewModel: AddCommunityMembersViewModel,
    showInviteView: Boolean,
    searchValue: String,
    lazyListScope: LazyListScope
) {
    lazyListScope.item {
        if (showInviteView && searchValue.isNotEmpty()) {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(
                    Intent.EXTRA_TEXT,
                    stringResource(R.string.add_community_screen_share_link_message)
                )
                type = "text/plain"
            }
            val shareIntent = Intent.createChooser(sendIntent, null)
            val context = LocalContext.current
            Column(
                modifier = Modifier
                    .fillParentMaxHeight(0.6f)
                    .fillParentMaxWidth(),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = stringResource(
                        R.string.add_community_member_could_not_find_text,
                        searchValue
                    ),
                    style = AppTheme.typography.h2TextStyle,
                    color = ComposeTheme.colors.textSecondary,
                    lineHeight = 38.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp),
                    textAlign = TextAlign.Center
                )
                PrimaryButton(
                    onClick = {
                        context.startActivity(shareIntent)
                        viewModel.logShareInviteEvent()
                    },
                    text = stringResource(R.string.add_community_member_share_invite_link_btn_text),
                    modifier = Modifier
                        .widthIn(max = 600.dp)
                        .fillMaxWidth()
                        .padding(top = 24.dp),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = ComposeTheme.colors.primary,
                        contentColor = Color.White,
                        disabledBackgroundColor = DisabledButton
                    )
                )
                Text(
                    text = buildAnnotatedString {
                        append(stringResource(R.string.add_community_member_once_the_person_text))
                        withStyle(
                            style = SpanStyle(fontFamily = InterSemiBoldFont)
                        ) {
                            append(stringResource(R.string.add_community_member_justly_text))
                        }
                        append(stringResource(R.string.add_community_member_add_to_community_text))
                    },
                    lineHeight = 22.sp,
                    style = AppTheme.typography.bodyTextStyle3,
                    color = ComposeTheme.colors.textSecondary,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp, start = 36.dp, end = 36.dp),
                )
            }
        }
    }
}

private val profileBorder = Color(0x1A1C191F)
private val darkSearchBar = Color(0xFF222222)
