package com.canopas.feature_community_ui.addcommunitymembers

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.FabPosition
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.DisabledButton
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.feature_community_ui.R

const val CONDITIONAL_LIST_SIZE = 1
const val INITIAL_SCROLL_INDEX = 0

@Composable
fun AddCommunityMembersView(communityId: String, showSkipButton: Boolean) {

    val viewModel = hiltViewModel<AddCommunityMembersViewModel>()
    val searchValue by viewModel.searchString.collectAsState()
    val membersList by viewModel.searchResult.collectAsState()
    val selectedMembers by viewModel.selectedMembers.collectAsState()
    val listState = rememberLazyListState()
    val state by viewModel.state.collectAsState()
    val showLoader = state is AddMembersState.LOADING
    val focusManager = LocalFocusManager.current
    val showInviteView by viewModel.showInviteButton.collectAsState()
    val communityMembers by viewModel.communityMembers.collectAsState()
    if (showSkipButton) {
        BackHandler(enabled = true, onBack = {
            // Do Nothing
            // Block user from using System's Back Action
        })
    }

    LaunchedEffect(key1 = selectedMembers, block = {
        if (selectedMembers.size == CONDITIONAL_LIST_SIZE) {
            listState.animateScrollToItem(INITIAL_SCROLL_INDEX)
        }
    })

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(communityId)
    })

    val showButtonLoader by viewModel.showButtonLoader.collectAsState()

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = stringResource(R.string.add_community_member_title_text),
                        navigationIconOnClick = { viewModel.managePopBack() },
                        enableNavigationIcon = !showSkipButton,
                        actions = {
                            if (showSkipButton) {
                                TextButton(
                                    onClick = { viewModel.onSkipButtonClicked() },
                                    modifier = Modifier.motionClickEvent { }
                                ) {
                                    Text(
                                        text = stringResource(R.string.add_community_member_skip_action_text),
                                        style = AppTheme.typography.topBarActionTextStyle,
                                        color = colors.textSecondary
                                    )
                                }
                            }
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        },
        floatingActionButton = {
            Box(
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .fillMaxWidth()
                    .height(48.dp),
                contentAlignment = Alignment.Center
            ) {
                PrimaryButton(
                    onClick = { viewModel.sendCommunityJoinRequest(communityId) },
                    text = stringResource(R.string.add_community_member_add_btn_text),
                    modifier = Modifier
                        .widthIn(max = 600.dp)
                        .fillMaxWidth(),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = if (showInviteView && showButtonLoader) colors.primary.copy(0.6f) else colors.primary,
                        contentColor = Color.White,
                        disabledBackgroundColor = DisabledButton
                    ),
                    enabled = selectedMembers.isNotEmpty(),
                    isProcessing = showInviteView && showButtonLoader
                )
            }
        },
        floatingActionButtonPosition = FabPosition.Center
    ) {

        state.let { state ->
            when (state) {
                is AddMembersState.SUCCESS -> {
                    viewModel.manageNavigation(communityId, showSkipButton)
                }
                is AddMembersState.FAILURE -> {
                    val message = state.message
                    val context = LocalContext.current
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                    }
                }
                else -> {}
            }
        }

        LazyColumn(
            modifier = Modifier
                .fillMaxSize()
                .background(colors.background)
                .padding(it),
            state = listState
        ) {

            SelectedMembersView(viewModel = viewModel, selectedMembers, this)

            SearchBarView(viewModel, searchValue, selectedMembers, focusManager, this)

            if (showLoader) {
                item {
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(colors.background),
                        horizontalArrangement = Arrangement.Center
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
            } else {
                itemsIndexed(membersList) { index, item ->
                    val isMemberInCommunity = remember {
                        communityMembers.any { it.user_id == item.id }
                    }
                    val invitedOrMember = remember {
                        !item.invitedToCommunity && !isMemberInCommunity
                    }
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(
                                color =
                                if (invitedOrMember) Color.Transparent else if (isDarkMode) backgroundColor.copy(
                                    0.1f
                                ) else backgroundColor
                            )
                            .motionClickEvent {
                                if (invitedOrMember) {
                                    viewModel.onUserSelected(item)
                                }
                            }
                            .padding(horizontal = 20.dp, vertical = 8.dp),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        ProfileImageView(
                            data = item.profileImageUrl,
                            modifier = Modifier
                                .size(65.dp)
                                .border(
                                    1.dp,
                                    if (item.profileImageUrl.isNullOrEmpty() && isDarkMode) lightUnselectedTab else profileBorder,
                                    CircleShape
                                )
                                .border(1.dp, profileBorder, CircleShape),
                            char = item.profileImageChar()
                        )

                        Text(
                            text = item.fullName,
                            style = AppTheme.typography.bodyTextStyle3,
                            color = if (invitedOrMember) colors.textPrimary else if (isDarkMode) darkUnselectedTab else lightUnselectedTab,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(start = 16.dp)
                                .weight(1f),
                            textAlign = TextAlign.Start
                        )
                        if (item.invitedToCommunity) {
                            CommunityMembersTag(
                                stringResource(R.string.add_community_member_invited_text),
                                R.drawable.ic_check_icon
                            )
                        } else if (isMemberInCommunity) {
                            CommunityMembersTag(
                                stringResource(R.string.add_community_member_already_member_tag),
                                null
                            )
                        }
                    }

                    if (index == membersList.lastIndex) {
                        Spacer(modifier = Modifier.height(80.dp))
                    }
                }

                InvitationView(viewModel, showInviteView, searchValue, this)
            }
        }
    }
}

@Composable
fun CommunityMembersTag(tagText: String, icon: Int?) {
    Box(
        modifier = Modifier
            .wrapContentSize()
            .clip(RoundedCornerShape(20.dp))
            .border(
                1.dp, color = borderColor,
                RoundedCornerShape(20.dp)
            )
    ) {
        Row(
            modifier = Modifier
                .wrapContentWidth()
                .padding(horizontal = 16.dp, vertical = 8.dp),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            if (icon != null) {
                Icon(
                    painter = painterResource(id = icon),
                    contentDescription = null,
                    tint = borderColor,
                    modifier = Modifier.padding(end = 4.dp)
                )
            }
            Text(
                text = tagText,
                style = AppTheme.typography.buttonStyle,
                color = borderColor,
            )
        }
    }
}

@Preview
@Composable
fun PreviewAddMemberView() {
    AddCommunityMembersView("0", false)
}

private val profileBorder = Color(0x1A1C191F)
private val borderColor = Color(0xCC47A96E)
private val backgroundColor = Color(0xffFAF7F6)
private val lightUnselectedTab = Color(0x66000000)
private val darkUnselectedTab = Color(0x66FFFFFF)
