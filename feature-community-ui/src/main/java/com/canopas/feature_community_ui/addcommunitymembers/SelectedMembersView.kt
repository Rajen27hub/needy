package com.canopas.feature_community_ui.addcommunitymembers

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.canopas.base.data.model.UserAccount
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.feature_community_ui.R

@Suppress("FunctionName")
fun SelectedMembersView(
    viewModel: AddCommunityMembersViewModel,
    selectedMembers: MutableList<UserAccount>,
    lazyListScope: LazyListScope
) {
    lazyListScope.item {
        AnimatedVisibility(selectedMembers.isNotEmpty()) {
            LazyRow(
                modifier = Modifier
                    .fillMaxWidth(),
                contentPadding = PaddingValues(vertical = 20.dp, horizontal = 20.dp)
            ) {
                itemsIndexed(selectedMembers) { _, item ->
                    Column(
                        modifier = Modifier
                            .fillParentMaxWidth(0.2f),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Box(
                            modifier = Modifier
                                .size(41.dp)
                                .motionClickEvent {
                                    viewModel.removeUser(item)
                                }
                                .background(ComposeTheme.colors.background, CircleShape)
                        ) {
                            ProfileImageView(
                                data = item.profileImageUrl,
                                modifier = Modifier
                                    .fillMaxSize()
                                    .border(
                                        1.dp,
                                        if (item.profileImageUrl.isNullOrEmpty() && ComposeTheme.isDarkMode) ComposeTheme.colors.textSecondary else profileBorder,
                                        CircleShape
                                    )
                                    .border(1.dp, profileBorder, CircleShape),
                                char = item.profileImageChar()
                            )

                            Image(
                                painter = painterResource(id = R.drawable.ic_remove_member),
                                contentDescription = null,
                                modifier = Modifier
                                    .size(12.dp)
                                    .align(Alignment.BottomEnd)
                            )
                        }
                        Text(
                            text = item.fullName,
                            style = AppTheme.typography.addMemberTextStyle,
                            color = ComposeTheme.colors.textSecondary,
                            modifier = Modifier.padding(top = 4.dp),
                            textAlign = TextAlign.Center,
                        )
                    }
                }
            }
        }
    }
}

private val profileBorder = Color(0x1A1C191F)
