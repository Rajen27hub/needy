package com.canopas.feature_community_ui.communitydetail

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AddCommunityDetailLineView
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.combinedMotionClickEvent
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.rememberForeverLazyListState
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.feature_community_data.model.Community
import com.canopas.feature_community_ui.R

const val ADMIN_ROLE = 1
const val NORMAL_ROLE = 0

@Composable
fun CommunityDetailView(communityId: String, isDataUpdated: Boolean) {
    val viewModel = hiltViewModel<CommunityDetailViewModel>()
    val detailState by viewModel.state.collectAsState()

    LaunchedEffect(key1 = communityId, key2 = isDataUpdated, block = {
        viewModel.onStart(communityId, isDataUpdated)
    })

    val showShareActivityView by viewModel.showShareActivityView.collectAsState()
    if (showShareActivityView) {
        viewModel.navigateToShareActivitiesPrompt(communityId)
    }

    Column(
        modifier = Modifier
            .fillMaxSize().background(colors.background)
    ) {
        detailState.let { state ->
            when (state) {
                DetailState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier.fillMaxSize().background(colors.background)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }

                is DetailState.SUCCESS -> {
                    val community = state.community
                    DetailSuccessView(viewModel, community)
                }

                is DetailState.FAILURE -> {
                    if (state.isNetworkError) {
                        NetworkErrorView {
                            viewModel.onStart(communityId, isDataUpdated)
                        }
                    } else {
                        ServerErrorView {
                            viewModel.onStart(communityId, isDataUpdated)
                        }
                    }
                }

                else -> {}
            }
        }
    }
}

@Composable
fun DetailSuccessView(
    viewModel: CommunityDetailViewModel,
    community: Community,
) {
    val communityDetailState = rememberForeverLazyListState(community.name)
    val showPoints by viewModel.showPointsField.collectAsState()

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = community.name.trim(),
                        navigationIconOnClick = {
                            viewModel.managePopBack()
                        },
                        actions = {
                            DropDownOptionsMenu(viewModel, community.id)
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        LazyColumn(
            state = communityDetailState,
            modifier = Modifier
                .fillMaxSize()
                .background(colors.background)
                .padding(it)
        ) {
            itemsIndexed(community.users) { index, user ->
                val userName = when {
                    user.hasFirstname() -> user.firstName
                    else -> stringResource(R.string.subscription_status_anonymous_user_text)
                }
                Row(
                    horizontalArrangement = Arrangement.SpaceBetween,
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier
                        .combinedMotionClickEvent(
                            onClick = {
                                viewModel.navigateToSharedSubscriptions(
                                    community.id.toString(),
                                    user.user_id.toString(),
                                    userName!!
                                )
                            }, onLongClick = {
                            viewModel.navigateToMemberInfo(community.id, user.user_id)
                        }
                        )
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp, vertical = 16.dp),

                ) {

                    ProfileImageView(
                        data = user.profileImageUrl,
                        modifier = Modifier
                            .size(56.dp)
                            .border(1.dp, if (user.profileImageUrl.isNullOrEmpty() && isDarkMode) colors.textSecondary else lightProfileBorder, CircleShape),
                        char = user.profileImageChar()
                    )

                    Column(Modifier.weight(1f)) {
                        (if (user.hasFirstname() && user.fullName.trim() != "") user.fullName.trim() else user.phone)?.let { userData ->
                            Text(
                                text = userData,
                                style = AppTheme.typography.bodyTextStyle2,
                                color = colors.textPrimary,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(start = 16.dp),
                                textAlign = TextAlign.Start
                            )
                        }

                        if (showPoints) {
                            Text(
                                text = stringResource(id = R.string.community_detail_points_text),
                                style = AppTheme.typography.bodyTextStyle3,
                                color = colors.textSecondary,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(start = 16.dp),
                                textAlign = TextAlign.Start
                            )
                        }
                    }

                    if (user.role == ADMIN_ROLE) {
                        Text(
                            text = stringResource(id = R.string.community_detail_admin_text),
                            style = AppTheme.typography.detailViewAdminStyle,
                            color = colors.textSecondary
                        )
                    }
                }
                if (index != community.users.lastIndex) {
                    AddCommunityDetailLineView(if (isDarkMode) darkDividerColor else lightDividerColor)
                }
            }
        }
    }
}

@Composable
fun DropDownOptionsMenu(viewModel: CommunityDetailViewModel, communityId: Int) {
    IconButton(onClick = {
        viewModel.navigateToCommunityInfoView(communityId)
    }) {
        Icon(
            imageVector = Icons.Outlined.MoreVert,
            contentDescription = "More Menu",
            tint = colors.textPrimary
        )
    }
}
private val darkDividerColor = Color(0xFF313131)
private val lightDividerColor = Color(0x261C191F)
private val lightProfileBorder = Color(0x331C191F)
