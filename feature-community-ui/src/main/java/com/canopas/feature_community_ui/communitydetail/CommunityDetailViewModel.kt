package com.canopas.feature_community_ui.communitydetail

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.di.CommunityCache
import com.canopas.feature_community_data.model.Community
import com.canopas.feature_community_data.model.UpdateSharePrompt
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CommunityDetailViewModel @Inject constructor(
    private val service: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val navManager: CommunityNavManager,
    private val communityCache: CommunityCache,
    private val authManager: AuthManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val showPointsField = MutableStateFlow(false)
    private val adminUsers = MutableStateFlow(mutableListOf<Int>())
    val userId = MutableStateFlow(0)
    val showShareActivityView = MutableStateFlow(false)

    val state = MutableStateFlow<DetailState>(DetailState.START)

    private fun displayCommunityDetailList(communityId: String, isDataUpdated: Boolean) = viewModelScope.launch {
        if (state.value !is DetailState.SUCCESS || isDataUpdated) {
            state.value = DetailState.LOADING
        }
        withContext(appDispatcher.IO) {
            service.getCommunity(communityId.toInt()).onSuccess { community ->
                state.tryEmit(DetailState.SUCCESS(community))
                communityCache.put(community)
                val list = ArrayList(adminUsers.value)
                authManager.currentUser?.id?.let { id ->
                    userId.tryEmit(id)
                }
                community.users.asIterable().forEach {
                    if (it.role == ADMIN_ROLE) {
                        list.add(it.user_id)
                    }
                    if (it.user_id == userId.value && !it.activity_share_prompted) {
                        showShareActivityView.tryEmit(true)
                    }
                }
                adminUsers.tryEmit(list)
            }.onFailure { e ->
                Timber.e(e)
                state.value = DetailState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    fun onStart(communityId: String, isDataUpdated: Boolean) {
        appAnalytics.logEvent("view_community_detail")
        displayCommunityDetailList(communityId, isDataUpdated)
    }

    fun managePopBack() {
        navManager.popBackStack()
    }

    fun navigateToSharedSubscriptions(communityId: String, userId: String, userName: String) {
        navManager.navigateToSharedSubscriptionsList(communityId, userId, userName)
    }

    fun navigateToCommunityInfoView(communityId: Int) {
        appAnalytics.logEvent("tap_community_detail_more_button")
        navManager.navigateToCommunityInfoScreen(communityId.toString(), userId.value.toString())
    }

    fun navigateToMemberInfo(communityId: Int, selectedUserId: Int) {
        if (userId.value != selectedUserId && adminUsers.value.contains(userId.value)) {
            appAnalytics.logEvent("long_tap_community_detail_user")
            navManager.navigateToCommunityMemberInfo(
                communityId.toString(),
                selectedUserId.toString()
            )
        }
    }

    fun navigateToShareActivitiesPrompt(communityId: String) = viewModelScope.launch {
        withContext(appDispatcher.MAIN) {
            delay(1000)
            service.updateSharePromptedFlag(communityId, userId.value.toString(), UpdateSharePrompt(true)).onSuccess {
                navManager.navigateToShareSubscriptions(
                    communityId.toInt(),
                    userId.value.toString()
                )
            }.onFailure { e ->
                e.localizedMessage?.let { Timber.e(it) }
            }
            showShareActivityView.tryEmit(false)
        }
    }
}

sealed class DetailState {
    object START : DetailState()
    object LOADING : DetailState()
    data class SUCCESS(
        val community: Community
    ) : DetailState()

    data class FAILURE(val message: String, val isNetworkError: Boolean) : DetailState()
}
