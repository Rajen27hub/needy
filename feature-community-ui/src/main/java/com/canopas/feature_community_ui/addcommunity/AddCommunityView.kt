package com.canopas.feature_community_ui.addcommunity

import androidx.compose.foundation.background
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.utils.Utils.Companion.decodeFromBase64
import com.canopas.base.ui.AddCommunityTextFieldLineView
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomTextField
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.onFocusSelectAll
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.feature_community_ui.R

@Composable
fun AddCommunityView(communityId: String = "", communityName: String = "") {

    val viewModel = hiltViewModel<AddCommunityViewModel>()
    val state by viewModel.state.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = Unit, block = {
        viewModel.setCommunityName(
            decodeFromBase64(communityName).ifEmpty { context.getString(R.string.add_community_default_community_name_title_text) },
            enableButton = decodeFromBase64(communityName).isEmpty()
        )
    })

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = "",
                        navigationIconOnClick = { viewModel.managePopBack() }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp,
            )
        }
    ) {
        state.let { state ->
            when (state) {
                is AddCommunityState.START -> {
                    AddCommunityScreen(viewModel, communityId)
                }

                is AddCommunityState.LOADING, is AddCommunityState.IDLE -> {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .background(colors.background)
                            .padding(it),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }

                is AddCommunityState.FAILURE -> {
                    val message = state.message
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                    }
                    AddCommunityScreen(viewModel = viewModel, communityId)
                }

                is AddCommunityState.SUCCESS -> {
                    val communityID = state.community.id
                    viewModel.navigateToAddMembers(communityID)
                }

                is AddCommunityState.UPDATE -> {
                    LaunchedEffect(key1 = Unit) {
                        viewModel.managePopBack()
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AddCommunityScreen(viewModel: AddCommunityViewModel, communityId: String) {
    val communityName by viewModel.communityName.collectAsState()

    val interactionSource = remember { MutableInteractionSource() }
    val isFocused by interactionSource.collectIsFocusedAsState()

    val allowUpdateCommunityName by viewModel.allowUpdateCommunity.collectAsState()

    val focusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current

    DisposableEffect(key1 = Unit, effect = {
        onDispose {
            keyboardController?.hide()
        }
    })

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.Start
    ) {
        val focusRequester = remember {
            FocusRequester()
        }
        LaunchedEffect(key1 = Unit, block = {
            focusRequester.requestFocus()
        })

        val titleText =
            if (communityId.isNotEmpty()) stringResource(R.string.add_community_update_community_name_title_text) else stringResource(
                R.string.add_community_whats_your_community_name_title_text
            )
        Text(
            text = titleText,
            style = AppTheme.typography.subTitleTextStyle1,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth(),
            color = colors.textSecondary
        )

        val textFieldValueState = remember {
            mutableStateOf(
                TextFieldValue(
                    communityName,
                    selection = TextRange(communityName.length)
                )
            )
        }

        CustomTextField(
            value = textFieldValueState.value,
            onValueChange = {
                textFieldValueState.value = it
                viewModel.onCommunityNameChange(it.text)
            },
            interactionSource = interactionSource,
            modifier = Modifier
                .motionClickEvent { }
                .focusRequester(focusRequester)
                .onFocusSelectAll(textFieldValueState)
                .fillMaxWidth()
                .padding(top = 40.dp, start = 20.dp, end = 20.dp, bottom = 8.dp),
            textStyle = AppTheme.typography.h2TextStyle.copy(
                color = colors.textPrimary,
                textAlign = TextAlign.Center,
                lineHeight = 29.sp,
                letterSpacing = -(0.96.sp)
            ),
            keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Done
            ),
            cursorBrush = SolidColor(colors.primary),
        )

        AddCommunityTextFieldLineView(
            if (isFocused) colors.primary.copy(0.5f) else if (isDarkMode) darkDividerColor else lightDividerColor
        )

        val buttonText =
            if (communityId.isNotEmpty()) stringResource(R.string.add_community_update_community_btn_text) else
                stringResource(R.string.add_community_create_community_btn_text)

        PrimaryButton(
            onClick = {
                viewModel.createAndUpdateCommunityClick(communityId)
            },
            modifier = Modifier
                .align(alignment = Alignment.CenterHorizontally)
                .motionClickEvent { }
                .wrapContentWidth()
                .padding(top = 35.dp),
            textModifier = Modifier.padding(horizontal = 36.dp),
            text = buttonText,
            enabled = allowUpdateCommunityName,
            colors = ButtonDefaults.buttonColors(
                backgroundColor = colors.primary,
                contentColor = White,
                disabledBackgroundColor = colors.primary.copy(0.6f)
            )
        )
    }
}

@Preview
@Composable
fun PreviewAddCommunityView() {
    AddCommunityView("", "")
}

private val darkDividerColor = Color(0xFf313131)
private val lightDividerColor = Color(0x1F1C191F)
