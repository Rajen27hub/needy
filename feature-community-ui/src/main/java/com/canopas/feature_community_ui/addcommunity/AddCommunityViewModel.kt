package com.canopas.feature_community_ui.addcommunity

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.model.AddCommunity
import com.canopas.feature_community_data.model.Community
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

const val MINIMUM_NAME_LENGTH = 3

@HiltViewModel
class AddCommunityViewModel @Inject constructor(
    private val navManager: CommunityNavManager,
    private val appDispatcher: AppDispatcher,
    private val communityService: CommunityService,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val communityName = MutableStateFlow("")
    val state = MutableStateFlow<AddCommunityState>(AddCommunityState.IDLE)
    val allowUpdateCommunity = MutableStateFlow(false)
    val previousCommunityName = MutableStateFlow("")

    private fun createCommunity() = viewModelScope.launch {
        state.tryEmit(AddCommunityState.LOADING)
        withContext(appDispatcher.IO) {
            communityName.tryEmit(communityName.value.trim())
            val communityDetails = AddCommunity(
                name = communityName.value,
                description = "",
                image_url = ""
            )
            communityService.createCommunity(communityDetails).onSuccess {
                state.tryEmit(AddCommunityState.SUCCESS(it))
            }.onFailure {
                state.tryEmit(AddCommunityState.FAILURE(it.toUserError(resources)))
                Timber.e(it.localizedMessage)
            }
        }
    }

    fun managePopBack() {
        navManager.popBackStack()
    }

    fun onCommunityNameChange(name: String) {
        communityName.value = name
        when {
            previousCommunityName.value == communityName.value && communityName.value.isNotEmpty() -> {
                allowUpdateCommunity.value = false
            }
            communityName.value.trim().length < MINIMUM_NAME_LENGTH -> {
                allowUpdateCommunity.value = false
            }
            else -> {
                allowUpdateCommunity.value = true
            }
        }
    }

    fun navigateToAddMembers(communityId: Int) {
        navManager.navigateToAddCommunityMembers(communityId, true)
    }

    private fun updateCommunityName(communityId: Int) {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                communityName.tryEmit(communityName.value.trim())
                val communityDetails = AddCommunity(
                    name = communityName.value,
                    description = "",
                    image_url = ""
                )
                communityService.updateCommunity(communityId, communityDetails).onSuccess {
                    state.tryEmit(AddCommunityState.UPDATE(it))
                }.onFailure {
                    state.tryEmit(AddCommunityState.FAILURE(it.toUserError(resources)))
                    Timber.e(it.localizedMessage)
                }
            }
        }
    }

    fun createAndUpdateCommunityClick(communityId: String) {
        appAnalytics.logEvent("tap_create_community")
        if (communityId.isNotEmpty()) {
            updateCommunityName(communityId.toInt())
        } else {
            createCommunity()
        }
    }

    fun setCommunityName(name: String, enableButton: Boolean) {
        appAnalytics.logEvent("view_add_community")
        previousCommunityName.value = name
        communityName.value = name
        state.tryEmit(AddCommunityState.START)
        allowUpdateCommunity.tryEmit(enableButton)
    }
}

sealed class AddCommunityState {
    object IDLE : AddCommunityState()
    object START : AddCommunityState()
    object LOADING : AddCommunityState()
    data class SUCCESS(val community: Community) : AddCommunityState()
    data class UPDATE(val community: Community) : AddCommunityState()
    data class FAILURE(val message: String) : AddCommunityState()
}
