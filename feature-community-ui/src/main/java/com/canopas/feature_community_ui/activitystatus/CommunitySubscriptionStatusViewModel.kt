package com.canopas.feature_community_ui.activitystatus

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.StreaksData
import com.canopas.base.data.model.getFormattedDayDate
import com.canopas.base.ui.Utils.Companion.allTimeFormat
import com.canopas.base.ui.model.ProgressTabType
import com.canopas.data.model.Subscription
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.subscriptionlist.DEFAULT_USER_ID
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class CommunitySubscriptionStatusViewModel @Inject constructor(
    private val service: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val navManager: CommunityNavManager,
    private val authManager: AuthManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<SubscriptionStatusState>(SubscriptionStatusState.LOADING)
    val streaksDataList = MutableStateFlow(mutableListOf<StreaksData>())
    val maxStreak = MutableStateFlow(0)
    val currentUserId = MutableStateFlow(DEFAULT_USER_ID)
    val sampleStreaksDate = MutableStateFlow("")
    val progressSelectedTab = MutableStateFlow(ProgressTabType.PROGRESS_7D)
    val progressState = MutableStateFlow<ProgressHistoryState>(ProgressHistoryState.LOADING)
    val allTimeProgressText = MutableStateFlow("")
    val previousReading = MutableStateFlow(0f)
    val currentReading = MutableStateFlow(0f)

    fun getSubscriptionDetails(communityId: String, userId: String, subscriptionId: String) =
        viewModelScope.launch {
            state.tryEmit(SubscriptionStatusState.LOADING)
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("view_community_subscription_detail")
                setStreaksSampleDate()
                currentUserId.value = authManager.currentUser!!.id
                service.retrieveUserSubscriptionById(communityId, userId, subscriptionId)
                    .onSuccess {
                        state.tryEmit(SubscriptionStatusState.SUCCESS(it))
                        setSubscriptionStatusData(it)
                    }.onFailure { e ->
                        Timber.e(e.localizedMessage)
                        state.tryEmit(SubscriptionStatusState.FAILURE(e.toUserError(resources), e is APIError.NetworkError))
                    }
            }
        }

    private fun setStreaksSampleDate() {
        val timeMillis = System.currentTimeMillis()
        val currentTimeInMillis: Long = TimeUnit.MILLISECONDS.toSeconds(timeMillis)
        val streaksEndDateTimeInMillis: Long = TimeUnit.MILLISECONDS.toSeconds(timeMillis) + (86400L * 4)
        sampleStreaksDate.tryEmit(getFormattedDayDate(currentTimeInMillis).first + " to " + getFormattedDayDate(streaksEndDateTimeInMillis).first)
    }

    private fun setSubscriptionStatusData(
        subscription: Subscription
    ) {
        if (subscription.streaks.isNotEmpty()) {
            streaksDataList.value = subscription.streaks.toMutableList()
            maxStreak.value = streaksDataList.value[0].count
        }
    }

    fun managePopBack() {
        navManager.popBackStack()
    }

    fun setAllTabProgressText(habitDetail: Subscription) {
        val currentMonth = allTimeFormat.format(System.currentTimeMillis())
        val activityStartMonth = allTimeFormat.format(habitDetail.start_date * 1000L)
        val data = if (activityStartMonth == currentMonth) {
            activityStartMonth
        } else {
            "$activityStartMonth-$currentMonth"
        }
        allTimeProgressText.tryEmit(data)
    }

    fun handleProgressTabSelection(
        selectedTab: ProgressTabType
    ) {
        progressSelectedTab.tryEmit(selectedTab)
        when (progressSelectedTab.value) {
            ProgressTabType.PROGRESS_7D -> {
                appAnalytics.logEvent("tap_community_subs_detail_7_days")
            }
            ProgressTabType.PROGRESS_M -> {
                appAnalytics.logEvent("tap_community_subs_detail_monthly")
            }
            ProgressTabType.PROGRESS_6M -> {
                appAnalytics.logEvent("tap_community_subs_detail_6_months")
            }
            ProgressTabType.PROGRESS_Y -> {
                appAnalytics.logEvent("tap_community_subs_detail_yearly")
            }
            ProgressTabType.PROGRESS_ALL -> {
                appAnalytics.logEvent("tap_community_subs_detail_all")
            }
        }
    }

    fun getHighlightProgress(subscription: Subscription) {
        subscription.activity.highlights?.forEach {
            it.highlight_metrics.forEach { item ->
                currentReading.tryEmit(item.currentReading)
                previousReading.tryEmit(item.previousReading)
            }
        }
    }
}

sealed class SubscriptionStatusState {
    object LOADING : SubscriptionStatusState()
    data class SUCCESS(val subscription: Subscription) : SubscriptionStatusState()
    data class FAILURE(val message: String, val isNetworkError: Boolean) : SubscriptionStatusState()
}

sealed class ProgressHistoryState {
    object LOADING : ProgressHistoryState()
    object PROCESSED : ProgressHistoryState()
    data class FAILURE(val message: String) : ProgressHistoryState()
}
