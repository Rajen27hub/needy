package com.canopas.feature_community_ui.activitystatus

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawWithCache
import androidx.compose.ui.graphics.BlendMode
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.CircularGradientProgressView
import com.canopas.base.ui.activity.ProgressHeaderView
import com.canopas.base.ui.activity.SubscriptionProgressTabView
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.model.ProgressTabType
import com.canopas.base.ui.rememberForeverLazyListState
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.base.ui.themes.StatusTheme
import com.canopas.base.ui.themes.StatusTheme.SubscriptionColors
import com.canopas.data.model.Subscription
import com.canopas.feature_community_ui.R
import com.canopas.feature_community_ui.activitystatus.monthlyprogress.MonthlyProgressView
import com.canopas.feature_community_ui.activitystatus.recentprogress.RecentProgressView
import com.canopas.feature_community_ui.activitystatus.sixmonthlyprogress.SixMonthlyProgressView
import com.canopas.feature_community_ui.activitystatus.yearlyprogess.YearlyProgressView

@Composable
fun CommunitySubscriptionStatusView(
    communityId: String,
    userId: String,
    subscriptionId: String,
    userName: String
) {

    val viewModel = hiltViewModel<CommunitySubscriptionStatusViewModel>()
    val state by viewModel.state.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.getSubscriptionDetails(communityId, userId, subscriptionId)
    })

    state.let { statusState ->
        when (statusState) {
            is SubscriptionStatusState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(colors.background)
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }

            is SubscriptionStatusState.SUCCESS -> {
                val subscription = statusState.subscription
                StatusTheme(darkTheme = isDarkMode) {
                    SubscriptionStatusSuccessState(viewModel, subscription, userId, userName)
                }
            }

            is SubscriptionStatusState.FAILURE -> {
                if (statusState.isNetworkError) {
                    NetworkErrorView {
                        viewModel.getSubscriptionDetails(communityId, userId, subscriptionId)
                    }
                } else {
                    ServerErrorView {
                        viewModel.getSubscriptionDetails(communityId, userId, subscriptionId)
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SubscriptionStatusSuccessState(
    viewModel: CommunitySubscriptionStatusViewModel,
    subscription: Subscription,
    userId: String,
    userName: String
) {
    val context = LocalContext.current
    val topBarColor by remember { mutableStateOf(GreenSubscriptionCardBg) }
    val streaksSampleDate by viewModel.sampleStreaksDate.collectAsState()
    val currentSelectedTab by viewModel.progressSelectedTab.collectAsState()
    val currentUserId by viewModel.currentUserId.collectAsState()

    val analyticText by remember {
        mutableStateOf(
            if (currentUserId.toString() == userId) context.getString(R.string.subscription_status_analytic_part_you_text) else context.getString(
                R.string.subscription_status_analytic_part_user_text
            )
        )
    }

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = subscription.activity.name.trim(),
                        navigationIconOnClick = {
                            viewModel.managePopBack()
                        }
                    )
                },
                backgroundColor = if (subscription.progress.recent.isEmpty() && !isDarkMode) topBarColor else colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        LazyColumn(
            state = rememberForeverLazyListState(key = subscription.id.toString()),
            modifier = Modifier
                .fillMaxSize()
                .background(colors.background)
                .padding(it),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            item {
                if (subscription.progress.recent.isNotEmpty()) {
                    SubscriptionProgressTabView({ tabType ->
                        viewModel.handleProgressTabSelection(tabType)
                    }, currentSelectedTab)
                } else {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(SubscriptionColors.statusBarBg)
                            .wrapContentHeight()
                    ) {
                        Text(
                            text = stringResource(id = R.string.subscription_status_chart_title_just_started_text),
                            style = AppTheme.typography.statusScreenHeadingTextStyle,
                            lineHeight = 36.sp,
                            color = SubscriptionColors.headerText,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(bottom = 12.dp, top = 40.dp),
                            textAlign = TextAlign.Center
                        )
                    }
                }
            }

            item {

                if (subscription.progress.recent.isEmpty()) {
                    JustStartedActivityView()
                } else {
                    when (currentSelectedTab) {
                        ProgressTabType.PROGRESS_7D -> {
                            RecentProgressView(subscription)
                        }
                        ProgressTabType.PROGRESS_M -> {
                            MonthlyProgressView(subscription, userId)
                        }
                        ProgressTabType.PROGRESS_6M -> {
                            SixMonthlyProgressView(subscription, userId)
                        }
                        ProgressTabType.PROGRESS_Y -> {
                            YearlyProgressView(subscription, userId)
                        }
                        else -> {
                            AllProgressView(subscription, viewModel)
                        }
                    }
                }
            }

            item {
                if (currentSelectedTab != ProgressTabType.PROGRESS_ALL && subscription.activity.highlights != null) {
                    CommunityHighlightsView(
                        viewModel,
                        subscription,
                        currentUserId,
                        userId,
                        userName
                    )
                }
            }

            item {
                Column(
                    Modifier
                        .widthIn(max = 600.dp)
                        .fillMaxWidth()
                        .animateItemPlacement(tween(durationMillis = 600, easing = LinearEasing)),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    CommunityYourStreakView(viewModel, analyticText, streaksSampleDate)
                }
            }
        }
    }
}

@Composable
fun AllProgressView(
    subscription: Subscription,
    viewModel: CommunitySubscriptionStatusViewModel
) {
    LaunchedEffect(key1 = Unit, block = {
        viewModel.setAllTabProgressText(subscription)
    })
    val allTimeProgressText by viewModel.allTimeProgressText.collectAsState()
    val totalDays by remember {
        mutableStateOf(subscription.progress.total.completed_days + subscription.progress.total.missed_days)
    }
    val completedDays by remember {
        mutableStateOf(subscription.progress.total.completed_days)
    }

    BoxWithConstraints {
        val possibleSize = maxWidth.times(0.25f).plus(100.dp)
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(
                    top = 28.dp,
                    start = 20.dp,
                    end = 20.dp
                ),
            verticalArrangement = Arrangement.Top
        ) {
            ProgressHeaderView(
                totalDays = totalDays,
                completedDays = completedDays,
                isLoadingState = false,
                currentMonthName = allTimeProgressText,
                showProgressView = false
            )

            CircularGradientProgressView(
                boxModifier = Modifier
                    .fillMaxWidth()
                    .size(possibleSize)
                    .padding(end = 4.dp),
                strokeWidth = 16.dp,
                showText = true,
                strokeBackgroundWidth = 2.dp,
                progress = ((completedDays * 100) / totalDays).toFloat()
            )
        }
    }
}

fun Modifier.textBrush(brush: Brush) = this
    .graphicsLayer(alpha = 0.99f)
    .drawWithCache {
        onDrawWithContent {
            drawContent()
            drawRect(brush, blendMode = BlendMode.SrcAtop)
        }
    }

private val GreenSubscriptionCardBg = Color(0xFFE6F0DE)
