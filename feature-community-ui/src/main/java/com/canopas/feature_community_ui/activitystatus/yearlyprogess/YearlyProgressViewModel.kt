package com.canopas.feature_community_ui.activitystatus.yearlyprogess

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.ui.Utils
import com.canopas.data.model.ProgressHistory
import com.canopas.data.model.Subscription
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.activitystatus.ProgressHistoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.time.LocalDate
import java.util.Calendar
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

@HiltViewModel
class YearlyProgressViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: CommunityService,
    private val resources: Resources
) : ViewModel() {

    val progressState = MutableStateFlow<ProgressHistoryState>(ProgressHistoryState.LOADING)
    val yearlyProgressText = MutableStateFlow("")
    val subscriptionYears = MutableStateFlow(mutableListOf<Int>())
    val yearlyProgressList = MutableStateFlow(listOf<ProgressHistory>())
    val barChartData = MutableStateFlow(
        mutableListOf(
            Pair(0f, "J"),
            Pair(0f, "F"),
            Pair(0f, "M"),
            Pair(0f, "A"),
            Pair(0f, "M"),
            Pair(0f, "J"),
            Pair(0f, "J"),
            Pair(0f, "A"),
            Pair(0f, "S"),
            Pair(0f, "O"),
            Pair(0f, "N"),
            Pair(0f, "D")
        )
    )

    fun onStart(subscription: Subscription, userId: String) {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = subscription.start_date.times(1000L)
        val startYear = calendar.get(Calendar.YEAR)
        calendar.timeInMillis = System.currentTimeMillis()
        val endYear = calendar.get(Calendar.YEAR)
        subscriptionYears.tryEmit((startYear..endYear).toMutableList())
        onStateYearlyChanged(subscription = subscription, userId = userId.toInt())
    }

    fun onStateYearlyChanged(scrollingIndex: Int = 0, subscription: Subscription, userId: Int) {
        barChartData.value = mutableListOf(
            Pair(0f, "J"),
            Pair(0f, "F"),
            Pair(0f, "M"),
            Pair(0f, "A"),
            Pair(0f, "M"),
            Pair(0f, "J"),
            Pair(0f, "J"),
            Pair(0f, "A"),
            Pair(0f, "S"),
            Pair(0f, "O"),
            Pair(0f, "N"),
            Pair(0f, "D")
        )
        val calendar = Calendar.getInstance()
        calendar.set(
            Calendar.YEAR,
            subscriptionYears.value[subscriptionYears.value.lastIndex - scrollingIndex]
        )
        calendar.set(Calendar.MONTH, Calendar.JANUARY)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        val startTimeInMillis = calendar.timeInMillis
        calendar.set(
            Calendar.YEAR,
            subscriptionYears.value[subscriptionYears.value.lastIndex - scrollingIndex]
        )
        calendar.set(Calendar.MONTH, Calendar.DECEMBER)
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        val endTimeInMillis = calendar.timeInMillis
        val startMonth = Utils.allTimeFormat.format(max(subscription.start_date.times(1000L), startTimeInMillis))
        val endMonth = Utils.allTimeFormat.format(min(System.currentTimeMillis(), endTimeInMillis))
        val data = "$startMonth-$endMonth"
        yearlyProgressText.tryEmit(data)
        getYearlyProgressHistory(
            min(endTimeInMillis.div(1000L), System.currentTimeMillis().div(1000L)),
            startTimeInMillis.div(1000L),
            subscription.id,
            userId
        )
    }

    fun getYearlyProgressHistory(end: Long, start: Long, subscriptionId: Int, userId: Int) {
        viewModelScope.launch {
            progressState.tryEmit(ProgressHistoryState.LOADING)
            withContext(appDispatcher.IO) {
                service.getSubscriptionsProgressHistory(userId, subscriptionId, end, start)
                    .onSuccess { progressList ->
                        yearlyProgressList.value = progressList
                        val progressByMonth = progressList.groupBy { progressHistory ->
                            val localDate =
                                LocalDate.parse(Utils.format.format(progressHistory.date.times(1000L)))
                            localDate.month
                        }
                        for ((month, progressData) in progressByMonth) {
                            val completed = progressData.filter { it.completed }.size
                            barChartData.value[(month.value - 1)] =
                                Pair((completed).toFloat(), month.name[0].toString())
                        }
                        progressState.tryEmit(ProgressHistoryState.PROCESSED)
                    }.onFailure { e ->
                        Timber.e(e.toString())
                        progressState.tryEmit(ProgressHistoryState.FAILURE(e.toUserError(resources)))
                    }
            }
        }
    }
}
