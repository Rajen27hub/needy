package com.canopas.feature_community_ui.activitystatus

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.widthIn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.Utils
import com.canopas.base.ui.activity.highlights.HighlightsView
import com.canopas.data.model.Subscription
import com.canopas.feature_community_ui.R
import java.util.Calendar
import kotlin.math.roundToInt

@Composable
fun CommunityHighlightsView(
    viewModel: CommunitySubscriptionStatusViewModel,
    subscription: Subscription,
    currentUserId: Int,
    userId: String,
    userName: String
) {
    LaunchedEffect(key1 = Unit) {
        viewModel.getHighlightProgress(subscription)
    }

    val currentProgress = viewModel.currentReading.collectAsState()
    val previousProgress = viewModel.previousReading.collectAsState()
    val highlightStatus =
        if (currentProgress.value.roundToInt() == previousProgress.value.roundToInt()) {
            stringResource(R.string.highlight_same_progress_test_for_community)
        } else if (currentProgress.value.roundToInt() > previousProgress.value.roundToInt()) {
            stringResource(R.string.highlight_more_progress_test_for_community)
        } else {
            stringResource(R.string.highlight_less_progress_test_for_community)
        }
    Column(
        Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Spacer(modifier = Modifier.height(40.dp))
        val now = Calendar.getInstance()
        val currentMonth = Utils.monthFormat.format(now.time)
        now.set(Calendar.DAY_OF_MONTH, 1)
        now.set(Calendar.HOUR_OF_DAY, 0)
        now.set(Calendar.SECOND, 0)
        now.set(Calendar.MINUTE, -1)
        val previousMonth = Utils.monthFormat.format(now.time)
        val userNameText =
            if (currentUserId.toString() == userId) stringResource(id = R.string.community_subscription_status_highlights_user_text) else userName

        val descriptionText =
            if (currentProgress.value.roundToInt() == previousProgress.value.roundToInt()) {
                stringResource(
                    id = R.string.community_subscription_status_highlights_description_text_2,
                    userNameText,
                    currentMonth,
                    previousMonth
                )
            } else {
                stringResource(
                    id = R.string.community_subscription_status_highlights_description_text_1,
                    userNameText,
                    highlightStatus,
                    currentMonth,
                    previousMonth
                )
            }

        HighlightsView(
            currentMonth,
            previousMonth,
            currentProgress.value,
            previousProgress.value,
            descriptionText
        )
    }
}
