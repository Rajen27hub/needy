package com.canopas.feature_community_ui.activitystatus.sixmonthlyprogress

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.ui.Utils
import com.canopas.base.ui.model.HalfYearlyCompletion
import com.canopas.data.model.ProgressHistory
import com.canopas.data.model.Subscription
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.activitystatus.ProgressHistoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.Calendar
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

@HiltViewModel
class SixMonthlyProgressViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: CommunityService,
    private val resources: Resources
) : ViewModel() {

    val progressState = MutableStateFlow<ProgressHistoryState>(ProgressHistoryState.LOADING)
    val sixMonthlyChartData = MutableStateFlow(listOf<HalfYearlyCompletion>())
    private val previousIndex = MutableStateFlow(0)
    val sixMonthlyProgressList = MutableStateFlow(listOf<ProgressHistory>())
    val sixMonthlyProgressText = MutableStateFlow("")
    val sixMonthlySubscriptionDuration = MutableStateFlow(mutableListOf<Pair<Long, Long>>())

    init {
        sixMonthlyChartData.value = getInitialSixMonthsData().take(6)
    }

    private fun getInitialSixMonthsData(): MutableList<HalfYearlyCompletion> {
        return (0..11).map { month ->
            HalfYearlyCompletion(weeklyCompletion = List(5) { 0 }, month = month)
        }.toMutableList()
    }

    fun onStart(subscription: Subscription, userId: String) {
        val startMillis = subscription.start_date.times(1000L)
        val endMillis = System.currentTimeMillis()
        val calendar = Calendar.getInstance()
        val startYear = calendar.apply { timeInMillis = startMillis }.get(Calendar.YEAR)
        val endYear = calendar.apply { timeInMillis = endMillis }.get(Calendar.YEAR)
        if (startYear == endYear) {
            sixMonthlySubscriptionDuration.tryEmit(mutableListOf(Pair(startMillis, endMillis)))
            val month = calendar.get(Calendar.MONTH)
            if (month in 0..5) {
                sixMonthlyChartData.value = getInitialSixMonthsData().take(6)
            } else {
                sixMonthlyChartData.value = getInitialSixMonthsData().takeLast(6)
            }
        } else {
            sixMonthlySubscriptionDuration.tryEmit(getSixMonthlyDurationList(listOf(startYear, endYear), subscription).asReversed())
        }
        onStateSixMonthlyChanged(subscription = subscription, userId = userId.toInt())
    }

    fun getSixMonthlyProgressHistory(
        end: Long,
        start: Long,
        subscriptionId: Int,
        userId: Int
    ) {
        viewModelScope.launch {
            progressState.tryEmit(ProgressHistoryState.LOADING)
            withContext(appDispatcher.IO) {
                service.getSubscriptionsProgressHistory(userId, subscriptionId, end, start)
                    .onSuccess { progressList ->
                        sixMonthlyProgressList.tryEmit(progressList)
                        setSixMonthlyChartData(getWeeklyCompletion(progressList))
                        progressState.tryEmit(ProgressHistoryState.PROCESSED)
                    }.onFailure { e ->
                        Timber.e(e.toString())
                        progressState.tryEmit(ProgressHistoryState.FAILURE(e.toUserError(resources)))
                    }
            }
        }
    }

    private fun getWeeklyCompletion(progressList: List<ProgressHistory>): List<HalfYearlyCompletion> {
        val calendar = Calendar.getInstance()
        return progressList.groupBy {
            ZonedDateTime.ofInstant(Instant.ofEpochSecond(it.date), ZoneId.systemDefault()).toLocalDate().month
        }.map { (month, days) ->
            val weeklyCompletion = mutableListOf(0, 0, 0, 0, 0)
            days.groupBy {
                calendar.timeInMillis = it.date.times(1000)
                calendar.get(Calendar.WEEK_OF_MONTH)
            }.map {
                if (it.key in 1..5) {
                    weeklyCompletion[it.key - 1] = it.value.count {
                        it.completed
                    }
                }
            }
            HalfYearlyCompletion(weeklyCompletion, (month.value.minus(1)))
        }
    }

    private fun setSixMonthlyChartData(weeklyCompletion: List<HalfYearlyCompletion>) {
        if (weeklyCompletion.isNotEmpty()) {
            sixMonthlyChartData.value = if (weeklyCompletion[0].month == 0) {
                sixMonthlyChartData.value.take(6).mapIndexed { _, halfYearlyCompletion ->
                    val newCompletion =
                        weeklyCompletion.find { it.month == halfYearlyCompletion.month }
                    halfYearlyCompletion.copy(
                        weeklyCompletion = newCompletion?.weeklyCompletion
                            ?: halfYearlyCompletion.weeklyCompletion
                    )
                }
            } else {
                sixMonthlyChartData.value.takeLast(6).mapIndexed { _, halfYearlyCompletion ->
                    val newCompletion =
                        weeklyCompletion.find { it.month == halfYearlyCompletion.month }
                    halfYearlyCompletion.copy(
                        weeklyCompletion = newCompletion?.weeklyCompletion
                            ?: halfYearlyCompletion.weeklyCompletion
                    )
                }
            }
        } else {
            sixMonthlyChartData.value = sixMonthlyChartData.value.take(6)
        }
    }

    private fun getSixMonthlyDurationList(years: List<Int>, subscription: Subscription): MutableList<Pair<Long, Long>> {
        val calendar = Calendar.getInstance()
        val dateRanges = mutableListOf<Pair<Long, Long>>()
        for (year in years) {
            calendar.set(Calendar.YEAR, year)

            calendar.set(Calendar.MONTH, Calendar.JANUARY)
            calendar.set(Calendar.DAY_OF_MONTH, 1)
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MINUTE, 0)
            val startTimestamp = calendar.timeInMillis

            calendar.set(Calendar.MONTH, Calendar.JUNE)
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
            calendar.set(Calendar.HOUR_OF_DAY, 23)
            calendar.set(Calendar.MINUTE, 59)
            val endTimestamp = calendar.timeInMillis

            if (startTimestamp > subscription.start_date.times(1000)) {
                dateRanges.add(Pair(startTimestamp, endTimestamp))
            }

            calendar.set(Calendar.MONTH, Calendar.JULY)
            calendar.set(Calendar.DAY_OF_MONTH, 1)
            calendar.set(Calendar.HOUR_OF_DAY, 0)
            calendar.set(Calendar.MINUTE, 0)
            val startTimestamp2 = calendar.timeInMillis

            calendar.set(Calendar.MONTH, Calendar.DECEMBER)
            calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
            calendar.set(Calendar.HOUR_OF_DAY, 23)
            calendar.set(Calendar.MINUTE, 59)
            val endTimestamp2 = calendar.timeInMillis
            if (startTimestamp2 < subscription.start_date.times(1000)) {
                dateRanges.add(Pair(startTimestamp2, endTimestamp2))
            }
        }
        return dateRanges
    }

    fun onStateSixMonthlyChanged(scrollingIndex: Int = 0, subscription: Subscription, userId: Int) {
        if (previousIndex.value != scrollingIndex) {
            sixMonthlyChartData.value = if (sixMonthlyChartData.value[0].month == 0) getInitialSixMonthsData().takeLast(6) else getInitialSixMonthsData().take(6)
        }
        val (startTimeInMillis, endTimeInMillis) = sixMonthlySubscriptionDuration.value[scrollingIndex]
        val startMonth = Utils.allTimeFormat.format(max(subscription.start_date.times(1000L), startTimeInMillis))
        val endMonth = Utils.allTimeFormat.format(min(System.currentTimeMillis(), endTimeInMillis))
        sixMonthlyProgressText.tryEmit("$startMonth-$endMonth")
        val subscriptionEndTime = System.currentTimeMillis().div(1000)
        val endTime = min(endTimeInMillis.div(1000L), subscriptionEndTime)
        val startTime = startTimeInMillis.div(1000L)
        previousIndex.value = scrollingIndex
        getSixMonthlyProgressHistory(endTime, startTime, subscription.id, userId)
    }
}
