package com.canopas.feature_community_ui.activitystatus.recentprogress

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils
import com.canopas.base.ui.activity.ProgressHeaderView
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Subscription

@Composable
fun RecentProgressView(subscription: Subscription) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val durationDate =
            Utils.getFormattedStartAndEndDate(
                subscription.progress.recent.first().date,
                subscription.progress.recent.last().date
            )

        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(
                    top = 28.dp,
                    start = 20.dp,
                    end = 20.dp
                ),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            ProgressHeaderView(
                totalDays = subscription.progress.recent.count(),
                completedDays = subscription.progress.recent.count { it.completed },
                isLoadingState = false,
                currentMonthName = durationDate
            )
        }
    }

    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(top = 46.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        val currentWeekProgress = Utils.getCurrentWeekProgress(subscription.progress.recent)
        BoxWithConstraints(modifier = Modifier.fillMaxWidth()) {
            val outerPadding = if (maxWidth > 450.dp) 38.dp else 44.dp
            Row(
                Modifier
                    .wrapContentWidth()
                    .align(Alignment.Center),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy((minWidth / 7) - outerPadding)
            ) {
                val weekDayNumber =
                    Utils.getCurrentWeekDay(subscription.progress.recent)
                currentWeekProgress.forEachIndexed { index, recentDaysHistory ->
                    Column(
                        modifier = Modifier
                            .wrapContentSize(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = weekDayNumber[index].toString(),
                            style = AppTheme.typography.subscriptionDateTextStyle,
                            color = ComposeTheme.colors.textPrimary
                        )
                        Spacer(modifier = Modifier.height(4.dp))
                        Text(
                            text = recentDaysHistory.day,
                            style = AppTheme.typography.bodyTextStyle4,
                            color = ComposeTheme.colors.textSecondary
                        )
                        Spacer(modifier = Modifier.height(8.dp))
                        Image(
                            painter = painterResource(id = recentDaysHistory.icon),
                            contentDescription = null,
                            modifier = Modifier
                                .size(40.dp),
                            colorFilter = ColorFilter.tint(recentDaysHistory.tint)
                        )
                    }
                }
            }
        }
    }
}
