package com.canopas.feature_community_ui.memberinfoview

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.model.UserAccount
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.DividedLineInfoView
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.feature_community_ui.R
import com.canopas.feature_community_ui.communitydetail.ADMIN_ROLE

@Composable
fun CommunityMemberInfoView(communityId: String, userId: String) {
    val viewModel = hiltViewModel<CommunityMemberInfoViewModel>()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.fetchUserData(userId, communityId)
    })

    val state by viewModel.state.collectAsState()

    state.let { infoState ->
        when (infoState) {
            is MemberInfoState.LOADING -> {
                Box(
                    modifier = Modifier.fillMaxSize()
                        .background(colors.background),
                    contentAlignment = Alignment.Center
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }
            is MemberInfoState.FAILURE -> {
                val message = infoState.message
                val context = LocalContext.current
                showBanner(context, message)
            }
            is MemberInfoState.SUCCESS -> {
                val userData = infoState.userDetail
                MemberInfoSuccessView(viewModel, userData, communityId)
            }
            else -> {}
        }
    }
}

@Composable
fun MemberInfoSuccessView(
    viewModel: CommunityMemberInfoViewModel,
    userData: UserAccount,
    communityId: String
) {
    val configuration = LocalConfiguration.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
            .verticalScroll(rememberScrollState()),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.Start
    ) {

        IconButton(onClick = {
            viewModel.popBack()
        }) {
            Icon(
                painter = painterResource(id = R.drawable.ic_arrow_down),
                contentDescription = null,
                tint = colors.textPrimary,
                modifier = Modifier
                    .padding(horizontal = 20.dp, vertical = 28.dp)
                    .size(width = 18.dp, height = 24.dp)
            )
        }
        val imageHeight = (configuration.screenHeightDp * 0.18).dp

        ProfileImageView(
            data = userData.profileImageUrl,
            modifier = Modifier
                .padding(top = 64.dp)
                .size(imageHeight)
                .align(Alignment.CenterHorizontally)
                .border(2.dp, if (isDarkMode) colors.textSecondary else profileBorder, CircleShape),
            char = userData.profileImageChar()
        )

        val userName = userData.fullName.trim()
        Text(
            text = userName,
            modifier = Modifier
                .padding(top = 20.dp)
                .align(Alignment.CenterHorizontally),
            style = AppTheme.typography.h1TextStyle,
            color = colors.textPrimary,
        )

        if (userData.role == ADMIN_ROLE) {
            Text(
                text = stringResource(id = R.string.community_detail_admin_text),
                modifier = Modifier
                    .padding(top = 20.dp)
                    .align(Alignment.CenterHorizontally),
                style = AppTheme.typography.bodyTextStyle3,
                color = colors.textPrimary,
            )
        }

        Spacer(modifier = Modifier.height(120.dp))

        DividedLineInfoView()

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
                .motionClickEvent {
                    viewModel.updateUserRole(userData.role, userData.user_id, communityId)
                },
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_user_icon),
                contentDescription = null,
                tint = colors.textPrimary,
                modifier = Modifier.size(width = 18.dp, height = 13.dp)
            )

            Text(
                text = if (userData.role == ADMIN_ROLE) stringResource(R.string.member_info_make_member_text) else stringResource(
                    R.string.member_info_make_admin_text
                ),
                modifier = Modifier
                    .padding(20.dp),
                style = AppTheme.typography.bodyTextStyle2,
                color = colors.textPrimary,
            )
        }

        DividedLineInfoView()

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
                .motionClickEvent {
                    viewModel.removeUser(communityId, userData.user_id)
                },
            verticalAlignment = Alignment.CenterVertically
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_remove_from_community),
                contentDescription = null,
                tint = colors.textPrimary,
                modifier = Modifier.size(width = 20.dp, height = 18.dp)
            )

            Text(
                text = stringResource(R.string.member_info_remove_from_community_text),
                modifier = Modifier
                    .padding(20.dp),
                style = AppTheme.typography.bodyTextStyle2,
                color = colors.textPrimary,
            )
        }

        DividedLineInfoView()
    }
}

private val profileBorder = Color(0x1A1C191F)
