package com.canopas.feature_community_ui.memberinfoview

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.UserAccount
import com.canopas.feature_community_data.di.CommunityCache
import com.canopas.feature_community_data.model.RemoveUsers
import com.canopas.feature_community_data.model.UpdateCommunityMembersRole
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.communitydetail.ADMIN_ROLE
import com.canopas.feature_community_ui.communitydetail.NORMAL_ROLE
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CommunityMemberInfoViewModel @Inject constructor(
    private val service: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val navManager: CommunityNavManager,
    private val communityCache: CommunityCache,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<MemberInfoState>(MemberInfoState.LOADING)
    private val membersList = MutableStateFlow(mutableListOf<UserAccount>())
    private val memberListWithNewRole = MutableStateFlow(mutableListOf<UpdateCommunityMembersRole>())

    fun fetchUserData(userId: String, communityId: String) {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("view_community_member_info")
                val communityCache = communityCache.getCommunity(communityId)
                if (communityCache != null) {
                    val userData = communityCache.users.first {
                        it.user_id == userId.toInt()
                    }
                    val list = ArrayList(membersList.value)
                    communityCache.users.forEach { user ->
                        list.add(user)
                    }
                    membersList.tryEmit(list)
                    state.tryEmit(MemberInfoState.SUCCESS(userData))
                } else {
                    service.getCommunity(communityId.toInt()).onSuccess { community ->
                        val userData = community.users.first {
                            it.user_id == userId.toInt()
                        }
                        val list = ArrayList(membersList.value)
                        community.users.forEach { user ->
                            list.add(user)
                        }
                        membersList.tryEmit(list)
                        state.tryEmit(MemberInfoState.SUCCESS(userData))
                    }.onFailure { e ->
                        Timber.e(e.localizedMessage)
                        state.tryEmit(MemberInfoState.FAILURE(e.toUserError(resources)))
                    }
                }
            }
        }
    }

    fun popBack() {
        navManager.popBackStack()
    }

    fun removeUser(communityId: String, userId: Int) {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("tap_member_info_remove_from_community")
                val userIds = RemoveUsers(listOf(userId))
                service.removeUsersFromCommunity(communityId.toInt(), userIds).onSuccess {
                    Timber.d("User removed from community")
                    state.tryEmit(MemberInfoState.REMOVED)
                }.onFailure { e ->
                    Timber.e(e.localizedMessage)
                    state.tryEmit(MemberInfoState.FAILURE(e.toUserError(resources)))
                }
            }
            if (state.value == MemberInfoState.REMOVED) {
                popBackToCommunityDetail(communityId)
            }
        }
    }

    fun updateUserRole(role: Int, userId: Int, communityId: String) {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                val newRole = if (role == ADMIN_ROLE) NORMAL_ROLE else ADMIN_ROLE
                when (newRole) {
                    ADMIN_ROLE -> {
                        appAnalytics.logEvent("tap_member_info_make_admin")
                    }
                    NORMAL_ROLE -> {
                        appAnalytics.logEvent("tap_member_info_make_normal_user")
                    }
                }
                val list = ArrayList(memberListWithNewRole.value)
                membersList.value.forEach {
                    if (it.user_id == userId) {
                        list.add(UpdateCommunityMembersRole(it.user_id, newRole))
                    } else {
                        list.add(UpdateCommunityMembersRole(it.user_id, it.role))
                    }
                }
                memberListWithNewRole.tryEmit(list)
                service.addMembersToCommunity(communityId.toInt(), memberListWithNewRole.value).onSuccess {
                    state.tryEmit(MemberInfoState.UPDATEDROLE)
                }.onFailure { e ->
                    Timber.e(e.localizedMessage)
                    state.tryEmit(MemberInfoState.FAILURE(e.toUserError(resources)))
                }
            }
            if (state.value == MemberInfoState.UPDATEDROLE) {
                popBackToCommunityDetail(communityId)
            }
        }
    }

    private suspend fun popBackToCommunityDetail(communityId: String) {
        withContext(appDispatcher.MAIN) {
            navManager.managePopBackToCommunityDetail(communityId, true)
        }
    }
}

sealed class MemberInfoState {
    object LOADING : MemberInfoState()
    object REMOVED : MemberInfoState()
    object UPDATEDROLE : MemberInfoState()
    data class SUCCESS(val userDetail: UserAccount) : MemberInfoState()
    data class FAILURE(val message: String) : MemberInfoState()
}
