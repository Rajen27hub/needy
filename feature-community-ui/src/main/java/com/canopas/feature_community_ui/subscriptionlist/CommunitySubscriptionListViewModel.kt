package com.canopas.feature_community_ui.subscriptionlist

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.Subscription
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

const val DEFAULT_USER_ID = 0

@HiltViewModel
class CommunitySubscriptionListViewModel @Inject constructor(
    private val service: CommunityService,
    private val appDispatcher: AppDispatcher,
    private val navManager: CommunityNavManager,
    private val authManager: AuthManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<SubscriptionListState>(SubscriptionListState.LOADING)
    val currentUserId = MutableStateFlow(DEFAULT_USER_ID)

    fun getSharedSubscriptionsList(communityId: String, userId: String) {
        viewModelScope.launch {
            appAnalytics.logEvent("view_user_shared_subscriptions")
            state.tryEmit(SubscriptionListState.LOADING)
            currentUserId.value = authManager.currentUser!!.id
            withContext(appDispatcher.IO) {
                service.getSharedSubscriptionsList(communityId, userId).onSuccess {
                    state.tryEmit(SubscriptionListState.SUCCESS(it))
                }.onFailure { e ->
                    Timber.e(e.localizedMessage)
                    state.tryEmit(SubscriptionListState.FAILURE(e.toUserError(resources), e is APIError.NetworkError))
                }
            }
        }
    }

    fun popBack() {
        navManager.popBackStack()
    }

    fun navigateToSubscriptionStatus(
        subscriptionId: String,
        communityId: String,
        userId: String,
        userName: String
    ) {
        appAnalytics.logEvent("tap_shared_subscriptions_item")
        navManager.navigateToSubscriptionStatus(subscriptionId, communityId, userId, userName)
    }
}

sealed class SubscriptionListState {
    object LOADING : SubscriptionListState()
    data class SUCCESS(val subscriptions: List<Subscription>) : SubscriptionListState()
    data class FAILURE(val message: String, val isNetworkError: Boolean) : SubscriptionListState()
}
