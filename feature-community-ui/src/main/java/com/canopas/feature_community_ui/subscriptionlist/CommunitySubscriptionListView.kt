package com.canopas.feature_community_ui.subscriptionlist

import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.model.SubscriptionCardDetails
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils.Companion.getSubscriptionCardDetails
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.Subscription
import com.canopas.feature_community_ui.R

@Composable
fun CommunitySubscriptionListView(communityId: String, userId: String, userName: String) {

    val viewModel = hiltViewModel<CommunitySubscriptionListViewModel>()
    val state by viewModel.state.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.getSharedSubscriptionsList(communityId, userId)
    })

    val currentUser by viewModel.currentUserId.collectAsState()
    val topBarTitleText =
        if (currentUser.toString() == userId) stringResource(R.string.subscription_list_your_activities_text) else stringResource(
            R.string.subscription_list_top_bar_title_text,
            userName
        )
    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = topBarTitleText,
                        navigationIconOnClick = {
                            viewModel.popBack()
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }, backgroundColor = colors.background
    ) {
        state.let { listState ->
            when (listState) {
                is SubscriptionListState.LOADING -> {
                    Box(
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(it)
                            .background(colors.background),
                        contentAlignment = Alignment.Center
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }

                is SubscriptionListState.SUCCESS -> {
                    val subscriptions = listState.subscriptions
                    SubscriptionListSuccessView(
                        viewModel,
                        subscriptions,
                        communityId,
                        userId,
                        userName
                    )
                }

                is SubscriptionListState.FAILURE -> {
                    if (listState.isNetworkError) {
                        NetworkErrorView {
                            viewModel.getSharedSubscriptionsList(communityId, userId)
                        }
                    } else {
                        ServerErrorView {
                            viewModel.getSharedSubscriptionsList(communityId, userId)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun SubscriptionListSuccessView(
    viewModel: CommunitySubscriptionListViewModel,
    subscriptions: List<Subscription>,
    communityId: String,
    userId: String,
    userName: String
) {
    if (subscriptions.isEmpty()) {
        val currentUser by viewModel.currentUserId.collectAsState()
        val emptyListMessage =
            if (userId == currentUser.toString()) stringResource(R.string.subscription_list_empty_list_message_for_own_account_text) else stringResource(
                R.string.subscription_list_empty_list_message_text,
                userName
            )
        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(horizontal = 20.dp),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = emptyListMessage,
                style = AppTheme.typography.bodyTextStyle1,
                textAlign = TextAlign.Center,
                color = colors.textSecondary
            )
        }
    } else {
        val context = LocalContext.current
        val isDarkMode = isDarkMode
        LazyColumn(
            contentPadding = PaddingValues(horizontal = 20.dp, vertical = 12.dp)
        ) {
            items(subscriptions) { item ->
                val subscriptionCardData = getSubscriptionCardDetails(context, item.progress, isDarkMode)
                Box(
                    modifier = Modifier.fillMaxWidth(),
                    contentAlignment = Alignment.Center
                ) {
                    Box(
                        modifier = Modifier
                            .padding(vertical = 8.dp)
                            .widthIn(max = 600.dp)
                            .motionClickEvent {
                                viewModel.navigateToSubscriptionStatus(
                                    item.id.toString(),
                                    communityId,
                                    userId,
                                    userName
                                )
                            }
                            .fillMaxWidth()
                            .wrapContentHeight()
                            .clip(shape = RoundedCornerShape(15.dp))
                            .background(
                                color = if (isDarkMode) darkActivityCard else subscriptionCardData.cardBgColor,
                                shape = RoundedCornerShape(15.dp)
                            )
                            .border(
                                BorderStroke(1.dp, subscriptionCardData.cardBgColor),
                                RoundedCornerShape(16)
                            )

                    ) {
                        Column(
                            modifier = Modifier
                                .padding(14.dp),
                            verticalArrangement = Arrangement.SpaceBetween,
                            horizontalAlignment = Alignment.Start
                        ) {
                            Text(
                                text = item.activity.name,
                                style = AppTheme.typography.subscriptionListActivityNameTextStyle,
                                color = colors.textPrimary,
                                modifier = Modifier.fillMaxWidth(0.7f)
                            )

                            Text(
                                text = if (item.progress.recent.isNotEmpty()) "${subscriptionCardData.recentProgress}%" else stringResource(
                                    R.string.subscription_list_just_started_text
                                ),
                                color = if (isDarkMode)colors.textPrimary else subscriptionCardData.progressTextColor,
                                style = AppTheme.typography.h2TextStyle,
                                modifier = Modifier.padding(top = 8.dp)
                            )
                            if (item.progress.recent.isNotEmpty()) {
                                Text(
                                    text = subscriptionCardData.recentDaysCount,
                                    color = if (isDarkMode)colors.textSecondary else subscriptionCardData.progressTextColor.copy(0.75f),
                                    style = AppTheme.typography.buttonStyle
                                )
                            }
                        }
                        Row(
                            modifier = Modifier
                                .align(Alignment.TopEnd)
                                .padding(16.dp),
                            verticalAlignment = Alignment.CenterVertically
                        ) {
                            Text(
                                text = if (item.activity_time != null) item.getFormattedActivityTime() else stringResource(
                                    id = R.string.subscription_list_no_reminder_text
                                ),
                                color = colors.textSecondary,
                                style = AppTheme.typography.bodyTextStyle2,
                                modifier = Modifier.padding(top = 4.dp)
                            )
                            if (item.activity_time != null) {
                                Icon(
                                    painter = painterResource(id = R.drawable.ic_arrow),
                                    contentDescription = "Reminder Arrow",
                                    modifier = Modifier
                                        .padding(start = 4.dp, top = 4.dp)
                                        .size(10.dp),
                                    tint = colors.textSecondary
                                )
                            }
                        }
                        if (item.progress.recent.isNotEmpty()) {
                            Row(
                                modifier = Modifier
                                    .align(Alignment.BottomEnd)
                                    .padding(bottom = 16.dp, end = 16.dp)
                                    .wrapContentWidth()
                                    .height(40.dp),
                                horizontalArrangement = Arrangement.End,
                                verticalAlignment = Alignment.Bottom
                            ) {
                                BarChartGraph(subscriptionCardData, item)
                            }
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun BarChartGraph(subscriptionCardData: SubscriptionCardDetails, item: Subscription) {
    var graphState by remember { mutableStateOf(GraphPosition.Start) }
    val cardBackBg = subscriptionCardData.progressTextColor
    val isDarkMode = isDarkMode

    val height: Dp by animateDpAsState(
        if (graphState == GraphPosition.Start) 1.dp else 40.dp,
        spring(stiffness = Spring.StiffnessLow)
    )
    LaunchedEffect(key1 = Unit, block = {
        graphState = when (graphState) {
            GraphPosition.Start -> GraphPosition.Finish
            GraphPosition.Finish -> GraphPosition.Start
        }
    })
    item.progress.recent.forEach {
        CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {
            Canvas(
                modifier = Modifier
                    .width(10.dp)
                    .height(if (it.completed) height else 4.dp)
                    .clip(shape = RoundedCornerShape(topStart = 50.dp, topEnd = 50.dp)),
            ) {
                drawRect(
                    brush = Brush.verticalGradient(
                        if (isDarkMode) listOf(cardBackBg, cardBackBg) else
                            listOf(
                                subscriptionCardData.progressTextColor.copy(
                                    0.50f
                                ),
                                subscriptionCardData.progressTextColor
                            )
                    ),
                )
            }
            Spacer(modifier = Modifier.width(4.dp))
        }
    }
}

enum class GraphPosition {
    Start, Finish
}

private val darkActivityCard = Color(0xFF222222)
