package com.canopas.feature_community_ui

import androidx.navigation.NavController
import javax.inject.Inject
import javax.inject.Singleton

sealed class CommunityScreen(val route: String) {
    object Root : CommunityScreen("Root")
    object Community : CommunityScreen("Community")
    object AddCommunity : CommunityScreen("AddCommunity")
    object AddMembers : CommunityScreen("AddMembers")
    object ShareSubscriptions : CommunityScreen("ShareSubscriptions")
    object SignInMethod : CommunityScreen("SignInMethod")
    object CommunitySubscriptionList : CommunityScreen("CommunitySubscriptionList")
    object CommunitySubscriptionStatus : CommunityScreen("CommunitySubscriptionStatus")
    object CommunityDetail : CommunityScreen("CommunityDetail")
    object CommunityInfo : CommunityScreen("CommunityInfo")
    object CommunityMemberInfo : CommunityScreen("CommunityMemberInfo")
    object CommunityRequest : CommunityScreen("CommunityRequest")
    object PendingInvitation : CommunityScreen("PendingInvitation")
}

@Singleton
class CommunityNavManager @Inject constructor() {
    private var navController: NavController? = null

    fun setNavController(navController: NavController) {
        this.navController = navController
    }

    fun removeNavController() {
        navController = null
    }

    fun navigateToAddCommunity() {
        navController?.navigate(CommunityScreen.AddCommunity.route)
    }

    fun popBackStack() {
        navController?.popBackStack()
    }

    fun navigateToLogin() {
        navController?.navigate(CommunityScreen.SignInMethod.route)
    }

    fun navigateToShareSubscriptions(communityId: Int, userId: String) {
        navController?.navigate(CommunityScreen.ShareSubscriptions.route + "/$communityId" + "?userId=$userId")
    }
    fun navigateToPendingInvitation(communityId: Int) {
        navController?.navigate(CommunityScreen.PendingInvitation.route + "/$communityId")
    }

    fun navigateToCommunityDetailScreen(communityId: String) {
        navController?.navigate(CommunityScreen.CommunityDetail.route + "?communityId=$communityId")
    }

    fun popCommunityScreens() {
        navController?.popBackStack(CommunityScreen.Root.route, inclusive = true)
    }

    fun navigateToCommunityDetailScreenForFirstTime(communityId: String) {
        navController?.navigate(CommunityScreen.CommunityDetail.route + "?communityId=$communityId")
    }

    fun navigateToAddCommunityMembers(communityId: Int, showSkipButton: Boolean) {
        navController?.navigate(CommunityScreen.AddMembers.route + "/$communityId" + "?showSkipButton=$showSkipButton")
    }

    fun navigateToSharedSubscriptionsList(communityId: String, userId: String, userName: String) {
        navController?.navigate(CommunityScreen.CommunitySubscriptionList.route + "/$communityId" + "/$userName" + "/$userId")
    }

    fun navigateToSubscriptionStatus(
        subscriptionId: String,
        communityId: String,
        userId: String,
        userName: String
    ) {
        navController?.navigate(CommunityScreen.CommunitySubscriptionStatus.route + "/$communityId" + "/$userId" + "/$subscriptionId" + "/$userName")
    }

    fun navigateToCommunityMemberInfo(communityId: String, userId: String) {
        navController?.navigate(CommunityScreen.CommunityMemberInfo.route + "/$communityId" + "/$userId")
    }

    fun managePopBackToCommunityDetail(
        communityId: String,
        isDataUpdated: Boolean
    ) {
        navController?.previousBackStackEntry?.savedStateHandle?.set("communityId", communityId)
        navController?.previousBackStackEntry?.savedStateHandle?.set("isDataUpdated", isDataUpdated)
        navController?.popBackStack()
    }

    fun navigateToAddCommunityWithId(communityId: String, communityName: String) {
        navController?.navigate(CommunityScreen.AddCommunity.route + "?communityId=$communityId" + "?communityName=$communityName")
    }

    fun navigateToCommunityInfoScreen(communityId: String, userId: String) {
        navController?.navigate(CommunityScreen.CommunityInfo.route + "/$communityId" + "/$userId")
    }

    fun navigateToCommunityListScreen() {
        navController?.popBackStack(CommunityScreen.Community.route, inclusive = false)
    }

    fun navigateToCommunityRequestScreen() {
        navController?.navigate(CommunityScreen.CommunityRequest.route)
    }
}
