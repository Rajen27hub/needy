package com.canopas.feature_community_ui.community

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.auth.AuthStateChangeListener
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils.Companion.communities
import com.canopas.feature_community_data.TestUtils.Companion.communityRequest
import com.canopas.feature_community_data.TestUtils.Companion.communityRequest2
import com.canopas.feature_community_data.TestUtils.Companion.user
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.kotlin.capture
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class CommunityViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: CommunityViewModel

    private val services = mock<CommunityService>()
    private val resources = mock<Resources>()
    private val appAnalytics = mock<AppAnalytics>()

    private val authManager = mock<AuthManager>()

    private val navManager = mock<CommunityNavManager>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel = CommunityViewModel(services, testDispatcher, authManager, navManager, appAnalytics, resources)
    }

    @Test
    fun test_defaultCommunityStatus() = runTest {
        val loadingState = CommunityState.LOADING
        whenever(services.getCommunities()).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(emptyList())
        }
        viewModel.getCommunities()
        Assert.assertEquals(loadingState, viewModel.state.value)

        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        whenever(services.getCommunities()).thenReturn(
            Result.success(listOf(communities))
        )
        viewModel.getCommunities()
        val successState = CommunityState.SUCCESS(listOf(communities))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_loadingCommunityListTest() = runTest {
        whenever(services.getCommunities()).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(emptyList())
        }
        viewModel.getCommunities()
        val loadingState = CommunityState.LOADING
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_SuccessGetCommunities() = runTest {
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        whenever(services.getCommunities()).thenReturn(Result.success(listOf(communities)))
        viewModel.getCommunities()
        val successState = CommunityState.SUCCESS(listOf(communities))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_SuccessGetCommunities_withEmptyList() = runTest {
        whenever(authManager.authState).thenReturn(AuthState.ANONYMOUS(user))
        whenever(services.getCommunities()).thenReturn(
            Result.success(emptyList())
        )
        viewModel.getCommunities()
        val successState = CommunityState.SUCCESS(emptyList())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_FailureCommunityList() = runTest {
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        whenever(services.getCommunities()).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.getCommunities()
        val failureState = CommunityState.FAILURE("Error", false)
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun testDisplayCommunityList_onSuccess_onAuthStateChange() = runTest {
        val successState = CommunityState.SUCCESS(listOf(communities))
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        whenever(services.getCommunities()).thenReturn(Result.success(listOf(communities)))
        val authCaptor: ArgumentCaptor<AuthStateChangeListener> =
            ArgumentCaptor.forClass(AuthStateChangeListener::class.java)
        verify(authManager).addListener(capture(authCaptor))
        authCaptor.value.onAuthStateChanged(AuthState.VERIFIED(user))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun testDisplayCommunityList_onFailure_onAuthStateChange() = runTest {
        val failureState = CommunityState.FAILURE("Error", false)
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        whenever(services.getCommunities()).thenReturn(Result.failure(RuntimeException("Error")))
        val authCaptor: ArgumentCaptor<AuthStateChangeListener> =
            ArgumentCaptor.forClass(AuthStateChangeListener::class.java)
        verify(authManager).addListener(capture(authCaptor))
        authCaptor.value.onAuthStateChanged(AuthState.ANONYMOUS(user))
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_resetState_whenReconnect_to_Network() = runTest {
        val startState = CommunityState.START
        viewModel.resetState()
        Assert.assertEquals(startState, viewModel.state.value)
    }

    @Test
    fun test_navigate_to_logIn() {
        whenever(authManager.authState).thenReturn(AuthState.ANONYMOUS(user))
        viewModel.navigateToAddCommunity()
        verify(navManager).navigateToLogin()
    }

    @Test
    fun test_navigate_to_add_community() {
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        viewModel.navigateToAddCommunity()
        verify(navManager).navigateToAddCommunity()
    }

    @Test
    fun test_loadingGetCommunityRequestListSection() = runTest {
        val loadingState = RequestState.LOADING
        whenever(services.getCommunityRequests()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.getCommunityRequestList()
        Assert.assertEquals(loadingState, viewModel.requestState.value)
    }

    @Test
    fun test_SuccessGetCommunityRequestListSection() = runTest {
        val successState = RequestState.SUCCESS
        whenever(services.getCommunityRequests()).thenReturn(Result.success(listOf(communityRequest, communityRequest2)))
        viewModel.getCommunityRequestList()
        Assert.assertEquals(successState, viewModel.requestState.value)
    }

    @Test
    fun test_FailureGetCommunityRequestListSection() = runTest {
        val failureState = RequestState.FAILURE("Error")
        whenever(services.getCommunityRequests()).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getCommunityRequestList()
        Assert.assertEquals(failureState, viewModel.requestState.value)
    }

    @Test
    fun test_resetJoinState() {
        val setState = JoinState.IDLE
        viewModel.resetJoinState()
        Assert.assertEquals(setState, viewModel.joinState.value)
    }
}
