package com.canopas.feature_community_ui.pendinginvitation

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils.Companion.pendingInvitation
import com.canopas.feature_community_data.TestUtils.Companion.pendingInvitation2
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class PendingInvitationViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: PendingInvitationsViewModel
    private val service = mock<CommunityService>()
    private val navManager = mock<CommunityNavManager>()
    private val appAnalytics = mock<AppAnalytics>()
    private val resources = mock<Resources>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setUp() {
        viewModel = PendingInvitationsViewModel(navManager, service, testDispatcher, appAnalytics, resources)
    }

    @Test
    fun test_getPendingInvitation_loading_state() = runTest {

        whenever(service.getPendingInvitation(1)) doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.getPendingInvitation(1)
        assertEquals(InvitationState.LOADING, viewModel.state.value)
    }

    @Test
    fun test_getPendingInvitation_success_state() = runTest {
        val successState = InvitationState.SUCCESS(
            listOf(
                pendingInvitation,
                pendingInvitation2
            )
        )
        whenever(service.getPendingInvitation(1)).thenReturn(
            Result.success(
                listOf(
                    pendingInvitation,
                    pendingInvitation2
                )
            )
        )
        viewModel.getPendingInvitation(1)
        assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_getPendingInvitation_failure_state() = runTest {

        whenever(service.getPendingInvitation(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getPendingInvitation(1)
        assertEquals(InvitationState.FAILURE("Error", false), viewModel.state.value)
    }

    @Test
    fun test_onStart() {
        viewModel.onStart(1)
        assertEquals(1, viewModel.communityId.value)
    }

    @Test
    fun test_resetDeleteState() {
        viewModel.resetDeleteState()
        assertEquals(DeleteState.NONE, viewModel.deleteState.value)
    }

    @Test
    fun test_popBack() {
        viewModel.navigatePopBack()
        verify(navManager).popBackStack()
    }
}
