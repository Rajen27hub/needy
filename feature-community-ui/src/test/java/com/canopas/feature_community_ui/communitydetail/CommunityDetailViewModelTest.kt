package com.canopas.feature_community_ui.communitydetail

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils.Companion.community
import com.canopas.feature_community_data.TestUtils.Companion.user
import com.canopas.feature_community_data.di.CommunityCache
import com.canopas.feature_community_data.model.UpdateSharePrompt
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class CommunityDetailViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: CommunityDetailViewModel
    private val resources = mock<Resources>()
    private val service = mock<CommunityService>()

    private val navManager = mock<CommunityNavManager>()
    private val authManager = mock<AuthManager>()
    private val communityCache = mock<CommunityCache>()

    private val appAnalytics = mock<AppAnalytics>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel = CommunityDetailViewModel(
            service,
            testDispatcher,
            navManager,
            communityCache,
            authManager,
            appAnalytics,
            resources
        )
        whenever(authManager.currentUser).thenReturn(user)
    }

    @Test
    fun test_display_community_detail_loading_state() = runTest {
        val loadingState = DetailState.LOADING
        whenever(service.getCommunity(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(community)
        }
        viewModel.onStart("1", true)
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_display_community_detail_success_state() = runTest {
        whenever(service.getCommunity(1)).thenReturn(Result.success(community))
        viewModel.onStart("1", false)
        val successState = DetailState.SUCCESS(community)
        verify(communityCache).put(community)
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_display_community_detail_failure_state() = runTest {
        whenever(service.getCommunity(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.onStart("1", false)
        val failureState = DetailState.FAILURE("Error", false)
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_popBackStack() {
        viewModel.managePopBack()
        verify(navManager).popBackStack()
    }

    @Test
    fun test_navigate_to_SubscriptionList() {
        viewModel.navigateToSharedSubscriptions(
            community.id.toString(),
            user.user_id.toString(),
            ""
        )
        verify(navManager).navigateToSharedSubscriptionsList(
            community.id.toString(),
            user.user_id.toString(),
            ""
        )
    }

    @Test
    fun test_navigate_to_Community_info() {
        viewModel.navigateToCommunityInfoView(community.id)
        verify(navManager).navigateToCommunityInfoScreen(
            community.id.toString(), viewModel.userId.value.toString()
        )
    }

    @Test
    fun test_navigate_to_ShareActivitiesPrompt() = runTest {
        val body = UpdateSharePrompt(true)
        val jsonElement = mock<JsonElement>()
        whenever(service.updateSharePromptedFlag(community.id.toString(), viewModel.userId.value.toString(), body)).thenReturn(Result.success(jsonElement))
        viewModel.navigateToShareActivitiesPrompt(community.id.toString())
        withContext(testDispatcher.MAIN) {
            delay(2000)
        }
        verify(navManager).navigateToShareSubscriptions(
            community.id, viewModel.userId.value.toString()
        )
    }
}
