package com.canopas.feature_community_ui.subscriptionlist

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils.Companion.community
import com.canopas.feature_community_data.TestUtils.Companion.subscription
import com.canopas.feature_community_data.TestUtils.Companion.user
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class CommunitySubscriptionListViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: CommunitySubscriptionListViewModel
    private val resources = mock<Resources>()
    private val services = mock<CommunityService>()

    private val appAnalytics = mock<AppAnalytics>()

    private val navManager = mock<CommunityNavManager>()
    private val authManager = mock<AuthManager>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel =
            CommunitySubscriptionListViewModel(services, testDispatcher, navManager, authManager, appAnalytics, resources)
    }

    @Test
    fun test_popBack() {
        viewModel.popBack()
        verify(navManager).popBackStack()
    }

    @Test
    fun test_navigate_to_status_screen() {
        viewModel.navigateToSubscriptionStatus("", "", "", "")
        verify(navManager).navigateToSubscriptionStatus("", "", "", "")
    }

    @Test
    fun test_defaultState_getSharedSubscriptionList() = runTest {
        val loadingState = SubscriptionListState.LOADING
        whenever(authManager.currentUser).thenReturn(user)
        whenever(services.getSharedSubscriptionsList("", "")).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.getSharedSubscriptionsList("", "")
        Assert.assertEquals(loadingState, viewModel.state.value)
        val successState = SubscriptionListState.SUCCESS(listOf(subscription))
        whenever(services.getSharedSubscriptionsList(community.id.toString(), user.id.toString())).thenReturn(Result.success(listOf(subscription)))
        viewModel.getSharedSubscriptionsList(community.id.toString(), user.id.toString())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_loadingState_getSharedSubscriptionList() = runTest {
        val loadingState = SubscriptionListState.LOADING
        whenever(authManager.currentUser).thenReturn(user)
        whenever(services.getSharedSubscriptionsList("", "")).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.getSharedSubscriptionsList("", "")
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_successState_getSharedSubscriptionList() = runTest {
        val successState = SubscriptionListState.SUCCESS(listOf(subscription))
        whenever(authManager.currentUser).thenReturn(user)
        whenever(
            services.getSharedSubscriptionsList(
                community.id.toString(),
                user.id.toString()
            )
        ).thenReturn(Result.success(listOf(subscription)))
        viewModel.getSharedSubscriptionsList(community.id.toString(), user.id.toString())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_failureState_getSharedSubscriptionList() = runTest {
        val failureState = SubscriptionListState.FAILURE("Error", false)
        whenever(authManager.currentUser).thenReturn(user)
        whenever(
            services.getSharedSubscriptionsList(
                community.id.toString(),
                user.id.toString()
            )
        ).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getSharedSubscriptionsList(community.id.toString(), user.id.toString())
        Assert.assertEquals(failureState, viewModel.state.value)
    }
}
