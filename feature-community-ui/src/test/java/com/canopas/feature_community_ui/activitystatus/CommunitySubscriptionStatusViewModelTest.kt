package com.canopas.feature_community_ui.activitystatus

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.ui.model.ProgressTabType
import com.canopas.feature_community_data.TestUtils.Companion.subscription
import com.canopas.feature_community_data.TestUtils.Companion.user
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class CommunitySubscriptionStatusViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: CommunitySubscriptionStatusViewModel

    private val services = mock<CommunityService>()

    private val appAnalytics = mock<AppAnalytics>()

    private val navManager = mock<CommunityNavManager>()
    private val authManager = mock<AuthManager>()
    private val resources = mock<Resources>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel = CommunitySubscriptionStatusViewModel(services, testDispatcher, navManager, authManager, appAnalytics, resources)
    }

    @Test
    fun test_onTabChange() {
        viewModel.handleProgressTabSelection(ProgressTabType.PROGRESS_7D)
        Assert.assertEquals(ProgressTabType.PROGRESS_7D, viewModel.progressSelectedTab.value)
    }

    @Test
    fun test_popBack() {
        viewModel.managePopBack()
        verify(navManager).popBackStack()
    }

    @Test
    fun test_defaultState_getSubscriptionDetails() = runTest {
        val loadingState = SubscriptionStatusState.LOADING
        whenever(authManager.currentUser).thenReturn(user)
        whenever(services.retrieveUserSubscriptionById("", "", "")).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(subscription)
        }
        viewModel.getSubscriptionDetails("", "", "")
        Assert.assertEquals(loadingState, viewModel.state.value)

        whenever(services.retrieveUserSubscriptionById("", "", "")).thenReturn(Result.success(subscription))
        viewModel.getSubscriptionDetails("", "", "")
        val successState = SubscriptionStatusState.SUCCESS(subscription)
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_loadingState_getSubscriptionDetails() = runTest {
        val loadingState = SubscriptionStatusState.LOADING
        whenever(services.retrieveUserSubscriptionById("", "", "")).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(subscription)
        }
        viewModel.getSubscriptionDetails("", "", "")
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_successState_getSubscriptionDetails() = runTest {
        whenever(authManager.currentUser).thenReturn(user)
        whenever(services.retrieveUserSubscriptionById("", "", "")).thenReturn(Result.success(subscription))
        viewModel.getSubscriptionDetails("", "", "")
        val successState = SubscriptionStatusState.SUCCESS(subscription)
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_failureState_getSubscriptionDetails() = runTest {
        whenever(authManager.currentUser).thenReturn(user)
        whenever(services.retrieveUserSubscriptionById("", "", "")).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getSubscriptionDetails("", "", "")
        val failureState = SubscriptionStatusState.FAILURE("Error", false)
        Assert.assertEquals(failureState, viewModel.state.value)
    }
}
