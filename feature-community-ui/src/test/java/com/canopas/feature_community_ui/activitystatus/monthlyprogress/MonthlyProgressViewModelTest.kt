package com.canopas.feature_community_ui.activitystatus.monthlyprogress

import android.content.res.Resources
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.MainCoroutineRule
import com.canopas.feature_community_ui.activitystatus.ProgressHistoryState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class MonthlyProgressViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private lateinit var viewModel: MonthlyProgressViewModel
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )
    private val resources = mock<Resources>()
    private val services = mock<CommunityService>()
    private val authManager = mock<AuthManager>()

    @Before
    fun setup() {
        viewModel = MonthlyProgressViewModel(testDispatcher, services, resources)
    }

    @Test
    fun test_get_progress_history_loadingState() = runTest {
        val loadingData = ProgressHistoryState.LOADING
        whenever(services.retrieveUserSubscriptionById("", "", ""))
            .thenReturn(Result.success(com.canopas.data.utils.TestUtils.subscription))
        whenever(services.getSubscriptionsProgressHistory(1, 1, 10000L, 10000L)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.getProgressHistory(10000L, 10000L, 1, "", "")
        Assert.assertEquals(loadingData, viewModel.progressState.value)
    }

    @Test
    fun test_get_progress_history_successState() = runTest {
        val successState = ProgressHistoryState.PROCESSED
        whenever(authManager.currentUser).thenReturn(TestUtils.user)
        whenever(services.retrieveUserSubscriptionById("", "", ""))
            .thenReturn(Result.success(TestUtils.subscription))
        whenever(services.getSubscriptionsProgressHistory(1, 1, 10000L, 10000L)).thenReturn(
            Result.success(
                listOf()
            )
        )
        viewModel.getProgressHistory(10000L, 10000L, 1, "December 2022", "1")
        Assert.assertEquals(successState, viewModel.progressState.value)
    }

    @Test
    fun test_get_progress_history_failureState() = runTest {
        val failureState = ProgressHistoryState.FAILURE("Error")
        whenever(authManager.currentUser).thenReturn(TestUtils.user)
        whenever(services.retrieveUserSubscriptionById("", "", ""))
            .thenReturn(Result.success(TestUtils.subscription))
        whenever(services.getSubscriptionsProgressHistory(1, 1, 0L, 10000L)).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.getProgressHistory(0L, 10000L, 1, "December 2022", "1")
        Assert.assertEquals(failureState, viewModel.progressState.value)
    }
}
