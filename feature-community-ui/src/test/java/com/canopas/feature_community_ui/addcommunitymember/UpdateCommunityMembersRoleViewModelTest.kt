package com.canopas.feature_community_ui.addcommunitymember

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.UserAccount
import com.canopas.feature_community_data.TestUtils.Companion.communities
import com.canopas.feature_community_data.TestUtils.Companion.community
import com.canopas.feature_community_data.TestUtils.Companion.user
import com.canopas.feature_community_data.di.CommunityCache
import com.canopas.feature_community_data.model.JoinRequest
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import com.canopas.feature_community_ui.addcommunitymembers.AddCommunityMembersViewModel
import com.canopas.feature_community_ui.addcommunitymembers.AddMembersState
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class UpdateCommunityMembersRoleViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: AddCommunityMembersViewModel
    private val resources = mock<Resources>()
    private val services = mock<CommunityService>()

    private val appAnalytics = mock<AppAnalytics>()

    private val navManager = mock<CommunityNavManager>()
    private val communityCache = mock<CommunityCache>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel = AddCommunityMembersViewModel(services, testDispatcher, navManager, communityCache, appAnalytics, resources)
    }

    @Test
    fun test_loading_state_on_search_value_change() = runTest {
        viewModel.onSearchValueChange("Test")
        Assert.assertEquals("Test", viewModel.searchString.value)
    }

    @Test
    fun test_add_user_on_userSelected() {
        viewModel.onUserSelected(user)
        Assert.assertEquals(listOf(user), viewModel.selectedMembers.value)
    }

    @Test
    fun test_remove_user() {
        viewModel.removeUser(user)
        val list = listOf<UserAccount>()
        Assert.assertEquals(list, viewModel.selectedMembers.value)
    }

    @Test
    fun test_navigate_to_share_subscriptions() {
        val communityId = communities.id
        viewModel.manageNavigation(
            communityId = communityId.toString(),
            showSkipButton = true
        )
        verify(navManager).navigateToShareSubscriptions(communityId, "")
    }

    @Test
    fun test_popBack_On_Invite_Click() {
        val communityId = communities.id
        viewModel.manageNavigation(
            communityId = communityId.toString(),
            showSkipButton = false
        )
        verify(navManager).popBackStack()
    }

    @Test
    fun test_invite_members() = runTest {
        val communityId = communities.id
        val jsonElement = mock<JsonElement>()
        val successState = AddMembersState.SUCCESS
        whenever(services.sendCommunityJoinRequest(JoinRequest(communityId, listOf()))).thenReturn(Result.success(jsonElement))
        viewModel.sendCommunityJoinRequest(communityId = communityId.toString())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_fetch_community_members_loading_state() = runTest {
        val loadingState = AddMembersState.LOADING
        whenever(services.getCommunity(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(community)
        }
        viewModel.onStart("1")
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_fetch_community_members_success_state() = runTest {
        whenever(services.getCommunity(1)).thenReturn(Result.success(community))
        viewModel.onStart("1")
        val successState = AddMembersState.FOUNDUSERS
        val users = community.users
        Assert.assertEquals(successState, viewModel.state.value)
        Assert.assertEquals(users, viewModel.communityMembers.value)
    }

    @Test
    fun test_fetch_community_members_failure_state() = runTest {
        whenever(services.getCommunity(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.onStart("1")
        val failureState = AddMembersState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_fetch_community_members_success_state_fromCache() = runTest {
        whenever(communityCache.getCommunity("1")).thenReturn(community)
        viewModel.onStart("1")
        val successState = AddMembersState.FOUNDUSERS
        val users = community.users
        Assert.assertEquals(successState, viewModel.state.value)
        Assert.assertEquals(users, viewModel.communityMembers.value)
    }

    @Test
    fun test_popBack() {
        viewModel.managePopBack()
        verify(navManager).popBackStack()
    }
}
