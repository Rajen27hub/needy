package com.canopas.feature_community_ui.memberinfoview

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils.Companion.community
import com.canopas.feature_community_data.TestUtils.Companion.user
import com.canopas.feature_community_data.di.CommunityCache
import com.canopas.feature_community_data.model.RemoveUsers
import com.canopas.feature_community_data.model.UpdateCommunityMembersRole
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class CommunityMemberInfoViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: CommunityMemberInfoViewModel
    private val resources = mock<Resources>()
    private val service = mock<CommunityService>()

    private val appAnalytics = mock<AppAnalytics>()

    private val navManager = mock<CommunityNavManager>()
    private val communityCache = mock<CommunityCache>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel = CommunityMemberInfoViewModel(service, testDispatcher, navManager, communityCache, appAnalytics, resources)
    }

    @Test
    fun test_display_community_detail_loading_state() = runTest {
        val loadingState = MemberInfoState.LOADING
        whenever(communityCache.getCommunity(community.id.toString())).thenReturn(null)
        whenever(service.getCommunity(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(community)
        }
        viewModel.fetchUserData("1", "1")
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_display_community_detail_success_state() = runTest {
        whenever(communityCache.getCommunity(community.id.toString())).thenReturn(null)
        whenever(service.getCommunity(1)).thenReturn(Result.success(community))
        viewModel.fetchUserData("1", "1")
        val successState = MemberInfoState.SUCCESS(community.users.first { it.user_id == 1 })
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_display_community_detail_failure_state() = runTest {
        whenever(communityCache.getCommunity(community.id.toString())).thenReturn(null)
        whenever(service.getCommunity(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.fetchUserData("1", "1")
        val failureState = MemberInfoState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_display_community_detail_loading_state_fromCache() {
        val loadingState = MemberInfoState.LOADING
        whenever(communityCache.getCommunity(community.id.toString())).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            community
        }
        viewModel.fetchUserData("1", "1")
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_display_community_detail_success_state_fromCache() {
        whenever(communityCache.getCommunity("1")).thenReturn(community)
        viewModel.fetchUserData("1", "1")
        val successState = MemberInfoState.SUCCESS(community.users.first { it.user_id == 1 })
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_popBackStack() {
        viewModel.popBack()
        verify(navManager).popBackStack()
    }

    @Test
    fun test_remove_user_from_community_loading_state() = runTest {
        val loadingState = MemberInfoState.LOADING
        whenever(service.removeUsersFromCommunity(1, RemoveUsers(listOf(1)))).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.removeUser("1", 1)
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_remove_user_from_community_success_state() = runTest {
        val jsonElement = mock<JsonElement>()
        val successState = MemberInfoState.REMOVED
        whenever(service.removeUsersFromCommunity(1, RemoveUsers(listOf(1)))).thenReturn(Result.success(jsonElement))
        viewModel.removeUser("1", 1)
        Assert.assertEquals(successState, viewModel.state.value)
        verify(navManager).managePopBackToCommunityDetail("1", true)
    }

    @Test
    fun test_remove_user_from_community_failure_state() = runTest {
        whenever(service.removeUsersFromCommunity(1, RemoveUsers(listOf(1)))).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.removeUser("1", 1)
        val failureState = MemberInfoState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_update_user_role_loading_state() = runTest {
        val loadingState = MemberInfoState.LOADING
        val membersList = listOf(UpdateCommunityMembersRole(user.id, user.role))
        whenever(service.addMembersToCommunity(1, membersList)).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.updateUserRole(1, 1, "")
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_update_user_role_success_state() = runTest {
        val jsonElement = mock<JsonElement>()
        val successState = MemberInfoState.UPDATEDROLE
        val membersList = listOf(UpdateCommunityMembersRole(user.user_id, user.role))
        whenever(service.addMembersToCommunity(1, membersList)).thenReturn(Result.success(jsonElement))
        viewModel.updateUserRole(0, 1, "1")
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_update_user_role_failure_state() = runTest {
        whenever(service.addMembersToCommunity(1, emptyList())).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.updateUserRole(1, 1, "1")
        val failureState = MemberInfoState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.state.value)
    }
}
