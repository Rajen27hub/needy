package com.canopas.feature_community_ui.addcommunity

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils.Companion.community
import com.canopas.feature_community_data.TestUtils.Companion.communityData
import com.canopas.feature_community_data.model.AddCommunity
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class AddCommunityViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: AddCommunityViewModel

    private val services = mock<CommunityService>()
    private val resources = mock<Resources>()
    private val navManager = mock<CommunityNavManager>()

    private val appAnalytics = mock<AppAnalytics>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel = AddCommunityViewModel(navManager, testDispatcher, services, appAnalytics, resources)
    }

    @Test
    fun test_manage_pop_back() {
        viewModel.managePopBack()
        verify(navManager).popBackStack()
    }

    @Test
    fun test_navigate_to_add_members() {
        viewModel.navigateToAddMembers(community.id)
        verify(navManager).navigateToAddCommunityMembers(community.id, true)
    }

    @Test
    fun test_on_community_name_changed() = runTest {
        viewModel.setCommunityName("Canopas", true)
        viewModel.onCommunityNameChange("Canopas")
        Assert.assertEquals("Canopas", viewModel.communityName.value)
        Assert.assertEquals(false, viewModel.allowUpdateCommunity.value)
    }

    @Test
    fun test_create_community_loading_state() = runTest {
        viewModel.onCommunityNameChange(communityData.name)
        val loadingState = AddCommunityState.LOADING
        whenever(services.createCommunity(communityData)).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
                null
            }
        }
        viewModel.createAndUpdateCommunityClick("")
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_create_community_success_state() = runTest {
        viewModel.onCommunityNameChange(communityData.name)
        val successState = AddCommunityState.SUCCESS(community)
        whenever(services.createCommunity(communityData)).thenReturn(Result.success(community))
        viewModel.createAndUpdateCommunityClick("")
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_create_community_failure_state() = runTest {
        viewModel.onCommunityNameChange(communityData.name)
        val failureState = AddCommunityState.FAILURE("Error")
        whenever(services.createCommunity(communityData)).thenReturn(
            Result.failure(
                RuntimeException(
                    "Error"
                )
            )
        )
        viewModel.createAndUpdateCommunityClick("")
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_update_community_name_success_state() = runTest {
        val updateSuccessState = AddCommunityState.UPDATE(community)
        val communityId = community.id
        val communityDetails = AddCommunity(name = community.name, description = "", image_url = "")
        whenever(services.updateCommunity(communityId, communityDetails)).thenReturn(
            Result.success(community)
        )
        viewModel.onCommunityNameChange(community.name)
        viewModel.createAndUpdateCommunityClick("1")
        Assert.assertEquals(updateSuccessState, viewModel.state.value)
    }

    @Test
    fun test_update_community_name_failure_state() = runTest {
        val communityId = community.id
        val communityDetails = AddCommunity(name = community.name, description = "", image_url = "")
        whenever(services.updateCommunity(communityId, communityDetails)).thenReturn(
            Result.failure(RuntimeException("Error"))
        )
        viewModel.onCommunityNameChange(community.name)
        viewModel.createAndUpdateCommunityClick("1")
        val failureState = AddCommunityState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_on_fetch_community_name() {
        viewModel.setCommunityName("Canopas", false)
        Assert.assertEquals("Canopas", viewModel.communityName.value)
        Assert.assertEquals("Canopas", viewModel.previousCommunityName.value)
    }
}
