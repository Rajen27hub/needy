package com.canopas.feature_community_ui.communityrequest

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils.Companion.communityRequest
import com.canopas.feature_community_data.TestUtils.Companion.communityRequest2
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class CommunityRequestModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private lateinit var viewModel: CommunityRequestsViewModel
    private val resources = mock<Resources>()
    private val services = mock<CommunityService>()

    private val navManager = mock<CommunityNavManager>()

    private val appAnalytics = mock<AppAnalytics>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun sutUp() {
        viewModel = CommunityRequestsViewModel(navManager, services, testDispatcher, appAnalytics, resources)
    }
    @Test
    fun test_getCommunityRequestList_loading() = runTest {
        val loadingState = RequestState.LOADING
        whenever(services.getCommunityRequests()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.getCommunityRequestList()
        assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_getCommunityRequestList_failure() = runTest {
        val failureState = RequestState.FAILURE("Error", false)
        whenever(services.getCommunityRequests()).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getCommunityRequestList()
        assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_getCommunityRequestList_Success() = runTest {
        val successState = RequestState.SUCCESS(listOf(communityRequest, communityRequest2))
        whenever(services.getCommunityRequests()).thenReturn(Result.success(listOf(communityRequest, communityRequest2)))
        viewModel.getCommunityRequestList()
        assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_resetJoinState() {
        val setState = JoinState.NONE
        viewModel.resetJoinState()
        assertEquals(setState, viewModel.joinState.value)
    }

    @Test
    fun test_managePopBack() {
        viewModel.managePopBack()
        verify(navManager).popBackStack()
    }
}
