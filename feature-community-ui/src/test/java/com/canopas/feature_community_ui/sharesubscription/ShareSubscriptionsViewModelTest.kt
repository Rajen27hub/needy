package com.canopas.feature_community_ui.sharesubscription

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils.Companion.community
import com.canopas.feature_community_data.TestUtils.Companion.subscription
import com.canopas.feature_community_data.TestUtils.Companion.user
import com.canopas.feature_community_data.model.ShareSubscriptions
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import com.canopas.feature_community_ui.sharesubscriptions.ShareSubscriptionState
import com.canopas.feature_community_ui.sharesubscriptions.ShareSubscriptionsViewModel
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class ShareSubscriptionsViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: ShareSubscriptionsViewModel
    private val resources = mock<Resources>()
    private val services = mock<CommunityService>()

    private val appAnalytics = mock<AppAnalytics>()

    private val navManager = mock<CommunityNavManager>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel = ShareSubscriptionsViewModel(services, testDispatcher, navManager, appAnalytics, resources)
    }

    @Test
    fun test_default_get_userSubscriptions() = runTest {
        val loadingState = ShareSubscriptionState.LOADING
        whenever(services.getSubscriptionsToShare()).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(emptyList())
        }
        viewModel.getUserSubscriptions(true, "", "")
        Assert.assertEquals(loadingState, viewModel.state.value)
        val successState = ShareSubscriptionState.SUCCESS(listOf(subscription))
        whenever(services.getSubscriptionsToShare()).thenReturn(Result.success(listOf(subscription)))
        viewModel.getUserSubscriptions(true, "", "")
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_success_state_get_userSubscriptions() = runTest {
        val successState = ShareSubscriptionState.SUCCESS(listOf(subscription))
        whenever(services.getSubscriptionsToShare()).thenReturn(Result.success(listOf(subscription)))
        viewModel.getUserSubscriptions(true, "", "")
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_failure_state_get_userSubscriptions() = runTest {
        val failureState = ShareSubscriptionState.FAILURE("Error")
        whenever(services.getSubscriptionsToShare()).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getUserSubscriptions(true, "", "")
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_default_get_remaining_userSubscriptions() = runTest {
        val loadingState = ShareSubscriptionState.LOADING
        whenever(services.getSharedSubscriptionsList("", "")).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(emptyList())
        }
        whenever(services.getSubscriptionsToShare()).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(emptyList())
        }
        viewModel.getUserSubscriptions(false, "", "")
        Assert.assertEquals(loadingState, viewModel.state.value)
        val successState = ShareSubscriptionState.SUCCESS(listOf(subscription))
        whenever(services.getSharedSubscriptionsList(community.id.toString(), user.user_id.toString())).thenReturn(Result.success(emptyList()))
        whenever(services.getSubscriptionsToShare()).thenReturn(Result.success(listOf(subscription)))
        viewModel.getUserSubscriptions(false, community.id.toString(), user.user_id.toString())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_success_state_get_remaining_userSubscriptions() = runTest {
        val successState = ShareSubscriptionState.SUCCESS(listOf(subscription))
        whenever(services.getSharedSubscriptionsList(community.id.toString(), user.user_id.toString())).thenReturn(Result.success(emptyList()))
        whenever(services.getSubscriptionsToShare()).thenReturn(Result.success(listOf(subscription)))
        viewModel.getUserSubscriptions(false, community.id.toString(), user.user_id.toString())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_subscriptionSelection_toggle() {
        viewModel.toggleSubscriptionsSelection(subscription.id)
        Assert.assertEquals(listOf(subscription.id), viewModel.selectedSubscriptions.value)
    }

    @Test
    fun test_defaultState_share_userSubscriptions() = runTest {
        val data = ShareSubscriptions(emptyList())
        val loadingState = ShareSubscriptionState.FAILURE("Error")
        whenever(services.shareSubscriptionsWithCommunity(community.id, data)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.shareSubscriptions(community.id.toString())
        Assert.assertEquals(loadingState, viewModel.state.value)
        val successData = ShareSubscriptions(listOf(subscription.id))
        val jsonElement = mock<JsonElement>()
        val successState = ShareSubscriptionState.SHARESUCCESS
        whenever(services.shareSubscriptionsWithCommunity(community.id, successData)).thenReturn(Result.success(jsonElement))
        viewModel.toggleSubscriptionsSelection(subscription.id)
        viewModel.shareSubscriptions(community.id.toString())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_loadingState_share_userSubscriptions() = runTest {
        val data = ShareSubscriptions(emptyList())
        val loadingState = ShareSubscriptionState.FAILURE("Error")
        whenever(services.shareSubscriptionsWithCommunity(community.id, data)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.shareSubscriptions(community.id.toString())
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_successState_share_userSubscriptions() = runTest {
        val data = ShareSubscriptions(listOf(subscription.id))
        val jsonElement = mock<JsonElement>()
        val successState = ShareSubscriptionState.SHARESUCCESS
        whenever(services.shareSubscriptionsWithCommunity(community.id, data)).thenReturn(Result.success(jsonElement))
        viewModel.shareSubscriptions(community.id.toString())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_failureState_share_userSubscriptions() = runTest {
        val data = ShareSubscriptions(emptyList())
        val loadingState = ShareSubscriptionState.FAILURE("Error")
        whenever(services.shareSubscriptionsWithCommunity(community.id, data)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.shareSubscriptions(community.id.toString())
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_navigateToCommunityDetail() {
        viewModel.navigateToCommunityDetailScreen(community.id.toString())
        verify(navManager).navigateToCommunityDetailScreenForFirstTime(community.id.toString())
    }

    @Test
    fun test_popBack() {
        viewModel.popBack()
        verify(navManager).popBackStack()
    }
}
