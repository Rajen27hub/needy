package com.canopas.feature_community_ui.communityinfo

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_community_data.TestUtils.Companion.community
import com.canopas.feature_community_data.TestUtils.Companion.community2
import com.canopas.feature_community_data.TestUtils.Companion.uploadProfile
import com.canopas.feature_community_data.TestUtils.Companion.userCommunity
import com.canopas.feature_community_data.di.CommunityCache
import com.canopas.feature_community_data.model.AddCommunity
import com.canopas.feature_community_data.rest.CommunityService
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.MainCoroutineRule
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.io.File

@ExperimentalCoroutinesApi
class CommunityInfoViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: CommunityInfoViewModel
    private val resources = mock<Resources>()
    private val services = mock<CommunityService>()

    private val navManager = mock<CommunityNavManager>()

    private val appAnalytics = mock<AppAnalytics>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    private val communityProfileImage = mock<File>()
    private val authManager = mock<AuthManager>()
    private val communityCache = mock<CommunityCache>()

    private fun initViewModel() = runTest {
        viewModel = CommunityInfoViewModel(
            services,
            testDispatcher,
            navManager,
            authManager,
            communityCache,
            appAnalytics,
            resources
        )
    }

    @Before
    fun setup() {
        initViewModel()
    }

    @Test
    fun test_display_community_Info_loading_state_fromCache() {
        val loadingState = InfoState.LOADING
        whenever(communityCache.getCommunity(community.id.toString())).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            community
        }
        viewModel.fetchCommunityInfo("1", "1")
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_display_community_info_success_state_fromCache() = runTest {
        whenever(communityCache.getCommunity(community.id.toString())).thenReturn(community)
        viewModel.fetchCommunityInfo("1", "1")
        val successState = InfoState.SUCCESS(community, community.users.first { it.user_id == 1 })
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_display_community_info_loading_state() = runTest {
        val loadingState = InfoState.LOADING
        whenever(communityCache.getCommunity(community.id.toString())).thenReturn(null)
        whenever(services.getCommunity(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(community)
        }
        viewModel.fetchCommunityInfo("1", "1")
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_display_community_info_success_state() = runTest {
        whenever(communityCache.getCommunity(community.id.toString())).thenReturn(null)
        whenever(services.getCommunity(1)).thenReturn(Result.success(community))
        viewModel.fetchCommunityInfo("1", "1")
        val successState = InfoState.SUCCESS(community, community.users.first { it.user_id == 1 })
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_display_community_info_failure_state() = runTest {
        whenever(communityCache.getCommunity(community.id.toString())).thenReturn(null)
        whenever(services.getCommunity(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.fetchCommunityInfo("1", "1")
        val failureState = InfoState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_success_state_of_leave_community() = runTest {
        val leaveState = InfoState.LEAVE
        val jsonElement = mock<JsonElement>()
        whenever(services.leaveCommunity(1)).thenReturn(Result.success(jsonElement))
        viewModel.leaveCommunity(1)
        Assert.assertEquals(leaveState, viewModel.state.value)
    }

    @Test
    fun test_failure_state_of_leave_community() = runTest {
        whenever(services.leaveCommunity(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.leaveCommunity(1)
        val failureState = InfoState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun testSuccess_updateCommunity() = runTest {
        val communityDetails = AddCommunity(
            name = community2.name,
            description = "",
            image_url = community2.image_url
        )
        whenever(
            services.updateCommunity(
                1,
                communityDetails
            )
        ).thenReturn(Result.success(community2))
        val successState = InfoState.SUCCESS(community2, userCommunity)
        initViewModel()
        Assert.assertEquals(community2, successState.community)
    }

    @Test
    fun testSuccess_uploadCommunityProfile() {
        runTest {
            val communityDetails = AddCommunity(
                name = community2.name,
                description = "",
                image_url = community2.image_url
            )
            whenever(services.getCommunity(1)).thenReturn(Result.success(community2))
            whenever(communityProfileImage.exists()).thenReturn(true)
            whenever(communityProfileImage.name).thenReturn("file.jpg")
            whenever(services.uploadCommunityProfile(any())).thenReturn(Result.success(uploadProfile))
            whenever(services.updateCommunity(1, communityDetails))
                .thenReturn(Result.success(community2))
            viewModel.uploadCommunityImage(communityProfileImage, userCommunity.user_id.toString())
            val successState = InfoState.SUCCESS(community2, userCommunity)
            initViewModel()
            Assert.assertEquals(community2, successState.community)
        }
    }

    @Test
    fun testFail_uploadCommunityProfile() {
        runTest {
            val failureState = InfoState.FAILURE("Error")
            val communityDetails = AddCommunity(
                name = community.name,
                description = "",
                image_url = community.image_url
            )
            whenever(services.getCommunity(1)).thenReturn(Result.success(community))
            whenever(communityProfileImage.exists()).thenReturn(true)
            whenever(communityProfileImage.name).thenReturn("file.jpg")
            whenever(services.uploadCommunityProfile(any())).thenReturn(
                Result.failure(
                    RuntimeException("Error")
                )
            )
            whenever(services.updateCommunity(1, communityDetails))
                .thenReturn(Result.success(community))
            viewModel.uploadCommunityImage(communityProfileImage, userCommunity.user_id.toString())
            Assert.assertEquals(failureState, viewModel.state.value)
        }
    }

    @Test
    fun test_popBackStack() {
        viewModel.managePopBack()
        verify(navManager).popBackStack()
    }

    @Test
    fun test_navigateToChangeName_screen() {
        viewModel.navigateToChangeNameScreen("1", "Canopas")
        verify(navManager).navigateToAddCommunityWithId("1", "Canopas")
    }

    @Test
    fun test_navigateToAddMember_screen() {
        viewModel.navigateToAddMembers(1)
        verify(navManager).navigateToAddCommunityMembers(1, false)
    }

    @Test
    fun test_navigateToShareActivities_screen() {
        viewModel.navigateToShareActivities("1")
        verify(navManager).navigateToShareSubscriptions(1, authManager.currentUser?.id.toString())
    }

    @Test
    fun test_open_leave_community_dialog_whenUser_click_onLeaveCommunity() {
        viewModel.openLeaveCommunityDialog()
        Assert.assertEquals(true, viewModel.openLeaveCommunityDialog.value.getContentIfNotHandled())
    }

    @Test
    fun test_close_sign_out_dialog_whenDismissDialog() {
        viewModel.closeLeaveCommunityDialog()
        Assert.assertEquals(
            false,
            viewModel.openLeaveCommunityDialog.value.getContentIfNotHandled()
        )
    }

    @Test
    fun test_open_community_profile_chooser_dialog() {
        viewModel.openCommunityImageChanger()
        Assert.assertEquals(
            true,
            viewModel.openCommunityImageChooserEvent.value.getContentIfNotHandled()
        )
    }

    @Test
    fun test_navigateToCommunityListScreen() = runTest {
        val jsonElement = mock<JsonElement>()
        whenever(services.leaveCommunity(1)).thenReturn(Result.success(jsonElement))
        viewModel.leaveCommunity(1)
        verify(navManager).navigateToCommunityListScreen()
    }

    @Test
    fun test_removeCommunityProfile() = runTest {
        val communityDetails = AddCommunity(
            name = community.name,
            description = "",
            image_url = community.image_url
        )
        whenever(services.getCommunity(1)).thenReturn(Result.success(community))
        whenever(communityProfileImage.exists()).thenReturn(true)
        whenever(communityProfileImage.name).thenReturn("file.jpg")
        whenever(services.uploadCommunityProfile(any())).thenReturn(Result.success(uploadProfile))
        whenever(services.updateCommunity(1, communityDetails))
            .thenReturn(Result.success(community))
        viewModel.removeCommunityProfile(userCommunity.user_id.toString())
        val successState = InfoState.SUCCESS(community, userCommunity)
        initViewModel()
        Assert.assertEquals(community, successState.community)
    }
}
