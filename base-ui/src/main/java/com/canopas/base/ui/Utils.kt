package com.canopas.base.ui

import android.content.Context
import androidx.annotation.StringRes
import androidx.compose.ui.graphics.Color
import com.canopas.base.data.model.DEFAULT_RECENT_PROGRESS
import com.canopas.base.data.model.GREEN_PROGRESS
import com.canopas.base.data.model.Progress
import com.canopas.base.data.model.Recent
import com.canopas.base.data.model.RecentDaysHistory
import com.canopas.base.data.model.SubscriptionCardDetails
import com.canopas.base.data.model.UNIX_TO_DATE_MULTIPLIER
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale
import java.util.concurrent.TimeUnit
import kotlin.math.roundToInt

class Utils {

    companion object {

        val format by lazy { SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()) }
        val headerFormat by lazy { SimpleDateFormat("MMMM yyyy", Locale.getDefault()) }
        val allTimeFormat by lazy { SimpleDateFormat("MMM yyyy", Locale.getDefault()) }
        val monthFormat by lazy { SimpleDateFormat("MMMM", Locale.getDefault()) }
        val summaryFormat by lazy { SimpleDateFormat("EEEE, MMMM dd yyyy", Locale.getDefault()) }
        val sortDateFormat by lazy { SimpleDateFormat("EEE, MMMM dd", Locale.getDefault()) }
        val yearFormat by lazy { SimpleDateFormat("yyyy", Locale.getDefault()) }
        val sixMonthlyFormat by lazy { SimpleDateFormat("MMM", Locale.getDefault()) }
        val goalsDueDateFormat by lazy { SimpleDateFormat("dd MMM", Locale.getDefault()) }
        val myGoalDueDateFormat by lazy { SimpleDateFormat("MMM, dd", Locale.getDefault()) }
        val dueDateFormat by lazy { SimpleDateFormat("MMM dd, yyyy", Locale.getDefault()) }

        fun DateFormat.parseOrNull(source: String): Date? {
            return try {
                this.parse(source)
            } catch (e: ParseException) {
                null
            }
        }

        private fun textResource(
            context: Context,
            @StringRes id: Int,
            vararg formatArgs: Any
        ): String {
            return context.resources.getString(id, *formatArgs)
        }

        fun getSubscriptionCardDetails(
            context: Context,
            progress: Progress,
            isDarkMode: Boolean
        ): SubscriptionCardDetails {
            val totalItems = progress.recent.size
            val completedItems = progress.recent.filter { it.completed }.size
            val recentProgress =
                if (progress.recent.isNotEmpty()) (completedItems * 100 / totalItems) else DEFAULT_RECENT_PROGRESS
            val recentDaysCount =
                if (progress.recent.size > 1) textResource(
                    context,
                    R.string.subscription_recent_days_count_text,
                    progress.recent.size
                ) else textResource(
                    context,
                    R.string.subscription_recent_day_count_text,
                    progress.recent.size
                )
            val cardBgColor: Color
            val progressTextColor: Color
            when {
                recentProgress > GREEN_PROGRESS || progress.recent.isEmpty() -> {
                    cardBgColor = if (isDarkMode) GreenSubscriptionCardBgDark else GreenSubscriptionCardBg
                    progressTextColor = if (isDarkMode) GreenSubscriptionCardBg else GreenProgressTextColor
                }
                recentProgress in 50..80 -> {
                    cardBgColor = if (isDarkMode) YellowSubscriptionCardBgDark else YellowSubscriptionCardBg
                    progressTextColor = if (isDarkMode)YellowSubscriptionCardBg else YellowProgressTextColor
                }
                else -> {
                    cardBgColor = if (isDarkMode) RedSubscriptionCardBgDark else RedSubscriptionCardBg
                    progressTextColor = if (isDarkMode) RedSubscriptionCardBg else RedProgressTextColor
                }
            }
            val chartTitleHeading: String
            val chartTitleBody: String
            when {
                progress.recent.isEmpty() -> {
                    chartTitleHeading =
                        context.getString(R.string.subscription_status_chart_title_just_started_text)
                    chartTitleBody = ""
                }
                recentProgress >= GREEN_PROGRESS -> {
                    chartTitleHeading =
                        context.getString(R.string.subscription_status_chart_title_amazing_heading_text)
                    chartTitleBody =
                        context.getString(R.string.subscription_status_chart_title_amazing_body_text)
                }
                recentProgress in 50..79 -> {
                    chartTitleHeading =
                        context.getString(R.string.subscription_status_chart_title_keep_pushing_heading_text)
                    chartTitleBody =
                        context.getString(R.string.subscription_status_chart_title_keep_pushing_body_text)
                }
                else -> {
                    chartTitleHeading =
                        context.getString(R.string.subscription_status_chart_title_dont_give_up_heading_text)
                    chartTitleBody =
                        context.getString(R.string.subscription_status_chart_title_dont_give_up_body_text)
                }
            }
            return SubscriptionCardDetails(
                recentProgress,
                recentDaysCount,
                cardBgColor,
                progressTextColor,
                chartTitleHeading,
                chartTitleBody
            )
        }

        fun getSubscriptionArchivedCardDetails(
            progress: Progress,
            isDarkMode: Boolean
        ): Triple<Int, Color, Color> {
            val totalProgress = progress.total.progress.roundToInt()
            val cardBgColor: Color
            val progressTextColor: Color
            when {
                totalProgress < 1 && progress.recent.isNotEmpty() -> {
                    cardBgColor = if (isDarkMode) GreenSubscriptionCardBgDark else GreenSubscriptionCardBg
                    progressTextColor = if (isDarkMode) GreenSubscriptionCardBg else GreenProgressTextColor
                }
                totalProgress > GREEN_PROGRESS || progress.recent.isEmpty() -> {
                    cardBgColor = if (isDarkMode) GreenSubscriptionCardBgDark else GreenSubscriptionCardBg
                    progressTextColor = if (isDarkMode) GreenSubscriptionCardBg else GreenProgressTextColor
                }
                totalProgress in 50..80 -> {
                    cardBgColor = if (isDarkMode) YellowSubscriptionCardBgDark else YellowSubscriptionCardBg
                    progressTextColor = if (isDarkMode)YellowSubscriptionCardBg else YellowProgressTextColor
                }
                totalProgress in 1..49 -> {
                    cardBgColor = if (isDarkMode) RedSubscriptionCardBgDark else RedSubscriptionCardBg
                    progressTextColor = if (isDarkMode) RedSubscriptionCardBg else RedProgressTextColor
                }
                else -> {
                    cardBgColor = if (isDarkMode) GreenSubscriptionCardBgDark else GreenSubscriptionCardBg
                    progressTextColor = if (isDarkMode) GreenSubscriptionCardBg else GreenProgressTextColor
                }
            }
            return Triple(totalProgress, cardBgColor, progressTextColor)
        }

        fun getCurrentWeekProgress(dateList: List<Recent>): List<RecentDaysHistory> {
            val currentWeekProgress: ArrayList<RecentDaysHistory> = ArrayList(emptyList())
            dateList.forEachIndexed { index, recent ->
                val day = SimpleDateFormat(
                    "EEE",
                    Locale.ENGLISH
                ).format(recent.date * UNIX_TO_DATE_MULTIPLIER)
                val currentDay =
                    SimpleDateFormat("EEE", Locale.ENGLISH).format(System.currentTimeMillis())
                val isCurrentDay = currentDay == day && index == dateList.lastIndex
                val icon: Int
                val tintColor: Color
                when {
                    recent.completed -> {
                        icon = R.drawable.ic_baseline_check_circle
                        tintColor = subscriptionStatusDaysCompletedProgressColor
                    }
                    else -> {
                        icon = R.drawable.ic_baseline_check_circle
                        tintColor = subscriptionStatusDaysUnCompletedProgressColor.copy(0.3f)
                    }
                }
                currentWeekProgress.add(
                    RecentDaysHistory(
                        day.substring(0, 3),
                        recent.completed,
                        isCurrentDay,
                        icon,
                        tintColor
                    )
                )
            }
            return currentWeekProgress
        }

        fun getCurrentWeekDay(dateList: List<Recent>): List<Int> {
            val currentWeekDate: ArrayList<Int> = ArrayList(emptyList())
            val calender = Calendar.getInstance()
            dateList.forEachIndexed { _, recent ->
                val weekdayNumber = Date(recent.date * UNIX_TO_DATE_MULTIPLIER)
                calender.time = weekdayNumber
                val date = calender.get(Calendar.DAY_OF_MONTH)
                currentWeekDate.add(date)
            }

            return currentWeekDate
        }

        fun getFormattedStartAndEndDate(startDate: Long, endDate: Long): String {
            val withYearFormat = SimpleDateFormat("dd MMM yyyy", Locale.getDefault())
            val withoutYearFormat = SimpleDateFormat("dd MMM", Locale.getDefault())
            val startDateMillis = TimeUnit.SECONDS.toMillis(startDate)
            val endDateMillis = TimeUnit.SECONDS.toMillis(endDate)
            val startDateWithYear = withYearFormat.format(startDateMillis)
            val endDateWithYear = withYearFormat.format(endDateMillis)

            val dateStart = Date(startDateMillis)
            val dateEnd = Date(endDateMillis)
            val calendar = Calendar.getInstance().apply { time = dateStart }
            val calendar1 = Calendar.getInstance().apply { time = dateEnd }
            val startYear = calendar.get(Calendar.YEAR)
            val endYear = calendar1.get(Calendar.YEAR)

            return if (startDateWithYear == endDateWithYear || endDateMillis == 0L) {
                startDateWithYear
            } else if (startYear == endYear) {
                val startDateWithoutYear = withoutYearFormat.format(startDateMillis)
                "$startDateWithoutYear - $endDateWithYear"
            } else {
                "$startDateWithYear - $endDateWithYear"
            }
        }
    }
}

private val GreenSubscriptionCardBg = Color(0xFFE6F0DE)
private val GreenSubscriptionCardBgDark = Color(0xFF47A96E)
private val GreenProgressTextColor = Color(0xFF5A9B26)
private val RedSubscriptionCardBg = Color(0xFFFBE8E8)
private val RedSubscriptionCardBgDark = Color(0xFFF67878)
private val RedProgressTextColor = Color(0xFFD61A1A)
private val YellowSubscriptionCardBg = Color(0xFFFBF4E1)
private val YellowSubscriptionCardBgDark = Color(0xFFDFAA1C)
private val YellowProgressTextColor = Color(0xFFD2A123)
private val subscriptionStatusDaysCompletedProgressColor = Color(0xFF47A96E)
private val subscriptionStatusDaysUnCompletedProgressColor = Color(0xFFF67C37)
