package com.canopas.base.ui

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.canopas.base.ui.themes.ComposeTheme.colors

@Composable
fun ProfileImageView(
    data: String?,
    modifier: Modifier = Modifier,
    char: String?
) {
    if (!data.isNullOrEmpty()) {
        Image(
            painter = rememberAsyncImagePainter(
                ImageRequest.Builder(LocalContext.current).data(
                    data = data
                ).build()
            ),
            modifier = modifier
                .clip(CircleShape),
            contentScale = ContentScale.Crop,
            contentDescription = "ProfileImage"
        )
    } else {
        BoxWithConstraints(
            modifier = modifier
                .clip(CircleShape)
                .background(colors.background)
                .fillMaxWidth(),
            contentAlignment = Alignment.Center
        ) {
            if (char != null) {
                Text(
                    text = char,
                    style = AppTheme.typography.profileTextCharStyle.copy(
                        fontSize = (maxWidth.value + maxHeight.value).times(0.20).sp
                    ),
                    color = colors.textSecondary,
                    textAlign = TextAlign.Center
                )
            }
        }
    }
}
