package com.canopas.base.ui.themes

import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

private val LightColorPalette = ComposeColors(
    background = Color(0xffffffff),
    primary = Color(0xFFF67C37),
    textPrimary = Color(0xDE000000),
    textSecondary = Color(0x99000000)
)

private val DarkColorPalette = ComposeColors(
    background = Color(0XFF121212),
    primary = Color(0xFFF67C37),
    textPrimary = Color(0xffffffff),
    textSecondary = Color(0xCCFFFFFF)
)

@Composable
fun JustlyAppTheme(
    isDarkMode: Boolean,
    content: @Composable () -> Unit
) {
    MaterialTheme(colors = if (isDarkMode) darkColors() else lightColors()) {
        val colors = if (isDarkMode) DarkColorPalette else LightColorPalette
        CompositionLocalProvider(
            LocalJustlyColors provides colors,
            LocalDarkMode provides isDarkMode,
            content = content
        )
    }
}

object ComposeTheme {
    val colors: ComposeColors
        @Composable
        get() = LocalJustlyColors.current
    val isDarkMode: Boolean
        @Composable
        get() = LocalDarkMode.current
}

private val LocalJustlyColors = staticCompositionLocalOf<ComposeColors> {
    error("No ComposeColorPalette provided")
}

private val LocalDarkMode = staticCompositionLocalOf {
    false
}

data class ComposeColors(
    val background: Color,
    val textPrimary: Color,
    val primary: Color,
    val textSecondary: Color
)
