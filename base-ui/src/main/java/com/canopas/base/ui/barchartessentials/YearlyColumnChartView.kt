package com.canopas.base.ui.barchartessentials

import android.graphics.RectF
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.gestures.rememberScrollableState
import androidx.compose.foundation.interaction.DragInteraction
import androidx.compose.foundation.interaction.Interaction
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.nativeCanvas
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColumnGraphBg
import com.canopas.base.ui.R
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.patrykandpatryk.vico.compose.axis.axisLineComponent
import com.patrykandpatryk.vico.compose.axis.horizontal.bottomAxis
import com.patrykandpatryk.vico.compose.axis.vertical.startAxis
import com.patrykandpatryk.vico.compose.chart.column.columnChart
import com.patrykandpatryk.vico.compose.chart.scroll.ChartScrollSpec
import com.patrykandpatryk.vico.compose.chart.scroll.rememberChartScrollSpec
import com.patrykandpatryk.vico.compose.component.lineComponent
import com.patrykandpatryk.vico.compose.component.textComponent
import com.patrykandpatryk.vico.compose.layout.getMeasureContext
import com.patrykandpatryk.vico.compose.style.ChartStyle
import com.patrykandpatryk.vico.compose.style.ProvideChartStyle
import com.patrykandpatryk.vico.compose.style.currentChartStyle
import com.patrykandpatryk.vico.core.DEF_LABEL_COUNT
import com.patrykandpatryk.vico.core.DefaultDimens
import com.patrykandpatryk.vico.core.axis.Axis
import com.patrykandpatryk.vico.core.axis.AxisManager
import com.patrykandpatryk.vico.core.axis.AxisPosition
import com.patrykandpatryk.vico.core.axis.AxisRenderer
import com.patrykandpatryk.vico.core.axis.formatter.AxisValueFormatter
import com.patrykandpatryk.vico.core.axis.horizontal.HorizontalAxis
import com.patrykandpatryk.vico.core.axis.horizontal.createHorizontalAxis
import com.patrykandpatryk.vico.core.axis.vertical.VerticalAxis
import com.patrykandpatryk.vico.core.axis.vertical.createVerticalAxis
import com.patrykandpatryk.vico.core.chart.Chart
import com.patrykandpatryk.vico.core.chart.decoration.ThresholdLine
import com.patrykandpatryk.vico.core.chart.draw.chartDrawContext
import com.patrykandpatryk.vico.core.chart.draw.drawMarker
import com.patrykandpatryk.vico.core.chart.draw.getMaxScrollDistance
import com.patrykandpatryk.vico.core.chart.edges.FadingEdges
import com.patrykandpatryk.vico.core.chart.scale.AutoScaleUp
import com.patrykandpatryk.vico.core.component.shape.LineComponent
import com.patrykandpatryk.vico.core.component.shape.ShapeComponent
import com.patrykandpatryk.vico.core.component.shape.Shapes
import com.patrykandpatryk.vico.core.entry.ChartEntryModel
import com.patrykandpatryk.vico.core.entry.entryModelOf
import com.patrykandpatryk.vico.core.extension.copyColor
import com.patrykandpatryk.vico.core.extension.set
import com.patrykandpatryk.vico.core.layout.VirtualLayout
import com.patrykandpatryk.vico.core.legend.Legend
import com.patrykandpatryk.vico.core.marker.Marker
import com.patrykandpatryk.vico.core.marker.MarkerVisibilityChangeListener
import com.patrykandpatryk.vico.core.model.Point
import com.patrykandpatryk.vico.core.scroll.ScrollHandler

private const val THRESHOLD_RANGE_START = 0f
private const val THRESHOLD_RANGE_END = 0f
private const val THRESHOLD_LINE_COLOR = 0xFFFFFFFF
private const val THRESHOLD_LINE_ALPHA = 0.07f
private const val MAX_LABEL_COUNT = 5
private const val EMPTY_LABEL_COUNT = 1
private val entityColors = longArrayOf(0xFF47A96E)

@Composable
fun YearlyColumnChartView(barChartData: List<Pair<Float, String>>, isLoadingState: Boolean) {

    val months = barChartData.map {
        it.second
    }
    val eachMonthValues = barChartData.map {
        it.first
    }

    val xAxisValueFormatter: AxisValueFormatter<AxisPosition.Horizontal.Bottom> =
        AxisValueFormatter { x, _ ->
            months[x.toInt() % months.size]
        }

    val entryModel = entryModelOf(*eachMonthValues.toTypedArray())

    val chartStyle = ChartStyle.fromColors(
        axisLabelColor = colors.textPrimary.copy(0.45f),
        axisGuidelineColor = colors.textPrimary.copy(0.07f),
        axisLineColor = colors.textPrimary.copy(0.07f),
        entityColors = listOf(ColumnGraphBg),
        elevationOverlayColor = colors.background,
    )
    val decorations = listOf(rememberGroupedColumnChartThresholdLine())
    ProvideChartStyle(chartStyle = chartStyle) {
        val columnChart = columnChart(
            spacing = 8.dp,
            decorations = decorations,
            columns = entityColors.map {
                lineComponent(
                    color = Color(it),
                    thickness = 15.dp,
                    shape = Shapes.roundedCornerShape(
                        topRightPercent = 20,
                        topLeftPercent = 20
                    ),
                )
            },
        )

        AnimatedVisibility(visible = barChartData.maxOf { it.first.toInt() } == 0 && !isLoadingState) {
            Box(modifier = Modifier.fillMaxWidth().padding(horizontal = 40.dp), contentAlignment = Alignment.Center) {
                Text(
                    text = stringResource(R.string.subscription_status_no_completion_text),
                    color = colors.textPrimary.copy(0.45f),
                    style = AppTheme.typography.bodyTextStyle2,
                    textAlign = TextAlign.Center
                )
            }
        }
        Chart(
            chart = columnChart,
            model = entryModel,
            topAxis = customTopAxis(),
            startAxis = if (barChartData.maxOf { it.first.toInt() } != 0) {
                startAxis(
                    valueFormatter = yAxisFormatter,
                    maxLabelCount = when {
                        barChartData.maxOf { it.first } < MAX_LABEL_COUNT -> barChartData.maxOf { it.first }.toInt()
                        else -> MAX_LABEL_COUNT
                    },
                )
            } else startAxis(maxLabelCount = EMPTY_LABEL_COUNT),
            chartScrollSpec = rememberChartScrollSpec(isScrollEnabled = false),
            endAxis = customEndAxis(),
            bottomAxis = bottomAxis(valueFormatter = xAxisValueFormatter, tickLength = months.size.dp),
            marker = marker()
        )
    }
}

@Composable
fun <Model : ChartEntryModel> Chart(
    chart: Chart<Model>,
    model: Model,
    modifier: Modifier = Modifier,
    startAxis: AxisRenderer<AxisPosition.Vertical.Start>? = null,
    topAxis: AxisRenderer<AxisPosition.Horizontal.Top>? = null,
    endAxis: AxisRenderer<AxisPosition.Vertical.End>? = null,
    bottomAxis: AxisRenderer<AxisPosition.Horizontal.Bottom>? = null,
    marker: Marker? = null,
    markerVisibilityChangeListener: MarkerVisibilityChangeListener? = null,
    legend: Legend? = null,
    chartScrollSpec: ChartScrollSpec<Model> = rememberChartScrollSpec(),
    oldModel: Model? = null,
    fadingEdges: FadingEdges? = null,
    autoScaleUp: AutoScaleUp = AutoScaleUp.Full,
) {
    ChartBox(modifier = modifier) {
        ChartImpl(
            chart = chart,
            model = model,
            startAxis = startAxis,
            topAxis = topAxis,
            endAxis = endAxis,
            bottomAxis = bottomAxis,
            marker = marker,
            markerVisibilityChangeListener = markerVisibilityChangeListener,
            legend = legend,
            chartScrollSpec = chartScrollSpec,
            oldModel = oldModel,
            fadingEdges = fadingEdges,
            autoScaleUp = autoScaleUp,
        )
    }
}

@Composable
fun <Model : ChartEntryModel> ChartImpl(
    chart: Chart<Model>,
    model: Model,
    startAxis: AxisRenderer<AxisPosition.Vertical.Start>?,
    topAxis: AxisRenderer<AxisPosition.Horizontal.Top>?,
    endAxis: AxisRenderer<AxisPosition.Vertical.End>?,
    bottomAxis: AxisRenderer<AxisPosition.Horizontal.Bottom>?,
    marker: Marker?,
    markerVisibilityChangeListener: MarkerVisibilityChangeListener?,
    legend: Legend?,
    chartScrollSpec: ChartScrollSpec<Model>,
    oldModel: Model? = null,
    fadingEdges: FadingEdges?,
    autoScaleUp: AutoScaleUp,
) {
    val axisManager = remember { AxisManager() }
    val bounds = remember { RectF() }
    val markerTouchPoint = remember { mutableStateOf<Point?>(null) }
    val horizontalScroll = remember { mutableStateOf(0f) }
    val zoom = remember { mutableStateOf(1f) }
    val measureContext = getMeasureContext(chartScrollSpec.isScrollEnabled, zoom.value, bounds)
    val interactionSource = remember { MutableInteractionSource() }
    val interaction = interactionSource.interactions.collectAsState(initial = null)

    axisManager.setAxes(startAxis, topAxis, endAxis, bottomAxis)

    val setHorizontalScroll = rememberSetHorizontalScroll(horizontalScroll, markerTouchPoint, interaction)
    val scrollHandler = remember { ScrollHandler(setHorizontalScroll) }
    val scrollableState = rememberScrollableState(scrollHandler::handleScrollDelta)
    val virtualLayout = remember { VirtualLayout(axisManager) }
    val elevationOverlayColor = currentChartStyle.elevationOverlayColor.toArgb()

    val (wasMarkerVisible, setWasMarkerVisible) = remember { mutableStateOf(false) }

    LaunchedEffect(key1 = model.id) {
        chartScrollSpec.performAutoScroll(
            model = model,
            oldModel = oldModel,
            currentScroll = horizontalScroll.value,
            maxScrollDistance = scrollHandler.maxScrollDistance,
            scrollableState = scrollableState,
        )
    }

    Canvas(
        modifier = Modifier
            .fillMaxSize()
            .chartTouchEvent(
                setTouchPoint = markerTouchPoint
                    .component2()
                    .takeIf { marker != null },
            ),
    ) {
        bounds.set(left = 0, top = 0, right = size.width, bottom = size.height)
        chart.updateChartValues(measureContext.chartValuesManager, model)

        val segmentProperties = chart.getSegmentProperties(measureContext, model)

        virtualLayout.setBounds(
            context = measureContext,
            contentBounds = bounds,
            chart = chart,
            legend = legend,
            segmentProperties = segmentProperties,
            marker,
        )

        scrollHandler.maxScrollDistance = measureContext.getMaxScrollDistance(
            chartWidth = chart.bounds.width(),
            segmentProperties = segmentProperties,
        )

        scrollHandler.handleInitialScroll(initialScroll = chartScrollSpec.initialScroll)

        val chartDrawContext = chartDrawContext(
            canvas = drawContext.canvas.nativeCanvas,
            elevationOverlayColor = elevationOverlayColor,
            measureContext = measureContext,
            markerTouchPoint = markerTouchPoint.value,
            segmentProperties = segmentProperties,
            chartBounds = chart.bounds,
            horizontalScroll = horizontalScroll.value,
            autoScaleUp = autoScaleUp,
        )

        val count = if (fadingEdges != null) chartDrawContext.saveLayer() else -1

        axisManager.drawBehindChart(chartDrawContext)
        chart.drawScrollableContent(chartDrawContext, model)

        fadingEdges?.apply {
            applyFadingEdges(chartDrawContext, chart.bounds)
            chartDrawContext.restoreCanvasToCount(count)
        }

        chart.drawNonScrollableContent(chartDrawContext, model)
        axisManager.drawAboveChart(chartDrawContext)
        legend?.draw(chartDrawContext)

        if (marker != null) {
            chartDrawContext.drawMarker(
                marker = marker,
                markerTouchPoint = markerTouchPoint.value,
                chart = chart,
                markerVisibilityChangeListener = markerVisibilityChangeListener,
                wasMarkerVisible = wasMarkerVisible,
                setWasMarkerVisible = setWasMarkerVisible,
            )
        }

        measureContext.reset()
    }
}

fun Modifier.chartTouchEvent(
    setTouchPoint: ((Point?) -> Unit)?
): Modifier = addIfNotNull(setTouchPoint) { setPoint ->
    pointerInput(Unit, Unit) {
        detectTapGestures(
            onPress = {
                setPoint(it.point)
                awaitRelease()
                setPoint(null)
            },
        )
    }
}

val Offset.point: Point
    get() = Point(x, y)

inline fun <T> Modifier.addIfNotNull(
    value: T?,
    crossinline factory: Modifier.(T) -> Modifier,
): Modifier = if (value != null) factory(value) else this

@Composable
fun ChartBox(
    modifier: Modifier,
    content: @Composable BoxScope.() -> Unit,
) {
    Box(
        modifier = modifier.height(DefaultDimens.CHART_HEIGHT.dp),
        content = content,
    )
}

@Composable
fun rememberSetHorizontalScroll(
    scroll: MutableState<Float>,
    touchPoint: MutableState<Point?>,
    interaction: State<Interaction?>,
): (Float) -> Unit = remember {
    var canClearTouchPoint = false
    return@remember { newScroll: Float ->
        touchPoint.value?.let { point ->
            if (interaction.value is DragInteraction.Stop && canClearTouchPoint) {
                touchPoint.value = null
                canClearTouchPoint = false
            } else {
                touchPoint.value = point.copy(x = point.x + scroll.value - newScroll)
                canClearTouchPoint = true
            }
        }
        scroll.value = newScroll
    }
}

@Composable
fun rememberGroupedColumnChartThresholdLine(): ThresholdLine {
    val labelComponent = textComponent(
        color = Color.Transparent,
        textSize = 0.sp
    )
    return remember(labelComponent) {
        ThresholdLine(
            thresholdRange = THRESHOLD_RANGE_START..THRESHOLD_RANGE_END,
            labelComponent = labelComponent,
            lineComponent = ShapeComponent(
                color = THRESHOLD_LINE_COLOR
                    .toInt().copyColor(THRESHOLD_LINE_ALPHA)
            )
        )
    }
}

@Composable
fun customEndAxis(
    axis: LineComponent? = axisLineComponent(),
    sizeConstraint: Axis.SizeConstraint = Axis.SizeConstraint.Auto(),
    maxLabelCount: Int = DEF_LABEL_COUNT,
): VerticalAxis<AxisPosition.Vertical.End> = createVerticalAxis {
    this.axis = axis
    this.sizeConstraint = sizeConstraint
    this.maxLabelCount = maxLabelCount
}

@Composable
fun customTopAxis(
    axis: LineComponent? = axisLineComponent(),
    sizeConstraint: Axis.SizeConstraint = Axis.SizeConstraint.Auto(),
): HorizontalAxis<AxisPosition.Horizontal.Top> = createHorizontalAxis {
    this.axis = axis
    this.sizeConstraint = sizeConstraint
}

val yAxisFormatter =
    AxisValueFormatter<AxisPosition.Vertical.Start> { value, _ ->
        (value.toInt()).toString()
    }
