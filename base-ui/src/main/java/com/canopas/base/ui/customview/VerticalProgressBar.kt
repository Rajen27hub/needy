package com.canopas.base.ui.customview

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp

@Composable
fun VerticalProgressBar(
    modifier: Modifier = Modifier,
    indicatorHeight: Dp = 70.dp,
    indicatorWidth: Dp,
    backgroundIndicatorColor: Color = highlightsProgressBg.copy(alpha = 0.87f),
    progressColor: Color,
    progress: Float
) {
    Column(
        modifier = Modifier
            .width(indicatorWidth)
            .height(indicatorHeight)
    ) {
        Canvas(
            modifier = modifier
                .clip(
                    shape = RoundedCornerShape(8.dp)
                )
        ) {
            val barMaxValue = size.height
            val offsetValue = ((progress * barMaxValue) / 100)
            drawRect(
                color = backgroundIndicatorColor,
                topLeft = Offset(x = 0f, y = 0f),
                size = size
            )
            drawRect(
                color = progressColor,
                topLeft = Offset(x = 0f, y = barMaxValue - offsetValue),
            )
        }
    }
}

@Preview
@Composable
fun PreviewVerticalProgressView() {
    VerticalProgressBar(progressColor = highlightsBorderBg, progress = 5F, indicatorWidth = 40.dp)
}

private val highlightsBorderBg = Color(0xFFE2972A)
private val highlightsProgressBg = Color(0xFFCFD5D1)
