package com.canopas.base.ui.model

import android.content.Context
import com.canopas.base.ui.R

enum class ProgressTabType {
    PROGRESS_7D, PROGRESS_M, PROGRESS_6M, PROGRESS_Y, PROGRESS_ALL;

    fun getTitle(context: Context): String {
        return when (this) {
            PROGRESS_7D -> context.getString(R.string.subscription_status_progress_7_days_title_text)
            PROGRESS_M -> context.getString(R.string.subscription_status_progress_month_title_text)
            PROGRESS_6M -> context.getString(R.string.subscription_status_progress_6_months_title_text)
            PROGRESS_Y -> context.getString(R.string.subscription_status_progress_year_title_text)
            PROGRESS_ALL -> context.getString(R.string.subscription_status_all_title_text)
        }
    }
}

enum class DurationTabType(val duration: Int) {
    DURATION_1_MIN(1), DURATION_15_MIN(15), DURATION_30_MIN(30), DURATION_45_MIN(45), DURATION_1_HOUR(60);
}

enum class RepeatTabStyle {
    REPEAT_DAILY, REPEAT_WEEKLY, REPEAT_MONTHLY;

    fun getTitle(context: Context): String {
        return when (this) {
            REPEAT_DAILY -> context.getString(R.string.configure_activity_repeat_tab_daily_text)
            REPEAT_WEEKLY -> context.getString(R.string.configure_activity_repeat_tab_weekly_text)
            REPEAT_MONTHLY -> context.getString(R.string.configure_activity_repeat_tab_monthly_text)
        }
    }
}
