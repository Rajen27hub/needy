package com.canopas.base.ui

import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.tween
import androidx.compose.animation.core.updateTransition
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.graphics.drawscope.Stroke
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.themes.ComposeTheme.colors
import kotlin.math.roundToInt

@Composable
fun CircularGradientProgressView(
    modifier: Modifier = Modifier,
    progressColor: Brush = Brush.linearGradient(
        colors = circularProgressGradient,
        start = Offset(Float.POSITIVE_INFINITY, 0f),
        end = Offset.Zero
    ),
    progressBackgroundColor: Color = ColorPrimary.copy(0.15f),
    strokeWidth: Dp = 12.dp,
    strokeBackgroundWidth: Dp = 3.dp,
    progress: Float,
    showText: Boolean = false,
    boxModifier: Modifier = Modifier.fillMaxSize(),
) {
    val currentState = remember {
        MutableTransitionState(AnimatedArcState.START)
            .apply { targetState = AnimatedArcState.END }
    }
    val animatedProgress = updateTransition(currentState, label = "")
    val sweep = progress * 360 / 100
    val progressAnimation by animatedProgress.animateFloat(
        transitionSpec = {
            tween(
                durationMillis = 600,
                easing = LinearEasing,
                delayMillis = 100
            )
        }, label = ""
    ) { state ->
        when (state) {
            AnimatedArcState.START -> 0f
            AnimatedArcState.END -> sweep
        }
    }

    val stroke = with(LocalDensity.current) {
        Stroke(width = strokeWidth.toPx(), cap = StrokeCap.Round)
    }

    val strokeBackground = with(LocalDensity.current) {
        Stroke(width = strokeBackgroundWidth.toPx())
    }

    Box(
        contentAlignment = Alignment.Center,
        modifier = boxModifier
    ) {

        if (showText) {
            Text(
                text = "${progress.roundToInt()}%",
                color = colors.textPrimary,
                style = AppTheme.typography.h2TextStyle,
            )
        }

        Canvas(
            modifier.fillMaxSize()
        ) {
            val higherStrokeWidth =
                if (stroke.width > strokeBackground.width) stroke.width else strokeBackground.width
            val radius1 = (size.minDimension - higherStrokeWidth) / 2
            val halfSize = size / 2.0f
            val topLeft = Offset(
                halfSize.width - radius1,
                halfSize.height - radius1
            )
            val size = Size(radius1 * 2, radius1 * 2)

            drawArc(
                startAngle = 0f,
                sweepAngle = 360f,
                color = progressBackgroundColor,
                useCenter = false,
                topLeft = topLeft,
                size = size,
                style = strokeBackground
            )

            drawArc(
                brush = progressColor,
                startAngle = 270f,
                sweepAngle = progressAnimation,
                useCenter = false,
                topLeft = topLeft,
                size = size,
                style = stroke
            )
        }
    }
}

private enum class AnimatedArcState {
    START,
    END
}

private val circularProgressGradient =
    listOf(Color(0xFFCA2F27).copy(0.7f), Color(0xFFFFD05E), Color(0xFF47A96E))
