package com.canopas.base.ui.customview

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.White

@Composable
fun PrimaryButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    text: String,
    enabled: Boolean = true,
    shape: Shape = RoundedCornerShape(50),
    colors: ButtonColors = ButtonDefaults.buttonColors(),
    textModifier: Modifier = Modifier,
    isProcessing: Boolean = false,
) {
    Button(
        onClick = { onClick() },
        shape = shape,
        colors = colors,
        elevation = ButtonDefaults.elevation(
            defaultElevation = 0.dp,
            pressedElevation = 0.dp,
            disabledElevation = 0.dp,
            hoveredElevation = 0.dp,
            focusedElevation = 0.dp
        ),
        modifier = modifier
            .wrapContentHeight()
            .padding(horizontal = 20.dp),
        enabled = enabled
    ) {
        Row(
            horizontalArrangement = Arrangement.Center
        ) {
            AnimatedVisibility(isProcessing) {
                CircularProgressIndicator(color = Color.White, modifier = Modifier.padding(end = 12.dp, top = 2.dp, bottom = 2.dp).size(26.dp))
            }
            Text(
                text = text,
                color = White,
                style = AppTheme.typography.buttonStyle,
                modifier = textModifier.padding(vertical = 6.dp)
            )
        }
    }
}

@Preview
@Composable
fun PreviewPrimaryButton() {
    PrimaryButton(onClick = { }, text = "Preview")
}
