package com.canopas.base.ui.customview

import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.TextFieldValue
import com.canopas.base.ui.themes.ComposeTheme.colors

@Composable
fun CustomTextField(
    value: TextFieldValue? = null,
    onValueChange: (TextFieldValue) -> Unit = {},
    text: String? = null,
    onTextChange: (String) -> Unit = {},
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    modifier: Modifier,
    textStyle: TextStyle,
    keyboardActions: KeyboardActions = KeyboardActions.Default,
    keyboardOptions: KeyboardOptions,
    singleLine: Boolean = false,
    maxLines: Int = Int.MAX_VALUE,
    cursorBrush: Brush,
    decorationBox: @Composable (innerTextField: @Composable () -> Unit) -> Unit =
        @Composable { innerTextField -> innerTextField() }
) {
    CompositionLocalProvider(
        LocalTextSelectionColors provides TextSelectionColors(
            colors.primary,
            colors.primary
        )
    ) {
        if (value != null) {
            BasicTextField(
                value = value,
                onValueChange = {
                    if (value != it) {
                        onValueChange(it)
                    }
                },
                interactionSource = interactionSource,
                modifier = modifier,
                textStyle = textStyle,
                keyboardActions = keyboardActions,
                keyboardOptions = keyboardOptions,
                cursorBrush = cursorBrush,
                decorationBox = decorationBox,
                singleLine = singleLine,
                maxLines = if (singleLine) 1 else maxLines
            )
        } else {
            BasicTextField(
                value = text!!,
                onValueChange = { it ->
                    onTextChange(it)
                },
                interactionSource = interactionSource,
                modifier = modifier,
                textStyle = textStyle,
                keyboardActions = keyboardActions,
                keyboardOptions = keyboardOptions,
                cursorBrush = cursorBrush,
                decorationBox = decorationBox,
                singleLine = singleLine,
                maxLines = if (singleLine) 1 else maxLines
            )
        }
    }
}
