package com.canopas.base.ui.themes

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

private val ExploreLightColorPalette = ExploreColors(
    exploreTodoCardBg = Color(0xFFE8F5F9),
    exploreTodoTitleText = Color(0xFF0D7090),
    exploreTodoCompleteText = Color(0xFF0D7090),
    exploreTodoCompleteBorder = Color(0xFF0D7090),
    exploreTodoCompleteButton = Color(0xFFE8F5F9),
    initialHeaderBg = Color(0xFFFFF4D9),
    categoryBgColor = Color(0xFFFDF0EE)
)

private val ExploreDarkColorPalette = ExploreColors(
    exploreTodoCardBg = Color(0xFF353341),
    exploreTodoTitleText = Color(0xFFFFFFFF),
    exploreTodoCompleteText = Color(0xFFF67C37),
    exploreTodoCompleteBorder = Color(0xFF454350),
    exploreTodoCompleteButton = Color(0xFF454350),
    initialHeaderBg = Color(0XFF1C191F),
    categoryBgColor = Color(0xFF222222)
)

@Composable
fun ExploreTheme(
    darkTheme: Boolean,
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) ExploreDarkColorPalette else ExploreLightColorPalette
    CompositionLocalProvider(LocalExploreColors provides colors, content = content)
}

object ExploreTheme {
    val exploreColors: ExploreColors
        @Composable
        get() = LocalExploreColors.current
}

private val LocalExploreColors = staticCompositionLocalOf<ExploreColors> {
    error("No ExploreColorPalette provided")
}

data class ExploreColors(
    val exploreTodoCardBg: Color,
    val exploreTodoTitleText: Color,
    val exploreTodoCompleteText: Color,
    val exploreTodoCompleteBorder: Color,
    val exploreTodoCompleteButton: Color,
    val initialHeaderBg: Color,
    val categoryBgColor: Color
)
