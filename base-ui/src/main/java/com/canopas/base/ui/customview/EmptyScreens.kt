package com.canopas.base.ui.customview

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme.typography
import com.canopas.base.ui.InterSemiBoldFont
import com.canopas.base.ui.R
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors

@Composable
fun ServerErrorView(onClick: () -> Unit) {
    val scrollState = rememberScrollState()
    Box(modifier = Modifier.fillMaxSize().background(colors.background), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxSize()
                .verticalScroll(scrollState),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_server_error),
                contentDescription = "",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 68.dp, end = 68.dp, bottom = 78.dp)
                    .aspectRatio(1.04f)
            )

            Text(
                text = stringResource(R.string.empty_screens_server_error_heading_text),
                style = typography.errorHeadingTextStyle,
                lineHeight = 36.sp,
                color = colors.textPrimary,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(bottom = 12.dp, start = 20.dp, end = 20.dp)
            )

            Text(
                text = stringResource(R.string.empty_screens_server_error_sub_text),
                style = typography.subTextStyle1,
                lineHeight = 25.sp,
                textAlign = TextAlign.Center,
                color = colors.textSecondary,
                modifier = Modifier.padding(bottom = 40.dp, start = 20.dp, end = 20.dp)
            )

            PrimaryButton(
                onClick = { onClick() },
                text = stringResource(R.string.empty_screens_retry_button_text),
                modifier = Modifier
                    .wrapContentWidth()
                    .motionClickEvent { },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = colors.primary,
                    contentColor = colors.textPrimary,
                    disabledBackgroundColor = colors.primary.copy(0.6f)
                ),
                textModifier = Modifier.padding(horizontal = 56.dp)
            )
        }
    }
}

@Composable
fun NetworkErrorView(onClick: () -> Unit) {
    val scrollState = rememberScrollState()
    Box(modifier = Modifier.fillMaxSize().background(colors.background), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxSize()
                .verticalScroll(scrollState),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_network_error),
                contentDescription = "",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 24.dp, end = 24.dp, bottom = 78.dp)
                    .aspectRatio(1.43f)

            )

            Text(
                text = stringResource(R.string.empty_screens_network_error_heading_text),
                style = typography.errorHeadingTextStyle,
                lineHeight = 36.sp,
                textAlign = TextAlign.Center,
                color = colors.textPrimary,
                modifier = Modifier.padding(bottom = 12.dp, start = 20.dp, end = 20.dp)
            )

            Text(
                text = stringResource(R.string.empty_screens_network_error_sub_text),
                style = typography.subTextStyle1,
                lineHeight = 25.sp,
                textAlign = TextAlign.Center,
                color = colors.textSecondary,
                modifier = Modifier.padding(bottom = 40.dp, start = 20.dp, end = 20.dp)
            )

            PrimaryButton(
                onClick = { onClick() },
                text = stringResource(R.string.empty_screens_retry_button_text),
                modifier = Modifier
                    .wrapContentWidth()
                    .motionClickEvent { },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = colors.primary,
                    contentColor = colors.textPrimary,
                    disabledBackgroundColor = colors.primary.copy(0.6f)
                ),
                textModifier = Modifier.padding(horizontal = 56.dp)
            )
        }
    }
}

@Composable
fun EmptyCommunityView(onClick: () -> Unit) {

    val scrollState = rememberScrollState()

    Box(modifier = Modifier.fillMaxSize().background(colors.background), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxSize()
                .verticalScroll(scrollState),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_empty_community),
                contentDescription = "",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 66.dp, end = 66.dp, bottom = 40.dp)
                    .aspectRatio(0.93f)
            )
            Text(
                text = stringResource(R.string.empty_screens_add_community_grow_with_your_circle_title_text),
                style = typography.errorHeadingTextStyle,
                lineHeight = 36.sp,
                color = colors.textPrimary,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(bottom = 16.dp, start = 64.dp, end = 64.dp)
            )

            Text(
                text = buildAnnotatedString {
                    withStyle(
                        style = SpanStyle(
                            fontFamily = InterSemiBoldFont,
                            color = colors.textPrimary.copy(0.9f)
                        )
                    ) {
                        append(stringResource(R.string.empty_screens_add_community_text_part1))
                    }
                    append(stringResource(R.string.empty_screens_add_community_text_part2))
                    withStyle(
                        style = SpanStyle(
                            fontFamily = InterSemiBoldFont,
                            color = colors.textPrimary.copy(0.9f)
                        )
                    ) {
                        append(stringResource(R.string.empty_screens_add_community_text_part3))
                    }
                },
                lineHeight = 25.sp,
                style = typography.subTextStyle1,
                color = colors.textSecondary,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(bottom = 40.dp, start = 40.dp, end = 40.dp),
            )

            PrimaryButton(
                onClick = { onClick() },
                text = stringResource(id = R.string.empty_screens_add_community_get_started_button_text),
                modifier = Modifier
                    .wrapContentWidth()
                    .motionClickEvent { }
                    .padding(bottom = 40.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = colors.primary,
                    contentColor = colors.textPrimary,
                    disabledBackgroundColor = colors.primary.copy(0.6f)
                ),
                textModifier = Modifier.padding(horizontal = 36.dp)
            )
        }
    }
}

@Composable
fun EmptyActivitiesView(onClick: () -> Unit) {

    val scrollState = rememberScrollState()

    Box(modifier = Modifier.fillMaxSize().background(colors.background), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxSize()
                .verticalScroll(scrollState),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Text(
                text = stringResource(R.string.empty_screens_list_view_heading_text),
                style = typography.errorHeadingTextStyle,
                lineHeight = 36.sp,
                color = colors.textPrimary,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(bottom = 16.dp, start = 64.dp, end = 64.dp, top = 24.dp)
            )

            Text(
                text = buildAnnotatedString {
                    append(stringResource(R.string.empty_screens_list_view_sub_text_part1))
                    withStyle(
                        style = SpanStyle(
                            fontFamily = InterSemiBoldFont
                        )
                    ) {
                        append(stringResource(R.string.empty_screens_list_view_sub_text_part2))
                    }
                },
                lineHeight = 25.sp,
                style = typography.subTextStyle1,
                color = colors.textPrimary,
                textAlign = TextAlign.Center,
                modifier = Modifier.padding(bottom = 40.dp, start = 40.dp, end = 40.dp),
            )

            Image(
                painter = painterResource(id = R.drawable.ic_empty_activity),
                contentDescription = "",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 90.dp, end = 90.dp, bottom = 40.dp)
                    .aspectRatio(0.74f)
            )

            PrimaryButton(
                onClick = { onClick() },
                text = stringResource(R.string.empty_screens_list_view_button_text),
                modifier = Modifier
                    .wrapContentWidth()
                    .padding(bottom = 40.dp)
                    .motionClickEvent { },
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = colors.primary,
                    contentColor = colors.textPrimary,
                    disabledBackgroundColor = colors.primary.copy(0.6f)
                ),
                textModifier = Modifier.padding(horizontal = 20.dp)
            )
        }
    }
}
