package com.canopas.base.ui

import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.gestures.awaitFirstDown
import androidx.compose.foundation.gestures.waitForUpOrCancellation
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.composed
import androidx.compose.ui.draw.scale
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.platform.debugInspectorInfo

const val FLAG_IGNORE_GLOBAL_SETTINGS = 2
const val FEEDBACK_CONSTANT = 3

fun Modifier.motionClickEvent(onClick: () -> Unit) = composed(
    inspectorInfo = debugInspectorInfo {
        name = "motionClickEvent"
    }
) {
    val interactionSource = remember { MutableInteractionSource() }
    var selected by remember { mutableStateOf(false) }
    val scale = animateFloatAsState(if (selected) 0.96f else 1f)
    this
        .scale(scale.value)
        .clickable(
            interactionSource = interactionSource,
            indication = null,
            onClick = {
                onClick()
            }
        )
        .pointerInput(selected) {
            awaitPointerEventScope {
                selected = if (selected) {
                    waitForUpOrCancellation()
                    false
                } else {
                    awaitFirstDown(false)
                    true
                }
            }
        }
}

@OptIn(ExperimentalFoundationApi::class)
fun Modifier.combinedMotionClickEvent(onClick: () -> Unit, onLongClick: () -> Unit) = composed(
    inspectorInfo = debugInspectorInfo {
        name = "combinedMotionClickEvent"
    }
) {
    val view = LocalView.current
    val interactionSource = remember { MutableInteractionSource() }
    var selected by remember { mutableStateOf(false) }
    val scale = animateFloatAsState(if (selected) 0.96f else 1f)
    this
        .scale(scale.value)
        .combinedClickable(
            interactionSource = interactionSource,
            indication = null,
            onClick = {
                onClick()
            },
            onLongClick = {
                view.performHapticFeedback(FEEDBACK_CONSTANT, FLAG_IGNORE_GLOBAL_SETTINGS)
                onLongClick()
            }
        )
        .pointerInput(selected) {
            awaitPointerEventScope {
                selected = if (selected) {
                    waitForUpOrCancellation()
                    false
                } else {
                    awaitFirstDown(false)
                    true
                }
            }
        }
}
