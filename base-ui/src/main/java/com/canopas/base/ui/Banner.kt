package com.canopas.base.ui

import android.app.Activity
import android.content.Context
import com.tapadoo.alerter.Alerter

fun showBanner(context: Context, message: String) {
    Alerter.create(context as Activity)
        .setText(message)
        .hideIcon()
        .enableVibration(false)
        .enableSwipeToDismiss()
        .setBackgroundColorRes(R.color.colorPrimary)
        .setDuration(1500)
        .show()
}
