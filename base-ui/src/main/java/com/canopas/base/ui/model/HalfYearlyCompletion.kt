package com.canopas.base.ui.model

data class HalfYearlyCompletion(val weeklyCompletion: List<Int>, val month: Int)
