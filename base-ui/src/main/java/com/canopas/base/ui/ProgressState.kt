package com.canopas.base.ui

enum class ProgressState {
    Start, Finish
}
