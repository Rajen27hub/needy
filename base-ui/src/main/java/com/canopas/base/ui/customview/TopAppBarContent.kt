package com.canopas.base.ui.customview

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.themes.ComposeTheme.colors

@Composable
fun TopAppBarContent(
    title: String = "",
    subText: String? = null,
    navigationIconOnClick: (() -> Unit)? = null,
    enableNavigationIcon: Boolean = true,
    actions: @Composable RowScope.() -> Unit = {},
) {
    Box(modifier = Modifier.fillMaxWidth()) {
        Row(
            modifier = Modifier
                .fillMaxWidth(0.9f)
                .align(if (navigationIconOnClick == null || !enableNavigationIcon) Alignment.Center else Alignment.CenterStart),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = if (navigationIconOnClick == null || !enableNavigationIcon) Arrangement.Center else Arrangement.Start
        ) {
            if (navigationIconOnClick != null && enableNavigationIcon) {
                IconButton(
                    onClick = {
                        navigationIconOnClick()
                    },
                    modifier = Modifier
                ) {
                    Icon(
                        Icons.Filled.ArrowBack,
                        contentDescription = null,
                        tint = colors.textSecondary
                    )
                }
            }
            Column(modifier = Modifier.padding(start = if (navigationIconOnClick == null || !enableNavigationIcon) 0.dp else 4.dp)) {
                Text(
                    text = title,
                    style = AppTheme.typography.topBarTitleTextStyle,
                    color = colors.textPrimary,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                if (subText != null) {
                    Text(
                        text = subText,
                        style = AppTheme.typography.videoTitleTextStyle,
                        lineHeight = 15.sp,
                        color = colors.textSecondary,
                        maxLines = 1,
                        overflow = TextOverflow.Ellipsis
                    )
                }
            }
        }
        Row(
            Modifier
                .fillMaxHeight()
                .align(Alignment.CenterEnd),
            horizontalArrangement = Arrangement.End,
            verticalAlignment = Alignment.CenterVertically,
            content = actions
        )
    }
}
