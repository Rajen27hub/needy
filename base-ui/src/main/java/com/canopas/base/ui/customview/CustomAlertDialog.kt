package com.canopas.base.ui.customview

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun CustomAlertDialog(
    title: String?,
    subTitle: String,
    confirmBtnText: String?,
    dismissBtnText: String?,
    confirmBtnColor: Color = colors.textPrimary,
    dismissBtnColor: Color = colors.textPrimary,
    backgroundColor: Color = if (isDarkMode) darkDialogBoxBg else colors.background,
    onConfirmClick: (() -> Unit)? = null,
    onDismissClick: (() -> Unit)? = null,
) {
    Dialog(
        onDismissRequest = {},
        DialogProperties(usePlatformDefaultWidth = false)
    ) {
        Box(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
                .clip(RoundedCornerShape(25.dp))
                .background(backgroundColor),
            contentAlignment = Alignment.Center
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 14.dp, start = 16.dp, end = 16.dp)
            ) {

                if (title != null) {
                    Text(
                        text = title,
                        style = AppTheme.typography.titleTextStyle,
                        color = colors.textPrimary,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(bottom = 12.dp)
                    )
                }
                Text(
                    text = subTitle,
                    style = AppTheme.typography.bodyTextStyle1,
                    color = colors.textPrimary,
                    modifier = Modifier.fillMaxWidth()
                )

                Row(
                    Modifier
                        .fillMaxWidth()
                        .padding(top = 24.dp),
                    horizontalArrangement = Arrangement.End,
                ) {
                    if (dismissBtnText != null && onDismissClick != null) {
                        TextButton(onClick = (onDismissClick)) {
                            Text(
                                text = dismissBtnText,
                                textAlign = TextAlign.Center,
                                style = AppTheme.typography.buttonStyle,
                                color = dismissBtnColor
                            )
                        }
                    }

                    if (confirmBtnText != null && onConfirmClick != null) {
                        TextButton(
                            onClick = (onConfirmClick), modifier = Modifier.padding(start = 8.dp)
                        ) {
                            Text(
                                text = confirmBtnText,
                                textAlign = TextAlign.Center,
                                style = AppTheme.typography.buttonStyle,
                                color = confirmBtnColor
                            )
                        }
                    }
                }
            }
        }
    }
}

private val darkDialogBoxBg = Color(0xff222222)
