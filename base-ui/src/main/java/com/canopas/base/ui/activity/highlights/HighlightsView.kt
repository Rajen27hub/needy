package com.canopas.base.ui.activity.highlights

import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterRegularFont
import com.canopas.base.ui.R
import com.canopas.base.ui.customview.VerticalProgressBar
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import kotlin.math.roundToInt

@Composable
fun HighlightsView(
    currentWeekDuration: String,
    previousWeekDuration: String,
    currentProgress: Float,
    previousProgress: Float,
    descriptionText: String,
) {

    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
    ) {
        Text(
            text = stringResource(R.string.subscription_status_highlight_text),
            style = AppTheme.typography.h2TextStyle,
            color = colors.textPrimary,
            textAlign = TextAlign.Start,
            modifier = Modifier
                .padding(start = 20.dp, end = 20.dp, bottom = 16.dp)
                .fillMaxWidth()
        )

        Box(
            modifier = Modifier
                .wrapContentHeight()
                .padding(horizontal = 20.dp)
                .clip(RoundedCornerShape(16.dp))
                .border(
                    width = 1.dp,
                    if (isDarkMode) darkHeaderText else lightHeaderText,
                    shape = RoundedCornerShape(16.dp)
                )
        ) {
            Column(modifier = Modifier.fillMaxWidth()) {
                Text(
                    text = currentWeekDuration,
                    style = AppTheme.typography.buttonStyle,
                    color = if (isDarkMode) darkHeaderText else lightHeaderText,
                    textAlign = TextAlign.Start,
                    lineHeight = 17.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 12.dp, start = 12.dp)
                )
                Text(
                    text = descriptionText,
                    style = AppTheme.typography.bodyTextStyle3,
                    color = colors.textPrimary,
                    textAlign = TextAlign.Start,
                    lineHeight = 17.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 16.dp, start = 12.dp, bottom = 8.dp, end = 12.dp)
                )
                Divider(
                    Modifier
                        .padding(horizontal = 12.dp)
                        .fillMaxWidth(),
                    thickness = 0.5.dp,
                    color = if (isDarkMode) darkDivider else lightDivider
                )
                BoxWithConstraints(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 12.dp, horizontal = 16.dp)
                ) {
                    val progressHeight = if (maxWidth > 400.dp) maxWidth / 5 else 80.dp
                    val progressWidth = (progressHeight.value / 3).dp

                    Row(
                        modifier = Modifier
                            .fillMaxWidth(),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Row(
                            modifier = Modifier.weight(1f),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Start
                        ) {
                            val currentReading = currentProgress.roundToInt()
                            val previousReading = previousProgress.roundToInt()
                            val currentValue =
                                if (currentReading >= previousReading) 75f
                                else (currentProgress / (previousProgress / 75))

                            val progressBarColor =
                                if (currentReading >= previousReading) {
                                    if (isDarkMode) darkProgressBarColor else lightProgressBarColor
                                } else {
                                    if (isDarkMode) darkSecondaryBar else lightSecondaryBar
                                }

                            VerticalProgressBar(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .weight(2f),
                                indicatorHeight = progressHeight,
                                indicatorWidth = progressWidth,
                                progressColor = progressBarColor,
                                backgroundIndicatorColor = if (isDarkMode) darkEmptyBar else lightEmptyBar,
                                progress = currentValue
                            )
                            Spacer(modifier = Modifier.width(12.dp))
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth(),
                                verticalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = buildAnnotatedString {
                                        append(currentReading.toString())
                                        withStyle(
                                            style = SpanStyle(
                                                color = colors.textSecondary, fontSize = 14.sp,
                                                fontFamily = InterRegularFont
                                            )
                                        ) {
                                            append(stringResource(R.string.subscription_status_highlights_times_text))
                                        }
                                    },
                                    style = AppTheme.typography.h2TextStyle,
                                    color = colors.textPrimary,
                                    textAlign = TextAlign.Start,
                                    lineHeight = 17.sp
                                )
                                Text(
                                    text = currentWeekDuration,
                                    style = AppTheme.typography.highlightMonthStyle,
                                    color = colors.textPrimary,
                                    textAlign = TextAlign.Start,
                                    lineHeight = 17.sp
                                )
                            }
                        }

                        Spacer(modifier = Modifier.width(12.dp))

                        Row(
                            modifier = Modifier.weight(1f),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Start
                        ) {
                            val currentReading = currentProgress.roundToInt()
                            val previousReading = previousProgress.roundToInt()
                            val previousValue =
                                if (currentReading <= previousReading) 75f
                                else (previousProgress / (currentProgress / 75))

                            val progressBarColor =
                                if (currentReading <= previousReading) {
                                    if (isDarkMode) darkProgressBarColor else lightProgressBarColor
                                } else {
                                    if (isDarkMode) darkSecondaryBar else lightSecondaryBar
                                }

                            VerticalProgressBar(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .weight(2f),
                                indicatorHeight = progressHeight,
                                indicatorWidth = progressWidth,
                                progressColor = progressBarColor,
                                backgroundIndicatorColor = if (isDarkMode) darkEmptyBar else lightEmptyBar,
                                progress = previousValue
                            )
                            Spacer(modifier = Modifier.width(12.dp))
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth(),
                                verticalArrangement = Arrangement.Center
                            ) {
                                Text(
                                    text = buildAnnotatedString {
                                        append(previousReading.toString())
                                        withStyle(
                                            style = SpanStyle(
                                                color = colors.textSecondary, fontSize = 14.sp,
                                                fontFamily = InterRegularFont
                                            )
                                        ) {
                                            append(stringResource(R.string.subscription_status_highlights_times_text))
                                        }
                                    },
                                    style = AppTheme.typography.h2TextStyle,
                                    color = colors.textPrimary,
                                    textAlign = TextAlign.Start,
                                    lineHeight = 17.sp
                                )
                                Text(
                                    text = previousWeekDuration,
                                    style = AppTheme.typography.highlightMonthStyle,
                                    color = colors.textPrimary,
                                    textAlign = TextAlign.Start,
                                    lineHeight = 17.sp
                                )
                            }
                        }
                    }
                }
            }
        }
    }
}

private val lightHeaderText = Color(0xFFE2972A)
private val darkHeaderText = Color(0xFFFFBA57)
private val darkDivider = Color(0xFF313131)
private val lightDivider = Color(0x66000000)
private val darkProgressBarColor = Color(0xFF47A96E)
private val lightProgressBarColor = Color(0xFFE2972A)
private val lightSecondaryBar = Color(0xDEB0B9B3)
private val darkSecondaryBar = Color(0xFF625E5E)
private val lightEmptyBar = Color(0xDED4DAD6)
private val darkEmptyBar = Color(0xFFBDC2BF)
