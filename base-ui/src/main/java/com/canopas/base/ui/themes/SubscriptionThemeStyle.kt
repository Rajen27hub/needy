package com.canopas.base.ui.themes

import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.graphics.Color

private val StatusLightColorPalette = SubscriptionStatusColors(
    statusBarBg = Color(0xFFE6F0DE),
    headerText = Color(0xFF244509),
    shareYourProgressBg = Color(0xffF4FAF5),
    statusSubtitleTextColor = Color(0xFF146E78),
    statusSubtitleTextColor2 = Color(0xFF1E2B4F),
    dividerColor = Color(0x1A1C191F),
    emptyStreaksColor = Color(0xFFF0A028),
    emptyStreaksIconColor = Color(0xFFF47920),

)

private val StatusDarkColorPalette = SubscriptionStatusColors(
    statusBarBg = Color(0xff121212),
    headerText = Color(0xFF47A96E),
    shareYourProgressBg = Color(0xff47A96E),
    statusSubtitleTextColor = Color(0xff1D9CC3),
    statusSubtitleTextColor2 = Color(0xFF47A96E),
    dividerColor = Color(0xFF313131),
    emptyStreaksColor = Color(0xFFF0A028),
    emptyStreaksIconColor = Color(0xFFF47920),

)

@Composable
fun StatusTheme(
    darkTheme: Boolean,
    content: @Composable () -> Unit
) {
    val colors = if (darkTheme) StatusDarkColorPalette else StatusLightColorPalette
    CompositionLocalProvider(LocalSubscriptionColors provides colors, content = content)
}

object StatusTheme {
    val SubscriptionColors: SubscriptionStatusColors
        @Composable
        get() = LocalSubscriptionColors.current
}

private val LocalSubscriptionColors = staticCompositionLocalOf<SubscriptionStatusColors> {
    error("No ExploreColorPalette provided")
}

data class SubscriptionStatusColors(
    val statusBarBg: Color,
    val headerText: Color,
    val shareYourProgressBg: Color,
    val statusSubtitleTextColor: Color,
    val statusSubtitleTextColor2: Color,
    val dividerColor: Color,
    val emptyStreaksColor: Color,
    val emptyStreaksIconColor: Color,

)
