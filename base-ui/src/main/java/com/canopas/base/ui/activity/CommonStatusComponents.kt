package com.canopas.base.ui.activity

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.data.model.StreaksData
import com.canopas.base.data.model.getFormattedDayDate
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.CircularGradientProgressView
import com.canopas.base.ui.ProgressState
import com.canopas.base.ui.R
import com.canopas.base.ui.White
import com.canopas.base.ui.model.ProgressTabType
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.kizitonwose.calendar.core.CalendarDay
import com.kizitonwose.calendar.core.DayPosition
import java.time.DayOfWeek
import java.time.LocalDate
import java.time.format.TextStyle
import java.time.temporal.ChronoUnit
import java.util.Locale

const val INITIAL_VALUE = 0.01f

@Composable
fun CompletionRateViewGraphs(streakData: StreaksData, maxProgress: Int) {
    var progressState by remember {
        mutableStateOf(ProgressState.Start)
    }

    var initialValue by remember {
        mutableStateOf(INITIAL_VALUE)
    }

    val weight = (streakData.count.toFloat() / maxProgress) - INITIAL_VALUE

    val weightAnimation: Float by animateFloatAsState(
        if (progressState == ProgressState.Start) initialValue else weight,
        tween(1000, easing = FastOutSlowInEasing),
    )
    LaunchedEffect(key1 = initialValue, key2 = Unit, block = {
        when {
            initialValue < weight -> {
                initialValue += 0.1f
            }
            initialValue > weight -> {
                initialValue = weight
                progressState = ProgressState.Finish
            }
        }
    })
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            modifier = Modifier
                .padding(
                    top = 16.dp,
                    start = 20.dp,
                    end = 20.dp,
                    bottom = 4.dp
                )
                .fillMaxWidth(),
            text = if (streakData.start_date != streakData.end_date) "${
            getFormattedDayDate(
                streakData.start_date
            ).first
            } to ${
            getFormattedDayDate(
                streakData.end_date
            ).first
            }" else getFormattedDayDate(streakData.start_date).first,
            style = if (streakData.is_current) AppTheme.typography.currentStreakTextStyle else AppTheme.typography.bodyTextStyle1,
            color = colors.textPrimary
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp, vertical = 8.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {

            Card(
                modifier = Modifier
                    .weight(weightAnimation)
                    .height(12.dp)
                    .clip(RoundedCornerShape(25.dp)),
                backgroundColor = if (streakData.is_current) CurrentStreakColor else MissedStreaksColor,
                shape = RoundedCornerShape(25.dp)
            ) {}

            Row(
                modifier = Modifier
                    .weight(1.2f - weightAnimation),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_streaks),
                    contentDescription = "Streaks",
                    modifier = Modifier
                        .clip(CircleShape)
                        .size(24.dp),
                    tint = if (streakData.is_current) CurrentStreakColor else MissedStreaksColor
                )

                Text(
                    text = "${streakData.count}",
                    style = AppTheme.typography.bodyTextStyle3,
                    color = colors.textPrimary,
                    modifier = Modifier
                        .padding(end = 4.dp)
                        .wrapContentWidth(unbounded = true)
                )
            }
        }
    }
}

@Composable
fun DayView(
    day: CalendarDay,
    currentDate: LocalDate,
    startDate: LocalDate,
    endDate: LocalDate,
    showArchivedStatus: Boolean = false,
    completedDays: MutableList<LocalDate>,
    missedDays: MutableList<LocalDate>
) {
    val daysFromEndDate: Long = ChronoUnit.DAYS.between(
        day.date,
        if (showArchivedStatus) endDate else currentDate
    )
    val daysFromStartDate: Long = ChronoUnit.DAYS.between(
        startDate,
        day.date
    )

    val boxModifier = if (day.position == DayPosition.MonthDate) {
        Modifier
            .fillMaxWidth()
            .aspectRatio(1f)
            .padding(4.dp)
            .background(
                if (day.date in completedDays) SubscriptionStatusGreenStatColor else colors.background,
                CircleShape
            )
            .border(
                if (day.date == currentDate && !showArchivedStatus) 1.dp else 0.dp,
                shape = CircleShape,
                color = if (day.date == currentDate && !showArchivedStatus) SubscriptionStatusGreenStatColor else Color.Transparent
            )
    } else {
        Modifier
            .fillMaxWidth()
            .aspectRatio(1f)
            .padding(
                horizontal = 2.dp,
                vertical = 4.dp
            )
    }

    val textColor = if (day.position == DayPosition.MonthDate) {
        if (day.date in completedDays)
            White
        else if (day.date in missedDays && daysFromEndDate > 0 && daysFromStartDate >= 0)
            missingDayText
        else if (day.date == currentDate && !showArchivedStatus)
            SubscriptionStatusGreenStatColor
        else if (day.date == endDate && day.date in missedDays)
            missingDayText
        else if (isDarkMode) darkEmptyDayText else lightEmptyDayText
    } else {
        if (isDarkMode) darkEmptyDayText else lightEmptyDayText
    }

    Box(
        modifier = boxModifier,
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = if (day.date.dayOfMonth in 1..9) "0" + day.date.dayOfMonth.toString() else day.date.dayOfMonth.toString(),
            style = AppTheme.typography.buttonStyle,
            lineHeight = 15.sp,
            color = textColor,
            modifier = Modifier.padding(vertical = 4.dp)
        )
    }
}

@Composable
fun DaysOfWeekHeader(daysOfWeek: List<DayOfWeek>) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 24.dp, top = 46.dp)
    ) {
        for (dayOfWeek in daysOfWeek) {
            Text(
                modifier = Modifier
                    .weight(1f)
                    .padding(horizontal = 8.dp),
                textAlign = TextAlign.Center,
                maxLines = 1,
                overflow = TextOverflow.Visible,
                style = AppTheme.typography.buttonStyle,
                color = colors.textPrimary,
                text = dayOfWeek.getDisplayName(TextStyle.SHORT, Locale.getDefault()),
            )
        }
    }
}

@Composable
fun ProgressHeaderView(
    totalDays: Int,
    completedDays: Int,
    isLoadingState: Boolean,
    currentMonthName: String,
    showProgressView: Boolean = true
) {
    Row(
        modifier = Modifier
            .fillMaxWidth(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        AnimatedVisibility(
            visible = !isLoadingState,
            enter = fadeIn(
                animationSpec = tween(600, delayMillis = 100)
            ),
            exit = fadeOut()
        ) {
            Column(
                modifier = Modifier
                    .wrapContentWidth()
                    .aspectRatio(3f),
                horizontalAlignment = Alignment.Start,
                verticalArrangement = Arrangement.Center
            ) {
                Text(
                    text = buildAnnotatedString {
                        append("$completedDays/$totalDays")
                        withStyle(
                            style = SpanStyle(
                                color = colors.textSecondary,
                                fontSize = 14.sp,
                            )
                        ) {
                            append(stringResource(R.string.subscription_status_times_header_text))
                        }
                    },
                    color = colors.textPrimary,
                    style = AppTheme.typography.monthlyStatHeaderStyle,
                )
                Text(
                    text = currentMonthName,
                    color = colors.textSecondary,
                    style = AppTheme.typography.buttonStyle,
                    lineHeight = 17.sp,
                )
            }
            if (showProgressView) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight(),
                    horizontalAlignment = Alignment.End
                ) {
                    if (totalDays != 0) {
                        CircularGradientProgressView(
                            boxModifier = Modifier
                                .fillMaxWidth(0.32f)
                                .aspectRatio(1f)
                                .padding(end = 4.dp),
                            strokeWidth = 8.dp,
                            showText = true,
                            strokeBackgroundWidth = 2.dp,
                            progress = ((completedDays * 100) / totalDays).toFloat(),
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun SubscriptionProgressTabView(
    onSelectedTab: (selectionTab: ProgressTabType) -> Unit,
    currentSelectedTab: ProgressTabType
) {
    val dividerColor = if (isDarkMode) darkDivider else lightDivider

    Row(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(start = 20.dp, end = 20.dp, top = 28.dp)
            .background(if (isDarkMode) darkStatBarColor else lightStatBarColor, shape = RoundedCornerShape(15.dp))
            .wrapContentHeight(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        val context = LocalContext.current
        ProgressTabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (currentSelectedTab == ProgressTabType.PROGRESS_7D) colors.primary else Color.Transparent,
            onClick = {
                onSelectedTab(ProgressTabType.PROGRESS_7D)
            },
            titleText = ProgressTabType.PROGRESS_7D.getTitle(context),
            textStyle = if (currentSelectedTab == ProgressTabType.PROGRESS_7D) AppTheme.typography.subscriptionListDaysTextStyle else AppTheme.typography.bodyTextStyle3,
            textColor = if (currentSelectedTab == ProgressTabType.PROGRESS_7D) White else colors.textPrimary,
            dividerColor = if ((currentSelectedTab == ProgressTabType.PROGRESS_7D) || (currentSelectedTab == ProgressTabType.PROGRESS_M)) Color.Transparent else dividerColor
        )

        ProgressTabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (currentSelectedTab == ProgressTabType.PROGRESS_M) colors.primary else Color.Transparent,
            onClick = {
                onSelectedTab(ProgressTabType.PROGRESS_M)
            },
            titleText = ProgressTabType.PROGRESS_M.getTitle(context),
            textStyle = if (currentSelectedTab == ProgressTabType.PROGRESS_M) AppTheme.typography.subscriptionListDaysTextStyle else AppTheme.typography.bodyTextStyle3,
            textColor = if (currentSelectedTab == ProgressTabType.PROGRESS_M) White else colors.textPrimary,
            dividerColor = if ((currentSelectedTab == ProgressTabType.PROGRESS_M) || (currentSelectedTab == ProgressTabType.PROGRESS_6M)) Color.Transparent else dividerColor
        )

        ProgressTabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (currentSelectedTab == ProgressTabType.PROGRESS_6M) colors.primary else Color.Transparent,
            onClick = {
                onSelectedTab(ProgressTabType.PROGRESS_6M)
            },
            titleText = ProgressTabType.PROGRESS_6M.getTitle(context),
            textStyle = if (currentSelectedTab == ProgressTabType.PROGRESS_6M) AppTheme.typography.subscriptionListDaysTextStyle else AppTheme.typography.bodyTextStyle3,
            textColor = if (currentSelectedTab == ProgressTabType.PROGRESS_6M) White else colors.textPrimary,
            dividerColor = if ((currentSelectedTab == ProgressTabType.PROGRESS_6M) || (currentSelectedTab == ProgressTabType.PROGRESS_Y)) Color.Transparent else dividerColor
        )

        ProgressTabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (currentSelectedTab == ProgressTabType.PROGRESS_Y) colors.primary else Color.Transparent,
            onClick = {
                onSelectedTab(ProgressTabType.PROGRESS_Y)
            },
            titleText = ProgressTabType.PROGRESS_Y.getTitle(context),
            textStyle = if (currentSelectedTab == ProgressTabType.PROGRESS_Y) AppTheme.typography.subscriptionListDaysTextStyle else AppTheme.typography.bodyTextStyle3,
            textColor = if (currentSelectedTab == ProgressTabType.PROGRESS_Y) White else colors.textPrimary,
            dividerColor = if ((currentSelectedTab == ProgressTabType.PROGRESS_Y) || (currentSelectedTab == ProgressTabType.PROGRESS_ALL)) Color.Transparent else dividerColor
        )
        ProgressTabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (currentSelectedTab == ProgressTabType.PROGRESS_ALL) colors.primary else Color.Transparent,
            onClick = {
                onSelectedTab(ProgressTabType.PROGRESS_ALL)
            },
            titleText = ProgressTabType.PROGRESS_ALL.getTitle(context),
            textStyle = if (currentSelectedTab == ProgressTabType.PROGRESS_ALL) AppTheme.typography.subscriptionListDaysTextStyle else AppTheme.typography.bodyTextStyle3,
            textColor = if (currentSelectedTab == ProgressTabType.PROGRESS_ALL) White else colors.textPrimary,
            dividerColor = Color.Transparent
        )
    }
}

@Composable
fun ProgressTabView(
    modifier: Modifier,
    backgroundColor: Color,
    onClick: () -> Unit,
    titleText: String,
    textColor: Color,
    dividerColor: Color,
    textStyle: androidx.compose.ui.text.TextStyle,
) {
    Box(
        modifier = modifier
            .background(
                color = backgroundColor,
                RoundedCornerShape(17.dp)
            )
            .padding(horizontal = 4.dp)
            .motionClickEvent {
                onClick()
            }
            .clip(RoundedCornerShape(17.dp))
    ) {
        Text(
            text = titleText,
            modifier = Modifier
                .align(Alignment.Center)
                .padding(8.dp),
            color = textColor,
            style = textStyle,
            lineHeight = 16.sp
        )
    }
    Divider(
        Modifier
            .fillMaxHeight(0.5f)
            .width(1.dp),
        color = dividerColor,
    )
}
private val darkStatBarColor = Color(0xFF222222)
private val lightStatBarColor = Color(0xA1C191F)
private val SubscriptionStatusGreenStatColor = Color(0xFF47A96E)
private val CurrentStreakColor = Color(0xFFFFBA57)
private val MissedStreaksColor = Color(0xFFEDEDED)
private val lightEmptyDayText = Color(0x33000000)
private val darkEmptyDayText = Color(0x66FFFFFF)
private val missingDayText = Color(0xFFCA2F27)
private val lightDivider = Color(0x45000000)
private val darkDivider = Color(0xFF313131)
