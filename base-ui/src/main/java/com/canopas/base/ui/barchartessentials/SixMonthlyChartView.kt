package com.canopas.base.ui.barchartessentials

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColumnGraphBg
import com.canopas.base.ui.R
import com.canopas.base.ui.Utils.Companion.sixMonthlyFormat
import com.canopas.base.ui.model.HalfYearlyCompletion
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.patrykandpatryk.vico.compose.axis.horizontal.bottomAxis
import com.patrykandpatryk.vico.compose.axis.vertical.startAxis
import com.patrykandpatryk.vico.compose.chart.column.columnChart
import com.patrykandpatryk.vico.compose.chart.entry.collectAsState
import com.patrykandpatryk.vico.compose.chart.entry.defaultDiffAnimationSpec
import com.patrykandpatryk.vico.compose.chart.scroll.ChartScrollSpec
import com.patrykandpatryk.vico.compose.chart.scroll.rememberChartScrollSpec
import com.patrykandpatryk.vico.compose.component.lineComponent
import com.patrykandpatryk.vico.compose.state.MutableSharedState
import com.patrykandpatryk.vico.compose.style.ChartStyle
import com.patrykandpatryk.vico.compose.style.ProvideChartStyle
import com.patrykandpatryk.vico.core.axis.AxisPosition
import com.patrykandpatryk.vico.core.axis.AxisRenderer
import com.patrykandpatryk.vico.core.axis.formatter.AxisValueFormatter
import com.patrykandpatryk.vico.core.chart.Chart
import com.patrykandpatryk.vico.core.chart.column.ColumnChart
import com.patrykandpatryk.vico.core.chart.edges.FadingEdges
import com.patrykandpatryk.vico.core.chart.scale.AutoScaleUp
import com.patrykandpatryk.vico.core.component.shape.Shapes
import com.patrykandpatryk.vico.core.entry.ChartEntryModel
import com.patrykandpatryk.vico.core.entry.ChartEntryModelProducer
import com.patrykandpatryk.vico.core.entry.ChartModelProducer
import com.patrykandpatryk.vico.core.entry.FloatEntry
import com.patrykandpatryk.vico.core.legend.Legend
import com.patrykandpatryk.vico.core.marker.Marker
import com.patrykandpatryk.vico.core.marker.MarkerVisibilityChangeListener
import java.util.Calendar

private const val MAX_LABEL_COUNT = 5
private const val EMPTY_LABEL_COUNT = 1
private val entityColors = longArrayOf(0xFF47A96E)

@Composable
fun SixMonthlyChartView(sixMonthlyChartData: List<HalfYearlyCompletion>, isLoadingState: Boolean) {

    val calendar = Calendar.getInstance()
    val months = sixMonthlyChartData.map {
        calendar.set(Calendar.MONTH, (it.month.plus(1)))
        calendar.set(Calendar.DAY_OF_MONTH, 0)
        sixMonthlyFormat.format(calendar.timeInMillis)
    }

    val xAxisValueFormatter: AxisValueFormatter<AxisPosition.Horizontal.Bottom> =
        AxisValueFormatter { x, _ ->
            months[x.toInt() % months.size]
        }

    val multiChartEntryModelProducer = ChartEntryModelProducer()
    multiChartEntryModelProducer.setEntries(
        entries = List(size = sixMonthlyChartData.maxOf { it.weeklyCompletion.size }) {
            sixMonthlyChartData.mapIndexed { index, halfYearlyCompletion ->
                FloatEntry(x = index.toFloat(), y = halfYearlyCompletion.weeklyCompletion[it].toFloat())
            }
        },
    )

    val chartStyle = ChartStyle.fromColors(
        axisLabelColor = colors.textPrimary.copy(0.45f),
        axisGuidelineColor = colors.textPrimary.copy(0.07f),
        axisLineColor = colors.textPrimary.copy(0.07f),
        entityColors = listOf(ColumnGraphBg),
        elevationOverlayColor = colors.background,
    )
    val decorations = listOf(rememberGroupedColumnChartThresholdLine())
    ProvideChartStyle(chartStyle = chartStyle) {
        val columnChart = columnChart(
            spacing = 15.dp,
            mergeMode = ColumnChart.MergeMode.Grouped,
            decorations = decorations,
            columns = entityColors.map {
                lineComponent(
                    color = Color(it),
                    thickness = 40.dp,
                    shape = Shapes.roundedCornerShape(
                        topRightPercent = 20,
                        topLeftPercent = 20
                    ),
                )
            },
        )
        AnimatedVisibility(visible = sixMonthlyChartData.maxOf { it.weeklyCompletion.maxOf { it } } == 0 && !isLoadingState) {
            Box(modifier = Modifier.fillMaxWidth().padding(horizontal = 40.dp), contentAlignment = Alignment.Center) {
                Text(
                    text = stringResource(R.string.subscription_status_no_completion_text),
                    color = colors.textPrimary.copy(0.45f),
                    style = AppTheme.typography.bodyTextStyle2,
                    textAlign = TextAlign.Center
                )
            }
        }
        SixMonthsChart(
            chart = columnChart,
            chartModelProducer = multiChartEntryModelProducer,
            topAxis = customTopAxis(),
            startAxis = if (sixMonthlyChartData.maxOf { it.weeklyCompletion.maxOf { it } } != 0) {
                startAxis(
                    valueFormatter = yAxisFormatter,
                    maxLabelCount = when {
                        sixMonthlyChartData.maxOf { it.weeklyCompletion.maxOf { it } } < MAX_LABEL_COUNT -> sixMonthlyChartData.maxOf { it.weeklyCompletion.maxOf { it } }.toInt()
                        else -> MAX_LABEL_COUNT
                    }
                )
            } else { startAxis(maxLabelCount = EMPTY_LABEL_COUNT) },
            chartScrollSpec = rememberChartScrollSpec(isScrollEnabled = false),
            endAxis = customEndAxis(),
            bottomAxis = bottomAxis(valueFormatter = xAxisValueFormatter, tickLength = months.size.dp),
            marker = marker()
        )
    }
}

@Composable
fun <Model : ChartEntryModel> SixMonthsChart(
    chart: Chart<Model>,
    chartModelProducer: ChartModelProducer<Model>,
    modifier: Modifier = Modifier,
    startAxis: AxisRenderer<AxisPosition.Vertical.Start>? = null,
    topAxis: AxisRenderer<AxisPosition.Horizontal.Top>? = null,
    endAxis: AxisRenderer<AxisPosition.Vertical.End>? = null,
    bottomAxis: AxisRenderer<AxisPosition.Horizontal.Bottom>? = null,
    marker: Marker? = null,
    markerVisibilityChangeListener: MarkerVisibilityChangeListener? = null,
    legend: Legend? = null,
    chartScrollSpec: ChartScrollSpec<Model> = rememberChartScrollSpec(),
    diffAnimationSpec: AnimationSpec<Float> = defaultDiffAnimationSpec,
    runInitialAnimation: Boolean = true,
    fadingEdges: FadingEdges? = null,
    autoScaleUp: AutoScaleUp = AutoScaleUp.Full,
) {
    val modelState: MutableSharedState<Model?, Model?> = chartModelProducer.collectAsState(
        chartKey = chart,
        producerKey = chartModelProducer,
        animationSpec = diffAnimationSpec,
        runInitialAnimation = runInitialAnimation,
    )

    ChartBox(modifier = modifier) {
        modelState.value?.also { model ->
            ChartImpl(
                chart = chart,
                model = model,
                oldModel = modelState.previousValue,
                startAxis = startAxis,
                topAxis = topAxis,
                endAxis = endAxis,
                bottomAxis = bottomAxis,
                marker = marker,
                markerVisibilityChangeListener = markerVisibilityChangeListener,
                legend = legend,
                chartScrollSpec = chartScrollSpec,
                fadingEdges = fadingEdges,
                autoScaleUp = autoScaleUp,
            )
        }
    }
}
