package com.canopas.base.ui.customview

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonColors
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.canopas.base.ui.AppTheme

@Composable
fun SecondaryOutlineButton(
    onClick: () -> Unit = {},
    modifier: Modifier = Modifier,
    border: BorderStroke? = ButtonDefaults.outlinedBorder,
    text: String,
    textModifier: Modifier = Modifier,
    shape: Shape = RoundedCornerShape(0),
    colors: ButtonColors = ButtonDefaults.outlinedButtonColors(),
    color: Color = Color.Unspecified,
    icon: Painter? = null,
    iconModifier: Modifier = Modifier,
) {
    OutlinedButton(
        modifier = modifier,
        onClick = {
            onClick()
        },
        border = border,
        shape = shape,
        colors = colors
    ) {
        if (icon != null) {
            Icon(
                painter = icon,
                contentDescription = null,
                modifier = iconModifier,
                tint = color
            )
        }
        Text(
            text = text,
            style = AppTheme.typography.buttonStyle,
            color = color,
            textAlign = TextAlign.Center,
            modifier = textModifier
        )
    }
}

@Preview
@Composable
fun PreviewSecondaryButton() {
    SecondaryOutlineButton(onClick = { }, text = "Preview")
}
