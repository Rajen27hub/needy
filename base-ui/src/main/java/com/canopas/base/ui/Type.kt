package com.canopas.base.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.staticCompositionLocalOf
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp

data class Typography(
    val buttonStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 14.sp
    ),

    val h1TextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 28.sp
    ),

    val h2TextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 24.sp
    ),

    val h3TextStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 20.sp
    ),

    val boldHeaderStyle: TextStyle = TextStyle(
        fontFamily = InterBoldFont,
        fontWeight = FontWeight.Bold,
        fontSize = 16.sp,
        lineHeight = 19.sp
    ),

    val notesHeaderStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp,
        lineHeight = 19.sp
    ),

    val bodyTextStyle1: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp
    ),

    val successTextStyle1: TextStyle = TextStyle(
        fontFamily = InterLightFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    ),

    val bodyTextStyle2: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 14.sp
    ),

    val bodyTextStyle3: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Normal,
        fontSize = 14.sp
    ),

    val subTitleTextStyle1: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 16.sp
    ),

    val captionTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    ),

    val notesDateStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 12.sp
    ),

    val subscriptionIntervalTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    ),

    val todoActivityTitleTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 18.sp,
    ),

    val todoActivitySubtitleTextStyle: TextStyle = TextStyle(
        fontFamily = InterItalicFont,
        fontSize = 14.sp,
    ),

    val todoActivityTimeTextStyle: TextStyle = TextStyle(
        fontFamily = InterBlackItalicFont,
        lineHeight = 24.sp
    ),

    val subscriptionListActivityNameTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 20.sp
    ),

    val subscriptionListDaysTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 14.sp
    ),

    val thoughtTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 20.sp
    ),

    val completeCardTitleTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 18.sp
    ),

    val titleTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 18.sp
    ),

    val topBarTitleTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 20.sp
    ),

    val tabTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    ),

    val topBarButtonTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 20.sp
    ),

    val reminderViewTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 36.sp
    ),

    val monthDayTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    ),

    val videoTitleTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 12.sp
    ),

    val subTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 18.sp
    ),

    val proTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    ),

    val premiumPlanStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 20.sp
    ),

    val storyUserNameTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    ),

    val countryCodeTextStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 18.sp
    ),

    val notesContentTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 18.sp
    ),

    val phoneNumberTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 18.sp,
        lineHeight = 22.sp,
        letterSpacing = 0.04.em
    ),

    val enterOtpTextStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 34.sp,
        lineHeight = 42.sp,
        letterSpacing = 0.25.em,
        textAlign = TextAlign.Center
    ),

    val defaultOtpTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 16.sp,
        textAlign = TextAlign.Center
    ),

    val premiumThoughtTextStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 20.sp
    ),

    val notesDateTextStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 12.sp
    ),
    val addMemberTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 12.sp
    ),

    val topBarActionTextStyle: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 20.sp
    ),

    val detailViewAdminStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 12.sp
    ),

    val currentStreakTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    ),

    val subTextStyle1: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 18.sp
    ),

    val errorHeadingTextStyle: TextStyle = TextStyle(
        fontFamily = AcmeRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 28.sp
    ),

    val statusScreenHeadingTextStyle: TextStyle = TextStyle(
        fontFamily = AcmeRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 28.sp
    ),

    val pointsUserTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    ),

    val pointUserNameTextStyle: TextStyle = TextStyle(
        fontFamily = AcmeRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 18.sp
    ),

    val onBoardDescriptionTextStyle: TextStyle = TextStyle(
        fontFamily = AcmeRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 28.sp
    ),
    val bottomBarLabelTextStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 10.sp
    ),
    val monthlyStatHeaderStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 36.sp,
        lineHeight = 44.sp
    ),
    val progressStatHeaderStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 36.sp,
        lineHeight = 44.sp
    ),

    val subscriptionDateTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 16.sp
    ),
    val bodyTextStyle4: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.Medium,
        fontSize = 11.sp
    ),
    val WeekStatHeaderStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 36.sp,
        lineHeight = 44.sp
    ),
    val notificationSummarySubText: TextStyle = TextStyle(
        fontFamily = InterRegularFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 14.sp
    ),

    val notificationSummaryBodyText: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 22.sp
    ),
    val notificationSummaryPointText: TextStyle = TextStyle(
        fontFamily = InterBoldFont,
        fontWeight = FontWeight.Bold,
        fontSize = 16.sp
    ),
    val subTextStyle2: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 14.sp
    ),
    val profileTextCharStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
    ),
    val highlightMonthStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 12.sp
    ),
    val highlightSubTextStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 24.sp
    ),
    val wakeUpTimeActiveTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 20.sp,
    ),
    val wakeUpTimeInactiveTextStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 18.sp,
    ),
    val configureActivityHeaderStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontStyle = FontStyle.Normal,
        fontSize = 20.sp,
        lineHeight = 24.sp
    ),
    val configureActiveTabStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontStyle = FontStyle.Normal,
        fontSize = 16.sp,
        lineHeight = 19.sp
    ),
    val configureInactiveTabStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontStyle = FontStyle.Normal,
        fontSize = 14.sp,
        lineHeight = 17.sp
    ),
    val configureInactiveItemStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontStyle = FontStyle.Normal,
        fontSize = 14.sp,
        lineHeight = 17.sp
    ),
    val configureActiveItemStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontStyle = FontStyle.Normal,
        fontSize = 14.sp,
        lineHeight = 17.sp
    ),
    val configureSubItemsStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontStyle = FontStyle.Normal,
        fontSize = 16.sp,
        lineHeight = 19.sp
    ),

    val introHeaderTextStyle: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 40.sp,
        letterSpacing = -(1.6).sp,
        lineHeight = 52.sp
    ),

    val introHeaderJustlyText: TextStyle = TextStyle(
        fontFamily = AcmeRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 42.sp,
        lineHeight = 52.sp
    ),

    val yourCommunityText: TextStyle = TextStyle(
        fontFamily = InterSemiBoldFont,
        fontWeight = FontWeight.SemiBold,
        fontSize = 20.sp
    ),

    val goalsBodyTextStyle: TextStyle = TextStyle(
        fontFamily = KalamRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 18.sp
    ),

    val perfectDayTextStyle: TextStyle = TextStyle(
        fontFamily = AcmeRegularFont,
        fontWeight = FontWeight.Normal,
        lineHeight = 36.sp
    ),
    val perfectDaySubtitleTextStyle: TextStyle = TextStyle(
        fontFamily = InterMediumFont,
        fontWeight = FontWeight.Medium,
        fontSize = 16.sp
    ),
    val addGoalTitleStyle: TextStyle = TextStyle(
        fontFamily = KalamRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 24.sp,
        lineHeight = 29.sp
    ),
    val goalTodoTextStyle: TextStyle = TextStyle(
        fontFamily = KalamRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 14.sp,
        lineHeight = 22.sp
    ),
    val storyViewQuotesTextStyle: TextStyle = TextStyle(
        fontFamily = UbuntuRegularFont,
        fontWeight = FontWeight.Normal,
        fontSize = 22.sp,
        lineHeight = 31.sp,
        letterSpacing = -(0.41).sp
    )
)

internal val LocalTypography = staticCompositionLocalOf { Typography() }

@Composable
fun dpToSp(dp: Dp) = with(LocalDensity.current) { dp.toSp() }
