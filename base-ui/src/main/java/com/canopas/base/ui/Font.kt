package com.canopas.base.ui

import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily

val InterSemiBoldFont = FontFamily(Font(R.font.inter_semi_bold))
val InterLightFont = FontFamily(Font(R.font.inter_light))
val InterRegularFont = FontFamily(Font(R.font.inter_regular))
val InterMediumFont = FontFamily(Font(R.font.inter_medium))
val InterBoldFont = FontFamily(Font(R.font.inter_bold))
val InterItalicFont = FontFamily(Font(R.font.inter_italic))
val InterBlackItalicFont = FontFamily(Font(R.font.inter_black_italic))
val AcmeRegularFont = FontFamily(Font(R.font.acme_regular))
val KalamRegularFont = FontFamily(Font(R.font.kalam_regular))
val KalamLightFont = FontFamily(Font(R.font.kalam_light))
val KalamBoldFont = FontFamily(Font(R.font.kalam_bold))
val UbuntuRegularFont = FontFamily(Font(R.font.ubuntu_regular))
