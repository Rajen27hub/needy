package com.canopas.base.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Divider
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode

@Composable
fun DividedLineView() {
    Divider(
        Modifier
            .fillMaxWidth(),
        thickness = 1.dp,
        color = Color.Gray.copy(alpha = 0.12f)
    )
}

@Composable
fun CustomActivityTitleLineView(modifier: Modifier, color: Color) {
    Divider(
        modifier = modifier,
        thickness = 1.dp,
        color = color
    )
}

@Composable
fun AddGoalTextFieldLineView(modifier: Modifier, color: Color) {
    Divider(
        modifier = modifier,
        thickness = 1.dp,
        color = color
    )
}

@Composable
fun PostStoryTitleLineView(color: Color) {
    Divider(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 4.dp),
        thickness = 1.dp,
        color = color
    )
}

@Composable
fun DividedLineInfoView() {
    Divider(
        Modifier
            .fillMaxWidth(),
        thickness = 1.dp,
        color = if (isDarkMode) darkDividerColor else lightDividerColor
    )
}

@Composable
fun AddCommunityTextFieldLineView(color: Color) {
    Divider(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp),
        thickness = 1.dp,
        color = color
    )
}

@Composable
fun AddCommunityDetailLineView(color: Color) {
    Divider(
        Modifier
            .fillMaxWidth()
            .padding(start = 92.dp),
        thickness = 1.dp,
        color = color
    )
}

@Composable
fun DividerLineView(color: Color) {
    Divider(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        thickness = Dp.Hairline,
        color = color,
    )
}

@Composable
fun ThinDividerLineView(color: Color) {
    Divider(
        Modifier
            .fillMaxWidth(),
        thickness = 1.dp,
        color = color,
    )
}

private val darkDividerColor = Color(0xFF313131)
private val lightDividerColor = Color(0x14000000)
