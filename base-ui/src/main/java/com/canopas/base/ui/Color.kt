package com.canopas.base.ui

import androidx.compose.ui.graphics.Color

val ColorPrimary = Color(0xFFF67C37)
val BackArrowTint = Color(0xFF007AFF)
val ExploreTodoCardBg = Color(0xFFE8F5F9)
val NotificationDialogButtonColor = Color(0xff007aff)
val White = Color(0xFFFFFFFF)
val textColor = Color(0xFF000000)
val SignOutTextColor = Color(0xEEE13333)
val uncheckSwitchBg = Color(0xFF1C191F)
val WelcomeBtnSkipBg = Color(0xFFA3B1C6)
val DisabledButton = Color(0xFFF9B085)
val ColumnGraphBg = Color(0xFF47A96E)

fun Color.Companion.parse(colorString: String): Color =
    Color(color = android.graphics.Color.parseColor(colorString))
