package com.canopas.feature_points_ui.leaderboard

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_points_data.TestUtils.Companion.userPoints
import com.canopas.feature_points_data.model.DurationType
import com.canopas.feature_points_data.model.UserPoints
import com.canopas.feature_points_data.rest.PointService
import com.canopas.feature_points_ui.MainCoroutineRule
import com.canopas.feature_points_ui.PointsNavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class LeaderboardViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val services = mock<PointService>()

    private val navManager = mock<PointsNavManager>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    private val authManager = mock<AuthManager>()

    private val appAnalytics = mock<AppAnalytics>()
    private val resources = mock<Resources>()

    private lateinit var viewModel: LeaderboardViewModel

    private fun initViewModel() {
        viewModel = LeaderboardViewModel(
            navManager, services, testDispatcher, appAnalytics, authManager, resources
        )
    }

    @Before
    fun setup() {
        initViewModel()
    }

    @Test
    fun test_loading_state_display_leaderboard_list() = runTest {
        val loadingState = PointState.LOADING
        whenever(services.getPoints("week")).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(listOf(userPoints))
        }
        viewModel.getLeaderBoard()
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_Success_state_display_leaderboard_list_duration_week() = runTest {
        val currentUserList = mutableListOf<UserPoints>()
        val loadingState = PointState.SUCCESS(listOf(userPoints), currentUserList)
        whenever(services.getPoints("week")).thenReturn(Result.success(listOf(userPoints)))
        viewModel.getLeaderBoard()
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_handleTabItemSelection_week() = runTest {
        viewModel.handleTabItemSelection(DurationType.DURATION_WEEK)
        Assert.assertEquals("week", viewModel.currentSelectedTab.value.title)
    }

    @Test
    fun test_handleTabItemSelection_month() = runTest {
        viewModel.handleTabItemSelection(DurationType.DURATION_MONTH)
        Assert.assertEquals("month", viewModel.currentSelectedTab.value.title)
    }

    @Test
    fun test_handleTabItemSelection_all_time() = runTest {
        viewModel.handleTabItemSelection(DurationType.DURATION_ALL)
        Assert.assertEquals("all", viewModel.currentSelectedTab.value.title)
    }

    @Test
    fun test_popBack() {
        viewModel.onBackClick()
        verify(navManager).popBackStack()
    }
}
