package com.canopas.feature_points_ui

import androidx.navigation.NavController
import javax.inject.Inject
import javax.inject.Singleton

sealed class PointScreen(val route: String) {
    object Root : PointScreen("PointsRoot")
    object LeaderboardView : PointScreen("LeaderboardView")
}

@Singleton
class PointsNavManager @Inject constructor() {
    private var navController: NavController? = null

    fun setNavController(navController: NavController) {
        this.navController = navController
    }

    fun removeNavController() {
        navController = null
    }

    fun popBackStack() {
        navController?.popBackStack()
    }
}
