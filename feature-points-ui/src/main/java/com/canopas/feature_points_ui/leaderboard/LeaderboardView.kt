package com.canopas.feature_points_ui.leaderboard

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.White
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.feature_points_data.model.DurationType
import com.canopas.feature_points_data.model.UserPoints
import com.canopas.feature_points_ui.R
import com.google.android.gms.common.util.DeviceProperties.isTablet

@Composable
fun LeaderboardView() {
    val viewModel = hiltViewModel<LeaderboardViewModel>()
    val pointState by viewModel.state.collectAsState()
    val useDarkIcons = isDarkMode

    LaunchedEffect(key1 = Unit) {
        viewModel.getLeaderBoard()
    }

    Scaffold(topBar = {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(R.string.point_leaderboard_screen_title),
                    navigationIconOnClick = { viewModel.onBackClick() }
                )
            },
            backgroundColor = if (useDarkIcons) darkHeaderBg else lightHeaderBg,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
    }) {
        pointState.let { pointState ->
            when (pointState) {
                PointState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = ColorPrimary)
                    }
                }

                is PointState.SUCCESS -> {
                    val firstThreeUsersList = pointState.firstThreeUsersList
                    val remainingUsersList = pointState.remainingUsersList
                    PointsSuccessView(
                        viewModel,
                        firstThreeUsersList,
                        remainingUsersList
                    )
                }
                else -> {}
            }
        }
    }
}

@Composable
fun PointsSuccessView(
    viewModel: LeaderboardViewModel,
    firstThreeUsersList: List<UserPoints>,
    remainingUsersList: MutableList<UserPoints>,
) {
    val context = LocalContext.current
    val currentUserId by viewModel.userId.collectAsState()
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
        ) {

            Image(
                painter = painterResource(
                    id =
                    if (isDarkMode)R.drawable.ic_leaderboard_header_bg1
                    else R.drawable.ic_leaderboard_header_bg
                ),
                contentScale = ContentScale.FillBounds,
                modifier = Modifier.matchParentSize(),
                contentDescription = "header image"
            )

            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.Center)
                    .wrapContentHeight(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.Center
                ) {
                    PointsSelectionTabView(viewModel)
                }
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_cheerful_bg),
                        contentDescription = null,
                        contentScale = ContentScale.FillBounds,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(horizontal = 20.dp)
                            .align(Alignment.TopCenter)
                    )
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 28.dp, bottom = 60.dp),
                    ) {
                        val topPadding = if (isTablet(context)) 84.dp else 60.dp

                        PointsRankView(
                            modifier = Modifier
                                .weight(0.7f)
                                .padding(top = topPadding),
                            painterResource(id = R.drawable.ic_rank_two),
                            firstThreeUsersList[1],
                            currentUserId
                        )

                        PointsRankView(
                            modifier = Modifier.weight(1f),
                            painterResource(id = R.drawable.ic_rank_one),
                            firstThreeUsersList[0],
                            currentUserId,
                            showKingCrownView = true
                        )

                        PointsRankView(
                            modifier = Modifier
                                .weight(0.7f)
                                .padding(top = topPadding),
                            painterResource(id = R.drawable.ic_rank_three),
                            firstThreeUsersList[2],
                            currentUserId
                        )
                    }
                }
            }
        }

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .offset(y = (-48).dp, x = 0.dp)
                .wrapContentHeight(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            remainingUsersList.forEach { item ->
                Row(
                    modifier = Modifier
                        .widthIn(max = 600.dp)
                        .fillMaxWidth()
                        .padding(horizontal = 20.dp, vertical = 8.dp)
                        .wrapContentHeight()
                        .clip(shape = RoundedCornerShape(16.dp))
                        .background(
                            color = if (currentUserId == item.userId) pointsItemBg
                            else if (isDarkMode) darkListBg else lightListBg,
                            shape = RoundedCornerShape(16.dp)
                        ),
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = item.rank.toString(),
                        style = AppTheme.typography.pointsUserTextStyle,
                        color = if (currentUserId == item.userId) White else colors.textPrimary,
                        lineHeight = 18.sp,
                        letterSpacing = -(0.5.sp),
                        textAlign = TextAlign.Start,
                        modifier = Modifier
                            .wrapContentWidth()
                            .padding(start = 12.dp)
                    )
                    Row(
                        modifier = Modifier.weight(0.7f),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Start
                    ) {
                        ProfileImageView(
                            modifier = Modifier
                                .padding(12.dp)
                                .size(36.dp)
                                .border(
                                    1.dp,
                                    if (item.user.profileImageUrl.isNullOrEmpty() && isDarkMode) colors.textSecondary else lightProfileBorder,
                                    CircleShape
                                )
                                .align(Alignment.CenterVertically),
                            data = item.user.profileImageUrl,
                            char = item.user.profileImageChar()
                        )
                        Text(
                            text = if (currentUserId == item.userId) stringResource(id = R.string.point_leaderboard_screen_user_name_text) else if (item.user.fullName.trim() == "") stringResource(
                                R.string.point_leaderboard_screen_anonymous_user_text
                            ) else item.user.fullName,
                            style = AppTheme.typography.pointsUserTextStyle,
                            color = if (currentUserId == item.userId) White else colors.textPrimary,
                            lineHeight = 18.sp,
                            maxLines = 1,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier.padding(start = 4.dp, end = 16.dp),
                        )
                    }

                    Row(
                        modifier = Modifier.wrapContentWidth(),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.End
                    ) {
                        Image(
                            painter =
                            if (currentUserId == item.userId) {
                                painterResource(id = R.drawable.ic_start_coin)
                            } else if (isDarkMode) {
                                painterResource(id = R.drawable.ic_star_coin_dark)
                            } else {
                                painterResource(id = R.drawable.ic_star_coin_main)
                            },
                            contentDescription = null,
                            contentScale = ContentScale.FillWidth,
                            modifier = Modifier.size(18.dp)

                        )

                        Text(
                            text = item.total_points.toString(),
                            style = AppTheme.typography.pointsUserTextStyle,
                            color = if (currentUserId == item.userId) White else colors.primary,
                            lineHeight = 16.sp,
                            modifier = Modifier
                                .padding(start = 4.dp, end = 12.dp)
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun PointsSelectionTabView(viewModel: LeaderboardViewModel) {
    val currentSelectedTab by viewModel.currentSelectedTab.collectAsState()

    Row(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(vertical = 24.dp, horizontal = 40.dp)
            .background(if (isDarkMode) darkPointsItemBg else colors.primary.copy(0.14f), shape = RoundedCornerShape(15.dp)),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        Box(
            modifier = Modifier
                .weight(1f)
                .motionClickEvent {
                    viewModel.handleTabItemSelection(DurationType.DURATION_WEEK)
                }
                .background(
                    if (currentSelectedTab == DurationType.DURATION_WEEK) ColorPrimary else Color.Transparent,
                    RoundedCornerShape(15.dp)
                )
                .padding(horizontal = 4.dp)
                .clip(RoundedCornerShape(15.dp))
        ) {
            Text(
                text = stringResource(R.string.point_leaderboard_screen_week_tab_title),
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(8.dp),
                color = if (currentSelectedTab == DurationType.DURATION_WEEK) White else colors.textPrimary,
                style = AppTheme.typography.subscriptionListDaysTextStyle,
                lineHeight = 16.sp,
                textAlign = TextAlign.Center
            )
        }
        Box(
            modifier = Modifier
                .weight(1f)
                .motionClickEvent {
                    viewModel.handleTabItemSelection(DurationType.DURATION_MONTH)
                }
                .background(
                    if (currentSelectedTab == DurationType.DURATION_MONTH) ColorPrimary else Color.Transparent,
                    RoundedCornerShape(15.dp)
                )
                .clip(RoundedCornerShape(15.dp))
        ) {
            Text(
                text = stringResource(R.string.point_leaderboard_screen_month_tab_title),
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(8.dp),
                color = if (currentSelectedTab == DurationType.DURATION_MONTH) White else colors.textPrimary,
                style = AppTheme.typography.subscriptionListDaysTextStyle,
                lineHeight = 16.sp,
                textAlign = TextAlign.Center
            )
        }
        Box(
            modifier = Modifier
                .weight(1f)
                .motionClickEvent {
                    viewModel.handleTabItemSelection(DurationType.DURATION_ALL)
                }
                .background(
                    if (currentSelectedTab == DurationType.DURATION_ALL) ColorPrimary else Color.Transparent,
                    RoundedCornerShape(15.dp)
                )
                .clip(RoundedCornerShape(15.dp))
        ) {
            Text(
                text = stringResource(R.string.point_leaderboard_screen_all_tab_title),
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(8.dp),
                color = if (currentSelectedTab == DurationType.DURATION_ALL) White else colors.textPrimary,
                style = AppTheme.typography.subscriptionListDaysTextStyle,
                lineHeight = 16.sp,
                textAlign = TextAlign.Center
            )
        }
    }
}

@Composable
fun PointsRankView(
    modifier: Modifier = Modifier,
    painter: Painter,
    points: UserPoints,
    currentUserId: Any,
    showKingCrownView: Boolean = false
) {
    Column(
        modifier = modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Box(
            contentAlignment = Alignment.Center
        ) {

            if (showKingCrownView) {
                Image(
                    painter = painterResource(id = R.drawable.ic_king_icon),
                    contentDescription = null,
                    contentScale = ContentScale.Fit,
                    modifier = Modifier
                        .fillMaxSize(0.5f)
                        .align(Alignment.TopCenter)
                        .offset(y = (-28).dp, x = 0.dp)
                )
            }

            ProfileImageView(
                modifier = Modifier
                    .border(
                        if (points.rank == 1) 5.dp else 3.dp, pointProfileBorder, CircleShape
                    )
                    .align(Alignment.TopCenter)
                    .fillMaxWidth(0.8f)
                    .aspectRatio(1f),
                data = points.user.profileImageUrl,
                char = points.user.profileImageChar()
            )

            Image(
                painter = painter,
                contentDescription = null,
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .fillMaxSize(0.3f)
                    .offset(y = 8.dp, x = 0.dp)
            )
        }

        Text(
            text = if (currentUserId == points.userId) stringResource(id = R.string.point_leaderboard_screen_user_name_text) else if (points.user.fullName.trim() == "") stringResource(
                R.string.point_leaderboard_screen_anonymous_user_text
            ) else points.user.fullName.trim(),
            modifier = Modifier.padding(top = 8.dp, start = 4.dp, end = 4.dp),
            maxLines = 1,
            overflow = TextOverflow.Ellipsis,
            letterSpacing = -(0.5.sp),
            textAlign = TextAlign.Center,
            lineHeight = 23.sp,
            style = AppTheme.typography.pointUserNameTextStyle,
            color = colors.textPrimary,
        )

        Row(
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically,
        ) {
            Image(
                painter = if (isDarkMode) painterResource(id = R.drawable.ic_star_coin_champ_dark) else painterResource(id = R.drawable.ic_star_coin_main),
                contentDescription = null,
                contentScale = ContentScale.FillWidth,
                modifier = Modifier.size(18.dp)
            )

            Spacer(modifier = Modifier.width(4.dp))

            Text(
                text = points.total_points.toString(),
                style = AppTheme.typography.pointsUserTextStyle,
                color = colors.primary,
                lineHeight = 16.sp,
                letterSpacing = -(0.5.sp),
                textAlign = TextAlign.Center
            )
        }
    }
}

@Preview
@Composable
fun PreviewLeaderBoardScreen() {
    LeaderboardView()
}

private val darkHeaderBg = Color(0xFF353341)
private val lightHeaderBg = Color(0xFFFDF0EE)
private val darkPointsItemBg = Color(0x14E8F5F9)
private val pointProfileBorder = Color(0xFFFFBA57)
private val lightListBg = Color(0xFFE8F5F9)
private val darkListBg = Color(0xFF313131)
private val lightProfileBorder = Color(0x331C191F)
private val pointsItemBg = Color(0xFF008CB7)
