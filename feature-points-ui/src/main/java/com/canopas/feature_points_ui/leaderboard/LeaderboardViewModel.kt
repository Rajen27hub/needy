package com.canopas.feature_points_ui.leaderboard

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.feature_points_data.model.DurationType
import com.canopas.feature_points_data.model.UserPoints
import com.canopas.feature_points_data.rest.PointService
import com.canopas.feature_points_ui.PointsNavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LeaderboardViewModel @Inject constructor(
    private val navManager: PointsNavManager,
    private val service: PointService,
    private val appDispatcher: AppDispatcher,
    private val appAnalytics: AppAnalytics,
    authManager: AuthManager,
    private val resources: Resources
) : ViewModel() {

    val currentSelectedTab = MutableStateFlow(DurationType.DURATION_WEEK)

    val state = MutableStateFlow<PointState>(PointState.LOADING)
    var userId = MutableStateFlow(authManager.currentUser?.id ?: "")

    fun getLeaderBoard() = viewModelScope.launch {
        appAnalytics.logEvent("view_tab_leaderboard")
        if (state.value !is PointState.SUCCESS) {
            state.value = PointState.LOADING
        }
        withContext(appDispatcher.IO) {
            service.getPoints(duration = currentSelectedTab.value.title).onSuccess { points ->
                val currentUserList = mutableListOf<UserPoints>()
                points.drop(3).forEach {
                    if (it.userId == userId.value && it.rank >= 4) {
                        currentUserList.add(0, it)
                    } else if (it.userId != userId.value && it.rank > 3) {
                        currentUserList.add(it)
                    }
                }
                state.tryEmit(PointState.SUCCESS(points.take(3), currentUserList))
            }.onFailure {
                Timber.e(it.toString())
                state.tryEmit(PointState.FAILURE(it.toUserError(resources)))
            }
        }
    }

    fun onBackClick() {
        navManager.popBackStack()
    }

    fun handleTabItemSelection(selectedTab: DurationType) {
        currentSelectedTab.tryEmit(selectedTab)
        when (currentSelectedTab.value) {
            DurationType.DURATION_WEEK -> {
                appAnalytics.logEvent("tap_leaderboard_7_days")
            }
            DurationType.DURATION_MONTH -> {
                appAnalytics.logEvent("tap_leaderboard_30_days")
            }
            DurationType.DURATION_ALL -> {
                appAnalytics.logEvent("tap_leaderboard_all_days")
            }
        }
        getLeaderBoard()
    }
}

sealed class PointState {
    object START : PointState()
    object LOADING : PointState()
    data class SUCCESS(
        val firstThreeUsersList: List<UserPoints>,
        val remainingUsersList: MutableList<UserPoints>
    ) : PointState()

    data class FAILURE(val message: String) : PointState()
}
