package com.canopas.feature_community_data.di

import com.canopas.feature_community_data.rest.CommunityService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RestModule {

    @Singleton
    @Provides
    fun provideService(@Named("ApiRetrofit") retrofit: Retrofit): CommunityService =
        retrofit.create(CommunityService::class.java)
}
