package com.canopas.feature_community_data.di

import android.util.LruCache
import com.canopas.feature_community_data.model.Community
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CommunityCache @Inject constructor() {
    private val communityCache = LruCache<String, Community>(25)

    fun put(community: Community) {
        communityCache.put(community.id.toString(), community)
    }

    fun getCommunity(communityId: String): Community? {
        return communityCache.get(communityId) ?: null
    }
}
