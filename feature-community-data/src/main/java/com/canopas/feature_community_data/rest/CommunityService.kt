package com.canopas.feature_community_data.rest

import com.canopas.base.data.model.UserAccount
import com.canopas.data.model.ProgressHistory
import com.canopas.data.model.Subscription
import com.canopas.feature_community_data.model.AddCommunity
import com.canopas.feature_community_data.model.Community
import com.canopas.feature_community_data.model.CommunityJoinRequest
import com.canopas.feature_community_data.model.CommunityRequest
import com.canopas.feature_community_data.model.JoinRequest
import com.canopas.feature_community_data.model.PendingInvitation
import com.canopas.feature_community_data.model.RemoveUsers
import com.canopas.feature_community_data.model.ShareSubscriptions
import com.canopas.feature_community_data.model.UpdateCommunityMembersRole
import com.canopas.feature_community_data.model.UpdateSharePrompt
import com.canopas.feature_community_data.model.UploadCommunityProfile
import com.google.gson.JsonElement
import okhttp3.MultipartBody
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.HTTP
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface CommunityService {

    @GET("user/v1/communities")
    suspend fun getCommunities(): Result<List<Community>>

    @POST("user/v1/communities")
    suspend fun createCommunity(@Body communityDetails: AddCommunity): Result<Community>

    @GET("user/v1/communities/{id}")
    suspend fun getCommunity(@Path("id") Id: Int): Result<Community>

    @GET("user/v1/users/search")
    suspend fun searchMembers(
        @Query("searchStr") search: String,
        @Query("communityId") id: String
    ): Result<List<UserAccount>>

    @PUT("user/v1/communities/{id}/users")
    suspend fun addMembersToCommunity(
        @Path("id") Id: Int,
        @Body usersList: List<UpdateCommunityMembersRole>,
    ): Result<JsonElement>

    @POST("user/v2/communities/requests")
    suspend fun sendCommunityJoinRequest(@Body joinRequest: JoinRequest): Result<JsonElement>

    @GET("user/v1/communities/{id}/subscriptions/{user_id}")
    suspend fun getSharedSubscriptionsList(
        @Path("id") communityId: String,
        @Path("user_id") userId: String,
    ): Result<List<Subscription>>

    @GET("user/v2/activities/subscriptions")
    suspend fun getSubscriptionsToShare(@Query("fetch_all") flag: Boolean = false): Result<List<Subscription>>

    @PUT("user/v1/communities/{id}/subscriptions")
    suspend fun shareSubscriptionsWithCommunity(
        @Path("id") id: Int,
        @Body subscriptions: ShareSubscriptions,
    ): Result<JsonElement>

    @GET("user/v1/communities/{id}/users/{u_id}/subscription/{s_id}")
    suspend fun retrieveUserSubscriptionById(
        @Path("id") communityId: String,
        @Path("u_id") userId: String,
        @Path("s_id") subscriptionId: String,
    ): Result<Subscription>

    @GET("user/v2/activities/subscription/progress/history/{user_id}/{id}")
    suspend fun getSubscriptionsProgressHistory(
        @Path("user_id") userId: Int,
        @Path("id") subscriptionId: Int,
        @Query("end") end: Long,
        @Query("start") start: Long
    ): Result<List<ProgressHistory>>

    @PUT("user/v1/communities/{id}")
    suspend fun updateCommunity(
        @Path("id") Id: Int,
        @Body communityDetails: AddCommunity
    ): Result<Community>

    @DELETE("user/v1/communities/{id}/users/self")
    suspend fun leaveCommunity(@Path("id") Id: Int): Result<JsonElement>

    @Multipart
    @POST("user/v1/image")
    suspend fun uploadCommunityProfile(@Part param: MultipartBody.Part): Result<UploadCommunityProfile>

    @HTTP(method = "DELETE", path = "user/v1/communities/{id}/users", hasBody = true)
    suspend fun removeUsersFromCommunity(
        @Path("id") communityId: Int,
        @Body user_ids: RemoveUsers
    ): Result<JsonElement>

    @PUT("user/v1/communities/{id}/users/{user_id}")
    suspend fun updateSharePromptedFlag(
        @Path("id") communityId: String,
        @Path("user_id") userId: String,
        @Body promptStatus: UpdateSharePrompt
    ): Result<JsonElement>

    @GET("user/v1/communities/requests")
    suspend fun getCommunityRequests(): Result<List<CommunityRequest>>

    @PUT("user/v1/communities/request")
    suspend fun updateCommunityRequest(@Body jointRequest: CommunityJoinRequest): Result<JsonElement>

    @GET("user/v1/communities/requests/{community_id}")
    suspend fun getPendingInvitation(@Path("community_id") communityId: Int): Result<List<PendingInvitation>>

    @DELETE("user/v1/communities/requests/{community_id}/{receiver_id}")
    suspend fun deletePendingInvitation(
        @Path("community_id") communityId: Int,
        @Path("receiver_id") receiverId: Int
    ): Result<JsonElement>
}
