package com.canopas.feature_community_data.model

import com.google.gson.annotations.SerializedName

data class PendingInvitation(
    val id: Int,
    val first_name: String,
    val last_name: String?,
    @SerializedName("profile_image_url")
    val profileUrl: String
)
