package com.canopas.feature_community_data.model

import com.google.gson.annotations.SerializedName

data class CommunityRequest(
    val id: Int,
    val name: String,
    val description: String,
    @SerializedName("image_url")
    val imageUrl: String,
    @SerializedName("members_count")
    val memberCount: Int,
    val created_at: String,
    val updated_at: String,
    val sender: Sender
)

data class Sender(
    val id: Int,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String?,
    @SerializedName("profile_image_url")
    val profileUrl: String
)

data class CommunityJoinRequest(
    @SerializedName("community_id")
    val communityId: Int,
    @SerializedName("sender_id")
    val senderId: Int,
    val status: Boolean
)
