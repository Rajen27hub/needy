package com.canopas.feature_community_data.model

import com.canopas.base.data.model.UserAccount
import com.google.gson.annotations.SerializedName

data class Community(
    val id: Int,
    val name: String,
    val description: String,
    var image_url: String,

    @SerializedName("created_at")
    var createdAt: String,

    @SerializedName("updated_at")
    var updatedAt: String,

    var users: List<UserAccount>
)

data class AddCommunity(
    val name: String,
    val description: String,
    val image_url: String,
)

data class JoinRequest(
    @SerializedName("community_id")
    val communityId: Int,
    @SerializedName("receiver_ids")
    val receiverIds: List<Int>
)

data class UpdateCommunityMembersRole(
    var id: Int,
    var role: Int
)

data class ShareSubscriptions(
    val subscription_ids: List<Int>
)

data class UploadCommunityProfile(
    var image_url: String
)

data class RemoveUsers(
    val user_ids: List<Int>
)

data class UpdateSharePrompt(
    @SerializedName("activity_share_prompted")
    val community_activity_share_prompted: Boolean
)
