package com.canopas.feature_community_data

import com.canopas.base.data.model.Notifications
import com.canopas.base.data.model.Progress
import com.canopas.base.data.model.Recent
import com.canopas.base.data.model.Session
import com.canopas.base.data.model.StreaksData
import com.canopas.base.data.model.Total
import com.canopas.base.data.model.UserAccount
import com.canopas.data.model.ActivityData
import com.canopas.data.model.ActivityMedia
import com.canopas.data.model.BasicActivity
import com.canopas.data.model.Benefit
import com.canopas.data.model.Category
import com.canopas.data.model.HighlightMetrics
import com.canopas.data.model.Highlights
import com.canopas.data.model.Notes
import com.canopas.data.model.Quotes
import com.canopas.data.model.Subscription
import com.canopas.feature_community_data.model.AddCommunity
import com.canopas.feature_community_data.model.Community
import com.canopas.feature_community_data.model.CommunityRequest
import com.canopas.feature_community_data.model.PendingInvitation
import com.canopas.feature_community_data.model.Sender
import com.canopas.feature_community_data.model.UploadCommunityProfile

class TestUtils {
    companion object {

        val session = Session(
            1, 14, deviceType = 1, "1", "MD1",
            "", 20000, "", "1", oldRefreshToken = "",
            limitedAccess = false, lastAccessedOn = 1641987105
        )
        val notification = Notifications(
            pushNotification = true, mail = false, inactivityMail = true, inactivityNotification = false
        )

        val user = UserAccount(
            1, 14, 1, 1, "abc", "",
            "userName", "12345", "dummy@gmail.com", "",
            isActive = true, isAnonymous = true,
            createdAt = "123", updatedAt = "456", gender = 1, userType = 0, 1, true,
            "", session, notification
        )

        const val timezone = "Asia/Kolkata"
        private val medias = ActivityMedia(
            1, 11,
            "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
            1
        )

        private val note = Notes(1, 10, 1, "Note", false, "", "")

        private val quotes = Quotes(
            1, 11,
            "Yoga is the journey of the self, through the self, to the self.",
            "The Bhagavad Gita"
        )

        private val benefit = Benefit(
            1, 11,
            "https://i.ibb.co/C6tWgbB/001-strength.png",
            "Yoga improves strength, balance and flexibility."
        )

        private val highlights = Highlights(
            id = 1, userId = 14, activityId = 1, type = 5,
            highlight_metrics = listOf(
                HighlightMetrics(
                    highlightId = 1,
                    previousStart = "2022-11-01T00:00:00Z",
                    previousEnd = "2022-11-30T23:59:59Z",
                    currentStart = "2022-12-01T00:00:00Z",
                    currentEnd = "2022-12-31T23:59:59Z",
                    criteria = 1,
                    previousReading = 1.75F,
                    currentReading = 1.5F
                )
            )
        )

        val activityData = ActivityData(
            1,
            1,
            "yoga",
            "Yoga is a mirror to look at ourselves from within",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            false,
            listOf(medias),
            listOf(quotes),
            listOf(benefit),
            listOf(highlights),
            false,
            "Inspiration",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png"
        )

        private val basicActivity = BasicActivity(
            1,
            1,
            "yoga",
            "Yoga is a mirror to look at ourselves from within",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            false,
        )

        val category = Category(
            1, "Refreshing activities", "this is description",
            "", false, listOf(activityData)
        )

        val subscription =
            Subscription(
                1,
                1,
                activityData.id,
                Progress(listOf(Recent(1649918051, true)), Total(0, 3, 0, 68f)),
                listOf(1649918051, 1650017000),
                timezone,
                1641340800,
                1641340821,
                is_active = true,
                is_prompted = false,
                is_custom = false,
                activity = basicActivity, streak = 2,
                listOf(
                    StreaksData(1641340800, 1641340821, 5, true)
                ),
                1,
                "",
                note,
                listOf(note),
            )

        private val sessionCommunity = Session(
            1, 1, deviceType = 1, "1", "MD1",
            "", 20000, "", "1", oldRefreshToken = "",
            limitedAccess = false, lastAccessedOn = 1641987105
        )
        val notifications = Notifications(
            pushNotification = true, mail = false, inactivityMail = true, inactivityNotification = false
        )

        val userCommunity = UserAccount(
            1, 1, 1, 1, "abc", "",
            "userName", "12345", "dummy@gmail.com", "",
            isActive = true, isAnonymous = true,
            createdAt = "123", updatedAt = "456", gender = 1, userType = 0, 1, true,
            "", sessionCommunity, notifications
        )

        val community =
            Community(1, "Canopas", "employee habit", "", "123", "789", listOf(userCommunity))

        val communities =
            Community(1, "canopas", "employee habit", "", "123", "789", listOf(userCommunity))

        val community2 = Community(
            1,
            "Canopas",
            "employee habit",
            "https://nolonely.s3.ap-south-1.amazonaws.com/users/__w-180__/46d8ed88-7053-440b-b89d-ef7d263e9aef.jpg",
            "123",
            "789",
            listOf(userCommunity)
        )
        val communityRequest = CommunityRequest(
            1, "xyz", "",
            "https://nolonely.s3.ap-south-1.amazonaws.com/users/__w-180__/46d8ed88-7053-440b-b89d-ef7d263e9aef.jpg", 12, "123", "523",
            Sender(2, "gggg", "xxxx", "https://picsum.photos/id/237/200/300")
        )
        val communityRequest2 = CommunityRequest(
            4, "fddf", "",
            "https://nolonely.s3.ap-south-1.amazonaws.com/users/__w-180__/46d8ed88-7053-440b-b89d-ef7d263e9aef.jpg", 10, "123", "523",
            Sender(5, "sdfsd", "xxdsfsfxx", "https://picsum.photos/id/237/200/300")
        )

        val communityData = AddCommunity("Canopas", "", "")

        val uploadProfile = UploadCommunityProfile(community2.image_url)

        val pendingInvitation = PendingInvitation(
            1, "dgsfg", "fgdgh", "https://nolonely.s3.ap-south-1.amazonaws.com/users/__w-180__/46d8ed88-7053-440b-b89d-ef7d263e9aef.jpg"
        )
        val pendingInvitation2 = PendingInvitation(
            3, "dfs", "fgdhgjkgh", "https://nolonely.s3.ap-south-1.amazonaws.com/users/__w-180__/46d8ed88-7053-440b-b89d-ef7d263e9aef.jpg"
        )
    }
}
