package com.canopas.data.model

data class Error(
    val status: String,
    val message: String
)
