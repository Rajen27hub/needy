package com.canopas.data.model

import com.google.gson.annotations.SerializedName

data class PremiumSubscriptionBody(
    @SerializedName("package_name")
    var packageName: String,

    @SerializedName("product_id")
    var productId: String,

    @SerializedName("purchase_token")
    var purchaseToken: String,
)
