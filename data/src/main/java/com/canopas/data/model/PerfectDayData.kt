package com.canopas.data.model

data class PerfectDayData(
    val background: Int,
    val image: Int
)
