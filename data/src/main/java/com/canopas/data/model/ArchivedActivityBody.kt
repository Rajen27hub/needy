package com.canopas.data.model

import com.google.gson.annotations.SerializedName

data class RestoreActivityBody(
    @SerializedName("subscription_id")
    val subscriptionId: Int
)

data class DeleteActivityBody(
    @SerializedName("subscription_ids")
    val subscriptionIds: List<Int>
)
