package com.canopas.data.model

import com.google.gson.annotations.SerializedName

data class CompletionBody(
    val subscription_id: Int,
    val completion_time: Long,
    @SerializedName("tz")
    val timeZone: String
)
