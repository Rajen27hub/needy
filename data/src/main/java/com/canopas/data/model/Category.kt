package com.canopas.data.model

import com.google.gson.annotations.SerializedName

data class Category(
    val id: Int,
    val name: String,
    val description: String,
    val image_url: String,
    val is_active: Boolean,
    val activities: List<ActivityData>
)

data class ActivityData(
    val id: Int,
    val category_id: Int,
    val name: String,
    val description: String,
    val image_url: String,
    val color: String,
    val image_url2: String?,
    val color2: String,
    val is_completed: Boolean,
    @SerializedName("activity_medias")
    val medias: List<ActivityMedia>?,
    @SerializedName("activity_quotes")
    val quotes: List<Quotes>?,
    @SerializedName("activity_benefits")
    val benefits: List<Benefit>?,
    @SerializedName("highlights")
    val highlights: List<Highlights>? = null,
    @SerializedName("is_subscribed")
    val isSubscribed: Boolean = false,
    val inspiration: String? = null,
    val inspiration_image_url: String? = null,
    val tip: String? = null,
    val default_activity_time: String? = null,
    val default_activity_duration: Int? = null,

) {
    @JvmName("getMedias1")
    fun getMedias(): List<ActivityMedia> {
        return medias ?: emptyList()
    }

    val getQuotes get() = quotes ?: emptyList()
    val getBenefits get() = benefits ?: emptyList()
}

data class BasicActivity(
    val id: Int,
    val category_id: Int,
    val name: String,
    val description: String,
    val image_url: String,
    val color: String,
    val image_url2: String?,
    val color2: String,
    val is_completed: Boolean,
    val tip: String? = null,
    @SerializedName("highlights")
    val highlights: List<Highlights>? = null,
)

const val MEDIA_TYPE_IMAGE = 0
const val MEDIA_TYPE_VIDEO = 1
const val MEDIA_TYPE_GIF = 2

data class ActivityMedia(
    val id: Int,
    @SerializedName("activity_id")
    val activityId: Int,
    val url: String,
    val type: Int
)

data class Benefit(
    val id: Int,
    @SerializedName("activity_id")
    val activityId: Int,
    @SerializedName("image_url")
    val imageUrl: String,
    val benefit: String,
    @SerializedName("align_right")
    val isAlignRight: Boolean = true
)

data class Quotes(
    val id: Int,
    @SerializedName("activity_id")
    val activityId: Int,
    val quote: String,
    @SerializedName("author_name")
    val authorName: String
)

data class Highlights(
    val id: Int,
    @SerializedName("user_id")
    val userId: Int,
    @SerializedName("activity_id")
    val activityId: Int,
    @SerializedName("type")
    val type: Int,
    @SerializedName("highlight_metrics")
    val highlight_metrics: List<HighlightMetrics> = emptyList()
)

data class HighlightMetrics(
    @SerializedName("highlight_id")
    val highlightId: Int,

    @SerializedName("previous_start")
    var previousStart: String,

    @SerializedName("previous_end")
    var previousEnd: String,

    @SerializedName("current_start")
    var currentStart: String,

    @SerializedName("current_end")
    var currentEnd: String,

    @SerializedName("criteria")
    val criteria: Int,

    @SerializedName("previous_reading")
    val previousReading: Float,

    @SerializedName("current_reading")
    val currentReading: Float,
)
