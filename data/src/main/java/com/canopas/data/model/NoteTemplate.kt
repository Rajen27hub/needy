package com.canopas.data.model

import com.google.gson.annotations.SerializedName

const val TYPE_TEMPLATE_CURATED = 1
const val TYPE_TEMPLATE_CUSTOM = 2

data class NoteTemplate(
    val id: Int,
    val type: Int,
    val title: String,
    val format: String,
    val index: Int,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("updated_at")
    val updatedAt: String
)

data class AddOrEditNoteTemplate(
    val title: String,
    val format: String
)
