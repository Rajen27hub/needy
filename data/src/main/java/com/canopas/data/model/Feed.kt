package com.canopas.data.model

const val FEED_TYPE_MOTIVATIONAL_MATERIAL = 1
const val FEED_TYPE_STORY = 2

data class Feed(
    val categories: List<Category>,
    val motivational_materials: List<MotivationalMaterial>
)

data class FeaturedFeed(
    val type: Int,
    val motivational_material: MotivationalMaterial,
    val story: Story
)
