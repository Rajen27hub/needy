package com.canopas.data.model

data class ProgressHistory(
    val date: Long,
    val completed: Boolean
)
