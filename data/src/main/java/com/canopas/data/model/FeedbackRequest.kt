package com.canopas.data.model

import com.canopas.base.data.model.DEVICE_TYPE

data class FeedbackRequest(
    val title: String,
    val description: String,
    var device_type: Int = DEVICE_TYPE,
    var app_version: String,
    var device_name: String,
    var os_version: String,
    val storyId: String? = null
)
