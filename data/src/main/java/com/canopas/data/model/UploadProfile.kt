package com.canopas.data.model

data class UploadProfile(
    var image_url: String
)
