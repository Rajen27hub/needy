package com.canopas.data.model

import com.google.gson.annotations.SerializedName

data class Goal(
    val id: Int,
    @SerializedName("subscription_id")
    val subscriptionId: Int,
    @SerializedName("activity_name")
    val activityName: String?,
    val title: String? = null,
    val description: String? = null,
    val type: Int,
    @SerializedName("due_date")
    val dueDate: String,
    val occurrences: Int,
    val completions: Int,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("updated_at")
    val updatedAt: String,
    @SerializedName("wall_visibility")
    val wallVisibility: Int = GOAL_WALL_VISIBILITY_WHEN_ACTIVE
) {
    fun getProgress(): Float {
        return ((completions * 100f) / occurrences) / 100f
    }
    fun getProgressText(): String {
        val progress = getProgress() * 100f
        return kotlin.math.round(progress).toString().substringBefore('.') + "%"
    }
}

data class SubscriptionGoal(
    val title: String,
    val type: Int
)

data class AddOrUpdateGoal(
    @SerializedName("subscription_id")
    val subscriptionId: Int,
    val title: String,
    val description: String,
    val type: Int,
    @SerializedName("due_date")
    val dueDate: String,
    @SerializedName("wall_visibility")
    val wallVisibility: Int = GOAL_WALL_VISIBILITY_WHEN_ACTIVE
)

data class GoalWithColors(
    val goal: Goal,
    val activityName: String,
    val subscriptionId: Int,
    val color2: String? = null,
    val color: String,
    var isCompleted: Boolean = false
)

data class GoalActivityTab(
    val tabName: String,
    val tabId: Int

)

data class DueDateGoals(
    val activeGoals: Map<String, List<Goal>>,
    val inactiveGoals: Map<String, List<Goal>>
)
