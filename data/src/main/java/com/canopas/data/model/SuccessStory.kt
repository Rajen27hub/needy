package com.canopas.data.model

import com.canopas.base.data.model.UserAccount

data class SuccessStory(
    val id: Int,
    val user_id: Int,
    var title: String,
    var description: String,
    var activity_ids: String,
    val activities: List<ActivityData>,
    val user: UserAccount
)
