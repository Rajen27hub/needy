package com.canopas.data.model

const val MOTIVATIONAL_TYPE_BLOGS = 1
const val MOTIVATIONAL_TYPE_VIDEOS = 2
const val MOTIVATIONAL_TYPE_PODCASTS = 3

data class MotivationalMaterial(
    var id: Int,
    var title: String,
    var subtitle: String?,
    var description: String,
    var image_url: String,
    var type: Int,
    var media_url: String,
    var author: String,
    var is_published: Boolean,
    var created_at: String,
    var updated_at: String
)
