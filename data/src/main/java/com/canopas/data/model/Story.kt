package com.canopas.data.model

const val TYPE_STORY_DETAIL_TEXT = 1
const val TYPE_STORY_DETAIL_ACTIVITY = 2
const val TYPE_STORY_DETAIL_QUOTE = 4

data class Story(
    val id: Int,
    val title: String,
    val subtitle: String,
    val description: String,
    val image_url: String,
    val is_published: Boolean,
    val created_at: String,
    val updated_at: String,
    val published_at: String,
    val details: List<Details>
)

data class Details(
    val id: Int,
    val story_id: Int,
    val type: Int,
    val label: String,
    val description: String,
    val entity_id: Int,
    val index: Int,
    val is_subscribed: Boolean,
    val activity: ActivityDetails? = null,
    val quote: String
)

data class ActivityDetails(
    val id: Int,
    val name: String,
    val color2: String,
    val image_url2: String
)
