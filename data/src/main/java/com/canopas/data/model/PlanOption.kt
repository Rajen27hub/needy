package com.canopas.data.model

import com.android.billingclient.api.ProductDetails

//const val SELECTED_PAN = 0

data class PlanOption(
    val type: PlanType = PlanType.PLAN_MONTHLY,
    val price: String? = "",
    val priceMicros: Long = 0,
    val productDetails: ProductDetails? = null,
    var isFreeTrialAvailable: Boolean = false
)

enum class PlanType(val id: String) {
    PLAN_MONTHLY("premium_1_month"),
    PLAN_YEARLY("premium_1_year");

    val title: String
        get() {
            return when (this) {
                PLAN_MONTHLY -> "Monthly"
                PLAN_YEARLY -> "Yearly"
            }
        }
}
