package com.canopas.data.model

import com.google.gson.annotations.SerializedName

data class SubscribeBody(
    var activity_id: Int,
    var timezone: String,
    var is_prompted: Boolean,
    var repeat_type: Int,
    var repeat_on: String? = null,
    var custom_activity: CustomActivityData? = null,
    val activity_time: String? = null,
    val activity_duration: Int? = null,
    val reminders: List<ActivityReminders>? = null,
    val show_note_on_complete: Boolean = false,
    val goal: SubscriptionGoal? = null
)

data class ActivityReminders(
    val type: Int,
    val time: String
)

data class DefaultSubscriptionBody(
    val activity_time: String,
    val timezone: String
)

data class AddOrUpdateNoteBody(
    @SerializedName("activity_id")
    var activityId: Int,

    var content: String,

    @SerializedName("is_custom")
    var isCustom: Boolean
)
