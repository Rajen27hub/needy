package com.canopas.data.model

import com.google.gson.annotations.SerializedName

const val TYPE_END_GOAL = 1
const val TYPE_SUB_GOAL = 2
const val GOAL_WALL_VISIBILITY_WHEN_ACTIVE = 1
const val GOAL_WALL_VISIBILITY_ALWAYS = 2
const val GOAL_WALL_VISIBILITY_NEVER = 3

data class CustomActivity(
    val name: String,
    val description: String,
    val color: String,
    val timeZone: String,
    var repeat_type: Int,
    var repeat_on: String? = null,
    val activity_time: String? = null,
    val activity_duration: Int? = null,
    val reminders: List<ActivityReminders>? = null,
    val show_note_on_complete: Boolean = false,
    val goal: SubscriptionGoal? = null
)

data class CustomActivityData(
    val name: String,
    val description: String = ""
)

data class SubscribedResponse(
    @SerializedName("subscription_id")
    val subscriptionId: Int
)
