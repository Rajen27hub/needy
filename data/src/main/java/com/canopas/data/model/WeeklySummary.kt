package com.canopas.data.model

import com.google.gson.annotations.SerializedName

data class WeeklySummary(
    val id: Int,
    val encoded_id: String,
    val email: String,
    @SerializedName("first_name")
    val firstName: String,
    @SerializedName("last_name")
    val lastName: String,
    @SerializedName("start_date")
    val startDate: String,
    @SerializedName("end_date")
    val endDate: String,
    @SerializedName("rank_point")
    val rankPoint: RankPoint,
    @SerializedName("subscription_summary")
    val SummaryList: List<SummaryDate>
)

data class RankPoint(
    @SerializedName("weekly_rank")
    val weeklyRank: Int,
    @SerializedName("weekly_points")
    val weekPoint: Int,
    @SerializedName("all_time_rank")
    val allTimeRank: Int,
    @SerializedName("all_time_points")
    val allTimePoints: Int
)

data class SummaryDate(
    val id: Int,
    @SerializedName("completed_days")
    val completedDays: Int,
    @SerializedName("total_days")
    val totalDays: Int,
    val percentage: Int,
    val progress: String,
    val color: String,
    val background: String,
    val activity: Activity
)

data class Activity(
    val id: Int,
    val name: String
)
