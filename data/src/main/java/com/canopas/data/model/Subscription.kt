package com.canopas.data.model

import com.canopas.base.data.model.Progress
import com.canopas.base.data.model.StreaksData
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.Locale

data class BasicSubscription(
    var id: Int,
    var user_id: Int,
    var activity_id: Int,
    var start_date: Long,
    var end_date: Long,
    var is_active: Boolean,
    var is_custom: Boolean,
    val activity: BasicActivity,
    val note: Notes? = null,
    val activity_time: String? = null,
    val activity_duration: Int? = null,
    @SerializedName("is_excluded")
    val isExcluded: Boolean = false,
    val show_note_on_complete: Boolean = false
) {
    fun activityTimeOrNull(): String? {
        return activity_time?.let {
            it.ifBlank { null }
        }
    }
}

data class Subscription(
    var id: Int,
    var user_id: Int,
    var activity_id: Int,
    var progress: Progress,
    var completion_days: List<Long>?,
    var timezone: String,
    var start_date: Long,
    var end_date: Long,
    var is_active: Boolean,
    var is_prompted: Boolean,
    var is_custom: Boolean,
    val activity: BasicActivity,
    val streak: Int,
    val streaks: List<StreaksData>,
    var repeat_type: Int,
    var repeat_on: String? = null,
    val note: Notes? = null,
    val notes: List<Notes>? = null,
    @SerializedName("is_excluded")
    val isExcluded: Boolean = false,
    val priority: Int = 0,
    val activity_time: String? = null,
    val activity_duration: Int? = null,
    val reminders: List<ActivityReminders>? = null,
    val show_note_on_complete: Boolean = false
) {
    fun getFormattedActivityTime(): String {
        val time = activity_time ?: return ""

        if (time.isBlank()) return ""

        val inputFormat = SimpleDateFormat("KK:mm", Locale.getDefault())
        val outputFormat = SimpleDateFormat("HH:mm", Locale.getDefault())
        return outputFormat.format(inputFormat.parse(time)!!)
    }
    fun activityTimeOrNull(): String? {
        return activity_time?.let {
            it.ifBlank { null }
        }
    }
}

data class Notes(
    var id: Int,

    @SerializedName("user_id")
    var userId: Int,

    @SerializedName("activity_id")
    var activityId: Int,

    var content: String,

    @SerializedName("is_custom")
    var isCustom: Boolean,

    @SerializedName("created_at")
    var createdAt: String,

    @SerializedName("updated_at")
    var updatedAt: String
)
