package com.canopas.data.model

data class PostSuccessStory(
    var title: String,
    var description: String,
    var activity_ids: List<Int>
)
