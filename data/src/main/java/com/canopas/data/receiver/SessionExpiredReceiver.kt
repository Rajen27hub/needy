package com.canopas.data.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.preferences.NoLonelyPreferences
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@AndroidEntryPoint
class SessionExpiredReceiver : BroadcastReceiver() {

    private val scope = CoroutineScope(Dispatchers.IO)

    @Inject
    lateinit var authManager: AuthManager

    @Inject
    lateinit var preferencesDataStore: NoLonelyPreferences

    override fun onReceive(context: Context, intent: Intent) {
        authManager.logOutUser()
        scope.launch {
            preferencesDataStore.setIsWelcomeScreenShown(false)

            withContext(Dispatchers.Main) {
                context.startActivity(
                    Intent("com.canopas.data.action_session_expired_ui")
                        .setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                )
            }
        }
    }
}
