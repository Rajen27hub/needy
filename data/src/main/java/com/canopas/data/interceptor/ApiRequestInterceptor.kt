package com.canopas.data.interceptor

import android.content.Context
import android.content.Intent
import com.canopas.base.data.auth.ACCESS_TOKEN
import com.canopas.base.data.auth.AUTH_REQUEST_CODE_UNAUTHORIZED
import com.canopas.base.data.auth.AuthManager
import com.canopas.data.receiver.SessionExpiredReceiver
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ApiRequestInterceptor @Inject internal constructor(
    @ApplicationContext private val context: Context,
    private val authManager: AuthManager
) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val accessToken = authManager.accessToken

        if (!accessToken.isNullOrEmpty()) {
            request = request.newBuilder().addHeader(ACCESS_TOKEN, accessToken).build()
        }

        val response = chain.proceed(request)
        if (!authManager.authState.isAuthorised && response.code == AUTH_REQUEST_CODE_UNAUTHORIZED) {
            context.sendBroadcast(Intent(context, SessionExpiredReceiver::class.java))
        }
        return response
    }
}
