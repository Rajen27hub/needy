package com.canopas.data

object FeatureFlag {
    const val isGoalsEnabled = true
    const val isNoteTemplatesEnabled = true
}
