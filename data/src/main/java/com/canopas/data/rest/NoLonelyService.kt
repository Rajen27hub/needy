package com.canopas.data.rest

import com.canopas.base.data.model.Notifications
import com.canopas.base.data.model.UserAccount
import com.canopas.data.model.ActivityData
import com.canopas.data.model.AddOrEditNoteTemplate
import com.canopas.data.model.AddOrUpdateGoal
import com.canopas.data.model.AddOrUpdateNoteBody
import com.canopas.data.model.BasicSubscription
import com.canopas.data.model.Category
import com.canopas.data.model.CompletionBody
import com.canopas.data.model.CustomActivity
import com.canopas.data.model.DefaultSubscriptionBody
import com.canopas.data.model.DeleteActivityBody
import com.canopas.data.model.FeaturedFeed
import com.canopas.data.model.Feed
import com.canopas.data.model.FeedbackRequest
import com.canopas.data.model.Goal
import com.canopas.data.model.NoteTemplate
import com.canopas.data.model.Notes
import com.canopas.data.model.PostSuccessStory
import com.canopas.data.model.PremiumSubscriptionBody
import com.canopas.data.model.ProgressHistory
import com.canopas.data.model.RestoreActivityBody
import com.canopas.data.model.Story
import com.canopas.data.model.SubscribeBody
import com.canopas.data.model.SubscribedResponse
import com.canopas.data.model.Subscription
import com.canopas.data.model.SuccessStory
import com.canopas.data.model.UploadProfile
import com.canopas.data.model.WeeklySummary
import com.google.gson.JsonElement
import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.HTTP
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Part
import retrofit2.http.Path
import retrofit2.http.Query

interface NoLonelyService {

    @GET("user/v1/feed")
    suspend fun getFeeds(): Result<Feed>

    @GET("user/v1/categories")
    suspend fun getAllCategories(): Result<List<Category>>

    @GET("user/v2/feed")
    suspend fun getFeaturedFeeds(
        @Query("skip") skip: Int = 0,
        @Query("limit") limit: Int = 10
    ): Result<List<FeaturedFeed>>

    @GET("user/v1/activities/{id}")
    suspend fun getActivityById(@Path("id") id: Int): Result<ActivityData>

    @GET("user/v1/users/{id}")
    suspend fun getUserDetailById(@Path("id") id: Int): Result<UserAccount>

    @GET("user/v2/feed/story/{id}")
    suspend fun getStoryById(@Path("id") storyId: Int): Result<Story>

    @PUT("user/v1/users/{id}")
    suspend fun updateUserById(
        @Path("id") id: Int,
        @Body userAccount: UserAccount
    ): Result<UserAccount>

    @GET("user/v2/activities/subscriptions")
    suspend fun getUserSubscriptions(@Query("fetch_all") flag: Boolean = false): Result<List<Subscription>>

    @GET("user/v1/activities/timeline")
    suspend fun getTodoTimeline(@Query("day") dayStartMillis: Long): Result<List<BasicSubscription>>

    @GET("user/v2/activities/subscription/progress/history/{user_id}/{id}")
    suspend fun getSubscriptionsProgressHistory(
        @Path("user_id") userId: Int,
        @Path("id") subscriptionId: Int,
        @Query("end") end: Long,
        @Query("start") start: Long
    ): Result<List<ProgressHistory>>

    @POST("user/v1/activities/subscription")
    suspend fun subscribeActivity(@Body subscribe: SubscribeBody): Result<SubscribedResponse>

    @DELETE("user/v1/activities/subscription/{id}")
    suspend fun unsubscribeActivity(@Path("id") subscriptionId: Int): Result<JsonElement>

    @Multipart
    @POST("user/v1/users/image")
    suspend fun uploadProfile(@Part param: MultipartBody.Part): Result<UploadProfile>

    @POST("user/v2/activities/daily-completion")
    suspend fun saveDailyCompletionActivity(@Body completion: CompletionBody): Result<JsonElement>

    @POST("user/v1/activities/daily-completion/past")
    suspend fun savePastCompletionActivity(@Body completion: CompletionBody): Result<JsonElement>

    @GET("user/v2/activities/subscription/{id}")
    suspend fun retrieveUserSubscriptionById(@Path("id") id: Int): Result<Subscription>

    @GET("user/v2/activities/subscriptions/activity/{id}")
    suspend fun retrieveUserSubscriptionByActivityId(@Path("id") id: Int): Result<Subscription>

    @POST("user/v1/success-stories")
    suspend fun postSuccessStory(@Body postStory: PostSuccessStory): Result<JsonElement>

    @PUT("user/v2/activities/subscription/{id}")
    suspend fun updateSubscriptionBodyBySubscriptionId(
        @Path("id") id: Int,
        @Body subscribeBody: SubscribeBody
    ): Result<JsonElement>

    @GET("user/v1/success-stories/activity/{id}")
    suspend fun retrieveSuccessStoriesByActivityId(@Path("id") id: Int): Result<List<SuccessStory>>

    @GET("user/v1/success-stories/{id}")
    suspend fun retrieveSuccessStoryDetailById(@Path("id") id: Int): Result<SuccessStory>

    @Multipart
    @POST("user/v1/user-feedbacks")
    suspend fun submitReport(
        @Part logParam: MultipartBody.Part,
        @Part imageParam: MultipartBody.Part?,
        @Part("data") request: FeedbackRequest
    ): Result<JsonElement>

    @GET("user/v1/success-stories")
    suspend fun retrieveSuccessStoriesByUserId(): Result<List<SuccessStory>>

    @DELETE("user/v1/success-stories/{id}")
    suspend fun deleteSuccessStory(@Path("id") id: Int): Result<JsonElement>

    @PUT("user/v1/success-stories/{id}")
    suspend fun updateSuccessStory(
        @Path("id") id: Int,
        @Body postStory: PostSuccessStory
    ): Result<JsonElement>

    @PUT("user/v1/subscription/playstore")
    suspend fun upgradeToPremiumSubscription(@Body premiumSubscription: PremiumSubscriptionBody): Response<UserAccount>

    @POST("user/v1/subscription/custom")
    suspend fun createCustomActivity(@Body customActivityRequest: CustomActivity): Result<SubscribedResponse>

    @GET("user/v1/activities/notes/{id}")
    suspend fun getNote(@Path("id") notedId: String): Result<Notes>

    @POST("user/v1/activities/notes")
    suspend fun addNote(@Body notesData: AddOrUpdateNoteBody): Result<JsonElement>

    @PUT("user/v1/activities/notes/{id}")
    suspend fun updateNote(
        @Path("id") notedId: String,
        @Body notesData: AddOrUpdateNoteBody
    ): Result<Notes>

    @DELETE("user/v1/activities/notes/{id}")
    suspend fun deleteNote(@Path("id") notedId: String): Result<JsonElement>

    @GET("user/v1/activities/{id}/notes")
    suspend fun getActivityNotes(
        @Path("id") id: Int,
        @Query("is_custom") isCustom: Boolean,
        @Query("skip") skip: Int = 0,
        @Query("limit") limit: Int = 30
    ): Result<List<Notes>>

    @GET("user/v1/notes/templates")
    suspend fun getNoteTemplates(): Result<List<NoteTemplate>>

    @GET("user/v1/notes/templates/{id}")
    suspend fun getNoteTemplateById(@Path("id") templateId: Int): Result<NoteTemplate>

    @POST("user/v1/notes/templates")
    suspend fun createNoteTemplate(@Body noteTemplate: AddOrEditNoteTemplate): Result<JsonElement>

    @PUT("user/v1/notes/templates/{id}")
    suspend fun updateNoteTemplate(@Path("id") templateId: Int, @Body noteTemplate: AddOrEditNoteTemplate): Result<JsonElement>

    @DELETE("user/v1/notes/templates/{id}")
    suspend fun deleteNoteTemplate(@Path("id") templateId: Int): Result<JsonElement>

    @PUT("user/v1/notifications/settings")
    suspend fun updateNotificationsSettings(@Body notificationBody: Notifications): Result<JsonElement>

    @PUT("user/v1/leader-board/{visibility}")
    suspend fun updateLeaderboardVisibility(@Path("visibility") visibility: Boolean): Result<JsonElement>

    @GET("user/v2/activities/subscription/history")
    suspend fun getUserSubscriptionsHistory(): Result<List<Subscription>>

    @PUT("user/v1/activities/subscriptions/archived/restore")
    suspend fun restoreArchivedActivity(@Body subscriptionId: RestoreActivityBody): Result<JsonElement>

    @HTTP(method = "DELETE", path = "user/v1/activities/subscriptions/archived", hasBody = true)
    suspend fun deleteArchivedActivity(@Body subscriptionIds: DeleteActivityBody): Result<JsonElement>

    @GET("user/v1/activities/summary")
    suspend fun getWeeklyActivitiesSummary(@Query("start") startDate: Long, @Query("end") endDate: Long): Result<WeeklySummary>

    @POST("user/v1/activities/subscribe/default")
    suspend fun subscribeDefaultActivity(@Body defaultSubscription: DefaultSubscriptionBody): Result<JsonElement>

    @GET("user/v1/activities/featured")
    suspend fun getFeaturedActivities(): Result<List<ActivityData>>

    @GET("user/v1/subscription/goals")
    suspend fun getAllGoals(): Result<List<Goal>>

    @GET("user/v1/subscription/{id}/goals")
    suspend fun getGoalsBySubscriptionId(@Path("id") subscriptionId: Int): Result<List<Goal>>

    @POST("user/v1/subscription/goals")
    suspend fun createGoal(@Body goalBody: AddOrUpdateGoal): Result<JsonElement>

    @PUT("user/v1/subscription/goals/{id}")
    suspend fun updateGoal(@Path("id") goalId: Int, @Body goalBody: AddOrUpdateGoal): Result<JsonElement>

    @GET("user/v1/subscription/goals/{id}")
    suspend fun getGoalById(@Path("id") goalId: Int): Result<Goal>

    @DELETE("user/v1/subscription/goals/{id}")
    suspend fun deleteGoal(@Path("id") goalId: Int): Result<JsonElement>
}
