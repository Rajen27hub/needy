package com.canopas.data.exception

import android.content.res.Resources
import com.canopas.base.data.auth.DEFAULT_STATUS_CODE
import com.canopas.base.data.exception.ALREADY_COMPLETED
import com.canopas.base.data.exception.ALREADY_SUBSCRIBED
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.COMMUNITY_ADMIN_AUTHORISED
import com.canopas.base.data.exception.ErrorResponse
import com.canopas.base.data.exception.LIMITED_ACCESS
import com.canopas.base.data.exception.LINK_CREATED_ALREADY
import com.canopas.base.data.exception.MAX_LIMIT_REACHED
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.UserAccount
import com.canopas.data.model.PremiumSubscriptionBody
import com.canopas.data.rest.NoLonelyService
import dagger.Lazy
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class BuyPremiumResultCall @Inject constructor(
    private val service: Lazy<NoLonelyService>,
    private val resources: Resources
) {
    suspend fun upgradeToPremium(premiumSubscription: PremiumSubscriptionBody): UpgradeResponse {
        return try {
            val response = service.get().upgradeToPremiumSubscription(premiumSubscription)
            if (response.isSuccessful) {
                UpgradeResponse(response.body(), null, response.code(), resources)
            } else {
                Timber.e("Upgrade to premium unsuccessful with code:${response.code()}, body:${response.errorBody()}")
                if (response.code() == 422) {
                    val errorMessage = ErrorResponse(response).reason
                    UpgradeResponse(
                        null,
                        when (errorMessage) {
                            LIMITED_ACCESS -> APIError.LimitedAccess
                            MAX_LIMIT_REACHED -> APIError.MaxLimitReached
                            ALREADY_COMPLETED -> APIError.AlreadyCompleted
                            ALREADY_SUBSCRIBED -> APIError.AlreadySubscribed
                            LINK_CREATED_ALREADY -> APIError.LinkCreatedAlready
                            COMMUNITY_ADMIN_AUTHORISED -> APIError.CommunityAdminAuthorised
                            else -> APIError.UnknownError(response.raw(), null)
                        },
                        response.code(), resources
                    )
                } else {
                    UpgradeResponse(null, APIError.UnknownError(response.raw(), null), response.code(), resources)
                }
            }
        } catch (e: Exception) {
            Timber.e(e, "Network error")
            if (e is IOException) {
                UpgradeResponse(null, APIError.NetworkError, resources = resources)
            } else {
                UpgradeResponse(null, APIError.UnknownError(null, e), resources = resources)
            }
        }
    }
}

data class UpgradeResponse constructor(
    val userAccount: UserAccount?,
    val error: APIError?,
    val statusCode: Int = DEFAULT_STATUS_CODE,
    val resources: Resources
) {
    fun hasError(): Boolean {
        return error != null
    }

    val errorMessage = error?.toUserError(resources)
}
