package com.canopas.data.di

import com.canopas.base.data.Config.BASE_URL
import com.canopas.base.data.exception.ResultCallAdapterFactory
import com.canopas.base.data.interceptor.LoggingInterceptor
import com.canopas.base.data.interceptor.TokenAuthenticator
import com.canopas.data.interceptor.ApiRequestInterceptor
import com.canopas.data.rest.NoLonelyService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RestModule {

    @Named("ApiHttpClient")
    @Singleton
    @Provides
    fun provideOkHttpClient(
        apiRequestInterceptor: ApiRequestInterceptor,
        loggingInterceptor: LoggingInterceptor,
        tokenAuthenticator: TokenAuthenticator
    ): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(apiRequestInterceptor)
            .addInterceptor(loggingInterceptor)
            .authenticator(tokenAuthenticator)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()

    @Named("ApiRetrofit")
    @Singleton
    @Provides
    fun provideApiRetrofit(@Named("ApiHttpClient") okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder().client(okHttpClient)
            .addCallAdapterFactory(ResultCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()

    @Singleton
    @Provides
    fun provideService(@Named("ApiRetrofit") retrofit: Retrofit): NoLonelyService =
        retrofit.create(NoLonelyService::class.java)
}
