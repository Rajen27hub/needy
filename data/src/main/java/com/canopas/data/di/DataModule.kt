package com.canopas.data.di

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.SharedPreferencesMigration
import androidx.datastore.preferences.core.PreferenceDataStoreFactory
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.preferencesDataStoreFile
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.PREF_NO_LONELY
import com.canopas.base.data.utils.Device
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {
    @Singleton
    @Provides
    fun provideAppDispatcher(): AppDispatcher = AppDispatcher()

    @Provides
    @Singleton
    fun provideDevice(
        @ApplicationContext context: Context,
        @Named("app_version_code") versionCode: Long
    ): Device {
        return Device(context, versionCode)
    }

    @Provides
    @Singleton
    @Named(PREF_NO_LONELY)
    fun providePreferencesDataStore(@ApplicationContext appContext: Context): DataStore<Preferences> =
        PreferenceDataStoreFactory.create(
            migrations = listOf(SharedPreferencesMigration(appContext, PREF_NO_LONELY)),
            produceFile = {
                appContext.preferencesDataStoreFile(PREF_NO_LONELY)
            }
        )
}
