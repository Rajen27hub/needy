package com.canopas.data.event

data class ActivitySubscriptionEvent(val activityId: Int)
