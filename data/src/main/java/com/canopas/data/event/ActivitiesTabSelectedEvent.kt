package com.canopas.data.event

data class ActivitiesTabSelectedEvent(val isActivitiesTabSelected: Boolean)
