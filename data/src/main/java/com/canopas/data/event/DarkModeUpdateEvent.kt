package com.canopas.data.event

data class DarkModeUpdateEvent(val isDarkModeEnabled: Boolean)
