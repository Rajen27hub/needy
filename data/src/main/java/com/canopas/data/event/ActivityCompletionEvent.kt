package com.canopas.data.event

import com.canopas.data.model.BasicActivity

data class ActivityCompletionEvent(val activity: BasicActivity)
