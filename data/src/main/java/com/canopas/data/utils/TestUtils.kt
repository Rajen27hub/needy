package com.canopas.data.utils

import com.canopas.base.data.model.Notifications
import com.canopas.base.data.model.Progress
import com.canopas.base.data.model.Recent
import com.canopas.base.data.model.Session
import com.canopas.base.data.model.StreaksData
import com.canopas.base.data.model.Total
import com.canopas.base.data.model.UserAccount
import com.canopas.data.model.Activity
import com.canopas.data.model.ActivityData
import com.canopas.data.model.ActivityMedia
import com.canopas.data.model.AddOrEditNoteTemplate
import com.canopas.data.model.AddOrUpdateGoal
import com.canopas.data.model.BasicActivity
import com.canopas.data.model.BasicSubscription
import com.canopas.data.model.Benefit
import com.canopas.data.model.Category
import com.canopas.data.model.CustomActivity
import com.canopas.data.model.FeaturedFeed
import com.canopas.data.model.Feed
import com.canopas.data.model.Goal
import com.canopas.data.model.GoalWithColors
import com.canopas.data.model.HighlightMetrics
import com.canopas.data.model.Highlights
import com.canopas.data.model.MotivationalMaterial
import com.canopas.data.model.NoteTemplate
import com.canopas.data.model.Notes
import com.canopas.data.model.Quotes
import com.canopas.data.model.RankPoint
import com.canopas.data.model.Story
import com.canopas.data.model.Subscription
import com.canopas.data.model.SuccessStory
import com.canopas.data.model.SummaryDate
import com.canopas.data.model.TYPE_END_GOAL
import com.canopas.data.model.TYPE_TEMPLATE_CUSTOM
import com.canopas.data.model.WeeklySummary

class TestUtils {
    companion object {

        val session = Session(
            1, 14, deviceType = 1, "1", "MD1",
            "", 20000, "", "1", oldRefreshToken = "",
            limitedAccess = false, lastAccessedOn = 1641987105
        )

        private val session1 = Session(
            1, 14, deviceType = 1, "1", "MD1",
            "", 20000, "", "1", oldRefreshToken = "",
            limitedAccess = true, lastAccessedOn = 1641987105
        )
        val notification = Notifications(
            pushNotification = true,
            mail = false,
            inactivityMail = true,
            inactivityNotification = false
        )

        val user = UserAccount(
            1, 14, 1, 1, "abc", "",
            "userName", "12345", "dummy@gmail.com", "",
            isActive = true, isAnonymous = true,
            createdAt = "123", updatedAt = "456", gender = 1, userType = 0, 1, true,
            "", session, notification
        )

        val userNotification = UserAccount(
            1,
            14,
            1,
            1,
            "abc",
            "",
            "userName",
            "12345",
            "dummy@gmail.com",
            "",
            isActive = true,
            isAnonymous = true,
            createdAt = "123",
            updatedAt = "456",
            gender = 1,
            userType = 0,
            1,
            true,
            "",
            session,
            Notifications(
                pushNotification = true,
                mail = true,
                inactivityMail = true,
                inactivityNotification = false
            )
        )

        val user2 = UserAccount(
            1, 14, 1, 1, "abc1", "",
            "userName1", "12345", "dummy@gmail.com", "url",
            isActive = true, isAnonymous = false,
            createdAt = "123", updatedAt = "456", gender = 1, userType = 0, 1, true, "",
            session, notification
        )

        val user3 = UserAccount(
            1, 14, 1, 1, "", "",
            "userName1", "12345", "", "url",
            isActive = true, isAnonymous = false,
            createdAt = "123", updatedAt = "456", gender = 1, userType = 0, 2, true, "",
            session, notification
        )

        val user4 = UserAccount(
            1, 14, 1, 1, "abc", "",
            "userName1", "12345", "dummy@gmail.com", "url",
            isActive = true, isAnonymous = false,
            createdAt = "123", updatedAt = "456", gender = 1, userType = 0, 2, true, "",
            session, notification
        )

        val user5 = UserAccount(
            1, 14, 1, 1, "abc", "",
            "userName1", "12345", "dummy@gmail.com", "url",
            isActive = true, isAnonymous = false,
            createdAt = "123", updatedAt = "456", gender = 1, userType = 0, 2, true, "",
            session1, notification
        )

        const val timezone = "Asia/Kolkata"
        private val medias = ActivityMedia(
            1, 11,
            "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
            1
        )

        val note = Notes(1, 10, 1, "Note", false, "", "")
        val emptyTemplate = NoteTemplate(
            id = 0,
            type = TYPE_TEMPLATE_CUSTOM,
            title = "",
            format = "Sample Format",
            index = 0,
            createdAt = "",
            updatedAt = ""
        )
        val addOrUpdateTemplateBody = AddOrEditNoteTemplate("", "")

        private val quotes = Quotes(
            1, 11,
            "Yoga is the journey of the self, through the self, to the self.",
            "The Bhagavad Gita"
        )

        private val benefit = Benefit(
            1, 11,
            "https://i.ibb.co/C6tWgbB/001-strength.png",
            "Yoga improves strength, balance and flexibility."
        )

        private val highlights = Highlights(
            id = 1, userId = 14, activityId = 1, type = 5,
            highlight_metrics = listOf(
                HighlightMetrics(
                    highlightId = 1,
                    previousStart = "2022-11-01T00:00:00Z",
                    previousEnd = "2022-11-30T23:59:59Z",
                    currentStart = "2022-12-01T00:00:00Z",
                    currentEnd = "2022-12-31T23:59:59Z",
                    criteria = 1,
                    previousReading = 1.75F,
                    currentReading = 1.5F
                )
            )
        )

        val activityData = ActivityData(
            1,
            1,
            "yoga",
            "Yoga is a mirror to look at ourselves from within",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            false,
            listOf(medias),
            listOf(quotes),
            listOf(benefit),
            listOf(highlights),
            false,
            "Inspiration",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            default_activity_duration = 30,
            default_activity_time = "08:00"
        )

        val basicActivity = BasicActivity(
            1,
            1,
            "yoga",
            "Yoga is a mirror to look at ourselves from within",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            false,
        )

        val activityData2 = ActivityData(
            2,
            1,
            "yoga",
            "Yoga is a mirror to look at ourselves from within",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png",
            "#80CBC4",
            false,
            listOf(medias),
            listOf(quotes),
            listOf(benefit),
            listOf(highlights),
            true,
            "Inspiration",
            "https://i.ibb.co/XzFFYQR/ic-meditation-yoga.png"
        )

        val category = Category(
            1, "Refreshing activities", "this is description",
            "", false, listOf(activityData)
        )

        val goal = Goal(
            1,
            1,
            activityData.name,
            "title",
            "desc",
            TYPE_END_GOAL,
            "",
            0,
            0,
            "",
            ""
        )

        val subscription =
            Subscription(
                1,
                1,
                activityData.id,
                Progress(listOf(Recent(1649918051, true)), Total(0, 3, 0, 68f)),
                listOf(1649918051, 1650017000),
                timezone,
                1641340800,
                1641340821,
                is_active = true,
                is_prompted = false,
                is_custom = false,
                activity = basicActivity, streak = 2,
                listOf(
                    StreaksData(1641340800, 1641340821, 5, true)
                ),
                1,
                "",
                note,
                listOf(note),
            )

        val goalBody = AddOrUpdateGoal(
            subscriptionId = subscription.id,
            title = "title",
            description = "desc",
            type = TYPE_END_GOAL,
            dueDate = ""
        )

        val basicSubscription =
            BasicSubscription(
                1,
                1,
                activityData.id,
                1641340800,
                1641340821,
                is_active = true,
                is_custom = false,
                activity = basicActivity,
            )

        val subscription3 =
            Subscription(
                1,
                1,
                activityData.id,
                Progress(listOf(Recent(1649918051, true)), Total(0, 3, 0, 68f)),
                listOf(1649918051, 1650017000),
                timezone,
                1641340800,
                1641340821,
                is_active = false,
                is_prompted = false,
                is_custom = false,
                activity = basicActivity, streak = 2,
                streaks = listOf(
                    StreaksData(1641340800, 1641340821, 5, true)
                ),
                repeat_type = 1,
                "",
                note,
                listOf(note),
            )

        val subscription5 =
            Subscription(
                1,
                1,
                activityData.id,
                Progress(listOf(Recent(1649918051, true)), Total(0, 3, 0, 68f)),
                listOf(1649918051, 1650017000),
                timezone,
                1641340800,
                1641340821,
                is_active = true,
                is_prompted = false,
                is_custom = false,
                activity = basicActivity, streak = 2,
                listOf(
                    StreaksData(1641340800, 1641340821, 5, true)
                ),
                1,
                "",
                note,
                listOf(note),
                activity_time = "13:10",
                activity_duration = 30,
                reminders = listOf(),
                show_note_on_complete = false
            )

        val customActivityRequest = CustomActivity(
            "title",
            "description",
            "#7046AA",
            timezone,
            1,
            ""
        )

        val goalWithColors = GoalWithColors(
            goal,
            basicSubscription.activity.name,
            basicSubscription.activity.id,
            basicSubscription.activity.color2,
            basicSubscription.activity.color
        )

        val successStory = SuccessStory(
            1, 2, "Yoga",
            "Yoga is amazing activity",
            "1,2",
            listOf(activityData), user
        )

        val motivationalMaterial = MotivationalMaterial(
            3,
            "Yoga",
            "Daily Routines of titans",
            "Yoga is good for health",
            "https://picsum.photos/id/237/200/300",
            3,
            "https://www.youtube.com/user/yogawithadriene",
            "Frida Kahlo",
            true,
            "2022-04-20T04:22:43.53028Z",
            "2022-04-20T04:23:07.78373Z"
        )

        val story = Story(
            3,
            "Yoga",
            "Yoga is good for health",
            "xyz",
            "",
            true,
            "2022-04-20T04:22:43.53028Z",
            "2022-04-20T04:22:43.53028Z",
            "2022-04-20T04:22:43.53028Z",
            details = listOf()
        )

        val feeds = Feed(
            listOf(category),
            listOf(motivationalMaterial)
        )

        val featuredFeeds = FeaturedFeed(
            type = 1,
            motivationalMaterial,
            story
        )

        val recent = Recent(date = 1670005800, completed = false)

        val subscriptionData = Subscription(
            id = 10676,
            user_id = 13667,
            activity_id = 31,
            progress = Progress(
                recent = listOf(
                    Recent(date = 1669573800, completed = false),
                    Recent(date = 1669660200, completed = true),
                    Recent(date = 1669746600, completed = true),
                    Recent(date = 1669833000, completed = true),
                    Recent(date = 1669919400, completed = false),
                    recent,
                    Recent(date = 1670092200, completed = false)
                ),
                total = Total(
                    completed_days = 4,
                    remaining_days = 0,
                    missed_days = 8,
                    progress = 30.76923F
                )
            ),
            completion_days = listOf(),
            timezone = "Asia/Kolkata",
            start_date = 1669141800,
            end_date = 0,
            is_active = true,
            is_prompted = false,
            is_custom = false,
            activity = BasicActivity(
                id = 31,
                category_id = 1,
                name = "Walking",
                description = "Increases cardio fitness and reduces body fat",
                image_url = "https://nolonely.s3.ap-south-1.amazonaws.com/activity/__w-180__/0316f008-18a6-422c-b8b2-ff495564affd.png",
                color = "#FFCC80",
                image_url2 = "https://nolonely.s3.ap-south-1.amazonaws.com/activity_media/08441daf-e419-4250-a62c-d171423c674f.svg",
                color2 = "#EAE9EF",
                is_completed = false,
            ),
            streak = 0,
            streaks = listOf(
                StreaksData(
                    start_date = 1669660200,
                    end_date = 1669833000,
                    count = 3,
                    is_current = false
                ),
                StreaksData(
                    start_date = 1669228200,
                    end_date = 1669228200,
                    count = 1,
                    is_current = false
                )
            ),
            repeat_type = 1,
            note = null,
            notes = null,
            isExcluded = false,
            priority = 3
        )
        val rank = RankPoint(12, 450, 125, 654)
        val activity = Activity(1, "fghsfs")

        val summaryDate = SummaryDate(7, 1, 5, 20, "poor", "#DE4200", "#FFECE4", activity)
        val summary = WeeklySummary(
            1,
            "",
            "dgfgfg@gamil.com",
            "fdf",
            "sjh",
            "Tuesday, October 25",
            "Monday, October 31",
            rank,
            listOf(summaryDate, summaryDate)
        )
    }
}
