package com.canopas.ui.notes.addnotes

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.utils.Utils.Companion.decodeFromBase64
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.BackArrowTint
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.InterMediumFont
import com.canopas.base.ui.SignOutTextColor
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.customview.CustomTextField
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.FeatureFlag.isNoteTemplatesEnabled
import com.canopas.data.model.Notes
import com.canopas.ui.R
import com.canopas.ui.utils.DateUtils

@Composable
fun AddNotesView(
    activityName: String,
    activityId: String,
    notesId: String = "",
    isCustom: Boolean,
    templateId: Int? = null
) {

    val viewModel = hiltViewModel<AddNotesViewModel>()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(notesId, templateId)
    })

    val state by viewModel.state.collectAsState()
    val context = LocalContext.current

    state.let { notesState ->
        when (notesState) {
            is NotesState.START -> {
                AddOrUpdateNoteView(
                    null,
                    decodeFromBase64(activityName),
                    viewModel,
                    activityId,
                    isCustom
                )
            }

            NotesState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(colors.background)
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }
            is NotesState.FAILURE -> {
                val message = notesState.message
                LaunchedEffect(key1 = message) {
                    showBanner(context, message)
                    viewModel.resetState()
                }
            }
            is NotesState.SUCCESS -> {
                val note = notesState.notes
                AddOrUpdateNoteView(
                    note,
                    decodeFromBase64(activityName),
                    viewModel,
                    activityId,
                    isCustom
                )
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AddOrUpdateNoteView(
    note: Notes?,
    activityName: String,
    viewModel: AddNotesViewModel,
    activityId: String,
    isCustom: Boolean,
) {
    val contentText by viewModel.content.collectAsState()
    val keyboardController = LocalSoftwareKeyboardController.current
    val focusRequester = remember {
        FocusRequester()
    }
    val scrollState = rememberScrollState()
    val showSaveButton by viewModel.showSaveButton.collectAsState()
    val showDeleteAlert by viewModel.showDeleteAlert.collectAsState()
    if (showDeleteAlert) {
        ShowDeleteAlertDialog(viewModel, note)
    }
    var notesTextFieldValue by remember {
        mutableStateOf(
            TextFieldValue(
                contentText,
                selection = TextRange(contentText.length)
            )
        )
    }

    LaunchedEffect(scrollState.maxValue) {
        scrollState.scrollTo(scrollState.maxValue)
    }

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = activityName,
                        subText = DateUtils.getCurrentDay(),
                        navigationIconOnClick = { viewModel.popBack() },
                        actions = {
                            if (note != null) {
                                IconButton(onClick = {
                                    viewModel.toggleDeleteAlert()
                                }) {
                                    Icon(
                                        painter = painterResource(id = R.drawable.ic_delete_action_icon),
                                        contentDescription = null,
                                        tint = BackArrowTint
                                    )
                                }
                            }
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        DisposableEffect(key1 = Unit, effect = {
            focusRequester.requestFocus()
            onDispose {
                keyboardController?.hide()
                focusRequester.freeFocus()
            }
        })
        val context = LocalContext.current
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(colors.background)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f)
            ) {
                CustomTextField(
                    value = notesTextFieldValue,
                    onValueChange = {
                        notesTextFieldValue = it
                        viewModel.onContentChange(it.text)
                    },
                    modifier = Modifier
                        .verticalScroll(scrollState)
                        .background(colors.background)
                        .focusRequester(focusRequester)
                        .padding(start = 16.dp, end = 16.dp, top = 16.dp, bottom = 68.dp)
                        .fillMaxSize(),
                    textStyle = AppTheme.typography.notesContentTextStyle.copy(color = colors.textPrimary),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Default
                    ),
                    singleLine = false,
                    cursorBrush = SolidColor(colors.primary.copy(0.5f)),
                    decorationBox = { innerTextField ->

                        if (contentText.isEmpty()) {
                            val isDarkMode = isDarkMode
                            val templateTag by remember {
                                mutableStateOf(context.getString(R.string.add_notes_view_template_tag))
                            }
                            val annotatedString by remember {
                                mutableStateOf(
                                    buildAnnotatedString {
                                        withStyle(
                                            style = SpanStyle(
                                                color = if (isDarkMode) darkEmptyNotesText else lightEmptyNotesText,
                                                letterSpacing = -(0.72.sp)
                                            )
                                        ) {
                                            append(context.getString(R.string.add_notes_view_write_down_your_notes_text))
                                        }
                                        pushStringAnnotation(
                                            tag = templateTag,
                                            annotation = templateTag
                                        )
                                        withStyle(
                                            style = SpanStyle(
                                                color = ColorPrimary,
                                                textDecoration = TextDecoration.Underline,
                                                fontWeight = FontWeight.Medium,
                                                fontFamily = InterMediumFont,
                                                letterSpacing = -(0.72.sp)
                                            )
                                        ) {
                                            append(context.getString(R.string.add_notes_view_use_a_template_text))
                                        }
                                        pop()
                                    }
                                )
                            }

                            if (isNoteTemplatesEnabled) {
                                ClickableText(
                                    text = annotatedString,
                                    onClick = { offset ->
                                        keyboardController?.show()
                                        annotatedString.getStringAnnotations(
                                            tag = templateTag,
                                            start = offset,
                                            end = offset
                                        ).firstOrNull()?.let {
                                            viewModel.navigateToNoteTemplatesView()
                                        }
                                    },
                                    style = AppTheme.typography.notesContentTextStyle,
                                    modifier = Modifier.padding(it)
                                )
                            } else {
                                Text(
                                    text = stringResource(R.string.add_notes_view_write_down_your_notes_text_old),
                                    style = AppTheme.typography.notesContentTextStyle,
                                    color = if (isDarkMode) darkEmptyNotesText else lightEmptyNotesText,
                                    modifier = Modifier.padding(it)
                                )
                            }
                        }
                        innerTextField()
                    },
                )
            }
            Spacer(modifier = Modifier.height(4.dp))
            if (showSaveButton) {
                Box(
                    Modifier
                        .offset(y = -(8.dp))
                        .height(8.dp)
                        .fillMaxWidth()
                ) {
                    Box(
                        Modifier
                            .fillMaxSize()
                            .background(
                                Brush.verticalGradient(
                                    listOf(
                                        Color.Transparent,
                                        colors.background
                                    )
                                )
                            )
                    )
                }

                Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                    PrimaryButton(
                        onClick = {
                            viewModel.handleSaveButtonClick(activityId, note, isCustom)
                        },
                        shape = RoundedCornerShape(50),
                        modifier = Modifier
                            .background(colors.background)
                            .motionClickEvent { }
                            .wrapContentSize(),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = colors.primary,
                            contentColor = White,
                            disabledBackgroundColor = colors.textSecondary,
                            disabledContentColor = White
                        ),
                        text = stringResource(R.string.add_notes_view_save_button_text),
                        textModifier = Modifier.padding(vertical = 4.dp, horizontal = 20.dp)
                    )
                }
                Spacer(modifier = Modifier.height(20.dp))
            }
        }
    }
}

@Composable
fun ShowDeleteAlertDialog(viewModel: AddNotesViewModel, note: Notes?) {
    CustomAlertDialog(
        title = stringResource(R.string.add_notes_view_alert_dialog_title_text),
        subTitle = stringResource(R.string.add_notes_view_alert_dialog_message_text),
        confirmBtnText = stringResource(R.string.add_notes_view_alert_dialog_delete_btn_text),
        dismissBtnText = stringResource(R.string.add_notes_view_alert_dialog_cancel_btn_text),
        confirmBtnColor = SignOutTextColor,
        onConfirmClick = { viewModel.deleteNote(note) },
        onDismissClick = { viewModel.toggleDeleteAlert() }
    )
}

private val darkEmptyNotesText = Color(0x4DFFFFFF)
private val lightEmptyNotesText = Color(0x4D1C191F)
