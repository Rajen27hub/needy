package com.canopas.ui.notes.addnotes

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.AddOrUpdateNoteBody
import com.canopas.data.model.Notes
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class AddNotesViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<NotesState>(NotesState.LOADING)
    val content = MutableStateFlow("")
    val showSaveButton = MutableStateFlow(false)
    val showDeleteAlert = MutableStateFlow(false)
    var note: Notes? = null

    fun onStart(notesId: String, templateId: Int? = null) {
        appAnalytics.logEvent("view_notes_detail")
        state.tryEmit(NotesState.LOADING)
        if (notesId.isEmpty() && templateId == null) {
            state.tryEmit(NotesState.START)
        } else if (templateId != null) {
            fetchTemplate(templateId)
        } else {
            fetchNotesData(notesId)
        }
    }

    private fun fetchTemplate(templateId: Int) = viewModelScope.launch {
        withContext(appDispatcher.IO) {
            service.getNoteTemplateById(templateId).onSuccess {
                content.tryEmit(it.format.ifEmpty { "" })
                showSaveButton.tryEmit(true)
                state.tryEmit(NotesState.START)
            }.onFailure {
                Timber.e(it.toString())
                state.tryEmit(NotesState.FAILURE(it.toUserError(resources)))
            }
        }
    }

    private fun fetchNotesData(notesId: String) = viewModelScope.launch {
        withContext(appDispatcher.IO) {
            service.getNote(notesId).onSuccess {
                note = it
                content.tryEmit(it.content)
                state.tryEmit(NotesState.SUCCESS(notes = it))
            }.onFailure { e ->
                Timber.e(e)
                state.tryEmit(NotesState.FAILURE(e.toUserError(resources)))
            }
        }
    }

    fun onContentChange(newValue: String) {
        content.tryEmit(newValue)
        showSaveButton.tryEmit(content.value.isNotEmpty())
        val note = note ?: return
        showSaveButton.tryEmit(content.value != note.content)
    }

    fun resetState() {
        state.tryEmit(NotesState.START)
    }

    fun popBack() = viewModelScope.launch {
        withContext(appDispatcher.MAIN) {
            navManager.popBack()
        }
    }

    fun handleSaveButtonClick(activityId: String, note: Notes?, isCustom: Boolean) = viewModelScope.launch {
        withContext(appDispatcher.IO) {
            state.tryEmit(NotesState.LOADING)
            appAnalytics.logEvent("tap_notes_save_button")
            val notesBody = AddOrUpdateNoteBody(activityId.toInt(), content.value, isCustom)
            val addOrUpdateJob = async {
                if (note != null) service.updateNote(note.id.toString(), notesBody) else service.addNote(notesBody)
            }
            addOrUpdateJob.await().onSuccess {
                popBack()
            }.onFailure { e ->
                Timber.e(e)
                state.tryEmit(NotesState.FAILURE(e.toUserError(resources)))
            }
        }
    }

    fun toggleDeleteAlert() {
        appAnalytics.logEvent(if (!showDeleteAlert.value) "tap_notes_delete_button" else "tap_notes_delete_button_no")
        showDeleteAlert.tryEmit(!showDeleteAlert.value)
    }

    fun deleteNote(note: Notes?) = viewModelScope.launch {
        state.tryEmit(NotesState.LOADING)
        withContext(appDispatcher.IO) {
            appAnalytics.logEvent("tap_notes_delete_button_yes")
            service.deleteNote(note?.id.toString()).onSuccess {
                Timber.e(it.toString())
                popBack()
            }.onFailure { e ->
                Timber.e(e)
                state.tryEmit(NotesState.FAILURE(e.toUserError(resources)))
            }
        }
    }

    fun navigateToNoteTemplatesView() {
        navManager.navigateToNoteTemplatesView()
    }
}

sealed class NotesState {
    object START : NotesState()
    object LOADING : NotesState()
    data class SUCCESS(val notes: Notes) : NotesState()
    data class FAILURE(val message: String) : NotesState()
}
