package com.canopas.ui.notes.noteslist

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.Notes
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class NotesListViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val navManager: NavManager,
    private val appDispatcher: AppDispatcher,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<NotesListState>(NotesListState.LOADING)
    val pageLoader = MutableStateFlow<LoaderState>(LoaderState.LOADING)
    val notes = MutableStateFlow<List<Notes>>(emptyList())
    val hasMoreItems = MutableStateFlow(true)

    fun onBackClick() {
        navManager.popBack()
    }

    fun onViewResume(activityId: Int, isCustom: Boolean, skip: Int) {
        appAnalytics.logEvent("view_notes_list")
        fetchNotesListData(activityId, isCustom, skip)
    }

    private fun fetchNotesListData(activityId: Int, isCustom: Boolean, skip: Int) =
        viewModelScope.launch {
            if (skip == 0) {
                if (state.value !is NotesListState.SUCCESS) {
                    state.tryEmit(NotesListState.LOADING)
                }
                notes.value = emptyList()
            } else if (notes.value.size >= 30) {
                pageLoader.tryEmit(LoaderState.LOADING)
            }
            withContext(appDispatcher.IO) {
                service.getActivityNotes(activityId, isCustom, skip).onSuccess {
                    notes.value = notes.value + it
                    state.tryEmit(NotesListState.SUCCESS(notes.value))
                    pageLoader.tryEmit(LoaderState.FINISH)
                    if (it.isEmpty()) {
                        hasMoreItems.tryEmit(false)
                    }
                }.onFailure {
                    Timber.e(it.toString())
                    state.tryEmit(NotesListState.FAILURE(it.toUserError(resources)))
                }
            }
        }

    fun resetState() {
        state.value = NotesListState.START
    }

    fun navigateToNotesView(
        activityName: String,
        activityId: String,
        notesId: Int?,
        isCustom: Boolean
    ) {
        appAnalytics.logEvent("tap_notes_list_item")
        val id = notesId?.toString() ?: ""
        navManager.navigateToAddOrUpdateNotes(
            activityName,
            activityId = activityId,
            notesId = id,
            isCustom = isCustom
        )
    }

    fun onLastItemVisible(index: Int, activityId: Int, isCustom: Boolean, skip: Int) {
        val showPageLoader = pageLoader.value is LoaderState.LOADING
        if (index >= notes.value.size - 1 && hasMoreItems.value && !showPageLoader) {
            onViewResume(activityId, isCustom, skip)
        }
    }
}

sealed class NotesListState {
    object START : NotesListState()
    object LOADING : NotesListState()
    data class SUCCESS(val notes: List<Notes>) : NotesListState()
    data class FAILURE(val message: String) : NotesListState()
}

sealed class LoaderState {
    object LOADING : LoaderState()
    object FINISH : LoaderState()
}
