package com.canopas.ui.notes.managetemplates

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.NoteTemplate
import com.canopas.data.model.TYPE_TEMPLATE_CUSTOM
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.emptyTemplate
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ManageTemplatesViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val resources: Resources,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
) : ViewModel() {

    val state = MutableStateFlow<ManageTemplatesState>(ManageTemplatesState.START)

    fun onStart() {
        appAnalytics.logEvent("view_manage_templates")
        fetchNoteTemplates()
    }

    private fun fetchNoteTemplates() = viewModelScope.launch {
        state.tryEmit(ManageTemplatesState.LOADING)
        withContext(appDispatcher.IO) {
            service.getNoteTemplates().onSuccess {
                val templates = listOf(emptyTemplate) + it.filter { template -> template.type == TYPE_TEMPLATE_CUSTOM }
                state.tryEmit(ManageTemplatesState.SUCCESS(templates))
            }.onFailure {
                Timber.e(it.toString())
                state.tryEmit(ManageTemplatesState.FAILURE(it.toUserError(resources)))
            }
        }
    }

    fun resetState() {
        state.tryEmit(ManageTemplatesState.START)
    }

    fun popBack() {
        navManager.popBack()
    }

    fun navigateToEditTemplate(template: NoteTemplate) {
        if (template.id == 0 && template.title.isEmpty() && template.createdAt.isEmpty()) {
            navManager.navigateToAddNoteTemplate()
        } else {
            navManager.navigateToEditNoteTemplate(template.id)
        }
    }
}

sealed class ManageTemplatesState {
    object START : ManageTemplatesState()
    object LOADING : ManageTemplatesState()
    data class SUCCESS(val templatesList: List<NoteTemplate>) : ManageTemplatesState()
    data class FAILURE(val message: String) : ManageTemplatesState()
}
