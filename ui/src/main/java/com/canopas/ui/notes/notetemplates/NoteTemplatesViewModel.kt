package com.canopas.ui.notes.notetemplates

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.NoteTemplate
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class NoteTemplatesViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val resources: Resources,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
) : ViewModel() {

    val state = MutableStateFlow<NoteTemplatesState>(NoteTemplatesState.START)

    fun onStart() {
        appAnalytics.logEvent("view_note_templates")
        fetchNoteTemplates()
    }

    private fun fetchNoteTemplates() = viewModelScope.launch {
        state.tryEmit(NoteTemplatesState.LOADING)
        withContext(appDispatcher.IO) {
            service.getNoteTemplates().onSuccess {
                state.tryEmit(NoteTemplatesState.SUCCESS(it))
            }.onFailure {
                Timber.e(it.toString())
                state.tryEmit(NoteTemplatesState.FAILURE(it.toUserError(resources)))
            }
        }
    }

    fun resetState() {
        state.tryEmit(NoteTemplatesState.START)
    }

    fun popBack() {
        navManager.popBack()
    }

    fun navigateToManageTemplates() {
        appAnalytics.logEvent("tap_note_templates_manage_action")
        navManager.navigateToManageTemplatesView()
    }

    fun popBackWithTemplateId(templateId: Int) {
        appAnalytics.logEvent("tap_note_templates_card")
        navManager.managePopBackWithNoteTemplateId(templateId = templateId)
    }
}

sealed class NoteTemplatesState {
    object START : NoteTemplatesState()
    object LOADING : NoteTemplatesState()
    data class SUCCESS(val templatesList: List<NoteTemplate>) : NoteTemplatesState()
    data class FAILURE(val message: String) : NoteTemplatesState()
}
