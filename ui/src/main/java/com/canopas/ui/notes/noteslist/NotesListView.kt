package com.canopas.ui.notes.noteslist

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import com.canopas.base.data.utils.Utils.Companion.decodeFromBase64
import com.canopas.base.data.utils.Utils.Companion.encodeToBase64
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.data.model.Notes
import com.canopas.ui.R
import com.canopas.ui.utils.DateUtils
import com.canopas.ui.utils.OnLifecycleEvent

@Composable
fun NotesListView(activityId: Int, isCustom: Boolean, activityName: String) {
    val viewModel = hiltViewModel<NotesListViewModel>()
    val state by viewModel.state.collectAsState()
    val context = LocalContext.current

    OnLifecycleEvent { _, event ->
        when (event) {
            Lifecycle.Event.ON_RESUME -> {
                viewModel.onViewResume(activityId, isCustom, 0)
            }
            else -> return@OnLifecycleEvent
        }
    }

    Scaffold(topBar = {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(R.string.notes_list_activity_title_text),
                    navigationIconOnClick = {
                        viewModel.onBackClick()
                    }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
    }) {
        state.let { notesListState ->
            when (notesListState) {
                NotesListState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize().background(colors.background)
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is NotesListState.FAILURE -> {
                    val message = notesListState.message
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                        viewModel.resetState()
                    }
                }
                is NotesListState.SUCCESS -> {
                    val notes = notesListState.notes
                    NotesSuccessView(
                        notesList = notes,
                        viewModel,
                        decodeFromBase64(activityName),
                        activityId, isCustom
                    )
                }
                else -> {}
            }
        }
    }
}

@Composable
fun NotesSuccessView(
    notesList: List<Notes>,
    viewModel: NotesListViewModel,
    activityName: String,
    activityId: Int,
    isCustom: Boolean
) {
    val scrollState = rememberLazyListState()
    val pageLoader by viewModel.pageLoader.collectAsState()
    val showPageLoader = pageLoader is LoaderState.LOADING

    Box(
        modifier = Modifier.fillMaxSize().background(colors.background)
    ) {
        if (notesList.isEmpty()) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = stringResource(id = R.string.notes_list_empty_notes_text),
                    style = AppTheme.typography.subTitleTextStyle1,
                    lineHeight = 22.sp,
                    modifier = Modifier
                        .widthIn(max = 600.dp)
                        .fillMaxWidth()
                        .padding(
                            horizontal = 72.dp,
                            vertical = 32.dp
                        ),
                    textAlign = TextAlign.Center,
                    color = colors.textSecondary
                )
            }
        } else {
            LazyColumn(
                state = scrollState,
                contentPadding = PaddingValues(horizontal = 20.dp, vertical = 12.dp),
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                itemsIndexed(notesList) { index, note ->
                    viewModel.onLastItemVisible(index, activityId, isCustom, notesList.size)
                    NotesListItem(item = note, viewModel = viewModel, activityName = activityName)
                }
                item {
                    if (showPageLoader) {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(16.dp),
                            contentAlignment = Alignment.Center,
                        ) {
                            CircularProgressIndicator(color = colors.primary)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun NotesListItem(item: Notes, viewModel: NotesListViewModel, activityName: String) {
    val formattedDay = DateUtils.getLongFormat(item.createdAt)
    val noteColor = DateUtils.getNotesColor(item.createdAt, item.id).first
    val notesBorderColor = DateUtils.getNotesColor(item.createdAt, item.id).second

    Box(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .padding(vertical = 12.dp)
            .fillMaxWidth()
            .motionClickEvent {
                viewModel.navigateToNotesView(
                    encodeToBase64(activityName),
                    item.activityId.toString(),
                    item.id,
                    item.isCustom
                )
            }
            .background(
                noteColor,
                shape = RoundedCornerShape(16.dp)
            )
            .border(
                BorderStroke(1.dp, notesBorderColor),
                RoundedCornerShape(16)
            )
            .clip(shape = RoundedCornerShape(16.dp))
    ) {
        Column(
            modifier = Modifier
                .padding(14.dp),
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.Start
        ) {
            Text(
                text = formattedDay,
                style = AppTheme.typography.notesDateTextStyle,
                color = colors.textSecondary
            )

            Text(
                text = item.content,
                style = AppTheme.typography.subTitleTextStyle1,
                color = colors.textPrimary,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Preview
@Composable
fun PreviewNotesView() {
    NotesListView(1, false, "yoga")
}
