package com.canopas.ui.notes.addtemplate

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.ui.Event
import com.canopas.data.model.AddOrEditNoteTemplate
import com.canopas.data.model.NoteTemplate
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class AddOrEditTemplateViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<TemplateState>(TemplateState.LOADING)
    private var template: NoteTemplate? = null
    val templateTitle = MutableStateFlow("")
    val templateFormat = MutableStateFlow("")
    val openDeleteDialog = MutableStateFlow(Event(false))
    val enableActionButton = MutableStateFlow(false)
    val showActionLoader = MutableStateFlow(false)
    val showDeleteButton = MutableStateFlow(true)

    fun onStart(templateId: Int?) {
        if (templateId == null) {
            appAnalytics.logEvent("view_add_note_template")
            state.tryEmit(TemplateState.START)
        } else {
            appAnalytics.logEvent("view_edit_note_template")
            fetchNoteTemplate(templateId)
        }
    }

    private fun fetchNoteTemplate(templateId: Int) {
        viewModelScope.launch {
            state.tryEmit(TemplateState.LOADING)
            withContext(appDispatcher.IO) {
                service.getNoteTemplateById(templateId).onSuccess {
                    template = it
                    templateTitle.tryEmit(it.title)
                    templateFormat.tryEmit(it.format)
                    state.tryEmit(TemplateState.FETCH_SUCCESS(it))
                }.onFailure {
                    Timber.e(it.toString())
                    state.tryEmit(TemplateState.FAILURE(it.toUserError(resources), it is APIError.NetworkError))
                }
            }
        }
    }

    fun addNoteTemplate() {
        viewModelScope.launch {
            showActionLoader.tryEmit(true)
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("tap_add_template_save_action")
                val templateBody = AddOrEditNoteTemplate(templateTitle.value, templateFormat.value)
                service.createNoteTemplate(templateBody).onSuccess {
                    withContext(appDispatcher.MAIN) {
                        popBack()
                    }
                }.onFailure {
                    showActionLoader.tryEmit(false)
                    Timber.e(it.toString())
                    state.tryEmit(TemplateState.FAILURE(it.toUserError(resources), it is APIError.NetworkError))
                }
            }
        }
    }

    fun editNoteTemplate(templateId: Int) {
        viewModelScope.launch {
            showActionLoader.tryEmit(true)
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("tap_edit_template_save_action")
                val templateBody = AddOrEditNoteTemplate(templateTitle.value, templateFormat.value)
                service.updateNoteTemplate(templateId, templateBody).onSuccess {
                    withContext(appDispatcher.MAIN) {
                        popBack()
                    }
                }.onFailure {
                    showActionLoader.tryEmit(false)
                    Timber.e(it.toString())
                    state.tryEmit(TemplateState.FAILURE(it.toUserError(resources), it is APIError.NetworkError))
                }
            }
        }
    }

    fun onTemplateTitleChange(title: String) {
        templateTitle.tryEmit(title)
        if (template == null) {
            enableActionButton.tryEmit(checkEmptyFieldConditions())
        } else {
            enableActionButton.tryEmit(checkPreviousTemplateConditions())
        }
    }

    fun onTemplateFormatChange(format: String) {
        templateFormat.tryEmit(format)
        if (template == null) {
            enableActionButton.tryEmit(checkEmptyFieldConditions())
        } else {
            enableActionButton.tryEmit(checkPreviousTemplateConditions())
        }
    }

    private fun checkEmptyFieldConditions(): Boolean {
        return templateFormat.value.trim().isNotEmpty() && templateTitle.value.trim().isNotEmpty()
    }

    private fun checkPreviousTemplateConditions(): Boolean {
        val template = template ?: return false
        return ((templateTitle.value != template.title && templateTitle.value.trim().isNotEmpty()) || (templateFormat.value != template.format && templateFormat.value.trim().isNotEmpty()))
    }

    fun popBack() {
        navManager.popBack()
    }

    fun openDeleteTemplateDialog() {
        appAnalytics.logEvent("tap_edit_template_delete_btn")
        openDeleteDialog.value = Event(true)
    }

    fun closeDeleteTemplateDialog() {
        appAnalytics.logEvent("tap_edit_template_cancel_delete_btn")
        openDeleteDialog.value = Event(false)
    }

    fun deleteTemplate(templateId: Int) {
        state.tryEmit(TemplateState.LOADING)
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                service.deleteNoteTemplate(templateId = templateId).onSuccess {
                    closeDeleteTemplateDialog()
                    withContext(appDispatcher.MAIN) {
                        popBack()
                    }
                }.onFailure {
                    Timber.e(it.toString())
                    state.tryEmit(TemplateState.FAILURE(it.toUserError(resources), it is APIError.NetworkError))
                }
            }
        }
    }

    fun toggleDeleteButton(value: Boolean) {
        showDeleteButton.tryEmit(value)
    }
}

sealed class TemplateState {
    object LOADING : TemplateState()
    object START : TemplateState()
    data class FETCH_SUCCESS(val noteTemplate: NoteTemplate) : TemplateState()
    data class FAILURE(val message: String, val isNetworkError: Boolean) : TemplateState()
}
