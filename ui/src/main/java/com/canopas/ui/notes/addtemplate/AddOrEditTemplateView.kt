package com.canopas.ui.notes.addtemplate

import android.view.ViewTreeObserver
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.relocation.BringIntoViewRequester
import androidx.compose.foundation.relocation.bringIntoViewRequester
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.onFocusEvent
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalView
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.customview.CustomTextField
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.SecondaryOutlineButton
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.data.model.NoteTemplate
import com.canopas.ui.R
import kotlinx.coroutines.launch

@Composable
fun AddOrEditTemplateView(templateId: Int?) {
    val viewModel = hiltViewModel<AddOrEditTemplateViewModel>()
    val state by viewModel.state.collectAsState()
    val view = LocalView.current
    DisposableEffect(view) {
        val listener = ViewTreeObserver.OnGlobalLayoutListener {
            val isKeyboardOpen = ViewCompat.getRootWindowInsets(view)
                ?.isVisible(WindowInsetsCompat.Type.ime()) ?: true
            viewModel.toggleDeleteButton(!isKeyboardOpen)
        }

        view.viewTreeObserver.addOnGlobalLayoutListener(listener)
        onDispose {
            view.viewTreeObserver.removeOnGlobalLayoutListener(listener)
        }
    }
    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(templateId)
    })

    state.let { templateState ->
        when (templateState) {
            TemplateState.START -> {
                AddOrUpdateTemplateView(viewModel = viewModel, noteTemplate = null)
            }
            TemplateState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(colors.background)
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }
            is TemplateState.FETCH_SUCCESS -> {
                val noteTemplate = templateState.noteTemplate
                AddOrUpdateTemplateView(viewModel, noteTemplate)
            }
            is TemplateState.FAILURE -> {
                if (templateState.isNetworkError) {
                    NetworkErrorView {
                        viewModel.onStart(templateId)
                    }
                } else {
                    ServerErrorView {
                        viewModel.onStart(templateId)
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun AddOrUpdateTemplateView(viewModel: AddOrEditTemplateViewModel, noteTemplate: NoteTemplate?) {

    val enableActionButton by viewModel.enableActionButton.collectAsState()
    val showLoader by viewModel.showActionLoader.collectAsState()
    val showDeleteButton by viewModel.showDeleteButton.collectAsState()

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = if (noteTemplate == null) stringResource(R.string.add_template_view_top_bar_title) else stringResource(
                            R.string.add_template_view_top_bar_edit_title
                        ),
                        navigationIconOnClick = { viewModel.popBack() },
                        actions = {
                            IconButton(onClick = {
                                if (enableActionButton) {
                                    if (noteTemplate == null) {
                                        viewModel.addNoteTemplate()
                                    } else {
                                        viewModel.editNoteTemplate(noteTemplate.id)
                                    }
                                }
                            }) {
                                if (showLoader) {
                                    CircularProgressIndicator(color = colors.primary)
                                } else {
                                    Icon(
                                        Icons.Filled.Check,
                                        contentDescription = null,
                                        tint = if (enableActionButton) colors.textPrimary else colors.textSecondary.copy(
                                            0.3f
                                        )
                                    )
                                }
                            }
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        val title by viewModel.templateTitle.collectAsState()
        val format by viewModel.templateFormat.collectAsState()
        var formatTextFieldValue by remember {
            mutableStateOf(
                TextFieldValue(
                    format,
                    selection = TextRange(format.length)
                )
            )
        }
        val bringIntoViewRequester = remember { BringIntoViewRequester() }
        val coroutineScope = rememberCoroutineScope()

        Column(
            modifier = Modifier
                .fillMaxSize()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxSize()
                    .weight(1f).verticalScroll(rememberScrollState())
            ) {
                CustomTextField(
                    text = title,
                    onTextChange = {
                        viewModel.onTemplateTitleChange(it)
                    },
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 16.dp, end = 16.dp, top = 28.dp),
                    textStyle = AppTheme.typography.h3TextStyle.copy(color = colors.textPrimary),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Next
                    ),
                    maxLines = 4,
                    cursorBrush = SolidColor(colors.primary.copy(0.5f)),
                    decorationBox = { innerTextField ->
                        if (title.isEmpty()) {
                            Text(
                                text = stringResource(R.string.add_goal_view_title_placeholder_text),
                                style = AppTheme.typography.h3TextStyle.copy(color = colors.textPrimary.copy(0.4f))
                            )
                        }
                        innerTextField()
                    }
                )

                Spacer(modifier = Modifier.height(24.dp))

                Text(
                    text = stringResource(R.string.add_template_view_template_header_text),
                    style = AppTheme.typography.bodyTextStyle3,
                    color = colors.textSecondary,
                    lineHeight = 17.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 16.dp)
                )

                Spacer(modifier = Modifier.height(16.dp))

                CustomTextField(
                    value = formatTextFieldValue,
                    onValueChange = {
                        formatTextFieldValue = it
                        viewModel.onTemplateFormatChange(it.text)
                    },
                    modifier = Modifier
                        .background(colors.background)
                        .fillMaxWidth()
                        .height(IntrinsicSize.Min)
                        .bringIntoViewRequester(bringIntoViewRequester)
                        .onFocusEvent { focusState ->
                            if (focusState.isFocused) {
                                coroutineScope.launch {
                                    bringIntoViewRequester.bringIntoView()
                                }
                            }
                        }
                        .padding(start = 16.dp, end = 16.dp, top = 16.dp),
                    textStyle = AppTheme.typography.subTextStyle1.copy(
                        color = colors.textPrimary,
                        lineHeight = 24.sp
                    ),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Default
                    ),
                    singleLine = false,
                    cursorBrush = SolidColor(colors.primary.copy(0.5f)),
                    decorationBox = { innerTextField ->
                        if (format.isEmpty()) {
                            Text(
                                text = stringResource(R.string.add_template_view_write_your_template_text),
                                style = AppTheme.typography.notesContentTextStyle,
                                color = colors.textPrimary.copy(0.4f),
                                letterSpacing = -(0.72.sp),
                                modifier = Modifier.padding(it)
                            )
                        }
                        innerTextField()
                    },
                )
            }
            Spacer(modifier = Modifier.height(4.dp))
            if (noteTemplate != null && showDeleteButton) {
                Box(
                    Modifier
                        .offset(y = -(8.dp))
                        .height(8.dp)
                        .fillMaxWidth()
                ) {
                    Box(
                        Modifier
                            .fillMaxSize()
                            .background(
                                Brush.verticalGradient(
                                    listOf(
                                        Color.Transparent,
                                        colors.background
                                    )
                                )
                            )
                    )
                }

                DeleteButtonView(templateId = noteTemplate.id, viewModel = viewModel)
            }
        }
    }
}

@Composable
fun DeleteButtonView(templateId: Int, viewModel: AddOrEditTemplateViewModel) {
    val openDeleteGoalDialogEvent by viewModel.openDeleteDialog.collectAsState()
    val openDeleteGoalDialog = openDeleteGoalDialogEvent.getContentIfNotHandled() ?: false

    Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
        SecondaryOutlineButton(
            modifier = Modifier
                .wrapContentSize(),
            onClick = {
                viewModel.openDeleteTemplateDialog()
            },
            border = BorderStroke(
                1.dp,
                colors.primary
            ),
            text = stringResource(R.string.add_template_view_delete_btn_text),
            textModifier = Modifier.padding(top = 4.dp, bottom = 4.dp, start = 8.dp, end = 12.dp),
            shape = RoundedCornerShape(24.dp),
            colors = ButtonDefaults.outlinedButtonColors(contentColor = colors.primary, backgroundColor = colors.background),
            color = colors.primary,
            icon = painterResource(id = R.drawable.ic_delete_icon_for_button),
            iconModifier = Modifier.padding(start = 12.dp).size(16.dp),
        )
    }

    if (openDeleteGoalDialog) {
        ShowDeleteTemplateDialog(viewModel, templateId)
    }

    Spacer(modifier = Modifier.height(20.dp))
}

@Composable
fun ShowDeleteTemplateDialog(viewModel: AddOrEditTemplateViewModel, templateId: Int) {
    CustomAlertDialog(
        title = stringResource(R.string.add_template_view_delete_alert_title),
        subTitle = stringResource(R.string.add_template_view_alert_body_text),
        confirmBtnText = stringResource(R.string.add_template_view_alert_confirm_btn_text),
        dismissBtnText = stringResource(R.string.add_template_view_alert_dismiss_btn_text),
        confirmBtnColor = MaterialTheme.colors.error,
        onConfirmClick = { viewModel.deleteTemplate(templateId) },
        onDismissClick = { viewModel.closeDeleteTemplateDialog() }
    )
}
