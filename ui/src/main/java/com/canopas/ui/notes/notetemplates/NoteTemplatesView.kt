package com.canopas.ui.notes.notetemplates

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.NoteTemplate
import com.canopas.ui.R

@Composable
fun NoteTemplatesView() {
    val viewModel = hiltViewModel<NoteTemplatesViewModel>()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart()
    })

    val state by viewModel.state.collectAsState()
    val context = LocalContext.current

    state.let { templateState ->
        when (templateState) {
            NoteTemplatesState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(colors.background)
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }
            is NoteTemplatesState.FAILURE -> {
                val message = templateState.message
                LaunchedEffect(key1 = message) {
                    showBanner(context, message)
                    viewModel.resetState()
                }
            }
            is NoteTemplatesState.SUCCESS -> {
                val templates = templateState.templatesList
                NoteTemplatesListView(templates, viewModel)
            }
            else -> {}
        }
    }
}

@Composable
fun NoteTemplatesListView(templates: List<NoteTemplate>, viewModel: NoteTemplatesViewModel) {

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = stringResource(R.string.note_template_view_top_bar_title_text),
                        navigationIconOnClick = { viewModel.popBack() },
                        actions = {
                            Text(
                                text = stringResource(R.string.note_template_view_top_bar_action_text),
                                color = colors.textPrimary,
                                style = AppTheme.typography.bodyTextStyle1,
                                lineHeight = 22.sp,
                                letterSpacing = -(0.41.sp),
                                modifier = Modifier
                                    .padding(end = 8.dp)
                                    .clickable(
                                        interactionSource = MutableInteractionSource(),
                                        indication = rememberRipple(bounded = false),
                                        onClick = {
                                            viewModel.navigateToManageTemplates()
                                        }
                                    )
                            )
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        LazyVerticalGrid(
            columns = GridCells.Fixed(2),
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            contentPadding = PaddingValues(horizontal = 16.dp, vertical = 16.dp)
        ) {
            if (templates.isEmpty()) {
                item {
                    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                        Text(
                            text = stringResource(R.string.note_template_view_empty_templates_text),
                            color = colors.textSecondary,
                            modifier = Modifier.fillMaxWidth().padding(horizontal = 20.dp)
                        )
                    }
                }
            } else {
                itemsIndexed(templates) { index, template ->
                    Column(
                        modifier = Modifier
                            .fillMaxWidth()
                            .aspectRatio(1f)
                            .padding(
                                end = if (index % 2 == 0) 8.dp else 0.dp,
                                start = if (index % 2 == 0) 0.dp else 8.dp,
                                top = 8.dp,
                                bottom = 8.dp
                            )
                            .background(
                                if (isDarkMode) darkTemplateBackground else lightTemplateBackground,
                                shape = RoundedCornerShape(12.dp)
                            )
                            .clip(RoundedCornerShape(12.dp))
                            .motionClickEvent {
                                viewModel.popBackWithTemplateId(template.id)
                            }
                    ) {
                        Text(
                            text = template.title,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 12.dp, start = 12.dp, end = 12.dp),
                            style = AppTheme.typography.notesHeaderStyle,
                            color = colors.textPrimary
                        )

                        Text(
                            text = template.format,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 8.dp, start = 12.dp, end = 12.dp, bottom = 12.dp),
                            style = AppTheme.typography.notesDateStyle,
                            overflow = TextOverflow.Clip,
                            color = colors.textSecondary
                        )
                    }
                }
            }
        }
    }
}

private val lightTemplateBackground = Color(0xFFFAF7F6)
private val darkTemplateBackground = Color(0x66493D36)
