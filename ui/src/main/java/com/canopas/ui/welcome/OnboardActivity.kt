package com.canopas.ui.welcome

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.base.ui.themes.JustlyAppTheme
import com.canopas.ui.R
import com.canopas.ui.customview.jetpackview.PagerIndicator
import com.canopas.ui.welcome.wakeupactivity.WakeUpActivity
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint

const val HORIZONTAL_PAGER_COUNT = 4

@AndroidEntryPoint
class OnboardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val viewModel = hiltViewModel<OnboardViewModel>()
            val isDarkMode by viewModel.isDarkModeEnabled.collectAsState()

            JustlyAppTheme(isDarkMode = isDarkMode) {
                Column(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(colors.background)
                ) {
                    FeaturedOnboardView(viewModel)
                }
            }
        }
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun FeaturedOnboardView(viewModel: OnboardViewModel) {
    val context = LocalContext.current
    val loginState by viewModel.loginState.collectAsState(LoginState.START)
    val isLoginInProgress = loginState is LoginState.LOADING ||
        loginState is LoginState.SUCCESS
    val pagerState = rememberPagerState()
    val systemUiController = rememberSystemUiController()
    val defaultColor = colors.background
    val isDarkMode = ComposeTheme.isDarkMode

    LaunchedEffect(key1 = Unit) {
        systemUiController.setStatusBarColor(
            color = defaultColor,
            darkIcons = !isDarkMode
        )
    }

    LaunchedEffect(key1 = Unit) {
        viewModel.onStart()
    }

    LaunchedEffect(key1 = loginState) {
        loginState.let {
            if (loginState == LoginState.SUCCESS) {
                val navigateToWakeUpActivity =
                    Intent(context as OnboardActivity, WakeUpActivity::class.java)
                context.startActivity(navigateToWakeUpActivity)
                context.finish()
            } else if (loginState is LoginState.FAILURE) {
                val message = (loginState as LoginState.FAILURE).message
                showBanner(context, message)
                viewModel.restart()
            }
        }
    }

    LaunchedEffect(Unit) {
        pagerState.animateScrollToPage(pagerState.currentPage)
    }

    Column {
        HorizontalPagerView(
            Modifier
                .fillMaxSize()
                .padding(horizontal = 20.dp)
                .padding(top = 20.dp)
                .weight(1f),
            pagerState = pagerState
        )

        Box(
            Modifier
                .offset(y = -(20.dp))
                .height(20.dp)
                .fillMaxWidth()
        ) {
            Box(
                Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            listOf(
                                Color.Transparent,
                                colors.background
                            )
                        )
                    )
            )
        }

        Column(
            modifier = Modifier.fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            PagerIndicator(
                pagerState = pagerState,
                count = HORIZONTAL_PAGER_COUNT,
                activeColor = colors.primary,
                inactiveColor = InactiveDotIndicatorColor,
                activeIndicatorWidth = 16.dp,
                spacing = 6.dp,
                modifier = Modifier.padding(bottom = 30.dp)
            )
            Box(contentAlignment = Alignment.Center) {
                val showLoginButton = pagerState.currentPage == 3
                if (showLoginButton) {
                    PrimaryButton(
                        onClick = {
                            viewModel.anonymousLogin()
                        },
                        shape = RoundedCornerShape(50),
                        text = stringResource(id = R.string.on_board_screen_btn_get_started_text),
                        colors = ButtonDefaults.buttonColors(
                            backgroundColor = if (isLoginInProgress) colors.primary.copy(0.6f) else colors.primary,
                            contentColor = Color.White
                        ),
                        modifier = Modifier
                            .widthIn(max = 600.dp)
                            .motionClickEvent { }
                            .fillMaxWidth(),
                        isProcessing = isLoginInProgress
                    )
                }
            }
            Spacer(modifier = Modifier.height(50.dp))
        }
    }
}

@Composable
fun IntroBuildHeader() {
    val scrollState = rememberScrollState()

    val baseText = stringResource(id = R.string.on_board_screen_title)
    val baseHighlightText = stringResource(id = R.string.on_board_screen_title_highlight)

    val animatePartsList =
        listOf(
            stringResource(id = R.string.on_board_screen_title_part1),
            stringResource(id = R.string.on_board_screen_title_part2),
            stringResource(id = R.string.on_board_screen_title_part3),
            stringResource(id = R.string.on_board_screen_title_part4),
            stringResource(id = R.string.on_board_screen_title_part5),
            stringResource(id = R.string.on_board_screen_title_part6),
        )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        Row(
            modifier = Modifier.padding(vertical = 20.dp)
        ) {
            Text(
                text = stringResource(id = R.string.app_name),
                style = AppTheme.typography.introHeaderJustlyText,
                color = colors.primary,
                textAlign = TextAlign.Center
            )
        }

        Spacer(modifier = Modifier.weight(1f))

        Box(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth(),
            contentAlignment = Alignment.TopStart
        ) {
            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .alpha(0f),
                text = baseText + animatePartsList.first() + animatePartsList.first(),
                style = AppTheme.typography.introHeaderTextStyle,
                color = colors.textPrimary,
                textAlign = TextAlign.Start
            )

            AnimateTypewriterText(
                baseText = baseText,
                highlightText = baseHighlightText,
                parts = animatePartsList
            )
        }

        Spacer(modifier = Modifier.weight(0.8f))
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun HorizontalPagerView(modifier: Modifier, pagerState: PagerState) {
    HorizontalPager(
        count = HORIZONTAL_PAGER_COUNT,
        modifier = modifier,
        state = pagerState,
        verticalAlignment = Alignment.CenterVertically
    ) { swipeItem ->

        when (swipeItem) {
            0 -> {
                IntroBuildHeader()
            }
            1 -> {
                IntroSwipeScreenOne()
            }
            2 -> {
                IntroSwipeScreenTwo()
            }
            3 -> {
                IntroSwipeScreenThree()
            }
        }
    }
}

private val InactiveDotIndicatorColor = Color(0xFFD9D9D9)
