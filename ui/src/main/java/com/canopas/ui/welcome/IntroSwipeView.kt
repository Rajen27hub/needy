package com.canopas.ui.welcome

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterBoldFont
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.ui.R
import kotlinx.coroutines.delay

@Composable
fun AnimateTypewriterText(baseText: String, highlightText: String, parts: List<String>) {

    val highlightStart = baseText.indexOf(highlightText)

    var partIndex by remember {
        mutableStateOf(0)
    }

    var partText by remember {
        mutableStateOf("")
    }

    val textToDisplay = "$baseText$partText"

    LaunchedEffect(key1 = parts) {
        while (partIndex <= parts.size) {

            val part = parts[partIndex]

            part.forEachIndexed { charIndex, _ ->
                partText = part.substring(startIndex = 0, endIndex = charIndex + 1)
                delay(100)
            }

            delay(1000)

            part.forEachIndexed { charIndex, _ ->
                partText = part
                    .substring(startIndex = 0, endIndex = part.length - (charIndex + 1))
                delay(30)
            }

            delay(500)

            partIndex = (partIndex + 1) % parts.size
        }
    }

    val isDarkMode = isDarkMode

    var selectedPartRects by remember { mutableStateOf(listOf<Rect>()) }

    Row(modifier = Modifier.wrapContentSize()) {
        Text(
            text = textToDisplay,
            style = AppTheme.typography.introHeaderTextStyle,
            color = colors.textPrimary,
            modifier = Modifier.drawBehind {
                val borderSize = 20.sp.toPx()

                selectedPartRects.forEach { rect ->
                    val selectedRect = rect.translate(0f, -borderSize / 1.5f)
                    drawLine(
                        color = if (isDarkMode) darkIntroTextBg else lightIntroTextBg,
                        start = Offset(selectedRect.left, selectedRect.bottom),
                        end = selectedRect.bottomRight,
                        strokeWidth = borderSize
                    )
                }
            },
            onTextLayout = { layoutResult ->
                val start = baseText.length
                val end = textToDisplay.count()
                selectedPartRects = if (start < end) {
                    layoutResult
                        .getBoundingBoxesForRange(
                            start = start,
                            end = end - 1
                        )
                } else {
                    emptyList()
                }

                if (highlightStart >= 0) {
                    selectedPartRects = selectedPartRects + layoutResult
                        .getBoundingBoxesForRange(
                            start = highlightStart,
                            end = highlightStart + highlightText.length
                        )
                }
            }
        )
    }
}

fun TextLayoutResult.getBoundingBoxesForRange(start: Int, end: Int): List<Rect> {
    var prevRect: Rect? = null
    var firstLineCharRect: Rect? = null
    val boundingBoxes = mutableListOf<Rect>()
    for (i in start..end) {
        val rect = getBoundingBox(i)
        val isLastRect = i == end

        // single char case
        if (isLastRect && firstLineCharRect == null) {
            firstLineCharRect = rect
            prevRect = rect
        }

        // `rect.right` is zero for the last space in each line
        // looks like an issue to me, reported: https://issuetracker.google.com/issues/197146630
        if (!isLastRect && rect.right == 0f) continue

        if (firstLineCharRect == null) {
            firstLineCharRect = rect
        } else if (prevRect != null) {
            if (prevRect.bottom != rect.bottom || isLastRect) {
                boundingBoxes.add(
                    firstLineCharRect.copy(right = prevRect.right)
                )
                firstLineCharRect = rect
            }
        }
        prevRect = rect
    }
    return boundingBoxes
}

@Composable
fun IntroSwipeScreenOne() {
    IntroSwipeItem(
        introImage = painterResource(id = R.drawable.ic_intro_screen_image1),
        title = stringResource(id = R.string.intro_screen_description_header_text1),
        descriptionText1 = stringResource(id = R.string.on_board_screen_item_1_part1_description),
        descriptionText2 = stringResource(id = R.string.on_board_screen_item_1_part2_description),
        descriptionText3 = stringResource(id = R.string.on_board_screen_item_1_part3_description),
        descriptionText4 = "",
        descriptionText5 = ""
    )
}

@Composable
fun IntroSwipeScreenTwo() {
    IntroSwipeItem(
        introImage = painterResource(id = R.drawable.ic_intro_screen_image2),
        title = stringResource(R.string.intro_screen_description_header_text2),
        descriptionText1 = stringResource(R.string.on_board_screen_item_2_part1_description),
        descriptionText2 = stringResource(R.string.on_board_screen_item_2_part2_description),
        descriptionText3 = stringResource(R.string.on_board_screen_item_2_part3_description),
        descriptionText4 = "",
        descriptionText5 = ""
    )
}

@Composable
fun IntroSwipeScreenThree() {
    IntroSwipeItem(
        introImage = painterResource(id = R.drawable.ic_intro_screen_image3),
        title = stringResource(id = R.string.intro_screen_description_header_text3),
        descriptionText1 = stringResource(id = R.string.on_board_screen_item_3_part1_description),
        descriptionText2 = stringResource(id = R.string.on_board_screen_item_3_part2_description),
        descriptionText3 = stringResource(id = R.string.on_board_screen_item_3_part3_description),
        descriptionText4 = stringResource(id = R.string.on_board_screen_item_3_part4_description),
        descriptionText5 = stringResource(id = R.string.on_board_screen_item_3_part5_description)
    )
}

@Composable
fun IntroSwipeItem(
    introImage: Painter,
    title: String,
    descriptionText1: String,
    descriptionText2: String,
    descriptionText3: String,
    descriptionText4: String?,
    descriptionText5: String?,
) {
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .verticalScroll(scrollState)
            .fillMaxWidth()
            .padding(horizontal = 20.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        Image(
            painter = introImage,
            contentDescription = "intro screen image",
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .aspectRatio(1f)
                .padding(bottom = 40.dp)
        )

        Text(
            text = title,
            color = colors.textPrimary,
            style = AppTheme.typography.onBoardDescriptionTextStyle,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .padding(bottom = 8.dp)
        )

        Text(
            text = buildAnnotatedString {
                append(descriptionText1)
                withStyle(
                    style = SpanStyle(
                        fontFamily = InterBoldFont
                    )
                ) {
                    append(descriptionText2)
                }
                append(descriptionText3)
                if (!descriptionText4.isNullOrBlank() && !descriptionText5.isNullOrBlank()) {
                    withStyle(
                        style = SpanStyle(
                            fontFamily = InterBoldFont
                        )
                    ) {
                        append(descriptionText4)
                    }
                    append(descriptionText5)
                }
            },
            style = AppTheme.typography.subTextStyle1,
            color = colors.textSecondary,
            textAlign = TextAlign.Center,
            lineHeight = 26.sp,
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
        )

        Spacer(modifier = Modifier.height(50.dp))
    }
}

private val lightIntroTextBg = Color(0x408559DA)
private val darkIntroTextBg = Color(0xff8559DA)
