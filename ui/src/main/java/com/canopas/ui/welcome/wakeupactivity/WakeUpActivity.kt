package com.canopas.ui.welcome.wakeupactivity

import android.content.Intent
import android.os.Bundle
import android.text.format.DateFormat
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.JustlyAppTheme
import com.canopas.ui.R
import com.canopas.ui.customview.jetpackview.CustomWheelTimePicker
import com.canopas.ui.welcome.kickstart.KickStartActivity
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint

const val EXTRA_WAKE_UP_TIME = "wake_up_time"

@AndroidEntryPoint
class WakeUpActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val viewModel = hiltViewModel<WakeUpActivityViewModel>()
            val isDarkMode by viewModel.isDarkModeEnabled.collectAsState()
            JustlyAppTheme(isDarkMode = isDarkMode) {
                WakeUpTimeView(viewModel)
            }
        }
    }
}

@Composable
fun WakeUpTimeView(viewModel: WakeUpActivityViewModel) {
    val systemUiController = rememberSystemUiController()
    val defaultColor = colors.background
    val isDarkMode = ComposeTheme.isDarkMode
    val context = LocalContext.current

    val state by viewModel.wakeUpState.collectAsState()
    val isLoadingState = state is WakeUpState.LOADING
    val selectedWakeUpTime by viewModel.selectedWakeUpTime.collectAsState()

    LaunchedEffect(key1 = state) {
        state.let {
            if (state == WakeUpState.SUCCESS) {
                val navigateToKickStartActivity = Intent(context as WakeUpActivity, KickStartActivity::class.java).apply {
                    putExtra(EXTRA_WAKE_UP_TIME, selectedWakeUpTime.filterNot { it.isWhitespace() }.take(5))
                }
                context.startActivity(navigateToKickStartActivity)
                context.finish()
            } else if (state is WakeUpState.FAILURE) {
                val message = (state as WakeUpState.FAILURE).message
                showBanner(context, message)
            }
        }
    }
    LaunchedEffect(key1 = Unit) {
        systemUiController.setStatusBarColor(
            color = defaultColor,
            darkIcons = !isDarkMode
        )
    }

    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .background(colors.background)
            .fillMaxSize(),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .verticalScroll(scrollState)
                .padding(horizontal = 20.dp)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 40.dp)
            ) {
                Text(
                    text = stringResource(R.string.wake_up_time_set_your_wake_up_time_text),
                    style = AppTheme.typography.h1TextStyle,
                    lineHeight = 34.sp,
                    letterSpacing = -(1.12.sp),
                    color = colors.textPrimary
                )

                Text(
                    text = stringResource(R.string.wake_up_time_to_help_you_get_text),
                    style = AppTheme.typography.subTextStyle,
                    lineHeight = 26.sp,
                    letterSpacing = -(0.72.sp),
                    color = colors.textSecondary,
                    modifier = Modifier.padding(top = 16.dp)
                )
            }

            Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                CustomWheelTimePicker(
                    modifier = Modifier.padding(vertical = 40.dp, horizontal = 20.dp),
                    is24HoursFormat = DateFormat.is24HourFormat(LocalContext.current),
                    height = 220.dp,
                    selectedTextStyle = AppTheme.typography.wakeUpTimeActiveTextStyle,
                    normalTextStyle = AppTheme.typography.wakeUpTimeInactiveTextStyle
                ) {
                    viewModel.setWakeUpTime(it)
                }
            }
        }

        Box(
            Modifier
                .offset(y = -(20.dp))
                .height(20.dp)
                .fillMaxWidth()
        ) {
            Box(
                Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            listOf(
                                Color.Transparent,
                                colors.background
                            )
                        )
                    )
            )
        }

        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(bottom = 40.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Text(
                text = stringResource(R.string.wake_up_time_you_can_always_change_text),
                style = AppTheme.typography.subTitleTextStyle1,
                color = colors.textSecondary,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 16.dp, start = 20.dp, end = 20.dp)
            )

            val buttonModifier = Modifier
                .align(alignment = Alignment.CenterHorizontally)
                .fillMaxWidth()
                .motionClickEvent { }

            PrimaryButton(
                onClick = {
                    viewModel.onContinueBtnClick()
                },
                shape = RoundedCornerShape(50),
                text = stringResource(R.string.wake_up_time_continue_btn_text),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = if (isLoadingState) colors.primary.copy(0.6f) else colors.primary,
                    contentColor = Color.White
                ),
                modifier = buttonModifier,
                isProcessing = isLoadingState
            )
        }
    }
}
