package com.canopas.ui.welcome

import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.DEVICE_TYPE
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.utils.Device
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class OnboardViewModel @Inject constructor(
    private val authManager: AuthManager,
    private val device: Device,
    private val appDispatcher: AppDispatcher,
    private val appAnalytics: AppAnalytics,
    private val preferencesDataStore: NoLonelyPreferences
) : ViewModel() {

    val loginState = MutableStateFlow<LoginState>(LoginState.START)
    val isDarkModeEnabled = MutableStateFlow(false)

    init {
        viewModelScope.launch {
            isDarkModeEnabled.value = preferencesDataStore.getIsDarkModeEnabled()
        }
    }

    fun anonymousLogin() = viewModelScope.launch {
        withContext(appDispatcher.IO) {
            appAnalytics.logEvent("tap_intro_1_button", null)
            loginState.emit(LoginState.LOADING)
            val loginRequest = LoginRequest(
                DEVICE_TYPE,
                device.getId(),
                device.getAppVersionCode(),
                device.deviceModel(),
                device.getDeviceOsVersion()
            )
            val response = authManager.login(loginRequest)
            if (response.hasError()) {
                loginState.emit(LoginState.FAILURE(response.errorMessage))
                val bundle = Bundle()
                bundle.putString("status_code", response.statusCode.toString())
                appAnalytics.logEvent("error_anonymous_login", bundle)
            } else {
                loginState.emit(LoginState.SUCCESS)
                preferencesDataStore.setIsWelcomeScreenShown(true)
            }
        }
    }

    fun onStart() {
        appAnalytics.logEvent("view_intro_1", null)
    }

    fun restart() {
        loginState.tryEmit(LoginState.START)
    }
}

sealed class LoginState {
    object START : LoginState()
    object LOADING : LoginState()
    object SUCCESS : LoginState()
    data class FAILURE(val message: String) : LoginState()
}
