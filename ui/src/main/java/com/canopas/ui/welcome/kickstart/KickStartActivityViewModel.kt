package com.canopas.ui.welcome.kickstart

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.utils.Device
import com.canopas.data.model.ActivityData
import com.canopas.data.model.SubscribeBody
import com.canopas.data.rest.NoLonelyService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@HiltViewModel
class KickStartActivityViewModel @Inject constructor(
    private val preferencesDataStore: NoLonelyPreferences,
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val device: Device,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val isDarkModeEnabled = MutableStateFlow(false)
    val selectedActivityIndex = MutableStateFlow<Int?>(null)
    val enableContinueBtn = MutableStateFlow(false)
    val kickStartState = MutableStateFlow<KickStartState>(KickStartState.START)
    val subscribeActivityState = MutableStateFlow<SubscribeActivityState>(SubscribeActivityState.START)
    val featuredActivities = MutableStateFlow<List<ActivityData>>(emptyList())

    init {
        viewModelScope.launch {
            isDarkModeEnabled.value = preferencesDataStore.getIsDarkModeEnabled()
            appAnalytics.logEvent("view_kick_start_life_screen")
        }
    }

    fun toggleSelectedActivity(activityIndex: Int) {
        if (selectedActivityIndex.value == activityIndex) {
            selectedActivityIndex.value = null
            enableContinueBtn.tryEmit(false)
        } else {
            selectedActivityIndex.tryEmit(activityIndex)
            enableContinueBtn.tryEmit(true)
        }
    }

    fun onContinueBtnClick(wakeUpTime: String?) {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("tap_kick_start_life_screen_continue")
                subscribeActivityState.tryEmit(SubscribeActivityState.LOADING)
                val time = (wakeUpTime ?: "08:00").split(":").map { it.toInt() }
                val remindTime = LocalTime.of(time[0], time[1]).plusMinutes(30)
                val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm")
                val selectedActivity = featuredActivities.value[selectedActivityIndex.value ?: 0]
                val subscribeBody = SubscribeBody(
                    activity_id = selectedActivity.id,
                    timezone = device.timeZone(),
                    repeat_type = 1,
                    repeat_on = "",
                    is_prompted = false,
                    activity_time = formatter.format(remindTime),
                    activity_duration = 30
                )
                service.subscribeActivity(subscribeBody).onSuccess {
                    preferencesDataStore.setIsKickStartScreenShown(true)
                    subscribeActivityState.tryEmit(SubscribeActivityState.SUCCESS)
                }.onFailure { e ->
                    Timber.e(e)
                    subscribeActivityState.tryEmit(SubscribeActivityState.FAILURE(e.toUserError(resources)))
                }
            }
        }
    }

    fun fetchFeaturedActivities() {
        appAnalytics.logEvent("view_kick_start_life_screen")
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                kickStartState.tryEmit(KickStartState.LOADING)
                service.getFeaturedActivities().onSuccess {
                    featuredActivities.tryEmit(it)
                    kickStartState.tryEmit(KickStartState.SUCCESS(it))
                }.onFailure { e ->
                    Timber.e(e)
                    kickStartState.tryEmit(KickStartState.FAILURE(e.toUserError(resources)))
                }
            }
        }
    }
}

sealed class KickStartState {
    object START : KickStartState()
    object LOADING : KickStartState()
    data class SUCCESS(val featuredActivities: List<ActivityData>) : KickStartState()
    data class FAILURE(val message: String) : KickStartState()
}

sealed class SubscribeActivityState {
    object START : SubscribeActivityState()
    object LOADING : SubscribeActivityState()
    object SUCCESS : SubscribeActivityState()
    data class FAILURE(val message: String) : SubscribeActivityState()
}
