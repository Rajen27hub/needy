package com.canopas.ui.welcome.kickstart

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.dpToSp
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.JustlyAppTheme
import com.canopas.data.model.ActivityData
import com.canopas.ui.R
import com.canopas.ui.home.HomeActivity
import com.canopas.ui.welcome.wakeupactivity.EXTRA_WAKE_UP_TIME
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class KickStartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val wakeUpTime = this.intent.extras?.getString(EXTRA_WAKE_UP_TIME)
            val viewModel = hiltViewModel<KickStartActivityViewModel>()
            val isDarkMode by viewModel.isDarkModeEnabled.collectAsState()
            JustlyAppTheme(isDarkMode = isDarkMode) {

                LaunchedEffect(key1 = Unit, block = {
                    viewModel.fetchFeaturedActivities()
                })

                val kickStartState by viewModel.kickStartState.collectAsState()
                val context = LocalContext.current
                kickStartState.let { state ->
                    when (state) {
                        is KickStartState.LOADING -> {
                            Box(
                                modifier = Modifier.fillMaxSize(),
                                contentAlignment = Alignment.Center
                            ) {
                                CircularProgressIndicator(color = colors.primary)
                            }
                        }
                        is KickStartState.SUCCESS -> {
                            val activities = state.featuredActivities
                            KickStartActivityView(viewModel, activities, wakeUpTime)
                        }
                        is KickStartState.FAILURE -> {
                            val message = state.message
                            showBanner(context, message)
                        }
                        else -> {}
                    }
                }
            }
        }
    }
}

@Composable
fun KickStartActivityView(
    viewModel: KickStartActivityViewModel,
    featuredActivities: List<ActivityData>,
    wakeUpTime: String?,
) {
    val systemUiController = rememberSystemUiController()
    val defaultColor = colors.background
    val isDarkMode = ComposeTheme.isDarkMode
    val context = LocalContext.current

    val subscribeActivityState by viewModel.subscribeActivityState.collectAsState()
    val enableContinueBtn by viewModel.enableContinueBtn.collectAsState()
    val isLoadingState = subscribeActivityState is SubscribeActivityState.LOADING

    LaunchedEffect(key1 = subscribeActivityState) {
        subscribeActivityState.let {
            if (subscribeActivityState == SubscribeActivityState.SUCCESS) {
                val navigateToHome = Intent(context as KickStartActivity, HomeActivity::class.java)
                context.startActivity(navigateToHome)
                context.finish()
            } else if (subscribeActivityState is SubscribeActivityState.FAILURE) {
                val message = (subscribeActivityState as SubscribeActivityState.FAILURE).message
                showBanner(context, message)
            }
        }
    }

    LaunchedEffect(key1 = Unit) {
        systemUiController.setStatusBarColor(
            color = defaultColor,
            darkIcons = !isDarkMode
        )
    }

    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .background(colors.background)
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .weight(1f)
                .verticalScroll(scrollState)
                .padding(top = 40.dp, start = 20.dp, end = 20.dp, bottom = 20.dp)
        ) {
            Text(
                text = stringResource(R.string.kick_start_view_header_text),
                style = AppTheme.typography.h1TextStyle,
                lineHeight = 34.sp,
                letterSpacing = -(1.12.sp),
                color = colors.textPrimary
            )

            Text(
                text = stringResource(R.string.kick_start_view_sub_header_text),
                style = AppTheme.typography.subTextStyle,
                lineHeight = 26.sp,
                letterSpacing = -(0.72.sp),
                color = colors.textSecondary,
                modifier = Modifier.padding(top = 16.dp)
            )

            Text(
                text = stringResource(R.string.kick_start_view_sub_header_text_part_two),
                style = AppTheme.typography.subTextStyle,
                lineHeight = 26.sp,
                letterSpacing = -(0.72.sp),
                color = colors.textSecondary,
                modifier = Modifier.padding(top = 16.dp)
            )

            Spacer(modifier = Modifier.height(40.dp))

            val selectedIndex by viewModel.selectedActivityIndex.collectAsState()
            val groupedList = featuredActivities.chunked(3)
            for (itemList in groupedList) {
                BoxWithConstraints(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    val horizontalPadding =
                        if (maxWidth < 320.dp) 8.dp else if (maxWidth > 400.dp) 40.dp else 20.dp
                    val horizontalSpacing = if (maxWidth > 400.dp) 24.dp else 8.dp
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(vertical = 8.dp),
                        horizontalArrangement = Arrangement.spacedBy(
                            space = horizontalSpacing,
                            alignment = Alignment.CenterHorizontally
                        ),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        for (item in itemList) {
                            val index by remember { mutableStateOf(featuredActivities.indexOf(item)) }
                            BoxWithConstraints(
                                modifier = Modifier
                                    .wrapContentSize()
                                    .border(
                                        1.dp,
                                        if (index == selectedIndex) colors.primary else colors.textSecondary,
                                        RoundedCornerShape(40.dp)
                                    )
                                    .clip(RoundedCornerShape(40.dp))
                                    .background(if (index == selectedIndex) colors.primary else colors.background)
                                    .motionClickEvent {
                                        viewModel.toggleSelectedActivity(index)
                                    },
                                contentAlignment = Alignment.Center
                            ) {
                                Text(
                                    text = item.name,
                                    style = AppTheme.typography.bodyTextStyle3,
                                    modifier = Modifier
                                        .padding(
                                            horizontal = horizontalPadding,
                                            vertical = 12.dp
                                        ),
                                    fontSize = dpToSp(dp = 14.dp),
                                    color = if (index == selectedIndex) White else colors.textSecondary,
                                    maxLines = 1,
                                    overflow = TextOverflow.Ellipsis
                                )
                            }
                        }
                    }
                }
            }
        }

        Box(
            Modifier
                .offset(y = -(20.dp))
                .height(20.dp)
                .fillMaxWidth()
        ) {
            Box(
                Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            listOf(
                                Color.Transparent,
                                colors.background
                            )
                        )
                    )
            )
        }

        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(top = 8.dp, bottom = 40.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            Text(
                text = stringResource(R.string.kick_start_view_footer_text),
                style = AppTheme.typography.subTitleTextStyle1,
                lineHeight = 24.sp,
                color = colors.textSecondary,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 20.dp, start = 20.dp, end = 20.dp)
            )

            val buttonModifier = Modifier
                .align(alignment = Alignment.CenterHorizontally)
                .fillMaxWidth()
                .motionClickEvent { }

            PrimaryButton(
                onClick = {
                    viewModel.onContinueBtnClick(wakeUpTime)
                },
                enabled = enableContinueBtn,
                shape = RoundedCornerShape(50),
                text = stringResource(R.string.kick_start_view_continue_btn_text),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = if (isLoadingState) colors.primary.copy(0.6f) else colors.primary,
                    contentColor = Color.White
                ),
                modifier = buttonModifier,
                isProcessing = isLoadingState
            )
        }
    }
}
