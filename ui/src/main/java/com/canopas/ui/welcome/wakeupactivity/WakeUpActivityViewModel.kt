package com.canopas.ui.welcome.wakeupactivity

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.utils.Device
import com.canopas.data.model.DefaultSubscriptionBody
import com.canopas.data.rest.NoLonelyService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class WakeUpActivityViewModel @Inject constructor(
    private val preferencesDataStore: NoLonelyPreferences,
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val device: Device,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val isDarkModeEnabled = MutableStateFlow(false)
    val selectedWakeUpTime = MutableStateFlow("")
    val wakeUpState = MutableStateFlow<WakeUpState>(WakeUpState.START)

    init {
        viewModelScope.launch {
            isDarkModeEnabled.value = preferencesDataStore.getIsDarkModeEnabled()
            appAnalytics.logEvent("view_wake_up_time_screen")
        }
    }

    fun setWakeUpTime(time: String) {
        selectedWakeUpTime.tryEmit(time)
    }

    fun onContinueBtnClick() {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("tap_wake_up_time_screen_continue")
                wakeUpState.tryEmit(WakeUpState.LOADING)
                val subscriptionBody = DefaultSubscriptionBody(
                    activity_time = selectedWakeUpTime.value.filterNot { it.isWhitespace() }.take(5),
                    timezone = device.timeZone()
                )
                service.subscribeDefaultActivity(subscriptionBody).onSuccess {
                    wakeUpState.tryEmit(WakeUpState.SUCCESS)
                    preferencesDataStore.setIsWakeUpScreenShown(true)
                }.onFailure { e ->
                    Timber.e(e)
                    wakeUpState.tryEmit(WakeUpState.FAILURE(e.toUserError(resources)))
                }
            }
        }
    }
}

sealed class WakeUpState {
    object START : WakeUpState()
    object LOADING : WakeUpState()
    object SUCCESS : WakeUpState()
    data class FAILURE(val message: String) : WakeUpState()
}
