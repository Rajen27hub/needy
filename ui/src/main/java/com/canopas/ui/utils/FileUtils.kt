package com.canopas.ui.utils

import android.content.Context
import android.widget.Toast
import com.canopas.ui.R
import com.canopas.ui.utils.log.LogUtils
import timber.log.Timber
import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.OutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

private const val MAX_LOGS_LINE = 10_000

fun generateReportFile(context: Context): File {
    val sysLogFile = generateSystemLogsFile(context)
    val file = LogUtils.getLogFile(context)
    val zipFile = LogUtils.getZipFile(context)

    try {
        val list = arrayListOf(file.absolutePath)
        sysLogFile.let {
            list.add(sysLogFile.absolutePath)
        }
        zip(list, zipFile)
        return zipFile
    } catch (e: Exception) {
        Timber.e(e)
        Toast.makeText(
            context,
            R.string.feedback_screen_failed_to_attach_log,
            Toast.LENGTH_LONG
        ).show()
    }
    return zipFile
}

private fun generateSystemLogsFile(context: Context): File {
    val file = File(context.filesDir, "system-logs.txt")
    try {
        val logStream: InputStream? = getSystemLogs()
        if (logStream != null) {
            copyInputStreamToFile(logStream, file)
            return file
        }
    } catch (throwable: Throwable) {
        Timber.e(throwable, "System log file creation failed")
    }
    return file
}

private fun copyInputStreamToFile(inputStream: InputStream, file: File) {
    try {
        val out: OutputStream = FileOutputStream(file)
        val buf = ByteArray(1024)
        var len: Int
        while (inputStream.read(buf).also { len = it } > 0) {
            out.write(buf, 0, len)
        }
        out.close()
        inputStream.close()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

private fun getSystemLogs(): InputStream? {
    try {
        return Runtime.getRuntime().exec("logcat -t $MAX_LOGS_LINE").inputStream
    } catch (t: Throwable) {
        Timber.e(t, "Error while reading system logs")
    }
    return null
}

@Throws(IOException::class)
fun zip(files: List<String>, zipFile: File) {
    var origin: BufferedInputStream
    val outputStream = ZipOutputStream(BufferedOutputStream(FileOutputStream(zipFile)))
    outputStream.use { out ->
        val data = ByteArray(4096)
        for (i in files.indices) {
            val fi = FileInputStream(files[i])
            origin = BufferedInputStream(fi)
            try {
                val entry = ZipEntry(files[i].substring(files[i].lastIndexOf("/") + 1))
                out.putNextEntry(entry)
                var count: Int
                while (origin.read(data).also { count = it } != -1) {
                    out.write(data, 0, count)
                }
            } finally {
                origin.close()
            }
        }
    }
}
