package com.canopas.ui.utils

import android.content.Context
import android.content.Intent
import android.net.Uri

fun loadUriData(context: Context, mediaUrl: String) {
    val webIntent = Intent(
        Intent.ACTION_VIEW,
        Uri.parse(mediaUrl)
    )
    context.startActivity(webIntent)
}
