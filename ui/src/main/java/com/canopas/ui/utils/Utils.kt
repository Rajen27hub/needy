package com.canopas.ui.utils

import android.content.Context
import android.os.Build
import androidx.compose.ui.graphics.Color
import coil.ImageLoader
import coil.decode.GifDecoder
import coil.decode.ImageDecoderDecoder
import com.facebook.CallbackManager
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Date
import java.util.Locale

class Utils {

    companion object {
        fun getImageLoader(context: Context): ImageLoader {
            val imageLoader = ImageLoader.Builder(context)
                .components {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        add(ImageDecoderDecoder.Factory())
                    } else {
                        add(GifDecoder.Factory())
                    }
                }
                .build()
            return imageLoader
        }
    }
}

object FacebookUtil {
    val callbackManager by lazy {
        CallbackManager.Factory.create()
    }
}

object DateUtils {

    private val DATE_SERVER_FORMAT by lazy { SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.getDefault()) }
    private val LONG_DATE_FORMAT by lazy { SimpleDateFormat("EEEE, dd MMMM", Locale.getDefault()) }

    fun getSystemCurrentHour(): Int {
        val formatter = SimpleDateFormat("HH", Locale.getDefault())
        return formatter.format(Date()).toInt()
    }

    fun getSystemCurrentMinute(): Int {
        val formatter = SimpleDateFormat("mm", Locale.getDefault())
        return formatter.format(Date()).toInt()
    }

    fun getCurrentDay(): String {
        return LONG_DATE_FORMAT.format(Date())
    }

    private fun String.serverTimestampToDate(): Date {
        return DATE_SERVER_FORMAT.parse(this) ?: return Date().apply { time = 0 }
    }

    fun getLongFormat(createdAt: String): String {
        val date = createdAt.serverTimestampToDate()
        return LONG_DATE_FORMAT.format(date)
    }

    fun getNotesColor(createdAt: String, id: Int): Pair<Color, Color> {
        val date = createdAt.serverTimestampToDate()

        val calendar = Calendar.getInstance().apply { time = date }
        val dayOfYear = calendar.get(Calendar.DAY_OF_YEAR)

        val index = (dayOfYear + id) % 4
        val noteColor = notesColor[index]
        val notesBorderColor = notesBorderColor[index]

        return Pair(noteColor, notesBorderColor)
    }

    fun Date.toStartOfDay(): Date {
        val calendar = Calendar.getInstance()
        calendar.time = this
        val year = calendar[Calendar.YEAR]
        val month = calendar[Calendar.MONTH]
        val day = calendar[Calendar.DATE]
        calendar.set(year, month, day, 0, 0, 0)
        return calendar.time
    }
}

private val notesCardColor1 = Color(0xFFD4BD66).copy(0.14f)
private val notesCardColor2 = Color(0xFFE9BCAE).copy(0.10f)
private val notesCardColor3 = Color(0xFF93CB99).copy(0.10f)
private val notesCardColor4 = Color(0xFF8EC6D8).copy(0.14f)
private val notesColor = listOf(notesCardColor1, notesCardColor2, notesCardColor3, notesCardColor4)
private val notesBorderColor =
    listOf(Color(0xFFD4BD66), Color(0xFFE9BCAE), Color(0xFF93CB99), Color(0xFF76D0E4))
