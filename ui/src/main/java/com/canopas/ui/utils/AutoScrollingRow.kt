package com.canopas.ui.utils

import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animate
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.MutatePriority
import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.horizontalScroll
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.rememberScrollState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import kotlinx.coroutines.launch

@Composable
fun AutoScrollingRow(
    width: Float,
    cardSize: Float,
    index: Int,
    itemContent: @Composable () -> Unit,
) {
    val scrollState = rememberScrollState()
    val coroutineScope = rememberCoroutineScope()

    Row(
        modifier = Modifier
            .horizontalScroll(scrollState, enabled = false),
    ) {
        itemContent()
        itemContent()
    }

    LaunchedEffect(key1 = Unit) {
        coroutineScope.launch {
            scrollState.autoScroll(
                width,
                cardSize * if (index % 2 == 0) {
                    1f
                } else {
                    2f
                },
            )
        }
    }
}

suspend fun ScrollState.autoScroll(
    offset: Float,
    initialValue: Float,
    animationSpec: AnimationSpec<Float> = infiniteRepeatable(
        tween(
            durationMillis = 60000,
            easing = LinearEasing
        )
    )
) {
    scroll(MutatePriority.PreventUserInput) {
        animate(initialValue, offset + initialValue, animationSpec = animationSpec) { currentValue, _ ->
            scrollBy(currentValue - value)
        }
    }
}
