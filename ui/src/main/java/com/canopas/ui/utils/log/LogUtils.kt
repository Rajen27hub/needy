package com.canopas.ui.utils.log

import android.content.Context
import java.io.File

object LogUtils {

    @JvmStatic
    fun getLogFile(context: Context): File {
        return File(context.filesDir, "log.txt")
    }

    @JvmStatic
    fun getZipFile(context: Context): File {
        return File(context.externalCacheDir, "log.zip")
    }
}
