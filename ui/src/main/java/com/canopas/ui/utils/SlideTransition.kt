package com.canopas.ui.utils

import androidx.compose.animation.AnimatedContentScope
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInHorizontally
import androidx.compose.animation.slideOutHorizontally
import androidx.navigation.NavBackStackEntry
private const val TIME_DURATION = 400

@OptIn(ExperimentalAnimationApi::class)
val enterTransition: AnimatedContentScope<NavBackStackEntry>.() -> EnterTransition = {
    slideInHorizontally(
        initialOffsetX = { it },
        animationSpec = tween(
            durationMillis = TIME_DURATION,
            easing = FastOutSlowInEasing
        )
    ) + fadeIn(animationSpec = tween(TIME_DURATION))
}

@OptIn(ExperimentalAnimationApi::class)
val exitTransition: AnimatedContentScope<NavBackStackEntry>.() -> ExitTransition = {
    slideOutHorizontally(
        targetOffsetX = { -it / 5 },
        animationSpec = tween(
            durationMillis = TIME_DURATION,
            easing = FastOutLinearInEasing
        )
    ) + fadeOut(animationSpec = tween(TIME_DURATION))
}

@OptIn(ExperimentalAnimationApi::class)
val popExitTransition: AnimatedContentScope<NavBackStackEntry>.() -> ExitTransition = {
    slideOutHorizontally(
        targetOffsetX = { it / 5 },
        animationSpec = tween(
            durationMillis = TIME_DURATION
        )
    ) + fadeOut(animationSpec = tween(TIME_DURATION))
}

@OptIn(ExperimentalAnimationApi::class)
val popEnterTransition: AnimatedContentScope<NavBackStackEntry>.() -> EnterTransition = {
    slideInHorizontally(
        initialOffsetX = { -it },
        animationSpec = tween(
            durationMillis = TIME_DURATION,
            easing = FastOutSlowInEasing
        )
    ) + fadeIn(animationSpec = tween(TIME_DURATION))
}
