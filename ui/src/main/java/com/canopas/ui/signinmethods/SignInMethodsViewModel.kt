package com.canopas.ui.signinmethods

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.IntentSender
import android.os.Build
import android.os.Bundle
import androidx.activity.result.ActivityResult
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.ServiceError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.DEVICE_TYPE
import com.canopas.base.data.model.FREE_USER
import com.canopas.base.data.model.LOGIN_TYPE_GOOGLE
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.utils.Device
import com.canopas.ui.R
import com.canopas.ui.di.SIGN_IN_CLIENT
import com.canopas.ui.di.SIGN_IN_REQUEST
import com.canopas.ui.home.settings.buypremiumsubscription.DEVICE_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import com.facebook.FacebookException
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.identity.SignInClient
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class SignInMethodsViewModel @Inject constructor(
    private val authManager: AuthManager,
    private val device: Device,
    private val appDispatcher: AppDispatcher,
    private val navManager: NavManager,
    private val firebaseCrashlytics: FirebaseCrashlytics,
    private val appAnalytics: AppAnalytics,
    val oneTapClient: SignInClient,
    private val notificationManagerCompat: NotificationManagerCompat,
    @Named(SIGN_IN_REQUEST) val signInRequest: BeginSignInRequest,
    @Named(SIGN_IN_CLIENT) val signInClient: GoogleSignInClient
) : ViewModel() {

    val state = MutableStateFlow<SignInState>(SignInState.START)
    val isGoogleLoginInProgress = MutableStateFlow(false)
    val isFacebookLoginInProgress = MutableStateFlow(false)
    val isAboveAndroidS = MutableStateFlow(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)

    fun onStart() {
        appAnalytics.logEvent("view_sign_in", null)
    }

    fun proceedSignIn(credentials: AuthCredential, context: Context, loginType: Int) =
        viewModelScope.launch {
            state.tryEmit(SignInState.LOADING)
            appAnalytics.logEvent("received_token_from_sdk", null)
            FirebaseAuth.getInstance().signInWithCredential(credentials)
                .addOnCompleteListener(context as Activity) { task ->
                    if (task.isSuccessful && task.isComplete) {
                        val user = task.result?.user
                        user?.getIdToken(false)
                            ?.addOnCompleteListener { tokenResultTask ->
                                if (tokenResultTask.isSuccessful && tokenResultTask.result != null) {
                                    signInWithToken(tokenResultTask.result.token!!, loginType)
                                }
                            }
                    } else {
                        logError(
                            "Firebase google/facebook auth - Get Id Token error for loginType= $loginType",
                            task.exception
                        )
                        if (task.exception is FirebaseAuthInvalidCredentialsException) {
                            Timber.e(task.exception)
                            appAnalytics.logEvent("auth_invalid_credential_exception", null)
                        } else if (task.exception is FirebaseAuthUserCollisionException) {
                            appAnalytics.logEvent("auth_user_collision_exception", null)
                            if (loginType == LOGIN_TYPE_GOOGLE) {
                                state.tryEmit(SignInState.FAILURE("You have already created a Justly account with Facebook, please sign in with Facebook instead."))
                            } else {
                                state.tryEmit(SignInState.FAILURE("You have already created a Justly account with Google, please sign in with Google instead."))
                            }
                        }
                    }
                }
        }

    fun signInWithToken(tokenId: String, loginType: Int) = viewModelScope.launch {
        state.tryEmit(SignInState.LOADING)
        val loginRequest = LoginRequest(
            DEVICE_TYPE,
            device.getId(),
            device.getAppVersionCode(),
            device.deviceModel(),
            device.getDeviceOsVersion(),
            provider_firebase_id_token = tokenId,
            login_type = loginType
        )
        withContext(appDispatcher.IO) {
            try {
                val response = authManager.login(loginRequest, false)
                if (response.hasError()) {
                    Timber.e(response.errorMessage)
                    state.tryEmit(SignInState.FAILURE(response.errorMessage))
                    val bundle = Bundle()
                    bundle.putInt("status_code", response.statusCode)
                    if (loginType == LOGIN_TYPE_GOOGLE) {
                        appAnalytics.logEvent("error_google_login_server", bundle)
                    } else {
                        appAnalytics.logEvent("error_facebook_login_server", bundle)
                    }
                    if (response.error != ServiceError.NETWORKERROR) {
                        logError("Firebase google/facebook auth for loginType = $loginType - API error $response")
                    }
                } else {
                    onLoginSuccess()
                }
            } catch (e: Exception) {
                e.localizedMessage?.let {
                    Timber.e(it)
                    state.tryEmit(SignInState.FAILURE(it))
                }
            }
        }
    }

    private fun logError(message: String, cause: Throwable? = null) {
        firebaseCrashlytics.recordException(
            Throwable("Firebase Auth Error - $message", cause)
        )
        Timber.e(cause, message)
    }

    fun onGoogleSignInResult(result: ActivityResult, context: Context) {
        try {
            state.tryEmit(SignInState.LOADING)
            if (result.resultCode == RESULT_OK) {
                val googleCredentials = oneTapClient.getSignInCredentialFromIntent(result.data)
                val googleIdToken = googleCredentials.googleIdToken
                val credentials = GoogleAuthProvider.getCredential(googleIdToken, null)
                proceedSignIn(credentials, context as Activity, LOGIN_TYPE_GOOGLE)
            } else {
                Timber.e("Google SDK account is null! Intent Data:$result")
                resetState()
            }
        } catch (e: ApiException) {
            Timber.e(e, "Google sign-in SDK api exception.")
            val bundle = Bundle()
            bundle.putInt("error_code", e.statusCode)
            appAnalytics.logEvent("error_google_login_sdk", bundle)
            state.tryEmit(SignInState.FAILURE(context.getString(R.string.sign_in_methods_google_sign_in_failed_msg)))
        }
    }

    fun toggleLoadingState() {
        if (state.value !is SignInState.LOADING) state.tryEmit(SignInState.LOADING)
        else state.tryEmit(SignInState.START)
        isGoogleLoginInProgress.tryEmit(false)
        isFacebookLoginInProgress.tryEmit(false)
    }

    fun resetState() {
        state.tryEmit(SignInState.START)
        isGoogleLoginInProgress.tryEmit(false)
        isFacebookLoginInProgress.tryEmit(false)
    }

    private suspend fun onLoginSuccess() = withContext(appDispatcher.MAIN) {
        state.tryEmit(SignInState.SUCCESS)
        appAnalytics.logEvent("success_social_login", null)
        proceedLoginSuccessNavigation()
    }

    fun isNotificationOn(): Boolean {
        return notificationManagerCompat.areNotificationsEnabled()
    }

    fun proceedLoginSuccessNavigation() {
        if (isAboveAndroidS.value && !isNotificationOn()) {
            navManager.navigateToNotificationPermissionView()
        } else {
            navManager.popSignInScreens()
            if (authManager.currentUser?.session?.limitedAccess == true) {
                navManager.navigateToBuyPremiumScreen(
                    showBackArrow = false,
                    textType = DEVICE_LIMIT_TEXT_TYPE
                )
            } else {
                authManager.currentUser?.let {
                    if (!it.hasFirstname() || !it.hasValidEmail()) {
                        navManager.navigateToProfileScreen(showBackArrow = false, promptPremium = true)
                    } else if (it.userType == FREE_USER) {
                        navManager.navigateToBuyPremiumScreen(
                            showBackArrow = false,
                            showDismissButton = true
                        )
                    }
                }
            }
        }
    }

    fun popBackStack() {
        navManager.popBack()
    }

    fun navigateToPhoneLogin() {
        appAnalytics.logEvent("tap_sign_in_phone", null)
        navManager.navigateToPhoneLogin()
    }

    fun navigateToCreateAccount() {
        appAnalytics.logEvent("tap_sign_in_create_account", null)
        navManager.navigateToPhoneLogin()
    }

    fun addButtonClickAnalytics(type: Int) {
        if (type == LOGIN_TYPE_GOOGLE) {
            appAnalytics.logEvent("tap_sign_in_google", null)
        } else {
            isFacebookLoginInProgress.tryEmit(true)
            appAnalytics.logEvent("tap_sign_in_facebook", null)
        }
    }

    fun facebookSdkLoginError(error: FacebookException) {
        Timber.e(error, "Facebook SDK login error")
        val bundle = Bundle()
        bundle.putString("error_code", error.message)
        appAnalytics.logEvent("error_facebook_login_sdk", bundle)
        resetState()
    }

    fun onGoogleSignInClick() {
        isGoogleLoginInProgress.tryEmit(true)
        addButtonClickAnalytics(LOGIN_TYPE_GOOGLE)
    }

    fun logIntentException(e: IntentSender.SendIntentException) {
        Timber.e(e, "Couldn't start One Tap UI")
        val bundle = Bundle()
        bundle.putString("error_code", e.message)
        appAnalytics.logEvent("error_google_login_intent_sender", bundle)
    }

    private fun logApiException(e: ApiException) {
        Timber.e(e, "Api exception while fetching user account")
        val bundle = Bundle()
        bundle.putString("error_code", e.message)
        appAnalytics.logEvent("api_exception_for_google_sign_in_client", bundle)
    }

    fun logOneTapUIStatus(message: String) {
        appAnalytics.logEvent(message)
    }

    fun logEmptyCredentialsFailure(e: Exception) {
        Timber.e(e, "No credentials found")
        val bundle = Bundle()
        bundle.putString("error_code", e.message)
        appAnalytics.logEvent("error_google_login_intent_sender", bundle)
    }

    fun launchGoogleSignIn(result: ActivityResult, context: Context) {
        appAnalytics.logEvent("view_google_sign_in_client")
        if (result.resultCode == RESULT_OK) {
            try {
                val intent = result.data
                if (result.data != null) {
                    val task: Task<GoogleSignInAccount> =
                        GoogleSignIn.getSignedInAccountFromIntent(intent)
                    val account = task.getResult(ApiException::class.java)
                    account.idToken?.let {
                        val credentials = GoogleAuthProvider.getCredential(it, null)
                        proceedSignIn(credentials, context as Activity, LOGIN_TYPE_GOOGLE)
                    }
                } else {
                    val bundle = Bundle()
                    bundle.putString("error_code", result.resultCode.toString())
                    appAnalytics.logEvent("error_google_login_for_sign_in_client", bundle)
                    state.tryEmit(SignInState.FAILURE(context.getString(R.string.sign_in_methods_add_google_account_message)))
                }
            } catch (e: ApiException) {
                logApiException(e)
                state.tryEmit(SignInState.FAILURE(context.getString(R.string.sign_in_methods_add_google_account_message)))
            }
        } else {
            val bundle = Bundle()
            bundle.putString("error_code", result.resultCode.toString())
            appAnalytics.logEvent("error_google_login_for_sign_in_client", bundle)
        }
    }
}

sealed class SignInState {
    object START : SignInState()
    object LOADING : SignInState()
    object SUCCESS : SignInState()
    data class FAILURE(val message: String) : SignInState()
}
