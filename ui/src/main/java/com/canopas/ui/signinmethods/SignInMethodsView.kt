package com.canopas.ui.signinmethods

import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import androidx.activity.compose.ManagedActivityResultLauncher
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.ActivityResult
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.model.LOGIN_TYPE_FACEBOOK
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterMediumFont
import com.canopas.base.ui.InterSemiBoldFont
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.SecondaryOutlineButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.textColor
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.ui.R
import com.canopas.ui.utils.FacebookUtil
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.firebase.auth.FacebookAuthProvider

@Composable
fun SignInMethodsView() {

    val viewModel = hiltViewModel<SignInMethodsViewModel>()
    val context = LocalContext.current

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart()
    })

    val authResultLauncher =
        rememberLauncherForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) { result ->
            viewModel.onGoogleSignInResult(result, context)
        }

    val signInClientLauncher = rememberLauncherForActivityResult(contract = ActivityResultContracts.StartActivityForResult()) { result ->
        viewModel.launchGoogleSignIn(result, context)
    }

    val state by viewModel.state.collectAsState()
    state.let { signInState ->
        when (signInState) {
            is SignInState.FAILURE -> {
                val message = signInState.message
                showBanner(context, message)
                viewModel.resetState()
            }
            else -> {
                SignInMethods(viewModel, authResultLauncher, signInClientLauncher)
            }
        }
    }
}

@Composable
fun SignInMethods(
    viewModel: SignInMethodsViewModel,
    authResultLauncher: ManagedActivityResultLauncher<IntentSenderRequest, ActivityResult>,
    signInClientLauncher: ManagedActivityResultLauncher<Intent, ActivityResult>
) {
    val scrollState = rememberScrollState()
    val context = LocalContext.current
    val isGoogleLoginInProgress by viewModel.isGoogleLoginInProgress.collectAsState()
    val isFacebookLoginInProgress by viewModel.isFacebookLoginInProgress.collectAsState()
    val buttonEnabledState = !isGoogleLoginInProgress && !isFacebookLoginInProgress
    val oneTapClient = viewModel.oneTapClient
    val signInRequest = viewModel.signInRequest
    val signInClient = viewModel.signInClient

    val fbCallbackManager = FacebookUtil.callbackManager
    val facebookReadPermissions = listOf("email", "public_profile")
    val facebookResultCallback = object : FacebookCallback<LoginResult> {

        override fun onSuccess(result: LoginResult) {
            result.accessToken.token.let {
                val credentials = FacebookAuthProvider.getCredential(it)
                viewModel.proceedSignIn(credentials, context, LOGIN_TYPE_FACEBOOK)
            }
        }

        override fun onCancel() {
            viewModel.toggleLoadingState()
        }

        override fun onError(error: FacebookException) {
            viewModel.facebookSdkLoginError(error)
            showBanner(
                context,
                context.getString(R.string.sign_in_methods_facebook_sign_in_failed_msg)
            )
        }
    }

    Scaffold(
        topBar = {
            TopAppBar(
                title = {},
                navigationIcon = {
                    IconButton(onClick = {
                        viewModel.popBackStack()
                    }) {
                        Icon(
                            Icons.Filled.ArrowBack,
                            contentDescription = null,
                            tint = colors.textPrimary
                        )
                    }
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp,
            )
        }
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(it)
                .verticalScroll(scrollState)
                .background(colors.background),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {

            Image(
                painter = painterResource(id = R.drawable.ic_nolonely_logo),
                contentDescription = null,
                modifier = Modifier
                    .padding(top = 48.dp, bottom = 20.dp)
                    .fillMaxWidth()
                    .size(75.dp)
            )

            Text(
                text = buildAnnotatedString {
                    append(stringResource(R.string.sign_in_method_compound_title_text_part1))
                    withStyle(
                        style = SpanStyle(
                            fontFamily = InterSemiBoldFont,
                            color = colors.textPrimary
                        )
                    ) {
                        append(stringResource(R.string.sign_in_method_compound_title_text_part2))
                    }
                },
                lineHeight = 38.sp,
                letterSpacing = -(1.12.sp),
                fontSize = 28.sp,
                fontFamily = InterMediumFont,
                color = colors.textSecondary,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp, vertical = 40.dp),
            )

            ImageButton(
                image = R.drawable.ic_google_logo,
                buttonText = stringResource(R.string.sign_in_method_sign_in_with_google_btn_text),
                backgroundColor = if (buttonEnabledState) bottomSheetBg else bottomSheetBg.copy(0.6f),
                contentColor = textColor.copy(0.87f),
                showLoader = isGoogleLoginInProgress
            ) {
                if (buttonEnabledState) {
                    viewModel.onGoogleSignInClick()
                    oneTapClient.beginSignIn(signInRequest)
                        .addOnSuccessListener { result ->
                            try {
                                authResultLauncher.launch(IntentSenderRequest.Builder(result.pendingIntent.intentSender).build())
                                viewModel.logOneTapUIStatus("view_one_tap_ui_for_sign_in")
                            } catch (e: IntentSender.SendIntentException) {
                                viewModel.logIntentException(e)
                                signInClientLauncher.launch(signInClient.signInIntent)
                            }
                        }
                        .addOnFailureListener { e ->
                            viewModel.logEmptyCredentialsFailure(e)
                            signInClientLauncher.launch(signInClient.signInIntent)
                        }
                }
            }

            ImageButton(
                image = R.drawable.ic_facebook_logo,
                buttonText = stringResource(R.string.sign_in_method_sign_in_with_facebook_btn_text),
                backgroundColor = if (buttonEnabledState) facebookButtonColor else facebookButtonColor.copy(0.6f),
                showLoader = isFacebookLoginInProgress
            ) {
                if (buttonEnabledState) {
                    viewModel.addButtonClickAnalytics(LOGIN_TYPE_FACEBOOK)
                    val loginManager = LoginManager.getInstance()
                    loginManager.registerCallback(fbCallbackManager, facebookResultCallback)
                    loginManager.logInWithReadPermissions(context as Activity, facebookReadPermissions)
                }
            }

            ImageButton(
                image = R.drawable.ic_phone_logo,
                buttonText = stringResource(R.string.sign_in_method_sign_in_with_phone_btn_text),
                backgroundColor = if (buttonEnabledState) colors.primary else colors.primary.copy(0.6f)
            ) {
                if (buttonEnabledState) {
                    viewModel.navigateToPhoneLogin()
                }
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 20.dp),
                horizontalArrangement = Arrangement.Center,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Divider(
                    Modifier
                        .width(55.dp),
                    thickness = 1.dp,
                    color = colors.textSecondary
                )
                Text(
                    text = stringResource(R.string.sign_in_method_or_text),
                    style = AppTheme.typography.bodyTextStyle2,
                    color = colors.textSecondary,
                    modifier = Modifier.padding(horizontal = 12.dp)
                )
                Divider(
                    Modifier
                        .width(55.dp),
                    thickness = 1.dp,
                    color = colors.textSecondary
                )
            }

            SecondaryOutlineButton(
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .fillMaxWidth()
                    .motionClickEvent { }
                    .padding(horizontal = 20.dp),
                onClick = {
                    viewModel.navigateToCreateAccount()
                },
                border = BorderStroke(1.dp, colors.primary),
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.outlinedButtonColors(contentColor = colors.primary, backgroundColor = colors.background),
                text = stringResource(R.string.sign_in_method_create_account_btn_text),
                textModifier = Modifier.padding(vertical = 6.dp),
                color = colors.primary,
            )

            Spacer(modifier = Modifier.height(50.dp))
        }
    }
}

@Composable
fun ImageButton(
    image: Int,
    buttonText: String,
    backgroundColor: Color,
    contentColor: Color = White,
    showLoader: Boolean = false,
    onClick: () -> Unit,
) {
    if (showLoader) {
        Box(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth().background(colors.background)
                .padding(start = 20.dp, end = 20.dp, top = 20.dp)
                .height(48.dp),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator(color = colors.primary)
        }
    } else {
        Row(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(start = 20.dp, end = 20.dp, top = 20.dp)
                .height(48.dp)
                .motionClickEvent { onClick() }
                .background(backgroundColor, RoundedCornerShape(50.dp)),
            horizontalArrangement = Arrangement.Center, verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = painterResource(id = image),
                contentDescription = null,
                modifier = Modifier
                    .size(18.dp)
            )
            Text(
                text = buttonText,
                style = AppTheme.typography.buttonStyle,
                color = contentColor,
                modifier = Modifier.padding(horizontal = 12.dp)
            )
        }
    }
}

private val facebookButtonColor = Color(0xFF4267B2)
private val bottomSheetBg = Color(0xFFEEEEEE)
