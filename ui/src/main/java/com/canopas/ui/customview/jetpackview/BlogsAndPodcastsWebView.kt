package com.canopas.ui.customview.jetpackview

import android.annotation.SuppressLint
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.White
import com.google.accompanist.web.LoadingState
import com.google.accompanist.web.WebView
import com.google.accompanist.web.rememberWebViewNavigator
import com.google.accompanist.web.rememberWebViewState

@SuppressLint("SetJavaScriptEnabled")
@Composable
fun BlogsAndPodcastWebView(url: String) {
    val state = rememberWebViewState(url = url)
    val navigator = rememberWebViewNavigator()
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier.fillMaxSize().clip(RoundedCornerShape(topStart = 25.dp, topEnd = 25.dp)).background(
            White
        ).verticalScroll(scrollState),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        val loadingState = state.loadingState
        if (loadingState is LoadingState.Loading) {
            LinearProgressIndicator(
                color = ColorPrimary,
                modifier = Modifier.fillMaxWidth().clip(
                    CircleShape
                )
            )
        }
        WebView(
            state = state,
            navigator = navigator,
            modifier = Modifier
                .fillMaxSize()
                .padding(top = if (loadingState is LoadingState.Loading) 4.dp else 0.dp)
                .clip(RoundedCornerShape(topStart = 25.dp, topEnd = 25.dp)),
            onCreated = { webView ->
                webView.settings.javaScriptEnabled = true
                webView.settings.domStorageEnabled = true
            },
            onDispose = {
                it.destroy()
            }
        )
    }
}
