package com.canopas.ui.customview.jetpackview

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.size
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import coil.ImageLoader
import coil.compose.rememberAsyncImagePainter
import com.canopas.ui.R
import com.google.android.gms.common.util.DeviceProperties.isTablet

@Composable
fun ActivityMediaImagePlaceholder(imageLoader: ImageLoader) {
    val context = LocalContext.current
    val loaderSize = if (isTablet(context)) 300.dp else 150.dp
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Image(
            modifier = Modifier
                .size(loaderSize),
            painter = rememberAsyncImagePainter(
                model = R.drawable.ic_placeholder_loader,
                imageLoader = imageLoader
            ),
            contentScale = ContentScale.Crop,
            contentDescription = null
        )
    }
}
