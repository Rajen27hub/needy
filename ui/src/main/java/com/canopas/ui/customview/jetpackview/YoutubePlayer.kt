package com.canopas.ui.customview.jetpackview

import android.app.Activity
import android.content.pm.ActivityInfo
import androidx.appcompat.content.res.AppCompatResources
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.compose.ui.zIndex
import androidx.navigation.NavHostController
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.ui.R
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.options.IFramePlayerOptions
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.utils.loadOrCueVideo
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView
import com.pierfrancescosoffritti.androidyoutubeplayer.core.ui.DefaultPlayerUiController

@Composable
fun YoutubeViewDialog(url: String, navHostController: NavHostController? = null) {
    val lifecycleOwner = LocalLifecycleOwner.current
    val currentElapsedTime = remember { mutableStateOf(0f) }
    var state by remember {
        mutableStateOf<VideoState>(VideoState.LOADING)
    }
    val activity = LocalContext.current as Activity
    val systemUiController = rememberSystemUiController()
    val isDarkMode = ComposeTheme.isDarkMode
    val defaultColor = if (isDarkMode) darkColor else lightColor

    LaunchedEffect(key1 = Unit) {
        systemUiController.setStatusBarColor(
            color = Color.Black,
            darkIcons = false
        )
    }

    DisposableEffect(key1 = Unit, effect = {
        onDispose {
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
            systemUiController.setStatusBarColor(
                color = defaultColor,
                darkIcons = !isDarkMode
            )
        }
    })

    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black),
        contentAlignment = Alignment.Center
    ) {
        if (state == VideoState.LOADING) {
            CircularProgressIndicator(color = ColorPrimary, modifier = Modifier.zIndex(1f))
        }

        AndroidView(
            factory = {
                YouTubePlayerView(it).apply {
                    val youTubePlayerView: YouTubePlayerView = this
                    lifecycleOwner.lifecycle.addObserver(youTubePlayerView)
                    val listener: YouTubePlayerListener = object : AbstractYouTubePlayerListener() {
                        override fun onReady(youTubePlayer: YouTubePlayer) {
                            val defaultPlayerUiController = DefaultPlayerUiController(youTubePlayerView, youTubePlayer)
                            youTubePlayerView.enterFullScreen()
                            youTubePlayerView.setBackgroundColor(Color.Black.toArgb())
                            defaultPlayerUiController.setFullScreenButtonClickListener {
                                activity.requestedOrientation = activity.requestedOrientation.let {
                                    if (it == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
                                        ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                                    } else {
                                        ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                                    }
                                }
                            }
                            youTubePlayerView.isLongClickable = false
                            AppCompatResources.getDrawable(context, R.drawable.seek_backward)?.let { it1 ->
                                defaultPlayerUiController.setCustomAction1(it1) {
                                    currentElapsedTime.value -= 10f
                                    youTubePlayer.seekTo(currentElapsedTime.value)
                                }
                            }
                            AppCompatResources.getDrawable(context, R.drawable.seek_forward)?.let { it1 ->
                                defaultPlayerUiController.setCustomAction2(it1) {
                                    currentElapsedTime.value += 10f
                                    youTubePlayer.seekTo(currentElapsedTime.value)
                                }
                            }
                            youTubePlayerView.setCustomPlayerUi(defaultPlayerUiController.rootView)
                            youTubePlayer.loadOrCueVideo(lifecycleOwner.lifecycle, url.substringAfter("="), currentElapsedTime.value)
                            state = VideoState.FINISHED
                        }

                        override fun onCurrentSecond(youTubePlayer: YouTubePlayer, second: Float) {
                            currentElapsedTime.value = second
                        }
                    }
                    val options: IFramePlayerOptions = IFramePlayerOptions.Builder().controls(0).build()
                    enableAutomaticInitialization = false
                    youTubePlayerView.initialize(listener, options)
                }
            },
            modifier = Modifier
                .fillMaxSize()
                .background(Color.Black)
                .clip(RoundedCornerShape(8.dp))
                .zIndex(0f)
        )
        IconButton(
            onClick = {
                navHostController?.popBackStack()
            },
            modifier = Modifier
                .align(Alignment.TopEnd)
                .padding(8.dp)
        ) {
            Icon(Icons.Filled.Close, contentDescription = "Close", tint = Color.White)
        }
    }
}

sealed class VideoState {
    object LOADING : VideoState()
    object FINISHED : VideoState()
}

private val lightColor = Color(0xffffffff)
private val darkColor = Color(0xff121212)
