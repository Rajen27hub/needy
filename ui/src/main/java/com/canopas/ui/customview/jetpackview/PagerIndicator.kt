package com.canopas.ui.customview.jetpackview

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.WelcomeBtnSkipBg
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState

@ExperimentalPagerApi
@Composable
fun PagerIndicator(
    count: Int,
    pagerState: PagerState,
    modifier: Modifier = Modifier,
    activeColor: Color = ColorPrimaryDark,
    inactiveColor: Color = WelcomeBtnSkipBg,
    indicatorWidth: Dp = 7.dp,
    activeIndicatorWidth: Dp = 20.dp,
    indicatorHeight: Dp = indicatorWidth,
    spacing: Dp = 10.dp,
    indicatorShape: Shape = RoundedCornerShape(50)
) {

    Box(
        modifier = modifier,
        contentAlignment = Alignment.CenterStart
    ) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(spacing),
            verticalAlignment = Alignment.CenterVertically,
        ) {

            repeat(count) {
                val width =
                    if (pagerState.currentPage == it) activeIndicatorWidth else indicatorWidth
                val indicatorModifier = Modifier
                    .size(width = width, height = indicatorHeight)
                    .background(color = inactiveColor, shape = indicatorShape)
                Box(indicatorModifier)
            }
        }

        Box(
            Modifier
                .offset {
                    val scrollPosition = pagerState.currentPage + pagerState.currentPageOffset
                    IntOffset(
                        x = ((spacing + indicatorWidth) * scrollPosition).roundToPx(),
                        y = 0
                    )
                }
                .size(width = activeIndicatorWidth, height = indicatorHeight)
                .background(
                    color = activeColor,
                    shape = indicatorShape,
                )
        )
    }
}

@Preview
@Composable
@ExperimentalPagerApi
fun PreviewIndicator() {
    val pagerState = rememberPagerState(
        initialPage = 2,
    )

    PagerIndicator(3, pagerState)
}

private val ColorPrimaryDark = Color(0xFFFB709A)
