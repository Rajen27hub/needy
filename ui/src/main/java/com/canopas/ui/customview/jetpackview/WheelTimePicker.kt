package com.canopas.ui.customview.jetpackview

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.commandiron.wheel_picker_compose.core.SelectorProperties
import com.commandiron.wheel_picker_compose.core.WheelPickerDefaults
import dev.chrisbanes.snapper.ExperimentalSnapperApi
import dev.chrisbanes.snapper.SnapperLayoutInfo
import dev.chrisbanes.snapper.rememberLazyListSnapperLayoutInfo
import dev.chrisbanes.snapper.rememberSnapperFlingBehavior
import java.text.SimpleDateFormat
import java.time.LocalTime
import java.util.Calendar
import java.util.Locale
import kotlin.collections.ArrayList
import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.min

@Composable
fun CustomWheelTimePicker(
    modifier: Modifier = Modifier,
    is24HoursFormat: Boolean,
    startTime: LocalTime = LocalTime.of(7, 0),
    height: Dp = 200.dp,
    rowCount: Int = 5,
    selectorProperties: SelectorProperties = WheelPickerDefaults.selectorProperties(
        enabled = true,
        color = ColorPrimary,
        shape = RoundedCornerShape(40.dp),
        border = BorderStroke(0.dp, ColorPrimary)
    ),
    selectedTextStyle: TextStyle,
    normalTextStyle: TextStyle,
    onScrollStop: (selectedTime: String) -> Unit
) {
    val calendar = Calendar.getInstance()
    val hours = ArrayList<Hour>()
    for (hour in 0 until 24) {
        calendar.set(Calendar.HOUR_OF_DAY, hour)
        for (minuteIndex in 0..3) {
            calendar.set(Calendar.MINUTE, minuteIndex * 15)

            val hourText = SimpleDateFormat(
                if (is24HoursFormat) "HH   :   mm" else "hh   :   mm",
                Locale.getDefault()
            ).format(calendar.time)

            val hourCenterText = SimpleDateFormat(
                if (is24HoursFormat) "HH   :   mm" else "hh   :   mm   a",
                Locale.getDefault()
            ).format(calendar.time)

            hours.add(
                Hour(
                    text = hourText,
                    centerText = hourCenterText,
                    currentHour = hour,
                    currentMinute = minuteIndex * 15,
                    index = hour * 4 + minuteIndex
                )
            )
        }
    }
    val startIndex =
        hours.find { it.currentHour == startTime.hour && it.currentMinute == startTime.minute }?.index
            ?: 0
    var snapped by remember { mutableStateOf(startIndex) }

    Box(modifier = modifier, contentAlignment = Alignment.Center) {
        Row {
            // Hour
            WheelPicker(
                height = height,
                hours = hours,
                rowCount = rowCount,
                count = hours.size,
                startIndex = startIndex,
                selectorProperties = selectorProperties,
                selectedTextStyle = selectedTextStyle,
                normalTextStyle = normalTextStyle,
                onScrollFinished = { snappedIndex ->
                    val time = hours[snappedIndex]
                    val selectedTime = Calendar.getInstance().let {
                        it.set(Calendar.HOUR_OF_DAY, time.currentHour)
                        it.set(Calendar.MINUTE, time.currentMinute)
                        SimpleDateFormat("HH:mm", Locale.getDefault()).format(it.time)
                    }

                    onScrollStop(selectedTime)
                    snapped = snappedIndex
                    return@WheelPicker snappedIndex
                }
            )
        }
    }
}

@OptIn(ExperimentalSnapperApi::class)
@Composable
internal fun WheelPicker(
    modifier: Modifier = Modifier,
    hours: List<Hour>,
    startIndex: Int = 0,
    count: Int,
    rowCount: Int,
    height: Dp,
    selectorProperties: SelectorProperties = WheelPickerDefaults.selectorProperties(),
    selectedTextStyle: TextStyle,
    normalTextStyle: TextStyle,
    onScrollFinished: (snappedIndex: Int) -> Int? = { null },
) {
    val lazyListState = rememberLazyListState(startIndex)
    val snapperLayoutInfo = rememberLazyListSnapperLayoutInfo(lazyListState = lazyListState)
    val isScrollInProgress = lazyListState.isScrollInProgress

    LaunchedEffect(isScrollInProgress, startIndex) {
        onScrollFinished(
            calculateSnappedItemIndex(snapperLayoutInfo) ?: startIndex
        )?.let { lazyListState.scrollToItem(it) }
    }

    Box(
        modifier = modifier,
        contentAlignment = Alignment.CenterStart
    ) {

        if (selectorProperties.enabled().value) {
            Surface(
                modifier = Modifier.height(height / rowCount),
                shape = selectorProperties.shape().value,
                color = selectorProperties.color().value,
                border = selectorProperties.border().value
            ) {
                // Just for occupying space
                Box(
                    modifier = Modifier
                        .padding(horizontal = 40.dp)
                        .alpha(0f),
                ) {
                    Text(
                        text = hours[0].centerText,
                        maxLines = 1,
                        style = selectedTextStyle
                    )
                }
            }
        }

        LazyColumn(
            modifier = Modifier
                .height(height),
            state = lazyListState,
            contentPadding = PaddingValues(vertical = height / rowCount * ((rowCount - 1) / 2)),
            flingBehavior = rememberSnapperFlingBehavior(
                lazyListState = lazyListState
            )
        ) {
            items(count) { index ->
                val distanceFromCenter = distanceFromCenter(
                    lazyListState = lazyListState,
                    snapperLayoutInfo = snapperLayoutInfo,
                    index = index,
                    rowCount = rowCount
                )

                val text =
                    if (distanceFromCenter < 0.5) hours[index].centerText else hours[index].text

                Box(
                    modifier = Modifier
                        .height(height / rowCount)
                        .padding(start = 40.dp, end = 36.dp)
                        .alpha(max(0.4f, 1 - distanceFromCenter)),
                    contentAlignment = Alignment.CenterStart
                ) {
                    Text(
                        text = text,
                        maxLines = 1,
                        color = if (distanceFromCenter < 0.5f) Color.White else colors.textPrimary,
                        style = if (distanceFromCenter < 0.5f) selectedTextStyle else normalTextStyle
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalSnapperApi::class)
private fun calculateSnappedItemIndex(snapperLayoutInfo: SnapperLayoutInfo): Int? {
    var currentItemIndex = snapperLayoutInfo.currentItem?.index

    if (snapperLayoutInfo.currentItem?.offset != 0) {
        if (currentItemIndex != null) {
            currentItemIndex++
        }
    }
    return currentItemIndex
}

@OptIn(ExperimentalSnapperApi::class)
@Composable
private fun distanceFromCenter(
    lazyListState: LazyListState,
    snapperLayoutInfo: SnapperLayoutInfo,
    index: Int,
    rowCount: Int
): Float {

    val distanceToIndexSnap = snapperLayoutInfo.distanceToIndexSnap(index).absoluteValue
    val layoutInfo = remember { derivedStateOf { lazyListState.layoutInfo } }.value
    val viewPortHeight = layoutInfo.viewportSize.height.toFloat()
    val singleViewPortHeight = viewPortHeight / rowCount

    return min(1f, (distanceToIndexSnap / singleViewPortHeight))
}

data class Hour(
    val text: String,
    val centerText: String,
    val currentHour: Int,
    val currentMinute: Int,
    val index: Int
)
