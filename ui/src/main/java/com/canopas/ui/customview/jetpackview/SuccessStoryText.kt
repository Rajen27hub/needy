package com.canopas.ui.customview.jetpackview

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextLayoutResult
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.em
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.textColor
import com.canopas.ui.R

@Composable
fun SuccessStoryText(
    descriptionText: String,
    modifier: Modifier = Modifier
) {

    val textLayoutResultState = remember { mutableStateOf<TextLayoutResult?>(null) }
    val textLayoutResult = textLayoutResultState.value
    var showReadMore by remember {
        mutableStateOf(false)
    }

    val lineHeightSp = 16.sp
    val lineHeightDp: Dp = with(LocalDensity.current) {
        lineHeightSp.toDp()
    }

    val textHeight = lineHeightDp.times(if (showReadMore) 3 else 4)

    LaunchedEffect(key1 = textLayoutResult) {
        if (textLayoutResult == null) return@LaunchedEffect
        when {
            textLayoutResult.hasVisualOverflow -> {
                showReadMore = true
            }
        }
    }

    Text(
        text = descriptionText,
        color = textColor,
        style = AppTheme.typography.bodyTextStyle2,
        onTextLayout = { textLayoutResultState.value = it },
        lineHeight = lineHeightSp,
        maxLines = 3,
        overflow = TextOverflow.Ellipsis,
        modifier = modifier
            .padding(horizontal = 16.dp)
            .height(textHeight)
    )

    if (showReadMore) {
        Text(
            text = stringResource(R.string.habit_configure_read_more_text),
            textAlign = TextAlign.Start,
            color = ColorPrimary,
            letterSpacing = 0.01.em,
            style = AppTheme.typography.bodyTextStyle2,
            lineHeight = 17.sp,
            modifier = modifier
                .fillMaxWidth()
                .padding(start = 16.dp, top = 8.dp, bottom = 16.dp)
        )
    }
}

@Preview
@Composable
fun PreviewReadMoreTextView() {
    val descriptionText = "Yoga continues to grow in popularity as people experience its " +
        "physical and mental benefits. Developing a personal yoga practice can help prevent and reduce stress, " +
        "which is a common goal among people who want to create positive growth and focus on self-improvement." +
        "In addition to physical postures, your yoga routine can include breathing, meditation, and relaxation techniques like yoga nidra. " +
        "Continue reading to learn more about the stress-relieving benefits of yoga and how you can use your practice to enhance your well-being."
    SuccessStoryText(descriptionText)
}
