package com.canopas.ui.main

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.preferences.NoLonelyPreferences
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainActivityViewModel @Inject constructor(
    preferencesDataStore: NoLonelyPreferences,
    private val authManager: AuthManager
) : ViewModel() {
    val showUpdatedWelcomeView = MutableStateFlow(false)
    val showWakeUpTimeView = MutableStateFlow(false)
    val showKickStartView = MutableStateFlow(false)

    init {
        viewModelScope.launch {
            showUpdatedWelcomeView.value = !preferencesDataStore.isWelcomeScreenShown()
            showWakeUpTimeView.value = !preferencesDataStore.isWakeUpScreenShown() && !authManager.authState.isVerified
            showKickStartView.value = !preferencesDataStore.isKickStartScreenShown() && !authManager.authState.isVerified
        }
    }

    val showSplashView = MutableStateFlow(true)

    fun completeSplashScreen() {
        showSplashView.value = false
    }
}
