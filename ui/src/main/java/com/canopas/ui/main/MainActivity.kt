package com.canopas.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.ui.home.HomeActivity
import com.canopas.ui.navigation.NavManager
import com.canopas.ui.welcome.OnboardActivity
import com.canopas.ui.welcome.kickstart.KickStartActivity
import com.canopas.ui.welcome.wakeupactivity.WakeUpActivity
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var navManager: NavManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MainView()
        }
    }
}

@Composable
fun MainView() {
    val viewModel = hiltViewModel<MainActivityViewModel>()
    val showOnBoard by viewModel.showUpdatedWelcomeView.collectAsState()
    val showWakeUpScreen by viewModel.showWakeUpTimeView.collectAsState()
    val showKickStartScreen by viewModel.showKickStartView.collectAsState()

    val context = LocalContext.current
    when {
        showOnBoard -> {
            val navigateToOnBoard = Intent(context as MainActivity, OnboardActivity::class.java)
            context.startActivity(navigateToOnBoard)
        }
        showWakeUpScreen -> {
            val navigateToWakeUpScreen = Intent(context as MainActivity, WakeUpActivity::class.java)
            context.startActivity(navigateToWakeUpScreen)
        }
        showKickStartScreen -> {
            val navigateToKickStartScreen = Intent(context as MainActivity, KickStartActivity::class.java)
            context.startActivity(navigateToKickStartScreen)
        }
        else -> {
            val navigateToHome = Intent(context as MainActivity, HomeActivity::class.java)
            context.startActivity(navigateToHome)
        }
    }
    context.finish()
}
