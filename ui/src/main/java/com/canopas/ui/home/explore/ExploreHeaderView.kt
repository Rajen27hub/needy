package com.canopas.ui.home.explore

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.animateIntAsState
import androidx.compose.animation.core.tween
import androidx.compose.animation.slideIn
import androidx.compose.animation.slideOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterBoldFont
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.SecondaryOutlineButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.BasicSubscription
import com.canopas.ui.R

@Composable
fun ExploreHeader(headerTitle: String) {
    Text(
        text = stringResource(
            R.string.explore_hi_there_top_header_text,
            headerTitle
        ),
        style = AppTheme.typography.h2TextStyle,
        letterSpacing = -(0.96.sp),
        lineHeight = 32.sp,
        color = ComposeTheme.colors.textPrimary
    )
}

@Composable
fun NewHeaderState(viewModel: ExploreViewModel) {
    val headerState by viewModel.headerState.collectAsState()
    val showEmptyHeader by viewModel.showEmptyHeader.collectAsState()

    AnimatedVisibility(
        visible = headerState is HeaderState.EMPTY_ACTIVITY && showEmptyHeader,
        enter = slideIn { IntOffset(it.width, 0) },
        exit = slideOut { IntOffset(it.width, 0) }
    ) {
        BuildNewHeaderView(
            stringResource(R.string.explore_new_header_empty_screen_text1),
            stringResource(R.string.explore_new_header_empty_screen_text2),
            stringResource(R.string.explore_new_header_empty_screen_text3),
            stringResource(R.string.explore_new_header_empty_screen_text4),
            btnText = stringResource(R.string.explore_new_header_btn_show_me_text),
        ) { viewModel.dismissCurrentHeader() }
    }

    AnimatedVisibility(
        visible = headerState is HeaderState.FIRST_ACTIVITY,
        enter = slideIn { IntOffset(it.width, 0) },
        exit = slideOut { IntOffset(it.width, 0) }
    ) {
        BuildNewHeaderView(
            headerText1 = stringResource(R.string.explore_new_header_first_activity_text1),
            headerText2 = stringResource(R.string.explore_new_header_first_activity_text2),
            headerText3 = null,
            headerText4 = stringResource(R.string.explore_new_header_first_activity_text4),
            btnText = stringResource(R.string.explore_new_header_btn_got_it_text),
        ) { viewModel.dismissCurrentHeader() }
    }

    AnimatedVisibility(
        visible = headerState is HeaderState.FIRST_COMPLETION,
        enter = slideIn { IntOffset(it.width, 0) },
        exit = slideOut { IntOffset(it.width, 0) }
    ) {
        BuildNewHeaderView(
            headerText1 = stringResource(R.string.explore_completion_header_text1),
            headerText2 = stringResource(R.string.explore_completion_header_text2),
            headerText3 = null,
            headerText4 = stringResource(R.string.explore_completion_header_text4),
            btnText = stringResource(R.string.explore_new_header_btn_on_it_text),
        ) { viewModel.dismissCurrentHeader() }
    }
}

@Composable
fun BuildNewHeaderView(
    headerText1: String,
    headerText2: String,
    headerText3: String?,
    headerText4: String,
    btnText: String,
    onBtnClicked: () -> Unit
) {
    Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
                .padding(bottom = 16.dp)
                .background(newHeaderCardColor)
        ) {

            Text(
                text = buildAnnotatedString {
                    withStyle(
                        style = SpanStyle(
                            fontFamily = InterBoldFont
                        )
                    ) {
                        append(headerText1)
                    }
                    append(headerText2)
                    if (headerText3 != null) {
                        withStyle(
                            style = SpanStyle(fontFamily = InterBoldFont)
                        ) {
                            append(headerText3)
                        }
                    }
                    append(headerText4)
                },
                style = AppTheme.typography.subTitleTextStyle1,
                color = ComposeTheme.colors.textPrimary,
                lineHeight = 18.sp,
                modifier = Modifier.padding(
                    start = 16.dp, end = 16.dp, top = 16.dp, bottom = 24.dp
                )
            )

            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 18.dp, bottom = 18.dp)
                    .motionClickEvent { onBtnClicked() },
                contentAlignment = Alignment.CenterEnd
            ) {
                Text(
                    text = btnText,
                    style = AppTheme.typography.tabTextStyle,
                    color = ComposeTheme.colors.primary
                )
            }
        }
        Spacer(modifier = Modifier.height(8.dp))
    }
}

@Composable
fun ExploreAppBar(
    viewModel: ExploreViewModel,
    subscriptions: List<BasicSubscription>,
    showPerfectDayView: Boolean = false
) {
    val userPoints by viewModel.userPoints.collectAsState()
    val user by viewModel.user.collectAsState()
    val userProfile = user?.profileImageUrl ?: ""
    val userName = user?.firstName ?: ""

    val headerTitle = userName.ifEmpty { stringResource(R.string.explore_empty_name_header_text) }
    val headerState by viewModel.headerState.collectAsState()

    val pointsCounter by animateIntAsState(
        targetValue = userPoints,
        animationSpec = tween(
            durationMillis = 1000, easing = LinearEasing
        )
    )

    Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Row(
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .padding(top = 8.dp)
                    .padding(horizontal = 16.dp, vertical = 12.dp)
                    .fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {

                if (headerState != HeaderState.TODO || subscriptions.isEmpty() || showPerfectDayView) {
                    ExploreHeader(headerTitle)
                } else {
                    TodoHeader(subs = subscriptions)
                }

                Spacer(modifier = Modifier.weight(1f))

                SecondaryOutlineButton(
                    onClick = {
                        viewModel.navigateToLeaderBoardView()
                    },
                    modifier = Modifier
                        .wrapContentSize(),
                    text = pointsCounter.toString(),
                    textModifier = Modifier,
                    shape = RoundedCornerShape(24.dp),
                    colors = ButtonDefaults.outlinedButtonColors(contentColor = White, backgroundColor = ComposeTheme.colors.primary),
                    icon = painterResource(id = R.drawable.ic_point_star),
                    iconModifier = Modifier.padding(end = 4.dp).size(18.dp)
                )

                Spacer(modifier = Modifier.width(12.dp))

                if (userProfile.isNotEmpty()) {
                    ProfileImageView(
                        data = userProfile,
                        modifier = Modifier
                            .size(36.dp)
                            .motionClickEvent {
                                viewModel.navigateToSettingScreen()
                            }
                            .border(
                                1.dp,
                                if (ComposeTheme.isDarkMode) darkProfileBorder else lightProfileBorder,
                                CircleShape
                            ),
                        char = ""
                    )
                } else {
                    Row(
                        modifier = Modifier
                            .wrapContentSize()
                            .motionClickEvent {
                                viewModel.navigateToSettingScreen()
                            }
                            .background(colorUserAccountBgLight, RoundedCornerShape(18.dp)),
                    ) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_user_2),
                            contentDescription = "",
                            modifier = Modifier
                                .padding(10.dp)
                                .size(16.dp)
                        )
                    }
                }
            }
            Spacer(modifier = Modifier.height(4.dp))
            Divider(
                Modifier.fillMaxWidth(),
                thickness = 1.dp,
                color = ComposeTheme.colors.textPrimary.copy(alpha = 0.08f)
            )
        }
    }
}

private val colorUserAccountBgLight = Color(0xFFD4BCAE)
private val newHeaderCardColor = Color(0x14F67C37)
private val lightProfileBorder = Color(0xFFD9D9DA)
private val darkProfileBorder = Color(0xFF888888)
