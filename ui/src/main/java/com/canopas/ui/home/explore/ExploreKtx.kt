package com.canopas.ui.home.explore

import com.canopas.base.ui.Utils
import com.canopas.base.ui.Utils.Companion.parseOrNull
import com.canopas.data.model.BasicSubscription
import com.canopas.data.model.CompletionBody
import com.canopas.data.model.GOAL_WALL_VISIBILITY_NEVER
import com.canopas.data.model.GOAL_WALL_VISIBILITY_WHEN_ACTIVE
import com.canopas.data.model.Goal
import com.canopas.data.model.GoalWithColors
import com.canopas.data.model.TYPE_END_GOAL
import com.canopas.data.model.TYPE_SUB_GOAL
import java.util.Calendar
import java.util.TimeZone
import java.util.concurrent.TimeUnit

fun Int.mapCompletionData(): CompletionBody {
    val timeMillis = System.currentTimeMillis()
    val completionTime: Long = TimeUnit.MILLISECONDS.toSeconds(timeMillis)

    return CompletionBody(
        subscription_id = this,
        completion_time = completionTime,
        timeZone = TimeZone.getDefault().id
    )
}

fun List<GoalWithColors>.sort(): Pair<List<GoalWithColors>, List<GoalWithColors>> {
    val firstList = mutableListOf<GoalWithColors>()
    val secondList = mutableListOf<GoalWithColors>()
    val middleIndex = if (this.size % 2 != 0) {
        (this.size / 2) + 1
    } else {
        (this.size / 2)
    }
    firstList.addAll(this.subList(0, middleIndex))
    secondList.addAll(this.subList(middleIndex, this.size))
    return Pair(firstList, secondList)
}

fun MutableList<BasicSubscription>.mapGoalsWithColor(goals: List<Goal>): List<GoalWithColors> {
    val userSubscriptions = this.sortedWith(
        compareBy(
            { timeStringToCurrentMillis(it.activity_time)?.div(1000) ?: Long.MAX_VALUE },
            { it.activity_duration ?: Int.MAX_VALUE }
        )
    )
    val calendar = Calendar.getInstance()
    calendar.set(Calendar.HOUR_OF_DAY, 0)
    calendar.set(Calendar.MINUTE, -1)
    val groupedGoals = goals.filterNot {
        (
            Utils.format.parseOrNull(it.dueDate)?.time
                ?: calendar.timeInMillis
            ) < calendar.timeInMillis && it.wallVisibility == GOAL_WALL_VISIBILITY_WHEN_ACTIVE
    }.filter { goal ->
        goal.wallVisibility != GOAL_WALL_VISIBILITY_NEVER
    }.groupBy { it.activityName }

    val newGoalsList = mutableListOf<GoalWithColors>()

    for (basicSubscription in userSubscriptions) {
        val goalsForSubscription = groupedGoals[basicSubscription.activity.name] ?: emptyList()

        val (overdueGoals, activeGoals) = goalsForSubscription.partition { goal ->
            (Utils.format.parseOrNull(goal.dueDate)?.time ?: Long.MAX_VALUE) < calendar.timeInMillis
        }

        val sortedActiveGoals = activeGoals.sortedWith(
            compareBy<Goal> {
                if (it.dueDate.isEmpty() || it.type == TYPE_END_GOAL) 1 else 0
            }.thenBy {
                Utils.format.parseOrNull(it.dueDate)?.time ?: calendar.timeInMillis
            }
        )

        val sortedOverdueGoals = overdueGoals.filter { it.type == TYPE_SUB_GOAL }.sortedBy {
            Utils.format.parseOrNull(it.dueDate)?.time ?: Long.MAX_VALUE
        }

        for (goal in sortedActiveGoals) {
            val goalWithColors = GoalWithColors(
                goal = goal,
                activityName = basicSubscription.activity.name,
                subscriptionId = basicSubscription.id,
                color2 = basicSubscription.activity.color2,
                color = basicSubscription.activity.color
            )
            newGoalsList.add(goalWithColors)
        }

        for (goal in sortedOverdueGoals) {
            val isCompleted = (
                Utils.format.parseOrNull(goal.dueDate)?.time
                    ?: calendar.timeInMillis
                ) <= calendar.timeInMillis
            val goalWithColors = GoalWithColors(
                goal = goal,
                activityName = basicSubscription.activity.name,
                subscriptionId = basicSubscription.id,
                color2 = basicSubscription.activity.color2,
                color = basicSubscription.activity.color,
                isCompleted = isCompleted
            )
            newGoalsList.add(goalWithColors)
        }
    }

    return newGoalsList
}
