package com.canopas.ui.home.explore

import android.graphics.Color.parseColor
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.animateFloat
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.keyframes
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.geometry.Rect
import androidx.compose.ui.geometry.Size
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Outline
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.Density
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.ColorUtils.blendARGB
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.canopas.base.data.utils.Utils.Companion.encodeToBase64
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterMediumFont
import com.canopas.base.ui.White
import com.canopas.base.ui.dpToSp
import com.canopas.base.ui.parse
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.BasicSubscription
import com.canopas.data.model.GoalWithColors
import com.canopas.ui.R
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import kotlin.math.roundToInt

@Suppress("FunctionName")
fun ToDoCardView(
    viewModel: ExploreViewModel,
    subs: List<BasicSubscription>,
    goals: List<GoalWithColors>? = null,
    headerState: HeaderState,
    showNotesTips: Boolean,
    lazyListScope: LazyListScope,
    showPerfectDayView: Boolean = false,
) {
    val listInOrder = subs.sortedWith { sub1, sub2 ->
        val millis1 = timeStringToCurrentMillis(sub1.activity_time)?.div(1000) ?: 0
        val millis2 = timeStringToCurrentMillis(sub2.activity_time)?.div(1000) ?: 0
        val duration1 = sub1.activity_duration ?: 0
        val duration2 = sub2.activity_duration ?: 0

        if (millis1 == 0L) 1
        else if (millis2 == 0L) -1
        else if ((millis1 - millis2).toInt() != 0) (millis1 - millis2).toInt()
        else (duration1 - duration2)
    }

    if (showPerfectDayView || headerState is HeaderState.FIRST_ACTIVITY || headerState is HeaderState.FIRST_COMPLETION) {
        if (subs.isNotEmpty()) {
            lazyListScope.item {
                Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Center) {
                    Box(
                        modifier = Modifier
                            .widthIn(max = 600.dp)
                            .fillMaxWidth()
                            .padding(horizontal = 16.dp)
                            .padding(bottom = 16.dp)
                    ) {
                        TodoHeader(subs)
                    }
                }
            }
        }
    }

    lazyListScope.itemsIndexed(items = listInOrder, key = { _, sub -> sub.id }) { index, sub ->
        val subscriptionGoal = if (goals?.isNotEmpty() == true) {
            goals.drop(1).find { it.subscriptionId == sub.id && it.goal.occurrences != it.goal.completions }
        } else null
        ToDoCardItem(
            viewModel,
            subscription = sub,
            nextSubscription = listInOrder.getOrNull(index + 1),
            { viewModel.completeHabit(sub) },
            {
                if (listInOrder.size > index) {
                    viewModel.onToDoCardClicked(sub.id)
                }
            },
            {
                viewModel.navigateToNotesView(
                    encodeToBase64(sub.activity.name),
                    sub.activity.id.toString(),
                    sub.note?.id,
                    sub.is_custom
                )
            },
            index = index,
            showNotesTips,
            headerState = headerState,
            subscriptionGoal = subscriptionGoal ?: if (!goals.isNullOrEmpty()) goals.firstOrNull { it.subscriptionId == sub.id } else null,
            previousSubscription = listInOrder.getOrNull(index - 1),
        )
    }
}

@Composable
fun TodoHeader(
    subs: List<BasicSubscription>,
) {
    val completedSubscriptions = subs.filter { it.activity.is_completed }.size
    val totalSubscriptions = subs.size

    Row(
        modifier = Modifier,
        horizontalArrangement = Arrangement.Start,
        verticalAlignment = CenterVertically
    ) {
        Text(
            text = buildAnnotatedString {
                append(stringResource(R.string.explore_screen_today_header_text))
                withStyle(
                    style = SpanStyle(
                        fontFamily = InterMediumFont,
                        color = colors.textSecondary,
                        fontSize = 14.sp
                    )
                ) {
                    append(" $completedSubscriptions/$totalSubscriptions")
                }
            },
            style = AppTheme.typography.h2TextStyle,
            letterSpacing = -(0.96.sp),
            lineHeight = 32.sp,
            color = colors.textPrimary,
        )
    }
}

@Composable
fun TodoGoalCardView(
    subscriptionGoal: GoalWithColors,
    subscription: BasicSubscription,
    activityTime: String?
) {
    val cardColor = if (subscriptionGoal.color2 != null) subscriptionGoal.color2!! else subscriptionGoal.color
    val calendar = Calendar.getInstance()
    if (activityTime != null) {
        val time = (activityTime).split(":").map { it.toInt() }
        calendar.timeInMillis = System.currentTimeMillis()
        calendar.set(Calendar.HOUR_OF_DAY, time[0])
        calendar.set(Calendar.MINUTE, time[1])
        calendar.add(Calendar.MINUTE, subscription.activity_duration ?: 1)
        if (calendar.timeInMillis > System.currentTimeMillis()) {
            Row(
                modifier = Modifier
                    .background(
                        if (isDarkMode) Color
                            .parse(cardColor)
                            .copy(0.3f) else Color
                            .parse(cardColor)
                            .copy(0.3f),
                        shape = RoundedCornerShape(20.dp)
                    )
                    .clip(RoundedCornerShape(20.dp)),
                verticalAlignment = CenterVertically,
                horizontalArrangement = Arrangement.Start
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_goals_icon),
                    contentDescription = "",
                    modifier = Modifier
                        .padding(start = 16.dp, top = 12.dp, bottom = 12.dp)
                        .size(15.dp),
                    tint = if (isDarkMode) White else colors.textSecondary,
                )
                Spacer(modifier = Modifier.width(8.dp))
                Text(
                    text = subscriptionGoal.goal.title ?: "",
                    style = AppTheme.typography.goalTodoTextStyle,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                    color = if (isDarkMode) White else colors.textSecondary,
                    modifier = Modifier.padding(top = 8.dp, bottom = 8.dp, end = 16.dp),
                )
            }
        }
    }
}

data class DottedShape(
    val step: Dp,
) : Shape {
    override fun createOutline(
        size: Size,
        layoutDirection: LayoutDirection,
        density: Density
    ) = Outline.Generic(
        Path().apply {
            val stepPx = with(density) { step.toPx() }
            val stepsCount = (size.height / stepPx).roundToInt()
            val actualStep = size.height / stepsCount
            val dotSize = Size(width = size.width, height = actualStep / 2)
            for (i in 0 until stepsCount) {
                addRect(
                    Rect(
                        offset = Offset(x = 0f, y = i * actualStep),
                        size = dotSize
                    )
                )
            }
            close()
        }
    )
}

@Composable
fun AnimatedWithIconAndImage(subscription: BasicSubscription, timeProgress: Float) {
    val infiniteTransition1 = rememberInfiniteTransition()
    val infiniteTransition2 = rememberInfiniteTransition()

    val fgColorInt = parseColor(subscription.activity.color2).let {
        blendARGB(it, android.graphics.Color.BLACK, 0.4f)
    }
    val fgColor =
        if (timeProgress == 0f && isDarkMode) Color.parse(subscription.activity.color2)
        else Color(fgColorInt)

    val textAlpha by infiniteTransition1.animateFloat(
        initialValue = 1f,
        targetValue = 1F,
        animationSpec = infiniteRepeatable(
            animation = keyframes {
                durationMillis = 6000
                1.0f at 0 with FastOutSlowInEasing // for 0-15 ms
                1.0f at 2500 with FastOutSlowInEasing // for 15-75 ms
                0.0f at 3000 with FastOutSlowInEasing // for 0-15 ms
                0.0f at 5500 with FastOutSlowInEasing
                1.0f at 6000 with FastOutSlowInEasing
            },
            repeatMode = RepeatMode.Restart
        )
    )

    val imageAlpha by infiniteTransition2.animateFloat(
        initialValue = 0f,
        targetValue = 0F,
        animationSpec = infiniteRepeatable(
            animation = keyframes {
                durationMillis = 6000
                0.0f at 0 with FastOutSlowInEasing // for 0-15 ms
                0.0f at 3000 with FastOutSlowInEasing // for 15-75 ms
                1.0f at 3500 with FastOutSlowInEasing // for 0-15 ms
                1.0f at 5000 with FastOutSlowInEasing
                0.0f at 5500 with FastOutSlowInEasing
            },
            repeatMode = RepeatMode.Restart
        )
    )

    val time = timeStringToCurrentMillis(subscription.activity_time)?.let {
        TIME_FORMAT.format(it)
    }

    Text(
        modifier = Modifier
            .padding(vertical = 8.dp)
            .alpha(textAlpha),
        text = time ?: stringResource(id = R.string.explore_duration_any_text),
        fontSize = dpToSp(dp = 20.dp),
        style = AppTheme.typography.todoActivityTimeTextStyle,
        color = fgColor,
        letterSpacing = -(0.8.sp),
        textAlign = TextAlign.Center
    )

    val painterResource = if (subscription.is_custom) {
        painterResource(id = R.drawable.ic_custom_activity)
    } else {
        rememberAsyncImagePainter(
            ImageRequest.Builder(LocalContext.current).data(
                data = subscription.activity.image_url
            ).build()
        )
    }

    Icon(
        painter = painterResource,
        tint = fgColor,
        modifier = Modifier
            .size(32.dp)
            .alpha(imageAlpha),
        contentDescription = ""
    )
}

@Composable
fun NotesTipsView(viewModel: ExploreViewModel) {
    Box(
        modifier = Modifier.fillMaxWidth(), contentAlignment = Center
    ) {
        Row(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .padding(bottom = 12.dp, start = 16.dp, end = 16.dp)
                .fillMaxWidth()
                .wrapContentHeight()
                .clip(shape = RoundedCornerShape(12.dp))
                .background(
                    color = NotesCardBg.copy(0.05f), shape = RoundedCornerShape(12.dp)
                ),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            Text(
                text = stringResource(R.string.notes_tips_view_description_text),
                style = AppTheme.typography.bodyTextStyle3,
                color = colors.textSecondary,
                lineHeight = 19.sp,
                modifier = Modifier
                    .weight(0.9f)
                    .padding(start = 16.dp, top = 16.dp, bottom = 16.dp)
            )
            IconButton(modifier = Modifier.padding(top = 4.dp), onClick = {
                viewModel.closeNotesTipsView()
            }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_cancel_icon),
                    contentDescription = "",
                    modifier = Modifier.weight(0.1f)
                )
            }
        }
    }
}

private val TIME_FORMAT = SimpleDateFormat("HH\nmm", Locale.US)
private val CALENDAR: Calendar = Calendar.getInstance()

fun timeStringToCurrentMillis(time: String?): Long? {
    val times = time?.trim()?.split(":") ?: return null

    if (times.size != 2) return null

    CALENDAR.set(Calendar.HOUR_OF_DAY, times[0].toInt())
    CALENDAR.set(Calendar.MINUTE, times[1].toInt())
    return CALENDAR.timeInMillis
}

private val NotesCardBg = Color(0xFF272727)
