package com.canopas.ui.home.settings.userprofile

import android.content.res.Resources
import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.UserAccount
import com.canopas.base.data.rest.AuthServices
import com.canopas.base.data.utils.Utils
import com.canopas.base.ui.Event
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.R
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.io.File
import javax.inject.Inject

const val MINIMUM_CHARACTER_LENGTH = 2

@HiltViewModel
class UserProfileViewModel
@Inject constructor(
    private val noLonelyService: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    private val eventBus: EventBus,
    private val authManager: AuthManager,
    private val authServices: AuthServices,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    var userAccount: UserAccount? = null
    var updatedUserAccount: UserAccount? = null

    val userState = MutableStateFlow<UserState>(UserState.LOADING)
    val openProfileImageChooserEvent = MutableStateFlow(Event(false))
    var pendingUpdatedProfileImage = MutableStateFlow<File?>(null)
    val openDeleteDialog = MutableStateFlow(false)
    val enableSaveButton = MutableStateFlow(false)
    val shouldShowBackArrow = MutableStateFlow(true)
    val showFirstNameErrorState = MutableStateFlow(false)
    val showLastNameErrorState = MutableStateFlow(false)
    val showUserNameErrorState = MutableStateFlow(false)

    private var promptPremium: Boolean = false

    init {
        userAccount = authManager.currentUser
        updatedUserAccount = authManager.currentUser
        userAccount?.let {
            userState.tryEmit(UserState.SUCCESS(it))
            updateUserState()
            fetchUserData(it.id)
        }
    }

    fun onStart(showBackArrow: Boolean, promptPremium: Boolean) {
        appAnalytics.logEvent("view_update_profile", null)
        shouldShowBackArrow.value = showBackArrow
        this.promptPremium = promptPremium
    }

    private fun fetchUserData(id: Int) {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                noLonelyService.getUserDetailById(id).onSuccess { userData ->
                    if (userData != userAccount) {
                        userAccount = userData
                        updatedUserAccount = userAccount
                        authManager.currentUser = userData
                        userState.value = UserState.SUCCESS(userData)
                    }
                }.onFailure { e ->
                    Timber.e(e)
                    userState.value = UserState.ERROR(e.toUserError(resources))
                }
            }
        }
    }

    fun deleteUser() = viewModelScope.launch {
        appAnalytics.logEvent("tap_update_profile_delete", null)
        userState.value = UserState.LOADING
        withContext(appDispatcher.IO) {
            val userId = userAccount?.id ?: return@withContext
            authServices.deleteUser(userId).onSuccess {
                authManager.resetSession()
                withContext(appDispatcher.MAIN) { navManager.popBack() }
            }.onFailure { e ->
                Timber.e(e)
                userState.value = UserState.ERROR(e.toUserError(resources))
            }
        }
    }

    fun openDialog() {
        openDeleteDialog.tryEmit(true)
    }

    fun showErrorForImage(error: Exception?) {
        val user = userAccount ?: return
        userState.tryEmit(UserState.ERROR(resources.getString(R.string.user_profile_error_cropping_image_text)))
        userState.tryEmit(UserState.SUCCESS(user))
        Timber.e(error.toString())
    }

    fun closeDialog() {
        openDeleteDialog.tryEmit(false)
    }

    fun openProfileImageChanger() {
        openProfileImageChooserEvent.value = Event(true)
    }

    fun uploadProfileImage(image: File) {
        pendingUpdatedProfileImage.tryEmit(image)
        updatedUserAccount?.profileImageUrl = null
        enableSaveButton.tryEmit(true)
        updateUserState()
    }

    fun onSaveBtnClick() {
        appAnalytics.logEvent("tap_update_profile_save", null)
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                userState.value = UserState.LOADING

                pendingUpdatedProfileImage.value?.let { profileImage ->
                    val multipart: MultipartBody.Part = MultipartBody.Part.createFormData(
                        "file", profileImage.name,
                        profileImage
                            .asRequestBody("*/*".toMediaTypeOrNull())
                    )
                    noLonelyService.uploadProfile(multipart).onSuccess { profileUrl ->
                        updatedUserAccount?.profileImageUrl = profileUrl.image_url
                    }.onFailure { e ->
                        appAnalytics.logEvent(
                            "error_update_profile_save",
                            Bundle().apply {
                                putString("status_code", "profile_upload_failed_${e.toUserError(resources)}")
                            }
                        )
                        Timber.e(e)
                        userState.value = UserState.ERROR(e.toUserError(resources))
                        return@withContext
                    }
                }
                updateUser()
            }
        }
    }

    private suspend fun updateUser() {
        val user = updatedUserAccount ?: return

        noLonelyService.updateUserById(user.id, user)
            .onSuccess { userData ->
                authManager.currentUser = userData
                eventBus.post(userData)
                onUpdateUserSuccess()
            }.onFailure { e ->
                appAnalytics.logEvent(
                    "error_update_profile_save",
                    Bundle().apply {
                        putString("status_code", "user_update_failed_${e.toUserError(resources)}")
                    }
                )

                Timber.e(e)
                userState.value = UserState.ERROR(e.toUserError(resources))
            }
    }

    private suspend fun onUpdateUserSuccess() = withContext(appDispatcher.MAIN) {
        navManager.popBack()
        if (promptPremium) {
            navManager.navigateToBuyPremiumScreen(
                showBackArrow = false,
                showDismissButton = true
            )
        }
    }

    fun onFirstNameChanged(newValue: String) {
        updatedUserAccount?.firstName = newValue
        updateUserState()
        when {
            newValue.length < MINIMUM_CHARACTER_LENGTH -> {
                showFirstNameErrorState.tryEmit(true)
                enableSaveButton.value = false
            }
            newValue.length >= MINIMUM_CHARACTER_LENGTH && userAccount != updatedUserAccount -> {
                enableSaveButton.value = true
                showFirstNameErrorState.tryEmit(false)
            }
            else -> {
                showFirstNameErrorState.tryEmit(false)
                enableSaveButton.value = false
            }
        }
    }

    fun checkLengthValidation(newValue: String) {
        enableSaveButton.tryEmit(newValue.length >= MINIMUM_CHARACTER_LENGTH && userAccount != updatedUserAccount)
    }

    fun onLastNameChanged(newValue: String) {
        updatedUserAccount?.lastName = newValue
        updateUserState()
        when {
            newValue.length < MINIMUM_CHARACTER_LENGTH -> {
                showLastNameErrorState.tryEmit(true)
                enableSaveButton.value = false
            }
            newValue.length >= MINIMUM_CHARACTER_LENGTH && userAccount != updatedUserAccount -> {
                enableSaveButton.value = true
                showLastNameErrorState.tryEmit(false)
            }
            else -> {
                showLastNameErrorState.tryEmit(false)
                enableSaveButton.value = false
            }
        }
    }

    fun onUserNameChanged(newValue: String) {
        updatedUserAccount?.userName = newValue
        updateUserState()
        when {
            newValue.length < MINIMUM_CHARACTER_LENGTH -> {
                showUserNameErrorState.tryEmit(true)
                enableSaveButton.value = false
            }
            newValue.length >= MINIMUM_CHARACTER_LENGTH && userAccount != updatedUserAccount -> {
                enableSaveButton.value = true
                showUserNameErrorState.tryEmit(false)
            }
            else -> {
                showUserNameErrorState.tryEmit(false)
                enableSaveButton.value = false
            }
        }
    }

    fun onEmailChanged(newValue: String) {
        updatedUserAccount?.email = newValue
        updateUserState()
        enableSaveButton.tryEmit(Utils.isValidEmail(newValue) && userAccount != updatedUserAccount)
    }

    fun onPhoneChanged(newValue: String) {
        updatedUserAccount?.phone = newValue
        updateUserState()
        checkLengthValidation(newValue)
    }

    fun onGenderChanged(gender: Int) {
        updatedUserAccount?.gender = gender
        updateUserState()
        checkUpdatingValidation()
    }

    fun checkUpdatingValidation() {
        enableSaveButton.tryEmit(userAccount != updatedUserAccount)
    }

    fun removeProfile() {
        updatedUserAccount?.profileImageUrl = ""
        pendingUpdatedProfileImage.tryEmit(null)
        updateUserState()
        checkUpdatingValidation()
    }

    private fun updateUserState() {
        val user = updatedUserAccount ?: return
        userState.tryEmit(UserState.SUCCESS(user))
    }

    fun managePopBack() {
        navManager.popBack()
    }
}

sealed class UserState {
    object LOADING : UserState()
    data class SUCCESS(val userData: UserAccount) : UserState() {
        override fun equals(other: Any?): Boolean {
            return userData == other
        }

        override fun hashCode(): Int {
            return userData.hashCode()
        }
    }

    data class ERROR(val message: String) : UserState()
}
