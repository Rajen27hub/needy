package com.canopas.ui.home.explore.activitylist

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.ActivityData
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ActivityListViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<ActivityState>(ActivityState.LOADING)
    val categoryName = MutableStateFlow("")

    fun fetchActivities(categoryId: Int) = viewModelScope.launch {
        if (state.value !is ActivityState.SUCCESS) {
            state.value = ActivityState.LOADING
        }
        withContext(appDispatcher.IO) {
            service.getFeeds().onSuccess { feeds ->
                val categories = feeds.categories.filter {
                    it.id == categoryId
                }.elementAt(0)
                categoryName.value = categories.name
                state.value = ActivityState.SUCCESS(categories.activities)
            }.onFailure { e ->
                Timber.e(e.localizedMessage)
                state.emit(ActivityState.FAILURE(e.toUserError(resources), e is APIError.NetworkError))
            }
        }
    }

    fun onActivitySelected(activity: ActivityData) {
        appAnalytics.logEvent("tap_explore_activity", null)
        navManager.navigateToHabitConfigure(activity.id)
    }

    fun popBack() {
        navManager.popBack()
    }

    fun navigateToAddActivity() {
        appAnalytics.logEvent("tap_activity_list_own_activity", null)
        navManager.navigateToCustomActivityView()
    }
}

sealed class ActivityState {
    object LOADING : ActivityState()
    data class SUCCESS(
        val activities: List<ActivityData>,
    ) : ActivityState()

    data class FAILURE(val message: String, val isNetworkError: Boolean) : ActivityState()
}
