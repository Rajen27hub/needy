package com.canopas.ui.home.settings.privacy

import androidx.lifecycle.ViewModel
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class PrivacyViewModel @Inject constructor(
    private val navManager: NavManager
) : ViewModel() {

    val storingSectionExpanded = MutableStateFlow(false)
    val researchSectionExpanded = MutableStateFlow(false)
    val analyticsSectionExpanded = MutableStateFlow(false)
    val requiredFieldSectionExpanded = MutableStateFlow(false)
    val optionalFieldSectionExpanded = MutableStateFlow(false)
    val showRequiredDataPrompt = MutableStateFlow(false)
    val showOptionalDataPrompt = MutableStateFlow(false)
    val openNotificationDialog = MutableStateFlow(false)

    fun manageStoringClick() {
        storingSectionExpanded.value = !storingSectionExpanded.value
    }

    fun manageResearchClick() {
        researchSectionExpanded.value = !researchSectionExpanded.value
    }

    fun manageAnalyticsClick() {
        analyticsSectionExpanded.value = !analyticsSectionExpanded.value
    }

    fun manageRequiredFieldClick() {
        requiredFieldSectionExpanded.value = !requiredFieldSectionExpanded.value
    }

    fun manageOptionalFieldClick() {
        optionalFieldSectionExpanded.value = !optionalFieldSectionExpanded.value
    }

    fun openNotificationDialog(openRequiredDataDialog: Boolean) {
        if (openRequiredDataDialog) {
            showRequiredDataPrompt.tryEmit(true)
        } else {
            showOptionalDataPrompt.tryEmit(true)
        }
        openNotificationDialog.tryEmit(true)
    }

    fun closeNotificationDialog() {
        showRequiredDataPrompt.tryEmit(false)
        showOptionalDataPrompt.tryEmit(false)
        openNotificationDialog.tryEmit(false)
    }

    fun managePopBack() {
        navManager.popBack()
    }
}
