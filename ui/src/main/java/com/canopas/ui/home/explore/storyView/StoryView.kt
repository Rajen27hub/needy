package com.canopas.ui.home.explore.storyView

import android.graphics.drawable.BitmapDrawable
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.graphics.ColorUtils
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.palette.graphics.Palette
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.rememberForeverLazyListState
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.Details
import com.canopas.data.model.Story
import com.canopas.data.model.TYPE_STORY_DETAIL_ACTIVITY
import com.canopas.data.model.TYPE_STORY_DETAIL_QUOTE
import com.canopas.data.model.TYPE_STORY_DETAIL_TEXT
import com.canopas.ui.R

@Composable
fun StoryView(
    storyId: String
) {
    val viewModel = hiltViewModel<StoryViewModel>()
    val state by viewModel.state.collectAsState()
    val activityState by viewModel.activityState.collectAsState()
    val context = LocalContext.current
    val lazyListState = rememberForeverLazyListState(storyId)

    LaunchedEffect(key1 = Unit) {
        viewModel.onStart(storyId)
    }
    LaunchedEffect(key1 = activityState) {
        activityState.let {
            if (activityState is ActivityState.FAILURE) {
                val message = (activityState as ActivityState.FAILURE).message
                showBanner(context, message)
                viewModel.resetActivityState()
            }
        }
    }

    Scaffold {
        state.let { storyState ->
            when (storyState) {
                is StoryState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is StoryState.FAILURE -> {
                    if (storyState.isNetworkError) {
                        NetworkErrorView {
                            viewModel.onStart(storyId)
                        }
                    } else {
                        ServerErrorView {
                            viewModel.onStart(storyId)
                        }
                    }
                }
                is StoryState.SUCCESS -> {
                    StoryListView(viewModel, storyState.story, lazyListState)
                }
            }
        }
    }
}

@Composable
fun StoryListView(viewModel: StoryViewModel, storyItem: Story, lazyListState: LazyListState) {
    LazyColumn(
        state = lazyListState,
        contentPadding = PaddingValues(bottom = 30.dp),
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        item { StoryAboutView(viewModel, storyItem) }

        this.itemsIndexed(storyItem.details, key = null) { _, item ->
            when (item.type) {
                TYPE_STORY_DETAIL_TEXT -> {
                    StoryDetailView(item)
                }
                TYPE_STORY_DETAIL_ACTIVITY -> {
                    StoryActivityCard(viewModel, item)
                }
                TYPE_STORY_DETAIL_QUOTE -> {
                    StoryQuoteCard(item)
                }
            }
        }

        item { CustomActivityCard(viewModel = viewModel, storyItem.id) }
    }
}

@Composable
fun StoryQuoteCard(item: Details) {
    Box(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = item.quote,
            style = AppTheme.typography.storyViewQuotesTextStyle,
            color = colors.textPrimary.copy(if (isDarkMode) 0.70f else 0.50f),
            modifier = Modifier.padding(top = 40.dp, start = 28.dp, end = 28.dp)
        )
    }
}

@Composable
fun StoryAboutView(viewModel: StoryViewModel, storyItem: Story) {

    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxSize()
    ) {
        val painter = rememberAsyncImagePainter(
            ImageRequest.Builder(LocalContext.current).data(data = storyItem.image_url)
                .allowHardware(false).build()
        )
        when (val painterState = painter.state) {
            is AsyncImagePainter.State.Loading -> {
                ImageHeaderView(
                    viewModel,
                    colors.textPrimary,
                    colors.textSecondary,
                    title = storyItem.title,
                    subtitle = storyItem.subtitle,
                    painter = painter,
                    background = Brush.verticalGradient(
                        colors = listOf(
                            Color.Transparent,
                            Color.Transparent
                        )
                    )
                )
            }
            is AsyncImagePainter.State.Success -> {
                val palette = remember(painterState.result) {
                    Palette.from(
                        (painterState.result.drawable as BitmapDrawable).bitmap
                    ).generate()
                }
                val dominantColor = remember(palette) { palette.dominantSwatch }
                val titleTextColor = remember(dominantColor) {
                    if (dominantColor == null) {
                        darkItemTextColor
                    } else {
                        if (ColorUtils.calculateLuminance(dominantColor.rgb) <= 0.5f) {
                            lightItemTextColor
                        } else {
                            darkItemTextColor
                        }
                    }
                }

                val subTitleTextColor =
                    remember(titleTextColor) { titleTextColor.copy(alpha = 0.80f) }

                ImageHeaderView(
                    viewModel,
                    titleTextColor,
                    subTitleTextColor,
                    title = storyItem.title,
                    subtitle = storyItem.subtitle,
                    painter = painter,
                    background = Brush.verticalGradient(
                        colors = listOf(
                            if (dominantColor == null) Color.Transparent else Color.Transparent,
                            Color(
                                dominantColor!!.rgb
                            )
                        )
                    )
                )
            }
            else -> {}
        }
        Text(
            text = stringResource(R.string.story_view_header_about_text),
            style = AppTheme.typography.h2TextStyle,
            color = colors.textPrimary,
            letterSpacing = -(0.8.sp),
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 24.dp, start = 20.dp, bottom = 16.dp)
        )
        Text(
            text = storyItem.description,
            style = AppTheme.typography.subTextStyle1,
            lineHeight = 24.sp,
            color = colors.textSecondary,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 20.dp, end = 20.dp)
        )
    }
}

@Composable
fun ImageHeaderView(
    viewModel: StoryViewModel,
    titleColor: Color,
    subtitleColor: Color,
    subtitle: String,
    title: String,
    painter: AsyncImagePainter,
    background: Brush
) {
    Box(
        modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.TopCenter
    ) {
        Image(
            painter = painter,
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(1f),
            contentDescription = null,
            contentScale = ContentScale.Crop
        )
        Row(
            Modifier
                .fillMaxWidth()
                .align(Alignment.TopStart)
        ) {
            IconButton(onClick = { viewModel.popBack() }) {
                Icon(Icons.Default.ArrowBack, contentDescription = null, tint = titleColor)
            }
        }

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .align(Alignment.BottomStart)
                .background(
                    brush = background
                )
                .padding(20.dp)
        ) {
            Text(
                text = title,
                style = AppTheme.typography.h2TextStyle,
                lineHeight = 28.sp,
                letterSpacing = -(0.96.sp),
                color = titleColor,
                modifier = Modifier.padding(top = 8.dp)
            )
            Text(
                text = subtitle,
                style = AppTheme.typography.bodyTextStyle1,
                lineHeight = 18.sp,
                letterSpacing = -(0.64.sp),
                color = subtitleColor
            )
        }
    }
}

@Composable
fun StoryDetailView(storyDetail: Details) {
    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .padding(horizontal = 20.dp)
    ) {
        Text(
            text = storyDetail.label,
            style = AppTheme.typography.h2TextStyle,
            color = colors.textPrimary,
            letterSpacing = -(0.96.sp),
            modifier = Modifier.padding(top = 40.dp, bottom = 16.dp)
        )
        Text(
            text = storyDetail.description,
            style = AppTheme.typography.subTextStyle1,
            lineHeight = 24.sp,
            color = colors.textSecondary,
        )
    }
}

private val lightItemTextColor = Color(0xFFFFFFFF)
private val darkItemTextColor = Color(0xFF000000)
