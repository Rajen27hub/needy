package com.canopas.ui.home.explore

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.auth.AuthStateChangeListener
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.UserAccount
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.utils.Utils.Companion.encodeToBase64
import com.canopas.base.ui.Event
import com.canopas.data.event.ActivitiesTabSelectedEvent
import com.canopas.data.event.ActivityCompletionEvent
import com.canopas.data.model.BasicSubscription
import com.canopas.data.model.FeaturedFeed
import com.canopas.data.model.Goal
import com.canopas.data.model.MOTIVATIONAL_TYPE_PODCASTS
import com.canopas.data.model.Story
import com.canopas.data.rest.NoLonelyService
import com.canopas.feature_points_data.repository.PointsRepository
import com.canopas.ui.home.settings.buypremiumsubscription.DEVICE_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import com.canopas.ui.utils.DateUtils.toStartOfDay
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.util.Date
import javax.inject.Inject
import kotlin.collections.ArrayList

const val DEFAULT_USER_POINTS = 11
const val DEFAULT_WAKE_UP_ACTIVITY_ID = 38
const val DEFAULT_SLEEP_ACTIVITY_ID = 28

@HiltViewModel
class ExploreViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    private val eventBus: EventBus,
    private val authManager: AuthManager,
    private val preferencesDataStore: NoLonelyPreferences,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val pointsRepository: PointsRepository,
    private val resources: Resources
) : ViewModel(), AuthStateChangeListener {

    val state = MutableStateFlow<ExploreState>(ExploreState.LOADING)
    val stateCompletion = MutableStateFlow<CompletionState>(CompletionState.IDLE)
    val headerState = MutableStateFlow<HeaderState>(HeaderState.TODO)
    val user = MutableStateFlow<UserAccount?>(null)
    val showNotesTipsView = MutableStateFlow(false)
    val showAppRatingCard = MutableStateFlow(false)
    val showInAppReviewDialog = MutableStateFlow(Event(false))
    val userPoints = MutableStateFlow(DEFAULT_USER_POINTS)
    val showEmptyHeader = MutableStateFlow(true)
    val feedsLoader = MutableStateFlow<LoaderState>(LoaderState.LOADING)

    private var hasMoreItems = true
    private var playVideo = false
    private var pointsScope: CoroutineScope? = null
    private var userSubscriptionsList = mutableListOf<BasicSubscription>()
    private var featuredFeeds = MutableStateFlow<List<FeaturedFeed>>(emptyList())

    init {
        viewModelScope.launch {
            userPoints.value = preferencesDataStore.getUserPoints()
            showEmptyHeader.value = preferencesDataStore.shouldShowEmptyActivityHeader()
        }
    }

    fun onStart() {
        appAnalytics.logEvent("explore_open", null)
        appAnalytics.logEvent("view_tab_explore", null)
        authManager.addListener(this)
        startObservingPoints()
        refreshView()

        showPopupsIfApplicable()
    }

    private fun startObservingPoints() {
        pointsScope?.cancel()
        pointsScope = CoroutineScope(appDispatcher.MAIN).apply {
            launch {
                authManager.currentUser?.firebaseAccessKey?.let { key ->
                    pointsRepository.getPoints(key).collectLatest {
                        userPoints.value = it
                        preferencesDataStore.setUserPoints(it)
                    }
                } ?: kotlin.run {
                    preferencesDataStore.setUserPoints(DEFAULT_USER_POINTS)
                    userPoints.value = DEFAULT_USER_POINTS
                }
            }
        }
    }

    private fun stopObservingPoints() {
        pointsScope?.cancel()
        pointsScope = null
    }

    fun onStop() {
        authManager.removeListener(this)
        stopObservingPoints()
        hasMoreItems = true
        feedsLoader.tryEmit(LoaderState.LOADING)
    }

    private fun refreshView() {
        user.value = authManager.currentUser
        loadData()
    }

    private fun loadData(skip: Int = 0) = viewModelScope.launch {
        if (skip == 0) {
            if (state.value !is ExploreState.SUCCESS) {
                state.value = ExploreState.LOADING
            }
            featuredFeeds.tryEmit(emptyList())
        } else if (featuredFeeds.value.size >= 10) {
            feedsLoader.tryEmit(LoaderState.LOADING)
        }

        withContext(appDispatcher.IO) {
            val subsJob = async { service.getTodoTimeline(Date().toStartOfDay().time) }

            service.getFeaturedFeeds(skip = skip).onSuccess { feeds ->
                val list = ArrayList(featuredFeeds.value)
                list.addAll(feeds)
                featuredFeeds.tryEmit(list)
                if (feeds.isEmpty()) {
                    hasMoreItems = false
                }
                loadFeaturedExploreData(subsJob, featuredFeeds)
            }.onFailure {
                Timber.e(it)
                state.value =
                    ExploreState.FAILURE(it.toUserError(resources), it is APIError.NetworkError)
            }
        }
    }

    private suspend fun loadFeaturedExploreData(
        subsJob: Deferred<Result<List<BasicSubscription>>>,
        feeds: MutableStateFlow<List<FeaturedFeed>>
    ) = withContext(appDispatcher.IO) {

        subsJob.await().onSuccess {
            preferencesDataStore.setUserSubscriptionsCount(
                it.filter { subs ->
                    subs.is_active && !(subs.activity.id == DEFAULT_WAKE_UP_ACTIVITY_ID || subs.activity.id == DEFAULT_SLEEP_ACTIVITY_ID)
                }.size
            )

            val userSubscriptions = it.filter { subs -> subs.is_active && !subs.isExcluded }

            userSubscriptionsList = it.filter { subs -> subs.is_active }.toMutableList()

            val overallSubscriptions = it.filter { it.is_active }
            val showPerfectDayView =
                overallSubscriptions.any() && overallSubscriptions.none { !it.activity.is_completed }
            feedsLoader.tryEmit(LoaderState.FINISH)
            val goalsJob = async { service.getAllGoals() }
            loadGoalsData(goalsJob, showPerfectDayView, userSubscriptions, feeds)
        }.onFailure { e ->
            Timber.e(e)
            if (e is APIError.LimitedAccess) {
                navigateToBuyPremiumScreen()
            } else {
                state.value =
                    ExploreState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    private fun navigateToBuyPremiumScreen() = viewModelScope.launch(appDispatcher.MAIN) {
        navManager.navigateToBuyPremiumScreen(
            showBackArrow = false, textType = DEVICE_LIMIT_TEXT_TYPE
        )
    }

    private suspend fun loadGoalsData(
        goalsJob: Deferred<Result<List<Goal>>>,
        showPerfectDayView: Boolean,
        userSubscriptions: List<BasicSubscription>,
        feeds: MutableStateFlow<List<FeaturedFeed>>
    ) = withContext(appDispatcher.IO) {
        goalsJob.await().onSuccess { goals ->
            state.value =
                ExploreState.SUCCESS(
                    showPerfectDayView,
                    userSubscriptions,
                    feeds.value,
                    userSubscriptionsList.mapGoalsWithColor(goals)
                )
            refreshHeaderViewState()
        }.onFailure { e ->
            Timber.e(e)
            if (e is APIError.LimitedAccess) {
                navigateToBuyPremiumScreen()
            } else {
                state.value =
                    ExploreState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    fun onToDoCardClicked(subscriptionId: Int) {
        appAnalytics.logEvent("tap_explore_todo_card", null)
        navManager.navigateToActivityStatusBySubscriptionId(subscriptionId = subscriptionId)
    }

    fun completeHabit(subscription: BasicSubscription) {
        appAnalytics.logEvent("complete_habit", null)
        appAnalytics.logEvent("tap_explore_complete_card", null)

        viewModelScope.launch {
            stateCompletion.emit(CompletionState.COMPLETING(subscriptionId = subscription.id))
            withContext(appDispatcher.IO) {
                service.saveDailyCompletionActivity(subscription.id.mapCompletionData())
                    .onSuccess {
                        Timber.d("Subscription complete success")
                        updateStateOnDailyActivityCompletion(subscription)
                    }
                    .onFailure { e ->
                        Timber.e(e, "Subscription complete failure")
                        stateCompletion.emit(CompletionState.FAILURE(e.toUserError(resources)))
                    }
            }
        }
    }

    private suspend fun showRatingCardViewIfApplicable() {
        if (preferencesDataStore.shouldShowAppRatingAlert()) {
            showAppRatingCard.tryEmit(true)
            appAnalytics.logEvent("prompt_external_rating", null)
        }
    }

    private suspend fun showInAppReviewIfApplicable() {
        if (preferencesDataStore.shouldShowInAppReview()) {
            showInAppReviewDialog.tryEmit(Event(true))
            appAnalytics.logEvent("prompt_in_app_rating", null)
            preferencesDataStore.setInAppReviewPromptShown(true)
        }
    }

    private fun updateStateOnDailyActivityCompletion(habit: BasicSubscription) {
        viewModelScope.launch {
            val activity = habit.activity.copy(is_completed = true)
            val sub = habit.copy(activity = activity)
            userSubscriptionsList.replaceAll { if (it.id == sub.id) sub else it }

            val showPerfectDayView =
                userSubscriptionsList.any() && userSubscriptionsList.none { !it.activity.is_completed }
            eventBus.post(ActivityCompletionEvent(habit.activity))
            preferencesDataStore.setCompletedActivitiesCount(preferencesDataStore.getCompletedActivitiesCount() + 1)
            stateCompletion.emit(CompletionState.IDLE)
            val goalsJob = async { service.getAllGoals() }
            loadGoalsData(goalsJob, showPerfectDayView, userSubscriptionsList, featuredFeeds)

            if (sub.show_note_on_complete) {
                navigateToNotesView(
                    encodeToBase64(sub.activity.name),
                    sub.activity.id.toString(),
                    null,
                    sub.is_custom
                )
            }
            showPopupsIfApplicable()
        }
    }

    private fun showPopupsIfApplicable() = viewModelScope.launch {
        showNotesTipsViewIfApplicable()
        showRatingCardViewIfApplicable()
        showInAppReviewIfApplicable()
    }

    private suspend fun showNotesTipsViewIfApplicable() {
        if (preferencesDataStore.shouldShowNotesTipsView()) {
            showNotesTipsView.tryEmit(true)
        }
    }

    override fun onAuthStateChanged(authState: AuthState) {
        refreshView()
    }

    fun closeRatingCardView() = viewModelScope.launch {
        showAppRatingCard.tryEmit(false)
        appAnalytics.logEvent("tap_prompt_external_rating_close", null)
        preferencesDataStore.setRatingPromptShownTime(System.currentTimeMillis())
    }

    fun redirectToPlayStore() = viewModelScope.launch {
        showAppRatingCard.tryEmit(false)
        appAnalytics.logEvent("tap_prompt_external_rating_rate", null)
        preferencesDataStore.setRatingPromptShownTime(System.currentTimeMillis())
    }

    fun closeNotesTipsView() = viewModelScope.launch {
        appAnalytics.logEvent("tap_explore_notes_tips_close")
        showNotesTipsView.tryEmit(false)
        preferencesDataStore.setIsNotesTipsShown(true)
    }

    fun navigateToMyGoals() {
        navManager.navigateToMyGoalsScreen()
    }

    fun showWebView(url: String, type: Int) {
        navManager.navigateToWebView(url)
        if (type == MOTIVATIONAL_TYPE_PODCASTS) {
            appAnalytics.logEvent("tap_explore_podcast_item", null)
        } else {
            appAnalytics.logEvent("tap_explore_blog_item", null)
        }
    }

    fun playVideo(url: String) {
        playVideo = !playVideo
        if (playVideo) {
            navManager.navigateToVideo(url)
            playVideo = false
        }
    }

    fun onMotivationalItemClick(url: String, isVideoItem: Boolean, type: Int) {
        if (isVideoItem) {
            playVideo(url)
            appAnalytics.logEvent("tap_explore_video_item", null)
        } else {
            showWebView(url, type)
        }
    }

    fun navigateToNotesView(
        activityName: String,
        activityId: String,
        notesId: Int?,
        isCustom: Boolean
    ) {
        appAnalytics.logEvent("long_tap_explore_todo_card")
        closeNotesTipsView()
        val id = notesId?.toString() ?: ""
        navManager.navigateToAddOrUpdateNotes(
            activityName, activityId = activityId, notesId = id, isCustom = isCustom
        )
    }

    fun navigateToLeaderBoardView() {
        appAnalytics.logEvent("tab_explore_points")
        navManager.navigateToLeaderboardScreen()
    }

    fun navigateToSettingScreen() {
        appAnalytics.logEvent("tab_explore_setting")
        navManager.navigateToSettingsScreen()
    }

    fun navigateToStoryView(storyList: Story) {
        navManager.navigateToStoryView(storyList.id)
    }

    private fun refreshHeaderViewState() = viewModelScope.launch {
        if (userSubscriptionsList.isEmpty() && preferencesDataStore.shouldShowEmptyActivityHeader()) {
            headerState.tryEmit(HeaderState.EMPTY_ACTIVITY)
        } else if (preferencesDataStore.shouldShowFirstActivityHeader()) {
            val userActivities =
                userSubscriptionsList.filterNot { it.activity_id == DEFAULT_SLEEP_ACTIVITY_ID || it.activity_id == DEFAULT_WAKE_UP_ACTIVITY_ID }
            if (userActivities.size == 1) {
                headerState.tryEmit(HeaderState.FIRST_ACTIVITY)
            } else {
                preferencesDataStore.setShowFirstActivityHeader(false)
                headerState.tryEmit(HeaderState.TODO)
            }
        } else if (preferencesDataStore.shouldShowFirstCompletionHeader() && preferencesDataStore.getCompletedActivitiesCount() > 0) {
            headerState.tryEmit(HeaderState.FIRST_COMPLETION)
        } else {
            headerState.tryEmit(HeaderState.TODO)
        }
    }

    fun dismissCurrentHeader() = viewModelScope.launch {
        when (headerState.value) {
            HeaderState.EMPTY_ACTIVITY -> {
                appAnalytics.logEvent("tap_empty_header_show_me_btn")
                showEmptyHeader.value = false
                preferencesDataStore.setShowEmptyActivityHeader(false)
                refreshHeaderViewState()
                eventBus.post(ActivitiesTabSelectedEvent(true))
            }
            HeaderState.FIRST_ACTIVITY -> {
                preferencesDataStore.setShowFirstActivityHeader(false)
                refreshHeaderViewState()
            }
            HeaderState.FIRST_COMPLETION -> {
                preferencesDataStore.setShowFirstCompletionHeader(false)
                refreshHeaderViewState()
            }
            else -> {}
        }
    }

    fun onLastItemVisible(index: Int, skip: Int) {
        val showPageLoader = feedsLoader.value is LoaderState.LOADING
        if (index >= featuredFeeds.value.size - 1 && hasMoreItems && !showPageLoader) {
            loadData(skip)
        }
    }

    fun resetCompletionState() {
        stateCompletion.tryEmit(CompletionState.IDLE)
    }

    fun navigateToEditGoal(goalId: Int, subscriptionId: Int) {
        navManager.navigateToEditGoal(subscriptionId, goalId)
    }
}
