package com.canopas.ui.home.ongoing.addActivityView

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.ActivityData
import com.canopas.data.model.Category
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class AddActivityViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val appDispatcher: AppDispatcher,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<CategoryState>(CategoryState.LOADING)
    val searchString = MutableStateFlow("")
    val searchResult = MutableStateFlow(listOf<ActivityData>())
    val searchState = MutableStateFlow<SearchState>(SearchState.START)
    private val activityList = mutableListOf<ActivityData>()
    fun onStart() {
        appAnalytics.logEvent("view_add_activity_list", null)
        getAllActivityCategory()
    }

    private fun getAllActivityCategory() = viewModelScope.launch {
        state.tryEmit(CategoryState.LOADING)
        activityList.clear()
        withContext(appDispatcher.IO) {
            delay(200)
            service.getAllCategories().onSuccess { category ->
                category.forEachIndexed { _, list ->
                    list.activities.forEach {
                        activityList.add(it)
                    }
                }
                state.tryEmit(CategoryState.SUCCESS(category))
            }.onFailure { e ->
                Timber.e(e)
                state.value = CategoryState.FAILURE(e.toUserError(resources))
            }
        }
    }

    fun onSearchValueChange(value: String) {
        searchString.value = value
        if (value.isNotBlank()) {
            val formatted = value.trim().lowercase()
            searchResult.value = activityList.filter { data ->
                if (value.length > 2) {
                    data.name.lowercase().contains(formatted)
                } else {
                    data.name.lowercase().startsWith(formatted)
                }
            }
            if (searchResult.value.isEmpty()) {
                searchState.value = SearchState.EMPTY
            } else {
                searchState.value = SearchState.SUCCESS
            }
        } else {
            searchState.value = SearchState.START
            searchResult.value = emptyList()
        }
    }

    fun navigateToCustomActivity() {
        appAnalytics.logEvent("tap_add_activity_to_own_activity", null)
        navManager.navigateToCustomActivityView()
    }

    fun navigateToActivityList(categoryId: Int) {
        appAnalytics.logEvent("tap_add_activity_category")
        navManager.navigateToActivityList(categoryId)
    }

    fun onActivitySelected(activity: ActivityData) {
        appAnalytics.logEvent("tap_add_activity_activity", null)
        navManager.navigateToHabitConfigure(activity.id)
    }

    fun onBackClick() {
        if (searchState.value !is SearchState.START) {
            onSearchValueChange("")
        } else {
            navManager.popBack()
        }
    }
}

sealed class CategoryState {
    object LOADING : CategoryState()
    data class SUCCESS(val category: List<Category>) : CategoryState()
    data class FAILURE(val message: String) : CategoryState()
}

sealed class SearchState {
    object START : SearchState()
    object SUCCESS : SearchState()
    object EMPTY : SearchState()
}
