package com.canopas.ui.home.settings.privacy

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.ui.R

const val EXPANDED_STATE_ROTATION = 90f
const val NORMAL_STATE_ROTATION = 0f

@Composable
fun PrivacyView() {

    val scrollState = rememberScrollState()
    val viewModel = hiltViewModel<PrivacyViewModel>()

    val storingExpanded by viewModel.storingSectionExpanded.collectAsState()
    val researchExpanded by viewModel.researchSectionExpanded.collectAsState()
    val analyticsExpanded by viewModel.analyticsSectionExpanded.collectAsState()
    val requiredFieldExpanded by viewModel.requiredFieldSectionExpanded.collectAsState()
    val optionalFieldExpanded by viewModel.optionalFieldSectionExpanded.collectAsState()

    val openNotificationDialog =
        viewModel.openNotificationDialog.collectAsState()

    if (openNotificationDialog.value) {
        ShowNotificationDialog(viewModel)
    }

    Scaffold(topBar = {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(id = R.string.privacy_screen_toolbar_title_text),
                    navigationIconOnClick = { viewModel.managePopBack() }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
    }, backgroundColor = colors.background) {
    Column(
        modifier = Modifier
            .padding(start = 16.dp, end = 16.dp, top = it.calculateTopPadding())
            .fillMaxWidth()
            .verticalScroll(scrollState),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        IntroText()
        NecessaryStoringProcessingView(viewModel, storingExpanded)
        AnonymousDataResearchView(viewModel, researchExpanded)
        AnalyticsView(viewModel, analyticsExpanded)
        CategoriesOfData(viewModel = viewModel, requiredFieldExpanded, optionalFieldExpanded)
    }
}
}

@Composable
fun DataListView(data: List<String>, modifier: Modifier) {
    Column(modifier = modifier) {
        data.forEachIndexed { index, text ->
            val paddingModifier = if (index != 0) {
                PaddingValues(start = 68.dp, end = 20.dp, top = 12.dp)
            } else {
                PaddingValues(start = 68.dp, end = 20.dp)
            }
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(paddingModifier),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Box(
                    modifier = Modifier
                        .background(
                            colors.primary,
                            shape = CircleShape,
                        )
                        .size(12.dp)
                )
                Text(
                    text = text,
                    style = AppTheme.typography.bodyTextStyle1,
                    color = colors.textPrimary,
                    modifier = Modifier.padding(start = 16.dp)
                )
            }
        }
    }
}

@Composable
fun IntroText() {
    Text(
        text = stringResource(id = R.string.privacy_screen_intro_text),
        style = AppTheme.typography.bodyTextStyle1,
        color = colors.textPrimary,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 12.dp),
        textAlign = TextAlign.Justify
    )
}

@Composable
fun NecessaryStoringProcessingView(viewModel: PrivacyViewModel, storingExpanded: Boolean) {
    Row(
        modifier = Modifier
            .motionClickEvent {
                viewModel.manageStoringClick()
            }
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {

        Icon(
            painter = painterResource(id = R.drawable.ic_arrow),
            contentDescription = "Consulting Image",
            tint = colors.textPrimary,
            modifier = Modifier
                .size(15.dp)
                .graphicsLayer(rotationZ = animateFloatAsState(if (storingExpanded) EXPANDED_STATE_ROTATION else NORMAL_STATE_ROTATION).value)
        )

        Text(
            text = stringResource(id = R.string.privacy_screen_storing_processing_title_text),
            style = AppTheme.typography.bodyTextStyle1,
            color = colors.textPrimary,
            modifier = Modifier
                .padding(start = 28.dp)
                .fillMaxWidth()
        )
    }

    AnimatedVisibility(
        visible = storingExpanded
    ) {
        Text(
            text = stringResource(id = R.string.privacy_screen_storing_processing_description_text),
            style = AppTheme.typography.bodyTextStyle1,
            color = colors.textPrimary,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 68.dp, end = 20.dp)
        )
    }
}

@Composable
fun AnonymousDataResearchView(viewModel: PrivacyViewModel, researchExpanded: Boolean) {
    Row(
        modifier = Modifier
            .motionClickEvent {
                viewModel.manageResearchClick()
            }
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {

        Icon(
            painter = painterResource(id = R.drawable.ic_arrow),
            contentDescription = "Consulting Image",
            tint = colors.textPrimary,
            modifier = Modifier
                .size(15.dp)
                .graphicsLayer(rotationZ = animateFloatAsState(if (researchExpanded) EXPANDED_STATE_ROTATION else NORMAL_STATE_ROTATION).value)
        )

        Text(
            text = stringResource(id = R.string.privacy_screen_research_title_text),
            style = AppTheme.typography.bodyTextStyle1,
            color = colors.textPrimary,
            modifier = Modifier
                .padding(start = 28.dp)
                .weight(1f)
        )
    }

    AnimatedVisibility(visible = researchExpanded) {
        Text(
            text = stringResource(id = R.string.privacy_screen_research_description_text),
            style = AppTheme.typography.bodyTextStyle1,
            color = colors.textPrimary,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 68.dp, end = 20.dp)
        )
    }
}

@Composable
fun AnalyticsView(viewModel: PrivacyViewModel, analyticsExpanded: Boolean) {
    Row(
        modifier = Modifier
            .motionClickEvent {
                viewModel.manageAnalyticsClick()
            }
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {

        Icon(
            painter = painterResource(id = R.drawable.ic_arrow),
            contentDescription = "Consulting Image",
            tint = colors.textPrimary,
            modifier = Modifier
                .size(15.dp)
                .graphicsLayer(rotationZ = animateFloatAsState(if (analyticsExpanded) EXPANDED_STATE_ROTATION else NORMAL_STATE_ROTATION).value)
        )

        Text(
            text = stringResource(id = R.string.privacy_screen_analytics_title_text),
            style = AppTheme.typography.bodyTextStyle1,
            color = colors.textPrimary,
            modifier = Modifier
                .padding(start = 28.dp)
                .weight(1f)
        )
    }

    AnimatedVisibility(visible = analyticsExpanded) {
        Text(
            text = stringResource(id = R.string.privacy_screen_analytics_description_text),
            style = AppTheme.typography.bodyTextStyle1,
            color = colors.textPrimary,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 68.dp, end = 20.dp)
        )
    }
}

@Preview
@Composable
fun PreviewPrivacyView() {
    PrivacyView()
}

@Preview
@Composable
fun PreviewIntroText() {
    IntroText()
}

@Preview
@Composable
fun PreviewNecessaryStoringProcessingView() {
    val viewModel: PrivacyViewModel = hiltViewModel()
    NecessaryStoringProcessingView(viewModel, true)
}

@Preview
@Composable
fun PreviewAnonymousDataResearchView() {
    val viewModel: PrivacyViewModel = hiltViewModel()
    AnonymousDataResearchView(viewModel, true)
}

@Preview
@Composable
fun PreviewAnalyticsView() {
    val viewModel: PrivacyViewModel = hiltViewModel()
    AnalyticsView(viewModel, true)
}
