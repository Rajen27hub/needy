package com.canopas.ui.home.settings.buypremiumsubscription

import android.app.Activity
import android.content.res.Resources
import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.billingclient.api.ProductDetails
import com.android.billingclient.api.Purchase
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.rest.AuthServices
import com.canopas.base.ui.Event
import com.canopas.data.exception.BuyPremiumResultCall
import com.canopas.data.model.*
import com.canopas.ui.billingdata.GoogleBillingClient
import com.canopas.ui.billingdata.GoogleBillingClient.PurchaseState
import com.canopas.ui.di.SIGN_IN_CLIENT
import com.canopas.ui.navigation.NavManager
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.Calendar
import java.util.Locale
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class BuyPremiumSubscriptionViewModel @Inject constructor(
    private val navManager: NavManager,
    private val buyPremiumResultCall: BuyPremiumResultCall,
    private val eventBus: EventBus,
    private val appDispatcher: AppDispatcher,
    private val googleBillingClient: GoogleBillingClient,
    private val appAnalytics: AppAnalytics,
    private val firebaseCrashlytics: FirebaseCrashlytics,
    private val authServices: AuthServices,
    private val authManager: AuthManager,
    @Named(SIGN_IN_CLIENT) private val signInClient: GoogleSignInClient,
    private val resources: Resources
) : ViewModel() {

    val selectedPlanType = MutableStateFlow(PlanType.PLAN_YEARLY)
    val planOptions = MutableStateFlow(listOf<PlanOption>())
    val openSignOutDialog = MutableStateFlow(Event(false))
    val state = MutableStateFlow<BuyPremiumState>(BuyPremiumState.LOADING)
    val purchaseFlowState = MutableStateFlow<PurchaseFlowState>(PurchaseFlowState.IDLE)
//    val viewAllPlans = MutableStateFlow(false)
    val selectedYearlyPlan = MutableStateFlow(true)
    val selectedMonthlyPlan = MutableStateFlow(false)

    init {
        googleBillingClient.connect()
        viewModelScope.launch { loadPlans() }
    }

    override fun onCleared() {
        googleBillingClient.disconnect()
    }

    fun onStart() = viewModelScope.launch {
        appAnalytics.logEvent(
            "view_upgrade_premium", null
        )
        if (state.value is BuyPremiumState.FAILURE) {
            loadPlans()
        }
    }

    private suspend fun loadPlans() {
        state.tryEmit(BuyPremiumState.LOADING)
        withContext(appDispatcher.IO) {
            googleBillingClient.queryProductDetails(
                listOf(
                    PlanType.PLAN_MONTHLY.id,
                    PlanType.PLAN_YEARLY.id
                )
            ).onSuccess {
                planOptions.tryEmit(it)
//                val isFreeTrialAvailable = it[0].isFreeTrialAvailable || it[1].isFreeTrialAvailable
                state.emit(BuyPremiumState.SUCCESS(it))
            }.onFailure {
                state.emit(BuyPremiumState.FAILURE(it.toUserError(resources), it is APIError.NetworkError))
            }
        }
    }

    fun onPremiumBtnClicked(activity: Activity, type: PlanType?) {
        if (type != null) {
            selectedPlanType.value = type
        }
        val selectPlan =
            planOptions.value.firstOrNull { it.type == selectedPlanType.value } ?: return

        when {
            selectPlan.isFreeTrialAvailable -> {
                appAnalytics.logEvent("tap_upgrade_premium_button_free_trial", null)
            }
            selectedPlanType.value == PlanType.PLAN_YEARLY -> {
                appAnalytics.logEvent("tap_upgrade_premium_button_yearly", null)
            }
            else -> {
                appAnalytics.logEvent("tap_upgrade_premium_button_monthly", null)
            }
        }
        selectPlan.productDetails?.let {
            startPurchaseFlow(activity, it)
        }
    }

    fun updateSubscription(type: PlanType) {
        if (type == PlanType.PLAN_MONTHLY) {
            appAnalytics.logEvent("tap_upgrade_premium_monthly_card", null)
        } else {
            appAnalytics.logEvent("tap_upgrade_premium_yearly_card", null)
        }
        selectedPlanType.value = type
    }

    fun managePopBack(topBarBackClicked: Boolean = false) {
        if (topBarBackClicked) {
            appAnalytics.logEvent("tap_upgrade_premium_back", null)
        } else {
            appAnalytics.logEvent("tap_upgrade_premium_close", null)
        }
        navManager.popBack()
    }

    fun navigateToPrivacyScreen() {
        navManager.navigateToPrivacyScreen()
    }

    private fun startPurchaseFlow(activity: Activity, productDetails: ProductDetails) =
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                googleBillingClient.purchase(activity, productDetails).collect {
                    when (it) {
                        is PurchaseState.Processing -> {
                            purchaseFlowState.tryEmit(PurchaseFlowState.PROCESSING)
                        }
                        is PurchaseState.Success -> {
                            appAnalytics.logEvent("success_upgrade_premium_store", null)
                            upgradeToPremiumSubscription(it.purchase)
                        }
                        is PurchaseState.Error -> {
                            val bundle = Bundle()
                            bundle.putString("error_code", it.error.code.toString())
                            appAnalytics.logEvent("error_upgrade_premium_store", bundle)
                            state.tryEmit(BuyPremiumState.FAILURE(it.error.error, false))
                        }
                        is PurchaseState.UserCancelled -> {
                            Timber.d("Buy Premium - User cancelled purchase")
                            appAnalytics.logEvent("tap_upgrade_premium_store_cancel", null)
                            purchaseFlowState.tryEmit(PurchaseFlowState.CANCELLED)
                        }
                        else -> {}
                    }
                }
            }
        }

    private fun upgradeToPremiumSubscription(purchase: Purchase) {
        viewModelScope.launch {
            purchaseFlowState.tryEmit(PurchaseFlowState.PROCESSING)
            withContext(appDispatcher.IO) {
                val response = buyPremiumResultCall.upgradeToPremium(
                    PremiumSubscriptionBody(
                        purchase.packageName,
                        purchase.products.first(),
                        purchase.purchaseToken
                    )
                )
                if (response.hasError()) {
                    val bundle = Bundle()
                    bundle.putString("status_code", response.statusCode.toString())
                    appAnalytics.logEvent("error_upgrade_premium_server", bundle)
                    Timber.e(response.errorMessage)
                    firebaseCrashlytics.recordException(RuntimeException("BuyPremiumSubscriptionViewModel - upgradeToPremiumSubscription: Got error while upgrading user, error ${response.errorMessage}, code ${response.statusCode}"))
                    state.tryEmit(BuyPremiumState.FAILURE(response.error!!.toUserError(resources), response.error is APIError.NetworkError))
                } else {
                    val userData = response.userAccount
                    authManager.currentUser = userData
                    eventBus.post(userData)
                    googleBillingClient.acknowledgePurchase(purchase)
                    state.tryEmit(BuyPremiumState.UPGRADED)
                    appAnalytics.logEvent("success_upgrade_premium", null)
                    withContext(appDispatcher.MAIN) {
                        navManager.popBack()
                    }
                }
            }
        }
    }

    fun logOutFromOtherDevices() = viewModelScope.launch {
        state.emit(BuyPremiumState.LOADING)
        withContext(appDispatcher.IO) {
            authServices.logOutFromAllDevices()
                .onSuccess {
                    withContext(appDispatcher.MAIN) {
                        navManager.popBack()
                    }
                }.onFailure { e ->
                    Timber.e(e)
                    state.tryEmit(BuyPremiumState.FAILURE(e.toUserError(resources), e is APIError.NetworkError))
                }
        }
    }

    fun logOutUser() = viewModelScope.launch {
        state.emit(BuyPremiumState.LOADING)
        withContext(appDispatcher.IO) {
            authServices.logOutUser(authManager.refreshToken)
                .onSuccess {
                    authManager.resetSession()
                    signInClient.signOut()
                    withContext(appDispatcher.MAIN) {
                        navManager.popBack()
                    }
                }.onFailure { e ->
                    Timber.e(e)
                    state.tryEmit(BuyPremiumState.FAILURE(e.toUserError(resources), e is APIError.NetworkError))
                }
        }
    }

    fun openSignOutDialog() {
        openSignOutDialog.value = Event(true)
    }

    fun closeSignOutDialog() {
        openSignOutDialog.value = Event(false)
    }

    fun getProductAmount(planOption: PlanOption, yearlyDiscountedPrice: String): String {
        val premiumAmount =
            if (planOption.type == PlanType.PLAN_YEARLY && !planOption.price.isNullOrEmpty()) {
                val amount = String.format("%.2f", yearlyDiscountedPrice.toFloat())
                "${planOption.price?.substring(0, 1) ?: ""}$amount"
            } else {
                planOption.price ?: ""
            }
        return premiumAmount
    }

//    fun showAllPlans() {
//        viewAllPlans.tryEmit(true)
//    }

    fun getTrialEndDate(): String {
        val dateFormat = SimpleDateFormat(" dd MMM.", Locale.ENGLISH)
        val currentCal: Calendar = Calendar.getInstance()
        currentCal.add(Calendar.DATE, 7)
        return dateFormat.format(currentCal.time)
    }

    fun onClickBtnView(value: Int){
        when(value){
            0 -> {
                selectedYearlyPlan.tryEmit(true)
                selectedMonthlyPlan.tryEmit(false)
            }
            1 -> {
                selectedMonthlyPlan.tryEmit(true)
                selectedYearlyPlan.tryEmit(false)
            }
        }
    }


    sealed class BuyPremiumState {
        object LOADING : BuyPremiumState()
        data class SUCCESS(val plans: List<PlanOption>) :
            BuyPremiumState()

        data class FAILURE(val message: String, val isNetworkError: Boolean) : BuyPremiumState()
        object UPGRADED : BuyPremiumState()
    }

    sealed class PurchaseFlowState {
        object IDLE : PurchaseFlowState()
        object PROCESSING : PurchaseFlowState()
        object CANCELLED : PurchaseFlowState()
    }
}
