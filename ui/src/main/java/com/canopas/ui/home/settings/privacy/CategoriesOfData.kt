package com.canopas.ui.home.settings.privacy

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.core.animateFloatAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.Icon
import androidx.compose.material.Switch
import androidx.compose.material.SwitchDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.NotificationDialogButtonColor
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.uncheckSwitchBg
import com.canopas.ui.R

@Composable
fun CategoriesOfData(
    viewModel: PrivacyViewModel,
    requiredFieldExpanded: Boolean,
    optionalFieldExpanded: Boolean
) {
    Text(
        text = stringResource(id = R.string.privacy_screen_data_categories_text),
        style = AppTheme.typography.h2TextStyle,
        color = ComposeTheme.colors.textPrimary,
        modifier = Modifier
            .padding(top = 20.dp)
            .fillMaxWidth()

    )
    RequiredDataView(viewModel, requiredFieldExpanded)
    OptionalDataView(viewModel, optionalFieldExpanded)
}

@Composable
fun RequiredDataView(viewModel: PrivacyViewModel, requiredFieldExpanded: Boolean) {
    Row(
        modifier = Modifier
            .motionClickEvent {
                viewModel.manageRequiredFieldClick()
            }
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {

        Icon(
            painter = painterResource(id = R.drawable.ic_arrow),
            contentDescription = "Consulting Image",
            tint = ComposeTheme.colors.textPrimary,
            modifier = Modifier
                .size(15.dp)
                .graphicsLayer(rotationZ = animateFloatAsState(if (requiredFieldExpanded) EXPANDED_STATE_ROTATION else NORMAL_STATE_ROTATION).value)
        )

        Text(
            text = stringResource(id = R.string.privacy_screen_required_checkbox_data_text),
            style = AppTheme.typography.bodyTextStyle1,
            color = ComposeTheme.colors.textPrimary,
            modifier = Modifier
                .padding(start = 28.dp)
                .weight(1f)
        )

        Switch(
            modifier = Modifier
                .wrapContentHeight()
                .wrapContentWidth(),
            checked = true,
            onCheckedChange = {
                viewModel.openNotificationDialog(true)
            },
            colors = SwitchDefaults.colors(
                checkedThumbColor = White,
                checkedTrackColor = ComposeTheme.colors.primary,
                uncheckedThumbColor = White,
                uncheckedTrackColor = uncheckSwitchBg
            )
        )
    }

    AnimatedVisibility(visible = requiredFieldExpanded) {
        val requiredFieldData = listOf(
            stringResource(id = R.string.privacy_screen_email_text),
            stringResource(id = R.string.privacy_screen_phone_text),
            stringResource(id = R.string.privacy_screen_gender_text)
        )
        DataListView(requiredFieldData, modifier = Modifier)
    }
}

@Composable
fun OptionalDataView(viewModel: PrivacyViewModel, optionalFieldExpanded: Boolean) {
    Row(
        modifier = Modifier
            .motionClickEvent {
                viewModel.manageOptionalFieldClick()
            }
            .fillMaxWidth()
            .padding(horizontal = 12.dp, vertical = 16.dp),
        verticalAlignment = Alignment.CenterVertically
    ) {

        Icon(
            painter = painterResource(id = R.drawable.ic_arrow),
            contentDescription = "Consulting Image",
            tint = ComposeTheme.colors.textPrimary,
            modifier = Modifier
                .size(15.dp)
                .graphicsLayer(rotationZ = animateFloatAsState(if (optionalFieldExpanded) EXPANDED_STATE_ROTATION else NORMAL_STATE_ROTATION).value)
        )

        Text(
            text = stringResource(id = R.string.privacy_screen_optional_checkbox_data_text),
            style = AppTheme.typography.bodyTextStyle1,
            color = ComposeTheme.colors.textPrimary,
            modifier = Modifier
                .padding(start = 28.dp)
                .weight(1f)
        )

        Box(contentAlignment = Alignment.Center) {
            Switch(
                modifier = Modifier
                    .wrapContentHeight()
                    .wrapContentWidth(),
                checked = true,
                onCheckedChange = {
                    viewModel.openNotificationDialog(false)
                },
                colors = SwitchDefaults.colors(
                    checkedThumbColor = White,
                    checkedTrackColor = ComposeTheme.colors.primary,
                    uncheckedThumbColor = White,
                    uncheckedTrackColor = uncheckSwitchBg
                )
            )
        }
    }

    AnimatedVisibility(visible = optionalFieldExpanded) {
        val optionalFieldData = listOf(
            stringResource(id = R.string.privacy_screen_username_text),
            stringResource(id = R.string.privacy_screen_avatar_text),
            stringResource(id = R.string.privacy_screen_occupation_text),
            stringResource(id = R.string.privacy_screen_feedback_text)
        )
        DataListView(data = optionalFieldData, modifier = Modifier.padding(bottom = 16.dp))
    }
}

@Composable
fun ShowNotificationDialog(viewModel: PrivacyViewModel) {
    val dialogTitleText: String
    val dialogDescriptionText: String
    if (viewModel.showRequiredDataPrompt.collectAsState().value) {
        dialogTitleText =
            stringResource(id = R.string.privacy_screen_alert_dialog_required_data_text)
        dialogDescriptionText =
            stringResource(id = R.string.privacy_screen_alert_dialog_description_text)
    } else {
        dialogTitleText =
            stringResource(id = R.string.privacy_screen_alert_dialog_optional_data_text)
        dialogDescriptionText =
            stringResource(id = R.string.privacy_screen_alert_dialog_optional_description_text)
    }
    CustomAlertDialog(
        title = dialogTitleText,
        subTitle = dialogDescriptionText,
        confirmBtnText = null,
        dismissBtnText = stringResource(id = R.string.privacy_screen_alert_dialog_ok_button_text),
        dismissBtnColor = NotificationDialogButtonColor,
        onDismissClick = { viewModel.closeNotificationDialog() }
    )
}

@Preview
@Composable
fun PreviewRequiredDataView() {
    val viewModel: PrivacyViewModel = hiltViewModel()
    RequiredDataView(viewModel, true)
}

@Preview
@Composable
fun PreviewOptionalDataView() {
    val viewModel: PrivacyViewModel = hiltViewModel()
    OptionalDataView(viewModel, true)
}

@Preview
@Composable
fun PreviewShowNotificationDialog() {
    val viewModel: PrivacyViewModel = hiltViewModel()
    ShowNotificationDialog(viewModel)
}
