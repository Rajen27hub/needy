package com.canopas.ui.home.settings.feedback

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.DEVICE_TYPE
import com.canopas.base.data.utils.Device
import com.canopas.base.ui.Event
import com.canopas.data.model.FeedbackRequest
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.customactivity.MINIMUM_CHARACTER_LENGTH
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import timber.log.Timber
import java.io.File
import javax.inject.Inject

@HiltViewModel
class FeedbackViewModel @Inject constructor(
    private val noLonelyService: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    private val navManager: NavManager,
    private val device: Device,
    private val resources: Resources
) : ViewModel() {

    val title = MutableStateFlow("")
    val description = MutableStateFlow("")
    val showShortTitleError = MutableStateFlow(false)
    val feedbackState = MutableStateFlow<FeedbackState>(FeedbackState.START)
    val openImageChooserEvent = MutableStateFlow(Event(false))
    var pendingUploadedFeedbackImage = MutableStateFlow<File?>(null)
    val showSuccessAlertDialog = MutableStateFlow(false)

    fun onTitleChange(newValue: String) {
        title.tryEmit(newValue)
        if (title.value.length >= MINIMUM_CHARACTER_LENGTH) {
            showShortTitleError.tryEmit(false)
        }
    }

    fun onDescriptionChange(newValue: String) {
        description.tryEmit(newValue)
    }

    fun submitReport(logFile: File, title: String, feedback: String, storyId: String) =
        viewModelScope.launch {
            if (title.length < MINIMUM_CHARACTER_LENGTH) {
                showShortTitleError.tryEmit(true)
                return@launch
            }
            feedbackState.value = FeedbackState.LOADING
            withContext(appDispatcher.IO) {
                val request =
                    FeedbackRequest(
                        title,
                        feedback,
                        DEVICE_TYPE,
                        device.getAppVersionCode().toString(),
                        device.deviceName(),
                        device.getDeviceOsVersion(),
                        if (storyId != "") storyId else null
                    )

                val logFileData = MultipartBody.Part.createFormData(
                    "log_file", logFile.name,
                    logFile.asRequestBody("*/*".toMediaTypeOrNull())
                )

                val imageFileData = pendingUploadedFeedbackImage.value?.let { feedBackImage ->
                    MultipartBody.Part.createFormData(
                        "file", feedBackImage.name,
                        feedBackImage
                            .asRequestBody("*/*".toMediaTypeOrNull())
                    )
                }

                noLonelyService.submitReport(logFileData, imageFileData, request).onSuccess {
                    showSuccessAlertDialog.value = true
                }.onFailure { e ->
                    Timber.e(e)
                    feedbackState.tryEmit(FeedbackState.ERROR(e.toUserError(resources)))
                }
            }
        }

    fun openImageChanger() {
        openImageChooserEvent.value = Event(true)
    }

    fun uploadSelectedImage(image: File) {
        pendingUploadedFeedbackImage.tryEmit(image)
    }

    fun removeSelectedImage() {
        pendingUploadedFeedbackImage.tryEmit(null)
    }

    fun managePopBack() {
        showSuccessAlertDialog.value = false
        navManager.popBack()
    }
}

sealed class FeedbackState {
    object START : FeedbackState()
    object LOADING : FeedbackState()
    data class ERROR(val message: String) : FeedbackState()
}
