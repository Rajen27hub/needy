package com.canopas.ui.home.ongoing.history

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.Subscription
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ArchivedActivitiesViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<SubscriptionsHistoryState>(SubscriptionsHistoryState.LOADING)

    fun onStart() {
        appAnalytics.logEvent("view_archived_activities")
        getSubscriptionHistory()
    }

    private fun getSubscriptionHistory() = viewModelScope.launch {
        if (state.value !is SubscriptionsHistoryState.SUCCESS && state.value !is SubscriptionsHistoryState.FAILURE) {
            state.tryEmit(SubscriptionsHistoryState.LOADING)
        }
        withContext(appDispatcher.IO) {
            service.getUserSubscriptionsHistory().onSuccess { subscriptions ->
                state.tryEmit(SubscriptionsHistoryState.SUCCESS(subscriptions))
            }.onFailure { e ->
                Timber.e(e)
                state.value = SubscriptionsHistoryState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    fun onBackClick() {
        navManager.popBack()
    }

    fun onActivityClicked(subscriptionId: Int) {
        appAnalytics.logEvent("tap_archived_activities_item")
        navManager.navigateToActivityStatusBySubscriptionId(
            subscriptionId = subscriptionId,
            "",
            true
        )
    }
}

sealed class SubscriptionsHistoryState {
    object START : SubscriptionsHistoryState()
    object LOADING : SubscriptionsHistoryState()
    data class SUCCESS(
        val subscriptionHistoryList: List<Subscription>
    ) : SubscriptionsHistoryState()

    data class FAILURE(val message: String, val isNetworkError: Boolean) : SubscriptionsHistoryState()
}
