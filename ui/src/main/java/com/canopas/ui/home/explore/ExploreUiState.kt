package com.canopas.ui.home.explore

import com.canopas.data.model.BasicSubscription
import com.canopas.data.model.FeaturedFeed
import com.canopas.data.model.GoalWithColors

sealed class ExploreState {
    object LOADING : ExploreState()

    data class SUCCESS(
        val showPerfectDayView: Boolean,
        val subscriptions: List<BasicSubscription>,
        val feeds: List<FeaturedFeed>,
        val goals: List<GoalWithColors>
    ) : ExploreState()

    data class FAILURE(val message: String, val isNetworkError: Boolean) : ExploreState()
}

sealed class CompletionState {
    object IDLE : CompletionState()
    data class COMPLETING(val subscriptionId: Int) : CompletionState()
    data class FAILURE(val message: String) : CompletionState()
}

sealed class HeaderState {
    object EMPTY_ACTIVITY : HeaderState()
    object FIRST_ACTIVITY : HeaderState()
    object FIRST_COMPLETION : HeaderState()
    object TODO : HeaderState()
}

sealed class LoaderState {
    object LOADING : LoaderState()
    object FINISH : LoaderState()
}
