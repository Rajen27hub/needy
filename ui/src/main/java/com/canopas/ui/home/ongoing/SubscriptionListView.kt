package com.canopas.ui.home.ongoing

import androidx.compose.animation.core.Spring
import androidx.compose.animation.core.animateDpAsState
import androidx.compose.animation.core.spring
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Canvas
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.model.SubscriptionCardDetails
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils.Companion.getSubscriptionCardDetails
import com.canopas.base.ui.customview.EmptyActivitiesView
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.Subscription
import com.canopas.ui.R

@Composable
fun SubscriptionListView() {

    val viewModel = hiltViewModel<SubscriptionListViewModel>()
    val state by viewModel.state.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart()
    })

    state.let { listState ->
        when (listState) {
            is SubscriptionsState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .background(colors.background)
                        .fillMaxSize()
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }
            is SubscriptionsState.FAILURE -> {
                if (listState.isNetworkError) {
                    NetworkErrorView {
                        viewModel.onStart()
                    }
                } else {
                    ServerErrorView {
                        viewModel.onStart()
                    }
                }
            }
            is SubscriptionsState.SUCCESS -> {
                val subscriptions = listState.onGoingActivities
                SubscriptionSuccessState(subscriptions, viewModel)
            }
            else -> {}
        }
    }
}

@Composable
fun SubscriptionSuccessState(
    subscriptions: List<Subscription>,
    viewModel: SubscriptionListViewModel
) {
    val context = LocalContext.current

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(id = R.string.subscription_list_heading_text),
                    actions = {
                        IconButton(onClick = {
                            viewModel.navigateToArchivedActivitiesView()
                        }) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_history_icon),
                                contentDescription = null,
                                tint = colors.textPrimary,
                                modifier = Modifier.size(24.dp)
                            )
                        }
                    }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )

        if (subscriptions.isEmpty()) {
            EmptyActivitiesView {
                viewModel.navigateToAddActivityView()
            }
        } else {
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(colors.background),
                contentAlignment = Alignment.TopCenter
            ) {
                LazyColumn(modifier = Modifier.padding(horizontal = 20.dp)) {
                    itemsIndexed(subscriptions) { _, item ->
                        val subscriptionCardData = getSubscriptionCardDetails(
                            context, item.progress,
                            isDarkMode
                        )
                        Box(
                            modifier = Modifier
                                .widthIn(max = 600.dp)
                                .fillMaxWidth()
                                .wrapContentHeight()
                                .padding(vertical = 5.dp)
                                .motionClickEvent {
                                    viewModel.onActivityClicked(item.id)
                                }
                                .clip(shape = RoundedCornerShape(15.dp))
                                .background(
                                    color = if (isDarkMode) darkActivityCard else subscriptionCardData.cardBgColor,
                                    shape = RoundedCornerShape(15.dp)
                                )
                                .border(
                                    BorderStroke(1.dp, subscriptionCardData.cardBgColor),
                                    RoundedCornerShape(16)
                                )
                        ) {

                            Column(
                                modifier = Modifier
                                    .padding(12.dp),
                                verticalArrangement = Arrangement.SpaceBetween,
                                horizontalAlignment = Alignment.Start
                            ) {
                                Text(
                                    text = item.activity.name.trim(),
                                    style = AppTheme.typography.subscriptionListActivityNameTextStyle,
                                    color = colors.textPrimary,
                                    modifier = Modifier.fillMaxWidth(0.7f),
                                    maxLines = 3,
                                    overflow = TextOverflow.Ellipsis
                                )

                                Text(
                                    text = if (item.progress.recent.isNotEmpty()) "${subscriptionCardData.recentProgress}%" else stringResource(
                                        R.string.subscription_list_just_started_text
                                    ),
                                    color = subscriptionCardData.progressTextColor,
                                    style = AppTheme.typography.h2TextStyle,
                                    modifier = Modifier.padding(top = 8.dp)
                                )
                                if (item.progress.recent.isNotEmpty()) {
                                    Text(
                                        text = subscriptionCardData.recentDaysCount,
                                        color = subscriptionCardData.progressTextColor.copy(0.75f),
                                        style = AppTheme.typography.buttonStyle
                                    )
                                }
                            }
                            Row(
                                modifier = Modifier
                                    .align(Alignment.TopEnd)
                                    .padding(16.dp),
                                verticalAlignment = Alignment.CenterVertically
                            ) {
                                Text(
                                    text = if (item.activity_time != null) item.getFormattedActivityTime() else stringResource(
                                        id = R.string.subscription_list_no_reminder_text
                                    ),
                                    color = colors.textSecondary,
                                    style = AppTheme.typography.bodyTextStyle2,
                                    modifier = Modifier.padding(top = 4.dp)
                                )
                                if (item.activity_time != null) {
                                    Icon(
                                        painter = painterResource(id = R.drawable.ic_arrow),
                                        contentDescription = "Reminder Arrow",
                                        modifier = Modifier
                                            .padding(start = 4.dp, top = 4.dp)
                                            .size(10.dp),
                                        tint = colors.textSecondary
                                    )
                                }
                            }
                            if (item.progress.recent.isNotEmpty()) {
                                Row(
                                    modifier = Modifier
                                        .align(Alignment.BottomEnd)
                                        .padding(bottom = 16.dp, end = 16.dp)
                                        .wrapContentWidth()
                                        .height(40.dp),
                                    horizontalArrangement = Arrangement.End,
                                    verticalAlignment = Alignment.Bottom
                                ) {
                                    BarChartGraph(subscriptionCardData, item)
                                }
                            }
                        }
                    }
                    item {
                        Spacer(modifier = Modifier.height(100.dp))
                    }
                }
                Box(
                    modifier = Modifier
                        .widthIn(max = 600.dp)
                        .fillMaxSize(),
                    contentAlignment = Alignment.BottomEnd
                ) {
                    Row(
                        Modifier
                            .padding(16.dp)
                            .size(56.dp)
                            .clip(shape = CircleShape)
                            .background(colors.primary)
                            .motionClickEvent { viewModel.navigateToAddActivityView() },
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Center
                    ) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_add_activity_icon),
                            contentDescription = "",
                            tint = Color.White,
                            modifier = Modifier.size(24.dp)
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun BarChartGraph(subscriptionCardData: SubscriptionCardDetails, item: Subscription) {
    var graphState by remember { mutableStateOf(GraphPosition.Start) }
    val graphColor = subscriptionCardData.progressTextColor
    val isDarkMode = isDarkMode
    val height: Dp by animateDpAsState(
        if (graphState == GraphPosition.Start) 1.dp else 40.dp,
        spring(stiffness = Spring.StiffnessLow)
    )
    LaunchedEffect(key1 = Unit, block = {
        graphState = when (graphState) {
            GraphPosition.Start -> GraphPosition.Finish
            GraphPosition.Finish -> GraphPosition.Start
        }
    })
    item.progress.recent.forEach {
        CompositionLocalProvider(LocalLayoutDirection provides LayoutDirection.Rtl) {
            Canvas(
                modifier = Modifier
                    .width(10.dp)
                    .height(if (it.completed) height else 4.dp)
                    .clip(shape = RoundedCornerShape(topStart = 50.dp, topEnd = 50.dp)),
            ) {
                drawRect(
                    brush = Brush.verticalGradient(
                        if (isDarkMode) listOf(graphColor, graphColor) else
                            listOf(
                                subscriptionCardData.progressTextColor.copy(
                                    0.50f
                                ),
                                subscriptionCardData.progressTextColor
                            )
                    ),
                )
            }
            Spacer(modifier = Modifier.width(4.dp))
        }
    }
}

enum class GraphPosition {
    Start, Finish
}

private val darkActivityCard = Color(0xFF222222)
