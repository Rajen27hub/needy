package com.canopas.ui.home.ongoing.poststory

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.BasicTextField
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Checkbox
import androidx.compose.material.CheckboxDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.PostStoryTitleLineView
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.textColor
import com.canopas.ui.R

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun PostSuccessStoryView(
    activityName: String,
    activityId: String,
    storyId: String = "",
    isEditModeEnable: Boolean = false
) {

    val viewModel = hiltViewModel<PostSuccessStoryViewModel>()
    val title by viewModel.title.collectAsState()
    val description by viewModel.description.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.fetchSuccessStory(storyId)
    })

    val enabledPostBtnState by viewModel.enablePostButton.collectAsState()

    val focusManager = LocalFocusManager.current

    val state by viewModel.storyState.collectAsState()
    val context = LocalContext.current

    val keyboardController = LocalSoftwareKeyboardController.current
    DisposableEffect(key1 = Unit, effect = {
        onDispose {
            keyboardController?.hide()
        }
    })

    Scaffold(topBar = {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(id = R.string.post_success_story_txt_toolbar_title),
                    navigationIconOnClick = { viewModel.managePopBack() }
                )
            },
            backgroundColor = White,
            contentColor = Color.Black,
            modifier = Modifier.fillMaxWidth(),
            elevation = 0.dp
        )
    }) {

        state.let { state ->
            when (state) {
                PostStoryState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = ColorPrimary)
                    }
                }
                is PostStoryState.FAILURE -> {
                    val message = state.message
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                    }
                }

                is PostStoryState.POSTED -> {
                    LaunchedEffect(key1 = Unit) {
                        viewModel.managePopBackExplore()
                    }
                }
                PostStoryState.COMPLETED -> {
                    LaunchedEffect(key1 = Unit) {
                        viewModel.managePopBack()
                    }
                }
                else -> {}
            }
        }
        Box(
            modifier = Modifier
                .fillMaxSize()
                .clickable(
                    interactionSource = MutableInteractionSource(),
                    indication = null,
                    onClick = {
                        keyboardController?.hide()
                        focusManager.clearFocus()
                    }
                )
        ) {
            Column(
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .padding(start = 20.dp, end = 20.dp)
                    .align(Alignment.TopCenter),
            ) {
                Text(
                    text = stringResource(id = R.string.post_success_story_txt_well_performed_activities),
                    style = AppTheme.typography.subTextStyle,
                    color = textColor.copy(0.87f),
                    modifier = Modifier.padding(top = 20.dp, bottom = 20.dp)
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(
                            textColor.copy(0.04f),
                            shape = RoundedCornerShape(7.dp)
                        ),
                    verticalAlignment = Alignment.CenterVertically,
                ) {
                    Checkbox(
                        checked = true, onCheckedChange = {},
                        colors = CheckboxDefaults.colors(textColor.copy(0.6f))
                    )
                    Text(
                        text = activityName,
                        color = textColor.copy(0.87f),
                        style = AppTheme.typography.bodyTextStyle1
                    )
                }

                val titleInteractionSource = remember { MutableInteractionSource() }
                val descriptionInteractionSource = remember { MutableInteractionSource() }
                val isTitleFocused by titleInteractionSource.collectIsFocusedAsState()
                val isDescriptionFocused by descriptionInteractionSource.collectIsFocusedAsState()

                Text(
                    text = stringResource(id = R.string.post_success_story_text_title),
                    color = if (isTitleFocused) ColorPrimary else textColor.copy(0.4f),
                    style = AppTheme.typography.bodyTextStyle2,
                    modifier = Modifier
                        .padding(top = 48.dp, start = 4.dp)
                )

                BasicTextField(
                    value = title,
                    onValueChange = {
                        viewModel.onTitleChange(it)
                    },
                    interactionSource = titleInteractionSource,
                    modifier = Modifier
                        .motionClickEvent { }
                        .padding(start = 4.dp, end = 4.dp, top = 4.dp)
                        .fillMaxWidth(),
                    maxLines = 4,
                    singleLine = false,
                    textStyle = AppTheme.typography.bodyTextStyle1,
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Default
                    ),
                    cursorBrush = SolidColor(ColorPrimary.copy(0.5f))
                )

                PostStoryTitleLineView(
                    if (isTitleFocused) ColorPrimary.copy(0.5f) else textColor.copy(
                        0.4f
                    )
                )

                Text(
                    text = stringResource(id = R.string.post_success_story_txt_description),
                    color = if (isDescriptionFocused) ColorPrimary else textColor.copy(0.4f),
                    style = AppTheme.typography.bodyTextStyle2,
                    modifier = Modifier.padding(top = 20.dp, start = 4.dp)
                )

                OutlinedTextField(
                    value = description,
                    onValueChange = {
                        viewModel.onDescriptionChange(it)
                    },
                    interactionSource = descriptionInteractionSource,
                    modifier = Modifier
                        .motionClickEvent { }
                        .fillMaxWidth()
                        .fillMaxHeight(0.6f)
                        .padding(top = 4.dp),
                    shape = RoundedCornerShape(12.dp),
                    textStyle = AppTheme.typography.bodyTextStyle1,
                    singleLine = false,
                    keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Default
                    ),
                    colors = TextFieldDefaults.outlinedTextFieldColors(
                        focusedBorderColor = ColorPrimary.copy(0.5f),
                        unfocusedBorderColor = textColor.copy(0.6f),
                        cursorColor = ColorPrimary.copy(0.5f)
                    )
                )
                val buttonText = if (isEditModeEnable)
                    stringResource(id = R.string.post_success_story_txt_update_btn) else
                    stringResource(id = R.string.post_success_story_txt_submit_btn)

                PrimaryButton(
                    onClick = {
                        viewModel.handleSaveButtonClick(
                            storyId,
                            activityId.toInt(),
                            isEditModeEnable
                        )
                    },
                    enabled = enabledPostBtnState,
                    shape = RoundedCornerShape(50),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = ColorPrimary,
                        contentColor = Color.White,
                        disabledBackgroundColor = ColorPrimary.copy(0.6f)
                    ),
                    modifier = Modifier
                        .padding(top = 48.dp)
                        .fillMaxWidth()
                        .height(48.dp),
                    text = buttonText,
                )
            }
        }
    }
}

@Preview
@Composable
fun PreviewPostSuccessStoryView() {
    PostSuccessStoryView(activityName = "", activityId = "1", "", false)
}
