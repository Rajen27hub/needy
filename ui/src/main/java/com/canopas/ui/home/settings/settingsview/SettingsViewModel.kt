package com.canopas.ui.home.settings.settingsview

import android.app.NotificationManager
import android.content.res.Resources
import androidx.appcompat.app.AppCompatDelegate
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.UserAccount
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.rest.AuthServices
import com.canopas.base.ui.Event
import com.canopas.data.event.DarkModeUpdateEvent
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.di.SIGN_IN_CLIENT
import com.canopas.ui.navigation.NavManager
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Named

@HiltViewModel
class SettingsViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val authServices: AuthServices,
    private val authManager: AuthManager,
    private val eventBus: EventBus,
    private val navManager: NavManager,
    private val noLonelyService: NoLonelyService,
    private val appAnalytics: AppAnalytics,
    private val preferences: NoLonelyPreferences,
    private val notificationManager: NotificationManager,
    @Named(SIGN_IN_CLIENT) private val signInClient: GoogleSignInClient,
    private val resources: Resources
) : ViewModel() {
    val state = MutableStateFlow<SettingsState>(SettingsState.LOADING)
    val openSignOutDialog = MutableStateFlow(Event(false))
    var userType = MutableStateFlow(authManager.currentUser?.userType)
    val productId = MutableStateFlow("")
    val scrollToTop = MutableStateFlow(false)
    val darkModeState = MutableStateFlow(false)
    val userVisibilityState = MutableStateFlow<UserVisibilityState>(UserVisibilityState.PROGRESS)
    val isVisibleOnLeaderboard = MutableStateFlow(true)

    init {
        eventBus.register(this)
        viewModelScope.launch {
            darkModeState.value = preferences.getIsDarkModeEnabled()
        }
    }

    override fun onCleared() {
        eventBus.unregister(this)
        super.onCleared()
    }

    fun handleBuyPremiumCardClick() {
        if (authManager.authState.isVerified) {
            openBuyPremiumScreen()
        } else {
            navigateToSignInMethods()
        }
    }

    fun logOutUser() = viewModelScope.launch {
        state.emit(SettingsState.LOADING)
        withContext(appDispatcher.IO) {
            authServices.logOutUser(authManager.refreshToken)
                .onSuccess {
                    authManager.resetSession()
                    notificationManager.cancelAll()
                    signInClient.signOut()
                    userType.tryEmit(authManager.currentUser?.userType)
                    state.emit(SettingsState.UNAUTHORIZED)
                    scrollToTop.tryEmit(true)
                }.onFailure { e ->
                    Timber.e(e)
                    state.tryEmit(SettingsState.FAILURE(e.toUserError(resources)))
                }
        }
    }

    fun navigateToSignInMethods() {
        appAnalytics.logEvent("tap_settings_sign_in", null)
        navManager.navigateToSignInMethods()
    }

    fun openProfileScreen() {
        appAnalytics.logEvent("tap_settings_view_profile", null)
        navManager.navigateToProfileScreen()
    }

    fun openFeedbackScreen(selectedTab: Int) {
        appAnalytics.logEvent("tap_settings_send_feedback", null)
        navManager.navigateToFeedback(selectedTab = selectedTab)
    }

    fun openNotificationScreen() {
        appAnalytics.logEvent("tap_setting_Notifications", null)
        navManager.navigateToNotificationsScreen()
    }

    fun openPrivacyScreen() {
        navManager.navigateToPrivacyScreen()
    }

    fun openBuyPremiumScreen() {
        appAnalytics.logEvent("tap_settings_upgrade_premium", null)
        navManager.navigateToBuyPremiumScreen()
    }

    fun addPremiumBtnClickAnalyticEvent() {
        appAnalytics.logEvent("tap_settings_view_premium_sub", null)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onUserUpdate(user: UserAccount?) {
        val isAnonymous = user?.isAnonymous ?: true
        if (!isAnonymous) {
            this.state.tryEmit(SettingsState.AUTHORIZED(user!!))
        } else {
            this.state.tryEmit(SettingsState.UNAUTHORIZED)
        }
        userType.value = user?.userType
    }

    private fun updateLeaderboardVisibility() {
        viewModelScope.launch {
            userVisibilityState.tryEmit(UserVisibilityState.PROGRESS)
            withContext(appDispatcher.IO) {
                val userVisibility = isVisibleOnLeaderboard.value
                val user = authManager.currentUser
                user!!.leaderBoardVisibility = userVisibility
                authManager.currentUser = user
                noLonelyService.updateLeaderboardVisibility(userVisibility).onSuccess {
                    userVisibilityState.tryEmit(UserVisibilityState.SUCCESS)
                }.onFailure { e ->
                    Timber.e(e)
                    userVisibilityState.tryEmit(UserVisibilityState.FAILURE(e.toUserError(resources)))
                }
            }
        }
    }

    fun onLeaderboardVisibilityChange(enabled: Boolean) {
        appAnalytics.logEvent("setting_tab_leader_board_switch {$enabled}", null)
        viewModelScope.launch {
            isVisibleOnLeaderboard.tryEmit(enabled)
            updateLeaderboardVisibility()
        }
    }

    fun onStart() {
        appAnalytics.logEvent("view_tab_settings", null)
        val user = authManager.currentUser
        if (user != null) {
            isVisibleOnLeaderboard.tryEmit(user.leaderBoardVisibility)
            userType.tryEmit(user.userType)
            if (user.isAnonymous) {
                state.tryEmit(SettingsState.UNAUTHORIZED)
            } else {
                state.tryEmit(SettingsState.AUTHORIZED(user))
            }
        }
    }

    fun openSignOutDialog() {
        appAnalytics.logEvent("tap_settings_sign_out", null)
        openSignOutDialog.value = Event(true)
    }

    fun closeSignOutDialog() {
        openSignOutDialog.value = Event(false)
    }

    fun onDarkModeChange(enableDarkMode: Boolean) {
        viewModelScope.launch {
            appAnalytics.logEvent("setting_tab_dark_mode_switch {$enableDarkMode}")
            darkModeState.tryEmit(enableDarkMode)
            preferences.setIsDarkModeEnabled(enableDarkMode)
            eventBus.post(DarkModeUpdateEvent(enableDarkMode))
            if (enableDarkMode) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }
    }
    fun popBack() {
        navManager.popBack()
    }

    fun updateScroll() {
        scrollToTop.tryEmit(false)
    }
}

sealed class SettingsState {
    object LOADING : SettingsState()
    object UNAUTHORIZED : SettingsState()
    data class AUTHORIZED(val user: UserAccount) : SettingsState()
    data class FAILURE(val message: String) : SettingsState()
}

sealed class UserVisibilityState {
    object PROGRESS : UserVisibilityState()
    object SUCCESS : UserVisibilityState()
    data class FAILURE(val message: String) : UserVisibilityState()
}
