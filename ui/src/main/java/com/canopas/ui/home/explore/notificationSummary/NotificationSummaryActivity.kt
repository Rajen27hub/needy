package com.canopas.ui.home.explore.notificationSummary

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.BackHandler
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.CircularGradientProgressView
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.InterSemiBoldFont
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.parse
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.base.ui.themes.JustlyAppTheme
import com.canopas.data.model.SummaryDate
import com.canopas.data.model.WeeklySummary
import com.canopas.ui.R
import com.canopas.ui.home.HomeActivity
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint
import kotlin.math.roundToInt

@AndroidEntryPoint
class NotificationSummaryActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val startDate = intent.getStringExtra("start_date") ?: ""
        val endDate = intent.getStringExtra("end_date") ?: ""

        setContent {
            val viewModel = hiltViewModel<NotificationSummaryViewModel>()
            val isAppDarkModeEnabled by viewModel.isDarkModeEnabled.collectAsState()
            LaunchedEffect(key1 = Unit) {
                viewModel.onStart(startDate, endDate)
            }
            JustlyAppTheme(isDarkMode = isAppDarkModeEnabled) {
                BuildWeeklySummaryView(viewModel)
            }
        }
    }
}

@Composable
fun BuildWeeklySummaryView(
    viewModel: NotificationSummaryViewModel
) {
    val systemUiController = rememberSystemUiController()
    val defaultColor = colors.background
    val isDarkMode = isDarkMode

    val state by viewModel.state.collectAsState()
    val context = LocalContext.current
    LaunchedEffect(key1 = Unit) {
        systemUiController.setStatusBarColor(
            color = defaultColor,
            darkIcons = !isDarkMode
        )
    }

    BackHandler(enabled = true, onBack = {
        val intent = Intent(context, HomeActivity::class.java)
        context.startActivity(intent)
    })

    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(id = R.string.notification_summary_top_bar_text),
                    navigationIconOnClick = {
                        val intent = Intent(context, HomeActivity::class.java)
                        context.startActivity(intent)
                    },
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
        state.let { state ->
            when (state) {
                is SummaryState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .background(colors.background)
                            .fillMaxSize()
                    ) {
                        CircularProgressIndicator(color = ColorPrimary)
                    }
                }
                is SummaryState.FAILURE -> {
                    val message = state.message
                    showBanner(context, message)
                }
                is SummaryState.SUCCESS -> {
                    val weeklySummary = state.weeklySummary
                    WeeklySummarySuccessView(viewModel, weeklySummary)
                }
            }
        }
    }
}

@Composable
fun WeeklySummarySuccessView(
    viewModel: NotificationSummaryViewModel,
    summaryData: WeeklySummary
) {

    val setStartDate by viewModel.getStartDate.collectAsState()
    val setEndDate by viewModel.getEndDate.collectAsState()
    val scrollState = rememberScrollState()
    val userName =
        summaryData.firstName.ifEmpty { stringResource(R.string.notification_summary_empty_name_text) }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
            .verticalScroll(scrollState),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .padding(horizontal = 20.dp)
        ) {
            Text(
                text = "$setStartDate - $setEndDate",
                style = AppTheme.typography.subscriptionIntervalTextStyle,
                color = colors.textPrimary,
                letterSpacing = -(0.8.sp),
                lineHeight = 18.sp,
                modifier = Modifier.padding(top = 30.dp)

            )
            Text(
                text = stringResource(
                    R.string.notification_summary_header_text,
                    userName
                ),
                style = AppTheme.typography.subTextStyle2,
                color = colors.textSecondary,
                lineHeight = 18.sp,
                modifier = Modifier.padding(top = 8.dp)
            )

            Spacer(modifier = Modifier.height(40.dp))

            Text(
                text = stringResource(R.string.notification_summary_your_rank_text),
                style = AppTheme.typography.topBarTitleTextStyle,
                color = colors.textPrimary,
                letterSpacing = -(0.8.sp),
                modifier = Modifier
                    .padding(bottom = 16.dp)
            )
            SummaryRankView(
                stringResource(R.string.explore_notification_summary_weekly_text),
                summaryData.rankPoint.weeklyRank, summaryData.rankPoint.weekPoint
            )

            SummaryRankView(
                stringResource(R.string.explore_notification_summary_all_time_text),
                summaryData.rankPoint.allTimeRank, summaryData.rankPoint.allTimePoints
            )

            Spacer(modifier = Modifier.height(40.dp))

            Text(
                text = stringResource(R.string.notification_summary_activities_text),
                style = AppTheme.typography.topBarTitleTextStyle,
                color = colors.textPrimary,
                letterSpacing = -(0.8.sp),
                modifier = Modifier
                    .padding(bottom = 8.dp)
            )
            val summaryList = summaryData.SummaryList
            if (summaryList.isNotEmpty()) {
                summaryList.forEach { item ->
                    ActivitiesListView(item)
                }
            }

            Text(
                text = stringResource(R.string.notification_summary_bottom_text),
                style = AppTheme.typography.subTitleTextStyle1,
                color = colors.textSecondary,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 32.dp, bottom = 40.dp)
            )
        }
    }
}

@Composable
fun SummaryRankView(
    summaryType: String,
    totalDays: Int,
    points: Int
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(bottom = 8.dp)
            .background(
                if (isDarkMode) cardDarkBg else rankCardBg, RoundedCornerShape(12.dp)
            )
            .border(
                border = if (isDarkMode) BorderStroke(1.dp, rankCardBorder) else BorderStroke(0.dp, Color.Transparent),
                shape = RoundedCornerShape(12.dp)
            ),
        horizontalArrangement = Arrangement.SpaceBetween,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Box(
            modifier = Modifier
                .padding(vertical = 20.dp),
            contentAlignment = Alignment.CenterStart
        ) {
            Text(
                text = summaryType,
                style = AppTheme.typography.subscriptionDateTextStyle,
                color = colors.textSecondary,
                lineHeight = 18.sp,
                modifier = Modifier
                    .padding(start = 16.dp)
            )
        }
        Box(
            modifier = Modifier
                .padding(vertical = 20.dp),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = totalDays.toString(),
                style = AppTheme.typography.notificationSummaryBodyText,
                color = colors.textPrimary,
                lineHeight = 28.sp
            )
        }
        Box(
            modifier = Modifier
                .padding(vertical = 20.dp),
            contentAlignment = Alignment.CenterEnd
        ) {
            Row {
                Image(
                    painter = if (isDarkMode) painterResource(id = R.drawable.ic_points_start_icon_dark) else painterResource(id = R.drawable.ic_points_start_icon_1),
                    contentDescription = "",
                    modifier = Modifier
                        .align(Alignment.CenterVertically)
                        .padding(end = 4.dp)
                )
                Text(
                    text = points.toString(),
                    style = AppTheme.typography.notificationSummaryPointText,
                    color = ColorPrimary,
                    lineHeight = 18.sp,
                    modifier = Modifier
                        .padding(end = 16.dp)
                )
            }
        }
    }
}

@Composable
fun ActivitiesListView(
    item: SummaryDate
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp)
            .background(color = if (isDarkMode) cardDarkBg else Color.parse(item.background), RoundedCornerShape(12.dp))
            .border(
                border =
                if (isDarkMode) {
                    when {
                        item.percentage > 80 -> {
                            BorderStroke(1.dp, excellentBorder)
                        }
                        item.percentage in 50..80 -> {
                            BorderStroke(1.dp, goodBorder)
                        }
                        else -> {
                            BorderStroke(1.dp, poorBorder)
                        }
                    }
                } else {
                    BorderStroke(0.dp, Color.Transparent)
                },
                shape = RoundedCornerShape(12.dp)
            )
    ) {
        BoxWithConstraints(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp, vertical = 16.dp)
        ) {
            val setSize = if (maxWidth > 350.dp) 1.25f else 1.15f
            val setWidth = (maxWidth - 32.dp) / 3

            Row(
                modifier = Modifier
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically
            ) {
                Column(
                    horizontalAlignment = Alignment.Start,
                    modifier = Modifier.weight(1f)
                ) {
                    Text(
                        text = item.activity.name,
                        style = AppTheme.typography.titleTextStyle,
                        color = colors.textPrimary,
                        lineHeight = 22.sp,
                        maxLines = 2
                    )
                    Text(
                        text = buildAnnotatedString {
                            append("${item.completedDays}/${item.totalDays}")
                            withStyle(
                                style = SpanStyle(
                                    color = colors.textSecondary,
                                    fontSize = 14.sp,
                                )
                            ) {
                                append(stringResource(R.string.subscription_status_times_header_text))
                            }
                        },
                        color = colors.textPrimary,
                        style = AppTheme.typography.subTextStyle1,
                        modifier = Modifier
                            .padding(top = 12.dp),
                        overflow = TextOverflow.Clip
                    )
                }
                Column(
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.weight(1f)
                ) {
                    Text(
                        text = item.progress,
                        style = AppTheme.typography.subTextStyle2,
                        color = colors.textSecondary,
                        lineHeight = 18.sp,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.padding(end = 16.dp)
                    )
                }
                Column(
                    horizontalAlignment = Alignment.End,
                    modifier = Modifier
                        .width(setWidth)
                ) {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier.fillMaxSize()
                    ) {
                        val progress =
                            if (item.completedDays == 0) 0f else ((item.completedDays * 100) / item.totalDays).toFloat()
                        Text(
                            text = "${progress.roundToInt()}%",
                            color = colors.textPrimary,
                            fontFamily = InterSemiBoldFont,
                            fontSize = 14.sp,
                            lineHeight = 18.sp,
                            textAlign = TextAlign.Center
                        )
                        CircularGradientProgressView(
                            boxModifier = Modifier.size(setWidth.div(setSize)),
                            strokeWidth = 8.dp,
                            showText = false,
                            strokeBackgroundWidth = 2.dp,
                            progress = item.percentage.toFloat(),
                        )
                    }
                }
            }
        }
    }
}

private val rankCardBg = Color(0xFFE8F5F9)
private val cardDarkBg = Color(0xFF222222)
private val rankCardBorder = Color(0xFF008CB7)
private val excellentBorder = Color(0xFF47A96E)
private val goodBorder = Color(0xFFDFAA1C)
private val poorBorder = Color(0xFFF67878)
