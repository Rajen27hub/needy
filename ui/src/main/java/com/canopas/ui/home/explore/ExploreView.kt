package com.canopas.ui.home.explore

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.BottomEnd
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.base.ui.themes.ExploreTheme
import com.canopas.data.FeatureFlag.isGoalsEnabled
import com.canopas.data.model.BasicSubscription
import com.canopas.data.model.FeaturedFeed
import com.canopas.data.model.GoalWithColors
import com.canopas.ui.R
import com.canopas.ui.reviewmanager.launchInAppReview
import com.canopas.ui.reviewmanager.redirectToPlayStore
import com.google.android.gms.common.util.DeviceProperties.isTablet

@Composable
fun ExploreView(
    viewModel: ExploreViewModel,
) {
    val context = LocalContext.current

    DisposableEffect(key1 = Unit) {
        viewModel.onStart()

        onDispose {
            viewModel.onStop()
        }
    }

    val completionState by viewModel.stateCompletion.collectAsState()
    LaunchedEffect(key1 = completionState) {
        when (completionState) {
            is CompletionState.FAILURE -> {
                val message = (completionState as CompletionState.FAILURE).message
                showBanner(context, message)
                viewModel.resetCompletionState()
            }
            else -> {}
        }
    }

    val state by viewModel.state.collectAsState()

    Column {
        state.let { state ->
            when (state) {
                ExploreState.LOADING -> {
                    Box(
                        contentAlignment = Center, modifier = Modifier.fillMaxSize()
                    ) {
                        CircularProgressIndicator(color = ColorPrimary)
                    }
                }
                is ExploreState.FAILURE -> {
                    if (state.isNetworkError) {
                        NetworkErrorView {
                            viewModel.onStart()
                        }
                    } else {
                        ServerErrorView {
                            viewModel.onStart()
                        }
                    }
                }
                is ExploreState.SUCCESS -> {
                    ExploreTheme(darkTheme = isDarkMode) {
                        val subscriptions = state.subscriptions
                        val feeds = state.feeds
                        val showPerfectDayView = state.showPerfectDayView
                        val goals = state.goals
                        FeaturedExploreSuccessScreen(viewModel, subscriptions, feeds, showPerfectDayView, goals)
                    }
                }
            }
        }
    }
}

@Composable
fun FeaturedExploreSuccessScreen(
    viewModel: ExploreViewModel,
    subscriptions: List<BasicSubscription>,
    feeds: List<FeaturedFeed>,
    showPerfectDayView: Boolean,
    goals: List<GoalWithColors>
) {
    val context = LocalContext.current
    val activitiesListState = rememberLazyListState()

    val headerState by viewModel.headerState.collectAsState()
    val showRatingCard by viewModel.showAppRatingCard.collectAsState(false)
    val showNotesTipsView by viewModel.showNotesTipsView.collectAsState(false)
    val showInAppReviewDialog by viewModel.showInAppReviewDialog.collectAsState()
    val openReviewDialog = showInAppReviewDialog.getContentIfNotHandled() ?: false
    val feedsLoader by viewModel.feedsLoader.collectAsState()
    val showPageLoader = feedsLoader is LoaderState.LOADING

    LaunchedEffect(key1 = showInAppReviewDialog, block = {
        if (openReviewDialog) {
            launchInAppReview(context)
        }
    })

    LazyColumn(
        state = activitiesListState,
        contentPadding = PaddingValues(bottom = 30.dp),
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background),
    ) {
        item {
            ExploreAppBar(viewModel, subscriptions, showPerfectDayView)

            Spacer(modifier = Modifier.height(20.dp))

            NewHeaderState(viewModel = viewModel)

            if (showPerfectDayView) {
                PerfectDayView()
                Spacer(modifier = Modifier.height(24.dp))
            }

            AnimatedVisibility(
                visible = showNotesTipsView && headerState is HeaderState.TODO && subscriptions.isNotEmpty(),
                enter = fadeIn(),
                exit = fadeOut()
            ) {
                NotesTipsView(viewModel)
            }
        }

        ToDoCardView(
            viewModel,
            subscriptions,
            if (isGoalsEnabled) goals else null,
            headerState,
            showNotesTipsView,
            this,
            showPerfectDayView
        )

        item {
            if (showRatingCard) {
                AppRatingCardView(viewModel = viewModel)
            }
        }

        item {
            if (goals.isNotEmpty() && isGoalsEnabled) {
                GoalHeaderView(viewModel)
            }
        }

        item {
            if (goals.isNotEmpty() && isGoalsEnabled) {
                YourWallView(viewModel, goals)
            }
        }

        FeedsView(viewModel, feeds, subscriptions.isEmpty(), this)

        item {
            if (showPageLoader) {
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(16.dp),
                    contentAlignment = Center,
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }
        }
    }
}

@Composable
fun AppRatingCardView(viewModel: ExploreViewModel) {
    val context = LocalContext.current
    val topPadding = if (isTablet(context)) 12.dp else 4.dp
    Box(
        modifier = Modifier.fillMaxWidth(), contentAlignment = Center
    ) {
        Box(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .padding(start = 20.dp, end = 20.dp, top = 28.dp)
                .fillMaxWidth()
                .wrapContentHeight()
                .background(
                    if (isDarkMode) darkRatingCardBg else lightRatingCardBg,
                    shape = RoundedCornerShape(12.dp)
                )
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_cancel_icon),
                contentDescription = "Cancel image",
                modifier = Modifier
                    .padding(16.dp)
                    .align(Alignment.TopEnd)
                    .motionClickEvent {
                        viewModel.closeRatingCardView()
                    },
                colorFilter = ColorFilter.tint(colors.textPrimary)
            )

            Column(
                modifier = Modifier.padding(top = 16.dp, start = 12.dp)
            ) {
                Row(
                    modifier = Modifier.padding(horizontal = 12.dp),
                    verticalAlignment = CenterVertically
                ) {
                    Column(modifier = Modifier.fillMaxWidth()) {
                        Text(
                            text = stringResource(R.string.app_rating_view_title),
                            style = AppTheme.typography.todoActivityTitleTextStyle,
                            color = colors.textPrimary,
                            lineHeight = 22.sp
                        )

                        Text(
                            text = stringResource(R.string.app_rating_view_sub_title_text),
                            color = colors.textSecondary,
                            textAlign = TextAlign.Start,
                            maxLines = 3,
                            lineHeight = 21.sp,
                            overflow = TextOverflow.Ellipsis,
                            modifier = Modifier.padding(top = 8.dp),
                            style = AppTheme.typography.subTitleTextStyle1,
                        )
                    }
                }
                Box(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = topPadding, end = 20.dp, bottom = 16.dp)
                ) {
                    Button(
                        colors = ButtonDefaults.buttonColors(backgroundColor = ColorPrimary),
                        shape = RoundedCornerShape(19.dp),
                        elevation = ButtonDefaults.elevation(0.dp),
                        modifier = Modifier
                            .wrapContentWidth()
                            .height(40.dp)
                            .align(BottomEnd),
                        onClick = {
                            viewModel.redirectToPlayStore()
                            redirectToPlayStore(context)
                        }
                    ) {
                        Text(
                            text = stringResource(R.string.app_rating_view_write_a_review_btn_text),
                            textAlign = TextAlign.Center,
                            lineHeight = 17.sp,
                            color = White,
                            modifier = Modifier.padding(horizontal = 8.dp),
                            style = AppTheme.typography.buttonStyle,
                        )
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun PreviewExploreView() {
    val viewModel = hiltViewModel<ExploreViewModel>()
    ExploreView(viewModel)
}

@Preview
@Composable
fun PreviewRatingView() {
    val viewModel = hiltViewModel<ExploreViewModel>()
    AppRatingCardView(viewModel)
}

private val lightRatingCardBg = Color(0xFFF4FAF5)
private val darkRatingCardBg = Color(0xFF222222)
