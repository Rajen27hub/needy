package com.canopas.ui.home

import android.content.Intent
import android.os.Bundle
import android.view.WindowManager
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.AnimationSpec
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.RowScope
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.BottomAppBar
import androidx.compose.material.BottomNavigationItem
import androidx.compose.material.Divider
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.Scaffold
import androidx.compose.material.SwipeableDefaults
import androidx.compose.material.Text
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.currentBackStackEntryAsState
import androidx.navigation.navArgument
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.Event
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.base.ui.themes.JustlyAppTheme
import com.canopas.feature_community_data.FeatureFlag
import com.canopas.feature_community_ui.CommunityNavManager
import com.canopas.feature_community_ui.CommunityScreen
import com.canopas.feature_points_ui.PointsNavManager
import com.canopas.ui.R
import com.canopas.ui.home.settings.buypremiumsubscription.BuyPremiumSubscriptionView
import com.canopas.ui.home.settings.feedback.FeedbackView
import com.canopas.ui.home.settings.notification.NotificationView
import com.canopas.ui.home.settings.privacy.PrivacyView
import com.canopas.ui.home.settings.userprofile.UserProfileView
import com.canopas.ui.navigation.ARGUMENT_SELECTED_TAB
import com.canopas.ui.navigation.ARGUMENT_STORY_DETAIL_ID
import com.canopas.ui.navigation.MainScreen
import com.canopas.ui.navigation.NavManager
import com.canopas.ui.navigation.SettingsScreen
import com.canopas.ui.navigation.communityGraph
import com.canopas.ui.navigation.loginGraph
import com.canopas.ui.navigation.mainGraph
import com.canopas.ui.navigation.pointGraph
import com.canopas.ui.utils.FacebookUtil
import com.canopas.ui.utils.slideComposable
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.google.accompanist.navigation.material.BottomSheetNavigator
import com.google.accompanist.navigation.material.ExperimentalMaterialNavigationApi
import com.google.accompanist.navigation.material.ModalBottomSheetLayout
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

const val ARGUMENT_PREMIUM_SCREEN_SHOW_BACK_ARROW = "showBackArrow"
const val ARGUMENT_PREMIUM_SCREEN_SHOW_DISMISS_BUTTON = "showDismissButton"
const val ARGUMENT_PREMIUM_SCREEN_TEXT_TYPE = "textType"
const val ARGUMENT_PROFILE_SCREEN_SHOW_BACK_ARROW = "showBackArrow"
const val ARGUMENT_PROFILE_SCREEN_PROMPT_PREMIUM = "promptPremium"
const val ACCEPT_COMMUNITY_ID = "community_id"
const val EXTRA_COMMUNITY_FLAG = "is_community_request"

@AndroidEntryPoint
class HomeActivity : AppCompatActivity() {

    @Inject
    lateinit var navManager: NavManager

    @Inject
    lateinit var communityNavManager: CommunityNavManager

    @Inject
    lateinit var pointsNavManager: PointsNavManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val viewModel = hiltViewModel<HomeViewModel>()
            val navigateToCommunityView = intent.getBooleanExtra(EXTRA_COMMUNITY_FLAG, false)
            val communityId = intent.getStringExtra(ACCEPT_COMMUNITY_ID)
            HomeView(viewModel, navManager, communityNavManager, pointsNavManager, communityId, navigateToCommunityView)
        }
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val navigateToCommunityView = intent.getBooleanExtra(EXTRA_COMMUNITY_FLAG, false)
        val communityId = intent.getStringExtra(ACCEPT_COMMUNITY_ID)
        val viewModel = ViewModelProvider(this)[HomeViewModel::class.java]
        viewModel.onNewIntent(navigateToCommunityView, communityId)
        intent.extras?.clear()
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        FacebookUtil.callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
    }
}

@OptIn(ExperimentalAnimationApi::class, ExperimentalMaterialNavigationApi::class)
@Composable
fun HomeView(
    viewModel: HomeViewModel,
    navManager: NavManager,
    communityNavManager: CommunityNavManager,
    pointsNavManager: PointsNavManager,
    communityId: String? = null,
    navigateToCommunityView: Boolean
) {
    val isAppDarkModeEnabled by viewModel.isDarkModeEnabled.collectAsState()
    JustlyAppTheme(isDarkMode = isAppDarkModeEnabled) {
        val bottomSheetNavigator = rememberBottomSheetNavigator()
        val navController = rememberAnimatedNavController(bottomSheetNavigator)
        val systemUiController = rememberSystemUiController()
        val defaultColor = if (isAppDarkModeEnabled) darkColor else lightColor
        val isDarkMode = isDarkMode

        LaunchedEffect(key1 = isAppDarkModeEnabled) {
            systemUiController.setStatusBarColor(
                color = defaultColor,
                darkIcons = !isDarkMode
            )
            viewModel.onStart(navigateToCommunityView, communityId)
        }
        DisposableEffect(key1 = Unit) {
            navManager.setNavController(navController)

            communityNavManager.setNavController(navController)
            pointsNavManager.setNavController(navController)
            onDispose {
                navManager.removeNavController()
                communityNavManager.removeNavController()
                pointsNavManager.removeNavController()
            }
        }

        val navBackStackEntry by navController.currentBackStackEntryAsState()
        val bottomBarState = rememberSaveable { (mutableStateOf(true)) }
        val nonBottomBarRequiredRoute = viewModel.nonBottomBarRequiredRoute
        val nonBottomBarNonAdjustableRoute = viewModel.nonBottomBarNonAdjustableRoute
        val route =
            navBackStackEntry?.destination?.route?.substringBefore("?")?.substringBefore("/")
        bottomBarState.value =
            (route !in nonBottomBarRequiredRoute && route.toString() !in nonBottomBarNonAdjustableRoute)

        val contextAppCompat = LocalContext.current as AppCompatActivity

        when (route) {
            in nonBottomBarRequiredRoute -> contextAppCompat.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)
            else -> contextAppCompat.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)
        }

        ModalBottomSheetLayout(
            bottomSheetNavigator = bottomSheetNavigator,
            modifier = Modifier.fillMaxSize(),
            sheetBackgroundColor = bottomSheetBg.copy(0.97f),
            sheetElevation = 0.dp
        ) {
            Scaffold(
                bottomBar = {
                    AnimatedBottomBar(
                        viewModel,
                        navController,
                        bottomBarState = bottomBarState
                    )
                }
            ) {
                AnimatedNavHost(
                    navController,
                    startDestination = if (navigateToCommunityView || communityId != null) CommunityScreen.Root.route else MainScreen.Root.route,
                    enterTransition = { fadeIn(animationSpec = tween(400)) },
                    exitTransition = { fadeOut(animationSpec = tween(400)) },
                    modifier = Modifier.padding(it)
                ) {
                    mainGraph(navController)
                    loginGraph(navController)
                    communityGraph(navController)
                    pointGraph()

                    slideComposable(
                        SettingsScreen.UserProfile.route + "?$ARGUMENT_PROFILE_SCREEN_SHOW_BACK_ARROW={$ARGUMENT_PROFILE_SCREEN_SHOW_BACK_ARROW}" + "?$ARGUMENT_PROFILE_SCREEN_PROMPT_PREMIUM={$ARGUMENT_PROFILE_SCREEN_PROMPT_PREMIUM}",
                        arguments = listOf(
                            navArgument(ARGUMENT_PROFILE_SCREEN_SHOW_BACK_ARROW) {
                                type = NavType.BoolType
                            },
                            navArgument(ARGUMENT_PROFILE_SCREEN_PROMPT_PREMIUM) {
                                type = NavType.BoolType
                            }
                        )
                    ) { backStack ->
                        val showBackArrow = backStack.arguments?.getBoolean(
                            ARGUMENT_PROFILE_SCREEN_SHOW_BACK_ARROW
                        ) ?: true
                        val promptPremium = backStack.arguments?.getBoolean(
                            ARGUMENT_PROFILE_SCREEN_PROMPT_PREMIUM
                        ) ?: false

                        UserProfileView(showBackArrow, promptPremium)
                    }

                    slideComposable(
                        SettingsScreen.Feedback.route + "?storyDetailId={storyDetailId}" + "?selectedTab={selectedTab}",
                        arguments = listOf(
                            navArgument(ARGUMENT_SELECTED_TAB) {
                                type = NavType.IntType
                            }
                        )
                    ) { navBackStack ->
                        val storyId = navBackStack.arguments?.getString(ARGUMENT_STORY_DETAIL_ID)
                            ?: ""
                        val selectedTab = navBackStack.arguments?.getInt(ARGUMENT_SELECTED_TAB) ?: 0
                        FeedbackView(storyId = storyId, selectedTab = selectedTab)
                    }

                    slideComposable(SettingsScreen.Privacy.route) {
                        PrivacyView()
                    }
                    slideComposable(SettingsScreen.Privacy.route) {
                        PrivacyView()
                    }
                    slideComposable(SettingsScreen.Notifications.route) {
                        NotificationView()
                    }

                    slideComposable(
                        SettingsScreen.BuyPremium.route + "?showBackArrow={showBackArrow}" + "?showDismissButton={showDismissButton}" + "?textType={textType}",
                        arguments = listOf(
                            navArgument(ARGUMENT_PREMIUM_SCREEN_SHOW_BACK_ARROW) {
                                type = NavType.BoolType
                            },
                            navArgument(ARGUMENT_PREMIUM_SCREEN_SHOW_DISMISS_BUTTON) {
                                type = NavType.BoolType
                            },
                            navArgument(ARGUMENT_PREMIUM_SCREEN_TEXT_TYPE) {
                                type = NavType.IntType
                            }
                        )
                    ) { backStack ->
                        val showBackArrow = backStack.arguments?.getBoolean(
                            ARGUMENT_PREMIUM_SCREEN_SHOW_BACK_ARROW
                        ) ?: true
                        val showDismissButton = backStack.arguments?.getBoolean(
                            ARGUMENT_PREMIUM_SCREEN_SHOW_DISMISS_BUTTON
                        ) ?: false
                        val headerTextType = backStack.arguments?.getInt(
                            ARGUMENT_PREMIUM_SCREEN_TEXT_TYPE
                        ) ?: 0
                        BuyPremiumSubscriptionView(
                            showBackArrow,
                            showDismissButton,
                            headerTextType
                        )
                    }
                }
            }
        }
    }
}

@ExperimentalMaterialNavigationApi
@OptIn(ExperimentalMaterialApi::class)
@Composable
fun rememberBottomSheetNavigator(
    animationSpec: AnimationSpec<Float> = SwipeableDefaults.AnimationSpec,
    skipHalfExpanded: Boolean = true,
): BottomSheetNavigator {
    val sheetState = rememberModalBottomSheetState(
        ModalBottomSheetValue.Hidden,
        animationSpec,
        skipHalfExpanded,
    )
    return remember(sheetState) {
        BottomSheetNavigator(sheetState = sheetState)
    }
}

@Composable
fun AnimatedBottomBar(
    viewModel: HomeViewModel,
    navController: NavHostController,
    bottomBarState: MutableState<Boolean>
) {
    val fabShape = RoundedCornerShape(50)

    val currentRoute = viewModel.currentRoute
    val navigateTo by viewModel.navigateTo.collectAsState(Event(""))
    val route = navigateTo.getContentIfNotHandled() ?: ""
    val isDarkMode by viewModel.isDarkModeEnabled.collectAsState()

    if (route.isNotEmpty()) {
        navController.navigate(route) {
            navController.graph.startDestinationRoute?.let { route ->
                popUpTo(route) {
                    saveState = true
                }
            }
            launchSingleTop = true
            restoreState = true
        }
    }

    if (bottomBarState.value) {
        val modifier = if (isDarkMode) {
            Modifier
                .background(lightColor.copy(0.2f))
        } else {
            Modifier
                .background(colors.textPrimary.copy(0.2f))
        }
        Column {
            val dividerColor = if (isDarkMode) colors.textPrimary.copy(0.2f) else darkColor.copy(0.2f)
            Divider(
                color = dividerColor,
                thickness = (1.5).dp
            )
            BottomAppBar(
                modifier = modifier,
                cutoutShape = fabShape,
                elevation = 0.dp,
                contentColor = ColorPrimary,
                backgroundColor = if (isDarkMode) darkColor else lightColor
            ) {
                NavItem(
                    MainScreen.Explore,
                    currentRoute == MainScreen.Explore.route
                )
                NavItem(
                    MainScreen.Current,
                    currentRoute == MainScreen.Current.route
                )

                if (FeatureFlag.isCommunityEnabled) {
                    NavItem(
                        MainScreen.Community,
                        currentRoute == CommunityScreen.Community.route
                    )
                }
            }
        }
    }
}

@Composable
fun RowScope.NavItem(
    screen: MainScreen,
    isSelected: Boolean
) {
    val viewModel = hiltViewModel<HomeViewModel>()
    val isDarkMode by viewModel.isDarkModeEnabled.collectAsState()
    val label = when (screen.route) {
        MainScreen.Explore.route -> stringResource(R.string.home_activity_home_label_text)
        MainScreen.Current.route -> stringResource(R.string.home_activity_activities_label_text)
        MainScreen.Community.route -> stringResource(R.string.home_activity_communities_label_text)
        else -> stringResource(R.string.home_activity_settings_label_text)
    }
    BottomNavigationItem(
        icon = {
            Icon(
                painter = painterResource(id = screen.resourceId),
                null,
                modifier = Modifier
                    .size(28.dp)
                    .padding(4.dp),
            )
        },
        selected = isSelected,
        selectedContentColor = ColorPrimary,
        alwaysShowLabel = true,
        onClick = {
            viewModel.updateRoute(screen.route)
            viewModel.navigateTo(screen.route)
        },
        label = {
            Text(
                text = label,
                style = AppTheme.typography.bottomBarLabelTextStyle,
                lineHeight = 14.sp,
                maxLines = 1,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center,
                overflow = TextOverflow.Ellipsis
            )
        },
        unselectedContentColor = if (isDarkMode) darkContentColor else lightContentColor
    )
}

private val bottomSheetBg = Color(0xFFEEEEEE)
private val lightColor = Color(0xffffffff)
private val darkColor = Color(0xff121212)
private val lightContentColor = Color(0x801C191F)
private val darkContentColor = Color(0x80FFFFFF)
