package com.canopas.ui.home.explore.storyView

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.data.FeatureFlag.isGoalsEnabled
import com.canopas.data.model.Story
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.home.settings.buypremiumsubscription.ACTIVITY_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

const val MAX_SUBSCRIPTIONS_FOR_FREE_USER = 3

@HiltViewModel
class StoryViewModel @Inject constructor(
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val preferencesDataStore: NoLonelyPreferences,
    private val authManager: AuthManager,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<StoryState>(StoryState.LOADING)
    val activityState = MutableStateFlow<ActivityState>(ActivityState.START)

    fun getStoryById(activityId: Int) = viewModelScope.launch {
        state.value = StoryState.LOADING
        withContext(appDispatcher.IO) {
            service.getStoryById(activityId).onSuccess { story ->
                state.value = StoryState.SUCCESS(story)
            }.onFailure { e ->
                Timber.e(e)
                state.value = StoryState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    fun getActivityTimeAndDuration(activityId: Int, activityName: String, storyId: Int) =
        viewModelScope.launch {
            activityState.tryEmit(ActivityState.LOADING)
            withContext(appDispatcher.MAIN) {
                service.getActivityById(activityId).onSuccess { data ->
                    val time = data.default_activity_time ?: "8:00"
                    val duration = data.default_activity_duration ?: 1
                    handleUserSubscriptionLimits(
                        activityId, activityName, storyId,
                        time, duration
                    )
                    activityState.value = ActivityState.FINISH
                }.onFailure { e ->
                    Timber.e(e)
                    activityState.tryEmit(ActivityState.FAILURE(e.toUserError(resources)))
                }
            }
        }

    fun navigateToSignInMethods() {
        navManager.navigateToSignInMethods()
    }

    fun navigateToCustomActivityView(storyId: Int) {
        appAnalytics.logEvent("tap_story_view_own_activity", null)
        navManager.navigateToCustomActivityView(storyId)
    }

    private fun handleUserSubscriptionLimits(
        activityId: Int,
        activityName: String,
        storyId: Int,
        activityTime: String,
        activityDuration: Int,
    ) = viewModelScope.launch {
        appAnalytics.logEvent("tap_story_screen_add_btn")

        when {
            !authManager.authState.isVerified -> {
                navigateToSignInMethods()
            }
            (authManager.authState.isFree && preferencesDataStore.getUserSubscriptionCount() >= MAX_SUBSCRIPTIONS_FOR_FREE_USER) -> {
                openBuyPremiumScreen()
            }
            else -> {
                if (isGoalsEnabled) {
                    navigateToWhyThisActivity(
                        activityId, activityName,
                        storyId, activityTime,
                        activityDuration
                    )
                } else {
                    navigateToConfigureActivity(
                        activityId, activityName,
                        storyId, activityTime,
                        activityDuration
                    )
                }
            }
        }
    }

    fun openBuyPremiumScreen() {
        navManager.navigateToBuyPremiumScreen(textType = ACTIVITY_LIMIT_TEXT_TYPE)
    }

    fun navigateToWhyThisActivity(
        activityId: Int,
        activityName: String,
        storyId: Int,
        activityTime: String,
        activityDuration: Int,
    ) {
        navManager.navigateToWhyThisActivityScreen(activityId, activityName, activityTime, activityDuration, storyId)
    }

    fun navigateToConfigureActivity(
        activityId: Int,
        activityName: String,
        storyId: Int,
        activityTime: String,
        activityDuration: Int,
    ) {
        navManager.navigateToConfigureActivity(activityId, activityName, activityTime, activityDuration, storyId)
    }

    fun onStart(storyId: String) {
        appAnalytics.logEvent("view_story_screen", null)
        getStoryById(storyId.toInt())
    }

    fun resetActivityState() {
        activityState.value = ActivityState.START
    }

    fun popBack() {
        navManager.popBack()
    }
}

sealed class StoryState {
    object LOADING : StoryState()
    data class SUCCESS(val story: Story) : StoryState()
    data class FAILURE(val message: String, val isNetworkError: Boolean) : StoryState()
}

sealed class ActivityState {
    object START : ActivityState()
    object LOADING : ActivityState()
    object FINISH : ActivityState()
    data class FAILURE(val message: String) : ActivityState()
}
