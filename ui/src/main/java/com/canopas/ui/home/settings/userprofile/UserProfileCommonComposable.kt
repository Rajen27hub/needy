package com.canopas.ui.home.settings.userprofile

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.DividerLineView
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.ui.R

@Composable
fun ShowDeleteDialog(viewModel: UserProfileViewModel) {
    CustomAlertDialog(
        title = stringResource(id = R.string.profile_screen_alert_dialog_title_text),
        subTitle = stringResource(id = R.string.profile_screen_delete_alert_dialog),
        confirmBtnText = stringResource(id = R.string.profile_screen_alert_dialog_confirm_button_text),
        dismissBtnText = stringResource(id = R.string.profile_screen_alert_dialog_dismiss_button_text),
        onConfirmClick = { viewModel.deleteUser() },
        onDismissClick = { viewModel.closeDialog() },
        confirmBtnColor = profileDeleteBtn,
        backgroundColor = if (ComposeTheme.isDarkMode) darkAlertDialogBg else ComposeTheme.colors.background
    )
}

@Composable
fun ErrorMessage() {
    Text(
        text = stringResource(R.string.profile_screen_required_characters_error_text),
        color = MaterialTheme.colors.error,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        textAlign = TextAlign.Start
    )
}

@Composable
fun UserTextField(label: String, text: String, onValueChange: (value: String) -> Unit) {
    TextField(
        value = text,
        modifier = Modifier.fillMaxWidth(),
        onValueChange = {
            onValueChange(it)
        },
        label = {
            Text(
                text = label,
                style = AppTheme.typography.bodyTextStyle2,
                color = if (ComposeTheme.isDarkMode) darkHeaderText else lightHeaderText
            )
        },
        singleLine = true,
        textStyle = AppTheme.typography.subTextStyle1.copy(color = if (ComposeTheme.isDarkMode) darkTextField else lightTextField),
        keyboardOptions = KeyboardOptions(
            keyboardType = KeyboardType.Text,
            imeAction = ImeAction.Next
        ),
        colors = TextFieldDefaults.textFieldColors(
            backgroundColor = Color.Transparent,
            focusedIndicatorColor = Color.Transparent,
            unfocusedIndicatorColor = Color.Transparent,
            disabledIndicatorColor = Color.Transparent,
            cursorColor = ComposeTheme.colors.textPrimary.copy(0.5f)
        )
    )
    DividerLineView(if (ComposeTheme.isDarkMode) darkDivider else lightDivider)
}

private val darkAlertDialogBg = Color(0xff222222)
private val profileDeleteBtn = Color(0xFFE13333)
private val lightHeaderText = Color(0x66000000)
private val darkHeaderText = Color(0x66FFFFFF)
private val darkTextField = Color(0xDEFFFFFF)
private val lightTextField = Color(0xDE000000)
private val lightDivider = Color(0x1F000000)
private val darkDivider = Color(0xFF313131)
