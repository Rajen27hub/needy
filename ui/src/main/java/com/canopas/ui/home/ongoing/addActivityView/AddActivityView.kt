package com.canopas.ui.home.ongoing.addActivityView

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.sharp.Cancel
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.ImageLoader
import coil.compose.rememberAsyncImagePainter
import coil.decode.SvgDecoder
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomTextField
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.parse
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.ActivityData
import com.canopas.data.model.Category
import com.canopas.ui.R
import com.canopas.ui.utils.AutoScrollingRow
import com.google.android.gms.common.util.DeviceProperties.isTablet

private const val ACTIVITY_ANGLE_OFFSET = 7.59F
private const val TABLET_CARD_SIZE = 200
private const val PHONE_CARD_SIZE = 100
private const val TABLET_IMAGE_SIZE = 130
private const val PHONE_IMAGE_SIZE = 90
private const val ACTIVITY_CARD_COLUMN_COUNT = 4
private const val ACTIVITY_CARD_ROW_COUNT = 2

@Composable
fun AddActivityView() {

    val viewModel = hiltViewModel<AddActivityViewModel>()
    val state by viewModel.state.collectAsState()
    val context = LocalContext.current
    val listState = rememberLazyListState()

    val imageLoader = remember {
        ImageLoader.Builder(context).components {
            add(SvgDecoder.Factory())
        }.build()
    }

    LaunchedEffect(Unit) {
        viewModel.onStart()
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
    ) {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(R.string.add_activity_tap_bar_text),
                    navigationIconOnClick = { viewModel.onBackClick() }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
        state.let { state ->
            when (state) {
                is CategoryState.LOADING -> {
                    Box(
                        contentAlignment = Center,
                        modifier = Modifier.fillMaxSize(),
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is CategoryState.FAILURE -> {
                    val message = state.message
                    showBanner(context, message)
                }
                is CategoryState.SUCCESS -> {
                    val categoryItem = state.category
                    BuildAddActivityView(viewModel, categoryItem, imageLoader, listState)
                }
            }
        }
    }
}

@Composable
fun BuildAddActivityView(
    viewModel: AddActivityViewModel,
    categoryItem: List<Category>,
    imageLoader: ImageLoader,
    listState: LazyListState,
) {
    val activityData by viewModel.searchResult.collectAsState()
    val resultState by viewModel.searchState.collectAsState()
    val scrollState = rememberScrollState()

    Column(
        Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        SearchBarView(viewModel = viewModel)

        resultState.let { state ->
            when (state) {
                SearchState.START -> {
                    LazyColumn(
                        state = listState,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(colors.background),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        itemsIndexed(categoryItem) { index, category ->
                            BuildCategoryView(viewModel = viewModel, category, index, imageLoader)
                        }
                        item {
                            CustomActivityCard(viewModel = viewModel)
                        }
                    }
                }
                SearchState.SUCCESS -> {
                    SearchActivityResult(
                        activities = activityData,
                        viewModel = viewModel,
                        scrollState
                    )
                }
                SearchState.EMPTY -> {
                    EmptySearchResultView(viewModel = viewModel)
                }
            }
        }
    }
}

@Composable
fun BuildCategoryView(
    viewModel: AddActivityViewModel,
    category: Category,
    index: Int,
    imageLoader: ImageLoader,
) {
    val activities = remember {
        category.activities.toMutableList().apply {
            while (size < ACTIVITY_CARD_COLUMN_COUNT * ACTIVITY_CARD_ROW_COUNT) {
                addAll(category.activities)
            }
        }.subList(0, ACTIVITY_CARD_COLUMN_COUNT * ACTIVITY_CARD_ROW_COUNT)
    }

    val firstRowList = remember {
        activities.filterIndexed { index, _ ->
            index % 2 == 0
        }
    }

    val secRowList = remember {
        activities.filterIndexed { index, _ ->
            index % 2 == 1
        }
    }

    Box(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(
                start = 16.dp,
                end = 16.dp,
                bottom = 20.dp,
                top = if (index == 0) 24.dp else 0.dp
            )
            .motionClickEvent { viewModel.navigateToActivityList(category.id) },
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .clip(RoundedCornerShape(16.dp))
                .background(
                    if (isDarkMode) darkCategoryBgColor else lightCategoryBgColor,
                    RoundedCornerShape(16.dp)
                )
        ) {
            Text(
                text = category.name.uppercase(),
                style = AppTheme.typography.subTitleTextStyle1,
                color = colors.textSecondary,
                modifier = Modifier.padding(start = 16.dp, top = 16.dp)
            )
            Text(
                text = category.description,
                style = AppTheme.typography.h2TextStyle,
                letterSpacing = -(0.96.sp),
                lineHeight = 28.sp,
                color = colors.textPrimary,
                modifier = Modifier.padding(start = 16.dp, end = 16.dp, bottom = 20.dp)
            )
            val cardSize =
                if (isTablet(LocalContext.current)) TABLET_CARD_SIZE.dp else PHONE_CARD_SIZE.dp

            val totalWidth =
                (cardSize + 16.dp).value * LocalDensity.current.density * ACTIVITY_CARD_COLUMN_COUNT

            AutoScrollingRow(width = totalWidth, cardSize = cardSize.value, index = index) {
                Column {
                    Row {
                        firstRowList.forEach {
                            UpdateCategoryCard(activity = it, cardSize, imageLoader)
                        }
                    }
                    Spacer(modifier = Modifier.padding(8.dp))
                    Row {
                        secRowList.forEach {
                            UpdateCategoryCard(activity = it, cardSize, imageLoader)
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun UpdateCategoryCard(activity: ActivityData, cardSize: Dp, imageLoader: ImageLoader) {
    val imageSize =
        if (isTablet(LocalContext.current)) TABLET_IMAGE_SIZE.dp else PHONE_IMAGE_SIZE.dp
    Box(
        modifier = Modifier
            .padding(horizontal = 8.dp)
            .rotate(-ACTIVITY_ANGLE_OFFSET)
            .size(cardSize)
            .clip(RoundedCornerShape(10.dp))
            .background(
                Color.parse(if (activity.color2.isEmpty()) activity.color.trim() else activity.color2.trim()),
                RoundedCornerShape(10.dp)
            ),
        contentAlignment = Center
    ) {
        activity.image_url2?.let {
            val image =
                rememberAsyncImagePainter(model = it.ifEmpty { activity.image_url }, imageLoader)
            Image(
                painter = image, contentDescription = null,
                modifier = Modifier
                    .padding(8.dp)
                    .size(imageSize)
            )
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SearchBarView(viewModel: AddActivityViewModel) {

    val searchValue by viewModel.searchString.collectAsState()
    val focusManager = LocalFocusManager.current
    val interactionSource = remember { MutableInteractionSource() }

    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(start = 16.dp, end = 16.dp)
    ) {
        CustomTextField(
            text = searchValue,
            onTextChange = {
                viewModel.onSearchValueChange(it.trimStart())
            },
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .clip(RoundedCornerShape(24.dp))
                .border(1.dp, color = colors.textSecondary, shape = RoundedCornerShape(24.dp)),
            textStyle = AppTheme.typography.configureSubItemsStyle.copy(color = colors.textPrimary),
            keyboardActions = KeyboardActions(onSearch = { focusManager.clearFocus() }),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Search
            ),
            cursorBrush = SolidColor(colors.primary.copy(0.6f)),
            decorationBox = { innerTextField ->
                TextFieldDefaults.TextFieldDecorationBox(
                    value = searchValue,
                    innerTextField = innerTextField,
                    enabled = true,
                    singleLine = true,
                    visualTransformation = VisualTransformation.None,
                    interactionSource = interactionSource,
                    placeholder = {
                        Text(
                            text = stringResource(R.string.add_activity_empty_search_text),
                            color = colors.textSecondary
                        )
                    },
                    leadingIcon = {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_add_activity_search_icon),
                            contentDescription = null,
                            tint = if (searchValue.isEmpty()) colors.textSecondary else colors.textPrimary,
                            modifier = Modifier.padding(start = 16.dp, end = 12.dp)
                        )
                    },
                    trailingIcon = {
                        if (searchValue.isNotEmpty()) {
                            IconButton(onClick = {
                                viewModel.onSearchValueChange("")
                            }) {
                                Icon(
                                    Icons.Sharp.Cancel,
                                    contentDescription = null,
                                    tint = colors.textPrimary
                                )
                            }
                        }
                    },
                    contentPadding = PaddingValues(vertical = 12.dp)
                )
            }
        )
    }
}

@Composable
fun EmptySearchResultView(viewModel: AddActivityViewModel) {
    val searchValue by viewModel.searchString.collectAsState()
    Box(
        Modifier
            .widthIn(max = 600.dp)
            .fillMaxSize()
    ) {
        Text(
            text = stringResource(R.string.add_activity_empty_text, searchValue),
            style = AppTheme.typography.subTitleTextStyle1,
            color = colors.textPrimary,
            modifier = Modifier.padding(start = 16.dp, end = 16.dp, top = 24.dp)
        )
        Row(
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center,
            modifier = Modifier.fillMaxSize()
        ) {
            CustomActivityCard(viewModel = viewModel)
        }
    }
}

@Composable
fun CustomActivityCard(viewModel: AddActivityViewModel) {
    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(top = 20.dp, start = 16.dp, end = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {

        Text(
            text = stringResource(R.string.explore_screen_custom_activity_message_text),
            style = AppTheme.typography.subTitleTextStyle1,
            color = colors.textPrimary,
            lineHeight = 18.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
        )

        Button(
            onClick = { viewModel.navigateToCustomActivity() },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = ColorPrimary, contentColor = White
            ),
            elevation = null,
            modifier = Modifier
                .fillMaxWidth()
                .height(48.dp)
                .motionClickEvent { }
        ) {
            Text(
                text = stringResource(R.string.explore_screen_try_creating_custom_activity_btn_text),
                color = White,
                style = AppTheme.typography.buttonStyle
            )
        }
        Spacer(modifier = Modifier.height(26.dp))
    }
}

private val lightCategoryBgColor = Color(0xFFFDF0EE)
private val darkCategoryBgColor = Color(0xFF222222)
