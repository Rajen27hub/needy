package com.canopas.ui.home.explore

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextDecoration
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.InterMediumFont
import com.canopas.base.ui.InterSemiBoldFont
import com.canopas.base.ui.combinedMotionClickEvent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.parse
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.BasicSubscription
import com.canopas.data.model.GoalWithColors
import com.canopas.ui.R
import kotlin.math.max
import kotlin.math.min

@Composable
fun ToDoCardItem(
    viewModel: ExploreViewModel,
    subscription: BasicSubscription,
    nextSubscription: BasicSubscription?,
    onToDoCompleted: () -> Unit,
    onToDoClicked: () -> Unit,
    onTodoLongClicked: () -> Unit,
    index: Int,
    showNotesTips: Boolean,
    headerState: HeaderState,
    subscriptionGoal: GoalWithColors?,
    previousSubscription: BasicSubscription?,
) {
    val stateCompletion by viewModel.stateCompletion.collectAsState()
    val completionInProgressId = stateCompletion.let {
        if (it is CompletionState.COMPLETING) it.subscriptionId else 0
    }

    val isCompleted = subscription.activity.is_completed
    val activityColor = Color.parse(subscription.activity.color2.trim())

    val timeProgress = if (subscription.activity_time == null) {
        if (subscription.activity.is_completed) 1f else 0f
    } else {
        val start = timeStringToCurrentMillis(subscription.activity_time) ?: 0
        val duration = max(1, subscription.activity_duration ?: 1)
        val weight = (System.currentTimeMillis() - start).toFloat() / (duration * 60000)
        min(1f, max(0f, weight))
    }

    val previousActivityTimeProgress = if (previousSubscription?.activity_time == null) {
        if (previousSubscription?.activity?.is_completed == true) 1f else 0f
    } else {
        val start = timeStringToCurrentMillis(previousSubscription.activity_time) ?: 0
        val duration = max(1, previousSubscription.activity_duration ?: 1)
        val weight = (System.currentTimeMillis() - start).toFloat() / (duration * 60000)
        min(1f, max(0f, weight))
    }

    Column(modifier = Modifier.fillMaxWidth(), horizontalAlignment = Alignment.CenterHorizontally) {
        Box(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .combinedMotionClickEvent(onToDoClicked, onTodoLongClicked),
            contentAlignment = Alignment.Center,
        ) {
            Row(
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .fillMaxWidth()
                    .height(IntrinsicSize.Min)
                    .padding(
                        start = 16.dp, end = 8.dp
                    ),
                verticalAlignment = Alignment.CenterVertically
            ) {

                Box(
                    modifier = Modifier
                        .fillMaxHeight()
                        .width(48.dp)
                        .heightIn(min = 80.dp)
                        .clip(RoundedCornerShape(20.dp))
                        .border(
                            1.5.dp,
                            activityColor,
                            RoundedCornerShape(20.dp)
                        ),
                    contentAlignment = Alignment.Center
                ) {
                    Column(modifier = Modifier.fillMaxSize()) {
                        if (timeProgress > 0) {
                            Box(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .background(
                                        brush = Brush.verticalGradient(
                                            colors = listOf(
                                                activityColor,
                                                activityColor,
                                                if (timeProgress < 1f) Color.Transparent else activityColor
                                            )
                                        )
                                    )
                                    .weight(timeProgress)
                            )
                        }
                        if (timeProgress < 1) {
                            Box(
                                modifier = Modifier
                                    .weight(1 - timeProgress)
                            )
                        }
                    }
                    AnimatedWithIconAndImage(subscription = subscription, timeProgress)
                }

                Spacer(modifier = Modifier.width(12.dp))

                Column(
                    modifier = Modifier
                        .padding(vertical = 8.dp)
                        .weight(1f)
                        .alpha(if (isCompleted) 0.7f else 1f)
                ) {
                    Row(verticalAlignment = Alignment.Bottom) {
                        Text(
                            text = buildAnnotatedString {
                                append(subscription.activity.name.trim())
                                subscription.activity_duration?.let { duration ->
                                    if (duration > 1) {
                                        withStyle(
                                            style = SpanStyle(
                                                fontFamily = InterMediumFont,
                                                color = ComposeTheme.colors.textSecondary,
                                                fontSize = 14.sp
                                            )
                                        ) {
                                            append(
                                                " " + stringResource(
                                                    R.string.explore_duration_text,
                                                    duration
                                                )
                                            )
                                        }
                                    }
                                }
                            },
                            style = if (isCompleted) TextStyle(
                                fontFamily = InterSemiBoldFont,
                                fontWeight = FontWeight.SemiBold,
                                fontSize = 18.sp,
                                textDecoration = TextDecoration.LineThrough
                            ) else AppTheme.typography.todoActivityTitleTextStyle,
                            letterSpacing = -(0.72.sp),
                            color = if (isCompleted) ComposeTheme.colors.textSecondary else ComposeTheme.colors.textPrimary,
                        )
                    }

                    Spacer(modifier = Modifier.height(4.dp))

                    subscription.activity.tip?.let { tip ->
                        Text(
                            text = tip,
                            style = AppTheme.typography.todoActivitySubtitleTextStyle,
                            color = ComposeTheme.colors.textSecondary,
                        )
                    }
                }

                Spacer(modifier = Modifier.width(8.dp))

                val deadClick = {}
                Box(
                    modifier = Modifier
                        .wrapContentSize()
                        .motionClickEvent(if (subscription.id != completionInProgressId && !subscription.activity.is_completed) onToDoCompleted else deadClick)
                ) {
                    if (completionInProgressId == subscription.id) {
                        CircularProgressIndicator(
                            color = ColorPrimary,
                            modifier = Modifier
                                .padding(8.dp)
                                .size(30.dp)
                        )
                    } else {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_check_circle),
                            contentDescription = "",
                            modifier = Modifier
                                .padding(8.dp)
                                .size(30.dp),
                            tint = if (subscription.activity.is_completed) ComposeTheme.colors.primary else if (ComposeTheme.isDarkMode) darkTodoRemainingCheckColor else lightTodoRemainingCheckColor
                        )
                    }
                }
            }

            if (index == 0 && showNotesTips && headerState is HeaderState.TODO) {
                Image(
                    painterResource(id = R.drawable.ic_click_hand_icon),
                    contentDescription = "",
                    modifier = Modifier
                        .align(Alignment.TopCenter)
                        .offset(x = 0.dp, y = (-20).dp)
                )
            }
        }

        if (nextSubscription != null) {
            Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                val activityTime = subscription.activityTimeOrNull()
                val activityTimeNext = nextSubscription.activityTimeOrNull()
                Row(
                    modifier = Modifier
                        .widthIn(max = 600.dp)
                        .fillMaxWidth()
                        .height(IntrinsicSize.Min),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.Start
                ) {
                    if (activityTime == null || activityTimeNext == null) {
                        Column(
                            modifier = Modifier
                                .padding(start = 38.dp)
                                .width(4.dp)
                                .heightIn(min = 25.dp)
                                .fillMaxHeight()
                                .background(
                                    brush = Brush.verticalGradient(
                                        colors = listOf(
                                            activityColor,
                                            Color.parse(nextSubscription.activity.color2)
                                        )
                                    ),
                                    shape = DottedShape(step = 8.dp)
                                )
                        ) {}
                    } else {
                        val pipeProgress = kotlin.run {
                            val start = timeStringToCurrentMillis(activityTime) ?: 0
                            val end = timeStringToCurrentMillis(activityTimeNext) ?: 0
                            val duration = max(1, end - start)
                            val weight = (System.currentTimeMillis() - start).toFloat() / (duration)
                            min(1f, max(0f, weight))
                        }

                        Column(
                            modifier = Modifier
                                .padding(start = 37.dp)
                                .width(6.dp)
                                .heightIn(min = 25.dp)
                                .fillMaxHeight()
                                .background(
                                    brush = Brush.verticalGradient(
                                        colors = listOf(
                                            activityColor,
                                            Color.parse(nextSubscription.activity.color2)
                                        )
                                    ),
                                )
                                .padding(horizontal = 1.5.dp)
                        ) {
                            if (pipeProgress > 0) {
                                Box(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .weight(pipeProgress)
                                )
                            }
                            if (pipeProgress < 1) {
                                Box(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .background(ComposeTheme.colors.background)
                                        .weight(1 - pipeProgress)
                                )
                            }
                        }
                    }
                    Column(
                        modifier = Modifier
                            .weight(0.7f)
                            .fillMaxWidth()
                            .padding(start = 32.dp, top = 8.dp, bottom = 16.dp)
                    ) {
                        if (
                            subscriptionGoal != null &&
                            ((previousActivityTimeProgress> 0) || previousSubscription == null) &&
                            !subscription.activity.is_completed
                        ) {
                            TodoGoalCardView(subscriptionGoal, subscription, activityTime)
                        }
                    }
                    Spacer(modifier = Modifier.weight(0.2f))
                }
            }
        } else {
            Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                val activityTime = subscription.activityTimeOrNull()
                Row(modifier = Modifier.widthIn(max = 600.dp).fillMaxWidth(), horizontalArrangement = Arrangement.Start) {
                    Spacer(modifier = Modifier.padding(start = 37.dp).width(6.dp))
                    Column(Modifier.weight(0.7f).padding(start = 32.dp, top = 8.dp)) {
                        if (
                            subscriptionGoal != null &&
                            ((previousActivityTimeProgress> 0) || previousSubscription == null) &&
                            !subscription.activity.is_completed
                        ) {
                            TodoGoalCardView(subscriptionGoal, subscription, activityTime)
                        }
                    }
                    Spacer(modifier = Modifier.weight(0.2f))
                }
            }
        }
    }
}

private val lightTodoRemainingCheckColor = Color(0xFFE2D8D2)
private val darkTodoRemainingCheckColor = Color(0xFF493D36)
