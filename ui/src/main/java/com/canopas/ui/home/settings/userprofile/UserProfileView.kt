package com.canopas.ui.home.settings.userprofile

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.RadioButton
import androidx.compose.material.RadioButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.material.ripple.rememberRipple
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.model.GENDER_FEMALE
import com.canopas.base.data.model.GENDER_MALE
import com.canopas.base.data.model.LOGIN_TYPE_PHONE
import com.canopas.base.data.model.UserAccount
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.BackArrowTint
import com.canopas.base.ui.DividerLineView
import com.canopas.base.ui.ThinDividerLineView
import com.canopas.base.ui.customview.SecondaryOutlineButton
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.ui.R

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun UserProfileView(showBackArrow: Boolean = true, promptPremium: Boolean = false) {

    val viewModel = hiltViewModel<UserProfileViewModel>()
    val enabledSaveBtnState by viewModel.enableSaveButton.collectAsState()
    val showBackArrowValue by viewModel.shouldShowBackArrow.collectAsState()

    val userState by viewModel.userState.collectAsState()

    val context = LocalContext.current

    LaunchedEffect(key1 = Unit) {
        viewModel.onStart(showBackArrow, promptPremium)
    }

    val keyboardController = LocalSoftwareKeyboardController.current
    DisposableEffect(key1 = Unit, effect = {
        onDispose {
            keyboardController?.hide()
        }
    })

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
    ) {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(id = R.string.profile_screen_title_text),
                    navigationIconOnClick = { viewModel.managePopBack() },
                    enableNavigationIcon = showBackArrowValue,
                    actions = {
                        Text(
                            text = stringResource(id = R.string.profile_screen_save_text),
                            color = if (enabledSaveBtnState) BackArrowTint else Color.Gray,
                            style = AppTheme.typography.topBarButtonTextStyle,
                            modifier = Modifier
                                .padding(end = 8.dp)
                                .clickable(
                                    interactionSource = MutableInteractionSource(),
                                    indication = rememberRipple(bounded = false),
                                    enabled = enabledSaveBtnState,
                                    onClick = {
                                        viewModel.onSaveBtnClick()
                                    },
                                )
                        )
                    }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )

        userState.let { userState ->
            when (userState) {
                UserState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(colors.background)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }

                is UserState.ERROR -> {
                    val message = userState.message
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                    }
                }

                is UserState.SUCCESS -> {
                    val user = userState.userData
                    UserProfileScreen(viewModel, user)
                }
            }
        }
    }
}

@Composable
fun UserProfileScreen(
    viewModel: UserProfileViewModel,
    user: UserAccount,
) {
    val isEmailInvalid = !user.email.isNullOrEmpty() && !user.hasValidEmail()
    val showBackArrowValue by viewModel.shouldShowBackArrow.collectAsState()
    val showFirstNameErrorState by viewModel.showFirstNameErrorState.collectAsState()
    val showLastNameErrorState by viewModel.showLastNameErrorState.collectAsState()
    val showUserNameErrorState by viewModel.showUserNameErrorState.collectAsState()
    val scrollState = rememberScrollState()
    val dividerColors = if (isDarkMode) darkDivider else lightDivider

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState)
    ) {

        UserProfileImageView(viewModel, user)

        ThinDividerLineView(dividerColors)
        Spacer(modifier = Modifier.height(32.dp))

        CompositionLocalProvider(LocalTextSelectionColors provides TextSelectionColors(colors.primary, colors.primary)) {
            UserTextField(
                label = stringResource(R.string.profile_screen_first_name_label),
                text = user.firstName ?: "",
                onValueChange = {
                    viewModel.onFirstNameChanged(it)
                }
            )

            AnimatedVisibility(visible = showFirstNameErrorState) {
                ErrorMessage()
            }
            Spacer(modifier = Modifier.height(24.dp))

            UserTextField(
                label = stringResource(R.string.profile_screen_last_name_label),
                text = user.lastName ?: "",
                onValueChange = {
                    viewModel.onLastNameChanged(it)
                }
            )

            AnimatedVisibility(visible = showLastNameErrorState) {
                ErrorMessage()
            }
            Spacer(modifier = Modifier.height(24.dp))

            if (showBackArrowValue) {
                UserTextField(
                    label = stringResource(R.string.profile_screen_username_text_label),
                    text = user.userName ?: "",
                    onValueChange = {
                        viewModel.onUserNameChanged(it)
                    }
                )
                AnimatedVisibility(visible = showUserNameErrorState) {
                    ErrorMessage()
                }
            }
            Spacer(modifier = Modifier.height(24.dp))
            val headerTextColor = if (isDarkMode) darkHeaderText else lightHeaderText
            val textFiledColor = if (isDarkMode) darkTextField else lightTextField
            val dividerColor = if (isDarkMode) darkDivider else lightDivider

            TextField(
                value = user.email ?: "",
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 4.dp),
                onValueChange = {
                    viewModel.onEmailChanged(it)
                },
                enabled = user.loginType == LOGIN_TYPE_PHONE,
                label = {
                    Text(
                        text = stringResource(R.string.profile_screen_label_email),
                        style = AppTheme.typography.bodyTextStyle2,
                        color = headerTextColor,
                    )
                },
                singleLine = true,
                textStyle = AppTheme.typography.subTextStyle1.copy(
                    color = textFiledColor
                ),
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Email,
                    imeAction = ImeAction.Done
                ),
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent,
                    cursorColor = colors.textSecondary
                ),
            )

            DividerLineView(dividerColor)

            if (isEmailInvalid)
                Text(
                    text = stringResource(R.string.profile_screen_invalid_email),
                    color = MaterialTheme.colors.error,
                    modifier = Modifier.padding(horizontal = 16.dp)
                )

            Spacer(modifier = Modifier.height(24.dp))

            if (showBackArrowValue) {
                val focusManager = LocalFocusManager.current
                TextField(
                    value = user.phone ?: "",
                    modifier = Modifier.fillMaxWidth(),
                    onValueChange = {
                        viewModel.onPhoneChanged(it)
                    },
                    label = {
                        Text(
                            text = stringResource(R.string.profile_screen_label_phone_number),
                            style = AppTheme.typography.bodyTextStyle2,
                            color = headerTextColor
                        )
                    },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Phone,
                        imeAction = ImeAction.Done
                    ),
                    keyboardActions = KeyboardActions(
                        onDone = {
                            focusManager.clearFocus()
                        }
                    ),
                    colors = TextFieldDefaults.textFieldColors(
                        backgroundColor = Color.Transparent,
                        focusedIndicatorColor = Color.Transparent,
                        unfocusedIndicatorColor = Color.Transparent,
                        disabledIndicatorColor = Color.Transparent,
                        cursorColor = colors.textSecondary
                    )
                )
                DividerLineView(dividerColor)

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                ) {
                    Text(
                        text = stringResource(id = R.string.profile_screen_label_gender),
                        style = AppTheme.typography.bodyTextStyle2,
                        color = headerTextColor,
                        modifier = Modifier.padding(start = 20.dp, end = 16.dp, top = 24.dp)
                    )

                    Row(modifier = Modifier.padding(bottom = 8.dp, start = 8.dp)) {
                        RadioButton(
                            selected = (user.gender == GENDER_MALE),
                            onClick = { viewModel.onGenderChanged(GENDER_MALE) },
                            colors = RadioButtonDefaults.colors(
                                selectedColor = colors.primary,
                                unselectedColor = textFiledColor
                            )
                        )
                        Text(
                            text = stringResource(id = R.string.profile_screen_gender_male_text),
                            color = textFiledColor,
                            modifier = Modifier
                                .padding(top = 10.dp)
                                .clickable(onClick = { viewModel.onGenderChanged(GENDER_MALE) }),
                            style = AppTheme.typography.subTextStyle1,
                        )
                        Spacer(modifier = Modifier.width(50.dp))
                        RadioButton(
                            selected = (user.gender == GENDER_FEMALE),
                            onClick = { viewModel.onGenderChanged(GENDER_FEMALE) },
                            colors = RadioButtonDefaults.colors(
                                selectedColor = colors.primary,
                                unselectedColor = textFiledColor
                            )
                        )

                        Text(
                            text = stringResource(id = R.string.profile_screen_gender_female_text),
                            color = textFiledColor,
                            modifier = Modifier
                                .padding(top = 10.dp)
                                .clickable(onClick = { viewModel.onGenderChanged(GENDER_FEMALE) }),
                            style = AppTheme.typography.subTextStyle1,
                        )
                    }
                }
            }
        }

        if (showBackArrowValue) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 20.dp),
                contentAlignment = Alignment.BottomCenter
            ) {
                val openDeleteDialog = viewModel.openDeleteDialog.collectAsState()

                SecondaryOutlineButton(
                    modifier = Modifier.motionClickEvent { },
                    onClick = {
                        viewModel.openDialog()
                    },
                    border = BorderStroke(0.dp, Color.Transparent),
                    text = stringResource(id = R.string.setting_screen_delete_account),
                    colors = ButtonDefaults.outlinedButtonColors(contentColor = profileDeleteBtn),
                    color = profileDeleteBtn
                )

                if (openDeleteDialog.value) {
                    ShowDeleteDialog(viewModel)
                }
            }
        }
    }
}

@Preview
@Composable
fun PreviewUserProfile() {
    UserProfileView()
}

private val lightDivider = Color(0x1F000000)
private val darkDivider = Color(0xFF313131)
private val lightHeaderText = Color(0x66000000)
private val darkHeaderText = Color(0x66FFFFFF)
private val darkTextField = Color(0xDEFFFFFF)
private val lightTextField = Color(0xDE000000)
private val profileDeleteBtn = Color(0xFFE13333)
