package com.canopas.ui.home.explore

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.dpToSp
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.PerfectDayData
import com.canopas.ui.R
import java.util.Calendar
import java.util.concurrent.TimeUnit

@Composable
fun PerfectDayView() {
    val isDarkMode = isDarkMode
    val perfectDayList = remember {
        if (isDarkMode) {
            mutableListOf(
                PerfectDayData(
                    R.drawable.ic_perfect_day_dark_bg_1, R.drawable.ic_perfect_day_image_dark_1
                ),
                PerfectDayData(
                    R.drawable.ic_perfect_day_dark_bg_2, R.drawable.ic_perfect_day_image_2
                ),
                PerfectDayData(
                    R.drawable.ic_perfect_day_dark_bg_3, R.drawable.ic_perfect_day_image_3
                ),
                PerfectDayData(
                    R.drawable.ic_perfect_day_dark_bg_4, R.drawable.ic_perfect_day_image_4
                ),
                PerfectDayData(
                    R.drawable.ic_perfect_day_dark_bg_5, R.drawable.ic_perfect_day_image_5
                ),
                PerfectDayData(
                    R.drawable.ic_perfect_day_dark_bg_6, R.drawable.ic_perfect_day_image_6
                ),
                PerfectDayData(
                    R.drawable.ic_perfect_day_dark_bg_7, R.drawable.ic_perfect_day_image_7
                ),
                PerfectDayData(
                    R.drawable.ic_perfect_day_dark_bg_8, R.drawable.ic_perfect_day_image_8
                ),
                PerfectDayData(
                    R.drawable.ic_perfect_day_dark_bg_9, R.drawable.ic_perfect_day_image_9
                ),
            )
        } else {
            mutableListOf(
                PerfectDayData(R.drawable.ic_perfect_day_bg_1, R.drawable.ic_perfect_day_image_1),
                PerfectDayData(R.drawable.ic_perfect_day_bg_2, R.drawable.ic_perfect_day_image_2),
                PerfectDayData(R.drawable.ic_perfect_day_bg_3, R.drawable.ic_perfect_day_image_3),
                PerfectDayData(R.drawable.ic_perfect_day_bg_4, R.drawable.ic_perfect_day_image_4),
                PerfectDayData(R.drawable.ic_perfect_day_bg_5, R.drawable.ic_perfect_day_image_5),
                PerfectDayData(R.drawable.ic_perfect_day_bg_6, R.drawable.ic_perfect_day_image_6),
                PerfectDayData(R.drawable.ic_perfect_day_bg_7, R.drawable.ic_perfect_day_image_7),
                PerfectDayData(R.drawable.ic_perfect_day_bg_8, R.drawable.ic_perfect_day_image_8),
                PerfectDayData(R.drawable.ic_perfect_day_bg_9, R.drawable.ic_perfect_day_image_9),
            )
        }
    }

    val index = remember {
        val currentDate = Calendar.getInstance().timeInMillis
        val daysSinceEpoch = TimeUnit.MILLISECONDS.toDays(currentDate)
        daysSinceEpoch.toInt() % perfectDayList.size
    }

    val perfectDayItem = perfectDayList[index]

    Box(Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
        PerfectDayItemView(perfectDayItem, index)
    }
}

@Composable
fun PerfectDayItemView(perfectDayItem: PerfectDayData, index: Int) {
    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        BoxWithConstraints(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(4.dp)),
        ) {
            val size = maxWidth / if (maxWidth < 300.dp) {
                if (index == 6 || index == 8) 1.5f else 3f
            } else {
                if (index == 6 || index == 8)
                    1.4f else 2.5f
            }

            Image(
                painter = painterResource(id = perfectDayItem.background),
                contentDescription = null,
                modifier = Modifier
                    .fillMaxSize()
                    .aspectRatio(1.21f)
                    .align(Alignment.Center)
            )

            Image(
                painter = painterResource(id = perfectDayItem.image),
                contentDescription = null,
                modifier = Modifier
                    .size(size)
                    .align(Alignment.Center)
            )

            Text(
                text = stringResource(R.string.explore_perfect_day_header_text),
                style = AppTheme.typography.perfectDayTextStyle,
                color = ComposeTheme.colors.primary,
                fontSize = dpToSp(dp = 28.dp),
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.TopCenter)
                    .padding(top = 24.dp)
            )

            Text(
                text = stringResource(R.string.explore_perfect_day_subtitle_text),
                style = AppTheme.typography.perfectDaySubtitleTextStyle,
                color = ComposeTheme.colors.textSecondary,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 24.dp, bottom = 16.dp, end = 24.dp)
                    .align(Alignment.BottomCenter)
            )
        }
    }
}
