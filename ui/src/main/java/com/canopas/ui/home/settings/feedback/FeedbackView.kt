package com.canopas.ui.home.settings.feedback

import android.content.Context
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.animateContentSize
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Cancel
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.PostStoryTitleLineView
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.customview.CustomTextField
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.ui.R
import com.canopas.ui.customactivity.MINIMUM_CHARACTER_LENGTH
import com.canopas.ui.home.settings.settingsview.SUGGEST_A_FEATURE_TAB
import com.canopas.ui.utils.generateReportFile
import java.io.File
import java.io.FileInputStream

@Composable
fun FeedbackView(
    storyId: String,
    selectedTab: Int
) {
    val viewModel = hiltViewModel<FeedbackViewModel>()
    val feedbackState by viewModel.feedbackState.collectAsState()
    val context = LocalContext.current

    val showSuccessAlertDialog by viewModel.showSuccessAlertDialog.collectAsState()
    if (showSuccessAlertDialog) {

        CustomAlertDialog(
            title = null,
            subTitle = stringResource(id = R.string.feedback_screen_successfully_recorded_feedback),
            confirmBtnText = stringResource(id = R.string.feedback_screen_ok_btn_text),
            dismissBtnText = null,
            confirmBtnColor = colors.textPrimary,
            onConfirmClick = { viewModel.managePopBack() },
        )
    }
    val toolBarText by remember {
        mutableStateOf(
            when (selectedTab) {
                SUGGEST_A_FEATURE_TAB -> {
                    context.getString(R.string.setting_suggest_feature_text)
                }
                else -> {
                    context.getString(R.string.setting_get_support_text)
                }
            }
        )
    }

    Scaffold(topBar = {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = toolBarText,
                    navigationIconOnClick = { viewModel.managePopBack() }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
    }) {
        feedbackState.let { state ->
            when (state) {
                is FeedbackState.START -> {
                    FeedbackScreen(storyId, viewModel)
                }

                is FeedbackState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(colors.background)
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }

                is FeedbackState.ERROR -> {
                    val message = state.message
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                        viewModel.managePopBack()
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun FeedbackScreen(
    storyId: String,
    viewModel: FeedbackViewModel
) {
    val title by viewModel.title.collectAsState()
    val description by viewModel.description.collectAsState()
    val showShortTitleError by viewModel.showShortTitleError.collectAsState()
    val context = LocalContext.current
    val scrollState = rememberScrollState()

    val focusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current

    DisposableEffect(key1 = Unit, effect = {
        onDispose {
            keyboardController?.hide()
        }
    })

    val feedbackImage by viewModel.pendingUploadedFeedbackImage.collectAsState()

    UploadImageView(viewModel)

    Box(
        modifier = Modifier
            .fillMaxSize()
            .clickable(
                interactionSource = MutableInteractionSource(),
                indication = null,
                onClick = {
                    keyboardController?.hide()
                    focusManager.clearFocus()
                }
            )
            .background(colors.background)
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .verticalScroll(scrollState)
                .padding(horizontal = 20.dp)
                .align(Alignment.TopCenter)
        ) {
            val titleInteractionSource = remember { MutableInteractionSource() }
            val descriptionInteractionSource = remember { MutableInteractionSource() }
            val isTitleFocused by titleInteractionSource.collectIsFocusedAsState()
            val isDescriptionFocused by descriptionInteractionSource.collectIsFocusedAsState()
            val focusRequester = remember {
                FocusRequester()
            }
            val headerTextColor = if (isDarkMode) headerTextDark else headerTextLight

            DisposableEffect(key1 = Unit) {
                focusRequester.requestFocus()
                onDispose {
                    focusRequester.freeFocus()
                }
            }

            Text(
                text = stringResource(id = R.string.feedback_screen_title_text),
                color = if (isTitleFocused) ColorPrimary else headerTextColor,
                style = AppTheme.typography.bodyTextStyle2,
                modifier = Modifier.padding(top = 28.dp, start = 4.dp)
            )

            CustomTextField(
                text = title,
                onTextChange = {
                    viewModel.onTitleChange(it)
                },
                interactionSource = titleInteractionSource,
                modifier = Modifier
                    .motionClickEvent { }
                    .focusRequester(focusRequester)
                    .padding(top = 4.dp, start = 4.dp)
                    .fillMaxWidth(),
                textStyle = AppTheme.typography.bodyTextStyle1.copy(color = colors.textPrimary),
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Next
                ),
                maxLines = 4,
                cursorBrush = SolidColor(colors.primary.copy(0.5f))
            )

            PostStoryTitleLineView(
                if (isTitleFocused) {
                    colors.primary.copy(0.5f)
                } else {
                    if (isDarkMode) dividerDark else dividerLight
                }
            )
            AnimatedVisibility(showShortTitleError) {
                Text(
                    text = stringResource(id = R.string.global_short_input_error_text),
                    color = MaterialTheme.colors.error,
                    style = AppTheme.typography.captionTextStyle,
                    modifier = Modifier
                        .padding(top = 4.dp, start = 4.dp)
                        .fillMaxWidth()
                )
            }

            Text(
                text = stringResource(id = R.string.feedback_screen_description_text),
                color = if (isDescriptionFocused) colors.primary else headerTextColor,
                style = AppTheme.typography.bodyTextStyle2,
                modifier = Modifier.padding(top = 20.dp, start = 4.dp)
            )

            CompositionLocalProvider(LocalTextSelectionColors provides TextSelectionColors(colors.primary, colors.primary)) {
                OutlinedTextField(
                    value = description,
                    onValueChange = {
                        viewModel.onDescriptionChange(it)
                    },
                    interactionSource = descriptionInteractionSource,
                    modifier = Modifier
                        .motionClickEvent { }
                        .aspectRatio(1.92f)
                        .padding(top = 4.dp),
                    shape = RoundedCornerShape(12.dp),
                    textStyle = AppTheme.typography.bodyTextStyle1.copy(color = colors.textPrimary),
                    keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Text,
                        imeAction = ImeAction.Default
                    ),
                    singleLine = false,
                    colors = TextFieldDefaults.outlinedTextFieldColors(
                        focusedBorderColor = colors.primary.copy(0.5f),
                        unfocusedBorderColor = if (isDarkMode) dividerDark else dividerLight,
                        cursorColor = ColorPrimary.copy(0.5f)
                    )
                )
            }

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 20.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start
            ) {
                IconButton(
                    onClick = {
                        viewModel.openImageChanger()
                    }
                ) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_baseline_attachment_24),
                        contentDescription = "Attachment Image",
                        tint = colors.textPrimary
                    )
                }

                Text(
                    text = if (feedbackImage != null) feedbackImage!!.name else stringResource(R.string.feedback_screen_attachment_text),
                    color = colors.textPrimary,
                    style = AppTheme.typography.subTitleTextStyle1,
                    modifier = Modifier
                        .padding(start = 4.dp)
                        .animateContentSize()
                )
                AnimatedVisibility(visible = feedbackImage != null) {
                    IconButton(
                        onClick = {
                            viewModel.removeSelectedImage()
                        }
                    ) {
                        Icon(
                            Icons.Filled.Cancel,
                            tint = colors.textPrimary,
                            contentDescription = null
                        )
                    }
                }
            }

            PrimaryButton(
                onClick = {
                    val reportFile = generateReportFile(context)
                    reportFile.let { zipFile ->
                        viewModel.submitReport(zipFile, title, description, storyId)
                    }
                },
                shape = RoundedCornerShape(50),
                modifier = Modifier
                    .motionClickEvent { }
                    .padding(top = 20.dp)
                    .height(48.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = if (title.length >= MINIMUM_CHARACTER_LENGTH)
                        colors.primary else colors.primary.copy(0.6f),
                    contentColor = White
                ),
                text = stringResource(id = R.string.feedback_screen_submit_btn_text)
            )
            Spacer(modifier = Modifier.height(200.dp))
        }
    }
}

@Composable
fun UploadImageView(viewModel: FeedbackViewModel) {
    val context = LocalContext.current
    val openImageChooserEvent by viewModel.openImageChooserEvent.collectAsState()
    val openImageChooser = openImageChooserEvent.getContentIfNotHandled() ?: false

    val launcher = rememberLauncherForActivityResult(
        contract =
        ActivityResultContracts.GetContent()
    ) { uri: Uri? ->
        uri?.let {
            val fileName = "IMG_${System.currentTimeMillis()}.jpg"
            val imageFile = File(context.filesDir, fileName)
            imageFile.copyFrom(context, it)
            viewModel.uploadSelectedImage(imageFile)
        }
    }

    if (openImageChooser) {
        launcher.launch("image/*")
    }
}

fun File.copyFrom(context: Context, imageUri: Uri) {
    FileInputStream(
        context.contentResolver.openFileDescriptor(imageUri, "r")!!.fileDescriptor
    ).use { input ->
        this.outputStream().use { output ->
            input.copyTo(output)
        }
    }
}

private val headerTextDark = Color(0xCCFFFFFF)
private val headerTextLight = Color(0x661C191F)
private val dividerDark = Color(0xFF313131)
private val dividerLight = Color(0x991C191F)
