package com.canopas.ui.home.ongoing.history

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils.Companion.allTimeFormat
import com.canopas.base.ui.Utils.Companion.getFormattedStartAndEndDate
import com.canopas.base.ui.Utils.Companion.getSubscriptionArchivedCardDetails
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.Subscription
import com.canopas.ui.R
import kotlinx.coroutines.launch

@Composable
fun ArchivedActivitiesView() {
    val viewModel = hiltViewModel<ArchivedActivitiesViewModel>()
    val state by viewModel.state.collectAsState()
    val subscriptionsListState = rememberLazyListState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart()
    })

    Column(
        modifier = Modifier
            .fillMaxSize()
    ) {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(id = R.string.archived_activities_list_toolbar_text),
                    navigationIconOnClick = { viewModel.onBackClick() }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )

        state.let { state ->
            when (state) {
                is SubscriptionsHistoryState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .background(colors.background)
                            .fillMaxSize()
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is SubscriptionsHistoryState.FAILURE -> {
                    if (state.isNetworkError) {
                        NetworkErrorView {
                            viewModel.onStart()
                        }
                    } else {
                        ServerErrorView {
                            viewModel.onStart()
                        }
                    }
                }
                is SubscriptionsHistoryState.SUCCESS -> {
                    val subscriptions = state.subscriptionHistoryList
                    SubscriptionHistorySuccessState(
                        subscriptions,
                        viewModel,
                        subscriptionsListState
                    )
                }
                else -> {}
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun SubscriptionHistorySuccessState(
    subscriptions: List<Subscription>,
    viewModel: ArchivedActivitiesViewModel,
    subscriptionsListState: LazyListState
) {
    val coroutineScope = rememberCoroutineScope()
    val showButton =
        remember { derivedStateOf { subscriptionsListState.firstVisibleItemIndex > 10 } }
    if (subscriptions.isEmpty()) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(colors.background),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = stringResource(id = R.string.archived_activities_list_no_activities_message),
                style = AppTheme.typography.bodyTextStyle1,
                color = colors.textSecondary
            )
        }
    } else {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .background(colors.background)
        ) {
            Box {
                val sortedList = subscriptions.sortedBy { it.end_date }.reversed()
                val groupedActivity =
                    sortedList.groupBy { allTimeFormat.format(it.end_date * 1000L) }
                LazyColumn(
                    state = subscriptionsListState,
                    contentPadding = PaddingValues(horizontal = 20.dp)
                ) {

                    item {
                        Spacer(modifier = Modifier.height(8.dp))
                    }

                    groupedActivity.forEach { (endDate, subscriptionItems) ->
                        stickyHeader {
                            Box(
                                modifier = Modifier.fillMaxWidth(),
                                contentAlignment = Alignment.Center
                            ) {
                                Column(
                                    modifier = Modifier
                                        .widthIn(max = 600.dp)
                                        .fillMaxWidth()
                                        .background(colors.background)
                                ) {
                                    Text(
                                        text = endDate,
                                        style = AppTheme.typography.h3TextStyle,
                                        color = colors.textPrimary,
                                        modifier = Modifier
                                            .padding(start = 14.dp, top = 8.dp, bottom = 16.dp)
                                    )
                                }
                            }
                        }
                        items(subscriptionItems) { item ->
                            ActivityListView(subscriptions = item, viewModel = viewModel)
                        }
                    }
                }
            }
            Box(
                contentAlignment = Alignment.BottomCenter,
                modifier = Modifier.fillMaxSize()
            ) {
                AnimatedVisibility(
                    visible = showButton.value,
                    enter = fadeIn(),
                    exit = fadeOut(),
                ) {
                    ScrollToTopButton(onClick = {
                        coroutineScope.launch {
                            subscriptionsListState.animateScrollToItem(0)
                        }
                    })
                }
            }
        }
    }
}

@Composable
fun ActivityListView(
    subscriptions: Subscription,
    viewModel: ArchivedActivitiesViewModel,
) {
    val subscriptionCardData = getSubscriptionArchivedCardDetails(subscriptions.progress, isDarkMode)
    val archivedActivityDate =
        getFormattedStartAndEndDate(subscriptions.start_date, subscriptions.end_date)
    Box(
        modifier = Modifier.fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        Box(
            modifier = Modifier
                .padding(vertical = 8.dp)
                .widthIn(max = 600.dp)
                .motionClickEvent {
                    viewModel.onActivityClicked(subscriptions.id)
                }
                .fillMaxWidth()
                .wrapContentHeight()
                .clip(shape = RoundedCornerShape(15.dp))
                .background(
                    color = if (isDarkMode) darkActivityCard else subscriptionCardData.second,
                    shape = RoundedCornerShape(15.dp)
                )
                .border(
                    BorderStroke(1.dp, subscriptionCardData.second),
                    shape = RoundedCornerShape(16.dp)
                )
        ) {
            Column(
                modifier = Modifier
                    .padding(14.dp),
                verticalArrangement = Arrangement.SpaceBetween,
                horizontalAlignment = Alignment.Start
            ) {
                Text(
                    text = subscriptions.activity.name,
                    style = AppTheme.typography.subscriptionListActivityNameTextStyle,
                    color = colors.textPrimary,
                    modifier = Modifier.fillMaxWidth(0.7f)
                )

                Text(
                    text = if (subscriptions.progress.recent.isNotEmpty()) "${subscriptionCardData.first}%" else stringResource(
                        R.string.archived_activities_list_no_completions_text
                    ),
                    color = subscriptionCardData.third,
                    style = AppTheme.typography.h2TextStyle,
                    modifier = Modifier.padding(top = 8.dp)
                )
            }

            if (subscriptions.progress.recent.isNotEmpty()) {
                Row(
                    modifier = Modifier
                        .align(Alignment.BottomEnd)
                        .padding(bottom = 16.dp, end = 16.dp)
                        .wrapContentWidth(),
                    horizontalArrangement = Arrangement.End,
                    verticalAlignment = Alignment.Bottom
                ) {
                    Text(
                        text = archivedActivityDate,
                        color = subscriptionCardData.third.copy(0.75f),
                        style = AppTheme.typography.buttonStyle
                    )
                }
            }
        }
    }
}

@Composable
fun ScrollToTopButton(onClick: () -> Unit) {
    Box(
        Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(bottom = 12.dp, end = 12.dp),
        contentAlignment = Alignment.CenterEnd
    ) {
        Button(
            onClick = { onClick() },
            modifier = Modifier
                .clip(shape = CircleShape)
                .size(48.dp),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = colors.primary,
                contentColor = Color.White
            )
        ) {
            Icon(painter = painterResource(id = R.drawable.ic_arrow_up), contentDescription = "")
        }
    }
}

private val darkActivityCard = Color(0xFF222222)
