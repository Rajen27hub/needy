package com.canopas.ui.home.settings.settingsview

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.ui.R

@Composable
fun SettingsBuyPremiumCardView(viewModel: SettingsViewModel) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp)
            .motionClickEvent { viewModel.handleBuyPremiumCardClick() }
            .background(
                Brush.horizontalGradient(
                    listOf(
                        premiumCardBgLight1, premiumCardBgLight2
                    )
                ),
                RoundedCornerShape(16.dp)
            )
    ) {

        TryPremiumCardView()

        Row(
            modifier = Modifier.align(Alignment.CenterEnd),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            val configuration = LocalConfiguration.current
            val imageHeight = (configuration.screenHeightDp * 0.10).dp

            Image(
                painter = painterResource(id = R.drawable.ic_premium_diamond1),
                contentDescription = "header image",
                contentScale = ContentScale.Fit,
                modifier = Modifier.size(imageHeight)
            )
        }
    }
}

@Composable
fun TryPremiumCardView() {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 20.dp)
    ) {
        Spacer(modifier = Modifier.height(20.dp))

        Text(
            text = stringResource(R.string.setting_try_premium_for_free_text),
            style = AppTheme.typography.titleTextStyle,
            color = ComposeTheme.colors.textPrimary
        )

        Spacer(modifier = Modifier.height(6.dp))

        Text(
            text = stringResource(id = R.string.setting_premium_card_free_user_text),
            style = AppTheme.typography.subTitleTextStyle1,
            color = ComposeTheme.colors.textSecondary,
            lineHeight = 21.sp,
            modifier = Modifier.widthIn(max = 240.dp)
        )
        Spacer(modifier = Modifier.height(20.dp))
    }
}

private val premiumCardBgLight1 = Color(0xFFF3863D)
private val premiumCardBgLight2 = Color(0xFFFFC34E)
