package com.canopas.ui.home.settings.notification

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.Notifications
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class NotificationViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val authManager: AuthManager,
    private val appDispatcher: AppDispatcher,
    private val resources: Resources
) : ViewModel() {

    var user = authManager.currentUser?.notificationsSetting
    val state = MutableStateFlow<NotificationState>(NotificationState.IDLE)
    val pushNotificationEnabled = MutableStateFlow(true)
    val emailNotificationEnabled = MutableStateFlow(true)
    val inActivePushNotificationEnabled = MutableStateFlow(true)
    val inActiveEmailNotificationEnabled = MutableStateFlow(true)

    fun onStart() {
        user?.pushNotification?.let { pushNotificationEnabled.tryEmit(it) }
        user?.mail?.let { emailNotificationEnabled.tryEmit(it) }
        user?.inactivityNotification?.let { inActivePushNotificationEnabled.tryEmit(it) }
        user?.inactivityMail?.let { inActiveEmailNotificationEnabled.tryEmit(it) }
    }

    fun updateNotificationSettings() {
        viewModelScope.launch {
            state.tryEmit(NotificationState.LOADING)
            withContext(appDispatcher.IO) {
                val notificationUpdate =
                    Notifications(
                        pushNotificationEnabled.value,
                        emailNotificationEnabled.value,
                        inActivePushNotificationEnabled.value,
                        inActiveEmailNotificationEnabled.value
                    )
                service.updateNotificationsSettings(notificationUpdate)
                    .onSuccess {
                        state.tryEmit(NotificationState.SUCCESS)
                        val user = authManager.currentUser
                        user!!.notificationsSetting = notificationUpdate
                        authManager.currentUser = user
                    }
                    .onFailure { e ->
                        Timber.e(e)
                        state.tryEmit(NotificationState.FAILURE(e.toUserError(resources)))
                    }
            }
        }
    }

    fun onEmailNotificationSwitchAction(enabled: Boolean) {
        val mailCheck = authManager.currentUser?.email
        if (mailCheck.isNullOrEmpty()) {
            openProfileScreen()
        } else {
            updateEmailNotification(enabled)
        }
    }

    fun updatePushNotification(enabled: Boolean) {
        appAnalytics.logEvent("tap_setting_notifications_push_switch {$enabled}", null)
        pushNotificationEnabled.tryEmit(enabled)
        updateNotificationSettings()
    }

    fun updateEmailNotification(enabled: Boolean) {
        appAnalytics.logEvent("tap_setting_notifications_email_switch {$enabled}", null)
        emailNotificationEnabled.tryEmit(enabled)
        updateNotificationSettings()
    }

    fun updatePushInActiveReminderNotification(enabled: Boolean) {
        appAnalytics.logEvent("tap_setting_inactive_reminder_push_switch {$enabled}", null)
        inActivePushNotificationEnabled.tryEmit(enabled)
        updateNotificationSettings()
    }

    fun updateEmailInactiveReminderNotification(enabled: Boolean) {
        appAnalytics.logEvent("tap_setting_inactive_reminder_email_switch {$enabled}", null)
        inActiveEmailNotificationEnabled.tryEmit(enabled)
        updateNotificationSettings()
    }

    fun openProfileScreen() {
        appAnalytics.logEvent("tap_email_notification_to_view_profile", null)
        navManager.navigateToProfileScreen()
    }

    fun managePopBack() {
        navManager.popBack()
    }
}

sealed class NotificationState {
    object IDLE : NotificationState()
    object LOADING : NotificationState()
    object SUCCESS : NotificationState()
    data class FAILURE(val message: String) : NotificationState()
}
