package com.canopas.ui.home.ongoing

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.data.model.Subscription
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.home.explore.DEFAULT_SLEEP_ACTIVITY_ID
import com.canopas.ui.home.explore.DEFAULT_WAKE_UP_ACTIVITY_ID
import com.canopas.ui.home.explore.timeStringToCurrentMillis
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SubscriptionListViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    private val preferencesDataStore: NoLonelyPreferences,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<SubscriptionsState>(SubscriptionsState.LOADING)

    fun onStart() {
        refreshView()
    }

    private fun refreshView() = viewModelScope.launch {
        appAnalytics.logEvent("view_tab_active_subscriptions", null)
        if (state.value !is SubscriptionsState.SUCCESS && state.value !is SubscriptionsState.FAILURE) {
            state.tryEmit(SubscriptionsState.LOADING)
        }
        withContext(appDispatcher.IO) {
            service.getUserSubscriptions().onSuccess { subscriptionsList ->
                val subscriptionsCount = subscriptionsList.filter { subs ->
                    subs.is_active && !(subs.activity.id == DEFAULT_WAKE_UP_ACTIVITY_ID || subs.activity.id == DEFAULT_SLEEP_ACTIVITY_ID)
                }.size
                val sortedSubscriptions = subscriptionsList.sortedWith(
                    compareBy(
                        { timeStringToCurrentMillis(it.activity_time)?.div(1000) ?: Long.MAX_VALUE },
                        { it.activity_duration ?: Int.MAX_VALUE }
                    )
                )
                preferencesDataStore.setUserSubscriptionsCount(subscriptionsCount)
                state.tryEmit(SubscriptionsState.SUCCESS(sortedSubscriptions))
            }.onFailure { e ->
                Timber.e(e)
                state.value = SubscriptionsState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    fun onActivityClicked(subscriptionId: Int) {
        appAnalytics.logEvent("tap_active_subscriptions_item", null)
        navManager.navigateToActivityStatusBySubscriptionId(subscriptionId = subscriptionId)
    }

    fun navigateToAddActivityView() {
        navManager.navigateToAddActivityView()
    }

    fun navigateToArchivedActivitiesView() {
        appAnalytics.logEvent("tap_active_subscriptions_archived_button")
        navManager.navigateToArchivedActivitiesView()
    }
}

sealed class SubscriptionsState {
    object START : SubscriptionsState()
    object LOADING : SubscriptionsState()
    data class SUCCESS(
        val onGoingActivities: List<Subscription>
    ) : SubscriptionsState()
    data class FAILURE(val message: String, val isNetworkError: Boolean) : SubscriptionsState()
}
