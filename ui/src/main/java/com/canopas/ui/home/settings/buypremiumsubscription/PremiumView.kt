package com.canopas.ui.home.settings.buypremiumsubscription

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterMediumFont
import com.canopas.base.ui.InterSemiBoldFont
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.data.model.PlanType
import com.canopas.ui.R

@Composable
fun BuildBodyForNewBuyPremiumView() {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 20.dp)
    ) {
        Text(
            text = stringResource(R.string.premium_unlock_features_header_text),
            style = AppTheme.typography.h1TextStyle,
            color = colors.textPrimary,
            lineHeight = 32.sp,
            textAlign = TextAlign.Center
        )
        Spacer(modifier = Modifier.height(30.dp))

        Text(
            text = buildAnnotatedString {

                append(stringResource(R.string.buy_premium_screen_losers_have_text))

                withStyle(
                    style = SpanStyle(
                        color = colors.textPrimary,
                        fontFamily = InterMediumFont,
                    )
                ) {
                    append(stringResource(R.string.buy_premium_screen_goals_text))
                }

                append(stringResource(R.string.buy_premium_screen_winners_have_text))
                withStyle(
                    style = SpanStyle(
                        color = colors.textPrimary, fontFamily = InterMediumFont
                    )
                ) {
                    append(stringResource(R.string.buy_premium_screen_systems_text))
                }
                withStyle(
                    style = SpanStyle(
                        color = colors.textPrimary,
                        fontSize = 16.sp,
                        fontFamily = InterSemiBoldFont
                    )
                ) {
                    append(stringResource(R.string.buy_premium_screen_author_name_text))
                }
            },
            style = AppTheme.typography.topBarButtonTextStyle,
            color = colors.textSecondary,
            lineHeight = 28.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 40.dp),
        )
    }
}

@Composable
fun NewPremiumBoxView(viewModel: BuyPremiumSubscriptionViewModel) {
    val selectedYearlyPlan by viewModel.selectedYearlyPlan.collectAsState()
    val selectedMonthlyPlan by viewModel.selectedMonthlyPlan.collectAsState()
    val planOptions by viewModel.planOptions.collectAsState()
    val yearlyPlan by remember {
        mutableStateOf(planOptions.find { it.type == PlanType.PLAN_YEARLY })
    }
    val monthlyPlan by remember { mutableStateOf(planOptions.find { it.type == PlanType.PLAN_MONTHLY }) }
    val yearlyDiscountedPrice = (yearlyPlan?.priceMicros?.div((1000000 * 12))).toString()
    val monthlyDiscountedPrice = viewModel.getProductAmount(yearlyPlan!!, yearlyDiscountedPrice)
    val formattedText = stringResource(R.string.premium_screen_subscription_with_amount_text,
        stringResource(R.string.premium_screen_subscription_text),
        yearlyPlan?.price.toString()
    );

    Column {
        TwoBox(
            value = selectedYearlyPlan,
            plan = "Yearly",
            description = formattedText,
            monthlyDiscountedPrice,
            onClick = {
                 viewModel.onClickBtnView(0)
            },
        )
        TwoBox(
            value = selectedMonthlyPlan,
            plan = "Monthly",
            description = "Subscription",
            monthlyPlan?.price.toString(),
            onClick = {
                viewModel.onClickBtnView(1)
            },
        )
    }
}

@Composable
fun TwoBox(value: Boolean, plan: String, description: String, price: String, onClick: () -> Unit) {
    Box(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(horizontal = 20.dp, vertical = 12.dp)
            .motionClickEvent {
                onClick()
            }
            .background(
                if (value) colors.primary.copy(0.2f) else colors.background,
                shape = RoundedCornerShape(18.dp)
            )
            .border(
                if (!value) BorderStroke(1.dp, colors.textSecondary.copy(0.2f)) else BorderStroke(
                    0.dp,
                    Color.Transparent
                ),
                RoundedCornerShape(18.dp)
            )
            .wrapContentHeight()
            .clip(shape = RoundedCornerShape(18.dp))
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Column(
                modifier = Modifier.padding(start = 6.dp),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_check_circle),
                    contentDescription = "",
                    modifier = Modifier
                        .padding(8.dp)
                        .size(30.dp),
                    tint = if(value) colors.primary else colors.textSecondary.copy(0.2f)
                )
            }
            Row(
                modifier = Modifier.fillMaxWidth().padding(horizontal = 4.dp, vertical = 8.dp),
                horizontalArrangement = Arrangement.SpaceBetween
            ){
                Column(
                    horizontalAlignment = Alignment.Start
                ) {
                    Text(
                        text = plan,
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        text = description,
                        fontSize = 16.sp
                    )
                }
                Column(
                    modifier = Modifier.padding(end = 12.dp),
                    horizontalAlignment = Alignment.End
                ) {
                    Text(
                        text = price,
                        fontSize = 20.sp,
                        fontWeight = FontWeight.Bold
                    )
                    Text(
                        text = "per month",
                        fontSize = 16.sp
                    )
                }
            }
        }
    }
}
