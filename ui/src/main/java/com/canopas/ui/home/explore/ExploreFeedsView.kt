package com.canopas.ui.home.explore

import android.graphics.drawable.BitmapDrawable
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.zIndex
import androidx.core.graphics.ColorUtils
import androidx.palette.graphics.Palette
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ExploreTheme
import com.canopas.data.model.FEED_TYPE_MOTIVATIONAL_MATERIAL
import com.canopas.data.model.FeaturedFeed
import com.canopas.data.model.MOTIVATIONAL_TYPE_PODCASTS
import com.canopas.data.model.MOTIVATIONAL_TYPE_VIDEOS
import com.canopas.data.model.Story
import com.canopas.ui.R

@Suppress("FunctionName")
fun FeedsView(
    viewModel: ExploreViewModel,
    feeds: List<FeaturedFeed>,
    isSubsEmpty: Boolean,
    lazyListScope: LazyListScope
) {
    lazyListScope.item {
        Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
            Text(
                text = stringResource(R.string.explore_screen_explore_header_text),
                style = AppTheme.typography.h2TextStyle,
                textAlign = TextAlign.Start,
                color = colors.textPrimary,
                letterSpacing = -(0.96.sp),
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .fillMaxWidth()
                    .padding(
                        start = 16.dp,
                        end = 16.dp,
                        top = if (isSubsEmpty) 8.dp else 28.dp,
                        bottom = 8.dp
                    )
            )
        }
    }

    lazyListScope.itemsIndexed(
        items = feeds
    ) { index, feed ->
        val context = LocalContext.current
        viewModel.onLastItemVisible(index, feeds.size)
        if (feed.type == FEED_TYPE_MOTIVATIONAL_MATERIAL) {
            val subtitle = feed.motivational_material.subtitle ?: ""
            FeedItem(
                imageUrl = feed.motivational_material.image_url,
                typeText = subtitle.ifEmpty {
                    when (feed.motivational_material.type) {
                        MOTIVATIONAL_TYPE_PODCASTS -> {
                            context.getString(R.string.explore_feeds_podcast_title_text)
                        }
                        MOTIVATIONAL_TYPE_VIDEOS -> {
                            context.getString(R.string.explore_feeds_video_title_text)
                        }
                        else -> context.getString(R.string.explore_feeds_blog_title_text)
                    }
                },
                mediaUrl = feed.motivational_material.media_url,
                title = feed.motivational_material.title,
                type = feed.motivational_material.type,
                viewModel = viewModel,
                storyItem = null
            )
        } else {
            FeedItem(
                imageUrl = feed.story.image_url,
                typeText = feed.story.subtitle,
                title = feed.story.title,
                type = feed.type,
                viewModel = viewModel,
                storyItem = feed.story,
                isStory = true
            )
        }
    }
}

@Composable
fun FeedItem(
    imageUrl: String,
    typeText: String,
    title: String,
    mediaUrl: String? = null,
    type: Int,
    viewModel: ExploreViewModel,
    storyItem: Story?,
    isStory: Boolean = false
) {
    Column(
        modifier = Modifier.fillMaxWidth(),
        verticalArrangement = Arrangement.Center, horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Card(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .motionClickEvent(onClick = {
                    if (isStory && storyItem != null) {
                        viewModel.navigateToStoryView(storyItem)
                    } else {
                        if (mediaUrl != null) {
                            viewModel.onMotivationalItemClick(
                                mediaUrl,
                                isVideoItem = type == MOTIVATIONAL_TYPE_VIDEOS,
                                type
                            )
                        }
                    }
                })
                .aspectRatio(1f)
                .clip(RoundedCornerShape(15.dp))
                .padding(horizontal = 16.dp, vertical = 8.dp),
            backgroundColor = ExploreTheme.exploreColors.categoryBgColor,
            shape = RoundedCornerShape(15.dp),
        ) {
            val painter = rememberAsyncImagePainter(
                ImageRequest.Builder(LocalContext.current).data(data = imageUrl).allowHardware(false).build()
            )

            when (val painterState = painter.state) {
                is AsyncImagePainter.State.Loading -> {
                    ImageHeaderView(
                        colors.textPrimary,
                        colors.textSecondary,
                        typeText,
                        title,
                        painter = painter,
                        background = Brush.verticalGradient(
                            colors = listOf(
                                Color.Transparent,
                                Color.Transparent
                            )
                        )
                    )
                }
                is AsyncImagePainter.State.Success -> {
                    val palette = remember(painterState.result) {
                        Palette.from(
                            (painterState.result.drawable as BitmapDrawable).bitmap
                        ).generate()
                    }
                    val dominantColor = remember(palette) { palette.dominantSwatch }
                    val titleTextColor = remember(dominantColor) {
                        if (dominantColor == null) {
                            darkItemTextColor
                        } else {
                            if (ColorUtils.calculateLuminance(dominantColor.rgb) <= 0.5f) {
                                lightItemTextColor
                            } else {
                                darkItemTextColor
                            }
                        }
                    }

                    val subTitleTextColor = remember(titleTextColor) { titleTextColor.copy(alpha = 0.80f) }

                    ImageHeaderView(
                        titleTextColor,
                        subTitleTextColor,
                        typeText,
                        title,
                        painter = painter,
                        background = Brush.verticalGradient(
                            colors = listOf(
                                Color.Transparent,
                                if (dominantColor == null) Color.Transparent else Color(dominantColor.rgb)
                            )
                        )
                    )
                }
                else -> {}
            }
        }
    }
}

@Composable
fun ImageHeaderView(
    titleTextColor: Color,
    subTitleTextColor: Color,
    typeText: String,
    title: String,
    painter: AsyncImagePainter,
    background: Brush
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .clip(RoundedCornerShape(15.dp))
    ) {
        Image(
            painter = painter,
            modifier = Modifier
                .zIndex(0f)
                .fillMaxSize(),
            contentDescription = null,
            contentScale = ContentScale.Crop
        )

        Column(
            modifier = Modifier
                .zIndex(1f)
                .fillMaxWidth()
                .align(Alignment.BottomStart)
                .background(
                    brush = background
                )
                .padding(16.dp)
        ) {
            Text(
                text = typeText.uppercase().trim(),
                color = subTitleTextColor,
                maxLines = 1,
                style = AppTheme.typography.subTitleTextStyle1,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                text = title.trim(),
                color = titleTextColor,
                maxLines = 2,
                style = AppTheme.typography.h2TextStyle,
                letterSpacing = -(0.96.sp),
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

private val lightItemTextColor = Color(0xFFFFFFFF)
private val darkItemTextColor = Color(0xFF000000)
