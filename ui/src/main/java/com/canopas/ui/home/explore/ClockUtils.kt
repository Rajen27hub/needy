package com.canopas.ui.home.explore

import javax.inject.Inject

interface ClockUtils {
    fun getCurrentDate(): Long
    fun getCurrentTimeInSeconds(): Long
}

class CurrentDate @Inject constructor() : ClockUtils {

    override fun getCurrentDate(): Long {
        return System.currentTimeMillis()
    }

    override fun getCurrentTimeInSeconds(): Long {
        return System.currentTimeMillis() / 1000L
    }
}
