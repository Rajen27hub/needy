package com.canopas.ui.home.settings.buypremiumsubscription

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.White
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme

@Composable
fun BuildFeaturedPurchaseView(
    viewModel: BuyPremiumSubscriptionViewModel,
    headerText: String,
    btnText: String,
    signOutDevicesText: String? = null,
    onPurchaseBtnClick: () -> Unit,
    signOutOtherDevicesOnClick: () -> Unit
) {
    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = headerText,
            style = AppTheme.typography.subTitleTextStyle1,
            color = ComposeTheme.colors.textSecondary,
            lineHeight = 21.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 20.dp, end = 20.dp, top = 28.dp)
        )

        Spacer(modifier = Modifier.height(16.dp))

        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
                .motionClickEvent { onPurchaseBtnClick() }
                .clip(shape = RoundedCornerShape(50.dp))
                .background(ComposeTheme.colors.primary)
        ) {
            Text(
                text = btnText,
                modifier = Modifier
                    .padding(vertical = 14.dp)
                    .align(Alignment.Center),
                style = AppTheme.typography.buttonStyle,
                color = White,
                lineHeight = 16.sp,
            )
        }
        Spacer(modifier = Modifier.height(18.dp))

        if (!signOutDevicesText.isNullOrEmpty()) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 20.dp)
                    .motionClickEvent { signOutOtherDevicesOnClick() }
                    .clip(shape = RoundedCornerShape(24.dp))
                    .border(1.dp, ComposeTheme.colors.primary, shape = RoundedCornerShape(24.dp))
            ) {
                Text(
                    text = signOutDevicesText,
                    modifier = Modifier
                        .padding(vertical = 14.dp)
                        .align(Alignment.Center),
                    style = AppTheme.typography.buttonStyle,
                    color = ComposeTheme.colors.primary,
                    lineHeight = 16.sp,
                )
            }
            Spacer(modifier = Modifier.height(24.dp))
        }

        Row(
            modifier = Modifier.align(Alignment.CenterHorizontally)
        ) {
            Text(
                text = "Restore Purchase",
                style = AppTheme.typography.buttonStyle,
                color = ComposeTheme.colors.textSecondary,
                lineHeight = 16.sp,
                modifier = Modifier
                    .padding(bottom = 40.dp)
                    .motionClickEvent {
//                        viewModel.showAllPlans()
                    }
            )
        }
    }
}
