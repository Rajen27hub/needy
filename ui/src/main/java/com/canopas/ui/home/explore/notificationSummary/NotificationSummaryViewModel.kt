package com.canopas.ui.home.explore.notificationSummary

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.ui.Utils
import com.canopas.base.ui.Utils.Companion.sortDateFormat
import com.canopas.base.ui.Utils.Companion.yearFormat
import com.canopas.data.model.WeeklySummary
import com.canopas.data.rest.NoLonelyService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.Date
import javax.inject.Inject

@HiltViewModel
class NotificationSummaryViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    private val preferences: NoLonelyPreferences,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<SummaryState>(SummaryState.LOADING)
    val isDarkModeEnabled = MutableStateFlow(false)
    val getStartDate = MutableStateFlow("")
    val getEndDate = MutableStateFlow("")

    init {
        viewModelScope.launch {
            isDarkModeEnabled.value = preferences.getIsDarkModeEnabled()
        }
    }

    fun onStart(startDate: String, endDates: String) {
        appAnalytics.logEvent("tap_weekly_summary_notification")
        appAnalytics.logEvent("view_notification_summary")
        getWeeklyActivitySummary(startDate.toLong(), endDates.toLong())
    }

    fun getWeeklyActivitySummary(startDate: Long, endDate: Long) {
        viewModelScope.launch {
            state.tryEmit(SummaryState.LOADING)
            withContext(appDispatcher.IO) {
                service.getWeeklyActivitiesSummary(startDate, endDate).onSuccess {
                    getDateDuration(it.startDate, it.endDate)
                    state.tryEmit(SummaryState.SUCCESS(it))
                }.onFailure { e ->
                    Timber.e(e)
                    state.tryEmit(SummaryState.FAILURE(e.toUserError(resources)))
                }
            }
        }
    }

    private fun getDateDuration(startDate: String, endDate: String) {
        val date = Date()
        val formattedYear = yearFormat.format(date)
        val startDateWithYear = ("$startDate $formattedYear")
        val endDateWithYear = ("$endDate $formattedYear")
        val formatStartDate = Utils.summaryFormat.parse(startDateWithYear)
        val formatEndDate = Utils.summaryFormat.parse(endDateWithYear)

        formatStartDate?.let { getStartDate.tryEmit(sortDateFormat.format(it)) }
        formatEndDate?.let { getEndDate.tryEmit(sortDateFormat.format(it)) }
    }
}

sealed class SummaryState {
    object LOADING : SummaryState()
    data class SUCCESS(val weeklySummary: WeeklySummary) : SummaryState()
    data class FAILURE(val message: String) : SummaryState()
}
