package com.canopas.ui.home.settings.settingsview

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Switch
import androidx.compose.material.SwitchDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import com.canopas.base.data.model.PREMIUM_USER
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.White
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.base.ui.uncheckSwitchBg
import com.canopas.data.model.PlanType
import com.canopas.ui.R
import com.canopas.ui.utils.loadUriData

@Composable
fun GeneralSettingsView(
    viewModel: SettingsViewModel,
    cardBackground: Color,
    userType: Int?,
    isUserVerified: Boolean
) {

    val darkModeState by viewModel.darkModeState.collectAsState()
    val leaderBoardVisibilityState by viewModel.isVisibleOnLeaderboard.collectAsState()
    val userVisibilityState by viewModel.userVisibilityState.collectAsState()
    val context = LocalContext.current
    val productId by viewModel.productId.collectAsState()
    val useDarkIcons = isDarkMode

    LaunchedEffect(key1 = userVisibilityState) {
        userVisibilityState.let {
            if (userVisibilityState is UserVisibilityState.FAILURE) {
                val message = (userVisibilityState as UserVisibilityState.FAILURE).message
                showBanner(context, message)
            }
        }
    }

    Text(
        text = stringResource(R.string.setting_general_tab_text),
        style = AppTheme.typography.todoActivityTitleTextStyle,
        color = ComposeTheme.colors.textPrimary,
        textAlign = TextAlign.Start,
        modifier = Modifier.fillMaxWidth().padding(start = 20.dp, top = 40.dp)
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp, vertical = 16.dp)
            .clip(shape = RoundedCornerShape(16.dp))
            .background(cardBackground)

    ) {
        Spacer(modifier = Modifier.height(20.dp))

        if (userType == PREMIUM_USER) {
            val userSubscription =
                if (productId == PlanType.PLAN_MONTHLY.id) stringResource(id = R.string.settings_screen_monthly_subscription)
                else stringResource(id = R.string.settings_screen_yearly_subscription)

            SettingCardDesignView(
                paint = painterResource(id = R.drawable.ic_setting_premium_icon),
                paintContentDescription = "Premium Icon",
                text = stringResource(R.string.setting_screen_premium_user_label),
                text2 = userSubscription
            ) {
                viewModel.addPremiumBtnClickAnalyticEvent()
                loadUriData(context, PLAY_STORE_SUBS_URL)
            }
            Spacer(modifier = Modifier.height(20.dp))
        }

        if (isUserVerified) {
            SettingCardDesignView(
                paint = painterResource(id = R.drawable.ic_setting_notifications_icon),
                paintContentDescription = "Notification Icon",
                text = stringResource(R.string.setting_screen_notification_label)
            ) {
                viewModel.openNotificationScreen()
            }
            Spacer(modifier = Modifier.height(20.dp))
        }

        if (isUserVerified) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 16.dp),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_setting_leaderboard_icon),
                    contentDescription = "Leader Board Icon",
                    modifier = Modifier.size(37.dp)
                )
                Column(
                    modifier = Modifier
                        .padding(start = 16.dp)
                        .weight(1f)
                        .align(Alignment.CenterVertically)
                ) {
                    Text(
                        text = stringResource(R.string.setting_screen_leaderboard_text),
                        style = AppTheme.typography.subTitleTextStyle1,
                        color = ComposeTheme.colors.textPrimary,
                    )
                }
                Switch(
                    modifier = Modifier
                        .padding(end = 16.dp)
                        .size(width = 39.dp, height = 22.dp),
                    checked = leaderBoardVisibilityState,
                    onCheckedChange = {
                        viewModel.onLeaderboardVisibilityChange(it)
                    },
                    colors = SwitchDefaults.colors(
                        checkedThumbColor = White,
                        checkedTrackColor = ColorPrimary,
                        uncheckedThumbColor = White,
                        uncheckedTrackColor = if (useDarkIcons) White else uncheckSwitchBg,
                        checkedTrackAlpha = 1.0f
                    )
                )
            }
            Spacer(modifier = Modifier.height(20.dp))
        }

        Row(
            modifier = Modifier
                .padding(start = 16.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_setting_darkmode_icon),
                contentDescription = "Dark Mode Icon",
                modifier = Modifier.size(37.dp)
            )
            Column(
                modifier = Modifier
                    .padding(start = 16.dp)
                    .weight(1f)
                    .align(Alignment.CenterVertically)
            ) {
                Text(
                    text = stringResource(R.string.setting_screen_darkmode_text),
                    style = AppTheme.typography.subTitleTextStyle1,
                    color = ComposeTheme.colors.textPrimary
                )
            }
            Switch(
                modifier = Modifier
                    .padding(end = 16.dp)
                    .size(width = 39.dp, height = 22.dp),
                checked = darkModeState,
                onCheckedChange = {
                    viewModel.onDarkModeChange(it)
                },
                colors = SwitchDefaults.colors(
                    checkedThumbColor = White,
                    checkedTrackColor = ColorPrimary,
                    uncheckedThumbColor = White,
                    uncheckedTrackColor = uncheckSwitchBg,
                    checkedTrackAlpha = 1.0f
                )
            )
        }
        Spacer(modifier = Modifier.height(20.dp))
    }
}
