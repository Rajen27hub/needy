package com.canopas.ui.home.explore

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.sizeIn
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.parse
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.GoalWithColors
import com.canopas.data.model.TYPE_END_GOAL
import com.canopas.ui.R

@Composable
fun GoalHeaderView(viewModel: ExploreViewModel) {
    Box(
        modifier = Modifier
            .fillMaxWidth(),
        contentAlignment = Alignment.TopCenter,
    ) {
        Row(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = stringResource(R.string.explore_view_your_wall_header_text),
                style = AppTheme.typography.h2TextStyle,
                textAlign = TextAlign.Start,
                color = ComposeTheme.colors.textPrimary,
                lineHeight = 32.sp,
                letterSpacing = -(0.96.sp),
                modifier = Modifier
                    .padding(
                        start = 16.dp,
                        end = 16.dp,
                        top = 20.dp
                    )
            )

            TextButton(
                onClick = {
                    viewModel.navigateToMyGoals()
                },
                modifier = Modifier
                    .padding(
                        start = 16.dp,
                        end = 16.dp,
                        top = 20.dp
                    )
                    .motionClickEvent { },
            ) {
                Text(
                    text = stringResource(R.string.explore_view_all_btn_text),
                    style = AppTheme.typography.buttonStyle,
                    textAlign = TextAlign.End,
                    color = ComposeTheme.colors.primary
                )
            }
        }
    }
}

@Composable
fun YourWallView(viewModel: ExploreViewModel, goals: List<GoalWithColors>) {
    Box(
        modifier = Modifier
            .fillMaxWidth(),
        contentAlignment = Alignment.TopCenter,
    ) {
        Row(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(top = 8.dp, bottom = 12.dp)
                .background(if (ComposeTheme.isDarkMode) darkYourWallBg else lightYourWallBg)
                .heightIn(min = 300.dp)
        ) {
            val sortedList = remember(goals) {
                goals.sort()
            }
            Column(modifier = Modifier.fillMaxWidth(0.48f)) {
                sortedList.first.forEachIndexed { index, goalWithColors ->
                    val topPadding = if (index == 0) 16.dp else 0.dp
                    val endPadding = 0.dp
                    GoalsCardView(
                        goalWithColor = goalWithColors,
                        viewModel = viewModel,
                        topPadding = topPadding,
                        endPadding = endPadding
                    )
                }
            }

            Column(modifier = Modifier.fillMaxWidth()) {
                sortedList.second.forEachIndexed { index, goalWithColors ->
                    val topPadding = if (index == 0) 16.dp else 0.dp
                    val endPadding = 16.dp
                    GoalsCardView(goalWithColor = goalWithColors, viewModel = viewModel, endPadding = endPadding, topPadding = topPadding)
                }
            }
        }
    }
}

@Composable
fun GoalsCardView(
    goalWithColor: GoalWithColors,
    viewModel: ExploreViewModel,
    topPadding: Dp,
    endPadding: Dp
) {
    val cardColor = if (goalWithColor.color2 != null) goalWithColor.color2!! else goalWithColor.color

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(start = 16.dp, top = topPadding, bottom = 16.dp, end = endPadding)
            .motionClickEvent(onClick = {
                viewModel.navigateToEditGoal(
                    goalId = goalWithColor.goal.id,
                    goalWithColor.subscriptionId
                )
            })
            .background(
                Color
                    .parse(cardColor)
                    .copy(0.3f)
            ),
        verticalArrangement = Arrangement.SpaceBetween
    ) {
        Column(verticalArrangement = Arrangement.Top) {
            Text(
                text = goalWithColor.goal.activityName?.trim() +
                    stringResource(R.string.explore_goal_card_dash_text) +
                    if (goalWithColor.goal.type == TYPE_END_GOAL) stringResource(R.string.explore_goal_card_why_title_text)
                    else Utils.goalsDueDateFormat.format(Utils.format.parse(goalWithColor.goal.dueDate) ?: System.currentTimeMillis()),
                style = AppTheme.typography.bodyTextStyle3,
                color = ComposeTheme.colors.textSecondary,
                textAlign = TextAlign.Start,
                letterSpacing = -(0.56.sp),
                lineHeight = 17.sp,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 12.dp, start = 8.dp, end = 8.dp)
            )
            Box {
                val lineHeight = AppTheme.typography.goalsBodyTextStyle.fontSize * 4 / 3
                Text(
                    text = goalWithColor.goal.title?.trim() ?: "",
                    style = AppTheme.typography.goalsBodyTextStyle,
                    color = ComposeTheme.colors.textPrimary,
                    textAlign = TextAlign.Start,
                    lineHeight = lineHeight,
                    letterSpacing = -(0.72.sp),
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp, start = 8.dp, end = 8.dp, bottom = 8.dp)
                        .sizeIn(
                            minHeight = with(LocalDensity.current) {
                                (lineHeight * 2).toDp()
                            }
                        )
                )
                if (goalWithColor.isCompleted) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_check_circle),
                        contentDescription = "",
                        modifier = Modifier
                            .align(Alignment.BottomEnd)
                            .padding(6.dp)
                            .size(18.dp),
                        tint = completedGoalsColor
                    )
                }
            }
        }
        if (goalWithColor.goal.occurrences != 0) {
            LinearProgressIndicator(
                progress = goalWithColor.goal.getProgress(),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(6.dp)
                    .clip(RoundedCornerShape(3.dp)),
                color = ComposeTheme.colors.primary,
                backgroundColor = ComposeTheme.colors.textPrimary.copy(0.04f)
            )
        }
    }
}

private val lightYourWallBg = Color(0xFFFAF7F6)
private val darkYourWallBg = Color(0xFF323232)
private val completedGoalsColor = Color(0xFF47A96E)
