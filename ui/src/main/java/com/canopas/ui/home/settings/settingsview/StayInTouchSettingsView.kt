package com.canopas.ui.home.settings.settingsview

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.SignOutTextColor
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.ui.R
import com.canopas.ui.reviewmanager.redirectToPlayStore
import com.canopas.ui.utils.loadUriData

@Composable
fun StayInTouchSettingsView(viewModel: SettingsViewModel, cardBackground: Color) {
    val context = LocalContext.current

    Text(
        text = stringResource(R.string.setting_stay_in_touch_tab_text),
        style = AppTheme.typography.todoActivityTitleTextStyle,
        color = ComposeTheme.colors.textPrimary,
        textAlign = TextAlign.Start,
        modifier = Modifier.fillMaxWidth().padding(top = 32.dp, start = 20.dp, end = 20.dp)
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp, vertical = 16.dp)
            .clip(shape = RoundedCornerShape(16.dp))
            .background(cardBackground)
    ) {
        Spacer(modifier = Modifier.height(20.dp))

        SettingCardDesignView(
            paint = painterResource(id = R.drawable.ic_setting_get_support_icon),
            paintContentDescription = "Get SupportIcon",
            text = stringResource(R.string.setting_get_support_text)
        ) {

            viewModel.openFeedbackScreen(GET_SUPPORT_TAB)
        }

        Spacer(modifier = Modifier.height(20.dp))

        SettingCardDesignView(
            paint = painterResource(id = R.drawable.ic_setting_facebook_like_icon),
            paintContentDescription = "Facebook Like Icon",
            text = stringResource(R.string.setting_facebook_like_text),
        ) {
            loadUriData(context, OPEN_FACEBOOK_URL)
        }

        Spacer(modifier = Modifier.height(20.dp))

        SettingCardDesignView(
            paint = painterResource(id = R.drawable.ic_setting_twitter_icon),
            paintContentDescription = "twitter icon",
            text = stringResource(R.string.setting_twitter_follow_text),
        ) {
            loadUriData(context, OPEN_TWITTER_URL)
        }

        Spacer(modifier = Modifier.height(20.dp))

        SettingCardDesignView(
            paint = painterResource(id = R.drawable.ic_setting_suggest_feature_icon),
            paintContentDescription = "Suggest Icon",
            text = stringResource(R.string.setting_suggest_feature_text),
        ) {

            viewModel.openFeedbackScreen(SUGGEST_A_FEATURE_TAB)
        }

        Spacer(modifier = Modifier.height(20.dp))

        SettingCardDesignView(
            paint = painterResource(id = R.drawable.ic_setting_rate_us_icon),
            paintContentDescription = "rate us icon",
            text = stringResource(R.string.setting_rate_us_text)
        ) {
            redirectToPlayStore(context)
        }

        Spacer(modifier = Modifier.height(20.dp))
    }
}

@Composable
fun SettingsAboutView(viewModel: SettingsViewModel, cardBackground: Color) {

    val context = LocalContext.current
    Text(
        text = stringResource(R.string.setting_about_tab_text),
        style = AppTheme.typography.todoActivityTitleTextStyle,
        color = ComposeTheme.colors.textPrimary,
        textAlign = TextAlign.Start,
        modifier = Modifier.fillMaxWidth().padding(top = 32.dp, start = 20.dp, end = 20.dp)
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp, vertical = 16.dp)
            .clip(shape = RoundedCornerShape(16.dp))
            .background(cardBackground)
    ) {
        Spacer(modifier = Modifier.height(20.dp))

        SettingCardDesignView(
            paint = painterResource(id = R.drawable.ic_setting_privacy_icon),
            paintContentDescription = "Privacy Icon",
            text = stringResource(R.string.setting_screen_privacy_setting)
        ) {
            viewModel.openPrivacyScreen()
        }

        Spacer(modifier = Modifier.height(20.dp))

        SettingCardDesignView(
            paint = painterResource(id = R.drawable.ic_setting_terms_of_use_icon),
            paintContentDescription = "terms icon",
            text = stringResource(R.string.setting_terms_of_use_text),
        ) {
            loadUriData(context, OPEN_JUSTLY_URL)
        }

        Spacer(modifier = Modifier.height(20.dp))

        SettingCardDesignView(
            paint = painterResource(id = R.drawable.ic_setting_acknowledgements_icon),
            paintContentDescription = "Acknowledgements Icon",
            text = stringResource(R.string.setting_acknowledgements_text),
        ) {
            loadUriData(context, OPEN_ACKNOWLEDGMENT_URL)
        }

        Spacer(modifier = Modifier.height(20.dp))
    }
}

@Composable
fun AccountSettingsView(viewModel: SettingsViewModel, cardBackground: Color) {
    Text(
        text = stringResource(R.string.setting_account_tab_text),
        style = AppTheme.typography.todoActivityTitleTextStyle,
        color = ComposeTheme.colors.textPrimary,
        textAlign = TextAlign.Start,
        modifier = Modifier.fillMaxWidth().padding(top = 32.dp, start = 20.dp, end = 20.dp)
    )

    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 20.dp, vertical = 16.dp)
            .clip(shape = RoundedCornerShape(16.dp))
            .motionClickEvent { viewModel.openSignOutDialog() }
            .background(cardBackground, RoundedCornerShape(16.dp))
    ) {
        val openSignOutDialogEvent by viewModel.openSignOutDialog.collectAsState()
        val openSignOutDialog =
            openSignOutDialogEvent.getContentIfNotHandled() ?: false

        Text(
            text = stringResource(R.string.setting_screen_sign_out_dialogue_confirm_btn_text),
            style = AppTheme.typography.tabTextStyle,
            textAlign = TextAlign.Center,
            color = ComposeTheme.colors.primary,
            lineHeight = 19.sp,
            modifier = Modifier.padding(16.dp)
        )

        if (openSignOutDialog) {
            ShowSignOutDialog(viewModel)
        }
    }
}

@Composable
fun ShowSignOutDialog(viewModel: SettingsViewModel) {
    CustomAlertDialog(
        title = stringResource(R.string.setting_screen_sign_out_dialogue_title_text),
        subTitle = stringResource(R.string.setting_screen_sign_out_dialogue_message_text),
        confirmBtnText = stringResource(R.string.setting_screen_sign_out_dialogue_confirm_btn_text),
        dismissBtnText = stringResource(R.string.setting_screen_sign_out_dialogue_cancel_btn_text),
        onConfirmClick = { viewModel.logOutUser() },
        onDismissClick = { viewModel.closeSignOutDialog() },
        confirmBtnColor = SignOutTextColor
    )
}
