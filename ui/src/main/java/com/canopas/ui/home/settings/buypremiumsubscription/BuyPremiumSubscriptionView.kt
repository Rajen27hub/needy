package com.canopas.ui.home.settings.buypremiumsubscription

import android.app.Activity
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.Close
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.BackArrowTint
import com.canopas.base.ui.SignOutTextColor
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.PlanType
import com.canopas.ui.R

const val DEVICE_LIMIT_TEXT_TYPE = 1
const val ACTIVITY_LIMIT_TEXT_TYPE = 2

@Composable
fun BuyPremiumSubscriptionView(
    showBackArrow: Boolean = true,
    showDismissButton: Boolean = false,
    headerTextType: Int = 0,
) {
    val viewModel = hiltViewModel<BuyPremiumSubscriptionViewModel>()

    if (headerTextType == DEVICE_LIMIT_TEXT_TYPE) {
        BackHandler(enabled = true, onBack = {
            // Do Nothing
            // Block user from using System's Back Action
        })
    }
    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart()
    })

    val state by viewModel.state.collectAsState()

    Scaffold(topBar = {
        BuildTopBar(
            viewModel, showBackArrow, showDismissButton, headerTextType == DEVICE_LIMIT_TEXT_TYPE
        )
    }) {
        state.let { buyPremiumState ->
            when (buyPremiumState) {
                is BuyPremiumSubscriptionViewModel.BuyPremiumState.FAILURE -> {
                    if (buyPremiumState.isNetworkError) {
                        NetworkErrorView {
                            viewModel.onStart()
                        }
                    } else {
                        ServerErrorView {
                            viewModel.onStart()
                        }
                    }
                }
                is BuyPremiumSubscriptionViewModel.BuyPremiumState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(if (isDarkMode) colors.background else backgroundColorLight)
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is BuyPremiumSubscriptionViewModel.BuyPremiumState.SUCCESS -> {
                    FeaturedBuyPremiumSubscriptionView(
                        viewModel,
                        headerTextType
                    )
                }
                else -> {}
            }
        }
    }
}

@Composable
fun FeaturedBuyPremiumSubscriptionView(
    viewModel: BuyPremiumSubscriptionViewModel,
    headerTextType: Int
) {
    val planOptions by viewModel.planOptions.collectAsState()
    val yearlyPlan by remember {
        mutableStateOf(planOptions.find { it.type == PlanType.PLAN_YEARLY })
    }
    val yearlyDiscountedPrice = (yearlyPlan?.priceMicros?.div((1000000 * 12))).toString()
    val monthlyDiscountedPrice = viewModel.getProductAmount(yearlyPlan!!, yearlyDiscountedPrice)
    val context = LocalContext.current
    LazyColumn(
        modifier = Modifier
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        item {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(colors.background),
                contentAlignment = Alignment.Center
            ) {
                Column(
                    modifier = Modifier
                        .widthIn(max = 600.dp)
                        .fillMaxWidth()
                        .wrapContentHeight()
                ) {
                    BuildBodyForNewBuyPremiumView()

                    NewPremiumBoxView(viewModel)

                    val selectedYearlyPlan by viewModel.selectedYearlyPlan.collectAsState()
                    BuildFeaturedPurchaseView(
                        viewModel,
                        headerText = "Try 7 days for free",
                        btnText = "Continue",
                        signOutDevicesText = if (headerTextType == DEVICE_LIMIT_TEXT_TYPE) stringResource(
                            id = R.string.buy_premium_screen_get_premium_subscription_sign_out_text
                        ) else "",
                        onPurchaseBtnClick = {
                            if (selectedYearlyPlan) {
                                viewModel.onPremiumBtnClicked(
                                    context as Activity,
                                    PlanType.PLAN_YEARLY
                                )
                            } else {
                                viewModel.onPremiumBtnClicked(
                                    context as Activity,
                                    PlanType.PLAN_MONTHLY
                                )
                            }
                        },
                        signOutOtherDevicesOnClick = { if (headerTextType == DEVICE_LIMIT_TEXT_TYPE) viewModel.logOutFromOtherDevices() }
                    )
                }
            }
        }
    }
}

@Composable
fun BuildTopBar(
    viewModel: BuyPremiumSubscriptionViewModel,
    showBackArrow: Boolean,
    showDismissButton: Boolean,
    showSignOutIcon: Boolean
) {
    val openSignOutDialogEvent by viewModel.openSignOutDialog.collectAsState()
    val openSignOutDialog = openSignOutDialogEvent.getContentIfNotHandled() ?: false
    if (openSignOutDialog) {
        ShowSignOutDialog(viewModel)
    }

    TopAppBar(
        content = {
            TopAppBarContent(
                title = "",
                navigationIconOnClick = { viewModel.managePopBack(topBarBackClicked = true) },
                enableNavigationIcon = showBackArrow,
                actions = {
                    if (showDismissButton) {
                        IconButton(onClick = { viewModel.managePopBack(topBarBackClicked = false) }) {
                            Icon(
                                imageVector = Icons.Outlined.Close,
                                tint = colors.textSecondary,
                                contentDescription = "close icon",
                                modifier = Modifier.padding(16.dp)
                            )
                        }
                    } else if (showSignOutIcon) {
                        Text(
                            text = stringResource(R.string.buy_premium_screen_sign_out_text),
                            color = BackArrowTint,
                            style = AppTheme.typography.topBarButtonTextStyle,
                            modifier = Modifier
                                .padding(end = 8.dp)
                                .clickable(
                                    interactionSource = MutableInteractionSource(),
                                    indication = null,
                                    onClick = {
                                        viewModel.openSignOutDialog()
                                    },
                                )
                        )
                    }
                }
            )
        },
        backgroundColor = colors.background, contentColor = colors.textPrimary, elevation = 0.dp
    )
}

@Composable
fun ShowSignOutDialog(viewModel: BuyPremiumSubscriptionViewModel) {
    CustomAlertDialog(
        title = stringResource(R.string.setting_screen_sign_out_dialogue_title_text),
        subTitle = stringResource(R.string.setting_screen_sign_out_dialogue_message_text),
        confirmBtnText = stringResource(R.string.setting_screen_sign_out_dialogue_confirm_btn_text),
        dismissBtnText = stringResource(R.string.setting_screen_sign_out_dialogue_cancel_btn_text),
        onConfirmClick = { viewModel.logOutUser() },
        onDismissClick = { viewModel.closeSignOutDialog() },
        confirmBtnColor = SignOutTextColor
    )
}

@Preview
@Composable
fun PreviewBuyPremiumSubscriptionView() {
    BuyPremiumSubscriptionView()
}

private val backgroundColorLight = Color(0xFFF6F6F6)
private val purchaseCardBgDark = Color(0xff222222)
