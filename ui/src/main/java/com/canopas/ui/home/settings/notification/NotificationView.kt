package com.canopas.ui.home.settings.notification

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Card
import androidx.compose.material.Switch
import androidx.compose.material.SwitchDefaults
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.DividedLineView
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.base.ui.uncheckSwitchBg
import com.canopas.ui.R

@Composable
fun NotificationView() {
    val viewModel = hiltViewModel<NotificationViewModel>()
    val state by viewModel.state.collectAsState()
    val context = LocalContext.current

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart()
    })

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(if (isDarkMode) colors.background else lightBackground),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(id = R.string.setting_screen_notification_label),
                    navigationIconOnClick = { viewModel.managePopBack() },
                )
            },
            backgroundColor = if (isDarkMode) colors.background else lightBackground,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
        LaunchedEffect(key1 = state) {
            state.let {
                if (state is NotificationState.FAILURE) {
                    val message = (state as NotificationState.FAILURE).message
                    showBanner(context, message)
                }
            }
        }
        NotificationScreenView(viewModel = viewModel)
    }
}

@Composable
fun NotificationScreenView(viewModel: NotificationViewModel) {
    val pushNotificationState by viewModel.pushNotificationEnabled.collectAsState()
    val emailNotificationState by viewModel.emailNotificationEnabled.collectAsState()
    val inActivePushNotificationState by viewModel.inActivePushNotificationEnabled.collectAsState()
    val inActiveEmailNotificationState by viewModel.inActiveEmailNotificationEnabled.collectAsState()
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .padding(horizontal = 20.dp)
            .verticalScroll(scrollState),
        verticalArrangement = Arrangement.Top
    ) {
        NotificationSettingsHeaderText(
            stringResource(R.string.notification_screen_weekly_summary_header_text),
            stringResource(R.string.notification_screen_weekly_summary_sub_header_text)
        )

        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp)
                .clip(RoundedCornerShape(16.dp))
                .align(Alignment.CenterHorizontally)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(if (isDarkMode) darkCardBackground else colors.background)

            ) {
                NotificationSettingsItem(
                    text = stringResource(R.string.notifications_screen_push_label),
                    checkedState = pushNotificationState,
                    onCheckChanged = { viewModel.updatePushNotification(it) }
                )

                DividedLineView()

                NotificationSettingsItem(
                    text = stringResource(R.string.notification_screen_email_label),
                    checkedState = emailNotificationState,
                    onCheckChanged = { viewModel.onEmailNotificationSwitchAction(it) }
                )
            }
        }

        NotificationSettingsHeaderText(
            stringResource(R.string.notification_screen_inactive_reminder_header_text),
            stringResource(R.string.notification_screen_inactive_reminder_sub_header_text),
            topPadding = 40.dp
        )
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp)
                .clip(RoundedCornerShape(16.dp))
                .align(Alignment.CenterHorizontally)
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .background(if (isDarkMode) darkCardBackground else colors.background)

            ) {
                NotificationSettingsItem(
                    text = stringResource(R.string.notifications_screen_push_label),
                    checkedState = inActivePushNotificationState,
                    onCheckChanged = { viewModel.updatePushInActiveReminderNotification(it) }
                )

                DividedLineView()

                NotificationSettingsItem(
                    text = stringResource(R.string.notification_screen_email_label),
                    checkedState = inActiveEmailNotificationState,
                    onCheckChanged = { viewModel.updateEmailInactiveReminderNotification(it) }
                )
            }
        }
        Spacer(modifier = Modifier.height(20.dp))
    }
}

@Composable
fun NotificationSettingsItem(text: String, checkedState: Boolean, onCheckChanged: (Boolean) -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        Text(
            text = text,
            style = AppTheme.typography.subTitleTextStyle1,
            lineHeight = 18.sp,
            color = colors.textPrimary,
            modifier = Modifier
                .weight(0.1f)
                .padding(vertical = 16.dp)
        )
        Switch(
            checked = checkedState,
            onCheckedChange = {
                onCheckChanged(it)
            },
            colors = SwitchDefaults.colors(
                checkedThumbColor = White,
                checkedTrackColor = colors.primary,
                uncheckedThumbColor = White,
                uncheckedTrackColor = if (isDarkMode) colors.textSecondary else uncheckSwitchBg,
                checkedTrackAlpha = 1.0f
            )
        )
    }
}

@Composable
fun NotificationSettingsHeaderText(headerText: String, subHeaderText: String, topPadding: Dp = 28.dp) {
    Row(
        modifier = Modifier
            .padding(top = topPadding)
    ) {
        Text(
            text = headerText,
            style = AppTheme.typography.topBarTitleTextStyle,
            lineHeight = 24.sp,
            color = colors.textPrimary
        )
    }
    Row(
        modifier = Modifier
            .padding(top = 4.dp)
    ) {
        Text(
            text = subHeaderText,
            style = AppTheme.typography.bodyTextStyle2,
            lineHeight = 16.sp,
            color = colors.textSecondary
        )
    }
}

@Preview
@Composable
fun PreviewNotification() {
    NotificationView()
}
private val lightBackground = Color(0xFFF7F7F7)
private val darkCardBackground = Color(0xFF222222)
