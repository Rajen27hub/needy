package com.canopas.ui.home.explore.storyView

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.material3.Card
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.data.utils.Utils.Companion.encodeToBase64
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.customview.SecondaryOutlineButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.parse
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.data.model.Details
import com.canopas.ui.R
import com.canopas.ui.customview.jetpackview.SvgImageView

@Composable
fun StoryActivityCard(viewModel: StoryViewModel, detail: Details) {
    val activityDetail = detail.activity ?: return
    Card(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(top = 20.dp, start = 20.dp, end = 20.dp)
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .background(color = Color.parse(activityDetail.color2)),
            verticalAlignment = Alignment.CenterVertically
        ) {

            SvgImageView(
                imageUrl = activityDetail.image_url2,
                modifier = Modifier
                    .size(80.dp)
                    .padding(start = 16.dp, top = 12.dp, bottom = 12.dp)
            )
            Text(
                text = activityDetail.name,
                style = AppTheme.typography.tabTextStyle,
                color = activityNameText,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .padding(start = 8.dp)
                    .weight(1f)
            )

            if (detail.is_subscribed) {
                SecondaryOutlineButton(
                    modifier = Modifier
                        .wrapContentSize()
                        .padding(start = 8.dp, end = 16.dp),
                    border = BorderStroke(
                        1.dp,
                        addedTextBorder
                    ),
                    text = stringResource(R.string.story_activity_added_text),
                    shape = RoundedCornerShape(50.dp),
                    colors = ButtonDefaults.outlinedButtonColors(contentColor = Color.Transparent),
                    color = addedActivityText,
                    icon = painterResource(id = R.drawable.ic_check_icon),
                    iconModifier = Modifier.padding(end = 2.dp),
                )
            } else {
                SecondaryOutlineButton(
                    onClick = {
                        viewModel.getActivityTimeAndDuration(
                            activityDetail.id,
                            encodeToBase64(activityDetail.name),
                            detail.story_id
                        )
                    },
                    modifier = Modifier
                        .wrapContentSize()
                        .padding(start = 8.dp, end = 16.dp),
                    text = stringResource(R.string.story_activity_add_text),
                    shape = RoundedCornerShape(50.dp),
                    colors = ButtonDefaults.outlinedButtonColors(contentColor = textColors, backgroundColor = colors.primary),
                    color = textColors,
                    icon = painterResource(id = R.drawable.ic_pluse_icon),
                    iconModifier = Modifier.padding(end = 2.dp),
                )
            }
        }
    }
}

@Composable
fun CustomActivityCard(viewModel: StoryViewModel, storyId: Int) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 40.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {

        Text(
            text = stringResource(R.string.explore_screen_custom_activity_message_text),
            style = AppTheme.typography.subTitleTextStyle1,
            color = colors.textPrimary,
            lineHeight = 18.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
        )

        Button(
            onClick = { viewModel.navigateToCustomActivityView(storyId) },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = ColorPrimary, contentColor = textColors
            ),
            elevation = null,
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(horizontal = 20.dp)
                .height(48.dp)
                .motionClickEvent { }
        ) {
            Text(
                text = stringResource(R.string.explore_screen_try_creating_custom_activity_btn_text),
                color = textColors,
                style = AppTheme.typography.buttonStyle
            )
        }
    }
}

private val textColors = Color(0xffffffff)
private val addedActivityText = Color(0xff47A96E)
private val addedTextBorder = Color(0xCC47A96E)
private val activityNameText = Color(0xff1C191F)
