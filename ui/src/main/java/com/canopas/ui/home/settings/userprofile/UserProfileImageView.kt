package com.canopas.ui.home.settings.userprofile

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.launch
import androidx.appcompat.app.AlertDialog
import androidx.compose.foundation.Image
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.canhub.cropper.CropImageContract
import com.canhub.cropper.CropImageContractOptions
import com.canhub.cropper.CropImageOptions
import com.canopas.base.data.model.UserAccount
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.ui.R
import id.zelory.compressor.Compressor
import kotlinx.coroutines.launch
import timber.log.Timber
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException

@Composable
fun UserProfileImageView(viewModel: UserProfileViewModel, user: UserAccount) {

    val context = LocalContext.current
    val scope = rememberCoroutineScope()
    val openProfileImageChooserEvent by viewModel.openProfileImageChooserEvent.collectAsState()
    val openProfileImageChooser = openProfileImageChooserEvent.getContentIfNotHandled() ?: false

    var imageUri by remember {
        mutableStateOf<Uri?>(null)
    }

    val imageCropLauncher = rememberLauncherForActivityResult(
        contract = CropImageContract()
    ) { result ->
        if (result.isSuccessful) {
            imageUri = result.uriContent
            result.uriContent?.let {
                val fileName = "IMG_${System.currentTimeMillis()}.jpg"
                val imageFile = File(context.filesDir, fileName)
                scope.launch {
                    imageFile.copyFrom(context, it)
                    val compressedImageFile = Compressor.compress(context, imageFile)
                    viewModel.uploadProfileImage(compressedImageFile)
                }
            }
        } else {
            viewModel.showErrorForImage(result.error)
        }
    }

    val imagePickerLauncher =
        rememberLauncherForActivityResult(contract = ActivityResultContracts.GetContent()) { uri: Uri? ->
            val cropOptions = CropImageContractOptions(uri, CropImageOptions())
            imageCropLauncher.launch(cropOptions)
        }

    val cameraLauncher = rememberLauncherForActivityResult(
        contract = ActivityResultContracts.TakePicturePreview()
    ) { cameraBitmap ->
        cameraBitmap?.let {
            val fileName = "IMG_${System.currentTimeMillis()}.jpg"
            val imageFile = File(context.filesDir, fileName)
            try {
                val out = FileOutputStream(imageFile)
                it.compress(Bitmap.CompressFormat.JPEG, 100, out)
                out.flush()
                out.close()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            viewModel.uploadProfileImage(imageFile)
        }
    }

    if (openProfileImageChooser) {
        AlertDialog
            .Builder(context)
            .setItems(R.array.profile_image_sources) { _, which ->
                when (which) {
                    0 -> {
                        cameraLauncher.launch()
                    }
                    1 -> {
                        imagePickerLauncher.launch("image/*")
                    }
                    2 -> {
                        viewModel.removeProfile()
                    }
                }
            }
            .show()
    }

    val profileImage by viewModel.pendingUpdatedProfileImage.collectAsState()
    val configuration = LocalConfiguration.current
    val picBorder = if (ComposeTheme.isDarkMode) darkProfileBorder else lightProfileBorder

    Box(modifier = Modifier.fillMaxWidth()) {
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .align(Alignment.Center)
                .padding(42.dp)
                .padding(horizontal = 20.dp)
        ) {
            val imageHeight = (configuration.screenHeightDp * 0.18).dp

            if (!user.profileImageUrl.isNullOrEmpty()) {
                com.canopas.base.ui.ProfileImageView(
                    data = user.profileImageUrl,
                    modifier = Modifier
                        .size(imageHeight)
                        .motionClickEvent {
                            viewModel.openProfileImageChanger()
                        }
                        .border(2.dp, picBorder, CircleShape),
                    char = user.profileImageChar()
                )
            } else {
                val setProfile =
                    if (profileImage != null) {
                        profileImage
                    } else {
                        R.drawable.ic_unknown_profile
                    }

                Image(
                    painter = rememberAsyncImagePainter(
                        ImageRequest.Builder(LocalContext.current).data(
                            setProfile
                        ).build()
                    ),
                    modifier = Modifier
                        .size(imageHeight)
                        .motionClickEvent {
                            viewModel.openProfileImageChanger()
                        }
                        .border(2.dp, picBorder, CircleShape)
                        .clip(CircleShape),
                    contentScale = ContentScale.Crop,
                    contentDescription = "ProfileImage"
                )
            }
            Image(
                painter = painterResource(id = R.drawable.ic_edit_user_profile),
                contentDescription = null,
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .size((imageHeight.value * 0.30).dp)
                    .padding(end = 8.dp)
                    .motionClickEvent {
                        viewModel.openProfileImageChanger()
                    }
            )
        }
        Spacer(modifier = Modifier.width(40.dp))
    }
}

fun File.copyFrom(context: Context, imageUri: Uri) {
    try {
        val descriptor = context.contentResolver.openFileDescriptor(imageUri, "r")
        FileInputStream(
            descriptor!!.fileDescriptor
        ).use { input ->
            this.outputStream().use { output ->
                input.copyTo(output)
            }
        }
        descriptor.close()
    } catch (e: IOException) {
        Timber.e("Exception while fetching selected file:-$e")
    }
}

private val lightProfileBorder = Color(0xFFD9D9DA)
private val darkProfileBorder = Color(0xFF888888)
