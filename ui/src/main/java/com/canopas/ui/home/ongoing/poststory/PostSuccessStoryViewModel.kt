package com.canopas.ui.home.ongoing.poststory

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.PostSuccessStory
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class PostSuccessStoryViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val analytics: AppAnalytics,
    private val navManager: NavManager,
    private val resources: Resources
) : ViewModel() {

    val title = MutableStateFlow("")
    val description = MutableStateFlow("")
    private val isEditModeEnable = true
    val storyState = MutableStateFlow<PostStoryState>(PostStoryState.START)
    val enablePostButton = MutableStateFlow(false)

    fun onTitleChange(newValue: String) {
        title.tryEmit(newValue)
        enablePostButton()
    }

    fun onDescriptionChange(newValue: String) {
        description.tryEmit(newValue)
        enablePostButton()
    }

    private fun postSuccessStory(activityId: Int) =
        viewModelScope.launch {
            storyState.tryEmit(PostStoryState.LOADING)

            val postSuccessStory = PostSuccessStory(
                title = title.value,
                description = description.value,
                listOf(activityId)
            )

            withContext(appDispatcher.IO) {
                service.postSuccessStory(postSuccessStory).onSuccess {
                    analytics.logEvent("success_story_posted", null)
                    storyState.tryEmit(PostStoryState.POSTED)
                }
                    .onFailure { e ->
                        Timber.e(e)
                        storyState.tryEmit(PostStoryState.FAILURE(e.toUserError(resources)))
                    }
            }
        }

    fun fetchSuccessStory(storyId: String) {
        viewModelScope.launch {
            withContext(appDispatcher.IO) {
                if (storyId.isNotEmpty() && isEditModeEnable) {
                    service.retrieveSuccessStoryDetailById(storyId.toInt()).onSuccess {
                        title.value = it.title
                        description.value = it.description
                    }.onFailure { e ->
                        Timber.e(e)
                        storyState.tryEmit(PostStoryState.FAILURE(e.toUserError(resources)))
                    }
                }
            }
        }
    }

    private fun updateSuccessStory(storyId: String, activityId: Int) {
        viewModelScope.launch {
            val postSuccessStory = PostSuccessStory(
                title = title.value,
                description = description.value,
                listOf(activityId)
            )
            service.updateSuccessStory(storyId.toInt(), postSuccessStory)
                .onSuccess {
                    storyState.tryEmit(PostStoryState.COMPLETED)
                }.onFailure { e ->
                    Timber.e(e)
                    storyState.tryEmit(PostStoryState.FAILURE(e.toUserError(resources)))
                }
        }
    }

    fun handleSaveButtonClick(storyId: String, activityId: Int, isEditModeEnable: Boolean) {
        if (isEditModeEnable) {
            updateSuccessStory(storyId, activityId)
        } else {
            postSuccessStory(activityId)
        }
    }

    fun managePopBackExplore() {
        navManager.navigateBackExplore()
    }

    fun managePopBack() {
        navManager.popBack()
    }

    private fun enablePostButton() {
        enablePostButton.value = title.value != "" && description.value != ""
    }
}

sealed class PostStoryState {
    object START : PostStoryState()
    object LOADING : PostStoryState()
    object POSTED : PostStoryState()
    object COMPLETED : PostStoryState()
    data class FAILURE(val message: String) : PostStoryState()
}
