package com.canopas.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.ui.Event
import com.canopas.data.event.ActivitiesTabSelectedEvent
import com.canopas.data.event.DarkModeUpdateEvent
import com.canopas.ui.navigation.LoginScreen
import com.canopas.ui.navigation.MainScreen
import com.canopas.ui.navigation.NavManager
import com.canopas.ui.navigation.Screen
import com.canopas.ui.navigation.SettingsScreen
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val navManager: NavManager,
    private val eventBus: EventBus,
    private val preferences: NoLonelyPreferences,
    private val appAnalytics: AppAnalytics
) : ViewModel() {

    val isDarkModeEnabled = MutableStateFlow(false)
    val navigateTo = MutableStateFlow(Event(""))
    val nonBottomBarNonAdjustableRoute = listOf(
        Screen.PostStory.route,
    )
    var currentRoute = MainScreen.Explore.route
    val nonBottomBarRequiredRoute = listOf(
        Screen.Congratulation.route,
        LoginScreen.SignInMethod.route,
        LoginScreen.PhoneLogin.route,
        LoginScreen.LoginOtp.route,
        LoginScreen.NotificationPermissionView.route,
        SettingsScreen.UserProfile.route,
        SettingsScreen.BuyPremium.route,
        Screen.EditScheduleView.route,
        Screen.CustomActivityView.route,
        Screen.AddNotesView.route,
        Screen.VideoView.route,
        SettingsScreen.Notifications.route,
        Screen.WhyThisActivity.route,
        Screen.AddOrEditTemplateView.route
    )

    init {
        eventBus.register(this)
        viewModelScope.launch {
            isDarkModeEnabled.value = preferences.getIsDarkModeEnabled()
        }
    }

    override fun onCleared() {
        eventBus.unregister(this)
        super.onCleared()
    }

    fun navigateTo(route: String) {
        when (route) {
            MainScreen.Explore.route -> {
                navManager.navigateBackExplore()
                navigateTo.value = Event(route)
            }
            MainScreen.Current.route -> {
                navManager.navigateBackCurrent()
                navigateTo.value = Event(route)
            }
            MainScreen.Community.route -> {
                navManager.navigateBackCommunity()
                navigateTo.value = Event(route)
            }
        }
    }

    fun updateRoute(route: String) {
        currentRoute = route
    }

    fun onStart(
        navigateToCommunityView: Boolean,
        communityId: String?
    ) {
        if (navigateToCommunityView) {
            updateRoute(MainScreen.Community.route)
            navigateTo(MainScreen.Community.route)
            appAnalytics.logEvent("tap_join_request_notification")
        } else {
            if (communityId != null) {
                updateRoute(MainScreen.Community.route)
                appAnalytics.logEvent("tap_accepted_request_notification")
                navManager.navigateToCommunityDetailScreen(communityId)
            }
        }
    }

    fun onNewIntent(
        navigateToCommunityView: Boolean,
        communityId: String?
    ) {
        if (navigateToCommunityView) {
            updateRoute(MainScreen.Community.route)
            navigateTo(MainScreen.Community.route)
            appAnalytics.logEvent("tap_join_request_notification")
        } else if (communityId != null) {
            updateRoute(MainScreen.Community.route)
            navManager.navigateToCommunityDetailScreen(communityId)
            appAnalytics.logEvent("tap_accepted_request_notification")
        } else {
            updateRoute(MainScreen.Explore.route)
            navigateTo(MainScreen.Explore.route)
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onExploreTabSelected(event: ActivitiesTabSelectedEvent) {
        updateRoute(MainScreen.Current.route)
        navigateTo(MainScreen.Current.route)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onDarkModeUpdated(event: DarkModeUpdateEvent) {
        isDarkModeEnabled.tryEmit(event.isDarkModeEnabled)
    }
}
