package com.canopas.ui.home.explore.activitylist

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.parse
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.ActivityData
import com.canopas.ui.R
import com.canopas.ui.customview.jetpackview.SvgImageView
import com.google.android.gms.common.util.DeviceProperties.isTablet

const val CONDITIONAL_INDEX_ZERO = 0
const val CONDITIONAL_INDEX_ONE = 1
const val CONDITIONAL_INDEX_TWO = 2
const val CONDITIONAL_INDEX_STEPS = 3
const val SMALL_CARD_SIZE_FOR_TABLET = 250
const val LARGE_CARD_SIZE_FOR_TABLET = 320
const val SMALL_CARD_SIZE_FOR_MOBILE = 170
const val LARGE_CARD_SIZE_FOR_MOBILE = 220

@Composable
fun ActivityListView(categoryId: Int) {
    val viewModel = hiltViewModel<ActivityListViewModel>()
    LaunchedEffect(key1 = Unit, block = {
        viewModel.fetchActivities(categoryId)
    })
    val state by viewModel.state.collectAsState()
    val categoryName by viewModel.categoryName.collectAsState()

    Scaffold(
        topBar = {
            TopAppBar(
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp,
                content = {
                    TopAppBarContent(
                        title = categoryName,
                        navigationIconOnClick = { viewModel.popBack() }
                    )
                }
            )
        }
    ) {
        state.let { activityState ->
            when (activityState) {
                ActivityState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is ActivityState.FAILURE -> {
                    if (activityState.isNetworkError) {
                        NetworkErrorView {
                            viewModel.fetchActivities(categoryId)
                        }
                    } else {
                        ServerErrorView {
                            viewModel.fetchActivities(categoryId)
                        }
                    }
                }
                is ActivityState.SUCCESS -> {
                    val activities = activityState.activities
                    ActivitiesSuccessScreen(activities, viewModel)
                }
            }
        }
    }
}

@Composable
fun ActivitiesSuccessScreen(activities: List<ActivityData>, viewModel: ActivityListViewModel) {
    val context = LocalContext.current
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState)
            .background(colors.background),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
        ) {
            val startPaddingModifier = Modifier
                .weight(1f)
                .padding(start = 8.dp)
            val endPaddingModifier = Modifier
                .weight(1f)
                .padding(end = 8.dp)
            val isTablet = isTablet(context)
            ActivityCardSizeCalculation(
                activities,
                CONDITIONAL_INDEX_ZERO,
                true,
                modifier = startPaddingModifier
            )
            ActivityCardSizeCalculation(
                activities,
                CONDITIONAL_INDEX_ONE,
                false,
                modifier = if (isTablet) Modifier.weight(1f) else endPaddingModifier
            )
            if (isTablet) {
                ActivityCardSizeCalculation(
                    activities,
                    CONDITIONAL_INDEX_TWO,
                    true,
                    modifier = endPaddingModifier
                )
            }
        }

        CustomActivityCard(viewModel = viewModel)
    }
}

@Composable
fun CustomActivityCard(viewModel: ActivityListViewModel) {
    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(top = 20.dp, bottom = 40.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(12.dp)
    ) {

        Text(
            text = stringResource(R.string.explore_screen_custom_activity_message_text),
            style = AppTheme.typography.subTitleTextStyle1,
            color = colors.textPrimary,
            lineHeight = 18.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 28.dp)
        )

        Button(
            onClick = { viewModel.navigateToAddActivity() },
            shape = RoundedCornerShape(50),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = colors.primary,
                contentColor = White
            ),
            elevation = null,
            modifier = Modifier
                .padding(horizontal = 20.dp)
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .height(48.dp)
                .motionClickEvent { }
        ) {
            Text(
                text = stringResource(R.string.explore_screen_try_creating_custom_activity_btn_text),
                color = White,
                style = AppTheme.typography.buttonStyle
            )
        }
    }
}

@Composable
fun ActivityCardSizeCalculation(
    activities: List<ActivityData>,
    conditionalIndex: Int,
    isSmallCard: Boolean,
    modifier: Modifier
) {
    val context = LocalContext.current
    var cardSize = isSmallCard
    if (isTablet(context)) {
        var newIndex = conditionalIndex
        Column(modifier = modifier) {
            Spacer(modifier = Modifier.height(20.dp))
            activities.forEachIndexed { index, activity ->
                if (newIndex == index) {
                    ActivityCard(activity = activity, isSmallCard = cardSize)
                    cardSize = cardSize != true
                    newIndex += CONDITIONAL_INDEX_STEPS
                }
            }
        }
    } else {
        Column(modifier = modifier) {
            Spacer(modifier = Modifier.height(20.dp))
            activities.forEachIndexed { index, activity ->
                if (index % CONDITIONAL_INDEX_TWO == conditionalIndex) {
                    ActivityCard(activity = activity, isSmallCard = cardSize)
                    cardSize = cardSize != true
                }
            }
        }
    }
}

@Composable
fun ActivityCard(
    activity: ActivityData,
    isSmallCard: Boolean
) {
    val context = LocalContext.current
    val height = if (isTablet(context)) {
        if (isSmallCard) SMALL_CARD_SIZE_FOR_TABLET.dp else LARGE_CARD_SIZE_FOR_TABLET.dp
    } else {
        if (isSmallCard) SMALL_CARD_SIZE_FOR_MOBILE.dp else LARGE_CARD_SIZE_FOR_MOBILE.dp
    }
    val viewModel = hiltViewModel<ActivityListViewModel>()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 12.dp)
            .motionClickEvent {
                viewModel.onActivitySelected(activity)
            }
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 4.dp)
                .height(height),
            verticalArrangement = Arrangement.Top, horizontalAlignment = Alignment.Start
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .clip(shape = RoundedCornerShape(10.dp))
                    .background(
                        color = Color
                            .parse(if (activity.color2.isEmpty()) activity.color.trim() else activity.color2.trim()),
                        shape = RoundedCornerShape(10.dp)
                    )
            ) {
                activity.image_url2?.let {
                    SvgImageView(
                        imageUrl = it.ifEmpty { activity.image_url },
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(horizontal = 24.dp, vertical = 24.dp)
                            .align(Alignment.Center)
                    )
                }
            }
        }

        Column(modifier = Modifier.padding(vertical = 16.dp)) {
            Text(
                text = activity.name.trim(),
                color = colors.textPrimary,
                style = AppTheme.typography.subTitleTextStyle1,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )

            Text(
                text = activity.description,
                style = AppTheme.typography.bodyTextStyle2,
                color = if (isDarkMode) subtextColor else colors.textSecondary
            )
        }
    }
}

private val subtextColor = Color(0x99FFFFFF)
