package com.canopas.ui.home.ongoing.addActivityView

import androidx.compose.foundation.ScrollState
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.parse
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.ActivityData
import com.canopas.ui.customview.jetpackview.SvgImageView
import com.google.android.gms.common.util.DeviceProperties

const val CONDITIONAL_INDEX_ZERO = 0
const val CONDITIONAL_INDEX_ONE = 1
const val CONDITIONAL_INDEX_TWO = 2
const val CONDITIONAL_INDEX_STEPS = 3
const val SMALL_CARD_SIZE_FOR_TABLET = 250
const val LARGE_CARD_SIZE_FOR_TABLET = 320
const val SMALL_CARD_SIZE_FOR_MOBILE = 170
const val LARGE_CARD_SIZE_FOR_MOBILE = 220

@Composable
fun SearchActivityResult(
    activities: List<ActivityData>,
    viewModel: AddActivityViewModel,
    scrollState: ScrollState
) {
    val context = LocalContext.current
    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState)
            .background(ComposeTheme.colors.background),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            modifier = Modifier
                .fillMaxSize()
                .padding(top = 24.dp)
        ) {
            val startPaddingModifier = Modifier
                .weight(1f)
                .padding(start = 8.dp)
            val endPaddingModifier = Modifier
                .weight(1f)
                .padding(end = 8.dp)
            val isTablet = DeviceProperties.isTablet(context)
            ActivityCard(
                viewModel,
                activities,
                CONDITIONAL_INDEX_ZERO,
                true,
                modifier = startPaddingModifier
            )
            ActivityCard(
                viewModel,
                activities,
                CONDITIONAL_INDEX_ONE,
                false,
                modifier = if (isTablet) Modifier.weight(1f) else endPaddingModifier
            )
            if (isTablet) {
                ActivityCard(
                    viewModel,
                    activities,
                    CONDITIONAL_INDEX_TWO,
                    true,
                    modifier = endPaddingModifier
                )
            }
        }
        CustomActivityCard(viewModel = viewModel)
    }
}

@Composable
fun ActivityCard(
    viewModel: AddActivityViewModel,
    activities: List<ActivityData>,
    conditionalIndex: Int,
    isSmallCard: Boolean,
    modifier: Modifier
) {
    val context = LocalContext.current
    var cardSize = isSmallCard
    if (DeviceProperties.isTablet(context)) {
        var newIndex = conditionalIndex
        Column(modifier = modifier) {
            activities.forEachIndexed { index, activity ->
                if (newIndex == index) {
                    ActivityListView(activity = activity, isSmallCard = cardSize) {
                        viewModel.onActivitySelected(activity)
                    }
                    cardSize = cardSize != true
                    newIndex += CONDITIONAL_INDEX_STEPS
                }
            }
        }
    } else {
        Column(modifier = modifier) {
            activities.forEachIndexed { index, activity ->
                if (index % CONDITIONAL_INDEX_TWO == conditionalIndex) {
                    ActivityListView(activity = activity, isSmallCard = cardSize) {
                        viewModel.onActivitySelected(activity)
                    }
                    cardSize = cardSize != true
                }
            }
        }
    }
}
@Composable
fun ActivityListView(
    activity: ActivityData,
    isSmallCard: Boolean,
    onActivityClick: () -> Unit
) {
    val context = LocalContext.current
    val height = if (DeviceProperties.isTablet(context)) {
        if (isSmallCard) SMALL_CARD_SIZE_FOR_TABLET.dp else LARGE_CARD_SIZE_FOR_TABLET.dp
    } else {
        if (isSmallCard) SMALL_CARD_SIZE_FOR_MOBILE.dp else LARGE_CARD_SIZE_FOR_MOBILE.dp
    }
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(horizontal = 12.dp)
            .motionClickEvent(onActivityClick)
    ) {
        Column(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 4.dp)
                .height(height),
            verticalArrangement = Arrangement.Top, horizontalAlignment = Alignment.Start
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .clip(shape = RoundedCornerShape(10.dp))
                    .background(
                        color = Color
                            .parse(if (activity.color2.isEmpty()) activity.color.trim() else activity.color2.trim()),
                        shape = RoundedCornerShape(10.dp)
                    )
            ) {
                activity.image_url2?.let {
                    SvgImageView(
                        imageUrl = it.ifEmpty { activity.image_url },
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(horizontal = 24.dp, vertical = 24.dp)
                            .align(Alignment.Center)
                    )
                }
            }
        }

        Column(modifier = Modifier.padding(vertical = 16.dp)) {
            Text(
                text = activity.name.trim(),
                color = ComposeTheme.colors.textPrimary,
                style = AppTheme.typography.subTitleTextStyle1,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )

            Text(
                text = activity.description,
                style = AppTheme.typography.bodyTextStyle2,
                color = if (ComposeTheme.isDarkMode) subtextColor else ComposeTheme.colors.textSecondary
            )
        }
    }
}

private val subtextColor = Color(0x99FFFFFF)
