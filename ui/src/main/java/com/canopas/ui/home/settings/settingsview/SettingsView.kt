package com.canopas.ui.home.settings.settingsview

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.model.PREMIUM_USER
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.ui.R
import kotlinx.coroutines.launch

const val PLAY_STORE_SUBS_URL = "https://play.google.com/store/account/subscriptions"
const val OPEN_JUSTLY_URL = "https://justly.life/"
const val OPEN_TWITTER_URL = "https://twitter.com/justly_life"
const val OPEN_FACEBOOK_URL = "https://www.facebook.com/justlylife/"
const val OPEN_ACKNOWLEDGMENT_URL = "https://justly.life/acknowledgement-android"
const val GET_SUPPORT_TAB = 1
const val SUGGEST_A_FEATURE_TAB = 2

@Composable
fun SettingsView() {
    val viewModel = hiltViewModel<SettingsViewModel>()
    val scrollState = rememberScrollState()

    val userState by viewModel.state.collectAsState()
    val isUserVerified = userState is SettingsState.AUTHORIZED
    val scrollToTop by viewModel.scrollToTop.collectAsState()
    val coroutineScope = rememberCoroutineScope()

    val isLoadingState = userState is SettingsState.LOADING

    LaunchedEffect(key1 = Unit) {
        viewModel.onStart()
    }
    LaunchedEffect(key1 = scrollToTop, block = {
        coroutineScope.launch {
            if (scrollToTop) {
                scrollState.animateScrollTo(0)
                viewModel.updateScroll()
            }
        }
    })

    val useDarkIcons = isDarkMode
    val backgroundColor = if (useDarkIcons) colors.background else primaryBackgroundLight
    val cardBackground = if (useDarkIcons) tabCardBgDark else tabCardBgLight

    if (isLoadingState) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(colors.background),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator(color = colors.primary)
        }
    } else {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .background(backgroundColor)
                .verticalScroll(scrollState),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {

            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = stringResource(id = R.string.setting_screen_settings_text),
                        navigationIconOnClick = { viewModel.popBack() }
                    )
                },
                backgroundColor = backgroundColor,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )

            Column(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                Text(
                    text = stringResource(id = R.string.setting_profile),
                    style = AppTheme.typography.todoActivityTitleTextStyle,
                    color = colors.textPrimary,
                    textAlign = TextAlign.Start,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 20.dp, top = 12.dp)
                )

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 16.dp, horizontal = 20.dp)
                        .clip(shape = RoundedCornerShape(16.dp))
                        .background(if (isDarkMode) tabCardBgDark else tabCardBgLight)
                ) {
                    if (!isUserVerified) {
                        UnauthorizedProfileView(viewModel)
                    } else {
                        AuthorizedProfileView(viewModel)
                    }
                }

                val userType by viewModel.userType.collectAsState()

                if (userType != PREMIUM_USER) {
                    SettingsBuyPremiumCardView(viewModel = viewModel)
                }

                GeneralSettingsView(
                    viewModel = viewModel,
                    cardBackground = cardBackground,
                    userType = userType,
                    isUserVerified = isUserVerified
                )

                StayInTouchSettingsView(viewModel, cardBackground)

                SettingsAboutView(viewModel = viewModel, cardBackground)

                if (isUserVerified) {
                    AccountSettingsView(viewModel = viewModel, cardBackground)
                }

                Spacer(modifier = Modifier.height(56.dp))
            }
        }
    }
}

@Composable
fun AuthorizedProfileView(viewModel: SettingsViewModel) {
    val authState = viewModel.state.collectAsState().value
    val user = (authState as SettingsState.AUTHORIZED).user
    var userName = user.fullName
    val profileImageUrl = user.profileImageUrl ?: ""
    val isDarkMode = isDarkMode

    Row(
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 12.dp, horizontal = 16.dp)
            .motionClickEvent {
                viewModel.openProfileScreen()
            }
    ) {

        ProfileImageView(
            data = profileImageUrl,
            modifier = Modifier
                .size(66.dp)
                .border(
                    1.dp,
                    if (isDarkMode) colors.textSecondary else profileBorderLight,
                    CircleShape
                ),
            char = user.profileImageChar()
        )

        userName = if (user.firstName.isNullOrEmpty()) {
            stringResource(R.string.setting_screen_set_profile_title_text)
        } else {
            userName.trim().ifEmpty {
                stringResource(R.string.setting_screen_view_profile)
            }
        }

        Column(
            modifier = Modifier
                .padding(start = 16.dp)
                .weight(1f)
        ) {
            Text(
                text = userName,
                style = AppTheme.typography.subTitleTextStyle1,
                color = colors.textPrimary,
            )
        }
        Icon(
            painter = painterResource(id = R.drawable.ic_arrow),
            contentDescription = "Consulting Image",
            modifier = Modifier.padding(horizontal = 8.dp),
            tint = colors.textSecondary
        )
    }
}

@Composable
fun UnauthorizedProfileView(viewModel: SettingsViewModel) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 12.dp, horizontal = 16.dp)
            .motionClickEvent { viewModel.navigateToSignInMethods() },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_unknown_profile),
            contentDescription = "profile Image",
            modifier = Modifier
                .size(66.dp)
                .border(
                    1.dp,
                    if (isDarkMode) colors.textSecondary else profileBorderLight,
                    CircleShape
                )
        )

        Text(
            text = stringResource(R.string.setting_screen_sign_in_btn),
            style = AppTheme.typography.subTitleTextStyle1,
            color = colors.textPrimary,
            modifier = Modifier
                .fillMaxSize()
                .padding(start = 16.dp)
                .weight(1f)
        )

        Icon(
            painter = painterResource(id = R.drawable.ic_arrow),
            contentDescription = "Consulting Image",
            modifier = Modifier
                .padding(end = 8.dp)
                .size(15.dp),
            tint = colors.textSecondary
        )
    }
}

@Composable
fun SettingCardDesignView(
    paint: Painter,
    paintContentDescription: String,
    text: String,
    text2: String? = null,
    onTabClicked: () -> Unit,
) {
    Row(
        modifier = Modifier
            .padding(start = 16.dp)
            .motionClickEvent { onTabClicked() },
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = paint,
            contentDescription = paintContentDescription,
            modifier = Modifier.size(37.dp)
        )
        Column(
            modifier = Modifier
                .padding(start = 16.dp)
                .weight(1f)
                .align(Alignment.CenterVertically)
        ) {
            Text(
                text = text,
                style = AppTheme.typography.subTitleTextStyle1,
                color = colors.textPrimary,
            )
            if (text2 != null) {
                Text(
                    text = text2,
                    style = AppTheme.typography.bodyTextStyle2,
                    color = colors.textSecondary,
                    lineHeight = 17.sp,
                )
            }
        }
        Icon(
            painter = painterResource(id = R.drawable.ic_arrow),
            contentDescription = "Consulting Image",
            tint = colors.textSecondary,
            modifier = Modifier.padding(end = 24.dp)
        )
    }
}

private val primaryBackgroundLight = Color(0xFFF7F7F7)
private val profileBorderLight = Color(0x331C191F)
private val tabCardBgDark = Color(0xff222222)
private val tabCardBgLight = Color(0xffffffff)
