package com.canopas.ui.editsubscription

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.CustomActivityTitleLineView
import com.canopas.base.ui.customview.CustomTextField
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Subscription
import com.canopas.ui.R

@Composable
fun EditSubscriptionView(
    subscriptionId: Int = 0,
) {
    val viewModel = hiltViewModel<EditSubscriptionViewModel>()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(subscriptionId)
    })

    val editState by viewModel.fetchSubscriptionState.collectAsState()

    editState.let { state ->
        when (state) {
            is FetchSubscriptionState.LOADING -> {
                Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
                    CircularProgressIndicator(color = ComposeTheme.colors.primary)
                }
            }
            is FetchSubscriptionState.SUCCESS -> {
                val subscription = state.subscription
                EditSubscriptionSuccessState(viewModel, subscription)
            }
            is FetchSubscriptionState.FAILURE -> {
                val message = state.message
                showBanner(context = LocalContext.current, message)
                viewModel.popBack()
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun EditSubscriptionSuccessState(
    viewModel: EditSubscriptionViewModel,
    subscription: Subscription
) {
    val context = LocalContext.current
    val setTime by viewModel.showActivityTimeSelector.collectAsState()
    val enableUpdateButton by viewModel.enableUpdateButton.collectAsState()
    val showLoader by viewModel.showLoader.collectAsState()
    val navigateToSettingsEvent by viewModel.navigateToAppSettings.collectAsState()
    val navigateToSettings = navigateToSettingsEvent.getContentIfNotHandled() ?: false
    val title by viewModel.title.collectAsState()

    val keyboardController = LocalSoftwareKeyboardController.current
    DisposableEffect(key1 = Unit, effect = {
        onDispose {
            keyboardController?.hide()
        }
    })
    val showShortTitleError by viewModel.showShortTitleError.collectAsState()

    val interactionSource = remember { MutableInteractionSource() }
    val isFocused by interactionSource.collectIsFocusedAsState()

    if (navigateToSettings) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", context.packageName, null)
        intent.data = uri
        context.startActivity(intent)
    }

    val openNotificationDialog = viewModel.openNotificationDialog.collectAsState()

    if (openNotificationDialog.value) {
        ShowNotificationDialog(viewModel)
    }

    Scaffold(topBar = {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = subscription.activity.name,
                    navigationIconOnClick = { viewModel.popBack() }
                )
            },
            backgroundColor = ComposeTheme.colors.background,
            contentColor = ComposeTheme.colors.textPrimary,
            elevation = 0.dp
        )
    }) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            LazyColumn(
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .fillMaxWidth()
                    .weight(1f),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                item {
                    if (title.isNotEmpty() || subscription.is_custom) {
                        Text(
                            text = stringResource(id = R.string.custom_activity_title_textview_text),
                            style = AppTheme.typography.configureActivityHeaderStyle,
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(top = 28.dp, start = 20.dp),
                            textAlign = TextAlign.Start,
                            letterSpacing = -(0.8.sp),
                            color = ComposeTheme.colors.textPrimary
                        )

                        CustomTextField(
                            text = title,
                            onTextChange = {
                                viewModel.onTitleChange(it)
                            },
                            interactionSource = interactionSource,
                            modifier = Modifier
                                .motionClickEvent { }
                                .fillMaxWidth()
                                .padding(top = 8.dp, start = 20.dp, end = 20.dp),
                            textStyle = AppTheme.typography.subTextStyle1.copy(color = ComposeTheme.colors.textSecondary),
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Text,
                                imeAction = ImeAction.Next
                            ),
                            maxLines = 4,
                            cursorBrush = SolidColor(ComposeTheme.colors.primary)
                        )

                        CustomActivityTitleLineView(
                            Modifier
                                .fillMaxWidth()
                                .padding(start = 20.dp, end = 20.dp),
                            if (isFocused) ComposeTheme.colors.primary else {
                                if (ComposeTheme.isDarkMode) darkTabBackground else lightTabBackground
                            }
                        )

                        AnimatedVisibility(showShortTitleError) {
                            Text(
                                text = stringResource(id = R.string.global_short_input_error_text),
                                color = MaterialTheme.colors.error,
                                style = AppTheme.typography.captionTextStyle,
                                modifier = Modifier
                                    .padding(top = 4.dp, start = 24.dp)
                                    .fillMaxWidth()
                            )
                        }
                    }
                }

                item {
                    EditActivityTime(viewModel, setTime, subscription)
                }

                item {
                    EditActivityDuration(viewModel)
                }

                item {
                    EditRepeatSchedule(viewModel, context)
                }

                item {
                    if (setTime) {
                        EditReminderView(viewModel, context)
                    }
                }

                item {
                    EditNotesPrompt(viewModel)
                    Spacer(modifier = Modifier.height(50.dp))
                }
            }
            UpdateButtonView(showLoader, viewModel, enableUpdateButton)
        }
    }
}

private val lightTabBackground = Color(0xffFAF7F6)
private val darkTabBackground = Color(0xff313131)
