package com.canopas.ui.editsubscription

import android.content.res.Resources
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.utils.Device
import com.canopas.base.ui.Event
import com.canopas.base.ui.model.DurationTabType
import com.canopas.base.ui.model.RepeatTabStyle
import com.canopas.data.model.ActivityReminders
import com.canopas.data.model.CustomActivityData
import com.canopas.data.model.SubscribeBody
import com.canopas.data.model.Subscription
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.customactivity.MINIMUM_CHARACTER_LENGTH
import com.canopas.ui.habit.configureactivity.TYPE_ACTIVITY_REMINDER_FIVE_MIN_BEFORE_ACTIVITY
import com.canopas.ui.habit.configureactivity.TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_END
import com.canopas.ui.habit.configureactivity.TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_START
import com.canopas.ui.navigation.NavManager
import com.canopas.ui.utils.DateUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.Calendar
import javax.inject.Inject

@HiltViewModel
class EditSubscriptionViewModel @Inject constructor(
    private val appAnalytics: AppAnalytics,
    private val calendar: Calendar,
    private val navManager: NavManager,
    private val notificationManagerCompat: NotificationManagerCompat,
    private val device: Device,
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val resources: Resources
) : ViewModel() {

    val showLoader = MutableStateFlow(false)
    val openNotificationDialog = MutableStateFlow(false)
    val navigateToAppSettings = MutableStateFlow(Event(false))
    val editSubscriptionState = MutableStateFlow<EditSubscriptionState>(EditSubscriptionState.IDLE)
    val fetchSubscriptionState = MutableStateFlow<FetchSubscriptionState>(FetchSubscriptionState.LOADING)
    val showActivityTimeSelector = MutableStateFlow(true)
    val selectedActivityTime = MutableStateFlow<String?>(null)
    val durationTabType = MutableStateFlow(DurationTabType.DURATION_30_MIN)
    val repeatTabType = MutableStateFlow(RepeatTabStyle.REPEAT_DAILY)
    private val subscriptionTabType = MutableStateFlow(RepeatTabStyle.REPEAT_DAILY)
    val selectedWeeklyDays = MutableStateFlow<List<Int>>(emptyList())
    val selectedMonthlyDays = MutableStateFlow<List<Int>>(emptyList())
    val selectedReminderTypes = MutableStateFlow<List<Int>>(emptyList())
    val promptForNote = MutableStateFlow(false)
    val enableUpdateButton = MutableStateFlow(false)
    var subscription: Subscription? = null
    val title = MutableStateFlow("")
    val showShortTitleError = MutableStateFlow(false)
    val showMinimumSelectionText = MutableStateFlow(false)

    init {
        selectedWeeklyDays.value = (0..6).map { 1 }
        selectedMonthlyDays.value = (0..30).map { if (it == 6 || it == 13 || it == 20) 1 else 0 }
        selectedReminderTypes.value = (0..2).map { if (it == 0) 1 else 0 }
    }

    fun setActivityTimeSelector(enabled: Boolean) {
        if (showActivityTimeSelector.value == enabled) return
        if (enabled) {
            selectedActivityTime.value = subscription?.activityTimeOrNull() ?: "08:00"
            val newList = ArrayList(selectedReminderTypes.value)
            newList[0] = 1
            selectedReminderTypes.tryEmit(newList)
        } else {
            selectedActivityTime.tryEmit(null)
            selectedReminderTypes.tryEmit(selectedReminderTypes.value.map { 0 })
        }
        showActivityTimeSelector.tryEmit(enabled)
        enableUpdateButton.tryEmit(checkPreviousData())
    }

    fun handleDurationTabSelection(selectedTab: DurationTabType) {
        durationTabType.tryEmit(selectedTab)
        if (durationTabType.value == DurationTabType.DURATION_1_MIN) {
            val newList = ArrayList(selectedReminderTypes.value)
            newList[2] = 0
            selectedReminderTypes.tryEmit(newList)
        }
        enableUpdateButton.tryEmit(checkPreviousData())
    }

    fun handleRepeatTabSelection(selectedTab: RepeatTabStyle) {
        repeatTabType.tryEmit(selectedTab)
        when (selectedTab) {
            RepeatTabStyle.REPEAT_DAILY -> {
                showMinimumSelectionText.tryEmit(false)
            }
            RepeatTabStyle.REPEAT_WEEKLY -> {
                appAnalytics.logEvent("tap_habit_configure_daily_config")
                showMinimumSelectionText.tryEmit(selectedWeeklyDays.value.none { it == 1 })
            }
            RepeatTabStyle.REPEAT_MONTHLY -> {
                appAnalytics.logEvent("tap_habit_configure_monthly_settings")
                showMinimumSelectionText.tryEmit(selectedMonthlyDays.value.none { it == 1 })
            }
        }
        enableUpdateButton.tryEmit(checkPreviousData())
    }

    fun toggleReminderSelection(selectedType: Int) {
        val newList = ArrayList(selectedReminderTypes.value)
        newList[selectedType] = newList[selectedType].xor(1)
        selectedReminderTypes.tryEmit(newList)
        val subscription = subscription ?: return
        val apiReminderList = getSubscriptionReminderTypes(subscription)
        enableUpdateButton.tryEmit(apiReminderList != newList || checkPreviousData())
    }

    fun toggleNotesPrompt(enabled: Boolean) {
        promptForNote.tryEmit(enabled)
        enableUpdateButton.tryEmit(checkPreviousData())
    }

    fun toggleWeeklySelection(selectedIndex: Int) {
        val newList = ArrayList(selectedWeeklyDays.value)
        newList[selectedIndex] = newList[selectedIndex].xor(1)
        selectedWeeklyDays.tryEmit(newList)
        showMinimumSelectionText.tryEmit((selectedWeeklyDays.value.none { it == 1 }))
        enableUpdateButton.tryEmit(!showMinimumSelectionText.value)
        val subscription = subscription ?: return
        if (subscription.repeat_type == 1 && !subscription.repeat_on.isNullOrEmpty() && !(selectedWeeklyDays.value.none { it == 1 })) {
            enableUpdateButton.tryEmit(getWeeklyRepeatDays(subscription.repeat_on!!) != newList || checkPreviousData())
        }
    }

    fun toggleMonthlySelection(selectedIndex: Int) {
        val newList = ArrayList(selectedMonthlyDays.value)
        newList[selectedIndex] = newList[selectedIndex].xor(1)
        selectedMonthlyDays.tryEmit(newList)
        showMinimumSelectionText.tryEmit((selectedMonthlyDays.value.none { it == 1 }))
        enableUpdateButton.tryEmit(!showMinimumSelectionText.value)
        val subscription = subscription ?: return
        if (subscription.repeat_type == 2 && !subscription.repeat_on.isNullOrEmpty() && !(selectedMonthlyDays.value.none { it == 1 })) {
            enableUpdateButton.tryEmit(getWeeklyRepeatDays(subscription.repeat_on!!) != newList || checkPreviousData())
        }
    }

    fun setActivityTime(time: String) {
        selectedActivityTime.tryEmit(time)
        enableUpdateButton.tryEmit(checkPreviousData())
    }

    override fun onCleared() {
        super.onCleared()
        calendar.set(Calendar.HOUR_OF_DAY, DateUtils.getSystemCurrentHour())
        calendar.set(Calendar.MINUTE, DateUtils.getSystemCurrentMinute())
    }

    fun onStart(subscriptionId: Int) {
        appAnalytics.logEvent("view_habit_configure", null)
        fetchSubscriptionData(subscriptionId)
    }

    private fun fetchSubscriptionData(subscriptionId: Int) {
        viewModelScope.launch {
            fetchSubscriptionState.tryEmit(FetchSubscriptionState.LOADING)
            withContext(appDispatcher.IO) {
                service.retrieveUserSubscriptionById(subscriptionId).onSuccess {
                    subscription = it
                    if (it.is_custom) {
                        title.tryEmit(it.activity.name)
                    }
                    setEditScreenData(it)
                    fetchSubscriptionState.tryEmit(FetchSubscriptionState.SUCCESS(it))
                }.onFailure { e ->
                    Timber.e(e.toString())
                    fetchSubscriptionState.tryEmit(FetchSubscriptionState.FAILURE(e.toUserError(resources)))
                }
            }
        }
    }

    private fun setEditScreenData(subscription: Subscription) {
        showActivityTimeSelector.tryEmit(!subscription.activity_time.isNullOrEmpty())
        selectedActivityTime.tryEmit(subscription.activityTimeOrNull())
        when (subscription.activity_duration) {
            0, 1 -> durationTabType.tryEmit(DurationTabType.DURATION_1_MIN)
            15 -> durationTabType.tryEmit(DurationTabType.DURATION_15_MIN)
            30 -> durationTabType.tryEmit(DurationTabType.DURATION_30_MIN)
            45 -> durationTabType.tryEmit(DurationTabType.DURATION_45_MIN)
            60 -> durationTabType.tryEmit(DurationTabType.DURATION_1_HOUR)
        }
        setSubscriptionRepeatDays(subscription)
        selectedReminderTypes.tryEmit(getSubscriptionReminderTypes(subscription))
        promptForNote.tryEmit(subscription.show_note_on_complete)
    }

    private fun getSubscriptionReminderTypes(subscription: Subscription): List<Int> {
        val reminderTypes = subscription.reminders
        val list = mutableListOf(0, 0, 0)
        if (reminderTypes != null) {
            for (reminderType in reminderTypes) {
                when (reminderType.type) {
                    TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_START -> list[0] = 1
                    TYPE_ACTIVITY_REMINDER_FIVE_MIN_BEFORE_ACTIVITY -> list[1] = 1
                    TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_END -> list[2] = 1
                }
            }
        }
        return list
    }

    private fun setSubscriptionRepeatDays(subscription: Subscription) {
        when (subscription.repeat_type) {
            1 -> {
                val repeatOn = subscription.repeat_on
                if (!repeatOn.isNullOrEmpty()) {
                    repeatTabType.tryEmit(RepeatTabStyle.REPEAT_WEEKLY)
                    subscriptionTabType.tryEmit(RepeatTabStyle.REPEAT_WEEKLY)
                    selectedWeeklyDays.tryEmit(getWeeklyRepeatDays(repeatOn))
                } else {
                    repeatTabType.tryEmit(RepeatTabStyle.REPEAT_DAILY)
                    subscriptionTabType.tryEmit(RepeatTabStyle.REPEAT_DAILY)
                }
            }
            2 -> {
                repeatTabType.tryEmit(RepeatTabStyle.REPEAT_MONTHLY)
                subscriptionTabType.tryEmit(RepeatTabStyle.REPEAT_MONTHLY)
                val repeatOn = subscription.repeat_on!!
                selectedMonthlyDays.tryEmit(getMonthlyRepeatDays(repeatOn))
            }
        }
    }

    private fun getMonthlyRepeatDays(repeatOn: String): List<Int> {
        val monthlyRepeatDays =
            repeatOn.filterNot { it.isWhitespace() }.split(',')
                .map { it.toInt() }
        val list = MutableList(31) { 0 }
        monthlyRepeatDays.filter { it in 1..31 }.forEach { list[it - 1] = 1 }
        return list
    }

    private fun getWeeklyRepeatDays(repeatOn: String): List<Int> {
        val weeklyRepeatDays =
            repeatOn.filterNot { it == ' ' }.split(",").map { it.toInt() }
        val apiResponse = ArrayList(weeklyRepeatDays)
        val selectedDays = MutableList(7) { 0 }
        if (apiResponse.contains(7)) {
            selectedDays[0] = 1
            apiResponse.remove(7)
        }
        apiResponse.map { selectedDays[it] = 1 }
        return selectedDays
    }

    fun getNearestMinute(minute: Int): Int {
        val remainder = minute % 15
        return if (remainder <= 7) minute - remainder else minute + (15 - remainder)
    }

    private fun getRepeatOnValue(): String {
        return when (repeatTabType.value) {
            RepeatTabStyle.REPEAT_WEEKLY -> {
                val days = (selectedWeeklyDays.value.drop(1) + selectedWeeklyDays.value.take(1)).mapIndexed { index, i ->
                    if (i == 1) (index + 1) else null
                }.filterNotNull()
                days.joinToString()
            }
            RepeatTabStyle.REPEAT_MONTHLY -> {
                val days = selectedMonthlyDays.value.mapIndexed { index, i ->
                    if (i == 1) (index + 1) else null
                }.filterNotNull()
                days.joinToString()
            }
            else -> { "" }
        }
    }

    private fun getRemindersList(): List<ActivityReminders> {
        val activityTimeString = selectedActivityTime.value ?: return emptyList()
        val time = activityTimeString.filterNot { it.isWhitespace() }.take(5).split(":")
            .map { it.toInt() }
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm")
        val list = ArrayList<ActivityReminders>()
        selectedReminderTypes.value.forEachIndexed { index, i ->
            if (i == 1) {
                when (index) {
                    0 -> {
                        list.add(
                            ActivityReminders(
                                TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_START,
                                activityTimeString.filterNot { it.isWhitespace() }.take(5)
                            )
                        )
                    }
                    1 -> {
                        val reminderTime = LocalTime.of(time[0], time[1]).minusMinutes(5)
                        list.add(
                            ActivityReminders(
                                TYPE_ACTIVITY_REMINDER_FIVE_MIN_BEFORE_ACTIVITY,
                                formatter.format(reminderTime)
                            )
                        )
                    }
                    2 -> {
                        val reminderTime = LocalTime.of(time[0], time[1])
                            .plusMinutes(durationTabType.value.duration.toLong())
                        list.add(
                            ActivityReminders(
                                TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_END,
                                formatter.format(reminderTime)
                            )
                        )
                    }
                }
            }
        }
        return list
    }

    fun openNotificationDialog() {
        openNotificationDialog.tryEmit(true)
    }

    fun closeNotificationDialog() {
        openNotificationDialog.tryEmit(false)
    }

    fun addNotificationPermission() {
        navigateToAppSettings.value = Event(true)
        openNotificationDialog.tryEmit(false)
    }

    fun isNotificationOn(): Boolean {
        return notificationManagerCompat.areNotificationsEnabled()
    }

    fun popBack() {
        navManager.popBack()
    }

    fun handleUpdateSubscriptionBtn() {
        if ((selectedReminderTypes.value.none { it == 1 }) || isNotificationOn()) {
            val subscription = subscription ?: return
            updateSubscription(subscription)
        } else {
            openNotificationDialog()
        }
    }

    private fun updateSubscription(subscription: Subscription) = viewModelScope.launch {
        val activityTime = selectedActivityTime.value?.filterNot { it.isWhitespace() }?.take(5)
        val repeatType = if (repeatTabType.value == RepeatTabStyle.REPEAT_MONTHLY) 2 else 1
        editSubscriptionState.tryEmit(EditSubscriptionState.PROCESSING)
        showLoader.tryEmit(true)
        val customActivityData = CustomActivityData(name = title.value)
        val subscriptionBody = SubscribeBody(
            activity_id = subscription.activity_id,
            timezone = device.timeZone(),
            is_prompted = false,
            repeat_type = repeatType,
            repeat_on = getRepeatOnValue(),
            activity_time = activityTime,
            activity_duration = durationTabType.value.duration,
            reminders = getRemindersList(),
            show_note_on_complete = promptForNote.value,
            custom_activity = if (title.value.isNotEmpty()) customActivityData else null
        )
        withContext(appDispatcher.IO) {
            service.updateSubscriptionBodyBySubscriptionId(subscription.id, subscriptionBody)
                .onSuccess {
                    editSubscriptionState.tryEmit(EditSubscriptionState.CONFIGURED)
                    withContext(appDispatcher.MAIN) {
                        popBack()
                    }
                }.onFailure { e ->
                    Timber.e(e)
                    editSubscriptionState.tryEmit(EditSubscriptionState.FAILURE(e.toUserError(resources)))
                    showLoader.tryEmit(false)
                }
        }
    }

    fun onTitleChange(value: String) {
        title.tryEmit(value)
        showShortTitleError.tryEmit(title.value.trim().length < MINIMUM_CHARACTER_LENGTH)
        enableUpdateButton.tryEmit(checkPreviousData())
    }

    private fun checkPreviousData(): Boolean {
        val subscription = subscription ?: return false
        val activityTitleCondition = subscription.activity.name != title.value && title.value.trim().length >= MINIMUM_CHARACTER_LENGTH
        val selectedTime = selectedActivityTime.value?.filterNot { it.isWhitespace() }?.take(5)
        val activityTimeCondition = subscription.activity_time != selectedTime
        val activityDurationCondition = durationTabType.value.duration != subscription.activity_duration
        val repeatTypeCondition = if (!subscription.repeat_on.isNullOrEmpty()) {
            val repeatOn = subscription.repeat_on!!.filterNot { it == ' ' }.split(",").map { it.toInt() }
            subscriptionTabType.value != repeatTabType.value && (repeatOn != selectedWeeklyDays.value || repeatOn != selectedMonthlyDays.value)
        } else {
            subscriptionTabType.value != repeatTabType.value && !showMinimumSelectionText.value
        }
        val promptForNoteCondition = promptForNote.value != subscription.show_note_on_complete
        return (activityTitleCondition || activityTimeCondition || repeatTypeCondition || promptForNoteCondition || activityDurationCondition)
    }
}

sealed class EditSubscriptionState {
    object IDLE : EditSubscriptionState()
    object PROCESSING : EditSubscriptionState()
    object CONFIGURED : EditSubscriptionState()
    data class FAILURE(val message: String) : EditSubscriptionState()
}

sealed class FetchSubscriptionState {
    object LOADING : FetchSubscriptionState()
    data class SUCCESS(val subscription: Subscription) : FetchSubscriptionState()
    data class FAILURE(val message: String) : FetchSubscriptionState()
}
