package com.canopas.ui.editsubscription

import android.content.Context
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.model.DurationTabType
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.ui.R

@Composable
fun EditReminderView(
    viewModel: EditSubscriptionViewModel,
    context: Context,
) {

    val selectedReminderTypes by viewModel.selectedReminderTypes.collectAsState()
    val reminderTypes by remember {
        mutableStateOf(context.resources.getStringArray(R.array.reminder_types))
    }
    val selectedDuration by viewModel.durationTabType.collectAsState()

    Text(
        text = stringResource(R.string.configure_activity_need_reminder_title_text),
        style = AppTheme.typography.configureActivityHeaderStyle,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 40.dp, start = 20.dp),
        textAlign = TextAlign.Start,
        letterSpacing = -(0.8.sp),
        color = ComposeTheme.colors.textPrimary
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 20.dp, end = 20.dp, top = 20.dp)
            .border(
                1.dp,
                color = if (ComposeTheme.isDarkMode) darkTabBackground.copy(0.8f) else lightTabBackground.copy(
                    0.8f
                ),
                shape = RoundedCornerShape(18.dp)
            )
    ) {
        reminderTypes.forEachIndexed { index, reminderType ->
            if ((selectedDuration == DurationTabType.DURATION_1_MIN && index == 2) || index != 0) return@forEachIndexed
            // if (index == reminderTypes.lastIndex || (selectedDuration == DurationTabType.DURATION_1_MIN && index == 1)) 20.dp else 0.dp
            // Add above condition in bottom padding for Row() when all options are available.
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(start = 12.dp, end = 12.dp, top = 8.dp, bottom = 8.dp)
                    .motionClickEvent {
                        viewModel.toggleReminderSelection(index)
                    },
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_baseline_check_circle),
                    contentDescription = null,
                    modifier = Modifier
                        .size(30.dp),
                    colorFilter = ColorFilter.tint(
                        if (selectedReminderTypes[index] == 1) ColorPrimary else ComposeTheme.colors.textPrimary.copy(
                            0.25f
                        )
                    )
                )

                Text(
                    text = reminderType,
                    style = AppTheme.typography.configureSubItemsStyle,
                    color = ComposeTheme.colors.textPrimary
                )
            }
        }
    }
}

@Composable
fun UpdateButtonView(
    showLoader: Boolean,
    viewModel: EditSubscriptionViewModel,
    enableUpdateButton: Boolean,
) {
    Box(
        Modifier
            .offset(y = -(12.dp))
            .height(12.dp)
            .fillMaxWidth()
    ) {
        Box(
            Modifier
                .fillMaxSize()
                .background(
                    Brush.verticalGradient(
                        listOf(
                            Color.Transparent,
                            ComposeTheme.colors.background
                        )
                    )
                )
        )
    }

    PrimaryButton(
        onClick = {
            viewModel.handleUpdateSubscriptionBtn()
        },
        shape = RoundedCornerShape(50),
        modifier = Modifier
            .widthIn(max = 600.dp)
            .motionClickEvent { }
            .fillMaxWidth(),
        colors = ButtonDefaults.buttonColors(
            backgroundColor = ComposeTheme.colors.primary,
            contentColor = White,
            disabledBackgroundColor = ComposeTheme.colors.primary.copy(0.6f)
        ),
        enabled = enableUpdateButton,
        text = stringResource(R.string.configure_activity_update_btn_text),
        isProcessing = showLoader
    )

    Spacer(modifier = Modifier.height(20.dp))
}

private val lightTabBackground = Color(0xffFAF7F6)
private val darkTabBackground = Color(0xff313131)
