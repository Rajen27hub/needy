package com.canopas.ui.customactivity

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.CustomActivityTitleLineView
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomTextField
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.onFocusSelectAll
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.FeatureFlag.isGoalsEnabled
import com.canopas.ui.R

const val MINIMUM_CHARACTER_LENGTH = 3

@Composable
fun CustomActivityView(storyId: Int = 0) {
    val viewModel = hiltViewModel<CustomActivityViewModel>()
    val context = LocalContext.current
    val state by viewModel.customActivityState.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(
            context.getString(R.string.custom_activity_default_activity_text),
            storyId
        )
    })

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        navigationIconOnClick = { viewModel.managePopBack() }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        state.let { state ->
            when (state) {
                is CustomActivityState.START -> {
                    CustomActivityScreen(viewModel, storyId)
                }
                is CustomActivityState.LOADING, is CustomActivityState.IDLE -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }

                is CustomActivityState.ERROR -> {
                    val message = state.message
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                        viewModel.managePopBack()
                    }
                }

                is CustomActivityState.ADD -> {
                    LaunchedEffect(key1 = Unit) {
                        viewModel.managePopBack()
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun CustomActivityScreen(
    viewModel: CustomActivityViewModel,
    storyId: Int,
) {

    val title by viewModel.title.collectAsState()
    val focusManager = LocalFocusManager.current
    val keyboardController = LocalSoftwareKeyboardController.current

    DisposableEffect(key1 = Unit, effect = {
        onDispose {
            if (!isGoalsEnabled) {
                keyboardController?.hide()
            }
        }
    })

    val scrollState = rememberScrollState()
    val showShortTitleError by viewModel.showShortTitleError.collectAsState()

    LaunchedEffect(key1 = Unit, key2 = showShortTitleError, block = {
        if (showShortTitleError) {
            scrollState.animateScrollTo(0)
        }
    })

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
            .clickable(
                interactionSource = MutableInteractionSource(),
                indication = null,
                onClick = {
                    keyboardController?.hide()
                    focusManager.clearFocus()
                }
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .padding(horizontal = 20.dp)
                .weight(1f)
                .verticalScroll(scrollState),
            verticalArrangement = Arrangement.Center,
        ) {
            val focusRequester = remember {
                FocusRequester()
            }
            val interactionSource = remember { MutableInteractionSource() }
            val isFocused by interactionSource.collectIsFocusedAsState()

            LaunchedEffect(key1 = Unit, block = {
                focusRequester.requestFocus()
            })

            Text(
                text = stringResource(R.string.custom_activity_view_whats_your_activity_name_text),
                color = colors.textSecondary,
                style = AppTheme.typography.subTitleTextStyle1,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .fillMaxWidth()
            )

            val textFieldValueState = remember {
                mutableStateOf(TextFieldValue(title, selection = TextRange(title.length)))
            }

            CustomTextField(
                value = textFieldValueState.value,
                onValueChange = {
                    textFieldValueState.value = it
                    viewModel.onTitleChange(it.text)
                },
                interactionSource = interactionSource,
                modifier = Modifier
                    .motionClickEvent { }
                    .focusRequester(focusRequester)
                    .onFocusSelectAll(textFieldValueState)
                    .padding(top = 40.dp, start = 20.dp, end = 20.dp)
                    .fillMaxWidth(),
                textStyle = AppTheme.typography.h2TextStyle.copy(
                    color = colors.textPrimary,
                    textAlign = TextAlign.Center,
                    lineHeight = 29.sp,
                    letterSpacing = -(0.96.sp)
                ),
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Next
                ),
                maxLines = 4,
                cursorBrush = SolidColor(colors.primary)
            )

            CustomActivityTitleLineView(
                Modifier.fillMaxWidth(),
                if (isFocused) colors.primary else if (isDarkMode) dividerColorDark else dividerColorLight
            )

            AnimatedVisibility(showShortTitleError) {
                Text(
                    text = stringResource(id = R.string.global_short_input_error_text),
                    color = MaterialTheme.colors.error,
                    style = AppTheme.typography.captionTextStyle,
                    modifier = Modifier
                        .padding(top = 4.dp, start = 24.dp)
                        .fillMaxWidth(),
                    textAlign = TextAlign.Center
                )
            }
        }

        Box(
            Modifier
                .offset(y = -(20.dp))
                .height(20.dp)
                .fillMaxWidth()
        ) {
            Box(
                Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            listOf(
                                Color.Transparent,
                                colors.background
                            )
                        )
                    )
            )
        }

        PrimaryButton(
            onClick = { viewModel.onStartJourneyButtonClicked(storyId) },
            shape = RoundedCornerShape(50),
            enabled = title.trim().length >= MINIMUM_CHARACTER_LENGTH,
            modifier = Modifier
                .widthIn(max = 600.dp)
                .motionClickEvent { }
                .align(Alignment.CenterHorizontally)
                .fillMaxWidth(),
            colors = ButtonDefaults.buttonColors(
                backgroundColor = colors.primary,
                disabledBackgroundColor = colors.primary.copy(
                    0.6f
                ),
                contentColor = White,
            ),
            text = stringResource(R.string.custom_activity_start_journey_btn_text)
        )

        Spacer(modifier = Modifier.height(20.dp))
    }
}

@Preview
@Composable
fun PreviewAddCustomActivity() {
    CustomActivityView()
}

private val dividerColorDark = Color(0xFF313131)
private val dividerColorLight = Color(0x991C191F)
