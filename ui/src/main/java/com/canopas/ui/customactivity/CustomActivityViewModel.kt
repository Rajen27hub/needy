package com.canopas.ui.customactivity

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.utils.Utils.Companion.encodeToBase64
import com.canopas.data.FeatureFlag.isGoalsEnabled
import com.canopas.ui.habit.MAX_SUBSCRIPTIONS_FOR_FREE_USER
import com.canopas.ui.home.settings.buypremiumsubscription.ACTIVITY_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CustomActivityViewModel @Inject constructor(
    private val navManager: NavManager,
    private val authManager: AuthManager,
    private val preferencesDataStore: NoLonelyPreferences,
    private val appAnalytics: AppAnalytics
) : ViewModel() {

    val title = MutableStateFlow("")
    val customActivityState = MutableStateFlow<CustomActivityState>(CustomActivityState.IDLE)
    val showShortTitleError = MutableStateFlow(false)
    private var requestState: RequestState = RequestState.None

    fun onStart(activityName: String, storyId: Int) {
        handleRequestState(storyId)
        setActivityName(activityName)
    }

    private fun setActivityName(activityName: String) {
        title.tryEmit(activityName)
        customActivityState.tryEmit(CustomActivityState.START)
    }

    fun onTitleChange(newValue: String) {
        title.value = newValue
        showShortTitleError.tryEmit(title.value.trim().length < MINIMUM_CHARACTER_LENGTH)
    }

    private fun handleRequestState(storyId: Int) {
        if (requestState == RequestState.Login && authManager.authState.isVerified) {
            onStartJourneyButtonClicked(storyId)
        }
        if (requestState == RequestState.Premium && authManager.authState.isPremium) {
            onStartJourneyButtonClicked(storyId)
        }
        requestState = RequestState.None
    }

    fun openBuyPremiumScreen() {
        navManager.navigateToBuyPremiumScreen(textType = ACTIVITY_LIMIT_TEXT_TYPE)
    }

    fun managePopBack() {
        navManager.popBack()
    }

    fun onStartJourneyButtonClicked(storyId: Int) {
        appAnalytics.logEvent("tap_habit_configure_subscribe", null)
        if (title.value.length < MINIMUM_CHARACTER_LENGTH) {
            showShortTitleError.tryEmit(true)
        } else {
            viewModelScope.launch {
                when {
                    !authManager.authState.isVerified -> {
                        navManager.navigateToSignInMethods()
                        requestState = RequestState.Login
                    }
                    (preferencesDataStore.getUserSubscriptionCount() >= MAX_SUBSCRIPTIONS_FOR_FREE_USER && authManager.authState.isFree) -> {
                        openBuyPremiumScreen()
                    }
                    else -> {
                        appAnalytics.logEvent("tap_habit_configure_start_journey", null)
                        title.tryEmit(title.value.trim())
                        if (isGoalsEnabled) {
                            navManager.navigateToWhyThisActivityScreen(encodeToBase64(title.value), storyId = storyId)
                        } else {
                            navManager.navigateToConfigureCustomActivity(encodeToBase64(title.value), storyId = storyId)
                        }
                    }
                }
            }
        }
    }
}

sealed class CustomActivityState {
    object IDLE : CustomActivityState()
    object START : CustomActivityState()
    object LOADING : CustomActivityState()
    object ADD : CustomActivityState()
    data class ERROR(val message: String) : CustomActivityState()
}

enum class RequestState {
    None, Login, Premium
}
