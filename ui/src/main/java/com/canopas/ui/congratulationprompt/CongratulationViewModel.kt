package com.canopas.ui.congratulationprompt

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.CustomActivityData
import com.canopas.data.model.SubscribeBody
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class CongratulationViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    private val navManager: NavManager,
    authManager: AuthManager
) : ViewModel() {
    var userName = MutableStateFlow(authManager.currentUser?.firstName?.trim() ?: "")
    var profile = MutableStateFlow(authManager.currentUser?.profileImageUrl ?: "")

    fun openPostStoryScreen(activityId: Int, activityName: String) {
        navManager.navigateToPostSuccessStory(activityId, activityName)
    }

    fun setIsPrompted(subscriptionId: String) = viewModelScope.launch {
        withContext(appDispatcher.IO) {
            service.retrieveUserSubscriptionById(subscriptionId.toInt()).onSuccess {
                service.updateSubscriptionBodyBySubscriptionId(
                    subscriptionId.toInt(),
                    SubscribeBody(
                        it.activity_id,
                        it.timezone,
                        true,
                        1,
                        repeat_on = null,
                        custom_activity = CustomActivityData(
                            it.activity.name,
                            it.activity.description
                        )
                    )
                ).onFailure { e ->
                    Timber.e(e)
                }
            }.onFailure {
                Timber.e(it)
            }
        }
    }

    fun managePopBack() {
        navManager.popBack()
    }
}
