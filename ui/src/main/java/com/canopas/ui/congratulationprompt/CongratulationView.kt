package com.canopas.ui.congratulationprompt

import androidx.compose.animation.core.Animatable
import androidx.compose.animation.core.FastOutLinearInEasing
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.tween
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Alignment.Companion.Center
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.graphicsLayer
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.rememberAsyncImagePainter
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.InterRegularFont
import com.canopas.base.ui.InterSemiBoldFont
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.SecondaryOutlineButton
import com.canopas.base.ui.textColor
import com.canopas.ui.R
import kotlinx.coroutines.delay

@Composable
fun CongratulationView(
    activityName: String,
    subscriptionId: String,
    activityId: String
) {

    val viewModel = hiltViewModel<CongratulationViewModel>()
    LaunchedEffect(key1 = Unit) {
        viewModel.setIsPrompted(subscriptionId)
    }
    val userName = viewModel.userName.collectAsState().value
    val profileImageUrl = viewModel.profile.collectAsState().value

    val waves = listOf(
        remember { Animatable(0f) },
        remember { Animatable(0f) },
        remember { Animatable(0f) },
        remember { Animatable(0f) },
        remember { Animatable(0f) },
    )

    val animationSpec = infiniteRepeatable<Float>(
        animation = tween(2500, easing = FastOutLinearInEasing),
        repeatMode = RepeatMode.Restart,
    )

    waves.forEachIndexed { index, animatable ->
        LaunchedEffect(animatable) {
            delay(index * 500L)
            animatable.animateTo(
                targetValue = 1f, animationSpec = animationSpec
            )
        }
    }

    val dys = waves.map { it.value }
    val scrollState = rememberScrollState()

    Column(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Box(
                contentAlignment = Center,
                modifier = Modifier.padding(top = 20.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_congratulation_bg),
                    contentDescription = null,
                    contentScale = ContentScale.FillWidth,
                    modifier = Modifier
                        .padding(horizontal = 30.dp)
                        .fillMaxWidth(0.8f)
                )
                Box(modifier = Modifier) {
                    dys.forEach { dy ->
                        Box(
                            Modifier
                                .size(70.dp)
                                .align(Center)
                                .graphicsLayer {
                                    scaleX = dy * 4 + 1
                                    scaleY = dy * 4 + 1
                                    alpha = 1 - dy
                                },
                        ) {
                            Box(
                                Modifier
                                    .fillMaxSize()
                                    .background(
                                        color = CongratulationRightBg,
                                        shape = CircleShape
                                    )
                            )
                        }
                    }
                    Box(
                        Modifier
                            .size(100.dp)
                            .align(Center)
                            .background(color = CongratulationRightBg, shape = CircleShape)
                    ) {

                        if (profileImageUrl != "") {
                            Image(
                                painter = rememberAsyncImagePainter(model = profileImageUrl),
                                contentDescription = "profile pic",
                                contentScale = ContentScale.Crop,
                                modifier = Modifier
                                    .size(100.dp)
                                    .align(Center)
                                    .clip(CircleShape)
                            )
                        } else {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_right),
                                contentDescription = "right icon",
                                tint = Color.White,
                                modifier = Modifier
                                    .size(65.dp)
                                    .padding(10.dp)
                                    .align(Center)
                            )
                        }
                    }
                }
            }
            Text(
                text = stringResource(
                    R.string.congratulation_prompt_text_with_user_name,
                    userName
                ),
                style = AppTheme.typography.h2TextStyle,
                color = textColor,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .padding(start = 16.dp, end = 16.dp, top = 60.dp)
            )

            Text(
                text = buildAnnotatedString {
                    withStyle(
                        style = SpanStyle(
                            color = Color.Black,
                            fontFamily = InterRegularFont,
                            fontSize = 16.sp
                        )
                    ) {
                        append(stringResource(R.string.congratulation_prompt_complete_text))
                    }
                    withStyle(
                        style = SpanStyle(
                            color = textColor,
                            fontFamily = InterSemiBoldFont,
                            fontWeight = FontWeight.SemiBold,
                            fontSize = 16.sp,
                        )
                    ) {
                        append(activityName)
                    }
                    withStyle(
                        style = SpanStyle(
                            color = Color.Black,
                            fontFamily = InterRegularFont,
                            fontSize = 16.sp
                        )
                    ) {
                        append(stringResource(id = R.string.congratulation_prompt_share_story_text))
                    }
                },
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .padding(start = 40.dp, end = 30.dp, top = 10.dp)
            )

            PrimaryButton(
                onClick = {
                    viewModel.openPostStoryScreen(activityId.toInt(), activityName)
                },
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = ColorPrimary,
                    contentColor = Color.White
                ),
                modifier = Modifier
                    .padding(top = 60.dp)
                    .fillMaxWidth()
                    .height(48.dp)
                    .align(Alignment.CenterHorizontally),
                text = stringResource(R.string.congratulation_prompt_share_btn),
            )

            SecondaryOutlineButton(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 12.dp, start = 20.dp, end = 20.dp)
                    .height(48.dp)
                    .align(Alignment.CenterHorizontally),
                onClick = {
                    viewModel.managePopBack()
                },
                border = BorderStroke(
                    0.5.dp,
                    ColorPrimary.copy(0.5f)
                ),
                text = stringResource(R.string.congratulation_prompt_not_now_btn),
                shape = RoundedCornerShape(50),
                color = ColorPrimary
            )
        }
    }
}

@Preview
@Composable
fun PreviewCongratulationView() {
    CongratulationView("", "", "")
}

val CongratulationRightBg = Color(0xFF484F9C)
