package com.canopas.ui.goals.addgoals

import android.app.DatePickerDialog
import android.widget.DatePicker
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AddGoalTextFieldLineView
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Goal
import com.canopas.ui.R
import java.util.Calendar

@Composable
fun GoalDueDateView(viewModel: AddOrUpdateGoalViewModel) {
    val dueDate by viewModel.dueDate.collectAsState()
    Text(
        text = stringResource(R.string.add_goal_view_due_date_text),
        style = AppTheme.typography.bodyTextStyle3,
        color = ComposeTheme.colors.textSecondary,
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp)
    )

    val context = LocalContext.current as AppCompatActivity
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 8.dp)
            .motionClickEvent {
                showDatePicker(
                    activity = context,
                    goalDueDate = dueDate
                ) { selectedDateInMillis ->
                    viewModel.setGoalDueDate(selectedDateInMillis)
                }
            },
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Start
    ) {
        Icon(
            painter = painterResource(id = R.drawable.ic_goals_due_date),
            contentDescription = "",
            modifier = Modifier.size(20.dp)
        )
        Text(
            text = dueDate,
            style = AppTheme.typography.subTextStyle1,
            color = ComposeTheme.colors.textPrimary,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 8.dp)
        )
    }

    AddGoalTextFieldLineView(
        Modifier
            .fillMaxWidth()
            .padding(horizontal = 16.dp),
        dueDateDividerColor
    )
}

@Composable
fun GoalProgressView(goal: Goal) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 40.dp, start = 16.dp, end = 16.dp)
    ) {
        Text(
            text = stringResource(R.string.add_goal_view_progress_text),
            style = AppTheme.typography.bodyTextStyle3,
            color = ComposeTheme.colors.textSecondary,
            modifier = Modifier
                .fillMaxWidth()
        )

        Text(
            text = buildAnnotatedString {
                append("${goal.completions}/${goal.occurrences}")
                withStyle(
                    style = SpanStyle(
                        color = ComposeTheme.colors.textSecondary,
                        fontSize = 14.sp,
                    )
                ) {
                    append(stringResource(com.canopas.base.ui.R.string.subscription_status_times_header_text))
                }
            },
            color = ComposeTheme.colors.textPrimary,
            modifier = Modifier.padding(top = 16.dp),
            style = AppTheme.typography.topBarTitleTextStyle,
        )
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 12.dp),
            verticalAlignment = Alignment.CenterVertically
        ) {
            LinearProgressIndicator(
                progress = goal.getProgress(),
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .padding(end = 12.dp)
                    .height(6.dp)
                    .clip(
                        RoundedCornerShape(3.dp)
                    ),
                color = ComposeTheme.colors.primary
            )
            Text(
                text = goal.getProgressText(),
                color = ComposeTheme.colors.textPrimary,
                style = AppTheme.typography.topBarTitleTextStyle,
            )
        }
    }
}

private fun showDatePicker(
    activity: AppCompatActivity,
    goalDueDate: String,
    updatedDate: (Long) -> Unit
) {
    val mCalendar = Calendar.getInstance()
    mCalendar.time = Utils.dueDateFormat.parse(goalDueDate)!!
    val mDatePickerDialog = DatePickerDialog(
        activity,
        { _: DatePicker, year: Int, month: Int, dayOfMonth: Int ->
            mCalendar.set(year, month, dayOfMonth)
            updatedDate(mCalendar.timeInMillis)
        },
        mCalendar.get(Calendar.YEAR),
        mCalendar.get(Calendar.MONTH),
        mCalendar.get(Calendar.DAY_OF_MONTH)
    )
    mDatePickerDialog.datePicker.minDate = System.currentTimeMillis()
    mDatePickerDialog.show()
}

private val dueDateDividerColor = Color(0xFFE2D8D2)
