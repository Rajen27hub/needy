package com.canopas.ui.goals.activityselection

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.data.model.Subscription

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun AddGoalSelectionView() {
    val viewModel = hiltViewModel<AddGoalSelectionViewModel>()
    val state by viewModel.state.collectAsState()
    val shouldPopBack by viewModel.shouldPopBack.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        if (shouldPopBack) {
            viewModel.popBack()
        } else {
            viewModel.getUserSubscriptions()
        }
    })

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = "Select Activity",
                        navigationIconOnClick = { viewModel.popBack() }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        val context = LocalContext.current
        state.let { state ->
            when (state) {
                is SubscriptionState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(colors.background)
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is SubscriptionState.FAILURE -> {
                    val message = state.message
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                    }
                    viewModel.popBack()
                }
                is SubscriptionState.SUCCESS -> {
                    val subscriptions = state.subscriptions
                    SelectSubscriptionSuccessState(subscriptions, viewModel)
                }
            }
        }
    }
}

@Composable
fun SelectSubscriptionSuccessState(subscriptions: List<Subscription>, viewModel: AddGoalSelectionViewModel) {
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
            .padding(horizontal = 20.dp, vertical = 40.dp)
            .verticalScroll(scrollState)
    ) {
        Spacer(modifier = Modifier.height(20.dp))
        subscriptions.forEach { item ->
            Box(
                modifier = Modifier.fillMaxWidth(),
                contentAlignment = Alignment.Center
            ) {
                Box(
                    modifier = Modifier
                        .padding(vertical = 8.dp)
                        .widthIn(max = 600.dp)
                        .motionClickEvent {
                            viewModel.onActivitySelected(item.id)
                        }
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .clip(shape = RoundedCornerShape(15.dp))
                        .background(
                            color = if (ComposeTheme.isDarkMode) darkActivityCard else lightActivityCard,
                            shape = RoundedCornerShape(16.dp)
                        )
                ) {
                    Row(
                        modifier = Modifier
                            .align(Alignment.CenterStart)
                            .fillMaxWidth(0.8f),
                        verticalAlignment = Alignment.CenterVertically,
                        horizontalArrangement = Arrangement.Start
                    ) {
                        item.activity.image_url2?.let {
                            AsyncImage(
                                model = ImageRequest.Builder(LocalContext.current)
                                    .data(it.ifEmpty { item.activity.image_url })
                                    .decoderFactory(SvgDecoder.Factory())
                                    .build(),
                                modifier = Modifier
                                    .padding(horizontal = 16.dp, vertical = 4.dp)
                                    .size(50.dp),
                                contentDescription = null
                            )
                        }
                        Text(
                            text = item.activity.name,
                            style = AppTheme.typography.bodyTextStyle1,
                            color = colors.textPrimary,
                        )
                    }
                }
            }
        }
    }
}

private val lightActivityCard = Color(0xD1C191F)
private val darkActivityCard = Color(0xFF222222)
