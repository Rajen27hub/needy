package com.canopas.ui.goals.addgoals

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.customview.SecondaryOutlineButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.GOAL_WALL_VISIBILITY_ALWAYS
import com.canopas.data.model.GOAL_WALL_VISIBILITY_NEVER
import com.canopas.data.model.GOAL_WALL_VISIBILITY_WHEN_ACTIVE
import com.canopas.data.model.Goal
import com.canopas.ui.R

@Composable
fun DeleteGoalButton(goal: Goal, viewModel: AddOrUpdateGoalViewModel) {
    val openDeleteGoalDialogEvent by viewModel.openDeleteDialog.collectAsState()
    val openDeleteGoalDialog = openDeleteGoalDialogEvent.getContentIfNotHandled() ?: false

    Row(Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.Center) {
        SecondaryOutlineButton(
            modifier = Modifier
                .wrapContentSize(),
            onClick = {
                viewModel.openDeleteGoalDialog()
            },
            border = BorderStroke(
                1.dp,
                ComposeTheme.colors.primary
            ),
            text = stringResource(R.string.add_goal_view_delete_btn_text),
            textModifier = Modifier.padding(top = 4.dp, bottom = 4.dp, start = 8.dp, end = 12.dp),
            shape = RoundedCornerShape(24.dp),
            colors = ButtonDefaults.outlinedButtonColors(contentColor = ComposeTheme.colors.primary, backgroundColor = ComposeTheme.colors.background),
            color = ComposeTheme.colors.primary,
            icon = painterResource(id = R.drawable.ic_delete_icon_for_button),
            iconModifier = Modifier.padding(start = 12.dp).size(16.dp),
        )
    }
    if (openDeleteGoalDialog) {
        ShowDeleteGoalDialog(viewModel, goal.id)
    }

    Spacer(modifier = Modifier.height(40.dp))
}

@Composable
fun ShowDeleteGoalDialog(viewModel: AddOrUpdateGoalViewModel, goalId: Int) {
    CustomAlertDialog(
        title = stringResource(R.string.add_goal_view_delete_alert_title_text),
        subTitle = stringResource(R.string.add_goal_view_delete_alert_body_text),
        confirmBtnText = stringResource(R.string.add_goal_view_delete_alert_confirm_btn_text),
        dismissBtnText = stringResource(R.string.add_goal_view_delete_alert_dismiss_btn_text),
        confirmBtnColor = MaterialTheme.colors.error,
        onConfirmClick = { viewModel.deleteGoal(goalId) },
        onDismissClick = { viewModel.closeDeleteGoalDialog() }
    )
}

@Composable
fun WallVisibilityView(viewModel: AddOrUpdateGoalViewModel) {
    val context = LocalContext.current
    val visibilityTypes by remember {
        mutableStateOf(context.resources.getStringArray(R.array.wall_visibility_types))
    }
    val selectedVisibility by viewModel.selectedVisibilityType.collectAsState()

    Column(modifier = Modifier.fillMaxWidth()) {
        Text(
            text = stringResource(R.string.add_goal_view_show_on_wall_heading_text),
            style = AppTheme.typography.bodyTextStyle3,
            color = ComposeTheme.colors.textSecondary,
            modifier = Modifier
                .fillMaxWidth()
                .padding(horizontal = 16.dp)
        )

        Spacer(modifier = Modifier.height(18.dp))

        visibilityTypes.forEachIndexed { index, visibilityType ->
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy(10.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_baseline_check_circle),
                    contentDescription = null,
                    modifier = Modifier
                        .size(30.dp)
                        .motionClickEvent {
                            viewModel.toggleVisibilitySelection((index))
                        },
                    colorFilter = ColorFilter.tint(
                        when {
                            selectedVisibility == GOAL_WALL_VISIBILITY_ALWAYS && index == 0 -> ColorPrimary
                            selectedVisibility == GOAL_WALL_VISIBILITY_WHEN_ACTIVE && index == 1 -> ColorPrimary
                            selectedVisibility == GOAL_WALL_VISIBILITY_NEVER && index == 2 -> ColorPrimary
                            else -> ComposeTheme.colors.textPrimary.copy(0.25f)
                        }
                    )
                )
                Text(
                    text = visibilityType,
                    style = AppTheme.typography.configureSubItemsStyle,
                    color = ComposeTheme.colors.textPrimary
                )
            }
            Spacer(modifier = Modifier.height(20.dp))
        }
    }
}
