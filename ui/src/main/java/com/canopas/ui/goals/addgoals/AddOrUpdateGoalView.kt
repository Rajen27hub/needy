package com.canopas.ui.goals.addgoals

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Check
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.CustomTextField
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.Goal
import com.canopas.data.model.TYPE_END_GOAL
import com.canopas.ui.R.string

@Composable
fun AddOrUpdateGoalView(subscriptionId: Int, goalId: Int? = null) {

    val viewModel = hiltViewModel<AddOrUpdateGoalViewModel>()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(goalId)
    })

    val state by viewModel.state.collectAsState()
    val enableActionButton by viewModel.enableActionButton.collectAsState()
    val showLoader by viewModel.showActionLoader.collectAsState()
    val context = LocalContext.current

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = if (goalId == null) stringResource(string.add_goal_view_add_goal_top_bar_title) else stringResource(
                            string.add_goal_view_update_goal_top_bar_title
                        ),
                        navigationIconOnClick = { viewModel.popBack() },
                        actions = {
                            IconButton(onClick = {
                                if (enableActionButton) {
                                    viewModel.onDoneButtonClick(
                                        subscriptionId,
                                        goalId
                                    )
                                }
                            }) {
                                if (showLoader) {
                                    CircularProgressIndicator(color = colors.primary)
                                } else {
                                    Icon(
                                        Icons.Filled.Check,
                                        contentDescription = null,
                                        tint = if (enableActionButton) colors.textPrimary else colors.textSecondary.copy(
                                            0.3f
                                        )
                                    )
                                }
                            }
                        }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        state.let { goalState ->
            when (goalState) {
                is GoalState.START -> {
                    AddOrUpdateGoal(viewModel)
                }
                is GoalState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(it)
                            .background(colors.background)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is GoalState.FAILURE -> {
                    val message = goalState.message
                    LaunchedEffect(key1 = message) {
                        showBanner(context, message)
                        viewModel.resetState()
                    }
                }
                is GoalState.SUCCESS -> {
                    val goal = goalState.goal
                    AddOrUpdateGoal(
                        viewModel = viewModel,
                        goal = goal
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun AddOrUpdateGoal(
    viewModel: AddOrUpdateGoalViewModel,
    goal: Goal? = null
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    val descriptionInteractionSource = remember { MutableInteractionSource() }
    val scrollState = rememberScrollState()

    val title by viewModel.goalTitle.collectAsState()
    val description by viewModel.goalDescription.collectAsState()

    DisposableEffect(key1 = Unit, effect = {
        onDispose {
            keyboardController?.hide()
        }
    })

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
            .clickable(
                onClick = { keyboardController?.hide() },
                interactionSource = MutableInteractionSource(),
                indication = null
            )
            .verticalScroll(scrollState)
    ) {
        CustomTextField(
            text = title,
            onTextChange = {
                viewModel.onTitleChange(it)
            },
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, end = 16.dp, top = 16.dp),
            textStyle = AppTheme.typography.addGoalTitleStyle.copy(color = colors.textPrimary),
            keyboardOptions = KeyboardOptions(
                keyboardType = KeyboardType.Text,
                imeAction = ImeAction.Next
            ),
            maxLines = 4,
            cursorBrush = SolidColor(colors.primary.copy(0.5f)),
            decorationBox = { innerTextField ->
                if (title.isEmpty()) {
                    Text(
                        text = stringResource(string.add_goal_view_title_placeholder_text),
                        style = AppTheme.typography.addGoalTitleStyle,
                        color = if (isDarkMode) darkEmptyGoalText else lightEmptyGoalText,
                    )
                }
                innerTextField()
            }
        )

        Spacer(modifier = Modifier.height(40.dp))

        CompositionLocalProvider(LocalTextSelectionColors provides TextSelectionColors(colors.primary, colors.primary)) {
            Text(
                text = stringResource(string.add_goal_view_description_label_text),
                style = AppTheme.typography.bodyTextStyle3,
                color = colors.textSecondary,
                lineHeight = 27.sp,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(horizontal = 16.dp)
            )
            TextField(
                value = description,
                onValueChange = {
                    viewModel.onDescriptionChange(it)
                },
                placeholder = {
                    Text(
                        text = stringResource(string.add_goal_view_description_placeholder_text),
                        style = AppTheme.typography.subTextStyle1,
                        color = if (isDarkMode) darkEmptyGoalText else lightEmptyGoalText
                    )
                },
                modifier = Modifier
                    .fillMaxWidth(),
                interactionSource = descriptionInteractionSource,
                singleLine = false,
                textStyle = AppTheme.typography.subTextStyle1.copy(color = colors.textPrimary),
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Text,
                    imeAction = ImeAction.Done
                ),
                colors = TextFieldDefaults.textFieldColors(
                    backgroundColor = Color.Transparent,
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                    disabledIndicatorColor = Color.Transparent,
                    cursorColor = colors.primary.copy(0.5f)
                )
            )
        }

        if (goal != null) {
            if (goal.type != TYPE_END_GOAL) {
                Spacer(modifier = Modifier.height(40.dp))
                GoalDueDateView(viewModel)
            }

            if (goal.occurrences != 0) {
                GoalProgressView(goal)
            }
        } else {
            Spacer(modifier = Modifier.height(40.dp))
            GoalDueDateView(viewModel = viewModel)
        }

        Spacer(modifier = Modifier.height(49.dp))

        WallVisibilityView(viewModel)

        Spacer(modifier = Modifier.height(40.dp))

        if (goal != null) {
            DeleteGoalButton(goal, viewModel)
        }
    }
}

private val darkEmptyGoalText = Color(0x4DFFFFFF)
private val lightEmptyGoalText = Color(0x4D1C191F)
