package com.canopas.ui.goals.addgoals

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.ui.Event
import com.canopas.base.ui.Utils.Companion.dueDateFormat
import com.canopas.base.ui.Utils.Companion.format
import com.canopas.base.ui.Utils.Companion.parseOrNull
import com.canopas.data.model.AddOrUpdateGoal
import com.canopas.data.model.GOAL_WALL_VISIBILITY_ALWAYS
import com.canopas.data.model.GOAL_WALL_VISIBILITY_NEVER
import com.canopas.data.model.GOAL_WALL_VISIBILITY_WHEN_ACTIVE
import com.canopas.data.model.Goal
import com.canopas.data.model.TYPE_SUB_GOAL
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.customactivity.MINIMUM_CHARACTER_LENGTH
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.Calendar
import java.util.Date
import javax.inject.Inject

@HiltViewModel
class AddOrUpdateGoalViewModel @Inject constructor(
    private val navManager: NavManager,
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val resources: Resources,
    private val appAnalytics: AppAnalytics
) : ViewModel() {

    val state = MutableStateFlow<GoalState>(GoalState.LOADING)
    val goalTitle = MutableStateFlow("")
    val goalDescription = MutableStateFlow("")
    val dueDate = MutableStateFlow("")
    private val goalsDueDate = MutableStateFlow("")
    private var goal: Goal? = null
    val enableActionButton = MutableStateFlow(false)
    val showActionLoader = MutableStateFlow(false)
    val selectedVisibilityType = MutableStateFlow(GOAL_WALL_VISIBILITY_WHEN_ACTIVE)
    val openDeleteDialog = MutableStateFlow(Event(false))

    init {
        val calendar = Calendar.getInstance().apply {
            this.add(Calendar.DAY_OF_MONTH, 7)
        }
        dueDate.tryEmit(dueDateFormat.format(calendar.timeInMillis))
        goalsDueDate.tryEmit(format.format(calendar.timeInMillis))
    }

    fun onStart(goalId: Int?) {
        if (goalId == null) {
            state.tryEmit(GoalState.START)
            appAnalytics.logEvent("view_add_goal")
        } else {
            viewModelScope.launch {
                withContext(appDispatcher.IO) {
                    appAnalytics.logEvent("view_update_goal")
                    state.tryEmit(GoalState.LOADING)
                    service.getGoalById(goalId).fold(
                        onSuccess = { goal ->
                            this@AddOrUpdateGoalViewModel.goal = goal
                            selectedVisibilityType.tryEmit(goal.wallVisibility)
                            state.tryEmit(GoalState.SUCCESS(goal))
                            goalTitle.tryEmit(goal.title ?: "")
                            goalDescription.tryEmit(goal.description ?: "")
                            if (goal.dueDate.isNotEmpty()) {
                                val dueDate = format.parseOrNull(goal.dueDate)?.let { dueDateFormat.format(it) } ?: ""
                                this@AddOrUpdateGoalViewModel.dueDate.tryEmit(dueDate)
                                goalsDueDate.tryEmit(format.format(format.parse(goal.dueDate) ?: Date()))
                            }
                        },
                        onFailure = { e ->
                            Timber.e(e.toString())
                            state.tryEmit(GoalState.FAILURE(e.toUserError(resources)))
                        }
                    )
                }
            }
        }
    }

    fun resetState() {
        if (goal != null) {
            state.tryEmit(GoalState.SUCCESS(goal!!))
        } else {
            state.tryEmit(GoalState.START)
        }
    }

    fun popBack() {
        navManager.popBack()
    }

    fun onDoneButtonClick(subscriptionId: Int, goalId: Int?) = viewModelScope.launch {
        showActionLoader.tryEmit(true)
        if (goalId == null) {
            appAnalytics.logEvent("tap_add_goals_done_action_icon")
        } else {
            appAnalytics.logEvent("tap_update_goals_done_action_icon")
        }
        withContext(appDispatcher.IO) {
            val goalBody = AddOrUpdateGoal(
                subscriptionId,
                title = goalTitle.value.trim(),
                description = goalDescription.value.trim(),
                type = if (goalId == null) TYPE_SUB_GOAL else goal!!.type,
                dueDate = goalsDueDate.value,
                wallVisibility = selectedVisibilityType.value
            )
            val goalJob = if (goalId == null) async { service.createGoal(goalBody) } else async { service.updateGoal(goalId, goalBody) }
            goalJob.await().onSuccess {
                withContext(appDispatcher.MAIN) {
                    popBack()
                }
            }.onFailure {
                Timber.e(it.toString())
                state.tryEmit(GoalState.FAILURE(it.toUserError(resources)))
            }
            showActionLoader.tryEmit(false)
        }
    }

    fun onTitleChange(value: String) {
        goalTitle.tryEmit(value)
        if (goal == null) {
            enableActionButton.tryEmit(goalTitle.value.trim().length >= MINIMUM_CHARACTER_LENGTH)
        } else {
            updateActionButtonState()
        }
    }

    fun onDescriptionChange(value: String) {
        goalDescription.tryEmit(value)
        updateActionButtonState()
    }

    fun setGoalDueDate(dueDate: Long) {
        this.dueDate.tryEmit(dueDateFormat.format(dueDate))
        goalsDueDate.tryEmit(format.format(dueDate))
        updateActionButtonState()
    }

    fun toggleVisibilitySelection(selectedType: Int) {
        selectedVisibilityType.tryEmit(
            when (selectedType) {
                0 -> GOAL_WALL_VISIBILITY_ALWAYS
                1 -> GOAL_WALL_VISIBILITY_WHEN_ACTIVE
                else -> GOAL_WALL_VISIBILITY_NEVER
            }
        )
        updateActionButtonState()
    }

    private fun updateActionButtonState() {
        val goal = goal ?: return
        enableActionButton.tryEmit(
            goalTitle.value.trim().length >= MINIMUM_CHARACTER_LENGTH &&
                (
                    goalsDueDate.value != goal.dueDate ||
                        goalTitle.value != goal.title ||
                        goalDescription.value != goal.description ||
                        selectedVisibilityType.value != goal.wallVisibility
                    )
        )
    }

    fun openDeleteGoalDialog() {
        openDeleteDialog.value = Event(true)
    }

    fun closeDeleteGoalDialog() {
        openDeleteDialog.value = Event(false)
    }

    fun deleteGoal(goalId: Int) {
        viewModelScope.launch {
            closeDeleteGoalDialog()
            state.tryEmit(GoalState.LOADING)
            withContext(appDispatcher.IO) {
                service.deleteGoal(goalId).onSuccess {
                    withContext(appDispatcher.MAIN) {
                        popBack()
                    }
                }.onFailure {
                    Timber.e(it.toString())
                    state.tryEmit(GoalState.FAILURE(it.toUserError(resources)))
                }
            }
        }
    }
}

sealed class GoalState {
    object START : GoalState()
    object LOADING : GoalState()
    data class SUCCESS(val goal: Goal) : GoalState()
    data class FAILURE(val message: String) : GoalState()
}
