package com.canopas.ui.goals.mygoals

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Icon
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils.Companion.format
import com.canopas.base.ui.Utils.Companion.myGoalDueDateFormat
import com.canopas.base.ui.customview.SecondaryOutlineButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.parse
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.Goal
import com.canopas.data.model.GoalActivityTab
import com.canopas.data.model.Subscription
import com.canopas.data.model.TYPE_END_GOAL
import com.canopas.ui.R
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.PagerState

@OptIn(ExperimentalPagerApi::class)
@Composable
fun MyGoalsSuccessView(
    viewModel: MyGoalsViewModel,
    userSubscriptions: List<Subscription>,
    activeGoals: Map<String, List<Goal>>,
    inactiveGoals: Map<String, List<Goal>>,
    pagerState: PagerState,
    goalTabList: List<GoalActivityTab>
) {

    LaunchedEffect(pagerState.targetPage, block = {
        viewModel.onTabUpdate(goalTabList[pagerState.targetPage].tabId)
    })

    val fetchGoalState by viewModel.fetchGoalState.collectAsState()
    val loadingState = fetchGoalState is FetchGoalState.LOADING
    val currentSelectedTab by viewModel.currentSelectedTab.collectAsState()

    if (loadingState) {
        Box(
            modifier = Modifier.fillMaxSize(),
            contentAlignment = Alignment.Center
        ) {
            CircularProgressIndicator(color = colors.primary)
        }
    } else {
        if (activeGoals.isEmpty() && inactiveGoals.isEmpty()) {
            EmptyGoalsView { viewModel.navigateToAddGoals(currentSelectedTab) }
        } else {

            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.TopCenter
            ) {
                LazyColumn(contentPadding = PaddingValues(bottom = 20.dp)) {
                    activeGoals.forEach { (dueDate, listItem) ->
                        item {
                            Box(
                                Modifier
                                    .fillMaxWidth()
                                    .background(colors.background)
                                    .padding(top = 20.dp)
                            ) {
                                Column(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(start = 16.dp, end = 16.dp)
                                ) {
                                    Text(
                                        text = dueDate,
                                        style = AppTheme.typography.yourCommunityText,
                                        lineHeight = 24.sp,
                                        letterSpacing = -(0.8.sp),
                                        color = colors.textSecondary
                                    )
                                }
                            }
                        }
                        itemsIndexed(listItem) { _, item ->
                            val subscription = userSubscriptions.find {
                                it.id == item.subscriptionId
                            }
                            GoalsItemsView(
                                viewModel = viewModel,
                                goalsItem = item,
                                subscription
                            )
                        }
                    }
                    inactiveGoals.forEach { (dueDate, listItem) ->
                        item {
                            Box(
                                Modifier
                                    .fillMaxWidth()
                                    .background(colors.background)
                                    .padding(top = 20.dp)
                            ) {
                                Column(
                                    modifier = Modifier
                                        .fillMaxWidth()
                                        .padding(start = 16.dp, end = 16.dp)
                                ) {
                                    Text(
                                        text = dueDate,
                                        style = AppTheme.typography.yourCommunityText,
                                        lineHeight = 24.sp,
                                        letterSpacing = -(0.8.sp),
                                        color = colors.textSecondary.copy(0.45f)
                                    )
                                }
                            }
                        }
                        itemsIndexed(listItem) { _, item ->
                            val subscription = userSubscriptions.find {
                                it.id == item.subscriptionId
                            }
                            GoalsItemsView(
                                viewModel = viewModel,
                                goalsItem = item,
                                subscription,
                                showCompletedIcon = true
                            )
                        }
                    }
                }
                if (currentSelectedTab != ALL_TAB_ID && pagerState.currentPage != ALL_TAB_ID) {
                    Box(
                        modifier = Modifier
                            .widthIn(max = 600.dp)
                            .fillMaxSize(),
                        contentAlignment = Alignment.BottomEnd
                    ) {
                        Row(
                            Modifier
                                .padding(16.dp)
                                .size(56.dp)
                                .clip(shape = CircleShape)
                                .motionClickEvent { viewModel.navigateToAddGoals(currentSelectedTab) }
                                .background(colors.primary),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Center
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_add_activity_icon),
                                contentDescription = "",
                                tint = Color.White,
                                modifier = Modifier.size(24.dp)
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
fun EmptyGoalsView(
    onClick: () -> Unit
) {
    Column(
        Modifier
            .fillMaxSize()
            .padding(horizontal = 52.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = stringResource(R.string.empty_goal_title_text1),
            style = AppTheme.typography.subTitleTextStyle1,
            color = colors.textSecondary,
            lineHeight = 24.sp,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 8.dp)
        )

        SecondaryOutlineButton(
            onClick = {
                onClick()
            },
            modifier = Modifier
                .wrapContentSize()
                .padding(top = 16.dp),
            text = stringResource(R.string.empty_goal_add_btn_text),
            textModifier = Modifier.padding(vertical = 4.dp, horizontal = 8.dp),
            shape = RoundedCornerShape(24.dp),
            colors = ButtonDefaults.outlinedButtonColors(contentColor = Color.White, backgroundColor = colors.primary),
            color = Color.White,
            icon = painterResource(id = R.drawable.ic_add_activity_icon),
            iconModifier = Modifier.padding(start = 8.dp).size(16.dp)
        )
    }
}

@Composable
fun GoalsItemsView(
    viewModel: MyGoalsViewModel,
    goalsItem: Goal,
    subscription: Subscription?,
    showCompletedIcon: Boolean = false
) {
    val subscriptionColor = subscription?.activity?.color2 ?: subscription?.activity?.color
    val activityColor = subscriptionColor?.let { Color.parse(it) } ?: cardBg.random()

    Box(
        Modifier
            .fillMaxWidth()
            .padding(top = 16.dp, start = 16.dp, end = 16.dp),
    ) {

        Column(
            modifier = Modifier
                .fillMaxWidth()
                .background(activityColor.copy(0.3f))
                .padding(horizontal = 16.dp, vertical = 16.dp)
                .motionClickEvent {
                    viewModel.navigateToUpdateGoals(
                        goalsItem.subscriptionId,
                        goalsItem.id
                    )
                }
        ) {
            Text(
                text = subscription?.activity?.name?.trim() +
                    if (goalsItem.type == TYPE_END_GOAL) stringResource(R.string.explore_goal_card_dash_text) + stringResource(
                        R.string.explore_goal_card_why_title_text
                    )
                    else "",
                style = AppTheme.typography.subTextStyle2,
                color = colors.textSecondary,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 8.dp)
            )

            Text(
                text = goalsItem.title?.trim() ?: "",
                style = AppTheme.typography.goalsBodyTextStyle,
                lineHeight = 22.sp,
                color = colors.textPrimary,
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
            )

            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 8.dp),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start
            ) {

                if (goalsItem.occurrences != 0) {
                    LinearProgressIndicator(
                        progress = goalsItem.getProgress(),
                        modifier = Modifier
                            .fillMaxWidth()
                            .weight(1f)
                            .padding(end = 12.dp)
                            .height(6.dp)
                            .clip(
                                RoundedCornerShape(3.dp)
                            ),
                        color = progressBarColor,
                        backgroundColor = if (isDarkMode) darkProgressBarBg else lightProgressBarBg
                    )
                    Text(
                        text = goalsItem.getProgressText(),
                        style = AppTheme.typography.tabTextStyle,
                        letterSpacing = -(0.64.sp),
                        color = colors.textSecondary
                    )
                }
            }
            if (goalsItem.dueDate.isNotEmpty()) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 8.dp),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Row(verticalAlignment = Alignment.CenterVertically) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_goal_due_icon),
                            contentDescription = "",
                            modifier = Modifier.size(16.dp),
                            tint = if (isDarkMode) colors.textSecondary else lightSubText
                        )
                        Text(
                            text = myGoalDueDateFormat.format(format.parse(goalsItem.dueDate)!!),
                            style = AppTheme.typography.subTextStyle2,
                            color = if (isDarkMode) colors.textSecondary else lightSubText,
                            modifier = Modifier.padding(start = 4.dp)
                        )
                    }
                    if (showCompletedIcon) {
                        Icon(
                            painter = painterResource(id = R.drawable.ic_check_circle),
                            contentDescription = "",
                            modifier = Modifier
                                .padding(6.dp)
                                .size(18.dp),
                            tint = completedGoalsColor
                        )
                    }
                }
            }
        }
    }
}

private val lightSubText = Color(0x66000000)
private val cardBg = listOf(Color(0xffFFF3EB), Color(0xffE0F6FF), Color(0xffFFFDE0))
private val progressBarColor = Color(0xffFFAD65)
private val lightProgressBarBg = Color(0xA000000)
private val darkProgressBarBg = Color(0x14000000)
private val completedGoalsColor = Color(0xFF47A96E)
