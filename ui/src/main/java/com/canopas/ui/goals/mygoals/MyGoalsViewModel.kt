package com.canopas.ui.goals.mygoals

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.ui.Utils
import com.canopas.base.ui.Utils.Companion.allTimeFormat
import com.canopas.base.ui.Utils.Companion.format
import com.canopas.data.model.DueDateGoals
import com.canopas.data.model.Goal
import com.canopas.data.model.GoalActivityTab
import com.canopas.data.model.Subscription
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.home.explore.timeStringToCurrentMillis
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.text.ParseException
import java.util.Calendar
import javax.inject.Inject

@HiltViewModel
class MyGoalsViewModel @Inject constructor(
    private val appAnalytics: AppAnalytics,
    private val service: NoLonelyService,
    private val navManager: NavManager,
    private val appDispatcher: AppDispatcher,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<GoalState>(GoalState.LOADING)
    val subscription = MutableStateFlow<List<Subscription>>(emptyList())
    val currentSelectedTab = MutableStateFlow(0)
    val goalTabList = MutableStateFlow<List<GoalActivityTab>>(emptyList())
    val fetchGoalState = MutableStateFlow<FetchGoalState>(FetchGoalState.LOADING)

    fun onStart(subscriptionId: Int) {
        appAnalytics.logEvent("view_my_goals_screen")
        currentSelectedTab.value = state.value
            .run {
                when {
                    this is GoalState.LOADING && subscriptionId == 0 -> ALL_TAB_ID.also {
                        loadData(ALL_TAB_ID)
                    }
                    this is GoalState.LOADING -> subscriptionId.also {
                        loadData(subscriptionId)
                    }
                    this is GoalState.SUCCESS -> (this).subscriptionId.also {
                        loadData(it)
                    }
                    else -> currentSelectedTab.value
                }
            }
    }

    fun loadData(subId: Int) = viewModelScope.launch {
        if (state.value !is GoalState.SUCCESS) {
            state.value = GoalState.LOADING
        }
        withContext(appDispatcher.IO) {
            service.getUserSubscriptions().onSuccess { sub ->
                val sortedlist = sub.sortedWith(
                    compareBy(
                        {
                            timeStringToCurrentMillis(it.activity_time)?.div(1000)
                                ?: Long.MAX_VALUE
                        },
                        { it.activity_duration ?: Int.MAX_VALUE }
                    )
                )
                val allTab = GoalActivityTab(ALL_TAB_STRING, ALL_TAB_ID)
                val newTabList = mutableListOf<GoalActivityTab>()
                newTabList.add(allTab)
                if (sortedlist.isNotEmpty()) {
                    for (i in sortedlist.indices) {
                        val subscriptionItem = sortedlist[i]
                        val goalTabList = GoalActivityTab(
                            subscriptionItem.activity.name,
                            subscriptionItem.id
                        )
                        newTabList.add(goalTabList)
                    }
                }

                goalTabList.value = newTabList
                subscription.value = sortedlist
                fetchGoals(subId)
            }.onFailure { e ->
                Timber.e(e)
                state.value =
                    GoalState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
            }
        }
    }

    fun fetchGoals(subId: Int) = viewModelScope.launch {
        withContext(appDispatcher.IO) {
            service.getAllGoals().onSuccess { goal ->

                val goalListById = mutableListOf<DueDateGoals>()
                goalTabList.value.forEach {
                    val groupedGoals = if (it.tabId == 0) goal else goal.filter { goalList ->
                        it.tabId == goalList.subscriptionId
                    }

                    val filterGoals = getGoalsList(groupedGoals)
                    goalListById.add(DueDateGoals(filterGoals.first, filterGoals.second))
                }

                state.value =
                    GoalState.SUCCESS(
                        subId,
                        subscription.value,
                        goalListById
                    )
                fetchGoalState.tryEmit(FetchGoalState.SUCCESS)
            }
                .onFailure { e ->
                    Timber.e(e)
                    state.value =
                        GoalState.FAILURE(e.toUserError(resources), e is APIError.NetworkError)
                }
        }
    }

    private fun getGoalsList(goalList: List<Goal>): Pair<Map<String, List<Goal>>, Map<String, List<Goal>>> {
        val sortedList = goalList.sortedBy { it.dueDate }
        val emptyDueDateList = sortedList.filter { it.dueDate.isEmpty() }
        val dueDateList = sortedList.filter { it.dueDate.isNotEmpty() }
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, -1)

        val activeGoalsList = dueDateList.filter { item ->
            val format = Utils.format.parse(item.dueDate)
            val milliseconds = format?.time
            milliseconds!! >= calendar.timeInMillis
        } + emptyDueDateList

        val inactiveGoalsList = dueDateList.filter { item ->
            val format = Utils.format.parse(item.dueDate)
            val milliseconds = format?.time
            milliseconds!! < calendar.timeInMillis
        }.reversed()

        val groupOfActiveGoal = activeGoalsList.groupBy {
            try {
                allTimeFormat.format(format.parse(it.dueDate) ?: calendar.timeInMillis)
            } catch (e: ParseException) {
                allTimeFormat.format(calendar.timeInMillis)
            }
        }
        val groupOfInactiveGoal = inactiveGoalsList.groupBy {
            try {
                allTimeFormat.format(format.parse(it.dueDate) ?: calendar.timeInMillis)
            } catch (e: ParseException) {
                allTimeFormat.format(calendar.timeInMillis)
            }
        }
        return Pair(groupOfActiveGoal, groupOfInactiveGoal)
    }

    fun handleTabItemSelection(selectedTab: Int) = viewModelScope.launch {
        if (selectedTab != currentSelectedTab.value) {
            currentSelectedTab.value = selectedTab
            fetchGoalState.tryEmit(FetchGoalState.LOADING)
            when (currentSelectedTab.value) {
                ALL_TAB_ID -> {
                    appAnalytics.logEvent("tap_my_goals_all_tab")
                }
                else -> {
                    appAnalytics.logEvent("tap_my_goals_activity_tab")
                }
            }
            delay(300)
            fetchGoals(currentSelectedTab.value)
        }
    }
    fun onStop() {
        state.value = GoalState.LOADING
    }

    fun onTabUpdate(selectedTab: Int) {
        currentSelectedTab.value = selectedTab
    }

    fun navigateToUpdateGoals(subscriptionId: Int, goalId: Int) {
        appAnalytics.logEvent("navigate_to_update_goals")
        navManager.navigateToEditGoal(subscriptionId, goalId)
    }

    fun navigateToAddGoals(subscriptionId: Int) {
        appAnalytics.logEvent("navigate_to_add_goals")
        if (subscriptionId == ALL_TAB_ID) {
            navManager.navigateToSelectActivity()
        } else {
            navManager.navigateToAddGoal(subscriptionId)
        }
    }

    fun managePopBack() {
        navManager.popBack()
    }
}

sealed class GoalState {
    object LOADING : GoalState()
    data class SUCCESS(
        val subscriptionId: Int,
        val subscription: List<Subscription>,
        val goals: List<DueDateGoals>
    ) : GoalState()

    data class FAILURE(val message: String, val isNetworkError: Boolean) : GoalState()
}

sealed class FetchGoalState {
    object LOADING : FetchGoalState()
    object SUCCESS : FetchGoalState()
}
