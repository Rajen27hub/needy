package com.canopas.ui.goals.activityselection

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.Subscription
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.home.explore.timeStringToCurrentMillis
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class AddGoalSelectionViewModel @Inject constructor(
    private val service: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<SubscriptionState>(SubscriptionState.LOADING)
    val shouldPopBack = MutableStateFlow(false)

    fun getUserSubscriptions() = viewModelScope.launch {
        state.tryEmit(SubscriptionState.LOADING)
        withContext(appDispatcher.IO) {
            appAnalytics.logEvent("view_add_goal_select_activity")
            service.getUserSubscriptions().onSuccess { subscriptions ->
                val sortedSubscriptions = subscriptions.sortedWith(
                    compareBy(
                        { timeStringToCurrentMillis(it.activity_time)?.div(1000) ?: Long.MAX_VALUE },
                        { it.activity_duration ?: Int.MAX_VALUE }
                    )
                )
                state.tryEmit(SubscriptionState.SUCCESS(sortedSubscriptions))
            }.onFailure { e ->
                Timber.e(e)
                state.tryEmit(SubscriptionState.FAILURE(e.toUserError(resources)))
            }
        }
    }

    fun onActivitySelected(subscriptionId: Int) {
        appAnalytics.logEvent("tap_active_subscriptions_item", null)
        shouldPopBack.tryEmit(true)
        navManager.navigateToAddGoal(subscriptionId = subscriptionId)
    }

    fun popBack() {
        navManager.popBack()
    }
}

sealed class SubscriptionState {
    object LOADING : SubscriptionState()
    data class SUCCESS(val subscriptions: List<Subscription>) : SubscriptionState()
    data class FAILURE(val message: String) : SubscriptionState()
}
