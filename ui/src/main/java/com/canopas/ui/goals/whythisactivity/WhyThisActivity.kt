package com.canopas.ui.goals.whythisactivity

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.SolidColor
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.utils.Utils.Companion.decodeFromBase64
import com.canopas.base.data.utils.Utils.Companion.encodeToBase64
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.CustomActivityTitleLineView
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomTextField
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.ui.R
import com.canopas.ui.customactivity.MINIMUM_CHARACTER_LENGTH

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun WhyThisActivityView(
    activityId: Int = 0,
    title: String = "",
    activityName: String = "",
    defaultTime: String = "",
    defaultDuration: String = "",
    storyId: Int = 0,
) {
    val viewModel = hiltViewModel<WhyThisActivityViewModel>()
    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = decodeFromBase64(activityName).ifEmpty { decodeFromBase64(title) },
                        navigationIconOnClick = { viewModel.popBack() },
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        val scrollState = rememberScrollState()
        val goalTitle by viewModel.title.collectAsState()
        val showShortTitleError by viewModel.showShortTitleError.collectAsState()
        val interactionSource = remember { MutableInteractionSource() }
        val isFocused by interactionSource.collectIsFocusedAsState()
        val focusManager = LocalFocusManager.current
        val keyboardController = LocalSoftwareKeyboardController.current
        val focusRequester = remember {
            FocusRequester()
        }

        LaunchedEffect(key1 = Unit, block = {
            focusRequester.requestFocus()
        })

        DisposableEffect(key1 = Unit, effect = {
            onDispose {
                keyboardController?.hide()
            }
        })

        Column(
            modifier = Modifier
                .padding(it)
                .fillMaxSize()
                .background(colors.background)
                .clickable(
                    interactionSource = MutableInteractionSource(),
                    indication = null,
                    onClick = {
                        keyboardController?.hide()
                        focusManager.clearFocus()
                    }
                ),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .verticalScroll(scrollState)
            ) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 40.dp, start = 16.dp, end = 16.dp)
                        .background(colors.primary.copy(0.08f), RoundedCornerShape(16.dp))
                        .clip(RoundedCornerShape(16.dp))
                ) {
                    Text(
                        text = stringResource(R.string.why_this_activity_header_text),
                        style = AppTheme.typography.subTitleTextStyle1,
                        lineHeight = 22.sp,
                        textAlign = TextAlign.Center,
                        modifier = Modifier.padding(16.dp)
                    )
                }

                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(start = 16.dp, end = 16.dp, top = 60.dp),
                ) {
                    Text(
                        text = stringResource(R.string.why_this_activity_textfield_title),
                        color = colors.textSecondary,
                        style = AppTheme.typography.subTitleTextStyle1,
                        lineHeight = 19.sp,
                        textAlign = TextAlign.Start,
                        modifier = Modifier
                            .fillMaxWidth()
                    )

                    CustomTextField(
                        text = goalTitle,
                        onTextChange = {
                            viewModel.onTitleChange(it)
                        },
                        interactionSource = interactionSource,
                        modifier = Modifier
                            .motionClickEvent { }
                            .fillMaxWidth()
                            .padding(top = 8.dp)
                            .focusRequester(focusRequester),
                        textStyle = AppTheme.typography.h2TextStyle.copy(
                            color = colors.textPrimary,
                            textAlign = TextAlign.Start,
                            lineHeight = 29.sp,
                            letterSpacing = -(0.96.sp)
                        ),
                        keyboardOptions = KeyboardOptions(
                            keyboardType = KeyboardType.Text,
                            imeAction = ImeAction.Done
                        ),
                        singleLine = true,
                        cursorBrush = SolidColor(colors.textPrimary)
                    )

                    CustomActivityTitleLineView(
                        Modifier.fillMaxWidth(),
                        if (isFocused) colors.primary else if (ComposeTheme.isDarkMode) dividerColorDark else dividerColorLight
                    )

                    AnimatedVisibility(showShortTitleError) {
                        Text(
                            text = stringResource(id = R.string.global_short_input_error_text),
                            color = MaterialTheme.colors.error,
                            style = AppTheme.typography.captionTextStyle,
                            modifier = Modifier
                                .padding(top = 4.dp)
                                .fillMaxWidth(),
                            textAlign = TextAlign.Start
                        )
                    }

                    Spacer(modifier = Modifier.height(100.dp))
                }
            }

            Box(
                Modifier
                    .offset(y = -(20.dp))
                    .height(20.dp)
                    .fillMaxWidth()
            ) {
                Box(
                    Modifier
                        .fillMaxSize()
                        .background(
                            Brush.verticalGradient(
                                listOf(
                                    Color.Transparent,
                                    colors.background
                                )
                            )
                        )
                )
            }

            PrimaryButton(
                onClick = { viewModel.onContinueButtonClick(activityId, title, activityName, defaultTime, defaultDuration, storyId, encodeToBase64(goalTitle)) },
                shape = RoundedCornerShape(50),
                enabled = goalTitle.trim().length >= MINIMUM_CHARACTER_LENGTH,
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .motionClickEvent { }
                    .align(Alignment.CenterHorizontally)
                    .fillMaxWidth(),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = colors.primary,
                    disabledBackgroundColor = colors.primary.copy(
                        0.6f
                    ),
                    contentColor = White,
                ),
                text = stringResource(R.string.why_this_activity_continue_btn_text)
            )

            Spacer(modifier = Modifier.height(20.dp))
        }
    }
}

private val dividerColorDark = Color(0xFF313131)
private val dividerColorLight = Color(0x991C191F)
