package com.canopas.ui.goals.mygoals

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Divider
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.data.model.DueDateGoals
import com.canopas.data.model.GoalActivityTab
import com.canopas.data.model.Subscription
import com.canopas.ui.R
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState
import kotlinx.coroutines.launch

const val ALL_TAB_ID = 0
const val ALL_TAB_STRING = "All"

@Composable
fun MyGoalsView(subscriptionId: String = "") {

    val viewModel = hiltViewModel<MyGoalsViewModel>()
    val state by viewModel.state.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(subscriptionId.toInt())
    })

    DisposableEffect(key1 = Unit) {
        onDispose {
            viewModel.onStop()
        }
    }

    state.let { subState ->
        when (subState) {
            is GoalState.LOADING -> {
                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .background(colors.background),
                    contentAlignment = Alignment.Center
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }
            is GoalState.FAILURE -> {
                if (subState.isNetworkError) {
                    NetworkErrorView {
                        viewModel.onStart(subscriptionId.toInt())
                    }
                } else {
                    ServerErrorView {
                        viewModel.onStart(subscriptionId.toInt())
                    }
                }
            }
            is GoalState.SUCCESS -> {
                val userSubscriptions = subState.subscription
                val activeGoals = subState.goals
                BuildGoalsView(viewModel, userSubscriptions, activeGoals)
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class, ExperimentalPagerApi::class)
@Composable
fun BuildGoalsView(
    viewModel: MyGoalsViewModel,
    userSubscriptions: List<Subscription>,
    activeGoals: List<DueDateGoals>,
) {
    val goalTabList by viewModel.goalTabList.collectAsState()
    val currentSelectedTab by viewModel.currentSelectedTab.collectAsState()
    val lazyRowState = rememberLazyListState()
    val pagerState =
        rememberPagerState(initialPage = goalTabList.indexOfFirst { it.tabId == currentSelectedTab })

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = stringResource(R.string.my_goals_top_bar_text),
                        navigationIconOnClick = { viewModel.managePopBack() }
                    )
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .background(colors.background),
            contentAlignment = Alignment.Center
        ) {
            Column(
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .fillMaxSize()
                    .padding(it),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                ActivitySelectionView(viewModel, goalTabList, lazyRowState, pagerState)

                Spacer(modifier = Modifier.height(12.dp))

                HorizontalPager(
                    state = pagerState,
                    count = goalTabList.size,
                    verticalAlignment = Alignment.Top
                ) { page ->

                    val goalList = remember { activeGoals[page] }

                    MyGoalsSuccessView(
                        viewModel,
                        userSubscriptions,
                        goalList.activeGoals,
                        goalList.inactiveGoals,
                        pagerState, goalTabList
                    )
                }
            }
        }
    }
}

@OptIn(ExperimentalPagerApi::class)
@Composable
fun ActivitySelectionView(
    viewModel: MyGoalsViewModel,
    goalTabList: List<GoalActivityTab>,
    lazyRowState: LazyListState,
    pagerState: PagerState,
) {
    val currentSelectedTab by viewModel.currentSelectedTab.collectAsState()
    val coroutineScope = rememberCoroutineScope()

    LaunchedEffect(key1 = currentSelectedTab, block = {
        val index = goalTabList.indexOfFirst { it.tabId == currentSelectedTab }
        if (index != -1) {
            lazyRowState.animateScrollToItem(index)
        }
    })

    LazyRow(
        state = lazyRowState,
        contentPadding = PaddingValues(start = 16.dp, end = 16.dp),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically,
        modifier = Modifier
            .fillMaxWidth()
    ) {

        itemsIndexed(goalTabList) { index, item ->
            val isLastIndex = remember { goalTabList.lastIndex == index }
            val selectedTab = currentSelectedTab == item.tabId
            val selectedTabIndex = goalTabList.indexOfFirst { it.tabId == currentSelectedTab }
            val clip = remember {
                if (isLastIndex) {
                    RoundedCornerShape(topEnd = 20.dp, bottomEnd = 20.dp)
                } else if (index == 0) {
                    RoundedCornerShape(topStart = 20.dp, bottomStart = 20.dp)
                } else {
                    RoundedCornerShape(0.dp)
                }
            }

            GoalTabView(
                Modifier
                    .fillMaxWidth()
                    .clip(clip)
                    .background(if (isDarkMode) darkActivitySelectionTabBg else lightActivitySelectionTabBg),
                selectedTab,
                item.tabName,
                (selectedTabIndex - 1 != index && !selectedTab && !isLastIndex)
            ) {
                coroutineScope.launch {
                    pagerState.scrollToPage(index)
                }
                viewModel.handleTabItemSelection(item.tabId)
            }
        }
    }
}

@Composable
fun GoalTabView(
    modifier: Modifier,
    selectedTab: Boolean,
    tabText: String,
    showDivider: Boolean,
    onClick: () -> Unit
) {
    val configuration = LocalConfiguration.current
    val height = (configuration.screenHeightDp * 0.02).dp

    Row(
        modifier = modifier,
        verticalAlignment = Alignment.CenterVertically
    ) {

        Text(
            text = tabText.trim(),
            style = if (selectedTab) AppTheme.typography.buttonStyle else AppTheme.typography.subTextStyle2,
            textAlign = TextAlign.Center,
            color = if (selectedTab) Color.White else colors.textPrimary,
            modifier = Modifier
                .fillMaxWidth()
                .motionClickEvent(onClick)
                .clip(RoundedCornerShape(40.dp))
                .background(if (selectedTab) colors.primary else Color.Transparent)
                .padding(horizontal = 24.dp, vertical = 8.dp)
        )

        if (showDivider) {
            Divider(
                Modifier
                    .height(height)
                    .width(0.5.dp),
                color = if (isDarkMode) darkBorder else lightBorder
            )
        }
    }
}

private val lightActivitySelectionTabBg = Color(0xffFAF7F6)
private val darkActivitySelectionTabBg = Color(0xff222222)
private val lightBorder = Color(0x45000000)
private val darkBorder = Color(0xff313131)
