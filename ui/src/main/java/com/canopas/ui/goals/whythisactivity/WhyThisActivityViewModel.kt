package com.canopas.ui.goals.whythisactivity

import androidx.lifecycle.ViewModel
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.ui.customactivity.MINIMUM_CHARACTER_LENGTH
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import javax.inject.Inject

@HiltViewModel
class WhyThisActivityViewModel @Inject constructor(
    private val appAnalytics: AppAnalytics,
    private val navManager: NavManager,
) : ViewModel() {

    val title = MutableStateFlow("")
    val showShortTitleError = MutableStateFlow(false)

    init {
        appAnalytics.logEvent("view_why_this_activity")
    }

    fun onTitleChange(value: String) {
        title.tryEmit(value)
        showShortTitleError.tryEmit(title.value.trim().length < MINIMUM_CHARACTER_LENGTH)
    }

    fun onContinueButtonClick(
        activityId: Int,
        title: String,
        activityName: String,
        defaultTime: String,
        defaultDuration: String,
        storyId: Int,
        goalsTitle: String
    ) {
        appAnalytics.logEvent("tap_why_this_activity_continue_btn")
        if (title.isEmpty()) {
            navManager.navigateToConfigureActivity(activityId, activityName, defaultTime, defaultDuration.toInt(), storyId, goalsTitle)
        } else {
            navManager.navigateToConfigureCustomActivity(title, storyId, goalsTitle)
        }
    }

    fun popBack() {
        navManager.popBack()
    }
}
