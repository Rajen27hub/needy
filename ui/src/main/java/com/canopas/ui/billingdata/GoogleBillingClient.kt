package com.canopas.ui.billingdata

import android.app.Activity
import android.content.Context
import android.os.Bundle
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import com.android.billingclient.api.*
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.data.model.PlanOption
import com.canopas.data.model.PlanType
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.withTimeout
import timber.log.Timber
import javax.inject.Inject
import kotlin.RuntimeException
import kotlin.time.Duration.Companion.milliseconds
import kotlin.time.Duration.Companion.seconds

class GoogleBillingClient @Inject constructor(
    @ApplicationContext private val context: Context,
    private val appAnalytics: AppAnalytics,
    private val firebaseCrashlytics: FirebaseCrashlytics
) {

    private val clientState: MutableStateFlow<ClientState> = MutableStateFlow(ClientState.Idle)

    private val _purchaseState: MutableStateFlow<PurchaseState> =
        MutableStateFlow(PurchaseState.Idle)

    private val purchasesUpdatedListener =
        PurchasesUpdatedListener { billingResult, purchases ->
            when (billingResult.responseCode) {
                BillingClient.BillingResponseCode.OK -> {
                    if (purchases.isNullOrEmpty()) {
                        Timber.e("GoogleBillingClient - Empty Purchases with OK response!")
                        _purchaseState.value =
                            PurchaseState.Error(BillingError(error = "Empty Purchases with OK response!"))
                    } else {
                        Timber.d("GoogleBillingClient - OK response")
                        val valid =
                            purchases.filter { it.purchaseState == Purchase.PurchaseState.PURCHASED }
                        if (valid.isNotEmpty()) {
                            if (valid.size > 1) {
                                Timber.e("GoogleBillingClient - Multiple purchases found, should not happen. Ignoring other purchases...")
                            }
                            _purchaseState.value = PurchaseState.Success(valid.first())
                        } else {
                            Timber.e("GoogleBillingClient - Pending Purchases with OK response!")
                            PurchaseState.Error(BillingError(error = "Pending Purchases with OK response!"))
                        }
                    }
                }
                BillingClient.BillingResponseCode.USER_CANCELED -> {
                    Timber.d("GoogleBillingClient - User cancelled purchase")
                    _purchaseState.value = PurchaseState.UserCancelled
                }
                else -> {
                    Timber.e("GoogleBillingClient - Listener failed with error -  $billingResult")
                    firebaseCrashlytics.recordException(RuntimeException("GoogleBillingClient - Listener failed with error: $billingResult"))

                    _purchaseState.value = PurchaseState.Error(BillingError(billingResult))
                }
            }
        }

    private val clientStateListener = object : BillingClientStateListener {

        override fun onBillingSetupFinished(billingResult: BillingResult) {
            when (billingResult.responseCode) {
                BillingClient.BillingResponseCode.OK -> {
                    Timber.d("GoogleBillingClient - Setup finished")
                    clientState.value = ClientState.Connected
                }
                else -> {
                    Timber.e("GoogleBillingClient - Setup finished with error: $billingResult")
                    clientState.value = ClientState.Error(billingResult.debugMessage)
                }
            }
        }

        override fun onBillingServiceDisconnected() {
            Timber.e("GoogleBillingClient - Billing connection disconnected")
        }
    }

    private var billingClient: BillingClient =
        BillingClient.newBuilder(context)
            .setListener(purchasesUpdatedListener)
            .enablePendingPurchases().build()

    fun connect() {
        if (clientState.value == ClientState.Idle || clientState.value is ClientState.Error) {
            clientState.value = ClientState.Initializing
            billingClient.startConnection(clientStateListener)
        }
    }

    fun disconnect() {
        billingClient.endConnection()
        clientState.value = ClientState.Idle
    }

    private suspend fun waitForClientConnected(): Boolean {
        try {
            withTimeout(10.seconds) {
                // wait for init
                while (clientState.value == ClientState.Initializing) {
                    delay(100.milliseconds)
                }
            }
        } catch (t: TimeoutCancellationException) {
            // Timed out
        }
        return clientState.value == ClientState.Connected
    }

    suspend fun purchase(
        activity: Activity,
        productDetail: ProductDetails
    ): Flow<PurchaseState> {
        if (!waitForClientConnected()) {
            Timber.e("GoogleBillingClient - BillingClient is not connected during purchase, state: ${clientState.value}")
            _purchaseState.value = PurchaseState.ClientNotReady
        } else {
            val productDetailsParams =
                BillingFlowParams.ProductDetailsParams.newBuilder().apply {
                    setProductDetails(productDetail)
                    productDetail.subscriptionOfferDetails?.get(0)?.offerToken?.let {
                        setOfferToken(
                            it
                        )
                    }
                }.build()

            val billingFlowParams = BillingFlowParams.newBuilder()
                .setProductDetailsParamsList(listOf(productDetailsParams))
                .build()

            val result = billingClient.launchBillingFlow(activity, billingFlowParams)
            if (result.responseCode != BillingClient.BillingResponseCode.OK) {
                firebaseCrashlytics.recordException(RuntimeException("GoogleBillingClient - Launching purchase flow failed with error: $result"))
                Timber.e("GoogleBillingClient - Launching purchase flow failed with error: $result")
                _purchaseState.value = PurchaseState.Error(BillingError(result))
            } else {
                Timber.d("GoogleBillingClient - Launching purchase flow success")
                _purchaseState.value = PurchaseState.Processing
            }
        }

        return _purchaseState
    }

    suspend fun acknowledgePurchase(purchase: Purchase): Boolean {
        if (!waitForClientConnected()) {
            Timber.e("GoogleBillingClient - BillingClient is not connected during ack, state: ${clientState.value}")
            appAnalytics.logEvent("error_upgrade_premium_store_ack", null)
            return false
        }

        if (purchase.isAcknowledged) return true

        val acknowledgePurchasesParams = AcknowledgePurchaseParams.newBuilder()
            .setPurchaseToken(purchase.purchaseToken)

        val result = billingClient.acknowledgePurchase(acknowledgePurchasesParams.build())
        return if (result.responseCode == BillingClient.BillingResponseCode.OK) {
            Timber.d("GoogleBillingClient - Ack success")
            true
        } else {
            Timber.e("GoogleBillingClient - Ack failed with error: $result")
            firebaseCrashlytics.recordException(RuntimeException("GoogleBillingClient - ACK failed with error: $result"))
            val bundle = Bundle()
            bundle.putString("error_code", result.responseCode.toString())
            appAnalytics.logEvent("error_upgrade_premium_store_ack", bundle)
            false
        }
    }

    suspend fun queryProductDetails(skus: List<String>): Result<List<PlanOption>> {
        if (!waitForClientConnected()) {
            Timber.e("GoogleBillingClient - BillingClient is not connected during ack, state: ${clientState.value}")
            appAnalytics.logEvent("error_upgrade_premium_plan_load", null)
            return Result.failure(BillingError("Client not connected"))
        }

        val productList = skus.map {
            QueryProductDetailsParams.Product.newBuilder()
                .setProductId(it)
                .setProductType(BillingClient.ProductType.SUBS)
                .build()
        }

        val params = QueryProductDetailsParams.newBuilder().apply {
            setProductList(productList)
        }

        val response = billingClient.queryProductDetails(params.build())
        return if (response.billingResult.responseCode == BillingClient.BillingResponseCode.OK &&
            response.productDetailsList?.isNotEmpty() == true
        ) {
            val plans = response.productDetailsList?.mapNotNull { it.toPlanOption() }
            Result.success(plans ?: emptyList())
        } else {
            Timber.e("GoogleBillingClient - queryProductDetailsAsync error: ${response.billingResult}")
            val bundle = Bundle()
            bundle.putString("error_code", response.billingResult.responseCode.toString())
            appAnalytics.logEvent("error_upgrade_premium_plan_load", bundle)
            Result.failure(
                BillingError(
                    response.billingResult.debugMessage,
                    response.billingResult.responseCode
                )
            )
        }
    }

    private fun ProductDetails.toPlanOption(): PlanOption? {
        val planType = PlanType.values().find { it.id == this.productId } ?: return null
        val offerDetails = subscriptionOfferDetails ?: return null
        val isFreeTrialAvailable = offerDetails.size == 2
        val pricing =
            (if (isFreeTrialAvailable) offerDetails[1] else offerDetails[0]).pricingPhases.pricingPhaseList[0]

        return PlanOption(
            type = planType,
            productDetails = this,
            isFreeTrialAvailable = isFreeTrialAvailable,
            price = pricing.formattedPrice,
            priceMicros = pricing.priceAmountMicros
        )
    }

    sealed class ClientState {
        object Idle : ClientState()
        object Initializing : ClientState()
        object Connected : ClientState()
        data class Error(val error: String) : ClientState()
    }

    sealed class PurchaseState {
        object Idle : PurchaseState()
        object Processing : PurchaseState()
        object UserCancelled : PurchaseState()
        object ClientNotReady : PurchaseState()
        data class Success(val purchase: Purchase) : PurchaseState()
        data class Error(val error: BillingError) : PurchaseState()
    }
}

class BillingError(val error: String, val code: Int = 0) :
    RuntimeException("BillingError - code: $code, error: $error") {
    constructor(result: BillingResult) : this(result.debugMessage, result.responseCode)
}
