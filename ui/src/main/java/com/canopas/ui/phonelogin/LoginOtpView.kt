package com.canopas.ui.phonelogin

import android.annotation.SuppressLint
import android.content.Context
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.TextField
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterSemiBoldFont
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.ui.R
import kotlinx.coroutines.delay

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun LoginOtpView(phoneNo: String, verificationId: String) {

    val viewModel = hiltViewModel<LoginOtpViewModel>()
    val context = LocalContext.current
    val state by viewModel.otpState.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(verificationId, phoneNo)
    })

    Scaffold(
        topBar = {
            TopAppBar(
                title = {},
                navigationIcon = {
                    IconButton(onClick = {
                        viewModel.popBackStack()
                    }) {
                        Icon(
                            Icons.Filled.ArrowBack,
                            contentDescription = null,
                            tint = colors.textPrimary
                        )
                    }
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        state.let { otpState ->
            when (otpState) {
                is OtpVerificationState.START -> {
                    OtpView(viewModel, context, phoneNo)
                }
                is OtpVerificationState.FAILURE -> {
                    val message = otpState.message
                    showBanner(context, message)
                    viewModel.resetState()
                }
                else -> {}
            }
        }
    }
}

@Composable
fun OtpView(viewModel: LoginOtpViewModel, context: Context, phoneNo: String) {
    val isVerificationInProgress by viewModel.isVerificationInProgress.collectAsState()

    var currentTime by remember {
        mutableStateOf(TIME_COUNTDOWN)
    }
    val runTimer by remember {
        mutableStateOf(true)
    }
    val scrollState = rememberScrollState()
    val enteredOtp by viewModel.enteredOtp.collectAsState()
    val showOtpInvalidError by viewModel.showInvalidOtpError.collectAsState()

    Box(
        modifier = Modifier
            .fillMaxSize()
            .verticalScroll(scrollState)
            .background(colors.background)
    ) {
        Column(
            modifier = Modifier
                .align(Alignment.TopCenter)
                .widthIn(max = 600.dp)
                .fillMaxWidth()
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 48.dp, bottom = 40.dp),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Image(
                    painter = painterResource(id = R.drawable.ic_nolonely_logo),
                    contentDescription = null,
                    modifier = Modifier
                        .size(75.dp)
                )

                Text(
                    text = stringResource(R.string.login_otp_screen_verification_title_text),
                    color = colors.textPrimary,
                    textAlign = TextAlign.Center,
                    style = AppTheme.typography.h1TextStyle,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 48.dp)
                )

                Text(
                    text = stringResource(R.string.login_otp_screen_verification_message_text),
                    color = if (isDarkMode) subHeaderTextDark else subHeaderTextLight,
                    textAlign = TextAlign.Center,
                    style = AppTheme.typography.bodyTextStyle1,
                    lineHeight = 22.sp,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 48.dp, start = 40.dp, end = 40.dp)
                )

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 12.dp, start = 40.dp, end = 40.dp),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Text(
                        text = phoneNo,
                        color = if (isDarkMode) subHeaderTextDark else subHeaderTextLight,
                        style = AppTheme.typography.subTitleTextStyle1,
                    )
                    Image(
                        painter = painterResource(id = R.drawable.ic_edit_phone_number),
                        contentDescription = null,
                        modifier = Modifier
                            .padding(start = 12.dp)
                            .motionClickEvent {
                                viewModel.popBackStack()
                            }
                            .size(18.dp)
                    )
                }
            }

            Box(contentAlignment = Alignment.Center) {
                Text(
                    text = if (enteredOtp.isEmpty()) stringResource(R.string.login_otp_enter_code_text) else "",
                    color = if (isDarkMode) emptyOtpTextDark else emptyOtpTextLight,
                    modifier = Modifier.fillMaxSize(),
                    style = AppTheme.typography.defaultOtpTextStyle,
                )

                TextField(
                    value = enteredOtp,
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(horizontal = 40.dp),
                    colors = textFieldColors(),
                    textStyle = AppTheme.typography.enterOtpTextStyle,
                    onValueChange = { otp ->
                        viewModel.onOtpInputChanged(otp, context)
                    },
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number,
                        imeAction = ImeAction.Done
                    )
                )

                Divider(
                    Modifier
                        .widthIn(max = 600.dp)
                        .fillMaxWidth()
                        .padding(horizontal = 90.dp)
                        .align(Alignment.BottomCenter),
                    thickness = 1.dp,
                    color = if (showOtpInvalidError) MaterialTheme.colors.error else colors.textSecondary.copy(0.12f)
                )
            }

            AnimatedVisibility(visible = showOtpInvalidError) {
                Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                    Text(
                        text = stringResource(id = R.string.phone_login_invalid_code),
                        color = MaterialTheme.colors.error,
                        style = AppTheme.typography.captionTextStyle,
                        textAlign = TextAlign.Center,
                        modifier = Modifier
                            .padding(top = 4.dp)
                            .fillMaxWidth()
                    )
                }
            }
            PrimaryButton(
                text = stringResource(R.string.login_otp_screen_btn_verify_text),
                modifier = Modifier
                    .motionClickEvent { }
                    .fillMaxWidth()
                    .padding(start = 20.dp, end = 20.dp, top = 40.dp, bottom = 20.dp)
                    .align(Alignment.CenterHorizontally),
                shape = RoundedCornerShape(50),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = if (isVerificationInProgress) colors.primary.copy(0.6f) else colors.primary,
                    disabledBackgroundColor = colors.primary.copy(0.6f),
                    contentColor = Color.White
                ),
                onClick = {
                    viewModel.verifyOtp(context)
                },
                enabled = enteredOtp.length == OTP_LENGTH,
                isProcessing = isVerificationInProgress
            )

            AnimatedVisibility(visible = runTimer) {
                Box(
                    modifier = Modifier.fillMaxWidth(),
                    contentAlignment = Alignment.Center
                ) {
                    TextButton(
                        onClick = {
                            currentTime = TIME_COUNTDOWN
                            viewModel.resetState()
                            viewModel.resendVerificationCode(context)
                        },
                        enabled = currentTime <= CONDITIONAL_VALUE_ZERO
                    ) {
                        LaunchedEffect(key1 = currentTime, key2 = runTimer, block = {
                            if (currentTime > CONDITIONAL_VALUE_ZERO && runTimer) {
                                delay(TIMER_NOTIFIER_INTERVAL)
                                currentTime--
                            }
                        })

                        Text(
                            text = buildAnnotatedString {
                                append(stringResource(R.string.login_otp_resend_otp_btn_text))
                                if (currentTime > CONDITIONAL_VALUE_ZERO) {
                                    withStyle(
                                        style = SpanStyle(
                                            color = colors.textPrimary,
                                            fontSize = 16.sp,
                                            fontFamily = InterSemiBoldFont,
                                        )
                                    ) {
                                        append("00:${if (currentTime >= CONDITIONAL_VALUE_TEN) currentTime else "0$currentTime"}")
                                    }
                                }
                            },
                            style = AppTheme.typography.buttonStyle,
                            color = if (currentTime > CONDITIONAL_VALUE_ZERO) colors.primary.copy(
                                0.6f
                            ) else colors.primary
                        )
                    }
                }
            }
        }
    }
}

private val subHeaderTextLight = Color(0xDE1C191F)
private val subHeaderTextDark = Color(0xCCFFFFFF)
private val emptyOtpTextDark = Color(0xCCFFFFFF)
private val emptyOtpTextLight = Color(0x661C191F)
