package com.canopas.ui.phonelogin

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.ServiceError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.DEVICE_TYPE
import com.canopas.base.data.model.LOGIN_TYPE_PHONE
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.utils.Device
import com.canopas.campose.countrypicker.countryList
import com.canopas.campose.countrypicker.model.Country
import com.canopas.ui.R
import com.canopas.ui.home.settings.buypremiumsubscription.DEVICE_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class PhoneLoginViewModel @Inject constructor(
    application: Application,
    private val authManager: AuthManager,
    private val appAnalytics: AppAnalytics,
    private val firebaseCrashlytics: FirebaseCrashlytics,
    private val device: Device,
    private val appDispatcher: AppDispatcher,
    private val navManager: NavManager,
) : AndroidViewModel(application) {
    val selectedCountry = MutableStateFlow<Country?>(null)
    val phoneNumberState = MutableStateFlow("")
    val countryEmptyErrorState = MutableStateFlow(false)
    val phoneEmptyErrorState = MutableStateFlow(false)
    private var storedVerificationId: String? = ""
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    val phoneState = MutableStateFlow<PhoneVerificationState>(PhoneVerificationState.START)
    val isVerificationInProgress = MutableStateFlow(false)

    fun requestFirebasePhoneVerification(context: Context) {
        appAnalytics.logEvent("tap_phone_sign_in_next", null)
        val countryCode = selectedCountry.value?.dial_code
        val phoneNo = phoneNumberState.value
        when {
            countryCode.isNullOrEmpty() -> {
                appAnalytics.logEvent("tap_phone_sign_in_err_code", null)
                countryEmptyErrorState.tryEmit(true)
                return
            }
            phoneNo.isEmpty() || phoneNo.length < 3 -> {
                appAnalytics.logEvent("tap_phone_sign_in_err_phone", null)
                phoneEmptyErrorState.tryEmit(true)
                return
            }
            else -> {
                proceedPhoneVerification(context)
            }
        }
    }

    private fun proceedPhoneVerification(context: Context) {
        isVerificationInProgress.tryEmit(true)
        val phoneNo = selectedCountry.value?.dial_code + phoneNumberState.value
        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                Timber.d("onVerificationCompleted:$credential")
                signInWithPhoneAuthCredential(credential, context, phoneNo)
            }

            override fun onVerificationFailed(e: FirebaseException) {
                isVerificationInProgress.tryEmit(false)
                Timber.e(e, "onVerificationFailed")
                firebaseCrashlytics.recordException(
                    Throwable("Firebase Auth Error - Verification failed for phone ${phoneNumberState.value}", e)
                )
                if (e is FirebaseAuthInvalidCredentialsException) {
                    phoneEmptyErrorState.tryEmit(true)
                } else if (e is FirebaseTooManyRequestsException) {
                    phoneState.tryEmit(PhoneVerificationState.FAILURE(context.getString(R.string.login_otp_screen_too_many_otp_requests_error)))
                }
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken,
            ) {
                Timber.d("OnCodeSent")
                storedVerificationId = verificationId
                navigateToOtpView()
                isVerificationInProgress.tryEmit(false)
            }
        }

        val options = PhoneAuthOptions.newBuilder(FirebaseAuth.getInstance())
            .setPhoneNumber(phoneNo)
            .setTimeout(60L, TimeUnit.SECONDS)
            .setActivity(context as Activity)
            .setCallbacks(callbacks)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    private fun navigateToOtpView() = viewModelScope.launch {
        withContext(appDispatcher.MAIN) {
            val phoneNo = selectedCountry.value?.dial_code + " " + phoneNumberState.value
            navManager.navigateToOtpView(phoneNo, storedVerificationId!!)
        }
    }

    private fun signInWithPhoneAuthCredential(
        credential: PhoneAuthCredential,
        context: Context,
        phoneNo: String
    ) {
        Firebase.auth.signInWithCredential(credential)
            .addOnCompleteListener(context as Activity) { task ->
                if (task.isSuccessful && task.isComplete) {
                    val user = task.result?.user
                    if (user != null) {
                        user.getIdToken(false).addOnCompleteListener { tokenResultTask ->
                            if (tokenResultTask.isSuccessful && tokenResultTask.result != null) {
                                requestToLoginWithNumber(user.phoneNumber!!, tokenResultTask.result!!.token!!)
                            } else {
                                val exception = tokenResultTask.exception
                                if (exception != null) {
                                    Timber.e(tokenResultTask.exception)
                                } else {
                                    phoneState.tryEmit(PhoneVerificationState.FAILURE(context.getString(R.string.login_otp_screen_phone_auth_error)))
                                }
                                logError("Firebase phone auth - Get Id Token error for phone ${user.phoneNumber}", exception)
                            }
                        }
                    } else {
                        phoneState.tryEmit(PhoneVerificationState.FAILURE((context.getString(R.string.login_otp_screen_phone_auth_error))))
                        logError("Firebase phone auth - User nil for phone $phoneNo")
                    }
                } else {
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        Timber.e(task.exception)
                    } else {
                        phoneState.tryEmit(PhoneVerificationState.FAILURE((context.getString(R.string.login_otp_screen_phone_auth_error))))
                    }
                    logError("Firebase phone auth - Get user error for phone $phoneNo", task.exception)
                }
            }
    }

    private fun logError(message: String, cause: Throwable? = null) {
        firebaseCrashlytics.recordException(
            Throwable("Firebase Auth Error - $message", cause)
        )
    }

    fun requestToLoginWithNumber(phoneNumber: String, token: String) = viewModelScope.launch {
        phoneState.tryEmit(PhoneVerificationState.LOADING)
        val loginRequest = LoginRequest(
            DEVICE_TYPE,
            device.getId(),
            device.getAppVersionCode(),
            device.deviceModel(),
            device.getDeviceOsVersion(),
            token, phoneNumber, "", login_type = LOGIN_TYPE_PHONE
        )

        withContext(appDispatcher.IO) {
            try {
                val response = authManager.login(loginRequest, false)
                if (response.hasError()) {
                    // Log errors to analytics and crashlytics both as this is
                    // critical functionality of app
                    val bundle = Bundle()
                    bundle.putInt("status_code", response.statusCode)
                    appAnalytics.logEvent("error_phone_verification_server", bundle)

                    if (response.error != ServiceError.NETWORKERROR) {
                        logError("Firebase phone auth - API error $response")
                    }
                    phoneState.tryEmit(PhoneVerificationState.FAILURE(response.errorMessage))
                } else {
                    onLoginSuccess()
                }
            } catch (e: Exception) {
                e.localizedMessage?.let {
                    phoneState.tryEmit(PhoneVerificationState.FAILURE((it)))
                }
            }
        }
    }

    private suspend fun onLoginSuccess() = withContext(appDispatcher.MAIN) {
        navManager.popSignInScreens()
        appAnalytics.logEvent("success_phone_login", null)
        if (authManager.currentUser?.session?.limitedAccess == true) {
            navManager.navigateToBuyPremiumScreen(showBackArrow = false, textType = DEVICE_LIMIT_TEXT_TYPE)
        } else {
            authManager.currentUser?.let {
                if (!it.hasFirstname() || !it.hasValidEmail()) {
                    navManager.navigateToProfileScreen(showBackArrow = false, promptPremium = true)
                }
            }
        }
    }

    fun onCountrySelected(country: Country) {
        selectedCountry.tryEmit(country)
        countryEmptyErrorState.tryEmit(false)
    }

    fun onPhoneNoChange(phoneNo: String) {
        phoneNumberState.tryEmit(phoneNo)
        phoneEmptyErrorState.tryEmit(false)
    }

    fun onStart(countryLanCode: String) {
        appAnalytics.logEvent("view_phone_sign_in", null)
        when (selectedCountry.value) {
            null -> {
                if (countryLanCode.isNotEmpty()) {
                    selectedCountry.value =
                        countryList(getApplication<Application>().applicationContext).single { it.code == countryLanCode }
                } else {
                    selectedCountry.value =
                        countryList(getApplication<Application>().applicationContext).single { it.code == "IN" }
                }
            }
        }
    }

    fun resetState() {
        phoneState.tryEmit(PhoneVerificationState.START)
    }
}

sealed class PhoneVerificationState {
    object START : PhoneVerificationState()
    object LOADING : PhoneVerificationState()
    data class FAILURE(val message: String) : PhoneVerificationState()
}
