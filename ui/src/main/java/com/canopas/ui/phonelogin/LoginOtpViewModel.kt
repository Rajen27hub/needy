package com.canopas.ui.phonelogin

import android.app.Activity
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.ServiceError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.DEVICE_TYPE
import com.canopas.base.data.model.FREE_USER
import com.canopas.base.data.model.LOGIN_TYPE_PHONE
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.utils.Device
import com.canopas.ui.R
import com.canopas.ui.home.settings.buypremiumsubscription.DEVICE_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import com.google.firebase.FirebaseException
import com.google.firebase.FirebaseTooManyRequestsException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.firebase.ktx.Firebase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@HiltViewModel
class LoginOtpViewModel @Inject constructor(
    private val authManager: AuthManager,
    private val appAnalytics: AppAnalytics,
    private val firebaseCrashlytics: FirebaseCrashlytics,
    private val device: Device,
    private val appDispatcher: AppDispatcher,
    private val navManager: NavManager,
    private val notificationManagerCompat: NotificationManagerCompat,
) : ViewModel() {
    val otpState = MutableStateFlow<OtpVerificationState>(OtpVerificationState.START)
    var phoneNumber = MutableStateFlow("")
    var storedVerificationId = MutableStateFlow("")
    private lateinit var callbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    val isVerificationInProgress = MutableStateFlow(false)
    val showInvalidOtpError = MutableStateFlow(false)
    val enteredOtp = MutableStateFlow("")
    val isAboveAndroidS = MutableStateFlow(Build.VERSION.SDK_INT >= Build.VERSION_CODES.S)

    fun onStart(verificationId: String, phoneNo: String) {
        storedVerificationId.value = verificationId
        phoneNumber.value = phoneNo
    }

    fun verifyOtp(context: Context) {
        val credential = PhoneAuthProvider.getCredential(storedVerificationId.value, enteredOtp.value)
        signInWithPhoneAuthCredential(credential, context, phoneNumber.value.trim(' '))
    }

    private fun signInWithPhoneAuthCredential(
        credential: PhoneAuthCredential,
        context: Context,
        phoneNo: String
    ) {
        isVerificationInProgress.tryEmit(true)
        Firebase.auth.signInWithCredential(credential)
            .addOnCompleteListener(context as Activity) { task ->
                if (task.isSuccessful && task.isComplete) {
                    val user = task.result?.user
                    if (user != null) {
                        user.getIdToken(false).addOnCompleteListener { tokenResultTask ->
                            if (tokenResultTask.isSuccessful && tokenResultTask.result != null) {
                                requestToLoginWithNumber(user.phoneNumber!!, tokenResultTask.result!!.token!!)
                            } else {
                                val exception = tokenResultTask.exception
                                if (exception != null) {
                                    Timber.e(tokenResultTask.exception)
                                } else {
                                    otpState.tryEmit(
                                        OtpVerificationState.FAILURE(
                                            context.getString(
                                                R.string.login_otp_screen_phone_auth_error
                                            )
                                        )
                                    )
                                }
                                logError("Firebase phone auth - Get Id Token error for phone ${user.phoneNumber}", exception)
                            }
                        }
                    } else {
                        isVerificationInProgress.tryEmit(false)
                        otpState.tryEmit(OtpVerificationState.FAILURE((context.getString(R.string.login_otp_screen_phone_auth_error))))
                        logError("Firebase phone auth - User nil for phone $phoneNo")
                    }
                } else {
                    isVerificationInProgress.tryEmit(false)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        Timber.e(task.exception)
                        showInvalidOtpError.tryEmit(true)
                    } else {
                        otpState.tryEmit(OtpVerificationState.FAILURE((context.getString(R.string.login_otp_screen_phone_auth_error))))
                    }
                    logError("Firebase phone auth - Get user error for phone $phoneNo", task.exception)
                }
            }
    }

    private fun logError(message: String, cause: Throwable? = null) {
        firebaseCrashlytics.recordException(
            Throwable("Firebase Auth Error - $message", cause)
        )
    }

    fun requestToLoginWithNumber(phoneNumber: String, token: String) = viewModelScope.launch {
        val loginRequest = LoginRequest(
            DEVICE_TYPE,
            device.getId(),
            device.getAppVersionCode(),
            device.deviceModel(),
            device.getDeviceOsVersion(),
            token, phoneNumber, "", login_type = LOGIN_TYPE_PHONE
        )
        withContext(appDispatcher.IO) {
            try {
                otpState.tryEmit(OtpVerificationState.LOADING)
                val response = authManager.login(loginRequest, false)
                if (response.hasError()) {
                    // Log errors to analytics and crashlytics both as this is
                    // critical functionality of app
                    val bundle = Bundle()
                    bundle.putInt("status_code", response.statusCode)
                    appAnalytics.logEvent("error_phone_verification_server", bundle)

                    if (response.error != ServiceError.NETWORKERROR) {
                        logError("Firebase phone auth - API error $response")
                    } else if (response.error == ServiceError.SERVERERROR) {
                        otpState.tryEmit(OtpVerificationState.FAILURE("Server error encountered"))
                    } else {
                        otpState.tryEmit(OtpVerificationState.FAILURE(response.errorMessage))
                    }
                } else {
                    onLoginSuccess()
                }
            } catch (e: Exception) {
                e.localizedMessage?.let {
                    otpState.tryEmit(OtpVerificationState.FAILURE((it)))
                }
            }
        }
    }

    fun resendVerificationCode(
        context: Context
    ) {

        callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            override fun onVerificationCompleted(credential: PhoneAuthCredential) {
                Timber.e("onVerificationCompleted:$credential")
                signInWithPhoneAuthCredential(credential, context, phoneNumber.value.trim(' '))
            }

            override fun onVerificationFailed(e: FirebaseException) {
                Timber.e(e, "onVerificationFailed")
                firebaseCrashlytics.recordException(
                    Throwable("Firebase Auth Error - Verification failed for phone ${phoneNumber.value}", e)
                )
                if (e is FirebaseTooManyRequestsException) {
                    otpState.tryEmit(OtpVerificationState.FAILURE(context.getString(R.string.login_otp_screen_too_many_otp_requests_error)))
                }
            }

            override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken,
            ) {
                storedVerificationId.value = verificationId
            }
        }

        val optionsBuilder = PhoneAuthOptions.newBuilder(Firebase.auth)
            .setPhoneNumber(phoneNumber.value.trim(' '))
            .setTimeout(60L, TimeUnit.SECONDS)
            .setActivity(context as Activity)
            .setCallbacks(callbacks)
        PhoneAuthProvider.verifyPhoneNumber(optionsBuilder.build())
    }

    private suspend fun onLoginSuccess() = withContext(appDispatcher.MAIN) {
        appAnalytics.logEvent("success_phone_login", null)
        proceedLoginSuccessNavigation()
    }

    fun isNotificationOn(): Boolean {
        return notificationManagerCompat.areNotificationsEnabled()
    }

    fun proceedLoginSuccessNavigation() {
        if (isAboveAndroidS.value && !isNotificationOn()) {
            navManager.navigateToNotificationPermissionView()
        } else {
            navManager.popSignInScreens()
            if (authManager.currentUser?.session?.limitedAccess == true) {
                navManager.navigateToBuyPremiumScreen(
                    showBackArrow = false,
                    textType = DEVICE_LIMIT_TEXT_TYPE
                )
            } else {
                authManager.currentUser?.let {
                    if (!it.hasFirstname() || !it.hasValidEmail()) {
                        navManager.navigateToProfileScreen(showBackArrow = false, promptPremium = true)
                    } else if (it.userType == FREE_USER) {
                        navManager.navigateToBuyPremiumScreen(
                            showBackArrow = false,
                            showDismissButton = true
                        )
                    }
                }
            }
        }
    }

    fun popBackStack() {
        navManager.popBack()
    }

    fun resetState() {
        otpState.tryEmit(OtpVerificationState.START)
    }

    fun onOtpInputChanged(otp: String, context: Context) {
        enteredOtp.tryEmit(otp.take(OTP_LENGTH))
        showInvalidOtpError.tryEmit(false)
        if (enteredOtp.value.length == OTP_LENGTH) {
            verifyOtp(context)
        }
    }
}

sealed class OtpVerificationState {
    object START : OtpVerificationState()
    object LOADING : OtpVerificationState()
    data class FAILURE(val message: String) : OtpVerificationState()
}
