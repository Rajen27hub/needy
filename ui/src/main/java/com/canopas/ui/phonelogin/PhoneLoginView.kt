package com.canopas.ui.phonelogin

import android.content.Context
import android.telephony.TelephonyManager
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.text.selection.LocalTextSelectionColors
import androidx.compose.foundation.text.selection.TextSelectionColors
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.TextFieldColors
import androidx.compose.material.TextFieldDefaults
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.campose.countrypicker.CountryPickerBottomSheet
import com.canopas.campose.countrypicker.countryList
import com.canopas.campose.countrypicker.expandable
import com.canopas.ui.R

const val TIME_COUNTDOWN: Int = 30
const val TIMER_NOTIFIER_INTERVAL = 1000L
const val CONDITIONAL_VALUE_ZERO = 0
const val CONDITIONAL_VALUE_TEN = 10
const val OTP_LENGTH = 6

@Composable
fun PhoneLoginView(navController: NavHostController) {

    val viewModel = hiltViewModel<PhoneLoginViewModel>()
    val phoneVerificationState by viewModel.phoneState.collectAsState()
    val context = LocalContext.current

    Scaffold(
        topBar = {
            TopAppBar(
                title = {},
                navigationIcon = {
                    IconButton(onClick = {
                        navController.popBackStack()
                    }) {
                        Icon(
                            Icons.Filled.ArrowBack,
                            contentDescription = null,
                            tint = colors.textPrimary
                        )
                    }
                },
                backgroundColor = colors.background,
                contentColor = colors.textPrimary,
                elevation = 0.dp
            )
        }
    ) {
        phoneVerificationState.let { phoneState ->
            when (phoneState) {
                is PhoneVerificationState.START -> {
                    PhoneView(viewModel, context, modifier = Modifier.padding(it))
                }
                is PhoneVerificationState.FAILURE -> {
                    val message = phoneState.message
                    showBanner(context, message)
                    viewModel.resetState()
                }
                else -> {}
            }
        }
    }
}

@OptIn(ExperimentalComposeUiApi::class)
@Composable
fun PhoneView(viewModel: PhoneLoginViewModel, context: Context, modifier: Modifier) {
    val focusManager = LocalFocusManager.current
    val manager = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    val countryLanCode = manager.networkCountryIso.uppercase()
    val isVerificationInProgress by viewModel.isVerificationInProgress.collectAsState()

    LaunchedEffect(key1 = Unit) {
        viewModel.onStart(countryLanCode)
    }

    val phoneFocusRequester = remember { FocusRequester() }
    val countryFocusRequester = remember { FocusRequester() }

    var expanded by remember { mutableStateOf(false) }
    val selectedCountry by viewModel.selectedCountry.collectAsState()
    val phoneNoState by viewModel.phoneNumberState.collectAsState()
    val isCountryInvalid by viewModel.countryEmptyErrorState.collectAsState()
    val isPhoneNoInvalid by viewModel.phoneEmptyErrorState.collectAsState()

    if (isCountryInvalid) {
        countryFocusRequester.requestFocus()
    } else if (isPhoneNoInvalid) {
        phoneFocusRequester.requestFocus()
    }

    val keyboardController = LocalSoftwareKeyboardController.current
    DisposableEffect(key1 = Unit, effect = {
        onDispose {
            keyboardController?.hide()
        }
    })

    CountryPickerBottomSheet(
        title = {
            Text(
                modifier = modifier
                    .fillMaxWidth()
                    .padding(16.dp),
                text = stringResource(id = R.string.phone_login_screen_select_country_text),
                textAlign = TextAlign.Center,
                style = AppTheme.typography.h2TextStyle
            )
        },
        show = expanded,
        onItemSelected = {
            viewModel.onCountrySelected(it)
            focusManager.moveFocus(FocusDirection.Next)
            expanded = false
        },
        onDismissRequest = { expanded = false }
    ) {
        val scrollState = rememberScrollState()
        Box(
            modifier = Modifier
                .fillMaxSize()
                .verticalScroll(scrollState)
                .background(colors.background)
        ) {

            Column(
                modifier = Modifier
                    .align(Alignment.TopCenter)
                    .widthIn(max = 600.dp)
                    .fillMaxWidth()
            ) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 48.dp, bottom = 42.dp),
                    verticalArrangement = Arrangement.Top,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Image(
                        painter = painterResource(id = R.drawable.ic_nolonely_logo),
                        contentDescription = null,
                        modifier = Modifier
                            .size(75.dp)
                    )

                    Text(
                        text = stringResource(R.string.phone_login_screen_enter_phone_number_title_text),
                        color = colors.textPrimary,
                        textAlign = TextAlign.Center,
                        style = AppTheme.typography.h1TextStyle,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 48.dp)
                    )

                    Text(
                        text = stringResource(R.string.phone_login_screen_verification_message_text),
                        color = if (isDarkMode) subHeaderTextDark else subHeaderTextLight,
                        textAlign = TextAlign.Center,
                        style = AppTheme.typography.bodyTextStyle1,
                        lineHeight = 22.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(top = 48.dp, start = 40.dp, end = 40.dp)
                    )
                }

                val defaultSelectedCountry by remember { mutableStateOf(countryList(context).first()) }
                val countryValue = "+ ${defaultSelectedCountry.dial_code.trim('+')}"

                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 40.dp)
                        .border(
                            border = BorderStroke(
                                1.dp,
                                if (isDarkMode) subHeaderTextDark else boxTextLight
                            ),
                            shape = RoundedCornerShape(16.dp)
                        ),
                    horizontalArrangement = Arrangement.Start,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    Row(
                        modifier = Modifier
                            .expandable(
                                menuLabel = stringResource(R.string.phone_login_screen_select_country_text),
                                onExpandedChange = { expanded = !expanded }
                            ),
                        verticalAlignment = Alignment.CenterVertically
                    ) {
                        Text(
                            text = if (selectedCountry == null) countryValue else "+ ${
                            selectedCountry!!.dial_code.trim('+')
                            }",
                            color = colors.textPrimary,
                            textAlign = TextAlign.Center,
                            style = AppTheme.typography.countryCodeTextStyle,
                            lineHeight = 22.sp,
                            modifier = Modifier
                                .padding(start = 20.dp)
                        )

                        Icon(
                            painter = painterResource(id = R.drawable.ic_arrow),
                            contentDescription = null,
                            Modifier
                                .padding(start = 12.dp)
                                .rotate(if (expanded) 270f else 90f),
                            tint = colors.textPrimary
                        )
                    }

                    Divider(
                        Modifier
                            .padding(start = 20.dp)
                            .width(1.dp)
                            .height(25.dp),
                        thickness = 1.dp,
                        color = colors.textSecondary
                    )
                    CompositionLocalProvider(LocalTextSelectionColors provides TextSelectionColors(colors.primary, colors.primary)) {
                        TextField(
                            value = phoneNoState,
                            modifier = Modifier
                                .motionClickEvent { }
                                .focusRequester(phoneFocusRequester),
                            onValueChange = { phoneNo ->
                                viewModel.onPhoneNoChange(phoneNo)
                            },
                            textStyle = AppTheme.typography.phoneNumberTextStyle,
                            placeholder = {
                                Text(
                                    stringResource(R.string.phone_login_screen_mobile_number_text),
                                    style = AppTheme.typography.bodyTextStyle1,
                                    color = if (isDarkMode) subHeaderTextDark else boxTextLight
                                )
                            },
                            colors = textFieldColors(),
                            keyboardOptions = KeyboardOptions(
                                keyboardType = KeyboardType.Number,
                                imeAction = ImeAction.Done
                            ),
                            keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
                            singleLine = true
                        )
                    }
                }

                Text(
                    text = if (isPhoneNoInvalid) stringResource(id = R.string.phone_login_error_invalid_mobile_number) else "",
                    color = MaterialTheme.colors.error,
                    textAlign = TextAlign.Center,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 40.dp),
                )

                PrimaryButton(
                    onClick = {
                        keyboardController?.hide()
                        viewModel.requestFirebasePhoneVerification(context)
                    },
                    shape = RoundedCornerShape(50),
                    colors = ButtonDefaults.buttonColors(
                        backgroundColor = if (isVerificationInProgress) colors.primary.copy(0.6f) else colors.primary,
                        contentColor = Color.White
                    ),
                    modifier = Modifier
                        .motionClickEvent { }
                        .padding(horizontal = 20.dp, vertical = 48.dp)
                        .fillMaxWidth(),
                    text = stringResource(R.string.phone_login_screen_next_btn_text),
                    isProcessing = isVerificationInProgress
                )
            }
        }
    }
}

@Composable
fun textFieldColors(): TextFieldColors {
    return TextFieldDefaults.outlinedTextFieldColors(
        unfocusedLabelColor = colors.textSecondary,
        unfocusedBorderColor = Color.Transparent,
        focusedLabelColor = colors.primary,
        focusedBorderColor = Color.Transparent,
        cursorColor = colors.primary,
        backgroundColor = Color.Transparent,
        textColor = colors.textPrimary
    )
}

@Preview
@Composable
fun PreviewPhoneLoginView() {
    PhoneLoginView(rememberNavController())
}

private val subHeaderTextLight = Color(0xDE1C191F)
private val subHeaderTextDark = Color(0xCCFFFFFF)
private val boxTextLight = Color(0x661C191F)
