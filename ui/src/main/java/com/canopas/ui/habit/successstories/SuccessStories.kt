package com.canopas.ui.habit.successstories

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.textColor
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.data.model.SuccessStory
import com.canopas.data.utils.TestUtils.Companion.successStory
import com.canopas.ui.R
import com.canopas.ui.customview.jetpackview.SuccessStoryText

@Composable
fun SuccessStories(activityId: String = "", isEditMode: Boolean = false) {
    val viewModel = hiltViewModel<SuccessStoriesViewModel>()
    val successStoriesListState = rememberLazyListState()

    LaunchedEffect(key1 = activityId) {
        viewModel.retrieveSuccessStory(activityId)
        viewModel.isEditModeEnable(isEditMode)
    }

    val state by viewModel.successStoryState.collectAsState()
    val context = LocalContext.current

    Scaffold(topBar = {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = stringResource(R.string.success_story_top_bar_title),
                    navigationIconOnClick = { viewModel.managePopBack() }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
    }, backgroundColor = colors.background) {
    state.let { state ->
        when (state) {
            is SuccessStoryState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .fillMaxSize()
                        .background(colors.background)
                        .padding(it)
                ) {
                    CircularProgressIndicator(color = colors.primary)
                }
            }
            is SuccessStoryState.FAILURE -> {
                val message = state.message
                LaunchedEffect(key1 = message) {
                    showBanner(context, message)
                }
            }

            is SuccessStoryState.SUCCESS -> {
                val successStory = state.successStories
                StorySuccessState(successStory, viewModel, successStoriesListState)
            }
        }
    }
}
}

@Composable
fun StorySuccessState(
    stories: List<SuccessStory>,
    viewModel: SuccessStoriesViewModel,
    successStoriesListState: LazyListState
) {
    Box {
        if (stories.isEmpty()) {
            Box(
                modifier = Modifier.fillMaxSize(),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = stringResource(id = R.string.setting_screen_no_success_story_posted_message),
                    style = AppTheme.typography.bodyTextStyle1,
                    color = colors.textSecondary
                )
            }
        } else {
            LazyColumn(
                state = successStoriesListState,
                modifier = Modifier
                    .fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top,
                contentPadding = PaddingValues(top = 12.dp, bottom = 16.dp)
            ) {
                item {
                    stories.forEach { story ->
                        SuccessStoryColumnView(story, viewModel)
                    }
                }
            }
        }
    }
}

@Composable
fun SuccessStoryColumnView(
    story: SuccessStory,
    viewModel: SuccessStoriesViewModel,
) {
    val randomCardColor = remember {
        successStoriesColors.random()
    }
    val user = story.user
    val isEditMode = viewModel.isEditModeOption

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .padding(vertical = 8.dp, horizontal = 20.dp),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .motionClickEvent {
                    viewModel.onSuccessStoryClicked(story.id, isEditMode)
                }
                .background(
                    randomCardColor.copy(0.25f),
                    shape = RoundedCornerShape(25.dp)
                )
                .clip(RoundedCornerShape(25.dp))
                .fillMaxWidth()
                .padding(18.dp)
        ) {
            Row(
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.CenterVertically,
                modifier = Modifier
                    .fillMaxWidth()
            ) {

                ProfileImageView(
                    data = user.profileImageChar(),
                    modifier = Modifier.size(95.dp),
                    char = user.profileImageChar()
                )

                Spacer(modifier = Modifier.width(20.dp))

                val userName =
                    user.fullName.trim()
                        .ifEmpty { stringResource(id = R.string.success_story_username) }

                Text(
                    text = userName,
                    textAlign = TextAlign.Center,
                    style = AppTheme.typography.buttonStyle,
                    color = textColor,
                    modifier = Modifier
                        .background(
                            randomCardColor.copy(0.5f),
                            shape = RoundedCornerShape(16.dp)
                        )
                        .padding(horizontal = 12.dp, vertical = 8.dp),
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
            }
            SuccessStoryTitleText(story.title)
            SuccessStoryText(story.description)
        }
    }
}

@Composable
fun SuccessStoryTitleText(titleText: String) {
    Text(
        text = titleText,
        style = AppTheme.typography.bodyTextStyle1,
        color = textColor,
        maxLines = 1,
        overflow = TextOverflow.Ellipsis,
        modifier = Modifier
            .padding(vertical = 12.dp)
            .fillMaxWidth(),
    )
}

@Preview
@Composable
fun PreviewSuccessStoryView() {
    SuccessStories("1", false)
}

@Preview
@Composable
fun PreviewSuccessStoryContainView() {

    val viewModel: SuccessStoriesViewModel = hiltViewModel()

    SuccessStoryColumnView(story = successStory, viewModel)
}

private val successStoryCardBg1 = Color(0xFFFB709A)
private val successStoryCardBg2 = Color(0xFF7046AA)
private val successStoryCardBg3 = Color(0xff80CBC4)
private val successStoryCardBg4 = Color(0XFF03DAC5)
private val successStoryCardBg5 = Color(0xff007aff)
private val successStoryCardBg6 = Color(0X33FB709A)
private val successStoryCardBg7 = Color(0xDDD6A5A5)
private val successStoriesColors = listOf(
    successStoryCardBg1, successStoryCardBg2, successStoryCardBg3,
    successStoryCardBg4, successStoryCardBg5, successStoryCardBg6, successStoryCardBg7
)
