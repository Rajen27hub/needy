package com.canopas.ui.habit.configureactivity

import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Scaffold
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.utils.Utils.Companion.decodeFromBase64
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.themes.ComposeTheme.colors

const val DAYS_IN_A_MONTH = 30

@Composable
fun ConfigureActivityView(
    activityId: Int = 0,
    title: String = "",
    description: String = "",
    activityName: String = "",
    defaultTime: String = "",
    defaultDuration: String = "",
    storyId: Int = 0,
    goalsTitle: String = ""
) {
    val viewModel = hiltViewModel<ConfigureActivityViewModel>()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(storyId)
    })

    val context = LocalContext.current
    val setTime by viewModel.showActivityTimeSelector.collectAsState()
    val enableDoneButton by viewModel.enableDoneButton.collectAsState()
    val showLoader by viewModel.showLoader.collectAsState()
    val navigateToSettingsEvent by viewModel.navigateToAppSettings.collectAsState()
    val navigateToSettings = navigateToSettingsEvent.getContentIfNotHandled() ?: false

    if (navigateToSettings) {
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        val uri: Uri = Uri.fromParts("package", context.packageName, null)
        intent.data = uri
        context.startActivity(intent)
    }

    val openNotificationDialog = viewModel.openNotificationDialog.collectAsState()

    if (openNotificationDialog.value) {
        ShowNotificationDialog(viewModel)
    }

    Scaffold(topBar = {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = decodeFromBase64(title).ifEmpty { decodeFromBase64(activityName) },
                    navigationIconOnClick = { viewModel.popBack() }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
    }) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(it),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            LazyColumn(
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .fillMaxWidth()
                    .weight(1f),
                verticalArrangement = Arrangement.Top,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                item {
                    ConfigureActivityTime(viewModel, setTime, subscription = null, defaultTime)
                }

                item {
                    LaunchedEffect(key1 = Unit, block = {
                        if (defaultDuration.isNotEmpty()) {
                            viewModel.setDefaultDuration(defaultDuration.toInt())
                        }
                    })
                    ConfigureActivityDuration(viewModel)
                }

                item {
                    ConfigureRepeatSchedule(viewModel, context)
                }

                item {
                    if (setTime) {
                        ConfigureReminderView(viewModel, context)
                    }
                }

                item {
                    ConfigureNotesPrompt(viewModel)
                    Spacer(modifier = Modifier.height(50.dp))
                }
            }

            Box(
                Modifier
                    .offset(y = -(12.dp))
                    .height(12.dp)
                    .fillMaxWidth()
            ) {
                Box(
                    Modifier
                        .fillMaxSize()
                        .background(
                            Brush.verticalGradient(
                                listOf(
                                    Color.Transparent,
                                    colors.background
                                )
                            )
                        )
                )
            }
            ConfigureDoneButtonView(showLoader, viewModel, enableDoneButton, activityId, title, description, goalsTitle)
        }
    }
}

val customActivityColor =
    listOf(
        "#FFECE4", "#FFF7CF", "#D1EDEA", "#FFCC80", "#DDC6DF", "#F6E4E3", "#D1F3E5", "#FFEFDB",
        "#DFF2FF", "#C2ECF5", "#FDE7C6", "#D8DCF8", "#D1E0F1", "#FBD7D6", "#C8E3C0", "#F0EDC1", "#C7E8F0", "#F4D5E1"
    )
