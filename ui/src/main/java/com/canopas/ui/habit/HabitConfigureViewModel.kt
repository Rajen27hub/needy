package com.canopas.ui.habit

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.utils.Utils
import com.canopas.data.FeatureFlag
import com.canopas.data.model.ActivityData
import com.canopas.data.model.SuccessStory
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.home.explore.DEFAULT_SLEEP_ACTIVITY_ID
import com.canopas.ui.home.explore.DEFAULT_WAKE_UP_ACTIVITY_ID
import com.canopas.ui.home.settings.buypremiumsubscription.ACTIVITY_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

const val MAX_SUBSCRIPTIONS_FOR_FREE_USER = 3

@HiltViewModel
class HabitConfigureViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val authManager: AuthManager,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val preferencesDataStore: NoLonelyPreferences,
    private val resources: Resources
) : ViewModel() {

    val state = MutableStateFlow<HabitState>(HabitState.LOADING)
    val videoAutoplayDelayInMillis = MutableStateFlow(10000L)
    private var requestState: RequestState = RequestState.None
    private var userSubsCount: Int = 0
    var isGoalsEnabled = MutableStateFlow(FeatureFlag.isGoalsEnabled)

    fun onStart(activityId: Int) {
        appAnalytics.logEvent("view_habit_configure", null)
        fetchData(activityId)
        fetchUserSubscriptionsCount()
        handleRequestState()
    }

    private fun fetchData(activityId: Int) =
        viewModelScope.launch {
            if (state.value !is HabitState.SUCCESS) {
                state.tryEmit(HabitState.LOADING)
            }
            withContext(appDispatcher.IO) {
                val activities = async { service.getActivityById(activityId) }
                val stories = async { service.retrieveSuccessStoriesByActivityId(activityId) }
                activities.await().onSuccess { activity ->
                    stories.await().onSuccess { stories ->
                        state.tryEmit(HabitState.SUCCESS(activity, stories))
                    }.onFailure {
                        Timber.e(it.toString())
                        state.tryEmit(HabitState.FAILURE(it.toUserError(resources), it is APIError.NetworkError))
                    }
                }.onFailure {
                    Timber.e(it.toString())
                    state.tryEmit(HabitState.FAILURE(it.toUserError(resources), it is APIError.NetworkError))
                }
            }
        }

    private fun handleRequestState() {
        if (requestState == RequestState.Login && authManager.authState.isVerified) {
            handleStartJourney()
        }
        if (requestState == RequestState.Premium && authManager.authState.isPremium) {
            handleStartJourney()
        }
        requestState = RequestState.None
    }

    fun onSuccessStoryClicked(Id: Int) {
        navManager.navigateToSuccessStoryDetail(Id, false)
    }

    fun handleStartJourney() {
        val successState = state.value as? HabitState.SUCCESS ?: return

        if (successState.activity.isSubscribed) {
            appAnalytics.logEvent("tap_habit_configure_open_activity", null)
            navManager.navigateToActivityStatusByActivityId(activityId = successState.activity.id)
        } else {
            appAnalytics.logEvent("tap_habit_configure_start_journey", null)

            when {
                !authManager.authState.isVerified -> {
                    navigateToLogin()
                    requestState = RequestState.Login
                }
                (userSubsCount >= MAX_SUBSCRIPTIONS_FOR_FREE_USER && authManager.authState.isFree) -> {
                    openBuyPremiumScreen()
                }
                else -> {
                    onStartJourney(
                        successState.activity.id,
                        Utils.encodeToBase64(successState.activity.name),
                        successState.activity.default_activity_time ?: "08:00",
                        successState.activity.default_activity_duration ?: 1
                    )
                }
            }
        }
    }

    private fun fetchUserSubscriptionsCount() = viewModelScope.launch {
        withContext(appDispatcher.IO) {
            service.getUserSubscriptions().onSuccess {
                userSubsCount = it.filter { subs ->
                    subs.is_active && !(subs.activity.id == DEFAULT_WAKE_UP_ACTIVITY_ID || subs.activity.id == DEFAULT_SLEEP_ACTIVITY_ID)
                }.size
                preferencesDataStore.setUserSubscriptionsCount(userSubsCount)
            }.onFailure {
                Timber.e(it.localizedMessage)
            }
        }
    }

    fun onStartJourney(
        id: Int,
        name: String,
        defaultActivityTime: String,
        defaultActivityDuration: Int
    ) {
        if (isGoalsEnabled.value) {
            navManager.navigateToWhyThisActivityScreen(id, name, defaultActivityTime, defaultActivityDuration)
        } else {
            navigateToConfigureActivity(id, name, defaultActivityTime, defaultActivityDuration)
        }
    }

    fun navigateToSuccessStory(activityId: Int) {
        navManager.navigateToSuccessStory(activityId)
    }

    fun updateVideoAutoplayDelay(timeInMillis: Long) {
        videoAutoplayDelayInMillis.value = timeInMillis
    }

    fun navigateToLogin() {
        navManager.navigateToSignInMethods()
    }

    fun openBuyPremiumScreen() {
        navManager.navigateToBuyPremiumScreen(textType = ACTIVITY_LIMIT_TEXT_TYPE)
        requestState = RequestState.Premium
    }

    fun managePopBack() {
        navManager.popBack()
    }

    fun navigateToConfigureActivity(
        activityId: Int,
        activityName: String,
        defaultActivityTime: String,
        defaultActivityDuration: Int
    ) {
        navManager.navigateToConfigureActivity(activityId, activityName, defaultActivityTime, defaultActivityDuration)
    }
}

sealed class HabitState {
    object LOADING : HabitState()
    data class SUCCESS(
        val activity: ActivityData,
        val successStories: List<SuccessStory>
    ) : HabitState()
    data class FAILURE(val message: String, val isNetworkError: Boolean) : HabitState()
}

enum class RequestState {
    None, Login, Premium
}
