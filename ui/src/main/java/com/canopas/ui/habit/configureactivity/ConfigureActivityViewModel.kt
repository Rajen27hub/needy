package com.canopas.ui.habit.configureactivity

import android.content.res.Resources
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.utils.Device
import com.canopas.base.ui.Event
import com.canopas.base.ui.model.DurationTabType
import com.canopas.base.ui.model.RepeatTabStyle
import com.canopas.data.FeatureFlag.isGoalsEnabled
import com.canopas.data.event.ActivitySubscriptionEvent
import com.canopas.data.model.ActivityReminders
import com.canopas.data.model.CustomActivity
import com.canopas.data.model.SubscribeBody
import com.canopas.data.model.SubscriptionGoal
import com.canopas.data.model.TYPE_END_GOAL
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.customactivity.MINIMUM_CHARACTER_LENGTH
import com.canopas.ui.home.settings.buypremiumsubscription.ACTIVITY_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import com.canopas.ui.utils.DateUtils
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.util.Calendar
import javax.inject.Inject

const val TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_START = 2
const val TYPE_ACTIVITY_REMINDER_FIVE_MIN_BEFORE_ACTIVITY = 3
const val TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_END = 4

@HiltViewModel
class ConfigureActivityViewModel @Inject constructor(
    private val appAnalytics: AppAnalytics,
    private val calendar: Calendar,
    private val navManager: NavManager,
    private val notificationManagerCompat: NotificationManagerCompat,
    private val device: Device,
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val eventBus: EventBus,
    private val resources: Resources
) : ViewModel() {

    val showLoader = MutableStateFlow(false)
    val openNotificationDialog = MutableStateFlow(false)
    val navigateToAppSettings = MutableStateFlow(Event(false))
    val configureActivityState = MutableStateFlow<ConfigureActivityState>(ConfigureActivityState.IDLE)
    val showActivityTimeSelector = MutableStateFlow(true)
    val selectedActivityTime = MutableStateFlow<String?>(null)
    val durationTabType = MutableStateFlow(DurationTabType.DURATION_30_MIN)
    val repeatTabType = MutableStateFlow(RepeatTabStyle.REPEAT_DAILY)
    val selectedWeeklyDays = MutableStateFlow<List<Int>>(emptyList())
    val selectedMonthlyDays = MutableStateFlow<List<Int>>(emptyList())
    val selectedReminderTypes = MutableStateFlow<List<Int>>(emptyList())
    val promptForNote = MutableStateFlow(false)

    val enableDoneButton = MutableStateFlow(true)

    val title = MutableStateFlow("")
    val showShortTitleError = MutableStateFlow(false)
    val showMinimumSelectionText = MutableStateFlow(false)
    private val storyId = MutableStateFlow(0)

    init {
        selectedWeeklyDays.value = (0..6).map { 1 }
        selectedMonthlyDays.value = (0..30).map { if (it == 6 || it == 13 || it == 20) 1 else 0 }
        selectedReminderTypes.value = (0..2).map { if (it == 0) 1 else 0 }
    }

    fun setActivityTimeSelector(enabled: Boolean) {
        if (showActivityTimeSelector.value == enabled) return

        if (enabled) {
            selectedActivityTime.value = "08:00"
            val newList = ArrayList(selectedReminderTypes.value)
            newList[0] = 1
            selectedReminderTypes.tryEmit(newList)
        } else {
            selectedActivityTime.tryEmit(null)
            selectedReminderTypes.tryEmit(selectedReminderTypes.value.map { 0 })
        }

        showActivityTimeSelector.tryEmit(enabled)
        enableDoneButton.tryEmit(true)
    }

    fun handleDurationTabSelection(selectedTab: DurationTabType) {
        durationTabType.tryEmit(selectedTab)
        if (durationTabType.value == DurationTabType.DURATION_1_MIN) {
            val newList = ArrayList(selectedReminderTypes.value)
            newList[2] = 0
            selectedReminderTypes.tryEmit(newList)
        }
    }

    fun handleRepeatTabSelection(selectedTab: RepeatTabStyle) {
        repeatTabType.tryEmit(selectedTab)
        when (selectedTab) {
            RepeatTabStyle.REPEAT_DAILY -> {
                enableDoneButton.tryEmit(true)
                showMinimumSelectionText.tryEmit(false)
            }
            RepeatTabStyle.REPEAT_WEEKLY -> {
                appAnalytics.logEvent("tap_habit_configure_daily_config")
                enableDoneButton.tryEmit(!(selectedWeeklyDays.value.none { it == 1 }))
                showMinimumSelectionText.tryEmit(selectedWeeklyDays.value.none { it == 1 })
            }
            RepeatTabStyle.REPEAT_MONTHLY -> {
                appAnalytics.logEvent("tap_habit_configure_monthly_settings")
                enableDoneButton.tryEmit(!(selectedMonthlyDays.value.none { it == 1 }))
                showMinimumSelectionText.tryEmit(selectedMonthlyDays.value.none { it == 1 })
            }
        }
    }

    fun toggleReminderSelection(selectedType: Int) {
        val newList = ArrayList(selectedReminderTypes.value)
        newList[selectedType] = newList[selectedType].xor(1)
        selectedReminderTypes.tryEmit(newList)
    }

    fun toggleNotesPrompt(enabled: Boolean) {
        promptForNote.tryEmit(enabled)
    }

    fun toggleWeeklySelection(selectedIndex: Int) {
        val newList = ArrayList(selectedWeeklyDays.value)
        newList[selectedIndex] = newList[selectedIndex].xor(1)
        selectedWeeklyDays.tryEmit(newList)
        enableDoneButton.tryEmit(!(selectedWeeklyDays.value.none { it == 1 }))
        showMinimumSelectionText.tryEmit((selectedWeeklyDays.value.none { it == 1 }))
    }

    fun toggleMonthlySelection(selectedIndex: Int) {
        val newList = ArrayList(selectedMonthlyDays.value)
        newList[selectedIndex] = newList[selectedIndex].xor(1)
        selectedMonthlyDays.tryEmit(newList)
        enableDoneButton.tryEmit(!(selectedMonthlyDays.value.none { it == 1 }))
        showMinimumSelectionText.tryEmit((selectedMonthlyDays.value.none { it == 1 }))
    }

    fun setActivityTime(time: String) {
        selectedActivityTime.tryEmit(time)
    }

    override fun onCleared() {
        super.onCleared()
        calendar.set(Calendar.HOUR_OF_DAY, DateUtils.getSystemCurrentHour())
        calendar.set(Calendar.MINUTE, DateUtils.getSystemCurrentMinute())
    }

    fun onStart(id: Int) {
        appAnalytics.logEvent("view_habit_configure", null)
        if (id != 0) {
            storyId.value = id
        }
    }

    fun getNearestMinute(minute: Int): Int {
        val remainder = minute % 15
        return if (remainder <= 7) minute - remainder else minute + (15 - remainder)
    }

    fun handleDoneBtnClick(
        activityId: Int,
        title: String,
        description: String,
        goalsTitle: String
    ) {
        if ((selectedReminderTypes.value.none { it == 1 }) || isNotificationOn()) {
            when {
                title.isNotEmpty() || description.isNotEmpty() -> {
                    appAnalytics.logEvent("tap_habit_configure_subscribe_custom", null)
                    subscribeCustomActivityWithConfiguration(title, description, goalsTitle)
                }
                else -> {
                    appAnalytics.logEvent("tap_habit_configure_subscribe", null)
                    subscribeActivityWithConfiguration(activityId, goalsTitle)
                }
            }
        } else {
            openNotificationDialog()
        }
    }

    private fun subscribeCustomActivityWithConfiguration(
        title: String,
        description: String,
        goalsTitle: String
    ) =
        viewModelScope.launch {
            val activityTime = selectedActivityTime.value?.filterNot { it.isWhitespace() }?.take(5)
            val repeatType = if (repeatTabType.value == RepeatTabStyle.REPEAT_MONTHLY) 2 else 1
            showLoader.tryEmit(true)
            configureActivityState.tryEmit(ConfigureActivityState.PROCESSING)
            val customActivityRequest = CustomActivity(
                title,
                description,
                customActivityColor.random(),
                device.timeZone(),
                repeatType,
                getRepeatOnValue(),
                activity_time = activityTime,
                activity_duration = durationTabType.value.duration,
                reminders = getRemindersList(),
                show_note_on_complete = promptForNote.value,
                goal = SubscriptionGoal(goalsTitle, TYPE_END_GOAL)
            )
            withContext(appDispatcher.IO) {
                service.createCustomActivity(customActivityRequest).onSuccess {
                    appAnalytics.logEvent("activity_subscribed_custom", null)
                    configureActivityState.tryEmit(ConfigureActivityState.CONFIGURED)
                    withContext(appDispatcher.MAIN) {
                        if (storyId.value != 0) {
                            navManager.popExploreScreen()
                        } else {
                            navManager.popHabitDetailScreen()
                        }
                        navManager.navigateToActivityStatusBySubscriptionId(
                            subscriptionId = it.subscriptionId,
                            "Congratulations!"
                        )
                    }
                }.onFailure { e ->
                    Timber.e(e)
                    if (e is APIError) {
                        when (e) {
                            is APIError.MaxLimitReached -> {
                                withContext(appDispatcher.MAIN) {
                                    navManager.navigateToBuyPremiumScreen(textType = ACTIVITY_LIMIT_TEXT_TYPE)
                                }
                            }
                            else -> {
                                configureActivityState.tryEmit(ConfigureActivityState.FAILURE(e.toUserError(resources)))
                            }
                        }
                    }
                    showLoader.tryEmit(false)
                }
            }
        }

    private fun subscribeActivityWithConfiguration(activityId: Int, goalsTitle: String) =
        viewModelScope.launch {
            val activityTime = selectedActivityTime.value?.filterNot { it.isWhitespace() }?.take(5)
            val repeatType = if (repeatTabType.value == RepeatTabStyle.REPEAT_MONTHLY) 2 else 1
            configureActivityState.tryEmit(ConfigureActivityState.PROCESSING)
            showLoader.tryEmit(true)
            val subscription = SubscribeBody(
                activity_id = activityId,
                timezone = device.timeZone(),
                is_prompted = false,
                repeat_type = repeatType,
                repeat_on = getRepeatOnValue(),
                activity_time = activityTime,
                activity_duration = durationTabType.value.duration,
                reminders = getRemindersList(),
                show_note_on_complete = promptForNote.value,
                goal = SubscriptionGoal(goalsTitle, TYPE_END_GOAL)
            )
            withContext(appDispatcher.IO) {
                service.subscribeActivity(subscription).onSuccess {
                    appAnalytics.logEvent("activity_subscribed", null)
                    configureActivityState.tryEmit(ConfigureActivityState.CONFIGURED)
                    eventBus.post(ActivitySubscriptionEvent(activityId))
                    withContext(appDispatcher.MAIN) {
                        if (storyId.value != 0) {
                            if (isGoalsEnabled) {
                                navManager.popConfigureActivityScreen(storyId.value)
                            } else {
                                popBack()
                            }
                        } else {
                            navManager.popHabitDetailScreen()
                            navManager.navigateToActivityStatusBySubscriptionId(
                                subscriptionId = it.subscriptionId,
                                "Congratulations!"
                            )
                        }
                    }
                }.onFailure { e ->
                    Timber.e(e)
                    configureActivityState.emit(ConfigureActivityState.FAILURE(e.toUserError(resources)))
                    showLoader.tryEmit(false)
                }
            }
        }

    private fun getRepeatOnValue(): String {
        return when (repeatTabType.value) {
            RepeatTabStyle.REPEAT_WEEKLY -> {
                selectedWeeklyDays.value =
                    selectedWeeklyDays.value.drop(1) + selectedWeeklyDays.value.take(1)
                val days = selectedWeeklyDays.value.mapIndexed { index, i ->
                    if (i == 1) (index + 1) else null
                }.filterNotNull()
                days.joinToString()
            }
            RepeatTabStyle.REPEAT_MONTHLY -> {
                val days = selectedMonthlyDays.value.mapIndexed { index, i ->
                    if (i == 1) (index + 1) else null
                }.filterNotNull()
                days.joinToString()
            }
            else -> {
                ""
            }
        }
    }

    private fun getRemindersList(): List<ActivityReminders> {
        val activityTimeString = selectedActivityTime.value ?: return emptyList()
        val time = activityTimeString.filterNot { it.isWhitespace() }.take(5).split(":")
            .map { it.toInt() }
        val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm")
        val list = ArrayList<ActivityReminders>()
        selectedReminderTypes.value.forEachIndexed { index, i ->
            if (i == 1) {
                when (index) {
                    0 -> {
                        list.add(
                            ActivityReminders(
                                TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_START,
                                activityTimeString.filterNot { it.isWhitespace() }.take(5)
                            )
                        )
                    }
                    1 -> {
                        val reminderTime = LocalTime.of(time[0], time[1]).minusMinutes(5)
                        list.add(
                            ActivityReminders(
                                TYPE_ACTIVITY_REMINDER_FIVE_MIN_BEFORE_ACTIVITY,
                                formatter.format(reminderTime)
                            )
                        )
                    }
                    2 -> {
                        val reminderTime = LocalTime.of(time[0], time[1])
                            .plusMinutes(durationTabType.value.duration.toLong())
                        list.add(
                            ActivityReminders(
                                TYPE_ACTIVITY_REMINDER_ON_ACTIVITY_END,
                                formatter.format(reminderTime)
                            )
                        )
                    }
                }
            }
        }
        return list
    }

    fun openNotificationDialog() {
        openNotificationDialog.tryEmit(true)
    }

    fun closeNotificationDialog() {
        openNotificationDialog.tryEmit(false)
    }

    fun addNotificationPermission() {
        navigateToAppSettings.value = Event(true)
        openNotificationDialog.tryEmit(false)
    }

    fun isNotificationOn(): Boolean {
        return notificationManagerCompat.areNotificationsEnabled()
    }

    fun popBack() {
        navManager.popBack()
    }

    fun onTitleChange(value: String) {
        title.tryEmit(value)
        showShortTitleError.tryEmit(title.value.trim().length < MINIMUM_CHARACTER_LENGTH)
    }

    fun setDefaultDuration(defaultDuration: Int) {
        when (defaultDuration) {
            0, 1 -> durationTabType.tryEmit(DurationTabType.DURATION_1_MIN)
            15 -> durationTabType.tryEmit(DurationTabType.DURATION_15_MIN)
            30 -> durationTabType.tryEmit(DurationTabType.DURATION_30_MIN)
            45 -> durationTabType.tryEmit(DurationTabType.DURATION_45_MIN)
            60 -> durationTabType.tryEmit(DurationTabType.DURATION_1_HOUR)
        }
    }
}

sealed class ConfigureActivityState {
    object IDLE : ConfigureActivityState()
    object PROCESSING : ConfigureActivityState()
    object CONFIGURED : ConfigureActivityState()
    data class FAILURE(val message: String) : ConfigureActivityState()
}
