package com.canopas.ui.habit.configureactivity

import android.content.Context
import androidx.compose.animation.AnimatedVisibility
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.layout.onGloballyPositioned
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.White
import com.canopas.base.ui.model.RepeatTabStyle
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.ui.R

@Composable
fun ConfigureRepeatSchedule(
    viewModel: ConfigureActivityViewModel,
    context: Context
) {
    val selectedRepeatType by viewModel.repeatTabType.collectAsState()
    val selectedWeeklyDays by viewModel.selectedWeeklyDays.collectAsState()
    val selectedMonthlyDays by viewModel.selectedMonthlyDays.collectAsState()
    val showMinimumSelectionText by viewModel.showMinimumSelectionText.collectAsState()
    val days by remember {
        mutableStateOf(context.resources.getStringArray(R.array.days))
    }
    val localDensity = LocalDensity.current
    var monthlyBoxHeight by remember {
        mutableStateOf(200.dp)
    }

    Text(
        text = stringResource(R.string.configure_activity_how_often_title_text),
        style = AppTheme.typography.configureActivityHeaderStyle,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 40.dp, start = 20.dp),
        textAlign = TextAlign.Start,
        letterSpacing = -(0.8.sp),
        color = ComposeTheme.colors.textPrimary
    )

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(start = 20.dp, end = 20.dp, top = 16.dp),
        contentAlignment = Alignment.TopCenter
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .clip(RoundedCornerShape(20.dp))
                .border(
                    1.dp,
                    color = if (ComposeTheme.isDarkMode) darkTabBackground.copy(0.8f) else lightTabBackground.copy(
                        0.8f
                    ),
                    shape = RoundedCornerShape(20.dp)
                ),
            contentAlignment = Alignment.Center
        ) {
            if (selectedRepeatType == RepeatTabStyle.REPEAT_WEEKLY) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 60.dp, bottom = 28.dp, start = 10.dp, end = 10.dp),
                    horizontalArrangement = Arrangement.SpaceAround
                ) {
                    days.forEachIndexed { index, day ->
                        Box(
                            modifier = Modifier
                                .size(36.dp)
                                .clip(RoundedCornerShape(50.dp))
                                .background(
                                    if (selectedWeeklyDays[index] == 1) ColorPrimary else Color.Transparent
                                )
                                .motionClickEvent {
                                    viewModel.toggleWeeklySelection(index)
                                },
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = day,
                                style = if (selectedWeeklyDays[index] == 1) AppTheme.typography.configureActiveItemStyle else AppTheme.typography.configureInactiveItemStyle,
                                textAlign = TextAlign.Center,
                                color = if (selectedWeeklyDays[index] == 1) White else ComposeTheme.colors.textPrimary.copy(
                                    0.5f
                                )
                            )
                        }
                    }
                }
            }

            if (selectedRepeatType == RepeatTabStyle.REPEAT_MONTHLY) {
                BoxWithConstraints(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 40.dp, bottom = 8.dp),
                ) {
                    val monthlyBoxMultiplier = when {
                        maxWidth in 350.dp..400.dp -> 0.8f
                        maxWidth > 400.dp -> 0.6f
                        else -> 1.1f
                    }
                    val horizontalPadding = when {
                        maxWidth in 350.dp..400.dp -> 4.dp
                        maxWidth > 400.dp -> 8.dp
                        else -> 1.dp
                    }
                    LazyVerticalGrid(
                        GridCells.Fixed(7),
                        modifier = Modifier
                            .height(monthlyBoxHeight)
                            .padding(horizontal = 10.dp)
                            .onGloballyPositioned { coordinates ->
                                monthlyBoxHeight = with(localDensity) {
                                    (coordinates.size.width)
                                        .times(monthlyBoxMultiplier)
                                        .toDp()
                                }
                            },
                        userScrollEnabled = false,
                        verticalArrangement = Arrangement.Center
                    ) {
                        itemsIndexed((0..DAYS_IN_A_MONTH).toList()) { index, day ->
                            val isSelected = selectedMonthlyDays[index] == 1
                            Column(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(horizontal = horizontalPadding, vertical = 8.dp)
                                    .clip(CircleShape)
                            ) {
                                Box(
                                    modifier = Modifier
                                        .motionClickEvent(
                                            onClick = {
                                                viewModel.toggleMonthlySelection(index)
                                            }
                                        )
                                        .background(
                                            if (isSelected) ComposeTheme.colors.primary else Color.Transparent,
                                            shape = CircleShape
                                        )
                                        .clip(CircleShape)
                                        .size(width = 50.dp, height = 35.dp),
                                    contentAlignment = Alignment.Center
                                ) {
                                    Text(
                                        text = "${day + 1}",
                                        style = if (isSelected) AppTheme.typography.configureActiveItemStyle else AppTheme.typography.configureInactiveItemStyle,
                                        color = if (isSelected) White else ComposeTheme.colors.textPrimary.copy(
                                            0.5f
                                        ),
                                        textAlign = TextAlign.Center
                                    )
                                }
                            }
                        }
                    }
                }
            }
        }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(20.dp))
                .background(
                    if (ComposeTheme.isDarkMode) darkTabBackground else lightTabBackground,
                    shape = RoundedCornerShape(20.dp)
                )
                .wrapContentHeight(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            TabView(
                modifier = Modifier.weight(1f),
                backgroundColor = if (selectedRepeatType == RepeatTabStyle.REPEAT_DAILY) ComposeTheme.colors.primary else Color.Transparent,
                onClick = {
                    viewModel.handleRepeatTabSelection(RepeatTabStyle.REPEAT_DAILY)
                },
                titleText = RepeatTabStyle.REPEAT_DAILY.getTitle(context),
                textStyle = if (selectedRepeatType == RepeatTabStyle.REPEAT_DAILY) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
                textColor = if (selectedRepeatType == RepeatTabStyle.REPEAT_DAILY) White else ComposeTheme.colors.textSecondary,
            )

            TabView(
                modifier = Modifier.weight(1f),
                backgroundColor = if (selectedRepeatType == RepeatTabStyle.REPEAT_WEEKLY) ComposeTheme.colors.primary else Color.Transparent,
                onClick = {
                    viewModel.handleRepeatTabSelection(RepeatTabStyle.REPEAT_WEEKLY)
                },
                titleText = RepeatTabStyle.REPEAT_WEEKLY.getTitle(context),
                textStyle = if (selectedRepeatType == RepeatTabStyle.REPEAT_WEEKLY) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
                textColor = if (selectedRepeatType == RepeatTabStyle.REPEAT_WEEKLY) White else ComposeTheme.colors.textSecondary,
            )

            TabView(
                modifier = Modifier.weight(1f),
                backgroundColor = if (selectedRepeatType == RepeatTabStyle.REPEAT_MONTHLY) ComposeTheme.colors.primary else Color.Transparent,
                onClick = {
                    viewModel.handleRepeatTabSelection(RepeatTabStyle.REPEAT_MONTHLY)
                },
                titleText = RepeatTabStyle.REPEAT_MONTHLY.getTitle(context),
                textStyle = if (selectedRepeatType == RepeatTabStyle.REPEAT_MONTHLY) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
                textColor = if (selectedRepeatType == RepeatTabStyle.REPEAT_MONTHLY) White else ComposeTheme.colors.textSecondary,
            )
        }
    }

    AnimatedVisibility(showMinimumSelectionText) {
        Text(
            text = stringResource(R.string.configure_activity_please_select_atleast_one_day_text),
            color = MaterialTheme.colors.error,
            style = AppTheme.typography.captionTextStyle,
            textAlign = TextAlign.Center,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 4.dp, start = 20.dp, end = 20.dp)
        )
    }
}

@Composable
fun ConfigureNotesPrompt(viewModel: ConfigureActivityViewModel) {

    val promptForNote by viewModel.promptForNote.collectAsState()

    Text(
        text = stringResource(R.string.configure_activity_prompt_for_note_title_text),
        style = AppTheme.typography.configureActivityHeaderStyle,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 40.dp, start = 20.dp),
        textAlign = TextAlign.Start,
        letterSpacing = -(0.8.sp),
        color = ComposeTheme.colors.textPrimary
    )

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(start = 20.dp, end = 20.dp, top = 20.dp)
            .border(
                1.dp,
                color = if (ComposeTheme.isDarkMode) darkTabBackground.copy(0.8f) else lightTabBackground.copy(
                    0.8f
                ),
                shape = RoundedCornerShape(18.dp)
            )
    ) {
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 12.dp, end = 12.dp, top = 8.dp, bottom = 8.dp)
                .motionClickEvent {
                    viewModel.toggleNotesPrompt(!promptForNote)
                },
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(10.dp)
        ) {
            Image(
                painter = painterResource(id = R.drawable.ic_baseline_check_circle),
                contentDescription = null,
                modifier = Modifier
                    .size(30.dp),
                colorFilter = ColorFilter.tint(
                    if (promptForNote) ColorPrimary else ComposeTheme.colors.textPrimary.copy(
                        0.25f
                    )
                )
            )

            Text(
                text = stringResource(R.string.configure_activity_on_activity_completion_text),
                style = AppTheme.typography.configureSubItemsStyle,
                color = ComposeTheme.colors.textPrimary
            )
        }
    }
}

private val lightTabBackground = Color(0xffFAF7F6)
private val darkTabBackground = Color(0xff313131)
