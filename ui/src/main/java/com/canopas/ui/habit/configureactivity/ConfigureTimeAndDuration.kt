package com.canopas.ui.habit.configureactivity

import android.text.format.DateFormat
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.NotificationDialogButtonColor
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.model.DurationTabType
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Subscription
import com.canopas.ui.R
import com.canopas.ui.customview.jetpackview.CustomWheelTimePicker
import java.time.LocalTime

@Composable
fun ConfigureActivityTime(
    viewModel: ConfigureActivityViewModel,
    setTime: Boolean,
    subscription: Subscription? = null,
    defaultTime: String? = null
) {
    Text(
        text = stringResource(R.string.configure_activity_when_title_text),
        style = AppTheme.typography.configureActivityHeaderStyle,
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                top = if (subscription != null && subscription.is_custom) 40.dp else 28.dp,
                start = 20.dp
            ),
        textAlign = TextAlign.Start,
        letterSpacing = -(0.8.sp),
        color = ComposeTheme.colors.textPrimary
    )

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(start = 20.dp, end = 20.dp, top = 16.dp),
        contentAlignment = Alignment.TopCenter
    ) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .wrapContentHeight()
                .clip(RoundedCornerShape(20.dp))
                .border(
                    1.dp,
                    color = if (ComposeTheme.isDarkMode) darkTabBackground.copy(0.8f) else lightTabBackground.copy(
                        0.8f
                    ),
                    shape = RoundedCornerShape(20.dp)
                ),
            contentAlignment = Alignment.Center
        ) {
            if (setTime) {
                val activityTime by viewModel.selectedActivityTime.collectAsState()
                val times = (activityTime ?: if (defaultTime.isNullOrEmpty()) "08:00" else defaultTime).split(":").map { it.trim().toInt() }

                CustomWheelTimePicker(
                    modifier = Modifier.padding(top = 60.dp, bottom = 16.dp, start = 20.dp),
                    is24HoursFormat = DateFormat.is24HourFormat(LocalContext.current),
                    startTime = LocalTime.of(
                        times[0],
                        viewModel.getNearestMinute(times[1])
                    ),
                    height = 200.dp,
                    selectedTextStyle = AppTheme.typography.configureActiveTabStyle,
                    normalTextStyle = AppTheme.typography.configureInactiveTabStyle
                ) {
                    viewModel.setActivityTime(it)
                }
            }
        }

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .clip(RoundedCornerShape(20.dp))
                .background(
                    if (ComposeTheme.isDarkMode) darkTabBackground else lightTabBackground,
                    shape = RoundedCornerShape(20.dp)
                )
                .wrapContentHeight(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            TabView(
                modifier = Modifier.weight(1f),
                backgroundColor = if (setTime) ComposeTheme.colors.primary else Color.Transparent,
                onClick = {
                    viewModel.setActivityTimeSelector(true)
                },
                titleText = stringResource(R.string.configure_activity_set_time_text),
                textStyle = if (setTime) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
                textColor = if (setTime) White else ComposeTheme.colors.textSecondary
            )

            TabView(
                modifier = Modifier.weight(1f),
                backgroundColor = if (!setTime) ComposeTheme.colors.primary else Color.Transparent,
                onClick = {
                    viewModel.setActivityTimeSelector(false)
                },
                titleText = stringResource(R.string.configure_activity_anytime_text),
                textStyle = if (!setTime) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
                textColor = if (!setTime) White else ComposeTheme.colors.textSecondary
            )
        }
    }
}

@Composable
fun ConfigureActivityDuration(
    viewModel: ConfigureActivityViewModel
) {
    val selectedDuration by viewModel.durationTabType.collectAsState()

    Text(
        text = stringResource(R.string.configure_activity_how_long_title_text),
        style = AppTheme.typography.configureActivityHeaderStyle,
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 40.dp, start = 20.dp),
        textAlign = TextAlign.Start,
        letterSpacing = -(0.8.sp),
        color = ComposeTheme.colors.textPrimary
    )

    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(top = 16.dp, start = 20.dp, end = 20.dp)
            .clip(RoundedCornerShape(20.dp))
            .background(
                if (ComposeTheme.isDarkMode) darkTabBackground else lightTabBackground,
                shape = RoundedCornerShape(20.dp)
            )
            .wrapContentHeight(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.Center
    ) {
        TabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (selectedDuration == DurationTabType.DURATION_1_MIN) ComposeTheme.colors.primary else Color.Transparent,
            onClick = {
                viewModel.handleDurationTabSelection(DurationTabType.DURATION_1_MIN)
            },
            titleText = DurationTabType.DURATION_1_MIN.duration.toString() + if (selectedDuration == DurationTabType.DURATION_1_MIN) "m" else "",
            textStyle = if (selectedDuration == DurationTabType.DURATION_1_MIN) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
            textColor = if (selectedDuration == DurationTabType.DURATION_1_MIN) White else ComposeTheme.colors.textSecondary,
        )

        TabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (selectedDuration == DurationTabType.DURATION_15_MIN) ComposeTheme.colors.primary else Color.Transparent,
            onClick = {
                viewModel.handleDurationTabSelection(DurationTabType.DURATION_15_MIN)
            },
            titleText = DurationTabType.DURATION_15_MIN.duration.toString() + if (selectedDuration == DurationTabType.DURATION_15_MIN) "m" else "",
            textStyle = if (selectedDuration == DurationTabType.DURATION_15_MIN) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
            textColor = if (selectedDuration == DurationTabType.DURATION_15_MIN) White else ComposeTheme.colors.textSecondary,
        )

        TabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (selectedDuration == DurationTabType.DURATION_30_MIN) ComposeTheme.colors.primary else Color.Transparent,
            onClick = {
                viewModel.handleDurationTabSelection(DurationTabType.DURATION_30_MIN)
            },
            titleText = DurationTabType.DURATION_30_MIN.duration.toString() + if (selectedDuration == DurationTabType.DURATION_30_MIN) "m" else "",
            textStyle = if (selectedDuration == DurationTabType.DURATION_30_MIN) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
            textColor = if (selectedDuration == DurationTabType.DURATION_30_MIN) White else ComposeTheme.colors.textSecondary,
        )

        TabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (selectedDuration == DurationTabType.DURATION_45_MIN) ComposeTheme.colors.primary else Color.Transparent,
            onClick = {
                viewModel.handleDurationTabSelection(DurationTabType.DURATION_45_MIN)
            },
            titleText = DurationTabType.DURATION_45_MIN.duration.toString() + if (selectedDuration == DurationTabType.DURATION_45_MIN) "m" else "",
            textStyle = if (selectedDuration == DurationTabType.DURATION_45_MIN) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
            textColor = if (selectedDuration == DurationTabType.DURATION_45_MIN) White else ComposeTheme.colors.textSecondary,
        )

        TabView(
            modifier = Modifier.weight(1f),
            backgroundColor = if (selectedDuration == DurationTabType.DURATION_1_HOUR) ComposeTheme.colors.primary else Color.Transparent,
            onClick = {
                viewModel.handleDurationTabSelection(DurationTabType.DURATION_1_HOUR)
            },
            titleText = DurationTabType.DURATION_1_HOUR.duration.toString(),
            textStyle = if (selectedDuration == DurationTabType.DURATION_1_HOUR) AppTheme.typography.configureActiveTabStyle else AppTheme.typography.configureInactiveTabStyle,
            textColor = if (selectedDuration == DurationTabType.DURATION_1_HOUR) White else ComposeTheme.colors.textSecondary,
        )
    }
}

@Composable
fun TabView(
    modifier: Modifier,
    backgroundColor: Color,
    onClick: () -> Unit,
    titleText: String,
    textColor: Color,
    textStyle: TextStyle,
) {
    Box(
        modifier = modifier
            .clip(RoundedCornerShape(20.dp))
            .background(
                color = backgroundColor,
                RoundedCornerShape(20.dp)
            )
            .padding(horizontal = 4.dp)
            .motionClickEvent {
                onClick()
            }
    ) {
        Text(
            text = titleText,
            modifier = Modifier
                .align(Alignment.Center)
                .padding(vertical = 8.dp),
            color = textColor,
            textAlign = TextAlign.Center,
            style = textStyle,
            lineHeight = 16.sp
        )
    }
}

@Composable
fun ShowNotificationDialog(viewModel: ConfigureActivityViewModel) {
    CustomAlertDialog(
        title = null,
        subTitle = stringResource(id = R.string.habit_configure_alert_dialog_description_text),
        confirmBtnText = stringResource(id = R.string.habit_configure_alert_dialog_confirm_button_text),
        dismissBtnText = stringResource(id = R.string.habit_configure_alert_dialog_dismiss_button_text),
        confirmBtnColor = NotificationDialogButtonColor,
        dismissBtnColor = NotificationDialogButtonColor,
        onConfirmClick = { viewModel.addNotificationPermission() },
        onDismissClick = { viewModel.closeNotificationDialog() }
    )
}

private val lightTabBackground = Color(0xffFAF7F6)
private val darkTabBackground = Color(0xff313131)
