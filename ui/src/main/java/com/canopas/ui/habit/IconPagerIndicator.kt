package com.canopas.ui.habit

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.PlayArrow
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntOffset
import androidx.compose.ui.unit.dp
import com.canopas.data.model.ActivityMedia
import com.canopas.data.model.MEDIA_TYPE_VIDEO
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.PagerState

@ExperimentalPagerApi
@Composable
fun IconPagerIndicator(
    pagerState: PagerState,
    medias: List<ActivityMedia>,
    modifier: Modifier = Modifier,
    activeColor: Color = HorizontalPagerActiveColor,
    inactiveColor: Color = HorizontalPagerInactiveColor,
    indicatorWidth: Dp = 9.dp,
    indicatorHeight: Dp = indicatorWidth,
    spacing: Dp = 10.dp,
    indicatorShape: Shape = RoundedCornerShape(50),
) {

    Box(
        modifier = modifier,
        contentAlignment = Alignment.CenterStart
    ) {
        Row(
            horizontalArrangement = Arrangement.spacedBy(spacing),
            verticalAlignment = Alignment.CenterVertically,
        ) {
            val indicatorModifier = Modifier
                .size(width = indicatorWidth, height = indicatorHeight)
                .background(color = inactiveColor, shape = indicatorShape)

            repeat(pagerState.pageCount) {
                Box(indicatorModifier) {
                    if (medias[it].type == MEDIA_TYPE_VIDEO) {
                        Icon(
                            Icons.Filled.PlayArrow,
                            contentDescription = null,
                            modifier = Modifier
                                .width(9.dp)
                                .height(9.dp),
                            tint = Color.Transparent
                        )
                    }
                }
            }
        }

        Box(
            Modifier
                .offset {
                    val scrollPosition = pagerState.currentPage + pagerState.currentPageOffset
                    IntOffset(
                        x = ((spacing + indicatorWidth) * scrollPosition).roundToPx(),
                        y = 0
                    )
                }
                .size(width = indicatorWidth, height = indicatorHeight)
                .background(
                    color = activeColor,
                    shape = indicatorShape,
                )
        ) {
            if (medias[pagerState.currentPage].type == 1) {
                Icon(
                    Icons.Filled.PlayArrow,
                    contentDescription = null,
                    modifier = Modifier
                        .width(9.dp)
                        .height(9.dp),
                    tint = Color.White
                )
            }
        }
    }
}

private val HorizontalPagerActiveColor = Color(0xFF000000)
private val HorizontalPagerInactiveColor = Color(0xFFA3B1C6)
