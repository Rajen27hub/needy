package com.canopas.ui.habit

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import coil.size.Scale
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.parse
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Benefit
import com.canopas.data.model.Quotes

@Composable
fun BenefitView(benefit: Benefit, activityColor: String) {
    Box(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth(),
    ) {
        if (benefit.isAlignRight) {
            RightAlignedView(benefit, activityColor)
        } else {
            LeftAlignedView(benefit, activityColor)
        }
    }
}

@Composable
fun LeftAlignedView(benefit: Benefit, activityColor: String) {
    Box(contentAlignment = Alignment.BottomStart) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 20.dp, end = 20.dp)
                .defaultMinSize(minHeight = 85.dp)
                .border(
                    width = 1.dp,
                    color = (
                        if (ComposeTheme.isDarkMode) darkBorderColor else Color
                            .parse(activityColor)
                            .copy(alpha = 0.5f)
                        ),
                    shape = RoundedCornerShape(20.dp)
                ),
            contentAlignment = Alignment.CenterEnd

        ) {
            Text(
                modifier = Modifier
                    .fillMaxWidth(0.7f)
                    .padding(horizontal = 20.dp),
                text = benefit.benefit,
                style = AppTheme.typography.bodyTextStyle1,
                color = if (ComposeTheme.isDarkMode) ComposeTheme.colors.textSecondary else ComposeTheme.colors.textPrimary,
                textAlign = TextAlign.Start,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )
        }

        val painter = rememberAsyncImagePainter(
            ImageRequest.Builder(LocalContext.current).data(data = benefit.imageUrl).apply(block = {
                Scale.FILL
            }).build()
        )

        Image(
            modifier = Modifier
                .size(130.dp)
                .padding(start = 25.dp),
            painter = painter,
            contentDescription = "benefit image",
            alignment = Alignment.BottomStart
        )
    }
}

@Composable
fun RightAlignedView(benefit: Benefit, activityColor: String) {
    Box(contentAlignment = Alignment.BottomEnd) {
        Box(
            modifier = Modifier
                .fillMaxWidth()
                .defaultMinSize(minHeight = 85.dp)
                .padding(start = 20.dp, end = 20.dp)
                .border(
                    width = 1.dp,
                    color = (
                        if (ComposeTheme.isDarkMode) darkBorderColor else Color
                            .parse(activityColor)
                            .copy(alpha = 0.5f)
                        ),
                    shape = RoundedCornerShape(20.dp)
                ),
            contentAlignment = Alignment.CenterStart
        ) {
            Text(
                modifier = Modifier
                    .fillMaxWidth(0.7f)
                    .padding(horizontal = 20.dp),
                text = benefit.benefit,
                style = AppTheme.typography.bodyTextStyle1,
                color = if (ComposeTheme.isDarkMode) ComposeTheme.colors.textSecondary else ComposeTheme.colors.textPrimary,
                textAlign = TextAlign.Start,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )
        }

        val painter = rememberAsyncImagePainter(
            ImageRequest.Builder(LocalContext.current).data(data = benefit.imageUrl).apply(block = {
                scale(Scale.FILL)
            }).build()
        )

        Image(
            modifier = Modifier
                .size(130.dp)
                .padding(end = 20.dp),
            painter = painter,
            contentDescription = "benefit image",
            alignment = Alignment.BottomEnd
        )
    }
}

@Composable
fun ThoughtCardView(quotes: List<Quotes>, activityColor: String) {

    Box(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .wrapContentHeight()
            .padding(vertical = 40.dp, horizontal = 20.dp)
            .clip(RoundedCornerShape(16.dp))
            .background(
                if (ComposeTheme.isDarkMode) darkHabitCardBg else Color
                    .parse(activityColor)
                    .copy(0.5f)
            )
    ) {
        Column(
            modifier = Modifier
                .background(
                    if (ComposeTheme.isDarkMode) darkHabitCardBg else Color
                        .parse(activityColor)
                        .copy(0.5f)
                )
                .fillMaxWidth()
        ) {
            Text(
                text = quotes[0].quote,
                style = AppTheme.typography.thoughtTextStyle,
                maxLines = 4,
                overflow = TextOverflow.Ellipsis,
                modifier = Modifier
                    .padding(start = 16.dp, end = 16.dp, top = 20.dp)
                    .wrapContentSize(),
                color = if (ComposeTheme.isDarkMode) ComposeTheme.colors.textSecondary else ComposeTheme.colors.textPrimary
            )
            Spacer(modifier = Modifier.height(14.dp))
            Box(
                modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.CenterEnd
            ) {
                Text(
                    text = "- ${quotes[0].authorName}",
                    style = AppTheme.typography.subTitleTextStyle1,
                    lineHeight = 30.sp,
                    modifier = Modifier.padding(end = 16.dp, bottom = 16.dp),
                    color = if (ComposeTheme.isDarkMode) ComposeTheme.colors.textSecondary else ComposeTheme.colors.textPrimary
                )
            }
        }
    }
}

private val darkHabitCardBg = Color(0xff222222)
private val darkBorderColor = Color(0x80605A85)
