package com.canopas.ui.habit

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.data.model.ActivityData
import com.canopas.data.model.ActivityMedia
import com.canopas.data.model.Benefit
import com.canopas.data.model.Quotes
import com.canopas.data.model.SuccessStory
import com.canopas.data.utils.TestUtils.Companion.activityData
import com.canopas.data.utils.TestUtils.Companion.successStory
import com.canopas.ui.R
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.rememberPagerState

const val STORY_MAX_LIMIT = 5
const val IMAGE_AUTOPLAY_DELAY_IN_MILLIS = 4000L
const val GIF_AUTOPLAY_DELAY_IN_MILLIS = 6000L

@Composable
fun HabitConfigureView(activityId: Int) {

    val viewModel = hiltViewModel<HabitConfigureViewModel>()

    LaunchedEffect(key1 = Unit) {
        viewModel.onStart(activityId)
    }

    val state by viewModel.state.collectAsState()
    var topBarTitle by remember {
        mutableStateOf("")
    }

    Scaffold(topBar = {
        TopAppBar(
            content = {
                TopAppBarContent(
                    title = topBarTitle,
                    navigationIconOnClick = { viewModel.managePopBack() }
                )
            },
            backgroundColor = colors.background,
            contentColor = colors.textPrimary,
            elevation = 0.dp
        )
    }) {
        state.let { state ->
            when (state) {
                is HabitState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .padding(it)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is HabitState.FAILURE -> {
                    if (state.isNetworkError) {
                        NetworkErrorView {
                            viewModel.onStart(activityId)
                        }
                    } else {
                        ServerErrorView {
                            viewModel.onStart(activityId)
                        }
                    }
                }
                is HabitState.SUCCESS -> {
                    val activity = state.activity
                    val successStories = state.successStories
                    topBarTitle = activity.name

                    HabitSuccessState(
                        viewModel = viewModel, activity = activity, successStories
                    )
                }
            }
        }
    }
}

@Composable
fun HabitSuccessState(
    viewModel: HabitConfigureViewModel,
    activity: ActivityData,
    stories: List<SuccessStory>,
) {
    val buttonText =
        if (activity.isSubscribed) stringResource(id = R.string.habit_configure_goto_activity_btn_text)
        else stringResource(id = R.string.habit_configure_start_new_journey)

    val showSubscribersCount by remember {
        mutableStateOf(false)
    }

    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(colors.background)
    ) {
        LazyColumn(
            modifier = Modifier.weight(1f),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            item {
                if (activity.getMedias().isNotEmpty()) ActivityMediaView(activity)
            }

            item {
                if (showSubscribersCount) {
                    SubscriptionCountStatus(activity)
                }
            }

            item {
                ThoughtCardView(activity.getQuotes, activity.color2)
            }

            item {
                Text(
                    text = "${activity.getBenefits.size} Benefits of ${activity.name}",
                    style = AppTheme.typography.h1TextStyle,
                    color = colors.textPrimary,
                    modifier = Modifier
                        .padding(start = 20.dp, end = 20.dp)
                        .widthIn(max = 600.dp)
                        .fillMaxWidth()
                )
            }

            itemsIndexed(activity.getBenefits) { _, benefit ->
                Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
                    BenefitView(benefit, activity.color2)
                }
            }

            item {
                Column(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    SuccessStoriesContentView(stories, viewModel, activity.id)
                }
            }
        }

        Box(
            Modifier
                .offset(y = -(12.dp))
                .height(12.dp)
                .fillMaxWidth()
        ) {
            Box(
                Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            listOf(
                                Color.Transparent, colors.background
                            )
                        )
                    )
            )
        }

        Box(modifier = Modifier.fillMaxWidth(), contentAlignment = Alignment.Center) {
            PrimaryButton(
                onClick = {
                    viewModel.handleStartJourney()
                },
                shape = RoundedCornerShape(50),
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .motionClickEvent { }
                    .fillMaxWidth(),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = colors.primary,
                    contentColor = White
                ),
                text = buttonText
            )
        }

        Spacer(modifier = Modifier.height(20.dp))
    }
}

@Preview
@Composable
fun PreviewActivityMediaView() {
    ActivityMediaView(activityData)
}

@Preview
@Composable
fun PreviewThoughtView() {
    val quotes = listOf(
        Quotes(
            1,
            11,
            "Yoga is the journey of the self, through the self, to the self.",
            "The Bhagavad Gita"
        )
    )
    ThoughtCardView(quotes, "")
}

@Preview
@Composable
fun PreviewBenefitView() {
    val benefit = Benefit(
        1,
        11,
        "https://i.ibb.co/C6tWgbB/001-strength.png",
        "Yoga improves strength, balance and flexibility."
    )
    BenefitView(benefit, "")
}

@Preview
@Composable
fun PreviewLeftAlignView() {
    val benefit = Benefit(
        1,
        11,
        "https://i.ibb.co/C6tWgbB/001-strength.png",
        "Yoga improves strength, balance and flexibility."
    )
    LeftAlignedView(benefit, "")
}

@Preview
@Composable
fun PreviewRightAlignView() {
    val benefit = Benefit(
        1,
        11,
        "https://i.ibb.co/C6tWgbB/001-strength.png",
        "Yoga improves strength, balance and flexibility."
    )
    RightAlignedView(benefit, "")
}

@Preview
@Composable
fun PreviewSuccessStoryContainView() {
    val viewModel: HabitConfigureViewModel = hiltViewModel()
    SuccessStoriesCardView(successStory, 230.dp, viewModel)
}

@OptIn(ExperimentalPagerApi::class)
@Preview
@Composable
fun PreviewExoplayerVideoView() {
    val medias = ActivityMedia(
        1,
        11,
        "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4",
        1
    )
    ExoVideoPlayer(medias, 1, rememberPagerState())
}
