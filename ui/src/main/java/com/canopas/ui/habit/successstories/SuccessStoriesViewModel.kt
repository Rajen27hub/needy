package com.canopas.ui.habit.successstories

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.SuccessStory
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SuccessStoriesViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val navManager: NavManager,
    private val authManager: AuthManager,
    private val resources: Resources
) : ViewModel() {

    val successStoryState = MutableStateFlow<SuccessStoryState>(SuccessStoryState.LOADING)
    var isEditModeOption = false

    fun isEditModeEnable(isEditable: Boolean) {
        isEditModeOption = isEditable
    }

    fun retrieveSuccessStory(activityId: String) = viewModelScope.launch {
        successStoryState.tryEmit(SuccessStoryState.LOADING)

        withContext(appDispatcher.IO) {

            if (!authManager.authState.isVerified) {
                successStoryState.tryEmit(SuccessStoryState.SUCCESS(emptyList()))
                return@withContext
            }

            if (activityId.isNotEmpty()) {
                service.retrieveSuccessStoriesByActivityId(activityId.toInt())
            } else {
                service.retrieveSuccessStoriesByUserId()
            }.onSuccess {
                successStoryState.tryEmit(SuccessStoryState.SUCCESS(it))
            }.onFailure { e ->
                Timber.e(e)
                successStoryState.tryEmit(SuccessStoryState.FAILURE(e.toUserError(resources)))
            }
        }
    }

    fun managePopBack() {
        navManager.popBack()
    }

    fun onSuccessStoryClicked(Id: Int, isEditModeOption: Boolean) {
        navManager.navigateToSuccessStoryDetail(Id, isEditModeOption)
    }
}

sealed class SuccessStoryState {
    object LOADING : SuccessStoryState()
    data class SUCCESS(
        val successStories: List<SuccessStory>
    ) : SuccessStoryState()

    data class FAILURE(val message: String) : SuccessStoryState()
}
