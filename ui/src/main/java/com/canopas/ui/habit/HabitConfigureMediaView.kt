package com.canopas.ui.habit

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.runtime.DisposableEffect
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.unit.dp
import androidx.compose.ui.viewinterop.AndroidView
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import coil.compose.AsyncImagePainter
import coil.compose.rememberAsyncImagePainter
import coil.request.ImageRequest
import com.canopas.base.ui.WelcomeBtnSkipBg
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.ActivityData
import com.canopas.data.model.ActivityMedia
import com.canopas.data.model.MEDIA_TYPE_GIF
import com.canopas.data.model.MEDIA_TYPE_IMAGE
import com.canopas.data.model.MEDIA_TYPE_VIDEO
import com.canopas.ui.customview.jetpackview.ActivityMediaImagePlaceholder
import com.canopas.ui.customview.jetpackview.TransitionPager
import com.canopas.ui.utils.Utils
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.PagerState
import com.google.accompanist.pager.rememberPagerState
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.google.android.exoplayer2.upstream.DefaultDataSource
import kotlinx.coroutines.delay

@OptIn(ExperimentalPagerApi::class)
@Composable
fun ActivityMediaView(activity: ActivityData) {
    val viewModel = hiltViewModel<HabitConfigureViewModel>()
    val pagerState = rememberPagerState()
    val medias = activity.getMedias()
    val videoAutoPlayDelay by viewModel.videoAutoplayDelayInMillis.collectAsState()
    LaunchedEffect(pagerState.currentPage) {
        if (medias.size > 1) {
            when (medias[pagerState.currentPage].type) {
                MEDIA_TYPE_VIDEO -> {
                    delay(videoAutoPlayDelay)
                }
                MEDIA_TYPE_GIF -> {
                    delay(GIF_AUTOPLAY_DELAY_IN_MILLIS)
                }
                else -> {
                    delay(IMAGE_AUTOPLAY_DELAY_IN_MILLIS)
                }
            }
            var newPage = pagerState.currentPage + 1
            if (newPage > medias.lastIndex) newPage = 0
            pagerState.animateScrollToPage(newPage)
        }
    }
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .aspectRatio(1.4f)
    ) {
        TransitionPager(
            count = medias.size,
            modifier = Modifier.fillMaxWidth(),
            pagerState = pagerState,
        ) { page ->
            Box(
                modifier = Modifier.fillMaxSize()
            ) {
                val item = medias[page]
                val context = LocalContext.current
                val imageLoader = Utils.getImageLoader(context)

                val showCustomPlaceholder = item.url.isEmpty()
                val painter = if (item.type == MEDIA_TYPE_IMAGE) rememberAsyncImagePainter(
                    ImageRequest.Builder(LocalContext.current).data(data = item.url).apply(block = {
                        ContentScale.Fit
                        if (showCustomPlaceholder) {
                            ActivityMediaImagePlaceholder(imageLoader)
                        }
                    }).build()
                ) else rememberAsyncImagePainter(
                    ImageRequest.Builder(LocalContext.current).data(data = item.url).apply(block = {
                        ContentScale.Fit
                        if (showCustomPlaceholder) {
                            ActivityMediaImagePlaceholder(imageLoader = imageLoader)
                        }
                    }).build(),
                    imageLoader = imageLoader
                )
                val painterState = painter.state
                when (item.type) {
                    MEDIA_TYPE_IMAGE -> {
                        Image(
                            modifier = Modifier
                                .fillMaxSize()
                                .background(ComposeTheme.colors.background),
                            painter = painter,
                            contentDescription = null
                        )
                        if (painterState is AsyncImagePainter.State.Loading) {
                            ActivityMediaImagePlaceholder(imageLoader = imageLoader)
                        }
                    }
                    MEDIA_TYPE_GIF -> {
                        Image(
                            modifier = Modifier.fillMaxSize(),
                            painter = painter,
                            contentDescription = null
                        )
                        if (painterState is AsyncImagePainter.State.Loading) {
                            ActivityMediaImagePlaceholder(imageLoader = imageLoader)
                        }
                    }
                    else -> {
                        ExoVideoPlayer(activity = item, page, pagerState)
                    }
                }
            }
        }

        if (medias.size > 1) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = 12.dp)
                    .align(Alignment.BottomCenter)
            ) {
                IconPagerIndicator(
                    pagerState = pagerState,
                    medias = medias,
                    activeColor = ComposeTheme.colors.primary,
                    inactiveColor = WelcomeBtnSkipBg,
                    modifier = Modifier.align(Alignment.Center),
                )
            }
        }
    }
}

@ExperimentalPagerApi
@Composable
fun ExoVideoPlayer(activity: ActivityMedia, page: Int, pagerState: PagerState) {
    val viewModel = hiltViewModel<HabitConfigureViewModel>()
    val context = LocalContext.current
    val lifecycleOwner = LocalLifecycleOwner.current

    val exoPlayer = remember {
        ExoPlayer.Builder(context).build()
    }

    LaunchedEffect(key1 = activity.url) {
        val dataSourceFactory = DefaultDataSource.Factory(
            context
        )

        val mediaItem = MediaItem.fromUri(activity.url)

        val internetVideoSource =
            ProgressiveMediaSource.Factory(dataSourceFactory).createMediaSource(mediaItem)

        exoPlayer.setMediaSource(internetVideoSource)
        exoPlayer.prepare()
    }

    AndroidView(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black),
        factory = {
            StyledPlayerView(context).apply {
                player = exoPlayer
                exoPlayer.repeatMode = Player.REPEAT_MODE_ONE
                exoPlayer.playWhenReady = page == pagerState.currentPage
                useController = false
            }
        }
    )

    if (exoPlayer.duration > 0) {
        viewModel.updateVideoAutoplayDelay(exoPlayer.duration)
    }

    LaunchedEffect(key1 = pagerState.currentPage) {
        exoPlayer.playWhenReady = page == pagerState.currentPage
    }

    DisposableEffect(key1 = lifecycleOwner) {
        val observer = LifecycleEventObserver { _, event ->
            if (event == Lifecycle.Event.ON_RESUME) {
                exoPlayer.playWhenReady = page == pagerState.currentPage
            } else if (event == Lifecycle.Event.ON_PAUSE) {
                exoPlayer.playWhenReady = false
            }
        }
        lifecycleOwner.lifecycle.addObserver(observer)
        onDispose {
            lifecycleOwner.lifecycle.removeObserver(observer)
        }
    }

    DisposableEffect(key1 = activity.url) {
        onDispose {
            exoPlayer.stop()
            exoPlayer.release()
        }
    }
}
