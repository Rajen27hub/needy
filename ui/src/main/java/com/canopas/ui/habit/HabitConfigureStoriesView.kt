package com.canopas.ui.habit

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterBoldFont
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.parse
import com.canopas.base.ui.rememberForeverLazyListState
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.ActivityData
import com.canopas.data.model.SuccessStory
import com.canopas.ui.R
import com.canopas.ui.customview.jetpackview.SuccessStoryText

@Composable
fun SuccessStoriesContentView(
    stories: List<SuccessStory>,
    viewModel: HabitConfigureViewModel,
    activityId: Int,
) {
    val textAlign = if (stories.size == 1) TextAlign.Center else TextAlign.Start
    if (stories.isNotEmpty()) {
        Text(
            text = stringResource(R.string.habit_configure_featured_success_stories_title_text),
            style = AppTheme.typography.h2TextStyle,
            color = ComposeTheme.colors.textPrimary,
            textAlign = textAlign,
            modifier = Modifier
                .padding(
                    top = 40.dp, start = 20.dp, end = 20.dp, bottom = 12.dp
                )
                .widthIn(max = 600.dp)
                .fillMaxWidth()
        )
    }

    val cardAlignment = if (stories.size > 1) Alignment.CenterStart else Alignment.Center

    BoxWithConstraints(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(bottom = 20.dp),
        contentAlignment = cardAlignment
    ) {
        val possibleHeight = maxWidth.times(0.2f).plus(220.dp)

        LazyRow(
            contentPadding = PaddingValues(start = 20.dp),
            state = rememberForeverLazyListState(key = "$activityId"),
            horizontalArrangement = Arrangement.Center,
            verticalAlignment = Alignment.CenterVertically
        ) {
            item {
                stories.forEachIndexed { index, successStory ->
                    if (index < 5) {
                        SuccessStoriesCardView(
                            successStory, possibleHeight, viewModel
                        )
                    }
                }
            }

            item {
                if (stories.count() > STORY_MAX_LIMIT) {
                    TextButton(
                        onClick = { viewModel.navigateToSuccessStory(activityId) },
                        modifier = Modifier
                            .align(Alignment.Center)
                            .padding(20.dp)
                    ) {
                        Text(
                            text = stringResource(id = R.string.habit_configure_success_stories_view_all_text),
                            style = AppTheme.typography.bodyTextStyle2,
                            textAlign = TextAlign.Center,
                            color = Gray,
                        )
                        Icon(
                            painter = painterResource(id = R.drawable.ic_arrow),
                            modifier = Modifier
                                .fillMaxWidth()
                                .align(Alignment.CenterVertically),
                            contentDescription = "arrow",
                            tint = Gray
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun SuccessStoriesCardView(
    story: SuccessStory,
    itemWidth: Dp,
    viewModel: HabitConfigureViewModel,
) {
    val user = story.user

    Column(
        modifier = Modifier
            .motionClickEvent(onClick = {
                viewModel.onSuccessStoryClicked(story.id)
            })
            .width(itemWidth)
            .aspectRatio(1.80f)
            .padding(end = 16.dp)
            .background(
                successStoryCardBg, shape = RoundedCornerShape(12.dp)
            )
    ) {
        Row(
            horizontalArrangement = Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 16.dp, top = 16.dp, bottom = 12.dp)
        ) {

            ProfileImageView(
                data = user.profileImageUrl,
                modifier = Modifier
                    .size(40.dp)
                    .border(
                        if (user.profileImageUrl.isNullOrEmpty()) 0.dp else 1.dp,
                        if (ComposeTheme.isDarkMode) darkBorderColor else lightProfileBorder,
                        CircleShape
                    ),
                char = user.profileImageChar()
            )

            Spacer(modifier = Modifier.width(8.dp))

            val userName =
                user.fullName.trim()
                    .ifEmpty { stringResource(R.string.habit_configure_success_stories_user_text) }

            Text(
                text = userName,
                textAlign = TextAlign.Start,
                style = AppTheme.typography.storyUserNameTextStyle,
                color = ComposeTheme.colors.textPrimary,
                modifier = Modifier.padding(end = 8.dp),
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        }
        SuccessStoryText(story.description)
    }
}

@Composable
fun SubscriptionCountStatus(activity: ActivityData) {

    Row(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .aspectRatio(4.57f)
            .background(
                if (ComposeTheme.isDarkMode) darkHabitCardBg else Color
                    .parse(activity.color2)
                    .copy(0.5f)
            ),
        horizontalArrangement = Arrangement.Center,
        verticalAlignment = Alignment.CenterVertically
    ) {
        Image(
            painter = painterResource(id = R.drawable.ic_habit_group_image),
            contentDescription = null,
            modifier = Modifier
                .weight(0.3f)
                .padding(start = 20.dp)
        )

        Text(
            text = buildAnnotatedString {
                withStyle(
                    style = SpanStyle(
                        fontFamily = InterBoldFont,
                    )
                ) {
                    append("755+ ")
                }
                append("people are currently doing ${activity.name}")
            },
            style = AppTheme.typography.bodyTextStyle1,
            lineHeight = 22.sp,
            color = if (ComposeTheme.isDarkMode) ComposeTheme.colors.textSecondary else ComposeTheme.colors.textPrimary,
            modifier = Modifier
                .weight(0.7f)
                .padding(start = 8.dp, end = 20.dp)
        )
    }
}

private val darkHabitCardBg = Color(0xff222222)
private val darkBorderColor = Color(0x80605A85)
private val successStoryCardBg = Color(0xFFE8F5F9)
private val lightProfileBorder = Color(0x4DE8F5F9)
private val Gray = Color(0xFF888888)
