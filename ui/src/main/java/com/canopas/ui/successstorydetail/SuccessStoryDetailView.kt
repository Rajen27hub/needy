package com.canopas.ui.successstorydetail

import android.content.res.Configuration
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.MoreHoriz
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ColorPrimary
import com.canopas.base.ui.DividedLineView
import com.canopas.base.ui.ProfileImageView
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.textColor
import com.canopas.data.model.ActivityData
import com.canopas.data.model.SuccessStory
import com.canopas.data.utils.TestUtils.Companion.activityData
import com.canopas.data.utils.TestUtils.Companion.successStory
import com.canopas.ui.R

@Composable
fun SuccessStoryDetailView(storyId: Int, isEditModeEnable: Boolean) {
    val viewModel = hiltViewModel<SuccessStoryDetailViewModel>()

    val state by viewModel.storyDetailState.collectAsState()
    val context = LocalContext.current

    LaunchedEffect(key1 = storyId) {
        viewModel.fetchSuccessStoryDetail(storyId)
    }

    state.let { storyDetailState ->
        when (storyDetailState) {
            StoryDetailState.DELETE -> {
                LaunchedEffect(key1 = storyDetailState) {
                    viewModel.managePopBackStack()
                }
            }
            is StoryDetailState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier
                        .fillMaxSize()
                ) {
                    CircularProgressIndicator(color = ColorPrimary)
                }
            }

            is StoryDetailState.FAILURE -> {
                val message = storyDetailState.message
                LaunchedEffect(key1 = message) {
                    showBanner(context, message)
                }
            }
            is StoryDetailState.SUCCESS -> {
                val successStoryDetail = storyDetailState.successStory
                successStoryDetail.activities.forEach {
                    StoryDetailContent(viewModel, successStoryDetail, isEditModeEnable, it)
                }
            }
        }
    }
}

@Composable
fun StoryDetailContent(
    viewModel: SuccessStoryDetailViewModel,
    story: SuccessStory,
    isEditModeEnable: Boolean,
    activityData: ActivityData
) {
    val configuration = LocalConfiguration.current

    val openDeleteDialog = viewModel.openDeleteDialog.collectAsState()
    if (openDeleteDialog.value) {
        ShowDeleteDialog(viewModel, story.id)
    }
    val userId by viewModel.userId.collectAsState()

    Scaffold(
        topBar = {
            TopAppBar(
                content = {
                    TopAppBarContent(
                        title = stringResource(id = R.string.detail_story_title),
                        navigationIconOnClick = { viewModel.managePopBackStack() },
                        actions = {
                            if (userId == story.user_id && isEditModeEnable) {
                                DropDownOptionMenu(viewModel, activityData, story)
                            } else if (userId != story.user_id) {
                                DropDownOptionReportMenu(viewModel, story.id.toString())
                            }
                        }
                    )
                },
                backgroundColor = White,
                contentColor = Color.Black,
                elevation = 0.dp
            )
        }
    ) {
        val padding = it
        SuccessStoryDetail(story, configuration, padding)
    }
}

@Composable
fun SuccessStoryDetail(
    successStory: SuccessStory,
    configuration: Configuration,
    paddingValues: PaddingValues
) {
    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(paddingValues),
        verticalArrangement = Arrangement.Top,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val imageHeight = (configuration.screenHeightDp * 0.18).dp
        val user = successStory.user

        ProfileImageView(
            data = user.profileImageUrl,
            modifier = Modifier
                .padding(top = 24.dp)
                .size(imageHeight),
            char = user.profileImageChar()
        )

        val userName = user.fullName.trim()
            .ifEmpty { stringResource(R.string.habit_configure_success_stories_user_text) }

        Text(
            text = userName,
            modifier = Modifier.padding(top = 12.dp),
            style = AppTheme.typography.h2TextStyle,
            color = textColor.copy(0.87f),
        )

        Box(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .padding(vertical = 12.dp, horizontal = 20.dp)
                .fillMaxWidth()
                .background(
                    textColor.copy(0.04f),
                    shape = RoundedCornerShape(topEnd = 25.dp, bottomStart = 25.dp)
                )
                .clip(RoundedCornerShape(topEnd = 25.dp, bottomStart = 25.dp)),
            contentAlignment = Alignment.Center
        ) {
            Text(
                text = successStory.description.trim(),
                style = AppTheme.typography.bodyTextStyle1,
                textAlign = TextAlign.Justify,
                lineHeight = 22.sp,
                color = textColor.copy(0.87f),
                modifier = Modifier
                    .verticalScroll(scrollState)
                    .padding(horizontal = 16.dp, vertical = 20.dp)
                    .fillMaxWidth()
            )
        }
    }
}

@Composable
fun DropDownOptionMenu(
    viewModel: SuccessStoryDetailViewModel,
    activityData: ActivityData,
    story: SuccessStory
) {
    var showMenu by remember { mutableStateOf(false) }

    IconButton(onClick = {
        showMenu = !showMenu
    }) {
        Icon(
            imageVector = Icons.Outlined.MoreHoriz,
            contentDescription = "More Menu",
        )
    }
    DropdownMenu(
        expanded = showMenu,
        onDismissRequest = { showMenu = false }
    ) {
        DropdownMenuItem(onClick = {
            viewModel.onEditButtonClick(activityData, story.id)
            showMenu = false
        }) {
            Text(stringResource(R.string.success_story_edit_option_text))
        }
        DividedLineView()
        DropdownMenuItem(onClick = {
            viewModel.openStoryDeleteDialog()
            showMenu = false
        }) {
            Text(stringResource(R.string.success_story_delete_option_text))
        }
    }
}

@Composable
fun DropDownOptionReportMenu(
    viewModel: SuccessStoryDetailViewModel,
    storyId: String
) {
    var showMenu by remember { mutableStateOf(false) }

    IconButton(onClick = {
        showMenu = !showMenu
    }) {
        Icon(
            imageVector = Icons.Outlined.MoreHoriz,
            contentDescription = "More Menu",
        )
    }
    DropdownMenu(
        expanded = showMenu,
        onDismissRequest = { showMenu = false }
    ) {
        DropdownMenuItem(onClick = {
            viewModel.openSuccessStoryFeedbackScreen(storyId)
            showMenu = false
        }) {
            Text(stringResource(R.string.success_story_report_option_text))
        }
    }
}

@Composable
fun ShowDeleteDialog(viewModel: SuccessStoryDetailViewModel, storyId: Int) {
    CustomAlertDialog(
        title = stringResource(R.string.success_story_delete_dialog_title),
        subTitle = stringResource(R.string.success_story_delete_dialog_message),
        confirmBtnText = stringResource(id = R.string.success_story_delete_dialog_delete_button_text),
        dismissBtnText = stringResource(id = R.string.success_story_delete_dialog_cancel_button_text),
        onConfirmClick = { viewModel.deleteSuccessStory(storyId) },
        onDismissClick = { viewModel.closeStoryDeleteDialog() },
        confirmBtnColor = ProfileScreenDeleteBtn
    )
}

@Preview
@Composable
fun PreviewSuccessStoryDetailView() {
    SuccessStoryDetailView(1, false)
}

@Preview
@Composable
fun PreviewSuccessStoryContent() {
    val viewModel: SuccessStoryDetailViewModel = hiltViewModel()
    StoryDetailContent(viewModel, successStory, false, activityData)
}

@Preview
@Composable
fun PreviewSuccessStoryDetail() {
    val configuration = LocalConfiguration.current
    SuccessStoryDetail(successStory, configuration, PaddingValues())
}

private val ProfileScreenDeleteBtn = Color(0xFFE13333)
