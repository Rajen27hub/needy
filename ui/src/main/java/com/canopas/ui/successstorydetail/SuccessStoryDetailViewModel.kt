package com.canopas.ui.successstorydetail

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.ActivityData
import com.canopas.data.model.SuccessStory
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class SuccessStoryDetailViewModel @Inject constructor(
    private val navManager: NavManager,
    private val service: NoLonelyService,
    private val appDispatcher: AppDispatcher,
    authManager: AuthManager,
    private val resources: Resources
) : ViewModel() {

    val storyDetailState = MutableStateFlow<StoryDetailState>(StoryDetailState.LOADING)
    val openDeleteDialog = MutableStateFlow(false)
    var userId = MutableStateFlow(authManager.currentUser?.id ?: "")

    fun managePopBackStack() {
        navManager.popBack()
    }

    fun fetchSuccessStoryDetail(storyId: Int) = viewModelScope.launch {
        storyDetailState.tryEmit(StoryDetailState.LOADING)
        withContext(appDispatcher.IO) {
            service.retrieveSuccessStoryDetailById(storyId).onSuccess { storyDetail ->
                storyDetailState.tryEmit(StoryDetailState.SUCCESS(storyDetail))
            }.onFailure {
                Timber.e(it)
                storyDetailState.tryEmit(StoryDetailState.FAILURE(it.toUserError(resources)))
            }
        }
    }

    fun deleteSuccessStory(storyId: Int) = viewModelScope.launch {
        storyDetailState.tryEmit(StoryDetailState.LOADING)
        withContext(appDispatcher.IO) {
            service.deleteSuccessStory(storyId).onSuccess {
                storyDetailState.value = StoryDetailState.DELETE
            }.onFailure { e ->
                Timber.e(e)
                storyDetailState.value = StoryDetailState.FAILURE(e.toUserError(resources))
            }
        }
    }

    fun openStoryDeleteDialog() {
        openDeleteDialog.tryEmit(true)
    }

    fun closeStoryDeleteDialog() {
        openDeleteDialog.tryEmit(false)
    }

    fun onEditButtonClick(activityData: ActivityData, storyId: Int) {
        navManager.navigateToUpdateSuccessStory(
            activityData.id,
            activityData.name,
            storyId
        )
    }

    fun openSuccessStoryFeedbackScreen(storyId: String) {
        navManager.navigateToFeedback(storyId)
    }
}

sealed class StoryDetailState {
    object LOADING : StoryDetailState()
    data class SUCCESS(val successStory: SuccessStory) : StoryDetailState()
    data class FAILURE(val message: String) : StoryDetailState()
    object DELETE : StoryDetailState()
}
