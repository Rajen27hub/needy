package com.canopas.ui.di

import android.app.Application
import android.content.Context
import android.content.res.Resources
import androidx.core.app.NotificationManagerCompat
import com.canopas.ui.R
import com.canopas.ui.home.explore.ClockUtils
import com.canopas.ui.home.explore.CurrentDate
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.identity.Identity
import com.google.android.gms.auth.api.identity.SignInClient
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.crashlytics.FirebaseCrashlytics
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import org.greenrobot.eventbus.EventBus
import java.util.Calendar
import javax.inject.Named
import javax.inject.Singleton

const val SIGN_IN_REQUEST = "signInRequest"
const val SIGN_IN_CLIENT = "signInClient"

@Module
@InstallIn(SingletonComponent::class)
object UiModule {
    @Provides
    @Singleton
    fun provideEventbus(): EventBus {
        return EventBus.getDefault()
    }

    @Provides
    fun provideResources(@ApplicationContext context: Context): Resources {
        return context.resources
    }

    @Provides
    @Singleton
    fun provideCurrentDate(date: CurrentDate): ClockUtils {
        return date
    }

    @Provides
    @Singleton
    fun provideCalender(): Calendar {
        return Calendar.getInstance()
    }

    @Provides
    @Singleton
    fun provideNotificationManagerCompat(@ApplicationContext context: Context): NotificationManagerCompat {
        return NotificationManagerCompat.from(context)
    }

    @Provides
    fun provideFirebaseCrashlytics(): FirebaseCrashlytics {
        return FirebaseCrashlytics.getInstance()
    }

    @Provides
    @Singleton
    fun provideSignInClient(app: Application): SignInClient = Identity.getSignInClient(app)

    @Provides
    @Named(SIGN_IN_REQUEST)
    @Singleton
    fun provideSignInRequest(
        app: Application
    ): BeginSignInRequest = BeginSignInRequest.builder()
        .setGoogleIdTokenRequestOptions(
            BeginSignInRequest.GoogleIdTokenRequestOptions.builder()
                .setSupported(true)
                .setServerClientId(app.getString(R.string.default_web_client_id))
                .setFilterByAuthorizedAccounts(false)
                .build()
        )
        .build()

    @Provides
    @Named(SIGN_IN_CLIENT)
    @Singleton
    fun provideGoogleSignInClient(
        app: Application
    ): GoogleSignInClient {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(app.getString(R.string.default_web_client_id))
            .requestEmail()
            .requestProfile()
            .build()
        return GoogleSignIn.getClient(app, gso)
    }
}
