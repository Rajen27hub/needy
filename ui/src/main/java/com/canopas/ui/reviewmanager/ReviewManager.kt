package com.canopas.ui.reviewmanager

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import com.canopas.ui.utils.loadUriData
import com.google.android.play.core.review.ReviewManagerFactory
import timber.log.Timber

fun redirectToPlayStore(context: Context) {
    try {
        val appPackageName = context.packageName
        val playStoreUri =
            "market://details?id=$appPackageName"
        loadUriData(context, playStoreUri)
    } catch (e: ActivityNotFoundException) {
        val appPackageName = context.packageName
        val playStoreUri =
            "https://play.google.com/store/apps/details?id=$appPackageName"
        loadUriData(context, playStoreUri)
    }
}

fun launchInAppReview(context: Context) {
    val reviewManager = ReviewManagerFactory.create(context as Activity)
    val requestReviewFlow = reviewManager.requestReviewFlow()
    requestReviewFlow.addOnCompleteListener { request ->
        if (request.isSuccessful) {
            val reviewInfo = request.result
            reviewManager.launchReviewFlow(context, reviewInfo)
        } else {
            Timber.e(request.exception?.localizedMessage)
        }
    }
}
