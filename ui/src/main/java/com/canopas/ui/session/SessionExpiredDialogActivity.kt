package com.canopas.ui.session

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.AlertDialog
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.DialogProperties
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.SecondaryOutlineButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.JustlyAppTheme
import com.canopas.ui.R
import com.canopas.ui.main.MainActivity
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SessionExpiredDialogActivity : AppCompatActivity() {

    @Inject
    lateinit var preferences: NoLonelyPreferences

    private val scope = CoroutineScope(Dispatchers.Main)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        scope.launch {
            val isDarkModeEnabled = preferences.getIsDarkModeEnabled()
            setContent {
                JustlyAppTheme(isDarkMode = isDarkModeEnabled) {
                    SessionExpireAlert()
                }
            }
        }
    }

    @Composable
    fun SessionExpireAlert() {
        val context = LocalContext.current
        AlertDialog(
            onDismissRequest = {},
            backgroundColor = colors.background,
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = false
            ),
            shape = RoundedCornerShape(14.dp),
            text = {
                Text(
                    text = stringResource(id = R.string.session_expire_prompt_txt_message),
                    style = AppTheme.typography.h3TextStyle,
                    color = colors.textPrimary
                )
            },
            confirmButton = {
                SecondaryOutlineButton(
                    modifier = Modifier.motionClickEvent { },
                    onClick = {
                        Intent(context, MainActivity::class.java).apply {
                            flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                            startActivity(this)
                        }
                    },
                    border = BorderStroke(0.dp, Color.Transparent),
                    text = stringResource(id = R.string.session_expire_prompt_btn_ok),
                    colors = ButtonDefaults.outlinedButtonColors(backgroundColor = Color.Transparent),
                    color = colors.primary
                )
            }
        )
    }
}
