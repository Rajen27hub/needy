package com.canopas.ui.navigation

import androidx.annotation.DrawableRes
import com.canopas.ui.R

const val ARGUMENT_ACTIVITY_NAME = "activityName"
const val ARGUMENT_ACTIVITY_TIME = "activityTime"
const val ARGUMENT_ACTIVITY_DURATION = "activityDuration"
const val ARGUMENT_GOALS_TITLE = "goalsTitle"
const val ARGUMENT_GOAL_ID = "goalId"
const val ARGUMENT_ACTIVITY_TITLE = "title"
const val ARGUMENT_SUBSCRIPTION_ID = "subscriptionId"
const val ARGUMENT_ACTIVITY_ID = "activityId"
const val ARGUMENT_COMMUNITY_ID = "communityId"
const val ARGUMENT_SHOW_SKIP_BUTTON = "showSkipButton"
const val ARGUMENT_USER_ID = "userId"
const val ARGUMENT_CATEGORY = "categoryId"
const val ARGUMENT_USERNAME = "userName"
const val ARGUMENT_PHONE_NO = "phoneNo"
const val ARGUMENT_VERIFICATION_ID = "verificationId"
const val ARGUMENT_STORY_DETAIL_ID = "storyDetailId"
const val ARGUMENT_URL = "url"
const val ARGUMENT_STORY_EDIT_MODE_ENABLE = "isEditModeEnable"
const val ARGUMENT_COMMUNITY_NAME = "communityName"
const val ARGUMENT_NOTES_ID = "notesId"
const val ARGUMENT_IS_CUSTOM = "isCustom"
const val ARGUMENT_SELECTED_TAB = "selectedTab"
const val ARGUMENT_STORY_ID = "storyId"
const val ARGUMENT_VIDEO_URL = "videoUrl"
const val ARGUMENT_TEMPLATE_ID = "templateId"

sealed class MainScreen(val route: String, @DrawableRes val resourceId: Int) {
    object Root : MainScreen("Main", R.drawable.ic_tab_home)
    object Explore : MainScreen("Explore", R.drawable.ic_tab_home)
    object Current : MainScreen("Current", R.drawable.ic_tab_current_list)
    object Community : MainScreen("Community", R.drawable.ic_tab_community)
}

sealed class Screen(val route: String) {
    object HabitConfigure : Screen("HabitConfigure")
    object ConfigureActivity : Screen("ConfigureActivity")
    object EditSubscription : Screen("EditSubscription")
    object WhyThisActivity : Screen("WhyThisActivity")
    object MyGoalsScreen : Screen("MyGoalsScreen")
    object Status : Screen("Status")
    object Congratulation : Screen("Congratulation")
    object PostStory : Screen("PostSuccessStory")
    object SuccessStory : Screen("SuccessStory")
    object SuccessStoryDetail : Screen("SuccessStoryDetail")
    object WebView : Screen("WebView")
    object CustomActivityView : Screen("CustomActivityView")
    object ActivityList : Screen("ActivityList")
    object EditScheduleView : Screen("EditScheduleView")
    object AddNotesView : Screen("AddNotesView")
    object NotesListView : Screen("NotesListView")
    object NoteTemplatesView : Screen("NoteTemplatesView")
    object ManageTemplatesView : Screen("ManageTemplatesView")
    object AddOrEditTemplateView : Screen("AddOrEditTemplateView")
    object ArchivedActivitiesView : Screen("ArchivedActivitiesView")
    object Setting : Screen("Settings")
    object AddActivityView : Screen("AddActivityView")
    object StoryView : Screen("StoryView")
    object VideoView : Screen("VideoView")
    object AddOrUpdateGoalView : Screen("AddOrUpdateGoalView")
    object AddGoalSelectionView : Screen("AddGoalSelectionView")
}

sealed class LoginScreen(val route: String) {
    object Root : LoginScreen("Login")
    object SignInMethod : LoginScreen("SignInMethod")
    object PhoneLogin : LoginScreen("PhoneLogin")
    object LoginOtp : LoginScreen("LoginOtp")
    object NotificationPermissionView : Screen("NotificationPermissionView")
}

sealed class SettingsScreen(val route: String) {
    object UserProfile : SettingsScreen("UserProfile")
    object Feedback : SettingsScreen("Feedback")
    object Privacy : SettingsScreen("Privacy")
    object BuyPremium : SettingsScreen("BuyPremium")
    object Notifications : SettingsScreen("Notifications")
}
