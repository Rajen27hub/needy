package com.canopas.ui.navigation

import androidx.navigation.NavController
import androidx.navigation.NavOptions
import com.canopas.feature_community_ui.CommunityScreen
import com.canopas.feature_points_ui.PointScreen
import javax.inject.Inject
import javax.inject.Singleton

const val INITIAL_POSITION = 0

@Singleton
class NavManager @Inject constructor() {

    private var navController: NavController? = null

    fun setNavController(navController: NavController) {
        this.navController = navController
    }

    fun removeNavController() {
        navController = null
    }

    fun navigateToHabitConfigure(habitId: Int) {
        navController?.navigate(Screen.HabitConfigure.route + "/$habitId")
    }

    fun navigateToActivityList(categoryId: Int) {
        navController?.navigate(Screen.ActivityList.route + "/$categoryId")
    }

    fun navigateToSuccessStoryDetail(id: Int, isEditModeEnable: Boolean) {
        navController?.navigate(Screen.SuccessStoryDetail.route + "/$id" + "?isEditModeEnable=$isEditModeEnable")
    }

    fun navigateToActivityStatusByActivityId(activityId: Int, headerText: String = "") {
        navController?.navigate(Screen.Status.route + "?activityId=$activityId" + "?headerText=$headerText")
    }

    fun popHabitDetailScreen() {
        navController?.popBackStack(MainScreen.Current.route, false)
    }

    fun popExploreScreen() {
        navController?.popBackStack(MainScreen.Explore.route, false)
    }

    fun popConfigureActivityScreen(storyId: Int) {
        navController?.navigate(
            route = Screen.StoryView.route + "/$storyId",
            navOptions = NavOptions.Builder()
                .setPopUpTo(MainScreen.Explore.route, false)
                .build()
        )
    }

    fun navigateToConfigureActivity(
        activityId: Int,
        activityName: String,
        activityTime: String,
        activityDuration: Int,
        storyId: Int = 0,
        goalsTitle: String = ""
    ) {
        navController?.navigate(
            Screen.ConfigureActivity.route +
                "/$activityId" + "/$activityName" + "/$storyId" +
                "?activityTime=$activityTime" + "?activityDuration=$activityDuration" + "?goalsTitle=$goalsTitle"
        )
    }

    fun navigateToConfigureCustomActivity(title: String, storyId: Int = 0, goalsTitle: String = "") {
        navController?.navigate(Screen.ConfigureActivity.route + "/$storyId" + "?title=$title" + "?goalsTitle=$goalsTitle")
    }

    fun navigateToEditSubscription(subscriptionId: String) {
        navController?.navigate(Screen.EditSubscription.route + "/$subscriptionId")
    }

    fun navigateToActivityStatusBySubscriptionId(
        subscriptionId: Int,
        headerText: String = "",
        showArchivedStatus: Boolean = false
    ) {
        navController?.navigate(Screen.Status.route + "?subscriptionId=$subscriptionId" + "?headerText=$headerText" + "?showArchivedStatus=$showArchivedStatus")
    }

    fun navigateToPostSuccessStory(
        activityId: Int,
        activityName: String
    ) {
        navController?.navigate(Screen.PostStory.route + "?activityName=$activityName" + "?activityId=$activityId")
    }

    fun navigateToFeedback(storyId: String = "", selectedTab: Int = 0) {
        navController?.navigate(SettingsScreen.Feedback.route + "?storyDetailId=$storyId" + "?selectedTab=$selectedTab")
    }

    fun navigateToPrivacyScreen() {
        navController?.navigate(SettingsScreen.Privacy.route)
    }
    fun navigateToNotificationsScreen() {
        navController?.navigate(SettingsScreen.Notifications.route)
    }

    fun navigateToBuyPremiumScreen(
        showBackArrow: Boolean = true,
        showDismissButton: Boolean = false,
        textType: Int = 0,
    ) {
        navController?.navigate(SettingsScreen.BuyPremium.route + "?showBackArrow=$showBackArrow" + "?showDismissButton=$showDismissButton" + "?textType=$textType")
    }

    fun navigateToProfileScreen(
        showBackArrow: Boolean = true,
        promptPremium: Boolean = false
    ) {
        navController?.navigate(SettingsScreen.UserProfile.route + "?showBackArrow=$showBackArrow" + "?promptPremium=$promptPremium")
    }

    fun navigateToSignInMethods() {
        navController?.navigate(LoginScreen.Root.route)
    }

    fun popSignInScreens() {
        navController?.popBackStack(LoginScreen.Root.route, inclusive = true)
    }

    fun navigateToPhoneLogin() {
        navController?.navigate(LoginScreen.PhoneLogin.route)
    }

    fun navigateToNotificationPermissionView() {
        navController?.navigate(LoginScreen.NotificationPermissionView.route)
    }

    fun popBack() {
        navController?.popBackStack()
    }

    fun navigateToSuccessStory(activityId: Int) {
        navController?.navigate(Screen.SuccessStory.route + "?activityId=$activityId")
    }

    fun navigateToWebView(url: String) {
        navController?.navigate(Screen.WebView.route + "?url=$url")
    }

    fun navigateToUserSuccessStories(isEditModeEnable: Boolean) {
        navController?.navigate(Screen.SuccessStory.route + "?isEditModeEnable=$isEditModeEnable")
    }

    fun navigateToUpdateSuccessStory(
        activityId: Int,
        activityName: String,
        storyId: Int
    ) {
        navController?.navigate(Screen.PostStory.route + "?activityName=$activityName" + "?activityId=$activityId" + "?storyDetailId=$storyId" + "?isEditModeEnable=${true}")
    }

    fun navigateBackExplore() {
        navController?.popBackStack(MainScreen.Explore.route, inclusive = false)
    }

    fun navigateBackCurrent() {
        if (navController?.currentBackStackEntry?.destination?.route == MainScreen.Current.route) {
            navController?.navigate(MainScreen.Current.route) {
                popUpTo(INITIAL_POSITION)
            }
        } else {
            navController?.popBackStack(MainScreen.Current.route, inclusive = false)
        }
    }

    fun navigateBackCommunity() {
        if (navController?.currentBackStackEntry?.destination?.route == MainScreen.Community.route) {
            navController?.navigate(MainScreen.Community.route) {
                popUpTo(INITIAL_POSITION)
            }
        } else {
            navController?.popBackStack(MainScreen.Community.route, inclusive = false)
        }
    }

    fun navigateToSettingsScreen() {
        navController?.navigate(Screen.Setting.route)
    }

    fun navigateToCustomActivityView(storyId: Int = 0) {
        navController?.navigate(Screen.CustomActivityView.route + "/$storyId")
    }

    fun navigateToAddOrUpdateNotes(
        activityName: String,
        activityId: String,
        notesId: String,
        isCustom: Boolean
    ) {
        navController?.navigate(
            Screen.AddNotesView.route + "?activityName=$activityName" +
                "?activityId=$activityId" + "?notesId=$notesId" + "?isCustom=$isCustom"
        )
    }

    fun navigateToOtpView(phoneNo: String, storedVerificationId: String) {
        navController?.navigate(LoginScreen.LoginOtp.route + "/$phoneNo" + "/$storedVerificationId")
    }

    fun navigateToNoteListView(activityId: Int, isCustom: Boolean, activityName: String) {
        navController?.navigate(Screen.NotesListView.route + "/$activityId" + "?isCustom=$isCustom" + "?activityName=$activityName")
    }

    fun navigateToNoteTemplatesView() {
        navController?.navigate(Screen.NoteTemplatesView.route)
    }

    fun navigateToManageTemplatesView() {
        navController?.navigate(Screen.ManageTemplatesView.route)
    }

    fun managePopBackWithNoteTemplateId(
        templateId: Int
    ) {
        navController?.previousBackStackEntry?.savedStateHandle?.set(ARGUMENT_TEMPLATE_ID, templateId)
        navController?.popBackStack()
    }

    fun navigateToAddNoteTemplate() {
        navController?.navigate(Screen.AddOrEditTemplateView.route)
    }

    fun navigateToEditNoteTemplate(templateId: Int) {
        navController?.navigate(Screen.AddOrEditTemplateView.route + "/$templateId")
    }

    fun navigateToLeaderboardScreen() {
        navController?.navigate(PointScreen.LeaderboardView.route)
    }

    fun navigateToAddCommunityScreen() {
        navController?.navigate(CommunityScreen.AddCommunity.route)
    }
    fun navigateToCommunityDetailScreen(communityId: String) {
        navController?.navigate(CommunityScreen.CommunityDetail.route + "?communityId=$communityId")
    }
    fun navigateToArchivedActivitiesView() {
        navController?.navigate(Screen.ArchivedActivitiesView.route)
    }

    fun navigateToStoryView(storyId: Int) {
        navController?.navigate(Screen.StoryView.route + "/$storyId")
    }
    fun navigateToAddActivityView() {
        navController?.navigate(Screen.AddActivityView.route)
    }

    fun navigateToVideo(videoUrl: String) {
        navController?.navigate(Screen.VideoView.route + "?videoUrl=$videoUrl")
    }

    fun navigateToWhyThisActivityScreen(
        activityId: Int,
        activityName: String,
        activityTime: String,
        activityDuration: Int,
        storyId: Int = 0
    ) {
        navController?.navigate(
            Screen.WhyThisActivity.route +
                "/$activityId" + "/$activityName" + "/$storyId" + "?activityTime=$activityTime" + "?activityDuration=$activityDuration"
        )
    }

    fun navigateToWhyThisActivityScreen(title: String, storyId: Int = 0) {
        navController?.navigate(Screen.WhyThisActivity.route + "/$storyId" + "?title=$title")
    }

    fun navigateToMyGoalsScreen(subscriptionId: Int = 0) {
        navController?.navigate(Screen.MyGoalsScreen.route + "/$subscriptionId")
    }

    fun navigateToAddGoal(subscriptionId: Int) {
        navController?.navigate(Screen.AddOrUpdateGoalView.route + "/$subscriptionId")
    }

    fun navigateToEditGoal(subscriptionId: Int, goalId: Int) {
        navController?.navigate(Screen.AddOrUpdateGoalView.route + "/$subscriptionId" + "/$goalId")
    }

    fun navigateToSelectActivity() {
        navController?.navigate(Screen.AddGoalSelectionView.route)
    }
}
