package com.canopas.ui.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.canopas.feature_community_ui.CommunityScreen
import com.canopas.feature_community_ui.activitystatus.CommunitySubscriptionStatusView
import com.canopas.feature_community_ui.addcommunity.AddCommunityView
import com.canopas.feature_community_ui.addcommunitymembers.AddCommunityMembersView
import com.canopas.feature_community_ui.community.CommunityView
import com.canopas.feature_community_ui.communitydetail.CommunityDetailView
import com.canopas.feature_community_ui.communityinfo.CommunityInfoView
import com.canopas.feature_community_ui.communityrequest.CommunityRequestsView
import com.canopas.feature_community_ui.memberinfoview.CommunityMemberInfoView
import com.canopas.feature_community_ui.pendinginvitation.PendingInvitationsView
import com.canopas.feature_community_ui.sharesubscriptions.ShareSubscriptionsView
import com.canopas.feature_community_ui.subscriptionlist.CommunitySubscriptionListView
import com.canopas.feature_points_ui.PointScreen
import com.canopas.feature_points_ui.leaderboard.LeaderboardView
import com.canopas.ui.signinmethods.SignInMethodsView
import com.canopas.ui.utils.slideComposable
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.navigation
import com.google.accompanist.navigation.material.ExperimentalMaterialNavigationApi
import com.google.accompanist.navigation.material.bottomSheet

@OptIn(ExperimentalMaterialNavigationApi::class)
@ExperimentalAnimationApi
fun NavGraphBuilder.communityGraph(
    navController: NavHostController
) {
    navigation(
        startDestination = CommunityScreen.Community.route,
        route = CommunityScreen.Root.route
    ) {
        composable(CommunityScreen.Community.route) {
            CommunityView()
        }

        slideComposable(CommunityScreen.AddCommunity.route) {
            AddCommunityView()
        }

        slideComposable(CommunityScreen.SignInMethod.route) {
            SignInMethodsView()
        }

        slideComposable(
            CommunityScreen.AddMembers.route + "/{$ARGUMENT_COMMUNITY_ID}" + "?showSkipButton={$ARGUMENT_SHOW_SKIP_BUTTON}",
            arguments = listOf(
                navArgument(ARGUMENT_SHOW_SKIP_BUTTON) {
                    type = NavType.BoolType
                }
            )
        ) { navBackStack ->
            val communityId = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_ID)
                ?: ""
            val showSkipButton =
                navBackStack.arguments?.getBoolean(ARGUMENT_SHOW_SKIP_BUTTON) ?: false
            AddCommunityMembersView(communityId, showSkipButton)
        }

        slideComposable(CommunityScreen.ShareSubscriptions.route + "/{$ARGUMENT_COMMUNITY_ID}" + "?userId={$ARGUMENT_USER_ID}") { navBackStack ->
            val communityId = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_ID)
                ?: ""
            val userId = navBackStack.arguments?.getString(ARGUMENT_USER_ID)
                ?: ""
            ShareSubscriptionsView(communityId, userId)
        }

        slideComposable(CommunityScreen.PendingInvitation.route + "/{$ARGUMENT_COMMUNITY_ID}") { navBackStack ->
            val communityId = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_ID) ?: ""
            PendingInvitationsView(communityID = communityId)
        }

        slideComposable(CommunityScreen.CommunitySubscriptionList.route + "/{$ARGUMENT_COMMUNITY_ID}" + "/{$ARGUMENT_USERNAME}" + "/{$ARGUMENT_USER_ID}") { navBackStack ->
            val communityId = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_ID) ?: ""
            val userName = navBackStack.arguments?.getString(ARGUMENT_USERNAME) ?: ""
            val userId = navBackStack.arguments?.getString(ARGUMENT_USER_ID) ?: ""
            CommunitySubscriptionListView(communityId, userId, userName)
        }

        slideComposable(CommunityScreen.CommunitySubscriptionStatus.route + "/{$ARGUMENT_COMMUNITY_ID}" + "/{$ARGUMENT_USER_ID}" + "/{$ARGUMENT_SUBSCRIPTION_ID}" + "/{$ARGUMENT_USERNAME}") { navBackStack ->
            val communityId = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_ID) ?: ""
            val userId = navBackStack.arguments?.getString(ARGUMENT_USER_ID) ?: ""
            val subscriptionId = navBackStack.arguments?.getString(ARGUMENT_SUBSCRIPTION_ID) ?: ""
            val userName = navBackStack.arguments?.getString(ARGUMENT_USERNAME) ?: ""

            CommunitySubscriptionStatusView(
                communityId = communityId,
                userId = userId,
                subscriptionId = subscriptionId,
                userName = userName
            )
        }

        slideComposable(
            CommunityScreen.CommunityDetail.route + "?communityId={communityId}",
            arguments = listOf(
                navArgument(ARGUMENT_COMMUNITY_ID) {
                    type = NavType.StringType
                },
            )
        ) { navBackStack ->
            val communityId = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_ID) ?: ""
            val isDataUpdated =
                navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Boolean>("isDataUpdated")?.value
                    ?: false
            CommunityDetailView(communityId, isDataUpdated)
            navController.currentBackStackEntry?.savedStateHandle?.clearSavedStateProvider("isDataUpdated")
        }

        bottomSheet(CommunityScreen.CommunityMemberInfo.route + "/{$ARGUMENT_COMMUNITY_ID}" + "/{$ARGUMENT_USER_ID}") { navBackStack ->
            val communityId = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_ID) ?: ""
            val userId = navBackStack.arguments?.getString(ARGUMENT_USER_ID) ?: ""
            CommunityMemberInfoView(communityId = communityId, userId = userId)
        }

        bottomSheet(CommunityScreen.CommunityInfo.route + "/{$ARGUMENT_COMMUNITY_ID}" + "/{$ARGUMENT_USER_ID}") { navBackStack ->
            val communityId = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_ID) ?: ""
            val userId = navBackStack.arguments?.getString(ARGUMENT_USER_ID) ?: ""
            CommunityInfoView(communityId, userId)
        }

        slideComposable(
            CommunityScreen.AddCommunity.route + "?communityId={communityId}" + "?communityName={communityName}",
            arguments = listOf(
                navArgument(ARGUMENT_COMMUNITY_ID) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_COMMUNITY_NAME) {
                    type = NavType.StringType
                }
            )
        ) { navBackStack ->
            val communityId = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_ID) ?: ""
            val communityName = navBackStack.arguments?.getString(ARGUMENT_COMMUNITY_NAME) ?: ""
            AddCommunityView(communityId, communityName)
        }
        slideComposable(CommunityScreen.CommunityRequest.route) {
            CommunityRequestsView()
        }
    }
}

@ExperimentalAnimationApi
fun NavGraphBuilder.pointGraph() {
    navigation(
        startDestination = PointScreen.LeaderboardView.route,
        route = PointScreen.Root.route
    ) {
        slideComposable(PointScreen.LeaderboardView.route) {
            LeaderboardView()
        }
    }
}
