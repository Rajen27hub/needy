package com.canopas.ui.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import com.canopas.ui.phonelogin.LoginOtpView
import com.canopas.ui.phonelogin.PhoneLoginView
import com.canopas.ui.signinmethods.SignInMethodsView
import com.canopas.ui.utils.slideComposable
import com.google.accompanist.navigation.animation.navigation

@ExperimentalAnimationApi
fun NavGraphBuilder.loginGraph(
    navController: NavHostController,
) {
    navigation(startDestination = LoginScreen.SignInMethod.route, route = LoginScreen.Root.route) {
        slideComposable(
            LoginScreen.SignInMethod.route
        ) {
            SignInMethodsView()
        }
        slideComposable(
            LoginScreen.PhoneLogin.route
        ) {
            PhoneLoginView(navController)
        }
        slideComposable(
            LoginScreen.LoginOtp.route + "/{$ARGUMENT_PHONE_NO}" + "/{$ARGUMENT_VERIFICATION_ID}"
        ) {
            val phoneNo = it.arguments?.getString(ARGUMENT_PHONE_NO)
                ?: ""
            val verificationId = it.arguments?.getString(ARGUMENT_VERIFICATION_ID)
                ?: ""
            LoginOtpView(phoneNo, verificationId)
        }
    }
}
