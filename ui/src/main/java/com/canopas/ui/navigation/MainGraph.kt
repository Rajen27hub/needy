package com.canopas.ui.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.canopas.feature_community_ui.community.CommunityView
import com.canopas.ui.activitystatus.SubscriptionStatusView
import com.canopas.ui.congratulationprompt.CongratulationView
import com.canopas.ui.customactivity.CustomActivityView
import com.canopas.ui.customview.jetpackview.BlogsAndPodcastWebView
import com.canopas.ui.customview.jetpackview.YoutubeViewDialog
import com.canopas.ui.editsubscription.EditSubscriptionView
import com.canopas.ui.goals.activityselection.AddGoalSelectionView
import com.canopas.ui.goals.addgoals.AddOrUpdateGoalView
import com.canopas.ui.goals.mygoals.MyGoalsView
import com.canopas.ui.goals.whythisactivity.WhyThisActivityView
import com.canopas.ui.habit.HabitConfigureView
import com.canopas.ui.habit.configureactivity.ConfigureActivityView
import com.canopas.ui.habit.successstories.SuccessStories
import com.canopas.ui.home.explore.ExploreView
import com.canopas.ui.home.explore.ExploreViewModel
import com.canopas.ui.home.explore.activitylist.ActivityListView
import com.canopas.ui.home.explore.storyView.StoryView
import com.canopas.ui.home.ongoing.SubscriptionListView
import com.canopas.ui.home.ongoing.addActivityView.AddActivityView
import com.canopas.ui.home.ongoing.history.ArchivedActivitiesView
import com.canopas.ui.home.ongoing.poststory.PostSuccessStoryView
import com.canopas.ui.home.settings.settingsview.SettingsView
import com.canopas.ui.notes.addnotes.AddNotesView
import com.canopas.ui.notes.addtemplate.AddOrEditTemplateView
import com.canopas.ui.notes.managetemplates.ManageTemplatesView
import com.canopas.ui.notes.noteslist.NotesListView
import com.canopas.ui.notes.notetemplates.NoteTemplatesView
import com.canopas.ui.notificationPermission.NotificationPermissionView
import com.canopas.ui.successstorydetail.SuccessStoryDetailView
import com.canopas.ui.utils.slideComposable
import com.google.accompanist.navigation.animation.composable
import com.google.accompanist.navigation.animation.navigation
import com.google.accompanist.navigation.material.ExperimentalMaterialNavigationApi
import com.google.accompanist.navigation.material.bottomSheet

@OptIn(ExperimentalMaterialNavigationApi::class)
@ExperimentalAnimationApi
fun NavGraphBuilder.mainGraph(
    navController: NavHostController,
) {
    navigation(startDestination = MainScreen.Explore.route, route = MainScreen.Root.route) {
        composable(MainScreen.Explore.route) {
            val viewModel = hiltViewModel<ExploreViewModel>()
            ExploreView(viewModel)
        }
        composable(MainScreen.Current.route) {
            SubscriptionListView()
        }

        composable(MainScreen.Community.route) {
            CommunityView()
        }

        composable(LoginScreen.NotificationPermissionView.route) {
            NotificationPermissionView()
        }

        slideComposable(Screen.HabitConfigure.route + "/{$ARGUMENT_ACTIVITY_ID}") { navBackStack ->
            val activityId = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_ID)
                ?: throw AssertionError("ActivityId can't be null")
            HabitConfigureView(activityId.toInt())
        }

        slideComposable(Screen.ActivityList.route + "/{$ARGUMENT_CATEGORY}") { navBackStack ->
            val category = navBackStack.arguments?.getString(ARGUMENT_CATEGORY)?.toInt() ?: 0
            ActivityListView(category)
        }

        slideComposable(
            Screen.SuccessStoryDetail.route + "/{$ARGUMENT_STORY_DETAIL_ID}" + "?isEditModeEnable={isEditModeEnable}",
            arguments = listOf(
                navArgument(ARGUMENT_STORY_EDIT_MODE_ENABLE) {
                    type = NavType.BoolType
                }
            )
        ) { navBackStack ->
            val storyId = navBackStack.arguments?.getString(ARGUMENT_STORY_DETAIL_ID)
                ?: throw AssertionError("StoryId can't be null")
            val isEditModeEnable =
                navBackStack.arguments?.getBoolean(ARGUMENT_STORY_EDIT_MODE_ENABLE) ?: false
            SuccessStoryDetailView(storyId.toInt(), isEditModeEnable)
        }

        slideComposable(
            route = Screen.Status.route + "?subscriptionId={subscriptionId}" + "?headerText={headerText}" + "?showArchivedStatus={showArchivedStatus}",
            arguments = listOf(
                navArgument(ARGUMENT_SUBSCRIPTION_ID) {
                    type = NavType.StringType
                },
                navArgument("headerText") {
                    type = NavType.StringType
                },
                navArgument("showArchivedStatus") {
                    type = NavType.BoolType
                },
            )
        ) { backStack ->
            val subscriptionId = backStack.arguments?.getString(ARGUMENT_SUBSCRIPTION_ID) ?: ""
            val headerText = backStack.arguments?.getString("headerText") ?: ""
            val showArchivedStatus = backStack.arguments?.getBoolean("showArchivedStatus") ?: false
            SubscriptionStatusView(
                subscriptionId,
                "",
                headerText,
                showArchivedStatus
            )
        }

        slideComposable(
            route = Screen.Status.route + "?activityId={activityId}" + "?headerText={headerText}",
            arguments = listOf(
                navArgument(ARGUMENT_ACTIVITY_ID) {
                    type = NavType.StringType
                },
                navArgument("headerText") {
                    type = NavType.StringType
                }
            )
        ) { backStack ->
            val activityId = backStack.arguments?.getString(ARGUMENT_ACTIVITY_ID) ?: ""
            val headerText = backStack.arguments?.getString("headerText") ?: ""
            SubscriptionStatusView("", activityId, headerText, false)
        }

        slideComposable(
            Screen.Congratulation.route + "?activityName={activityName}" + "?subscriptionId={subscriptionId}" + "?activityId={activityId}",
            arguments = listOf(
                navArgument(ARGUMENT_ACTIVITY_NAME) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_SUBSCRIPTION_ID) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_ACTIVITY_ID) {
                    type = NavType.StringType
                }
            )
        ) { navBackStack ->
            val activityName = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_NAME) ?: ""
            val subscriptionId = navBackStack.arguments?.getString(ARGUMENT_SUBSCRIPTION_ID) ?: ""
            val activityId = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_ID) ?: ""
            CongratulationView(activityName, subscriptionId, activityId)
        }

        slideComposable(
            Screen.PostStory.route + "?activityName={activityName}" + "?activityId={activityId}",
            arguments = listOf(
                navArgument(ARGUMENT_ACTIVITY_NAME) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_ACTIVITY_ID) {
                    type = NavType.StringType
                }
            )
        ) { navBackStack ->
            val activityName = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_NAME) ?: ""
            val activityId = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_ID) ?: ""
            PostSuccessStoryView(activityName, activityId, "")
        }

        slideComposable(
            Screen.PostStory.route + "?activityName={activityName}" + "?activityId={activityId}" + "?storyDetailId={storyDetailId}" + "?isEditModeEnable={isEditModeEnable}",
            arguments = listOf(
                navArgument(ARGUMENT_ACTIVITY_NAME) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_ACTIVITY_ID) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_STORY_DETAIL_ID) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_STORY_EDIT_MODE_ENABLE) {
                    type = NavType.BoolType
                }
            )
        ) { navBackStack ->
            val activityName = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_NAME) ?: ""
            val activityId = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_ID) ?: ""
            val storyId = navBackStack.arguments?.getString(ARGUMENT_STORY_DETAIL_ID) ?: ""
            val isEditModeEnable =
                navBackStack.arguments?.getBoolean(ARGUMENT_STORY_EDIT_MODE_ENABLE) ?: false
            PostSuccessStoryView(activityName, activityId, storyId, isEditModeEnable)
        }

        slideComposable(
            Screen.SuccessStory.route + "?activityId={activityId}",
            arguments = listOf(
                navArgument(ARGUMENT_ACTIVITY_ID) {
                    type = NavType.StringType
                },
            )
        ) { backStack ->
            val activityId = backStack.arguments?.getString(ARGUMENT_ACTIVITY_ID) ?: ""
            SuccessStories(activityId)
        }

        slideComposable(
            Screen.SuccessStory.route + "?isEditModeEnable={isEditModeEnable}",
            arguments = listOf(
                navArgument(ARGUMENT_STORY_EDIT_MODE_ENABLE) {
                    type = NavType.BoolType
                }
            )
        ) { backStack ->
            val isEditModeEnable =
                backStack.arguments?.getBoolean(ARGUMENT_STORY_EDIT_MODE_ENABLE) ?: false
            SuccessStories("", isEditModeEnable)
        }

        bottomSheet(
            Screen.WebView.route + "?url={url}",
            arguments = listOf(
                navArgument(ARGUMENT_URL) {
                    type = NavType.StringType
                },
            )
        ) { backStack ->
            val url =
                backStack.arguments?.getString(ARGUMENT_URL) ?: ""
            BlogsAndPodcastWebView(url = url)
        }

        slideComposable(Screen.CustomActivityView.route + "/{$ARGUMENT_STORY_ID}") { navBackStack ->
            val storyId = navBackStack.arguments?.getString(ARGUMENT_STORY_ID) ?: ""
            CustomActivityView(storyId.toInt())
        }

        slideComposable(Screen.EditSubscription.route + "/{$ARGUMENT_SUBSCRIPTION_ID}") { navBackStack ->
            val subscriptionId = navBackStack.arguments?.getString(ARGUMENT_SUBSCRIPTION_ID) ?: ""
            EditSubscriptionView(
                subscriptionId = subscriptionId.toInt()
            )
        }

        slideComposable(Screen.ConfigureActivity.route + "/{$ARGUMENT_ACTIVITY_ID}" + "/{$ARGUMENT_ACTIVITY_NAME}" + "/{$ARGUMENT_STORY_ID}" + "?activityTime={activityTime}" + "?activityDuration={activityDuration}" + "?goalsTitle={goalsTitle}") { navBackStack ->
            val activityId = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_ID)
                ?: throw AssertionError("ActivityId can't be null")
            val activityName = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_NAME) ?: ""
            val activityTime = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_TIME) ?: ""
            val activityDuration = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_DURATION) ?: ""
            val storyId = navBackStack.arguments?.getString(ARGUMENT_STORY_ID) ?: ""
            val goalsTitle = navBackStack.arguments?.getString(ARGUMENT_GOALS_TITLE) ?: ""
            ConfigureActivityView(
                activityId.toInt(),
                activityName = activityName,
                defaultTime = activityTime,
                defaultDuration = activityDuration,
                storyId = storyId.toInt(),
                goalsTitle = goalsTitle
            )
        }

        slideComposable(
            Screen.ConfigureActivity.route + "/{$ARGUMENT_STORY_ID}" + "?title={title}" + "?goalsTitle={goalsTitle}",
            arguments = listOf(
                navArgument(ARGUMENT_ACTIVITY_TITLE) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_GOALS_TITLE) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_STORY_ID) {
                    type = NavType.StringType
                }
            )
        ) { navBackStack ->
            val title = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_TITLE) ?: ""
            val goalsTitle = navBackStack.arguments?.getString(ARGUMENT_GOALS_TITLE) ?: ""
            val storyId = navBackStack.arguments?.getString(ARGUMENT_STORY_ID) ?: ""
            ConfigureActivityView(title = title, goalsTitle = goalsTitle, storyId = storyId.toInt())
        }

        slideComposable(Screen.WhyThisActivity.route + "/{$ARGUMENT_ACTIVITY_ID}" + "/{$ARGUMENT_ACTIVITY_NAME}" + "/{$ARGUMENT_STORY_ID}" + "?activityTime={activityTime}" + "?activityDuration={activityDuration}") { navBackStack ->
            val activityId = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_ID)
                ?: throw AssertionError("ActivityId can't be null")
            val activityName = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_NAME) ?: ""
            val activityTime = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_TIME) ?: ""
            val activityDuration =
                navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_DURATION) ?: ""
            val storyId = navBackStack.arguments?.getString(ARGUMENT_STORY_ID) ?: ""
            WhyThisActivityView(
                activityId.toInt(),
                activityName = activityName,
                defaultTime = activityTime,
                defaultDuration = activityDuration,
                storyId = storyId.toInt()
            )
        }

        slideComposable(
            Screen.WhyThisActivity.route + "/{$ARGUMENT_STORY_ID}" + "?title={title}",
            arguments = listOf(
                navArgument(ARGUMENT_ACTIVITY_TITLE) {
                    type = NavType.StringType
                }
            )
        ) { navBackStack ->
            val title = navBackStack.arguments?.getString(ARGUMENT_ACTIVITY_TITLE) ?: ""
            val storyId = navBackStack.arguments?.getString(ARGUMENT_STORY_ID) ?: ""
            WhyThisActivityView(title = title, storyId = storyId.toInt())
        }
        slideComposable(
            Screen.MyGoalsScreen.route + "/{$ARGUMENT_SUBSCRIPTION_ID}",
            arguments = listOf(
                navArgument(ARGUMENT_SUBSCRIPTION_ID) {
                    type = NavType.StringType
                }
            )
        ) { navBackStack ->
            val subscriptionId = navBackStack.arguments?.getString(ARGUMENT_SUBSCRIPTION_ID) ?: ""
            MyGoalsView(subscriptionId)
        }

        bottomSheet(
            Screen.VideoView.route + "?videoUrl={videoUrl}",
            arguments = listOf(
                navArgument(ARGUMENT_VIDEO_URL) {
                    type = NavType.StringType
                }
            )
        ) { navBackStack ->
            val video = navBackStack.arguments?.getString(ARGUMENT_VIDEO_URL) ?: ""
            YoutubeViewDialog(url = video, navController)
        }

        slideComposable(Screen.AddOrUpdateGoalView.route + "/{$ARGUMENT_SUBSCRIPTION_ID}") { navBackStack ->
            val subscriptionId = navBackStack.arguments?.getString(ARGUMENT_SUBSCRIPTION_ID) ?: "0"
            AddOrUpdateGoalView(subscriptionId = subscriptionId.toInt())
        }

        slideComposable(Screen.AddOrUpdateGoalView.route + "/{$ARGUMENT_SUBSCRIPTION_ID}" + "/{$ARGUMENT_GOAL_ID}") { navBackStack ->
            val subscriptionId = navBackStack.arguments?.getString(ARGUMENT_SUBSCRIPTION_ID) ?: "0"
            val goalId = navBackStack.arguments?.getString(ARGUMENT_GOAL_ID) ?: "0"
            AddOrUpdateGoalView(subscriptionId = subscriptionId.toInt(), goalId = goalId.toInt())
        }

        slideComposable(Screen.AddGoalSelectionView.route) {
            AddGoalSelectionView()
        }

        slideComposable(
            Screen.AddNotesView.route + "?activityName={activityName}" +
                "?activityId={activityId}" +
                "?notesId={notesId}" + "?isCustom={isCustom}",
            arguments = listOf(
                navArgument(ARGUMENT_ACTIVITY_NAME) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_ACTIVITY_ID) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_NOTES_ID) {
                    type = NavType.StringType
                },
                navArgument(ARGUMENT_IS_CUSTOM) {
                    type = NavType.BoolType
                }
            )
        ) { backStack ->
            val activityName = backStack.arguments?.getString(ARGUMENT_ACTIVITY_NAME) ?: ""
            val activityId = backStack.arguments?.getString(ARGUMENT_ACTIVITY_ID) ?: ""
            val notesId = backStack.arguments?.getString(ARGUMENT_NOTES_ID) ?: ""
            val isCustom = backStack.arguments?.getBoolean(ARGUMENT_IS_CUSTOM) ?: false
            val templateId = navController.currentBackStackEntry?.savedStateHandle?.getLiveData<Int>(
                ARGUMENT_TEMPLATE_ID
            )?.value
            AddNotesView(
                activityName = activityName,
                activityId = activityId,
                notesId = notesId,
                isCustom = isCustom,
                templateId = templateId
            )
            navController.currentBackStackEntry?.savedStateHandle?.clearSavedStateProvider(
                ARGUMENT_TEMPLATE_ID
            )
        }

        slideComposable(
            Screen.NotesListView.route + "/{$ARGUMENT_ACTIVITY_ID}" + "?isCustom={isCustom}" + "?activityName={activityName}",
            arguments = listOf(
                navArgument(ARGUMENT_IS_CUSTOM) {
                    type = NavType.BoolType
                },
                navArgument(ARGUMENT_ACTIVITY_NAME) {
                    type = NavType.StringType
                }

            )
        ) { backStack ->
            val activityId = backStack.arguments?.getString(ARGUMENT_ACTIVITY_ID)
                ?: throw AssertionError("ActivityId can't be null")
            val isCustom = backStack.arguments?.getBoolean(ARGUMENT_IS_CUSTOM) ?: false
            val activityName = backStack.arguments?.getString(ARGUMENT_ACTIVITY_NAME) ?: ""
            NotesListView(activityId.toInt(), isCustom, activityName)
        }

        slideComposable(Screen.NoteTemplatesView.route) {
            NoteTemplatesView()
        }

        slideComposable(Screen.AddOrEditTemplateView.route) {
            AddOrEditTemplateView(templateId = null)
        }

        slideComposable(Screen.AddOrEditTemplateView.route + "/{$ARGUMENT_TEMPLATE_ID}") { backStack ->
            val templateId = backStack.arguments?.getString(ARGUMENT_TEMPLATE_ID) ?: "0"
            AddOrEditTemplateView(templateId = templateId.toInt())
        }

        slideComposable(Screen.ManageTemplatesView.route) {
            ManageTemplatesView()
        }

        slideComposable(Screen.ArchivedActivitiesView.route) {
            ArchivedActivitiesView()
        }
        slideComposable(Screen.Setting.route) {
            SettingsView()
        }
        slideComposable(Screen.AddActivityView.route) {
            AddActivityView()
        }
        slideComposable(
            Screen.StoryView.route + "/{$ARGUMENT_STORY_ID}",
            arguments = listOf(
                navArgument(ARGUMENT_STORY_ID) {
                    type = NavType.StringType
                }
            )
        ) { backStack ->
            val storyId = backStack.arguments?.getString(ARGUMENT_STORY_ID) ?: ""
            StoryView(storyId = storyId)
        }
    }
}
