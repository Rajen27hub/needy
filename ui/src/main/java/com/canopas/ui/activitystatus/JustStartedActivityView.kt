package com.canopas.ui.activitystatus

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.InterBoldFont
import com.canopas.base.ui.White
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.StatusTheme
import com.canopas.data.model.Subscription
import com.canopas.feature_community_ui.activitystatus.textBrush
import com.canopas.ui.R

@Composable
fun JustStartedActivityView(habitDetail: Subscription, headerText: String) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(StatusTheme.SubscriptionColors.statusBarBg),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        if (headerText.isNotEmpty()) {
            Text(
                text = buildAnnotatedString {
                    append(stringResource(R.string.subscription_status_header_subtitle_text_part_1))
                    withStyle(
                        style = SpanStyle(
                            fontFamily = InterBoldFont,
                        )
                    ) {
                        append(habitDetail.activity.name)
                    }
                    append(stringResource(R.string.subscription_status_header_subtitle_text_part_2))
                },
                style = AppTheme.typography.bodyTextStyle1,
                lineHeight = 20.sp,
                textAlign = TextAlign.Center,
                color = ComposeTheme.colors.textPrimary,
                modifier = Modifier.padding(start = 20.dp, end = 20.dp)
            )
        } else {
            Text(
                text = stringResource(R.string.subscription_status_header_subtitle_text_2),
                style = AppTheme.typography.bodyTextStyle1,
                lineHeight = 20.sp,
                textAlign = TextAlign.Center,
                color = ComposeTheme.colors.textPrimary,
                modifier = Modifier.padding(start = 20.dp, end = 20.dp)
            )
        }
    }

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(
                Brush.verticalGradient(
                    if (ComposeTheme.isDarkMode)
                        listOf(
                            StatusTheme.SubscriptionColors.statusBarBg,
                            StatusTheme.SubscriptionColors.statusBarBg
                        ) else
                        listOf(
                            StatusTheme.SubscriptionColors.statusBarBg, White
                        )
                )
            ),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier.widthIn(max = 600.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(
                    id = if (ComposeTheme.isDarkMode) R.drawable.subscription_status_analytics_dark_image
                    else R.drawable.subscription_status_analytics_image
                ),
                contentDescription = null,
                modifier = Modifier
                    .widthIn(max = 500.dp)
                    .fillMaxWidth()
                    .padding(horizontal = 140.dp, vertical = 32.dp)
                    .aspectRatio(1.02f),
                contentScale = ContentScale.FillBounds
            )
            Text(
                text = stringResource(R.string.subscription_status_analytics_and_insights_text),
                style = AppTheme.typography.h3TextStyle,
                lineHeight = 28.sp,
                textAlign = TextAlign.Center,
                modifier = Modifier
                    .padding(
                        start = 40.dp, end = 40.dp, bottom = 44.dp
                    )
                    .textBrush(
                        Brush.linearGradient(
                            listOf(
                                StatusTheme.SubscriptionColors.statusSubtitleTextColor,
                                StatusTheme.SubscriptionColors.statusSubtitleTextColor2,
                            )
                        )
                    )
            )
        }
    }
}
