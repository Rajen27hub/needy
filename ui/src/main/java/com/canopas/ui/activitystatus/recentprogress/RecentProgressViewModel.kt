package com.canopas.ui.activitystatus.recentprogress

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.Recent
import com.canopas.base.data.model.RecentDaysHistory
import com.canopas.data.model.CompletionBody
import com.canopas.data.model.Subscription
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.activitystatus.PastCompletionState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.util.Calendar
import java.util.TimeZone
import javax.inject.Inject

@HiltViewModel
class RecentProgressViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val resources: Resources
) : ViewModel() {

    val pastCompletionState = MutableStateFlow<PastCompletionState>(PastCompletionState.START)
    val currentLoadingIndex = MutableStateFlow(0)
    private var subscription: Subscription? = null

    fun onStart(subscription: Subscription) {
        this.subscription = subscription
    }

    fun checkPastCompletionCondition(
        recentDaysHistory: RecentDaysHistory,
        recent: Recent,
        index: Int
    ) {
        if (recent.date >= getMinimumRequiredTime() && !recentDaysHistory.isCompleted && !recentDaysHistory.isCurrentDay) {
            onPastCompletionClicked(recent.date)
            currentLoadingIndex.tryEmit(index)
        }
    }

    fun onPastCompletionClicked(date: Long) {
        val subscription = subscription ?: return
        val subscriptionId = subscription.id
        viewModelScope.launch {
            pastCompletionState.tryEmit(PastCompletionState.LOADING)
            withContext(appDispatcher.IO) {
                val data = CompletionBody(
                    subscription_id = subscriptionId, completion_time = date, timeZone = TimeZone.getDefault().id
                )
                service.savePastCompletionActivity(data).onSuccess {
                    pastCompletionState.tryEmit(PastCompletionState.SUCCESS)
                }.onFailure { e ->
                    Timber.e(e)
                    pastCompletionState.emit(
                        PastCompletionState.FAILURE(
                            e.toUserError(resources)
                        )
                    )
                }
            }
        }
    }

    private fun getMinimumRequiredTime(): Long {
        val cal = Calendar.getInstance()
        cal[Calendar.HOUR_OF_DAY] = 0
        cal.clear(Calendar.MINUTE)
        cal.clear(Calendar.SECOND)
        cal.clear(Calendar.MILLISECOND)
        return (((cal.timeInMillis.div(1000L)) - (86400 * 2)))
    }
}
