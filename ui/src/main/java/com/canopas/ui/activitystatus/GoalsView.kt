package com.canopas.ui.activitystatus

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.AddCircle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.parse
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.StatusTheme
import com.canopas.data.model.Goal
import com.canopas.data.model.Subscription
import com.canopas.ui.R

@Suppress("FunctionName")
fun BuildGoalsView(
    viewModel: SubscriptionStatusViewModel,
    goals: List<Goal>,
    habitDetail: Subscription,
    lazyListScope: LazyListScope
) {

    lazyListScope.item {
        GoalsHeaderComposable(viewModel = viewModel, habitDetail)
    }
    if (goals.isNotEmpty()) {
        lazyListScope.item {
            BoxWithConstraints(
                modifier = Modifier
                    .widthIn(max = 600.dp)
                    .fillMaxWidth()
                    .padding(top = 16.dp),
                contentAlignment = Alignment.TopStart
            ) {
                val itemWidth = maxWidth.times(0.2f).plus(200.dp)
                LazyRow(
                    contentPadding = PaddingValues(start = 20.dp),
                    state = rememberLazyListState(),
                    horizontalArrangement = Arrangement.Center,
                    verticalAlignment = Alignment.CenterVertically
                ) {
                    item {
                        goals.forEachIndexed { index, goal ->
                            if (index < 5) {
                                GoalsCardView(
                                    goal, itemWidth, viewModel, habitDetail
                                )
                            }
                        }
                    }
                }
            }
        }
    } else {
        lazyListScope.item {
            EmptyGoalsComposable()
            Divider(
                Modifier
                    .background(ComposeTheme.colors.background)
                    .padding(start = 20.dp, end = 20.dp, top = 40.dp)
                    .fillMaxWidth(),
                thickness = 1.dp,
                color = StatusTheme.SubscriptionColors.dividerColor
            )
        }
    }
}

@Composable
fun GoalsHeaderComposable(
    viewModel: SubscriptionStatusViewModel,
    subscription: Subscription
) {
    Column(
        Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .background(ComposeTheme.colors.background)
            .padding(top = 40.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            modifier = Modifier.padding(horizontal = 20.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier.weight(1f), verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = stringResource(R.string.subscription_status_goals_header_text),
                    style = AppTheme.typography.h2TextStyle,
                    color = ComposeTheme.colors.textPrimary
                )
                IconButton(onClick = {
                    viewModel.navigateToAddGoal(subscription.id)
                }) {
                    Icon(
                        imageVector = Icons.Outlined.AddCircle,
                        tint = ComposeTheme.colors.primary,
                        contentDescription = "add goals icon",
                        modifier = Modifier.size(28.dp)
                    )
                }
            }

            TextButton(
                onClick = {
                    viewModel.navigateToMyGoalsView(subscription.id)
                },
                modifier = Modifier.motionClickEvent { },
            ) {
                Text(
                    text = stringResource(id = R.string.subscription_status_view_all_text),
                    style = AppTheme.typography.buttonStyle,
                    textAlign = TextAlign.End,
                    color = ComposeTheme.colors.primary
                )
            }
        }
    }
}

@Composable
fun GoalsCardView(
    goal: Goal,
    itemWidth: Dp,
    viewModel: SubscriptionStatusViewModel,
    subscription: Subscription
) {
    Column(
        modifier = Modifier
            .width(itemWidth)
            .aspectRatio(1.80f)
            .padding(end = 16.dp)
            .background(
                if (ComposeTheme.isDarkMode) GoalsCardColor.copy(0.30f) else {
                    Color
                        .parse(if (subscription.activity.color2 != null) subscription.activity.color2 else subscription.activity.color)
                        .copy(0.5f)
                },
                shape = RoundedCornerShape(4.dp)
            )
            .motionClickEvent(onClick = {
                viewModel.navigateToEditGoal(goal.id, subscription.id)
            }),
        verticalArrangement = Arrangement.SpaceBetween
    ) {

        Text(
            text = goal.title ?: "",
            style = AppTheme.typography.goalsBodyTextStyle,
            color = ComposeTheme.colors.textPrimary,
            maxLines = 4,
            overflow = TextOverflow.Ellipsis,
            textAlign = TextAlign.Start,
            lineHeight = 22.sp,
            modifier = Modifier
                .fillMaxWidth()
                .padding(top = 16.dp, start = 12.dp, end = 12.dp, bottom = 12.dp)
        )

        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(start = 12.dp, end = 12.dp, bottom = 16.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(verticalAlignment = Alignment.CenterVertically) {
                if (goal.dueDate.isNotEmpty()) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_goal_due_icon),
                        contentDescription = "",
                        modifier = Modifier.size(20.dp),
                        tint = if (ComposeTheme.isDarkMode) ComposeTheme.colors.textSecondary else GoalsSubTextColor
                    )
                    Text(
                        text = Utils.goalsDueDateFormat.format(Utils.format.parse(goal.dueDate)!!),
                        style = AppTheme.typography.bodyTextStyle3,
                        color = if (ComposeTheme.isDarkMode) ComposeTheme.colors.textSecondary else GoalsSubTextColor,
                        modifier = Modifier.padding(start = 4.dp)
                    )
                }
            }

            if (goal.occurrences != 0) {
                Text(
                    text = goal.completions.toString() + "/" + goal.occurrences.toString(),
                    style = AppTheme.typography.buttonStyle,
                    color = ComposeTheme.colors.textPrimary
                )
            }
        }
    }
}

@Composable
fun EmptyGoalsComposable() {
    Box(
        modifier = Modifier.background(ComposeTheme.colors.background),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(R.string.subscription_status_empty_goals_text),
            style = AppTheme.typography.subTitleTextStyle1,
            lineHeight = 24.sp,
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(
                    start = 40.dp, end = 40.dp, top = 40.dp
                ),
            textAlign = TextAlign.Center,
            color = ComposeTheme.colors.textSecondary
        )
    }
}

private val GoalsCardColor = Color(0xFFFFF3EB)
private val GoalsSubTextColor = Color(0x66000000)
