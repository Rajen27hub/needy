package com.canopas.ui.activitystatus.recentprogress

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils
import com.canopas.base.ui.activity.ProgressHeaderView
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Subscription
import com.canopas.ui.activitystatus.PastCompletionState
import com.canopas.ui.activitystatus.SubscriptionStatusViewModel
import com.google.android.gms.common.util.DeviceProperties

@Composable
fun RecentProgressView(
    habitDetail: Subscription,
    showArchivedStatus: Boolean
) {
    val viewModel = hiltViewModel<RecentProgressViewModel>()
    val statusViewModel = hiltViewModel<SubscriptionStatusViewModel>()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(habitDetail)
    })

    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(ComposeTheme.colors.background),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        val durationDate =
            Utils.getFormattedStartAndEndDate(
                habitDetail.progress.recent.first().date,
                habitDetail.progress.recent.last().date
            )

        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(
                    top = 28.dp,
                    start = 20.dp,
                    end = 20.dp,
                ),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Top
        ) {
            ProgressHeaderView(
                totalDays = habitDetail.progress.recent.count(),
                completedDays = habitDetail.progress.recent.count { it.completed },
                isLoadingState = false,
                currentMonthName = durationDate
            )
        }

        val currentWeekProgress = Utils.getCurrentWeekProgress(habitDetail.progress.recent)
        val completionState by viewModel.pastCompletionState.collectAsState()
        val loadingCompletionState = completionState is PastCompletionState.LOADING
        val isLoadingIndex by viewModel.currentLoadingIndex.collectAsState()

        if (completionState is PastCompletionState.SUCCESS) {
            statusViewModel.displayStatus(
                habitDetail.id.toString(),
                habitDetail.activity.id.toString()
            )
        }

        BoxWithConstraints(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(top = 46.dp),
            contentAlignment = Alignment.Center
        ) {
            val outerPadding = if (DeviceProperties.isTablet(LocalContext.current)) 38.dp else 44.dp
            Row(
                Modifier
                    .wrapContentWidth()
                    .align(Alignment.Center),
                verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.spacedBy((minWidth / 7) - outerPadding)
            ) {
                val weekDayNumber =
                    Utils.getCurrentWeekDay(habitDetail.progress.recent)
                currentWeekProgress.forEachIndexed { index, recentDaysHistory ->
                    Column(
                        modifier = Modifier
                            .wrapContentSize(),
                        verticalArrangement = Arrangement.Center,
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        Text(
                            text = weekDayNumber[index].toString(),
                            style = AppTheme.typography.subscriptionDateTextStyle,
                            color = ComposeTheme.colors.textPrimary.copy(0.87f)
                        )
                        Spacer(modifier = Modifier.height(4.dp))
                        Text(
                            text = recentDaysHistory.day,
                            style = AppTheme.typography.bodyTextStyle4,
                            color = ComposeTheme.colors.textPrimary.copy(0.87f)
                        )
                        Spacer(modifier = Modifier.height(8.dp))

                        if (isLoadingIndex == index && loadingCompletionState) {
                            CircularProgressIndicator(
                                modifier = Modifier.size(30.dp),
                                color = ComposeTheme.colors.primary
                            )
                        } else {
                            Image(
                                painter = painterResource(id = recentDaysHistory.icon),
                                contentDescription = null,
                                modifier = Modifier
                                    .size(40.dp)
                                    .motionClickEvent {
                                        if (!showArchivedStatus) {
                                            viewModel.checkPastCompletionCondition(
                                                recentDaysHistory,
                                                habitDetail.progress.recent[index],
                                                index
                                            )
                                        }
                                    },
                                colorFilter = ColorFilter.tint(recentDaysHistory.tint)
                            )
                        }
                    }
                }
            }
        }
    }
}
