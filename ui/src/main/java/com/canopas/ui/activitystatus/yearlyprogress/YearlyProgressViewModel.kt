package com.canopas.ui.activitystatus.yearlyprogress

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.ui.Utils
import com.canopas.data.model.ProgressHistory
import com.canopas.data.model.Subscription
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.activitystatus.ProgressHistoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.time.LocalDate
import java.util.Calendar
import javax.inject.Inject
import kotlin.math.max
import kotlin.math.min

@HiltViewModel
class YearlyProgressViewModel @Inject constructor(
    private val authManager: AuthManager,
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val resources: Resources
) : ViewModel() {

    val progressState = MutableStateFlow<ProgressHistoryState>(ProgressHistoryState.LOADING)
    private var subscription: Subscription? = null
    val yearlyProgressText = MutableStateFlow("")
    val subscriptionYears = MutableStateFlow(mutableListOf<Int>())
    val yearlyProgressList = MutableStateFlow(listOf<ProgressHistory>())
    val yearlyChartData = MutableStateFlow(
        mutableListOf(
            Pair(0f, "J"),
            Pair(0f, "F"),
            Pair(0f, "M"),
            Pair(0f, "A"),
            Pair(0f, "M"),
            Pair(0f, "J"),
            Pair(0f, "J"),
            Pair(0f, "A"),
            Pair(0f, "S"),
            Pair(0f, "O"),
            Pair(0f, "N"),
            Pair(0f, "D")
        )
    )

    fun onStart(subscription: Subscription, showArchivedStatus: Boolean) {
        this.subscription = subscription
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = subscription.start_date.times(1000L)
        val startYear = calendar.get(Calendar.YEAR)
        calendar.timeInMillis =
            if (showArchivedStatus) subscription.end_date.times(1000L) else System.currentTimeMillis()
        val endYear = calendar.get(Calendar.YEAR)
        subscriptionYears.tryEmit((startYear..endYear).toMutableList())
        onStateYearlyChanged(showArchivedStatus = showArchivedStatus)
    }

    fun getYearlyProgressHistory(end: Long, start: Long, subscriptionId: Int) {
        viewModelScope.launch {
            progressState.tryEmit(ProgressHistoryState.LOADING)
            val userId = authManager.currentUser?.id ?: return@launch
            withContext(appDispatcher.IO) {
                service.getSubscriptionsProgressHistory(userId, subscriptionId, end, start)
                    .onSuccess { progressList ->
                        yearlyProgressList.value = progressList
                        val progressByMonth = progressList.groupBy { progressHistory ->
                            val localDate =
                                LocalDate.parse(Utils.format.format(progressHistory.date.times(1000L)))
                            localDate.month
                        }
                        for ((month, progressData) in progressByMonth) {
                            val completed = progressData.filter { it.completed }.size
                            yearlyChartData.value[(month.value - 1)] =
                                Pair((completed).toFloat(), month.name[0].toString())
                        }
                        progressState.tryEmit(ProgressHistoryState.PROCESSED)
                    }.onFailure { e ->
                        Timber.e(e.toString())
                        progressState.tryEmit(ProgressHistoryState.FAILURE(e.toUserError(resources)))
                    }
            }
        }
    }

    fun onStateYearlyChanged(scrollingIndex: Int = 0, showArchivedStatus: Boolean) {
        val subscription = subscription ?: return
        yearlyChartData.value = mutableListOf(
            Pair(0f, "J"),
            Pair(0f, "F"),
            Pair(0f, "M"),
            Pair(0f, "A"),
            Pair(0f, "M"),
            Pair(0f, "J"),
            Pair(0f, "J"),
            Pair(0f, "A"),
            Pair(0f, "S"),
            Pair(0f, "O"),
            Pair(0f, "N"),
            Pair(0f, "D")
        )
        val calendar = Calendar.getInstance()
        calendar.set(
            Calendar.YEAR,
            subscriptionYears.value[subscriptionYears.value.lastIndex - scrollingIndex]
        )
        calendar.set(Calendar.MONTH, Calendar.JANUARY)
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        val startTimeInMillis = calendar.timeInMillis
        calendar.set(
            Calendar.YEAR,
            subscriptionYears.value[subscriptionYears.value.lastIndex - scrollingIndex]
        )
        calendar.set(Calendar.MONTH, Calendar.DECEMBER)
        calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH))
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        val endTimeInMillis = calendar.timeInMillis
        val startMonth =
            Utils.allTimeFormat.format(max(subscription.start_date.times(1000L), startTimeInMillis))
        val endMonth = Utils.allTimeFormat.format(min(System.currentTimeMillis(), endTimeInMillis))
        yearlyProgressText.tryEmit("$startMonth-$endMonth")
        val subscriptionEndTime =
            if (showArchivedStatus) subscription.end_date.times(1000L) else System.currentTimeMillis()
        getYearlyProgressHistory(
            min(endTimeInMillis.div(1000L), subscriptionEndTime.div(1000L)),
            startTimeInMillis.div(1000L),
            subscription.id
        )
    }
}
