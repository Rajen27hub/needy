package com.canopas.ui.activitystatus.sixmonthlyprogress

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.activity.ProgressHeaderView
import com.canopas.base.ui.barchartessentials.SixMonthlyChartView
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Subscription
import com.canopas.ui.activitystatus.ProgressHistoryState
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState

@OptIn(ExperimentalPagerApi::class)
@Composable
fun SixMonthlyProgressView(
    habitDetail: Subscription,
    showArchivedStatus: Boolean
) {
    val viewModel = hiltViewModel<SixMonthlyProgressViewModel>()
    val pagerState = rememberPagerState()
    val progressState by viewModel.progressState.collectAsState()
    val isLoadingState = progressState is ProgressHistoryState.LOADING
    val yearlyProgressText by viewModel.sixMonthlyProgressText.collectAsState()
    val sixMonthlySubscriptionDuration by viewModel.sixMonthlySubscriptionDuration.collectAsState()
    val sixMonthlyProgressList by viewModel.sixMonthlyProgressList.collectAsState()
    val sixMonthlyChartData by viewModel.sixMonthlyChartData.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(habitDetail, showArchivedStatus)
    })

    LaunchedEffect(key1 = pagerState.currentPage, block = {
        viewModel.onStateSixMonthlyChanged(pagerState.currentPage, showArchivedStatus)
    })
    Column(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(start = 20.dp, end = 20.dp, top = 28.dp),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            ProgressHeaderView(
                totalDays = sixMonthlyProgressList.size,
                completedDays = sixMonthlyProgressList.filter { it.completed }.size,
                isLoadingState = false,
                currentMonthName = yearlyProgressText
            )
            HorizontalPager(
                state = pagerState,
                count = sixMonthlySubscriptionDuration.size,
                reverseLayout = true,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 12.dp),
                itemSpacing = 20.dp,
            ) {
                Box(
                    modifier = Modifier
                        .wrapContentHeight()
                        .fillMaxWidth(),
                    contentAlignment = Alignment.Center
                ) {
                    if (isLoadingState) {
                        CircularProgressIndicator(color = ComposeTheme.colors.primary)
                    }
                    SixMonthlyChartView(sixMonthlyChartData = sixMonthlyChartData, isLoadingState)
                }
            }
        }
    }
}
