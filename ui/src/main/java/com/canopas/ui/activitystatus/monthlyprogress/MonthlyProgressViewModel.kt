package com.canopas.ui.activitystatus.monthlyprogress

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.ui.Utils
import com.canopas.data.model.Subscription
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.activitystatus.ProgressHistoryState
import com.canopas.ui.home.explore.CurrentDate
import com.kizitonwose.calendar.compose.CalendarState
import com.kizitonwose.calendar.core.CalendarDay
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.time.LocalDate
import java.time.YearMonth
import java.util.Calendar
import java.util.Date
import javax.inject.Inject
import kotlin.collections.ArrayList
import kotlin.math.max
import kotlin.math.min

@HiltViewModel
class MonthlyProgressViewModel @Inject constructor(
    private val date: CurrentDate,
    private val authManager: AuthManager,
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val resources: Resources
) : ViewModel() {

    val progressState = MutableStateFlow<ProgressHistoryState>(ProgressHistoryState.LOADING)
    val completedDaysList = MutableStateFlow(mutableListOf<LocalDate>())
    val missedDaysList = MutableStateFlow(mutableListOf<LocalDate>())
    val currentVisibleMonth = MutableStateFlow(YearMonth.now())
    val totalDays = MutableStateFlow(0)
    val completedDays = MutableStateFlow(0)
    val durationText = MutableStateFlow("")
    private var subscription: Subscription? = null

    fun onStart(
        subscription: Subscription,
        showArchivedStatus: Boolean,
        archivedMonthsBetween: Long
    ) {
        this.subscription = subscription
        onStateMonthChanged(
            if (showArchivedStatus) currentVisibleMonth.value.minusMonths(
                archivedMonthsBetween
            ) else currentVisibleMonth.value,
            if (showArchivedStatus) subscription.end_date else date.getCurrentTimeInSeconds(),
            showArchivedStatus
        )
    }

    fun onStateMonthChanged(yearMonth: YearMonth, endDate: Long, showArchivedStatus: Boolean) {
        currentVisibleMonth.tryEmit(yearMonth)
        val subscription = subscription ?: return
        setProgressData(
            subscription.start_date,
            endDate,
            subscription.id,
            showArchivedStatus
        )
    }

    private fun setProgressData(
        activityStartDate: Long,
        activityEndDate: Long,
        subscriptionId: Int,
        showArchivedStatus: Boolean
    ) {
        val now = Calendar.getInstance()
        now.set(Calendar.YEAR, (currentVisibleMonth.value.year))
        now.set(Calendar.MONTH, (currentVisibleMonth.value.monthValue - 1))
        now.set(Calendar.DAY_OF_MONTH, 1)
        now.set(Calendar.HOUR_OF_DAY, 0)
        now.set(Calendar.MINUTE, 0)
        val month = Utils.headerFormat.format(now.time)
        val startTime = max(activityStartDate, ((now.timeInMillis).div(1000L)))
        if (now.get(Calendar.YEAR).mod(4) == 0) {
            now.set(Calendar.DAY_OF_MONTH, currentVisibleMonth.value.month.maxLength())
        } else {
            if (now.get(Calendar.MONTH) == Calendar.FEBRUARY) {
                now.set(Calendar.DAY_OF_MONTH, currentVisibleMonth.value.month.maxLength() - 1)
            } else {
                now.set(Calendar.DAY_OF_MONTH, currentVisibleMonth.value.month.maxLength())
            }
        }
        now.set(Calendar.HOUR_OF_DAY, 23)
        now.set(Calendar.MINUTE, 59)
        now.set(Calendar.SECOND, 59)
        val endTime = min(
            if (showArchivedStatus) activityEndDate else date.getCurrentTimeInSeconds(),
            ((now.timeInMillis).div(1000L))
        )
        getProgressHistory(endTime, startTime, subscriptionId, month)
    }

    fun getProgressHistory(end: Long, start: Long, subscriptionId: Int, monthName: String) {
        viewModelScope.launch {
            progressState.tryEmit(ProgressHistoryState.LOADING)
            completedDaysList.value.clear()
            missedDaysList.value.clear()
            val userId = authManager.currentUser?.id ?: return@launch
            val completedList = ArrayList(completedDaysList.value)
            val missedList = ArrayList(missedDaysList.value)
            withContext(appDispatcher.IO) {
                service.getSubscriptionsProgressHistory(userId, subscriptionId, end, start)
                    .onSuccess { progressList ->
                        progressList.forEach {
                            val date = Date(it.date.times(1000L))
                            if (it.completed) {
                                completedList.add(LocalDate.parse(Utils.format.format(date)))
                            } else {
                                missedList.add(LocalDate.parse(Utils.format.format(date)))
                            }
                        }
                        completedDaysList.tryEmit(completedList)
                        missedDaysList.tryEmit(missedList)
                        totalDays.tryEmit(completedDaysList.value.size + missedDaysList.value.size)
                        completedDays.tryEmit(completedDaysList.value.size)
                        durationText.tryEmit(monthName)
                        progressState.tryEmit(ProgressHistoryState.PROCESSED)
                    }.onFailure { e ->
                        Timber.e(e.toString())
                        progressState.tryEmit(ProgressHistoryState.FAILURE(e.toUserError(resources)))
                    }
            }
        }
    }

    fun checkLastRowCondition(day: CalendarDay, state: CalendarState): Boolean {
        return day in state.firstVisibleMonth.weekDays.last() &&
            state.firstVisibleMonth.weekDays.last()
            .first().date.toString().substring(8, 10)
            .toInt() in (1..10) || day in state.lastVisibleMonth.weekDays.last() &&
            state.lastVisibleMonth.weekDays.last()
            .first().date.toString().substring(8, 10)
            .toInt() in (1..10)
    }
}
