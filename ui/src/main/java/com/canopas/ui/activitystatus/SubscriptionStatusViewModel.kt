package com.canopas.ui.activitystatus

import android.content.res.Resources
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.exception.APIError
import com.canopas.base.data.exception.toUserError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.StreaksData
import com.canopas.base.data.model.getFormattedDayDate
import com.canopas.base.ui.Utils.Companion.allTimeFormat
import com.canopas.base.ui.Utils.Companion.format
import com.canopas.base.ui.Utils.Companion.parseOrNull
import com.canopas.base.ui.model.ProgressTabType
import com.canopas.data.event.ActivitySubscriptionEvent
import com.canopas.data.model.DeleteActivityBody
import com.canopas.data.model.Goal
import com.canopas.data.model.RestoreActivityBody
import com.canopas.data.model.Subscription
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.home.explore.DEFAULT_SLEEP_ACTIVITY_ID
import com.canopas.ui.home.explore.DEFAULT_WAKE_UP_ACTIVITY_ID
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import timber.log.Timber
import javax.inject.Inject
import kotlin.collections.ArrayList

const val DEMO_STREAK_PLUS_DAYS = 259200L
const val TAB_RESTORE_ACTIVITY = 1
const val TAB_DELETE_ACTIVITY = 2
const val MAX_SUBSCRIPTION_LIMIT = 3

@HiltViewModel
class SubscriptionStatusViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val service: NoLonelyService,
    private val eventBus: EventBus,
    private val navManager: NavManager,
    private val appAnalytics: AppAnalytics,
    private val authManager: AuthManager,
    private val resources: Resources
) :
    ViewModel() {
    val streaksDataList = MutableStateFlow(mutableListOf<StreaksData>())
    private var subscription: Subscription? = null
    val showUnsubscribeAlert = MutableStateFlow(false)
    val maxStreak = MutableStateFlow(0)
    val sampleStreaksDate = MutableStateFlow("")
    val state =
        MutableStateFlow<SubscriptionState>(SubscriptionState.LOADING)
    val pastCompletionState = MutableStateFlow<PastCompletionState>(PastCompletionState.START)
    val progressSelectedTab = MutableStateFlow(ProgressTabType.PROGRESS_7D)
    val progressState = MutableStateFlow<ProgressHistoryState>(ProgressHistoryState.LOADING)
    val currentReading = MutableStateFlow(0f)
    val previousReading = MutableStateFlow(0f)
    val allTimeProgressText = MutableStateFlow("")
    val archivedState = MutableStateFlow<ArchivedState>(ArchivedState.LOADING)
    val userSubscriptionState =
        MutableStateFlow<UserSubscriptionsState>(UserSubscriptionsState.LOADING)
    val subscriptionIdList = MutableStateFlow(mutableListOf<Int>())
    private val activeSubscriptionsCount = MutableStateFlow(0)
    val showBanner = MutableStateFlow(false)

    fun displayStatus(
        subscriptionId: String,
        activityId: String
    ) {
        viewModelScope.launch {
            appAnalytics.logEvent("view_subscription_detail", null)
            if (state.value !is SubscriptionState.SUBSCRIBED) {
                state.tryEmit(SubscriptionState.LOADING)
            }
            withContext(appDispatcher.IO) {
                setStreaksSampleDate()
                val statusJob = async {
                    if (subscriptionId.isNotEmpty()) {
                        service.retrieveUserSubscriptionById(subscriptionId.toInt())
                    } else {
                        service.retrieveUserSubscriptionByActivityId(activityId.toInt())
                    }
                }
                val statusData = statusJob.await()
                statusData.onSuccess {
                    subscription = it
                    val goalsJob = async { service.getGoalsBySubscriptionId(it.id) }
                    getUserGoals(goalsJob, it)
                }.onFailure { e ->
                    Timber.e(e)
                    state.tryEmit(SubscriptionState.FAILURE(e.toUserError(resources), e is APIError.NetworkError))
                }
            }
        }
    }

    private suspend fun getUserGoals(
        goalJob: Deferred<Result<List<Goal>>>,
        subscription: Subscription
    ) {
        goalJob.await().fold(
            onSuccess = { goals ->
                val goalsList = goals.sortedWith(
                    compareBy(
                        { it.dueDate.isEmpty() },
                        { format.parseOrNull(it.dueDate) }
                    )
                )
                setSubscriptionStatusData(subscription)
                state.tryEmit(SubscriptionState.SUBSCRIBED(subscription, goalsList))
            },
            onFailure = { e ->
                Timber.e(e)
                state.tryEmit(SubscriptionState.FAILURE(e.toUserError(resources), e is APIError.NetworkError))
            }
        )
    }

    private fun setStreaksSampleDate() {
        val timeMillis = System.currentTimeMillis()
        sampleStreaksDate.tryEmit(
            getFormattedDayDate(timeMillis / 1000).first + " to " + getFormattedDayDate(
                timeMillis / 1000 + (DEMO_STREAK_PLUS_DAYS)
            ).first
        )
    }

    private fun setSubscriptionStatusData(
        subscription: Subscription
    ) {
        if (subscription.streaks.isNotEmpty()) {
            streaksDataList.value = subscription.streaks.toMutableList()
            maxStreak.value = streaksDataList.value[0].count
        }
    }

    fun managePopBack() {
        navManager.popBack()
    }

    fun onUnsubscribeClicked() {
        val subscription = subscription ?: return
        val subscriptionId = subscription.id
        viewModelScope.launch {
            state.tryEmit(SubscriptionState.LOADING)
            withContext(appDispatcher.IO) {
                service.unsubscribeActivity(subscriptionId)
                    .onSuccess {
                        eventBus.post(ActivitySubscriptionEvent(subscription.activity.id))
                        state.tryEmit(SubscriptionState.UNSUBSCRIBED)
                    }
                    .onFailure { e ->
                        Timber.e(e)
                        state.tryEmit(SubscriptionState.FAILURE(e.toUserError(resources), e is APIError.NetworkError))
                    }
            }
        }
    }

    fun getUserSubscriptions() {
        viewModelScope.launch {
            userSubscriptionState.tryEmit(UserSubscriptionsState.LOADING)
            withContext(appDispatcher.IO) {
                appAnalytics.logEvent("view_archived_subscription_detail")
                service.getUserSubscriptions().onSuccess { subscriptions ->
                    userSubscriptionState.tryEmit(UserSubscriptionsState.SUCCESS)
                    val listOfIds = ArrayList(subscriptionIdList.value)
                    subscriptions.forEach { items ->
                        listOfIds.add(items.activity.id)
                    }
                    subscriptionIdList.tryEmit(listOfIds)
                    activeSubscriptionsCount.tryEmit(
                        subscriptions.filter { subs ->
                            subs.is_active && !(subs.activity.id == DEFAULT_WAKE_UP_ACTIVITY_ID || subs.activity.id == DEFAULT_SLEEP_ACTIVITY_ID)
                        }.size
                    )
                }.onFailure { e ->
                    Timber.e(e)
                    e.localizedMessage?.let {
                        userSubscriptionState.tryEmit(UserSubscriptionsState.FAILURE(it))
                    }
                }
            }
        }
    }

    fun restoreOrDeleteArchivedActivity(subscriptionId: Int, type: Int) {
        viewModelScope.launch {
            archivedState.tryEmit(ArchivedState.LOADING)
            withContext(appDispatcher.IO) {
                if (type == TAB_RESTORE_ACTIVITY) {
                    appAnalytics.logEvent("tap_archived_subscription_restore_item")
                } else {
                    appAnalytics.logEvent("tap_archived_subscription_delete_item")
                }
                val restoreId = RestoreActivityBody(subscriptionId)
                val deleteActivityIds = DeleteActivityBody(listOf(subscriptionId))
                val stateJob = async {
                    if (type == TAB_RESTORE_ACTIVITY) {
                        service.restoreArchivedActivity(restoreId)
                    } else {
                        service.deleteArchivedActivity(deleteActivityIds)
                    }
                }
                val statusData = stateJob.await()
                statusData
                    .onSuccess {
                        archivedState.tryEmit(ArchivedState.SUCCESS)
                    }.onFailure { e ->
                        Timber.e(e)
                        e.localizedMessage?.let {
                            archivedState.tryEmit(ArchivedState.FAILURE(it))
                        }
                    }
            }
            if (archivedState.value == ArchivedState.SUCCESS) {
                navManager.popBack()
            }
        }
    }
    fun checkSubscriptionLimit(subscriptionId: Int, type: Int) {
        if (activeSubscriptionsCount.value >= MAX_SUBSCRIPTION_LIMIT && authManager.authState.isFree) {
            navigateToBuyPremiumView()
        } else {
            restoreOrDeleteArchivedActivity(subscriptionId, type)
        }
    }

    fun navigateToBuyPremiumView() {
        navManager.navigateToBuyPremiumScreen()
    }
    fun navigateToEditSubscription(subscriptionId: String) {
        appAnalytics.logEvent("tap_subscription_edit", null)
        navManager.navigateToEditSubscription(subscriptionId)
    }

    fun toggleUnsubscribeActivityAlert() {
        if (!showUnsubscribeAlert.value) {
            appAnalytics.logEvent("tap_subscription_unsubscribe", null)
        }
        showUnsubscribeAlert.value = !showUnsubscribeAlert.value
    }

    fun navigateToNotesView(
        activityName: String,
        activityId: String,
        notesId: Int?,
        isCustom: Boolean
    ) {
        appAnalytics.logEvent("tap_subscription_detail_notes_item")
        val id = notesId?.toString() ?: ""
        navManager.navigateToAddOrUpdateNotes(
            activityName,
            activityId = activityId,
            notesId = id,
            isCustom = isCustom
        )
    }

    fun navigateToNotesListView(activityId: Int, isCustom: Boolean, activityName: String) {
        appAnalytics.logEvent("tap_subscription_detail_view_all_notes")
        navManager.navigateToNoteListView(activityId, isCustom, activityName)
    }

    fun navigateToCreateCommunity() {
        navManager.navigateToAddCommunityScreen()
    }

    fun navigateToMyGoalsView(subscriptionId: Int) {
        navManager.navigateToMyGoalsScreen(subscriptionId)
    }

    fun getHighlightProgress(subscription: Subscription) {
        subscription.activity.highlights?.forEach {
            it.highlight_metrics.forEach { item ->
                currentReading.tryEmit(item.currentReading)
                previousReading.tryEmit(item.previousReading)
            }
        }
    }

    fun setAllTabProgressText(habitDetail: Subscription, showArchivedStatus: Boolean) {
        val currentMonth = allTimeFormat.format(System.currentTimeMillis())
        val activityStartMonth = allTimeFormat.format(habitDetail.start_date * 1000L)
        val activityEndMonth = allTimeFormat.format(habitDetail.end_date * 1000L)
        val data =
            if (activityStartMonth == activityEndMonth || activityStartMonth == currentMonth) {
                activityStartMonth
            } else if (showArchivedStatus) {
                "$activityStartMonth-$activityEndMonth"
            } else {
                "$activityStartMonth-$currentMonth"
            }
        allTimeProgressText.tryEmit(data)
    }

    fun handleProgressTabSelection(
        selectedTab: ProgressTabType,
        showArchivedStatus: Boolean = false
    ) {
        progressSelectedTab.tryEmit(selectedTab)
        when (progressSelectedTab.value) {
            ProgressTabType.PROGRESS_ALL -> {
                appAnalytics.logEvent(if (showArchivedStatus) "tap_archived_subscription_all" else "tap_subscription_detail_all", null)
            }
            ProgressTabType.PROGRESS_M -> {
                appAnalytics.logEvent(if (showArchivedStatus) "tap_archived_subscription_monthly" else "tap_subscription_detail_monthly", null)
            }
            ProgressTabType.PROGRESS_6M -> {
                appAnalytics.logEvent(if (showArchivedStatus) "tap_archived_subscription_6_months" else "tap_subscription_detail_6_months", null)
            }
            ProgressTabType.PROGRESS_Y -> {
                appAnalytics.logEvent(if (showArchivedStatus) "tap_archived_subscription_yearly" else "tap_subscription_detail_yearly", null)
            }
            else -> {
                appAnalytics.logEvent(if (showArchivedStatus) "tap_archived_subscription_7_days" else "tap_subscription_detail_7_days", null)
            }
        }
    }

    fun resetPastCompletionState() {
        pastCompletionState.tryEmit(PastCompletionState.START)
    }

    fun navigateToAddGoal(subscriptionId: Int) {
        navManager.navigateToAddGoal(subscriptionId)
    }

    fun navigateToEditGoal(goalId: Int, subscriptionId: Int) {
        navManager.navigateToEditGoal(subscriptionId, goalId)
    }
}
