package com.canopas.ui.activitystatus.yearlyprogress

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.activity.ProgressHeaderView
import com.canopas.base.ui.barchartessentials.YearlyColumnChartView
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Subscription
import com.canopas.ui.activitystatus.ProgressHistoryState
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.rememberPagerState

@OptIn(ExperimentalPagerApi::class)
@Composable
fun YearlyProgressView(
    habitDetail: Subscription,
    showArchivedStatus: Boolean
) {
    val viewModel = hiltViewModel<YearlyProgressViewModel>()
    val pagerState = rememberPagerState()
    val progressState by viewModel.progressState.collectAsState()
    val isLoadingState = progressState is ProgressHistoryState.LOADING
    val yearlyProgressText by viewModel.yearlyProgressText.collectAsState()
    val subscriptionYears by viewModel.subscriptionYears.collectAsState()
    val yearlyProgressList by viewModel.yearlyProgressList.collectAsState()
    val barChartData by viewModel.yearlyChartData.collectAsState()

    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(habitDetail, showArchivedStatus)
    })

    LaunchedEffect(key1 = pagerState.currentPage, block = {
        viewModel.onStateYearlyChanged(pagerState.currentPage, showArchivedStatus)
    })
    Column(
        modifier = Modifier
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(start = 20.dp, end = 20.dp, top = 28.dp),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            ProgressHeaderView(
                totalDays = yearlyProgressList.size,
                completedDays = yearlyProgressList.filter { it.completed }.size,
                isLoadingState = false,
                currentMonthName = yearlyProgressText
            )
            HorizontalPager(
                state = pagerState,
                count = subscriptionYears.size,
                reverseLayout = true,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 12.dp),
                itemSpacing = 20.dp,
            ) {
                Box(
                    modifier = Modifier
                        .wrapContentHeight()
                        .fillMaxWidth(),
                    contentAlignment = Alignment.Center
                ) {
                    if (isLoadingState) {
                        CircularProgressIndicator(color = ComposeTheme.colors.primary)
                    }
                    YearlyColumnChartView(barChartData, isLoadingState)
                }
            }
        }
    }
}
