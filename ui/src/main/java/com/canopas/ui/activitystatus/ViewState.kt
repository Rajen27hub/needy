package com.canopas.ui.activitystatus

import com.canopas.data.model.Goal
import com.canopas.data.model.Subscription

sealed class SubscriptionState {
    object LOADING : SubscriptionState()
    data class SUBSCRIBED(
        val habitStatus: Subscription,
        val goals: List<Goal>
    ) : SubscriptionState()

    object UNSUBSCRIBED : SubscriptionState()
    data class FAILURE(val message: String, val isNetworkError: Boolean) : SubscriptionState()
}

sealed class PastCompletionState {
    object START : PastCompletionState()
    object LOADING : PastCompletionState()
    object SUCCESS : PastCompletionState()
    data class FAILURE(val message: String) : PastCompletionState()
}

sealed class ProgressHistoryState {
    object LOADING : ProgressHistoryState()
    object PROCESSED : ProgressHistoryState()
    data class FAILURE(val message: String) : ProgressHistoryState()
}

sealed class ArchivedState {
    object LOADING : ArchivedState()
    object SUCCESS : ArchivedState()
    data class FAILURE(val message: String) : ArchivedState()
}

sealed class UserSubscriptionsState {
    object LOADING : UserSubscriptionsState()
    object SUCCESS : UserSubscriptionsState()
    data class FAILURE(val message: String) : UserSubscriptionsState()
}
