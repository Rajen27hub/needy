package com.canopas.ui.activitystatus

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TextButton
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.AddCircle
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.data.utils.Utils
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.StatusTheme
import com.canopas.data.model.Notes
import com.canopas.data.model.Subscription
import com.canopas.ui.R
import com.canopas.ui.utils.DateUtils

@Suppress("FunctionName")
fun ManageNotesView(
    viewModel: SubscriptionStatusViewModel,
    habitDetail: Subscription,
    showArchivedStatus: Boolean,
    lazyListScope: LazyListScope
) {
    if (showArchivedStatus && !habitDetail.notes.isNullOrEmpty()) {
        lazyListScope.item {
            NotesHeaderComposable(viewModel, habitDetail, true)
        }
        lazyListScope.items(habitDetail.notes!!) { item ->
            NotesListComposable(item, viewModel, habitDetail)
        }
    } else {
        if (!showArchivedStatus) {
            lazyListScope.item {
                NotesHeaderComposable(viewModel, habitDetail, false)
            }

            if (habitDetail.notes.isNullOrEmpty()) {
                lazyListScope.item {
                    EmptyNotesComposable()
                }
            } else {
                lazyListScope.items(habitDetail.notes!!) { item ->
                    NotesListComposable(item, viewModel, habitDetail)
                }
            }
        }
    }
}

@Composable
fun NotesHeaderComposable(
    viewModel: SubscriptionStatusViewModel,
    habitDetail: Subscription,
    showArchivedStatus: Boolean
) {
    Column(
        Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .background(ComposeTheme.colors.background)
            .padding(top = 40.dp),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            modifier = Modifier.padding(horizontal = 20.dp),
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Row(
                modifier = Modifier.weight(1f), verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    text = stringResource(R.string.subscription_status_notes_title_text),
                    style = AppTheme.typography.h2TextStyle,
                    color = ComposeTheme.colors.textPrimary
                )

                if (!showArchivedStatus) {
                    IconButton(onClick = {
                        viewModel.navigateToNotesView(
                            Utils.encodeToBase64(habitDetail.activity.name),
                            habitDetail.activity.id.toString(),
                            habitDetail.note?.id,
                            habitDetail.is_custom
                        )
                    }) {
                        Icon(
                            imageVector = Icons.Outlined.AddCircle,
                            tint = ComposeTheme.colors.primary,
                            contentDescription = "add notes icon",
                            modifier = Modifier.size(28.dp)
                        )
                    }
                }
            }

            TextButton(
                onClick = {
                    viewModel.navigateToNotesListView(
                        habitDetail.activity_id,
                        habitDetail.is_custom,
                        Utils.encodeToBase64(habitDetail.activity.name)
                    )
                },
                modifier = Modifier.motionClickEvent { },
            ) {
                Text(
                    text = stringResource(id = R.string.subscription_status_view_all_text),
                    style = AppTheme.typography.buttonStyle,
                    textAlign = TextAlign.End,
                    color = ComposeTheme.colors.primary
                )
            }
        }
    }
}

@Composable
fun NotesListComposable(
    item: Notes,
    viewModel: SubscriptionStatusViewModel,
    habitDetail: Subscription
) {
    val formattedDay = DateUtils.getLongFormat(item.createdAt)
    val noteColor = DateUtils.getNotesColor(item.createdAt, item.id).first
    val notesBorderColor = DateUtils.getNotesColor(item.createdAt, item.id).second

    Box(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(horizontal = 20.dp, vertical = 12.dp)
            .motionClickEvent {
                viewModel.navigateToNotesView(
                    Utils.encodeToBase64(habitDetail.activity.name),
                    habitDetail.activity.id.toString(),
                    item.id,
                    habitDetail.is_custom
                )
            }
            .background(
                noteColor,
                shape = RoundedCornerShape(15.dp)
            )
            .border(
                BorderStroke(1.dp, notesBorderColor), RoundedCornerShape(16)
            )
            .wrapContentHeight()
            .clip(shape = RoundedCornerShape(15.dp))
    ) {
        Column(
            modifier = Modifier.padding(14.dp),
            verticalArrangement = Arrangement.SpaceBetween,
            horizontalAlignment = Alignment.Start
        ) {
            Text(
                text = formattedDay,
                style = AppTheme.typography.notesDateStyle,
                color = ComposeTheme.colors.textSecondary
            )

            Text(
                text = item.content,
                style = AppTheme.typography.subTitleTextStyle1,
                color = ComposeTheme.colors.textPrimary,
                maxLines = 3,
                overflow = TextOverflow.Ellipsis
            )
        }
    }
}

@Composable
fun EmptyNotesComposable() {
    Box(
        modifier = Modifier.background(ComposeTheme.colors.background),
        contentAlignment = Alignment.Center
    ) {
        Text(
            text = stringResource(id = R.string.subscription_status_empty_notes_text),
            style = AppTheme.typography.subTitleTextStyle1,
            lineHeight = 22.sp,
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(
                    horizontal = 32.dp, vertical = 40.dp
                ),
            textAlign = TextAlign.Center,
            color = ComposeTheme.colors.textSecondary
        )
    }
}

@Composable
fun ShareActivityView(viewModel: SubscriptionStatusViewModel) {

    Divider(
        Modifier
            .background(ComposeTheme.colors.background)
            .padding(horizontal = 20.dp, vertical = 40.dp)
            .fillMaxWidth(),
        thickness = 1.dp,
        color = StatusTheme.SubscriptionColors.dividerColor
    )

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(ComposeTheme.colors.background),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .wrapContentHeight()
                .padding(start = 20.dp, end = 20.dp, bottom = 60.dp)
                .background(
                    StatusTheme.SubscriptionColors.shareYourProgressBg,
                    RoundedCornerShape(16.dp)
                ),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(top = 20.dp)
            ) {
                Image(
                    painter = painterResource(id = R.drawable.share_activities_group_image),
                    contentDescription = null,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(horizontal = 14.dp)
                        .aspectRatio(5.6f)
                        .align(Alignment.TopCenter),
                    contentScale = ContentScale.FillWidth
                )
            }
            Text(
                text = stringResource(R.string.subscription_status_share_activity_title_text),
                style = AppTheme.typography.tabTextStyle,
                color = ComposeTheme.colors.textPrimary,
                textAlign = TextAlign.Center,
                lineHeight = 22.sp,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        bottom = 6.dp
                    )
            )
            Text(
                text = stringResource(R.string.subscription_status_share_activity_body_text),
                style = AppTheme.typography.bodyTextStyle2,
                color = ComposeTheme.colors.textSecondary,
                textAlign = TextAlign.Center,
                lineHeight = 17.sp,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        start = 56.dp, end = 56.dp, bottom = 24.dp
                    )
            )
            PrimaryButton(
                onClick = {
                    viewModel.navigateToCreateCommunity()
                },
                text = stringResource(R.string.subscription_status_share_activity_btn_text),
                modifier = Modifier
                    .wrapContentWidth()
                    .motionClickEvent { }
                    .padding(bottom = 36.dp),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = ComposeTheme.colors.primary, contentColor = Color.White
                ),
                textModifier = Modifier.padding(horizontal = 40.dp)
            )
        }
    }
}
