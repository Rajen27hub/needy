package com.canopas.ui.activitystatus.monthlyprogress

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.Utils
import com.canopas.base.ui.White
import com.canopas.base.ui.activity.DayView
import com.canopas.base.ui.activity.DaysOfWeekHeader
import com.canopas.base.ui.activity.ProgressHeaderView
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Subscription
import com.canopas.ui.activitystatus.ProgressHistoryState
import com.kizitonwose.calendar.compose.HorizontalCalendar
import com.kizitonwose.calendar.compose.rememberCalendarState
import com.kizitonwose.calendar.core.OutDateStyle
import com.kizitonwose.calendar.core.daysOfWeek
import java.time.LocalDate
import java.time.YearMonth
import java.time.temporal.ChronoUnit

@Composable
fun MonthlyProgressView(
    habitDetail: Subscription,
    showArchivedStatus: Boolean
) {
    val viewModel = hiltViewModel<MonthlyProgressViewModel>()
    val activityEndDate =
        LocalDate.parse(Utils.format.format(habitDetail.end_date * 1000L).toString())
    val currentDate = LocalDate.parse(Utils.format.format(System.currentTimeMillis()))
    val archivedMonthsBetween: Long = ChronoUnit.MONTHS.between(
        activityEndDate.withDayOfMonth(1),
        currentDate
    )
    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(habitDetail, showArchivedStatus, archivedMonthsBetween)
    })

    val progressState by viewModel.progressState.collectAsState()
    val isLoadingState = progressState is ProgressHistoryState.LOADING
    Column(
        modifier = Modifier
            .widthIn(max = 600.dp)
            .fillMaxWidth()
            .padding(
                top = 28.dp,
                start = 20.dp,
                end = 20.dp
            ),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        val completedDaysList by viewModel.completedDaysList.collectAsState()
        val missedDaysList by viewModel.missedDaysList.collectAsState()
        val durationText by viewModel.durationText.collectAsState()
        val totalDays by viewModel.totalDays.collectAsState()
        val completedDays by viewModel.completedDays.collectAsState()
        val currentVisibleMonth by viewModel.currentVisibleMonth.collectAsState()
        val activityStartDate =
            LocalDate.parse(Utils.format.format(habitDetail.start_date * 1000L))
        val currentMonth = remember { YearMonth.now() }
        val monthsBetween: Long = ChronoUnit.MONTHS.between(
            activityStartDate.withDayOfMonth(1),
            currentDate
        )
        val calenderStartMonth =
            remember { currentMonth.minusMonths(monthsBetween) }
        val calenderEndMonth =
            remember { if (showArchivedStatus) currentMonth.minusMonths(archivedMonthsBetween) else currentMonth }
        val daysOfWeekHeader = remember { daysOfWeek() }

        val state = rememberCalendarState(
            startMonth = calenderStartMonth,
            endMonth = calenderEndMonth,
            firstVisibleMonth = if (showArchivedStatus) calenderStartMonth else calenderEndMonth,
            firstDayOfWeek = daysOfWeekHeader.first(),
            outDateStyle = OutDateStyle.EndOfRow
        )
        LaunchedEffect(
            key1 = state.firstVisibleMonth,
            key2 = state.isScrollInProgress,
            key3 = Unit,
            block = {
                if (!state.isScrollInProgress && state.firstVisibleMonth.yearMonth != currentVisibleMonth) {
                    viewModel.onStateMonthChanged(
                        state.firstVisibleMonth.yearMonth,
                        habitDetail.end_date,
                        showArchivedStatus
                    )
                }
            }
        )

        Column(
            modifier = Modifier
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Top,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            ProgressHeaderView(
                totalDays = totalDays,
                completedDays = completedDays,
                isLoadingState = isLoadingState,
                currentMonthName = durationText
            )
            DaysOfWeekHeader(daysOfWeek = daysOfWeekHeader)
            HorizontalCalendar(
                state = state,
                dayContent = { day ->
                    if (viewModel.checkLastRowCondition(day, state)) {
                        Box(
                            modifier = Modifier
                                .aspectRatio(1f)
                                .padding(vertical = 7.dp)
                                .background(White),
                            contentAlignment = Alignment.Center
                        ) {
                            Text(
                                text = "",
                                style = AppTheme.typography.buttonStyle,
                                lineHeight = 15.sp,
                                color = White
                            )
                        }
                    } else {
                        if (isLoadingState && day == state.firstVisibleMonth.weekDays[2][3]) {
                            Box(
                                contentAlignment = Alignment.Center,
                                modifier = Modifier.fillMaxSize()
                            ) {
                                CircularProgressIndicator(color = ComposeTheme.colors.primary)
                            }
                        } else {
                            DayView(
                                day,
                                currentDate = currentDate,
                                startDate = activityStartDate,
                                endDate = activityEndDate,
                                completedDays = completedDaysList,
                                missedDays = missedDaysList
                            )
                        }
                    }
                },
            )
        }
    }
}
