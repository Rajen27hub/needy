package com.canopas.ui.activitystatus

import androidx.activity.compose.BackHandler
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.core.tween
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxWithConstraints
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.ModalBottomSheetLayout
import androidx.compose.material.ModalBottomSheetValue
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.MoreVert
import androidx.compose.material.rememberModalBottomSheetState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.data.model.StreaksData
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.CircularGradientProgressView
import com.canopas.base.ui.SignOutTextColor
import com.canopas.base.ui.activity.CompletionRateViewGraphs
import com.canopas.base.ui.activity.ProgressHeaderView
import com.canopas.base.ui.activity.SubscriptionProgressTabView
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.customview.NetworkErrorView
import com.canopas.base.ui.customview.ServerErrorView
import com.canopas.base.ui.customview.TopAppBarContent
import com.canopas.base.ui.model.ProgressTabType
import com.canopas.base.ui.rememberForeverLazyListState
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.base.ui.themes.StatusTheme
import com.canopas.base.ui.themes.StatusTheme.SubscriptionColors
import com.canopas.data.FeatureFlag.isGoalsEnabled
import com.canopas.data.model.Goal
import com.canopas.data.model.Subscription
import com.canopas.ui.R
import com.canopas.ui.activitystatus.monthlyprogress.MonthlyProgressView
import com.canopas.ui.activitystatus.recentprogress.RecentProgressView
import com.canopas.ui.activitystatus.sixmonthlyprogress.SixMonthlyProgressView
import com.canopas.ui.activitystatus.yearlyprogress.YearlyProgressView
import kotlinx.coroutines.launch

@Composable
fun SubscriptionStatusView(
    subscriptionId: String = "",
    activityId: String = "",
    headerText: String = "",
    showArchivedStatus: Boolean = false
) {

    val viewModel = hiltViewModel<SubscriptionStatusViewModel>()
    val statusState by viewModel.state.collectAsState()

    LaunchedEffect(key1 = subscriptionId, key2 = activityId) {
        viewModel.displayStatus(subscriptionId, activityId)
    }
    LaunchedEffect(key1 = Unit) {
        if (showArchivedStatus) {
            viewModel.getUserSubscriptions()
        }
    }

    val context = LocalContext.current
    val completionState by viewModel.pastCompletionState.collectAsState()
    val failureCompletionState = completionState is PastCompletionState.FAILURE

    LaunchedEffect(key1 = completionState) {
        if (failureCompletionState) {
            val message = (completionState as PastCompletionState.FAILURE).message
            showBanner(context, message)
            viewModel.resetPastCompletionState()
        }
    }

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        statusState.let { state ->
            when (state) {
                SubscriptionState.LOADING -> {
                    Box(
                        contentAlignment = Alignment.Center,
                        modifier = Modifier
                            .fillMaxSize()
                            .background(colors.background)
                    ) {
                        CircularProgressIndicator(color = colors.primary)
                    }
                }
                is SubscriptionState.SUBSCRIBED -> {
                    val habitStatus = state.habitStatus
                    val goals = state.goals
                    StatusTheme(darkTheme = isDarkMode) {
                        StatusSuccessView(
                            viewModel,
                            habitStatus,
                            headerText,
                            showArchivedStatus,
                            goals
                        )
                    }
                }
                is SubscriptionState.FAILURE -> {
                    if (state.isNetworkError) {
                        NetworkErrorView {
                            viewModel.displayStatus(subscriptionId, activityId)
                        }
                    } else {
                        ServerErrorView {
                            viewModel.displayStatus(subscriptionId, activityId)
                        }
                    }
                }
                is SubscriptionState.UNSUBSCRIBED -> {
                    LaunchedEffect(key1 = Unit) {
                        viewModel.managePopBack()
                    }
                }
            }
        }
    }
}

@OptIn(ExperimentalMaterialApi::class, ExperimentalFoundationApi::class)
@Composable
fun StatusSuccessView(
    viewModel: SubscriptionStatusViewModel,
    habitDetail: Subscription,
    headerText: String,
    showArchivedStatus: Boolean,
    goals: List<Goal>
) {
    val showUnsubscribeAlert by viewModel.showUnsubscribeAlert.collectAsState()
    val currentSelectedTab by viewModel.progressSelectedTab.collectAsState()
    if (showUnsubscribeAlert) {
        ShowUnsubscribeActivityDialog(
            viewModel, habitDetail.activity.name
        )
    }
    val topBarColor by remember { mutableStateOf(GreenSubscriptionCardBg) }

    val sheetState = rememberModalBottomSheetState(
        initialValue = ModalBottomSheetValue.Hidden,
        confirmStateChange = { it != ModalBottomSheetValue.HalfExpanded }
    )
    val coroutineScope = rememberCoroutineScope()

    BackHandler(sheetState.isVisible) {
        coroutineScope.launch { sheetState.hide() }
    }

    LaunchedEffect(key1 = showUnsubscribeAlert, key2 = Unit, block = {
        if (showUnsubscribeAlert || sheetState.isVisible) {
            coroutineScope.launch {
                sheetState.hide()
            }
        }
    })

    ModalBottomSheetLayout(
        sheetState = sheetState,
        sheetContent = {
            BottomSheetItemMenu(
                viewModel,
                showArchivedStatus,
                habitDetail
            )
        },
        modifier = Modifier.fillMaxSize()
    ) {
        Scaffold(
            topBar = {
                TopAppBar(
                    backgroundColor = if (habitDetail.progress.recent.isEmpty() && !isDarkMode) topBarColor else colors.background,
                    contentColor = colors.textPrimary,
                    elevation = 0.dp,
                    content = {
                        TopAppBarContent(
                            title = habitDetail.activity.name.trim(),
                            navigationIconOnClick = { viewModel.managePopBack() },
                            actions = {
                                IconButton(onClick = {
                                    coroutineScope.launch {
                                        if (sheetState.isVisible) sheetState.hide()
                                        else sheetState.show()
                                    }
                                }) {
                                    Icon(
                                        imageVector = Icons.Outlined.MoreVert,
                                        contentDescription = "More Menu",
                                        tint = colors.textPrimary
                                    )
                                }
                            }
                        )
                    }
                )
            }
        ) {
            LazyColumn(
                state = rememberForeverLazyListState(key = habitDetail.id.toString()),
                modifier = Modifier
                    .fillMaxSize()
                    .background(colors.background)
                    .padding(it),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {

                item {
                    if (habitDetail.progress.recent.isNotEmpty()) {
                        SubscriptionProgressTabView({ tabType ->
                            viewModel.handleProgressTabSelection(
                                tabType,
                                showArchivedStatus
                            )
                        }, currentSelectedTab)
                    } else {
                        Box(
                            modifier = Modifier
                                .fillMaxWidth()
                                .background(SubscriptionColors.statusBarBg)
                                .wrapContentHeight()
                        ) {

                            val historyText =
                                if (showArchivedStatus) stringResource(R.string.archived_activities_list_no_completions_text) else stringResource(
                                    id = R.string.subscription_status_chart_title_just_started_text
                                )
                            Text(
                                text = if (headerText.isNotEmpty()) stringResource(
                                    R.string.subscription_status_congratulations_header_text
                                ) else historyText,
                                style = AppTheme.typography.statusScreenHeadingTextStyle,
                                lineHeight = 36.sp,
                                color = SubscriptionColors.headerText,
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(bottom = 12.dp, top = 40.dp),
                                textAlign = TextAlign.Center
                            )
                        }
                    }
                }

                item {
                    if (habitDetail.progress.recent.isEmpty()) {
                        JustStartedActivityView(habitDetail, headerText)
                    } else {
                        when (currentSelectedTab) {
                            ProgressTabType.PROGRESS_7D -> {
                                RecentProgressView(habitDetail, showArchivedStatus)
                            }
                            ProgressTabType.PROGRESS_M -> {
                                MonthlyProgressView(habitDetail, showArchivedStatus)
                            }
                            ProgressTabType.PROGRESS_6M -> {
                                SixMonthlyProgressView(habitDetail, showArchivedStatus)
                            }
                            ProgressTabType.PROGRESS_Y -> {
                                YearlyProgressView(habitDetail, showArchivedStatus)
                            }
                            else -> {
                                AllProgressView(habitDetail, showArchivedStatus, viewModel)
                            }
                        }
                    }
                }

                if (isGoalsEnabled && !showArchivedStatus) {
                    BuildGoalsView(viewModel, goals, habitDetail, this)
                }

                item {
                    if (currentSelectedTab != ProgressTabType.PROGRESS_ALL && habitDetail.activity.highlights != null) {
                        HighlightsStateView(viewModel = viewModel, habitDetail)
                    }
                }

                item {
                    Column(
                        Modifier
                            .widthIn(max = 600.dp)
                            .fillMaxWidth()
                            .background(colors.background)
                            .animateItemPlacement(
                                tween(
                                    durationMillis = 600,
                                    easing = LinearEasing
                                )
                            ),
                        horizontalAlignment = Alignment.CenterHorizontally
                    ) {
                        YourStreakView(viewModel, showArchivedStatus)
                    }
                }
                ManageNotesView(viewModel, habitDetail, showArchivedStatus, this)

                item {
                    if (!showArchivedStatus) {
                        ShareActivityView(viewModel)
                    }
                }
            }
        }
    }
}

@Composable
fun AllProgressView(
    habitDetail: Subscription,
    showArchivedStatus: Boolean,
    viewModel: SubscriptionStatusViewModel
) {
    LaunchedEffect(key1 = Unit, block = {
        viewModel.setAllTabProgressText(habitDetail, showArchivedStatus)
    })
    val allTimeProgressText by viewModel.allTimeProgressText.collectAsState()
    val totalDays by remember {
        mutableStateOf(habitDetail.progress.total.completed_days + habitDetail.progress.total.missed_days)
    }
    val completedDays by remember {
        mutableStateOf(habitDetail.progress.total.completed_days)
    }

    BoxWithConstraints {
        val possibleSize = maxWidth.times(0.25f).plus(100.dp)
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .padding(
                    top = 28.dp,
                    start = 20.dp,
                    end = 20.dp
                ),
            verticalArrangement = Arrangement.Top
        ) {
            ProgressHeaderView(
                totalDays = totalDays,
                completedDays = completedDays,
                isLoadingState = false,
                currentMonthName = allTimeProgressText,
                showProgressView = false
            )
            CircularGradientProgressView(
                boxModifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.CenterHorizontally)
                    .size(possibleSize),
                strokeWidth = 16.dp,
                showText = true,
                strokeBackgroundWidth = 2.dp,
                progress = ((completedDays * 100) / totalDays).toFloat()
            )
        }
    }
}

@Composable
fun ShowUnsubscribeActivityDialog(viewModel: SubscriptionStatusViewModel, activity: String) {
    CustomAlertDialog(
        title = null,
        subTitle = stringResource(
            R.string.subscription_status_alert_dialog_message_text, activity
        ),
        confirmBtnText = stringResource(id = R.string.subscription_status_alert_btn_stop_journey_text),
        dismissBtnText = stringResource(id = R.string.subscription_status_cancel_btn_text),
        confirmBtnColor = SignOutTextColor,
        onConfirmClick = { viewModel.onUnsubscribeClicked() },
        onDismissClick = { viewModel.toggleUnsubscribeActivityAlert() }
    )
}

@Preview
@Composable
fun PreviewCompletionRateView() {
    val streaksData = StreaksData(1641340890, 1641340821, 5, true)
    CompletionRateViewGraphs(streakData = streaksData, maxProgress = 5)
}

private val GreenSubscriptionCardBg = Color(0xFFE6F0DE)
