package com.canopas.ui.activitystatus

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.ExploreTodoCardBg
import com.canopas.base.ui.Utils
import com.canopas.base.ui.activity.CompletionRateViewGraphs
import com.canopas.base.ui.activity.highlights.HighlightsView
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.base.ui.themes.StatusTheme
import com.canopas.data.model.Subscription
import com.canopas.ui.R
import java.util.Calendar
import kotlin.math.roundToInt

@Composable
fun HighlightsStateView(
    viewModel: SubscriptionStatusViewModel,
    habitDetail: Subscription
) {
    LaunchedEffect(key1 = Unit) {
        viewModel.getHighlightProgress(habitDetail)
    }
    val currentProgress = viewModel.currentReading.collectAsState()
    val previousProgress = viewModel.previousReading.collectAsState()
    val highlightStatus =
        if (currentProgress.value.roundToInt() == previousProgress.value.roundToInt()) {
            stringResource(R.string.highlight_same_progress_text)
        } else if (currentProgress.value.roundToInt() > previousProgress.value.roundToInt()) {
            stringResource(R.string.highlight_more_progress_text)
        } else {
            stringResource(R.string.highlight_less_progress_text)
        }
    Column(
        Modifier.padding(top = 40.dp)
            .widthIn(max = 600.dp)
            .fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        val now = Calendar.getInstance()
        val currentMonth = Utils.monthFormat.format(now.time)
        now.set(Calendar.DAY_OF_MONTH, 1)
        now.set(Calendar.HOUR_OF_DAY, 0)
        now.set(Calendar.SECOND, 0)
        now.set(Calendar.MINUTE, -1)
        val previousMonth = Utils.monthFormat.format(now.time)

        val descriptionText = if (currentProgress.value.roundToInt() == previousProgress.value.roundToInt()) {
            stringResource(
                id = R.string.subscription_status_highlights_description_text_2,
                currentMonth,
                previousMonth
            )
        } else {
            stringResource(
                id = R.string.subscription_status_highlights_description_text_1,
                highlightStatus,
                currentMonth,
                previousMonth
            )
        }

        HighlightsView(
            currentMonth,
            previousMonth,
            currentProgress.value,
            previousProgress.value,
            descriptionText
        )
    }
}

@Composable
fun YourStreakView(viewModel: SubscriptionStatusViewModel, showArchivedStatus: Boolean) {
    val streaksData by viewModel.streaksDataList.collectAsState()
    val streaksSampleDate by viewModel.sampleStreaksDate.collectAsState()

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.Start
    ) {

        if (!showArchivedStatus || streaksData.isNotEmpty()) {
            Text(
                text = stringResource(R.string.subscription_status_view_your_top_streaks_title_text),
                style = AppTheme.typography.h2TextStyle,
                color = ComposeTheme.colors.textPrimary,
                textAlign = TextAlign.Start,
                modifier = Modifier
                    .padding(
                        top = 40.dp, start = 20.dp, end = 20.dp
                    )
                    .fillMaxWidth()
            )
        }
        if (streaksData.size > 0) {
            val maxProgress by viewModel.maxStreak.collectAsState()
            streaksData.forEach { data ->
                CompletionRateViewGraphs(
                    data, maxProgress
                )
            }
        } else {
            if (!showArchivedStatus) {
                Column(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentHeight()
                        .padding(start = 20.dp, end = 20.dp, top = 16.dp)
                        .background(
                            if (ComposeTheme.isDarkMode) StatusTheme.SubscriptionColors.statusSubtitleTextColor else
                                ExploreTodoCardBg,
                            RoundedCornerShape(16.dp)
                        )
                ) {
                    Text(
                        text = stringResource(R.string.subscription_status_empty_streaks_text),
                        style = AppTheme.typography.subTextStyle1,
                        color = ComposeTheme.colors.textPrimary,
                        textAlign = TextAlign.Start,
                        lineHeight = 22.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                top = 28.dp,
                                start = 16.dp,
                                end = 14.dp,
                                bottom = 20.dp
                            )
                    )
                    Text(
                        text = streaksSampleDate,
                        style = AppTheme.typography.bodyTextStyle3,
                        color = ComposeTheme.colors.textSecondary,
                        textAlign = TextAlign.Start,
                        lineHeight = 17.sp,
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(
                                start = 16.dp, bottom = 8.dp
                            )
                    )
                    Row(
                        modifier = Modifier
                            .fillMaxWidth()
                            .padding(start = 16.dp, bottom = 36.dp),
                        horizontalArrangement = Arrangement.Start,
                        verticalAlignment = Alignment.CenterVertically
                    ) {

                        Card(
                            modifier = Modifier
                                .weight(0.5f)
                                .height(12.dp)
                                .clip(RoundedCornerShape(25.dp)),
                            backgroundColor = StatusTheme.SubscriptionColors.emptyStreaksColor,
                            shape = RoundedCornerShape(25.dp)
                        ) {}

                        Row(
                            modifier = Modifier.weight(0.5f),
                            verticalAlignment = Alignment.CenterVertically,
                            horizontalArrangement = Arrangement.Start
                        ) {
                            Icon(
                                painter = painterResource(id = R.drawable.ic_streaks),
                                contentDescription = "Streaks",
                                modifier = Modifier
                                    .padding(
                                        start = 8.dp, end = 4.dp
                                    )
                                    .clip(CircleShape)
                                    .size(20.dp),
                                tint = StatusTheme.SubscriptionColors.emptyStreaksIconColor
                            )

                            Text(
                                text = stringResource(R.string.subscription_status_empty_streaks_text_3),
                                style = AppTheme.typography.captionTextStyle,
                                color = ComposeTheme.colors.textPrimary,
                            )
                        }
                    }
                }
            }
        }
    }
}
