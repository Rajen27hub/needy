package com.canopas.ui.activitystatus

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.Divider
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.showBanner
import com.canopas.base.ui.themes.ComposeTheme
import com.canopas.data.model.Subscription
import com.canopas.ui.R

@Composable
fun BottomSheetItemMenu(
    viewModel: SubscriptionStatusViewModel,
    showArchivedStatus: Boolean,
    subscription: Subscription
) {
    Column(
        modifier = Modifier
            .wrapContentSize()
            .background(ComposeTheme.colors.background)
    ) {
        val context = LocalContext.current
        val subscriptionIdList by viewModel.subscriptionIdList.collectAsState()
        val dividerColor = if (ComposeTheme.isDarkMode) darkBottomSheetDivider else lightBottomSheetDivider

        if (!showArchivedStatus) {
            BottomSheetItems(
                painter = painterResource(id = R.drawable.ic_change_schedule),
                text = stringResource(R.string.subscription_status_change_schedule_text)
            ) {
                viewModel.navigateToEditSubscription(subscription.id.toString())
            }

            Divider(
                Modifier.fillMaxWidth(), thickness = 1.dp, color = dividerColor
            )

            BottomSheetItems(
                painter = painterResource(id = R.drawable.ic_change_reminder),
                text = stringResource(R.string.subscription_status_change_reminder_text)
            ) {
                viewModel.navigateToEditSubscription(subscription.id.toString())
            }

            Divider(
                Modifier.fillMaxWidth(), thickness = 1.dp, color = dividerColor
            )

            BottomSheetItems(
                painter = painterResource(id = R.drawable.ic_stop_journey),
                text = stringResource(R.string.subscription_status_btn_stop_journey)
            ) {
                viewModel.toggleUnsubscribeActivityAlert()
            }
        } else {
            val message = stringResource(R.string.restore_activity_message)
            BottomSheetItems(
                painter = painterResource(id = R.drawable.ic_restore_activity_icon),
                text = stringResource(R.string.subscription_status_restore_activity_text)
            ) {
                if (subscription.activity.id in subscriptionIdList) {
                    viewModel.managePopBack()
                    showBanner(context, message)
                } else {
                    viewModel.checkSubscriptionLimit(
                        subscription.id, TAB_RESTORE_ACTIVITY
                    )
                }
            }

            Divider(
                Modifier.fillMaxWidth(), thickness = 1.dp, color = dividerColor
            )

            BottomSheetItems(
                painter = painterResource(id = R.drawable.ic_delete_activity_icon),
                text = stringResource(R.string.subscription_status_delete_activity_text)
            ) {
                viewModel.restoreOrDeleteArchivedActivity(
                    subscription.id, TAB_DELETE_ACTIVITY
                )
            }
        }
    }
}

@Composable
fun BottomSheetItems(painter: Painter, text: String, onClick: () -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .motionClickEvent { onClick() }
            .padding(horizontal = 24.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.spacedBy(21.dp)
    ) {
        Icon(
            painter = painter,
            contentDescription = "",
            tint = ComposeTheme.colors.textPrimary,
            modifier = Modifier.padding(vertical = 24.dp)
        )
        Text(
            text = text,
            style = AppTheme.typography.tabTextStyle,
            color = ComposeTheme.colors.textPrimary,
        )
    }
}

private val darkBottomSheetDivider = Color(0xFF313131)
private val lightBottomSheetDivider = Color(0x14000000)
