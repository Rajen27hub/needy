package com.canopas.ui.notificationPermission

import android.content.Intent
import android.content.Intent.CATEGORY_DEFAULT
import android.content.Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
import android.content.Intent.FLAG_ACTIVITY_NEW_TASK
import android.content.Intent.FLAG_ACTIVITY_NO_HISTORY
import android.net.Uri
import android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import com.canopas.base.ui.AppTheme
import com.canopas.base.ui.NotificationDialogButtonColor
import com.canopas.base.ui.White
import com.canopas.base.ui.customview.CustomAlertDialog
import com.canopas.base.ui.customview.PrimaryButton
import com.canopas.base.ui.motionClickEvent
import com.canopas.base.ui.textColor
import com.canopas.base.ui.themes.ComposeTheme.colors
import com.canopas.base.ui.themes.ComposeTheme.isDarkMode
import com.canopas.ui.R

@Composable
fun NotificationPermissionView() {
    val viewModel = hiltViewModel<NotificationPermissionViewModel>()
    val context = LocalContext.current
    val scrollState = rememberScrollState()
    val navigateToSettingsEvent by viewModel.navigateToAppSettings.collectAsState()
    val navigateToSettings = navigateToSettingsEvent.getContentIfNotHandled() ?: false

    BackHandler(enabled = true, onBack = {
        // Do Nothing
        // Block user from using System's Back Action
    })

    if (navigateToSettings) {
        val settingsIntent = Intent(ACTION_APPLICATION_DETAILS_SETTINGS)
        with(settingsIntent) {
            data = Uri.fromParts("package", context.packageName, null)
            addCategory(CATEGORY_DEFAULT)
            addFlags(FLAG_ACTIVITY_NEW_TASK)
            addFlags(FLAG_ACTIVITY_NO_HISTORY)
            addFlags(FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS)
        }
        context.startActivity(settingsIntent)
    }

    val openNotificationDialog = viewModel.openNotificationDialog.collectAsState()

    if (openNotificationDialog.value) {
        ShowNotificationDialog(viewModel)
    }

    Column(
        modifier = Modifier
            .background(colors.background)
            .fillMaxSize(),
        verticalArrangement = Arrangement.SpaceBetween,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
                .weight(0.5f)
                .padding(top = 24.dp, start = 20.dp, end = 20.dp)
                .verticalScroll(scrollState)
        ) {
            Text(
                text = stringResource(R.string.notifications_view_header_text),
                style = AppTheme.typography.h1TextStyle,
                lineHeight = 40.sp,
                letterSpacing = -(1.12.sp),
                color = colors.textPrimary
            )
            Image(
                painter = if (isDarkMode) painterResource(R.drawable.notification_on_smartphone_dark) else painterResource(
                    R.drawable.notification_on_smartphone
                ),
                contentDescription = "notification image",
                modifier = Modifier
                    .fillMaxWidth()
                    .aspectRatio(1f)
                    .padding(top = 20.dp, bottom = 28.dp)
                    .align(Alignment.CenterHorizontally)
            )
            Text(
                text = stringResource(R.string.notifications_view_sub_header_text),
                style = AppTheme.typography.subTextStyle,
                lineHeight = 26.sp,
                color = colors.textSecondary
            )
            Text(
                text = stringResource(R.string.notifications_view_sub_header_text_part_two),
                style = AppTheme.typography.subTextStyle,
                lineHeight = 26.sp,
                color = colors.textSecondary,
                modifier = Modifier.padding(top = 24.dp, bottom = 40.dp)
            )
        }

        Box(
            Modifier
                .offset(y = -(20.dp))
                .height(20.dp)
                .fillMaxWidth()
        ) {
            Box(
                Modifier
                    .fillMaxSize()
                    .background(
                        Brush.verticalGradient(
                            listOf(
                                Color.Transparent,
                                colors.background
                            )
                        )
                    )
            )
        }
        Column(
            modifier = Modifier
                .widthIn(max = 600.dp)
                .fillMaxWidth()
        ) {
            val buttonModifier = Modifier
                .align(alignment = Alignment.CenterHorizontally)
                .fillMaxWidth()
                .motionClickEvent { }

            PrimaryButton(
                onClick = {
                    viewModel.onContinueButtonClick()
                },
                shape = RoundedCornerShape(50),
                text = stringResource(R.string.notification_view_continue_btn_text),
                colors = ButtonDefaults.buttonColors(
                    backgroundColor = colors.primary,
                    contentColor = White
                ),
                modifier = buttonModifier
            )

            Box(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(vertical = 16.dp),
                contentAlignment = Alignment.Center
            ) {
                Text(
                    text = if (!viewModel.isNotificationOn()) stringResource(R.string.notification_view_not_now_btn_text) else "",
                    style = AppTheme.typography.bodyTextStyle1,
                    color = if (isDarkMode) colors.textSecondary else textColor.copy(0.4f),
                    lineHeight = 19.sp,
                    modifier = Modifier
                        .motionClickEvent {
                            if (!viewModel.isNotificationOn()) {
                                viewModel.onNotNowButtonClicked()
                            }
                        }
                )
            }
        }
    }
}

@Composable
fun ShowNotificationDialog(
    viewModel: NotificationPermissionViewModel,
) {
    CustomAlertDialog(
        title = null,
        subTitle = stringResource(id = R.string.notification_alert_dialog_description_text),
        confirmBtnText = stringResource(id = R.string.notification_alert_dialog_allow_button_text),
        dismissBtnText = stringResource(id = R.string.notification_alert_dialog_dismiss_button_text),
        confirmBtnColor = NotificationDialogButtonColor,
        onConfirmClick = { viewModel.onRedirectToSettings() },
        onDismissClick = { viewModel.closeNotificationDialog() }
    )
}
