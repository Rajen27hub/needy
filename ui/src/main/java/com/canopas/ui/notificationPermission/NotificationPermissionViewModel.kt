package com.canopas.ui.notificationPermission

import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.FREE_USER
import com.canopas.base.ui.Event
import com.canopas.ui.home.settings.buypremiumsubscription.DEVICE_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@HiltViewModel
class NotificationPermissionViewModel @Inject constructor(
    private val appDispatcher: AppDispatcher,
    private val appAnalytics: AppAnalytics,
    private val navManager: NavManager,
    private val notificationManagerCompat: NotificationManagerCompat,
    private val authManager: AuthManager
) : ViewModel() {
    val openNotificationDialog = MutableStateFlow(false)
    val navigateToAppSettings = MutableStateFlow(Event(false))

    init {
        viewModelScope.launch {
            appAnalytics.logEvent("view_notification_screen")
        }
    }

    fun openNotificationDialog() {
        openNotificationDialog.tryEmit(true)
    }

    fun onRedirectToSettings() {
        navigateToAppSettings.value = Event(true)
        openNotificationDialog.tryEmit(false)
    }

    fun closeNotificationDialog() {
        openNotificationDialog.tryEmit(false)
        popBack()
    }

    fun isNotificationOn(): Boolean {
        return notificationManagerCompat.areNotificationsEnabled()
    }

    fun onContinueButtonClick() {
        viewModelScope.launch {
            withContext(appDispatcher.MAIN) {
                appAnalytics.logEvent("tap_notification_screen_continue_btn")
                if (isNotificationOn()) {
                    popBack()
                } else {
                    openNotificationDialog()
                }
            }
        }
    }

    fun onNotNowButtonClicked() {
        appAnalytics.logEvent("tap_notification_screen_not_now_btn")
        popBack()
    }

    fun popBack() {
        navManager.popSignInScreens()
        if (authManager.currentUser?.session?.limitedAccess == true) {
            navManager.navigateToBuyPremiumScreen(
                showBackArrow = false,
                textType = DEVICE_LIMIT_TEXT_TYPE
            )
        } else {
            authManager.currentUser?.let {
                if (!it.hasFirstname() || !it.hasValidEmail()) {
                    navManager.navigateToProfileScreen(showBackArrow = false, promptPremium = true)
                } else if (it.userType == FREE_USER) {
                    navManager.navigateToBuyPremiumScreen(
                        showBackArrow = false,
                        showDismissButton = true
                    )
                }
            }
        }
    }
}
