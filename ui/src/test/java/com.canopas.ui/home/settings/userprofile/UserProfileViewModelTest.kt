package com.canopas.ui.home.settings.userprofile

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.GENDER_MALE
import com.canopas.base.data.rest.AuthServices
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.user
import com.canopas.data.utils.TestUtils.Companion.user2
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.io.File

@ExperimentalCoroutinesApi
class UserProfileViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    lateinit var viewModel: UserProfileViewModel

    private var service = mock<NoLonelyService>()

    private val profileImage = mock<File>()
    private val eventBus = mock<EventBus>()
    private var authManager = mock<AuthManager>()
    private var authService = mock<AuthServices>()
    private var navManager = mock<NavManager>()
    private var appAnalytics = mock<AppAnalytics>()
    private var resources = mock<Resources>()

    private val testDispatcher = AppDispatcher(IO = UnconfinedTestDispatcher())

    private fun initViewModel() = runTest {
        whenever(authManager.currentUser).thenReturn(user)
        viewModel = UserProfileViewModel(
            service, testDispatcher,
            eventBus, authManager, authService,
            navManager, appAnalytics, resources
        )
    }

    @Test
    fun test_firstName_change() = runTest {
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
        initViewModel()
        viewModel.onFirstNameChanged("abcd")
        val expected = (viewModel.userState.value as UserState.SUCCESS).userData
        Assert.assertEquals("abcd", expected.firstName)
        Assert.assertEquals(expected, viewModel.updatedUserAccount)
    }

    @Test
    fun test_lastName_change() = runTest {
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
        initViewModel()
        viewModel.onLastNameChanged("xyz")
        val expected = (viewModel.userState.value as UserState.SUCCESS).userData
        Assert.assertEquals("xyz", expected.lastName)
        Assert.assertEquals(expected, viewModel.updatedUserAccount)
    }

    @Test
    fun test_userName_change() = runTest {
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))

        initViewModel()
        viewModel.onUserNameChanged("xyz user")
        val expected = (viewModel.userState.value as UserState.SUCCESS).userData
        Assert.assertEquals("xyz user", expected.userName)
        Assert.assertEquals(expected, viewModel.updatedUserAccount)
    }

    @Test
    fun test_Email_Change() = runTest {
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
        initViewModel()
        viewModel.onEmailChanged("aaa@gmail.com")
        val expected = (viewModel.userState.value as UserState.SUCCESS).userData
        Assert.assertEquals("aaa@gmail.com", expected.email)
        Assert.assertEquals(false, viewModel.enableSaveButton.value)
    }

    @Test
    fun test_phone_Change() = runTest {
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))

        initViewModel()
        viewModel.onPhoneChanged("+91 9898704051")
        val expected = (viewModel.userState.value as UserState.SUCCESS).userData
        Assert.assertEquals("+91 9898704051", expected.phone)
    }

    @Test
    fun test_gender_Change() = runTest {
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))

        initViewModel()
        viewModel.onGenderChanged(GENDER_MALE)
        val expected = (viewModel.userState.value as UserState.SUCCESS).userData
        Assert.assertEquals(GENDER_MALE, expected.gender)
    }

    @Test
    fun testSuccess_fetchUserData() = runTest {
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user2))

        val successState = UserState.SUCCESS(user2)
        initViewModel()
        Assert.assertEquals(user2, successState.userData)
    }

    @Test
    fun testFailure_fetchUserData() {
        runTest {
            val errorState = UserState.ERROR("Error")
            whenever(service.getUserDetailById(1)).thenReturn(Result.failure(RuntimeException("Error")))
            initViewModel()
            Assert.assertEquals(errorState, viewModel.userState.value)
        }
    }

    @Test
    fun testSuccess_onSaveButtonClick() {
        runTest {
            whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
            whenever(service.updateUserById(1, user)).thenReturn(
                Result.success(user2)
            )
            initViewModel()
            viewModel.onSaveBtnClick()
            verify(navManager).popBack()
        }
    }

    @Test
    fun testFail_uploadProfile_onSaveButtonClick() {
        runTest {
            val errorState = UserState.ERROR("Error")
            whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
            whenever(profileImage.exists()).thenReturn(true)
            whenever(profileImage.name).thenReturn("file.jpg")
            whenever(service.uploadProfile(any())).thenReturn(Result.failure(RuntimeException("Error")))
            whenever(service.updateUserById(1, user))
                .thenReturn(Result.success(user))
            initViewModel()
            viewModel.uploadProfileImage(profileImage)
            viewModel.onSaveBtnClick()
            Assert.assertEquals(errorState, viewModel.userState.value)
        }
    }

    @Test
    fun testFailure_onSaveButtonClick() {
        runTest {
            val errorState = UserState.ERROR("Error")
            whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
            whenever(
                service.updateUserById(
                    1,
                    user
                )
            ).thenReturn(Result.failure(RuntimeException("Error")))
            initViewModel()
            viewModel.onSaveBtnClick()
            Assert.assertEquals(errorState, viewModel.userState.value)
        }
    }

    @Test
    fun testLoading_onSaveButtonClick() {
        runTest {
            val loadingState = UserState.LOADING
            whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
            whenever(service.updateUserById(1, user)).doSuspendableAnswer {
                withContext(Dispatchers.IO) { delay(5000) }
                Result.success(user2)
            }
            initViewModel()
            viewModel.onSaveBtnClick()
            Assert.assertEquals(loadingState, viewModel.userState.value)
        }
    }

    @Test
    fun test_loading_state_whenDeleteUser() = runTest {
        whenever(authService.deleteUser(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(mock())
        }
        initViewModel()
        viewModel.deleteUser()
        Assert.assertEquals(viewModel.userState.value, UserState.LOADING)
    }

    @Test
    fun test_Success_state_whenDeleteUser() = runTest {
        whenever(authService.deleteUser(1)).thenReturn(
            Result.success(mock())
        )
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))

        initViewModel()
        viewModel.deleteUser()
        verify(navManager).popBack()
    }

    @Test
    fun test_failure_state_whenDeleteUser() = runTest {
        whenever(authService.deleteUser(1)).thenReturn(
            Result.failure(RuntimeException("Error"))
        )
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
        initViewModel()

        viewModel.deleteUser()

        Assert.assertEquals(viewModel.userState.value, UserState.ERROR("Error"))
    }

    @Test
    fun test_open_delete_dialog_whenDeleteUser() = runTest {
        initViewModel()
        viewModel.openDialog()
        Assert.assertEquals(true, viewModel.openDeleteDialog.value)
    }

    @Test
    fun test_close_delete_dialog_whenDismissDialog() = runTest {
        initViewModel()
        viewModel.closeDialog()
        Assert.assertEquals(false, viewModel.openDeleteDialog.value)
    }

    @Test
    fun test_managePopBack() {
        initViewModel()
        viewModel.managePopBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_showBackArrow() {
        initViewModel()
        viewModel.onStart(showBackArrow = true, promptPremium = false)
        Assert.assertEquals(true, viewModel.shouldShowBackArrow.value)
    }

    @Test
    fun test_checkLengthValidation() = runTest {
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
        initViewModel()
        viewModel.updatedUserAccount = user2
        viewModel.checkLengthValidation("abcd")
        Assert.assertEquals(true, viewModel.enableSaveButton.value)
    }

    @Test
    fun test_checkUpdatingValidation() = runTest {
        whenever(service.getUserDetailById(1)).thenReturn(Result.success(user))
        initViewModel()
        viewModel.updatedUserAccount = user2
        viewModel.checkUpdatingValidation()
        Assert.assertEquals(true, viewModel.enableSaveButton.value)
    }
}
