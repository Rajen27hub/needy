package com.canopas.ui.home.settings.privacy

import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class PrivacyViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val navManager = mock<NavManager>()

    private lateinit var viewModel: PrivacyViewModel

    @Before
    fun setup() {
        viewModel =
            PrivacyViewModel(navManager)
    }

    @Test
    fun test_manageStoringClick() {
        viewModel.manageStoringClick()
        Assert.assertEquals(true, viewModel.storingSectionExpanded.value)
    }

    @Test
    fun test_manageResearchClick() {
        viewModel.manageResearchClick()
        Assert.assertEquals(true, viewModel.researchSectionExpanded.value)
    }

    @Test
    fun test_manageAnalyticsClick() {
        viewModel.manageAnalyticsClick()
        Assert.assertEquals(true, viewModel.analyticsSectionExpanded.value)
    }

    @Test
    fun test_manageRequiredFieldClick() {
        viewModel.manageRequiredFieldClick()
        Assert.assertEquals(true, viewModel.requiredFieldSectionExpanded.value)
    }

    @Test
    fun test_manageOptionalFieldClick() {
        viewModel.manageOptionalFieldClick()
        Assert.assertEquals(true, viewModel.optionalFieldSectionExpanded.value)
    }

    @Test
    fun test_requiredData_openNotificationDialog() {
        viewModel.openNotificationDialog(true)
        Assert.assertEquals(true, viewModel.showRequiredDataPrompt.value)
        Assert.assertEquals(false, viewModel.showOptionalDataPrompt.value)
        Assert.assertEquals(true, viewModel.openNotificationDialog.value)
    }

    @Test
    fun test_optionalData_openNotificationDialog() {
        viewModel.openNotificationDialog(false)
        Assert.assertEquals(false, viewModel.showRequiredDataPrompt.value)
        Assert.assertEquals(true, viewModel.showOptionalDataPrompt.value)
        Assert.assertEquals(true, viewModel.openNotificationDialog.value)
    }

    @Test
    fun test_closeNotificationDialog() {
        viewModel.closeNotificationDialog()
        Assert.assertEquals(false, viewModel.showRequiredDataPrompt.value)
        Assert.assertEquals(false, viewModel.showOptionalDataPrompt.value)
        Assert.assertEquals(false, viewModel.openNotificationDialog.value)
    }

    @Test
    fun test_popBackStack() {
        viewModel.managePopBack()
        verify(navManager).popBack()
    }
}
