package com.canopas.ui.home.settings.buypremiumsubscription

import android.app.Activity
import android.content.res.Resources
import com.android.billingclient.api.Purchase
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.rest.AuthServices
import com.canopas.base.ui.Event
import com.canopas.data.exception.BuyPremiumResultCall
import com.canopas.data.exception.UpgradeResponse
import com.canopas.data.model.PlanOption
import com.canopas.data.model.PlanType
import com.canopas.data.model.PremiumSubscriptionBody
import com.canopas.data.utils.TestUtils.Companion.user
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.billingdata.BillingError
import com.canopas.ui.billingdata.GoogleBillingClient
import com.canopas.ui.navigation.NavManager
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class BuyPremiumSubscriptionViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val navManager = mock<NavManager>()

    private var service = mock<BuyPremiumResultCall>()

    private val eventBus = mock<EventBus>()

    private val purchase = mock<Purchase>()
    private val googleBillingClient = mock<GoogleBillingClient>()
    private val appAnalytics = mock<AppAnalytics>()
    private val firebaseCrashlytics = mock<FirebaseCrashlytics>()

    private var authServices = mock<AuthServices>()
    private var authManager = mock<AuthManager>()

    private lateinit var viewModel: BuyPremiumSubscriptionViewModel

    private val testDispatcher = AppDispatcher(IO = UnconfinedTestDispatcher())
    private var signInClient = mock<GoogleSignInClient>()
    private val resources = mock<Resources>()

    private val plans = mutableListOf(
        PlanOption(
            type = PlanType.PLAN_MONTHLY,
            price = "$12.0",
            priceMicros = 12000,
            productDetails = mock()
        ),
        PlanOption(
            type = PlanType.PLAN_YEARLY,
            price = "$24.0",
            priceMicros = 24000,
            productDetails = mock()
        )
    )

    @Before
    fun setup() = runTest {
        whenever(
            googleBillingClient.queryProductDetails(
                listOf(
                    PlanType.PLAN_MONTHLY.id,
                    PlanType.PLAN_YEARLY.id
                )
            )
        ).thenReturn(Result.success(plans))
        viewModel =
            BuyPremiumSubscriptionViewModel(
                navManager,
                service,
                eventBus,
                testDispatcher,
                googleBillingClient,
                appAnalytics,
                firebaseCrashlytics,
                authServices,
                authManager,
                signInClient,
                resources
            )
    }

    @Test
    fun test_monthly_subscription_interval_change() {
        viewModel.updateSubscription(PlanType.PLAN_MONTHLY)
        Assert.assertEquals(PlanType.PLAN_MONTHLY, viewModel.selectedPlanType.value)
    }

    @Test
    fun test_yearly_subscription_interval_change() {
        viewModel.updateSubscription(PlanType.PLAN_YEARLY)
        Assert.assertEquals(PlanType.PLAN_YEARLY, viewModel.selectedPlanType.value)
    }

    @Test
    fun test_popBackStack_from_topBar() {
        viewModel.managePopBack(true)
        verify(appAnalytics).logEvent("tap_upgrade_premium_back", null)
        verify(navManager).popBack()
    }

    @Test
    fun test_popBackStack() {
        viewModel.managePopBack()
        verify(appAnalytics).logEvent("tap_upgrade_premium_close", null)
        verify(navManager).popBack()
    }

    @Test
    fun testProcessing_onUpgradeToPremium() {
        runTest {
            val contextWrapper = mock<Activity>()
            val processingState = BuyPremiumSubscriptionViewModel.PurchaseFlowState.PROCESSING
            viewModel.selectedPlanType.value = PlanType.PLAN_YEARLY
            whenever(purchase.packageName).thenReturn("com.example.package")
            whenever(purchase.products).thenReturn(listOf("product1"))
            whenever(purchase.purchaseToken).thenReturn("purchaseToken")
            whenever(
                googleBillingClient.purchase(
                    contextWrapper,
                    plans[1].productDetails!!
                )
            ).thenReturn(
                flowOf(GoogleBillingClient.PurchaseState.Processing)
            )
            viewModel.onPremiumBtnClicked(contextWrapper, PlanType.PLAN_YEARLY)

            Assert.assertEquals(processingState, viewModel.purchaseFlowState.value)
        }
    }

    @Test
    fun testSuccess_onUpgradeToPremium() {
        runTest {
            val contextWrapper = mock<Activity>()
            viewModel.selectedPlanType.value = PlanType.PLAN_YEARLY
            val successData = flowOf(GoogleBillingClient.PurchaseState.Success(purchase))
            whenever(purchase.packageName).thenReturn("com.example.package")
            whenever(purchase.products).thenReturn(listOf("product1"))
            whenever(purchase.purchaseToken).thenReturn("purchaseToken")

            whenever(
                googleBillingClient.purchase(
                    contextWrapper,
                    plans[1].productDetails!!
                )
            ).thenReturn(
                successData
            )
            val premiumSubscriptionBody = PremiumSubscriptionBody(purchase.packageName, purchase.products[0], purchase.purchaseToken)
            whenever(service.upgradeToPremium(premiumSubscriptionBody)).thenReturn(
                UpgradeResponse(user, null, resources = resources)
            )
            viewModel.onPremiumBtnClicked(contextWrapper, PlanType.PLAN_YEARLY)
            Assert.assertEquals(
                BuyPremiumSubscriptionViewModel.BuyPremiumState.UPGRADED,
                viewModel.state.value
            )
        }
    }

    @Test
    fun testFailure_onUpgradeToPremium() {
        runTest {
            val contextWrapper = mock<Activity>()
            viewModel.selectedPlanType.value = PlanType.PLAN_YEARLY

            val failureState = flowOf(GoogleBillingClient.PurchaseState.Error(BillingError("Error", 0)))

            whenever(purchase.packageName).thenReturn("com.example.package")
            whenever(purchase.products).thenReturn(listOf("product1"))
            whenever(purchase.purchaseToken).thenReturn("purchaseToken")

            whenever(
                googleBillingClient.purchase(
                    contextWrapper,
                    plans[1].productDetails!!
                )
            ).thenReturn(
                failureState
            )

            viewModel.onPremiumBtnClicked(contextWrapper, PlanType.PLAN_YEARLY)

            Assert.assertEquals(
                BuyPremiumSubscriptionViewModel.BuyPremiumState.FAILURE("Error", false),
                viewModel.state.value
            )
        }
    }

    @Test
    fun test_navigateToPrivacyScreen() {
        viewModel.navigateToPrivacyScreen()
        verify(navManager).navigateToPrivacyScreen()
    }

    @Test
    fun test_loading_state_whenLogout_from_other_device() = runTest {
        val loadingState = BuyPremiumSubscriptionViewModel.BuyPremiumState.LOADING
        whenever(authServices.logOutFromAllDevices()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }

        viewModel.logOutFromOtherDevices()
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_success_state_whenLogout_from_other_device() = runTest {
        val jsonElement = Mockito.mock(JsonElement::class.java)
        whenever(authServices.logOutFromAllDevices()).thenReturn(Result.success(jsonElement))
        viewModel.logOutFromOtherDevices()
        verify(navManager).popBack()
    }

    @Test
    fun test_Failure_whenLogout_from_other_device() = runTest {
        whenever(authServices.logOutFromAllDevices())
            .thenReturn(Result.failure(RuntimeException("Error")))
        val failureState =
            Event(BuyPremiumSubscriptionViewModel.BuyPremiumState.FAILURE("Error", false)).peekContent()
        viewModel.logOutFromOtherDevices()
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_loading_state_whenLogoutUser() = runTest {
        whenever(authServices.logOutUser(authManager.refreshToken)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        val loadingState =
            Event(BuyPremiumSubscriptionViewModel.BuyPremiumState.LOADING).peekContent()
        viewModel.logOutUser()
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_success_state_whenLogoutUser() = runTest {
        val jsonElement = Mockito.mock(JsonElement::class.java)
        whenever(authServices.logOutUser(authManager.refreshToken)).thenReturn(
            Result.success(
                jsonElement
            )
        )
        whenever(authManager.anonymousLogin()).thenReturn(Unit)
        viewModel.logOutUser()
        verify(navManager).popBack()
    }

    @Test
    fun test_Failure_whenLogoutUser() = runTest {
        whenever(authServices.logOutUser(authManager.refreshToken))
            .thenReturn(Result.failure(RuntimeException("Error")))
        val failureState = Event(BuyPremiumSubscriptionViewModel.BuyPremiumState.FAILURE("Error", false)).peekContent()
        viewModel.logOutUser()
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_open_sign_out_dialog_whenLogOutUser() {
        viewModel.openSignOutDialog()
        Assert.assertEquals(true, viewModel.openSignOutDialog.value.getContentIfNotHandled())
    }

    @Test
    fun test_close_sign_out_dialog_whenDismissDialog() {
        viewModel.closeSignOutDialog()
        Assert.assertEquals(false, viewModel.openSignOutDialog.value.getContentIfNotHandled())
    }
}
