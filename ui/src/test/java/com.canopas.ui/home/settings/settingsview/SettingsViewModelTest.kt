package com.canopas.ui.home.settings.settingsview

import android.app.NotificationManager
import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.rest.AuthServices
import com.canopas.base.ui.Event
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.user
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class SettingsViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private var authServices = mock<AuthServices>()

    private var authManager = mock<AuthManager>()
    private var eventBus = mock<EventBus>()

    lateinit var viewModel: SettingsViewModel

    private var navManager = mock<NavManager>()

    private var service = mock<NoLonelyService>()

    private var appAnalytics = mock<AppAnalytics>()
    private var preferences = mock<NoLonelyPreferences>()
    private var notificationManager = mock<NotificationManager>()
    private var signInClient = mock<GoogleSignInClient>()
    private var resources = mock<Resources>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setUp() {
        initViewModel()
    }

    private fun initViewModel() {
        whenever(authManager.authState).thenReturn(AuthState.UNAUTHORIZED)
        viewModel =
            SettingsViewModel(
                testDispatcher,
                authServices,
                authManager,
                eventBus,
                navManager,
                service,
                appAnalytics,
                preferences,
                notificationManager,
                signInClient,
                resources
            )
    }

    @Test
    fun test_loading_state_whenLogoutUser() = runTest {
        whenever(authServices.logOutUser(authManager.refreshToken)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        val loadingState = Event(SettingsState.LOADING).peekContent()
        initViewModel()
        viewModel.logOutUser()
        assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_success_state_whenLogoutUser() = runTest {
        val jsonElement = Mockito.mock(JsonElement::class.java)
        initViewModel()
        whenever(authServices.logOutUser(authManager.refreshToken)).thenReturn(
            Result.success(
                jsonElement
            )
        )
        whenever(authManager.resetSession()).thenReturn(Unit)
        viewModel.logOutUser()
        val successData = SettingsState.UNAUTHORIZED
        assertEquals(successData, viewModel.state.value)
    }

    @Test
    fun test_Failure_whenLogoutUser() = runTest {
        whenever(authServices.logOutUser(authManager.refreshToken))
            .thenReturn(Result.failure(RuntimeException("Error")))
        val failureState = Event(SettingsState.FAILURE("Error")).peekContent()
        initViewModel()
        viewModel.logOutUser()
        assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun testSuccess_fetchUserData() = runTest {
        whenever(authManager.currentUser).thenReturn(user)
        initViewModel()
        viewModel.onStart()
        verify(appAnalytics).logEvent("view_tab_settings", null)
        assertEquals(user.userType, viewModel.userType.value)
    }

    @Test
    fun test_navigateToSignInMethods() = runTest {
        viewModel.navigateToSignInMethods()
        initViewModel()
        verify(appAnalytics).logEvent("tap_settings_sign_in", null)
        verify(navManager).navigateToSignInMethods()
    }

    @Test
    fun test_navigateToReportProblem() = runTest {
        viewModel.openFeedbackScreen(0)
        initViewModel()
        verify(appAnalytics).logEvent("tap_settings_send_feedback", null)
        verify(navManager).navigateToFeedback()
    }

    @Test
    fun test_navigateToProfileScreen() = runTest {
        viewModel.openProfileScreen()
        initViewModel()
        verify(appAnalytics).logEvent("tap_settings_view_profile", null)
        verify(navManager).navigateToProfileScreen()
    }

    @Test
    fun test_open_sign_out_dialog_whenLogOutUser() {
        initViewModel()
        viewModel.openSignOutDialog()
        verify(appAnalytics).logEvent("tap_settings_sign_out", null)
        assertEquals(true, viewModel.openSignOutDialog.value.getContentIfNotHandled())
    }

    @Test
    fun test_close_sign_out_dialog_whenDismissDialog() {
        initViewModel()
        viewModel.closeSignOutDialog()
        assertEquals(false, viewModel.openSignOutDialog.value.getContentIfNotHandled())
    }

    @Test
    fun test_addPremiumBtnClickAnalyticEvent() {
        viewModel.addPremiumBtnClickAnalyticEvent()
        verify(appAnalytics).logEvent("tap_settings_view_premium_sub", null)
    }

    @Test
    fun test_NavigateToBuyPremiumScreen() {
        viewModel.openBuyPremiumScreen()
        verify(appAnalytics).logEvent("tap_settings_upgrade_premium", null)
        verify(navManager).navigateToBuyPremiumScreen()
    }

    @Test
    fun test_openNotificationScreen() {
        viewModel.openNotificationScreen()
        verify(appAnalytics).logEvent("tap_setting_Notifications", null)
        verify(navManager).navigateToNotificationsScreen()
    }

    @Test
    fun test_onLeaderboardVisibilityChange() {
        viewModel.onLeaderboardVisibilityChange(true)
        verify(appAnalytics).logEvent("setting_tab_leader_board_switch {true}", null)
        assertEquals(true, viewModel.isVisibleOnLeaderboard.value)
    }
}
