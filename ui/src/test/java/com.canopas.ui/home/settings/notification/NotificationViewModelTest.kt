package com.canopas.ui.home.settings.notification

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.Notifications
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@OptIn(ExperimentalCoroutinesApi::class)
class NotificationViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    lateinit var viewModel: NotificationViewModel
    var service = mock<NoLonelyService>()
    var navManager = mock<NavManager>()
    var appAnalytics = mock<AppAnalytics>()
    var authManager = mock<AuthManager>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )
    private val resources = mock<Resources>()
    @Before
    fun setup() {
        viewModel = NotificationViewModel(
            service, navManager, appAnalytics, authManager, testDispatcher, resources
        )
    }

    @Test
    fun test_updateNotificationSetting_onSuccess() = runTest {
        val completedState = NotificationState.SUCCESS
        val json = mock<JsonElement>()

        whenever(authManager.currentUser).thenReturn(TestUtils.userNotification)
        whenever(
            service.updateNotificationsSettings(
                Notifications(
                    true,
                    true,
                    true,
                    true
                )
            )
        ).thenReturn(
            Result.success(json)
        )
        viewModel.updateNotificationSettings()
        assertEquals(completedState, viewModel.state.value)
    }

    @Test
    fun test_updateNotificationSetting_onLoading() = runTest {
        val loadingState = NotificationState.LOADING
        whenever(authManager.currentUser).thenReturn(TestUtils.userNotification)
        whenever(
            service.updateNotificationsSettings(Notifications(true, true, true, true))
        ).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.updateNotificationSettings()
        assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_updateNotificationSetting_onFailure() = runTest {
        val failureState = NotificationState.FAILURE("Error")
        whenever(
            service.updateNotificationsSettings(Notifications(true, true, true, true))
        ).thenReturn(
            Result.failure(RuntimeException("Error"))
        )
        viewModel.updateNotificationSettings()
        assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_updatePushNotification() {
        viewModel.updatePushNotification(true)
        assertEquals(true, viewModel.pushNotificationEnabled.value)
    }

    @Test
    fun test_updatePushInactiveReminder() {
        viewModel.updatePushInActiveReminderNotification(true)
        assertEquals(true, viewModel.inActivePushNotificationEnabled.value)
    }

    @Test
    fun test_updateEmailNotification() {
        viewModel.updateEmailNotification(false)
        assertEquals(false, viewModel.emailNotificationEnabled.value)
    }

    @Test
    fun test_updateEmailInactiveReminder() {
        viewModel.updateEmailInactiveReminderNotification(false)
        assertEquals(false, viewModel.inActiveEmailNotificationEnabled.value)
    }

    @Test
    fun test_navigateToProfileScreen() {
        viewModel.openProfileScreen()
        verify(navManager).navigateToProfileScreen()
    }

    @Test
    fun test_popBackStack() {
        viewModel.managePopBack()
        verify(navManager).popBack()
    }
}
