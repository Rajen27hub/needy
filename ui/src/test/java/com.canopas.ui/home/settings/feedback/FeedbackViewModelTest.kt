package com.canopas.ui.home.settings.feedback

import android.content.res.Resources
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.utils.Device
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.io.File

@ExperimentalCoroutinesApi
class FeedbackViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    lateinit var viewModel: FeedbackViewModel

    private val uploadedImage = mock<File>()

    private var noLonelyService = mock<NoLonelyService>()
    private var navManager = mock<NavManager>()

    private val device = mock<Device>()
    private val resources = mock<Resources>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    private fun initViewModel() = runTest {
        viewModel = FeedbackViewModel(noLonelyService, testDispatcher, navManager, device, resources)
        whenever(device.getAppVersionCode()).thenReturn(102000)
        whenever(device.deviceName()).thenReturn("generic_x86")
        whenever(device.getDeviceOsVersion()).thenReturn("11")
    }

    @Test
    fun test_titleText_change() = runTest {
        initViewModel()
        viewModel.onTitleChange("title")
        Assert.assertEquals("title", viewModel.title.value)
        Assert.assertEquals(false, viewModel.showShortTitleError.value)
    }

    @Test
    fun test_descriptionText_change() = runTest {
        initViewModel()
        viewModel.onDescriptionChange("description")
        Assert.assertEquals("description", viewModel.description.value)
    }

    @Test
    fun test_loading_onSubmitReport() = runTest {
        val loadingState = FeedbackState.LOADING
        val data = mock<JsonElement>()
        val file = mock<File>()
        whenever(uploadedImage.exists()).thenReturn(true)
        whenever(uploadedImage.name).thenReturn("file.jpg")
        whenever(noLonelyService.submitReport(any(), any(), any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(data)
        }
        initViewModel()
        viewModel.submitReport(file, "request", "feedback", "storyId")
        Assert.assertEquals(loadingState, viewModel.feedbackState.value)
    }

    @Test
    fun test_success_onSubmitReport() = runTest {
        val data = mock<JsonElement>()
        val file = mock<File>()
        whenever(uploadedImage.exists()).thenReturn(true)
        whenever(uploadedImage.name).thenReturn("file.jpg")
        whenever(noLonelyService.submitReport(any(), any(), any())).thenReturn(Result.success(data))
        initViewModel()
        viewModel.uploadSelectedImage(uploadedImage)
        viewModel.submitReport(file, "request", "feedback", "storyId")
        Assert.assertEquals(true, viewModel.showSuccessAlertDialog.value)
    }

    @Test
    fun test_Failure_onSubmitReport() = runTest {
        val errorState = FeedbackState.ERROR("Error")
        val file = mock<File>()
        whenever(uploadedImage.exists()).thenReturn(true)
        whenever(uploadedImage.name).thenReturn("file.jpg")
        whenever(noLonelyService.submitReport(any(), any(), any())).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        initViewModel()
        viewModel.uploadSelectedImage(uploadedImage)
        viewModel.submitReport(file, "request", "feedback", "storyId")
        Assert.assertEquals(errorState, viewModel.feedbackState.value)
    }

    @Test
    fun test_open_image_changer_dialog() {
        initViewModel()
        viewModel.openImageChanger()
        Assert.assertEquals(true, viewModel.openImageChooserEvent.value.getContentIfNotHandled())
    }

    @Test
    fun test_upload_selected_image() {
        whenever(uploadedImage.exists()).thenReturn(true)
        whenever(uploadedImage.name).thenReturn("file.jpg")
        initViewModel()
        viewModel.uploadSelectedImage(uploadedImage)
        Assert.assertEquals(uploadedImage, viewModel.pendingUploadedFeedbackImage.value)
    }

    @Test
    fun test_remove_selected_image() {
        initViewModel()
        viewModel.removeSelectedImage()
        Assert.assertEquals(null, viewModel.pendingUploadedFeedbackImage.value)
    }

    @Test
    fun test_managePopBack() {
        initViewModel()
        viewModel.managePopBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_popBackStack() {
        initViewModel()
        viewModel.managePopBack()
        Assert.assertEquals(false, viewModel.showSuccessAlertDialog.value)
        verify(navManager).popBack()
    }
}
