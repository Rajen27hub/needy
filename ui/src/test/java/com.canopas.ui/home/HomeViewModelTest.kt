package com.canopas.ui.home

import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.greenrobot.eventbus.EventBus
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

class HomeViewModelTest {
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private val navManager = mock<NavManager>()
    private val eventBus = mock<EventBus>()
    private val preferences = mock<NoLonelyPreferences>()
    private val appAnalytics = mock<AppAnalytics>()

    private lateinit var viewModel: HomeViewModel
    @Before
    fun setup() {
        viewModel =
            HomeViewModel(navManager, eventBus, preferences, appAnalytics)
    }

    @Test
    fun test_navigateTo_onRouteChange() {
        viewModel.navigateTo("Explore")
        Assert.assertEquals("Explore", viewModel.navigateTo.value.getContentIfNotHandled())
        verify(navManager).navigateBackExplore()
    }

    @Test
    fun test_currentRoute() {
        viewModel.updateRoute("Current")
        Assert.assertEquals("Current", viewModel.currentRoute)
    }
}
