package com.canopas.ui.home.explore

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.auth.AuthStateChangeListener
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.data.event.ActivityCompletionEvent
import com.canopas.data.model.CompletionBody
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.basicActivity
import com.canopas.data.utils.TestUtils.Companion.basicSubscription
import com.canopas.data.utils.TestUtils.Companion.featuredFeeds
import com.canopas.data.utils.TestUtils.Companion.goal
import com.canopas.data.utils.TestUtils.Companion.goalWithColors
import com.canopas.data.utils.TestUtils.Companion.motivationalMaterial
import com.canopas.data.utils.TestUtils.Companion.story
import com.canopas.data.utils.TestUtils.Companion.subscription
import com.canopas.data.utils.TestUtils.Companion.user
import com.canopas.feature_points_data.repository.PointsRepository
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.ArgumentCaptor
import org.mockito.kotlin.any
import org.mockito.kotlin.capture
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

const val MOTIVATIONAL_TYPE_VIDEOS = 2
const val MOTIVATIONAL_TYPE_BLOGS = 1

@ExperimentalCoroutinesApi
class ExploreViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val services = mock<NoLonelyService>()

    private val clock = mock<CurrentDate>()

    private val navManager = mock<NavManager>()

    private val eventBus = EventBus()

    private lateinit var viewModel: ExploreViewModel

    private val authManager = mock<AuthManager>()
    private val pointsRepository = mock<PointsRepository>()
    private val appAnalytics = mock<AppAnalytics>()

    private val resources = mock<Resources>()
    val testDispatcher = UnconfinedTestDispatcher()

    private val appDispatcher = AppDispatcher(
        IO = testDispatcher
    )

    private val noLonelySharePreferences = mock<NoLonelyPreferences>()

    @Before
    fun setup() {
        initViewModel()
    }

    private fun initViewModel() {
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        viewModel =
            ExploreViewModel(
                services,
                appDispatcher,
                eventBus,
                authManager,
                noLonelySharePreferences,
                navManager,
                appAnalytics,
                pointsRepository,
                resources
            )
    }

    @Test
    fun test_defaultExploreStatus_fetchFeeds() = runTest {
        val loadingData = flowOf(ExploreState.LOADING)
        whenever(authManager.currentUser).thenReturn(user)
        whenever(services.getFeaturedFeeds()).thenReturn(Result.success(listOf(featuredFeeds)))
        whenever(services.getTodoTimeline(any())).doSuspendableAnswer {
            withContext(appDispatcher.IO) { delay(5000) }
            Result.success(listOf(basicSubscription))
        }
        initViewModel()
        assertEquals(loadingData.first(), viewModel.state.value)
        whenever(services.getAllGoals()).thenReturn(Result.success(listOf(goal)))
        viewModel.onStart()
        testDispatcher.scheduler.advanceUntilIdle()
        val successState =
            flowOf(
                ExploreState.SUCCESS(
                    false,
                    listOf(basicSubscription),
                    listOf(featuredFeeds),
                    listOf(goalWithColors)
                )
            )

        assertEquals(successState.first(), viewModel.state.value)
    }

    @Test
    fun test_loadingExploreStatus_fetchFeeds() = runTest {
        val loadingData = flowOf(ExploreState.LOADING)
        whenever(authManager.currentUser).thenReturn(user)
        whenever(services.getFeaturedFeeds()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        whenever(services.getUserSubscriptions(true)).thenReturn(Result.success(emptyList()))
        initViewModel()
        viewModel.onStart()
        verify(appAnalytics).logEvent("explore_open", null)
        verify(appAnalytics).logEvent("view_tab_explore", null)
        assertEquals(loadingData.first(), viewModel.state.value)
    }

    @Test
    fun test_SuccessExploreStatus_fetchFeeds() = runTest {
        whenever(authManager.currentUser).thenReturn(user)
        whenever(services.getFeaturedFeeds()).thenReturn(Result.success(listOf(featuredFeeds)))
        whenever(services.getTodoTimeline(any())).thenReturn(
            Result.success(
                listOf(
                    basicSubscription
                )
            )
        )
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        whenever(services.getAllGoals()).thenReturn(Result.success(listOf(goal)))
        initViewModel()
        viewModel.onStart()
        verify(appAnalytics).logEvent("explore_open", null)
        verify(appAnalytics).logEvent("view_tab_explore", null)
        val successState =
            flowOf(
                ExploreState.SUCCESS(
                    false,
                    listOf(basicSubscription),
                    listOf(featuredFeeds),
                    listOf(goalWithColors)
                )
            )
        assertEquals(successState.first(), viewModel.state.value)
    }

    @Test
    fun test_LoadingState_whenCompleteActivity() = runTest {
        val loadingData = CompletionState.COMPLETING(basicSubscription.id)
        whenever(clock.getCurrentDate()).thenReturn(0)
        val completionTime = clock.getCurrentDate() / 1000
        val completionBody = CompletionBody(1, completionTime, "Asis/Kolkata")
        whenever(services.saveDailyCompletionActivity(completionBody)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.completeHabit(basicSubscription)
        verify(appAnalytics).logEvent("complete_habit", null)
        verify(appAnalytics).logEvent("tap_explore_complete_card", null)
        assertEquals(loadingData, viewModel.stateCompletion.value)
    }

    @Test
    fun test_SuccessState_whenCompleteActivity() = runTest {
        val successData = CompletionState.IDLE
        val jsonElement = mock<JsonElement>()
        whenever(clock.getCurrentDate()).thenReturn(0)
        val completionTime = clock.getCurrentDate() / 1000
        val completionBody = CompletionBody(1, completionTime, "Asis/Kolkata")
        whenever(services.saveDailyCompletionActivity(completionBody)).thenReturn(
            Result.success(jsonElement)
        )
        whenever(noLonelySharePreferences.getCompletedActivitiesCount()).thenReturn(0)
        viewModel.completeHabit(basicSubscription)
        verify(appAnalytics).logEvent("complete_habit", null)
        verify(appAnalytics).logEvent("tap_explore_complete_card", null)
        eventBus.post(ActivityCompletionEvent(basicActivity))
        assertEquals(
            successData, viewModel.stateCompletion.value
        )
    }

    @Test
    fun test_Failure_whenCompleteActivity() = runTest {
        val failureState = CompletionState.FAILURE("Error")
        whenever(services.saveDailyCompletionActivity(any()))
            .thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.completeHabit(basicSubscription)
        verify(appAnalytics).logEvent("complete_habit", null)
        verify(appAnalytics).logEvent("tap_explore_complete_card", null)
        assertEquals(failureState, viewModel.stateCompletion.value)
    }

    @Test
    fun testUserAccount_onAuthStateChange() = runTest {
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        whenever(authManager.currentUser).thenReturn(user)
        val authCaptor: ArgumentCaptor<AuthStateChangeListener> =
            ArgumentCaptor.forClass(AuthStateChangeListener::class.java)
        viewModel.onStart()
        verify(authManager).addListener(capture(authCaptor))
        authCaptor.value.onAuthStateChanged(AuthState.VERIFIED(user))
        assertEquals(authManager.currentUser, viewModel.user.value)
    }

    @Test
    fun test_NavigateToActivityStatus_onToDoCardClicked() {
        viewModel.onToDoCardClicked(subscription.id)
        verify(appAnalytics).logEvent("tap_explore_todo_card", null)
        verify(navManager).navigateToActivityStatusBySubscriptionId(subscription.id)
    }

    @Test
    fun test_close_app_rating_view() {
        initViewModel()
        viewModel.closeRatingCardView()
        assertEquals(false, viewModel.showAppRatingCard.value)
    }

    @Test
    fun test_showWebView() {
        viewModel.showWebView(motivationalMaterial.media_url, MOTIVATIONAL_TYPE_BLOGS)
        verify(navManager).navigateToWebView(motivationalMaterial.media_url)
        verify(appAnalytics).logEvent("tap_explore_blog_item", null)
    }

    @Test
    fun test_play_video() {
        viewModel.playVideo(motivationalMaterial.media_url)
        verify(navManager).navigateToVideo(motivationalMaterial.media_url)
    }

    @Test
    fun test_onMotivationalItemClickForVideo() {
        viewModel.onMotivationalItemClick(motivationalMaterial.media_url, isVideoItem = true, MOTIVATIONAL_TYPE_VIDEOS)
        verify(navManager).navigateToVideo(motivationalMaterial.media_url)
        viewModel.showWebView(motivationalMaterial.media_url, MOTIVATIONAL_TYPE_VIDEOS)
    }

    @Test
    fun test_onMotivationalItemClickForBlogAndPodcast() {
        viewModel.onMotivationalItemClick(
            motivationalMaterial.media_url,
            isVideoItem = false,
            MOTIVATIONAL_TYPE_BLOGS
        )
        verify(navManager).navigateToWebView(motivationalMaterial.media_url)
        viewModel.showWebView(motivationalMaterial.media_url, MOTIVATIONAL_TYPE_BLOGS)
    }

    @Test
    fun test_navigate_to_notes() {
        viewModel.navigateToNotesView("activity1", "1", 1, false)
        verify(navManager).navigateToAddOrUpdateNotes("activity1", "1", "1", false)
    }

    @Test
    fun test_close_notes_tips_view() {
        initViewModel()
        viewModel.closeNotesTipsView()
        assertEquals(false, viewModel.showNotesTipsView.value)
    }

    @Test
    fun test_navigateToMyGoalsScreen() {
        viewModel.navigateToMyGoals()
        verify(navManager).navigateToMyGoalsScreen()
    }

    @Test
    fun test_navigateToLeaderBoardView() {
        viewModel.navigateToLeaderBoardView()
        verify(appAnalytics).logEvent("tab_explore_points")
        verify(navManager).navigateToLeaderboardScreen()
    }

    @Test
    fun test_navigateToSettingScreen() {
        viewModel.navigateToSettingScreen()
        verify(appAnalytics).logEvent("tab_explore_setting")
        verify(navManager).navigateToSettingsScreen()
    }

    @Test
    fun test_navigateToStoryView() {
        viewModel.navigateToStoryView(story)
        verify(navManager).navigateToStoryView(story.id)
    }

    @Test
    fun test_navigateToEditGoal() {
        viewModel.navigateToEditGoal(1, 1)
        verify(navManager).navigateToEditGoal(1, 1)
    }

    @Test
    fun test_resetCompletionState() {
        viewModel.resetCompletionState()
        assertEquals(CompletionState.IDLE, viewModel.stateCompletion.value)
    }
}
