package com.canopas.ui.home.explore.activitylist

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.activityData
import com.canopas.data.utils.TestUtils.Companion.category
import com.canopas.data.utils.TestUtils.Companion.feeds
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class ActivityListViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: ActivityListViewModel
    private val service = mock<NoLonelyService>()
    private val navManager = mock<NavManager>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    private val appAnalytics = mock<AppAnalytics>()
    private val resources = mock<Resources>()

    @Before
    fun setup() {
        viewModel = ActivityListViewModel(service, testDispatcher, navManager, appAnalytics, resources)
    }

    @Test
    fun test_default_onLoad() = runTest {
        val loadingData = flowOf(ActivityState.LOADING)
        whenever(service.getFeeds()).thenReturn(null)
        viewModel.fetchActivities(category.id)
        Assert.assertEquals(loadingData.first(), viewModel.state.value)
        whenever(service.getFeeds()).thenReturn(Result.success(feeds))
        viewModel.fetchActivities(category.id)
        val successState = flowOf(ActivityState.SUCCESS(category.activities))
        Assert.assertEquals(successState.first(), viewModel.state.value)
    }

    @Test
    fun test_loadingState_fetchActivities() = runTest {
        val loadingData = flowOf(ActivityState.LOADING)
        whenever(service.getFeeds()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.fetchActivities(category.id)
        Assert.assertEquals(loadingData.first(), viewModel.state.value)
    }

    @Test
    fun test_SuccessState_fetchActivities() = runTest {
        whenever(service.getFeeds()).thenReturn(Result.success(feeds))
        viewModel.fetchActivities(category.id)
        val successState = flowOf(ActivityState.SUCCESS(category.activities))
        Assert.assertEquals(successState.first(), viewModel.state.value)
    }

    @Test
    fun test_FailureState_fetchActivities() = runTest {
        whenever(service.getFeeds()).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.fetchActivities(category.id)
        val failureState = flowOf(ActivityState.FAILURE("Error", false))
        Assert.assertEquals(failureState.first(), viewModel.state.value)
    }

    @Test
    fun test_onActivitySelected() {
        viewModel.onActivitySelected(activityData)
        verify(appAnalytics).logEvent("tap_explore_activity", null)
        verify(navManager).navigateToHabitConfigure(activityData.id)
    }

    @Test
    fun test_popBackStack() {
        viewModel.popBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_navigateToCustomActivityView() {
        viewModel.navigateToAddActivity()
        verify(navManager).navigateToCustomActivityView()
    }
}
