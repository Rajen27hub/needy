package com.canopas.ui.home.explore.notificationSummary

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.summary
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@OptIn(ExperimentalCoroutinesApi::class)
class NotificationSummaryViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    lateinit var viewModel: NotificationSummaryViewModel
    var service = mock<NoLonelyService>()
    var navManager = mock<NavManager>()
    var appAnalytics = mock<AppAnalytics>()
    var authManager = mock<AuthManager>()
    var preferences = mock<NoLonelyPreferences>()
    private val resources = mock<Resources>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun sutUp() {
        viewModel = NotificationSummaryViewModel(service, testDispatcher, preferences, appAnalytics, resources)
    }

    @Test
    fun test_getWeeklyActivitySummary_loadingState() = runTest {
        val loadingState = SummaryState.LOADING
        whenever(service.getWeeklyActivitiesSummary(123345655, 654987635)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.getWeeklyActivitySummary(123345655, 654987635)
        assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_getWeeklyActivitySummary_onSuccess() = runTest {
        val onSuccess = SummaryState.SUCCESS(summary)
        whenever(service.getWeeklyActivitiesSummary(123345655, 654987635)).thenReturn(
            Result.success(
                summary
            )
        )
        viewModel.getWeeklyActivitySummary(123345655, 654987635)
        assertEquals(onSuccess, viewModel.state.value)
    }

    @Test
    fun test_getWeeklyActivitySummary_onFailure() = runTest {
        val onFailure = SummaryState.FAILURE("Error")
        whenever(service.getWeeklyActivitiesSummary(123345655, 654987635)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getWeeklyActivitySummary(123345655, 654987635)
        assertEquals(onFailure, viewModel.state.value)
    }

    @Test
    fun test_onStart() {
        viewModel.onStart("123345655", "654987635")
        verify(appAnalytics).logEvent("tap_weekly_summary_notification")
        verify(appAnalytics).logEvent("view_notification_summary")
    }
}
