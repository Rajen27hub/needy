package com.canopas.ui.home.explore.storyView

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.activityData
import com.canopas.data.utils.TestUtils.Companion.story
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.home.settings.buypremiumsubscription.ACTIVITY_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@OptIn(ExperimentalCoroutinesApi::class)
class StoryViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private lateinit var viewModel: StoryViewModel
    private val navManager = mock<NavManager>()
    private val appAnalytics = mock<AppAnalytics>()
    private val service = mock<NoLonelyService>()
    private val sharePreferences = mock<NoLonelyPreferences>()
    private val authManager = mock<AuthManager>()
    private val resources = mock<Resources>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setUp() {
        viewModel = StoryViewModel(
            navManager, appAnalytics, testDispatcher, service, sharePreferences, authManager, resources
        )
    }

    @Test
    fun test_getStoryById_loading() = runTest {
        val loadingSate = StoryState.LOADING
        whenever(service.getStoryById(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.getStoryById(1)
        assertEquals(loadingSate, viewModel.state.value)
    }

    @Test
    fun test_getStoryById_Success() = runTest {
        val successState = StoryState.SUCCESS(story)
        whenever(service.getStoryById(1)).thenReturn(Result.success(story))
        viewModel.getStoryById(1)
        assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_getStoryById_failure() = runTest {
        val failureState = StoryState.FAILURE("Error", false)
        whenever(service.getStoryById(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getStoryById(1)
        assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_getActivityTimeAndDuration_failure() = runTest {
        val failureState = ActivityState.FAILURE("Error")
        whenever(service.getActivityById(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getActivityTimeAndDuration(1, "story", 1)
        assertEquals(failureState, viewModel.activityState.value)
    }

    @Test
    fun test_getActivityTimeAndDuration_finish() = runTest {
        val finishState = ActivityState.FINISH
        whenever(service.getActivityById(1)).thenReturn(Result.success(activityData))
        viewModel.getActivityTimeAndDuration(1, "story", 1)
        assertEquals(finishState, viewModel.activityState.value)
    }

    @Test
    fun test_getActivityTimeAndDuration_loading() = runTest {
        val loadingSate = ActivityState.LOADING
        whenever(service.getActivityById(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.getActivityTimeAndDuration(1, "story", 1)
        assertEquals(loadingSate, viewModel.activityState.value)
    }

    @Test
    fun test_navigateToCustomActivityView() {
        viewModel.navigateToCustomActivityView(0)
        verify(appAnalytics).logEvent("tap_story_view_own_activity", null)
        verify(navManager).navigateToCustomActivityView()
    }

    @Test
    fun test_navigateToSignInMethods() {
        viewModel.navigateToSignInMethods()
        verify(navManager).navigateToSignInMethods()
    }
    @Test
    fun test_navigateToWhyThisActivity() {
        viewModel.navigateToWhyThisActivity(38, "story", 1, "8:00", 1)
        verify(navManager).navigateToWhyThisActivityScreen(38, "story", "8:00", 1, 1)
    }

    @Test
    fun test_navigateToConfigureActivity() {
        viewModel.navigateToConfigureActivity(38, "story", 1, "8:00", 1)
        verify(navManager).navigateToConfigureActivity(38, "story", "8:00", 1, 1)
    }

    @Test
    fun test_resetActivityState() {
        val startState = ActivityState.START
        viewModel.resetActivityState()
        assertEquals(startState, viewModel.activityState.value)
    }

    @Test
    fun test_popBack() {
        viewModel.popBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_openBuyPremiumScreen() {
        viewModel.openBuyPremiumScreen()
        verify(navManager).navigateToBuyPremiumScreen(textType = ACTIVITY_LIMIT_TEXT_TYPE)
    }
}
