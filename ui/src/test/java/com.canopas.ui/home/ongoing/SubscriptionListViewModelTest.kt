package com.canopas.ui.home.ongoing

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.subscription
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class SubscriptionListViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: SubscriptionListViewModel

    private val services = mock<NoLonelyService>()
    private val preferences = mock<NoLonelyPreferences>()
    private val resources = mock<Resources>()
    private val navManager = mock<NavManager>()

    private var appAnalytics = mock<AppAnalytics>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        initViewModel()
    }

    private fun initViewModel() {
        viewModel =
            SubscriptionListViewModel(
                services,
                testDispatcher,
                preferences,
                navManager,
                appAnalytics,
                resources
            )
    }

    @Test
    fun test_onActivityClicked() = runTest {
        viewModel.onActivityClicked(subscription.id)
        verify(appAnalytics).logEvent("tap_active_subscriptions_item", null)
        verify(navManager).navigateToActivityStatusBySubscriptionId(subscription.id)
    }

    @Test
    fun test_defaultSubscriptionStatus() = runTest {
        whenever(services.getUserSubscriptions()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(emptyList())
        }
        initViewModel()
        viewModel.onStart()
        verify(appAnalytics).logEvent("view_tab_active_subscriptions", null)
        Assert.assertEquals(SubscriptionsState.LOADING, viewModel.state.value)

        whenever(services.getUserSubscriptions()).thenReturn(Result.success(listOf(subscription)))
        initViewModel()
        viewModel.onStart()
        val successState = SubscriptionsState.SUCCESS(listOf(subscription))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun loadingSubscriptionStatusTest() = runTest {
        whenever(services.getUserSubscriptions()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(emptyList())
        }
        initViewModel()
        viewModel.onStart()
        verify(appAnalytics).logEvent("view_tab_active_subscriptions", null)
        Assert.assertEquals(SubscriptionsState.LOADING, viewModel.state.value)
    }

    @Test
    fun test_SuccessSubscriptionStatus() = runTest {
        whenever(services.getUserSubscriptions()).thenReturn(Result.success(listOf(subscription)))
        initViewModel()
        viewModel.onStart()
        verify(appAnalytics).logEvent("view_tab_active_subscriptions", null)
        val successState = SubscriptionsState.SUCCESS(listOf(subscription))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_FailureSubscriptionStatus() = runTest {
        whenever(services.getUserSubscriptions()).thenReturn(Result.failure(RuntimeException("Error")))
        initViewModel()
        viewModel.onStart()
        verify(appAnalytics).logEvent("view_tab_active_subscriptions", null)
        val failureState = SubscriptionsState.FAILURE("Error", false)
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_navigateTo_SubscriptionHistory_view() = runTest {
        viewModel.navigateToArchivedActivitiesView()
        verify(navManager).navigateToArchivedActivitiesView()
    }
}
