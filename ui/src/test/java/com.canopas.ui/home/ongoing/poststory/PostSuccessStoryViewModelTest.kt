package com.canopas.ui.home.ongoing.poststory

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.PostSuccessStory
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.successStory
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class PostSuccessStoryViewModelTest {
    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    lateinit var viewModel: PostSuccessStoryViewModel

    private val services = mock<NoLonelyService>()
    private val analytics = mock<AppAnalytics>()
    private val navManager = mock<NavManager>()
    private val resources = mock<Resources>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    private fun initViewModel() = runTest {
        viewModel =
            PostSuccessStoryViewModel(testDispatcher, services, analytics, navManager, resources)
    }

    private val postStory =
        PostSuccessStory("Running", "Running is amazing activity", listOf(1))

    @Test
    fun test_titleText_change() = runTest {
        initViewModel()
        viewModel.onTitleChange("title")
        Assert.assertEquals("title", viewModel.title.value)
        Assert.assertEquals(false, viewModel.enablePostButton.value)
    }

    @Test
    fun test_descriptionText_change() = runTest {
        initViewModel()
        viewModel.onDescriptionChange("description")
        Assert.assertEquals("description", viewModel.description.value)
        Assert.assertEquals(false, viewModel.enablePostButton.value)
    }

    @Test
    fun test_loadingState_SuccessStoryPost() = runTest {
        val loadingState = PostStoryState.LOADING
        initViewModel()
        val postStory =
            PostSuccessStory(viewModel.title.value, viewModel.description.value, listOf(1))
        whenever(services.postSuccessStory(postStory)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        initViewModel()
        viewModel.handleSaveButtonClick("1", 1, false)
        Assert.assertEquals(loadingState, viewModel.storyState.value)
    }

    @Test
    fun test_successState_SuccessStoryPost() = runTest {
        val successState = PostStoryState.POSTED
        val jsonObject = Mockito.mock(JsonObject::class.java)
        whenever(services.postSuccessStory(postStory)).thenReturn(Result.success(jsonObject))
        initViewModel()
        viewModel.handleSaveButtonClick("1", 1, false)
        Assert.assertEquals(successState, viewModel.storyState.value)
    }

    @Test
    fun test_failureState_SuccessStoryPost() = runTest {
        initViewModel()
        val postStory =
            PostSuccessStory(viewModel.title.value, viewModel.description.value, listOf(1))
        whenever(services.postSuccessStory(postStory)).thenReturn(
            Result.failure(
                RuntimeException(
                    "Error"
                )
            )
        )
        initViewModel()
        viewModel.handleSaveButtonClick("1", 1, false)
        val failureState = PostStoryState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.storyState.value)
    }

    @Test
    fun test_successStoryState_onFetchSuccessStory() = runTest {
        whenever(services.retrieveSuccessStoryDetailById(1)).thenReturn(Result.success(successStory))
        initViewModel()
        viewModel.fetchSuccessStory("1")
        Assert.assertEquals("Yoga", viewModel.title.value)
        Assert.assertEquals("Yoga is amazing activity", viewModel.description.value)
    }

    @Test
    fun test_failureStoryState_onFetchSuccessStory() = runTest {
        whenever(services.retrieveSuccessStoryDetailById(1)).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        initViewModel()
        viewModel.fetchSuccessStory("1")
        val failureState = PostStoryState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.storyState.value)
    }

    @Test
    fun test_successStoryState_onUpdateSuccessStory() = runTest {
        val jsonElement = Mockito.mock(JsonElement::class.java)
        whenever(services.updateSuccessStory(1, postStory)).thenReturn(Result.success(jsonElement))
        initViewModel()
        viewModel.handleSaveButtonClick("1", 1, true)
        val successState = PostStoryState.COMPLETED
        Assert.assertEquals(successState, viewModel.storyState.value)
    }

    @Test
    fun test_failureStoryState_onUpdateSuccessStory() = runTest {
        initViewModel()
        val postStory =
            PostSuccessStory(viewModel.title.value, viewModel.description.value, listOf(1))
        whenever(services.updateSuccessStory(1, postStory)).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        initViewModel()
        viewModel.handleSaveButtonClick("1", 1, true)
        val failureState = PostStoryState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.storyState.value)
    }

    @Test
    fun test_navigateToExplore() {
        initViewModel()
        viewModel.managePopBackExplore()
        verify(navManager).navigateBackExplore()
    }

    @Test
    fun test_managePopBack() {
        initViewModel()
        viewModel.managePopBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_enablePostButton_whenTitle_and_descriptionChange() {
        initViewModel()
        viewModel.onTitleChange("Read a Book Activity.")
        viewModel.onDescriptionChange("This is a Description.")
        Assert.assertEquals(true, viewModel.enablePostButton.value)
    }
}
