package com.canopas.ui.home.ongoing.history

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.subscription
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class ArchivedActivitiesViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: ArchivedActivitiesViewModel

    private val services = mock<NoLonelyService>()

    private val appAnalytics = mock<AppAnalytics>()

    private val navManager = mock<NavManager>()
    private val resources = mock<Resources>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        initViewModel()
    }

    private fun initViewModel() {
        viewModel = ArchivedActivitiesViewModel(services, testDispatcher, navManager, appAnalytics, resources)
    }

    @Test
    fun test_defaultSubscriptionHistory_State() = runTest {
        whenever(services.getUserSubscriptionsHistory()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(emptyList())
        }
        initViewModel()
        viewModel.onStart()
        Assert.assertEquals(SubscriptionsHistoryState.LOADING, viewModel.state.value)

        whenever(services.getUserSubscriptionsHistory()).thenReturn(
            Result.success(
                listOf(
                    subscription
                )
            )
        )
        initViewModel()
        viewModel.onStart()
        val successState = SubscriptionsHistoryState.SUCCESS(listOf(subscription))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_loadingState_SubscriptionHistory() = runTest {
        whenever(services.getUserSubscriptionsHistory()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(emptyList())
        }
        initViewModel()
        viewModel.onStart()
        Assert.assertEquals(SubscriptionsHistoryState.LOADING, viewModel.state.value)
    }

    @Test
    fun test_SuccessState_SubscriptionHistory() = runTest {
        whenever(services.getUserSubscriptionsHistory()).thenReturn(
            Result.success(
                listOf(
                    subscription
                )
            )
        )
        initViewModel()
        viewModel.onStart()
        val successState = SubscriptionsHistoryState.SUCCESS(listOf(subscription))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_SuccessState_SubscriptionHistory_withEmptyList() = runTest {
        whenever(services.getUserSubscriptionsHistory()).thenReturn(Result.success(emptyList()))
        initViewModel()
        viewModel.onStart()
        val successState = SubscriptionsHistoryState.SUCCESS(emptyList())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_FailureState_SubscriptionHistory() = runTest {
        whenever(services.getUserSubscriptionsHistory()).thenReturn(
            Result.failure(
                RuntimeException(
                    "Error"
                )
            )
        )
        initViewModel()
        viewModel.onStart()
        val failureState = SubscriptionsHistoryState.FAILURE("Error", false)
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_onActivityClicked() = runTest {
        viewModel.onActivityClicked(subscription.id)
        verify(navManager).navigateToActivityStatusBySubscriptionId(subscription.id, "", true)
    }

    @Test
    fun test_popBack() {
        viewModel.onBackClick()
        verify(navManager).popBack()
    }
}
