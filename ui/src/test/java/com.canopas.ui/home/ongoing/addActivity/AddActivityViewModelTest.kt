package com.canopas.ui.home.ongoing.addActivity

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.activityData
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.home.ongoing.addActivityView.AddActivityViewModel
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@OptIn(ExperimentalCoroutinesApi::class)
class AddActivityViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private lateinit var viewModel: AddActivityViewModel
    private val navManager = mock<NavManager>()
    private val appAnalytics = mock<AppAnalytics>()
    private val service = mock<NoLonelyService>()
    private val resources = mock<Resources>()

    @OptIn(ExperimentalCoroutinesApi::class)
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel = AddActivityViewModel(
            service, navManager, appAnalytics, testDispatcher, resources
        )
    }

    @Test
    fun test_navigateToCustomActivityList() = runTest {
        viewModel.navigateToCustomActivity()
        verify(appAnalytics).logEvent("tap_add_activity_to_own_activity", null)
        verify(navManager).navigateToCustomActivityView()
    }

    @Test
    fun test_onActivitySelected() = runTest {
        viewModel.onActivitySelected(activityData)
        verify(appAnalytics).logEvent("tap_add_activity_activity", null)
        verify(navManager).navigateToHabitConfigure(activityData.id)
    }

    @Test
    fun test_navigateToActivityList() = runTest {
        viewModel.navigateToActivityList(1)
        verify(appAnalytics).logEvent("tap_add_activity_category")
        verify(navManager).navigateToActivityList(1)
    }

    @Test
    fun test_onBackClick() = runTest {
        viewModel.onBackClick()
        verify(navManager).popBack()
    }
}
