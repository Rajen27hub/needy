package com.canopas.ui.congratulationprompt

import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.CustomActivityData
import com.canopas.data.model.SubscribeBody
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.subscription
import com.canopas.data.utils.TestUtils.Companion.subscription3
import com.canopas.data.utils.TestUtils.Companion.user
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class CongratulationViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: CongratulationViewModel

    private val authManager = mock<AuthManager>()

    private val services = mock<NoLonelyService>()

    private val navManager = mock<NavManager>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        initViewModel()
    }

    private fun initViewModel() {
        viewModel = CongratulationViewModel(services, testDispatcher, navManager, authManager)
    }

    @Test
    fun test_userName_setOnUserAccount() = runTest {
        whenever(authManager.currentUser).thenReturn(user)
        initViewModel()
        Assert.assertEquals(authManager.currentUser?.firstName, viewModel.userName.value)
    }

    @Test
    fun test_userProfile_setOnUserAccount() = runTest {
        whenever(authManager.currentUser).thenReturn(user)
        initViewModel()
        Assert.assertEquals(authManager.currentUser?.profileImageUrl, viewModel.profile.value)
    }

    @Test
    fun test_Post_SuccessStoryScreen() = runTest {
        viewModel.openPostStoryScreen(
            subscription.id,
            subscription.activity.name
        )
        verify(navManager).navigateToPostSuccessStory(
            subscription.id,
            subscription.activity.name
        )
    }

    @Test
    fun test_SetPromptStatus() = runTest {
        val subscription = subscription3
        val subscribeBody = SubscribeBody(
            activity_id = subscription.activity_id,
            timezone = "Asia/Kolkata",
            is_prompted = true, 1,
            null, CustomActivityData(subscription.activity.name, subscription.activity.description)
        )
        val jsonElement = mock<JsonElement>()
        whenever(
            services.updateSubscriptionBodyBySubscriptionId(
                subscription.id,
                subscribeBody
            )
        ).thenReturn(
            Result.success(jsonElement)
        )
        whenever(services.retrieveUserSubscriptionById(subscription.id)).thenReturn(
            Result.success(subscription)
        )
        initViewModel()
        viewModel.setIsPrompted(subscription3.id.toString())

        verify(services).updateSubscriptionBodyBySubscriptionId(subscription.id, subscribeBody)
    }

    @Test
    fun test_managePopBack() {
        viewModel.managePopBack()
        verify(navManager).popBack()
    }
}
