package com.canopas.ui.phonelogin

import android.app.Application
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthResponse
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.auth.ServiceError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.utils.Device
import com.canopas.campose.countrypicker.model.Country
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class PhoneLoginViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    val application = mock<Application>()
    private val authManager = mock<AuthManager>()
    private val navManager = mock<NavManager>()
    private val device = mock<Device>()
    private val appAnalytics = mock<AppAnalytics>()
    private val firebaseCrashlytics = mock<FirebaseCrashlytics>()
    private lateinit var viewModel: PhoneLoginViewModel
    private val testDispatcher = AppDispatcher(IO = UnconfinedTestDispatcher())

    @Before
    fun setUp() {
        viewModel = PhoneLoginViewModel(
            application,
            authManager,
            appAnalytics,
            firebaseCrashlytics,
            device,
            appDispatcher = testDispatcher,
            navManager
        )
        whenever(device.getId()).thenReturn("1")
        whenever(device.getAppVersionCode()).thenReturn(20000)
        whenever(device.deviceModel()).thenReturn("MD1")
        whenever(device.getDeviceOsVersion()).thenReturn("1")
    }

    @Test
    fun test_request_loginWithNumber_loadingState() = runTest {
        val loadingState = PhoneVerificationState.LOADING
        val authResponse = AuthResponse(AuthState.VERIFIED(mock()), ServiceError.NONE)
        val loginRequest = LoginRequest(
            1, "1",
            20000, "MD1", "1", "abcd", "123456789", "", 1
        )
        whenever(authManager.login(loginRequest, false)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            authResponse
        }
        viewModel.requestToLoginWithNumber("123456789", "abcd")
        Assert.assertEquals(loadingState, viewModel.phoneState.value)
    }

    @Test
    fun test_request_loginWithNumber_successState() = runTest {
        val authResponse = AuthResponse(AuthState.VERIFIED(mock()), ServiceError.NONE)
        val loginRequest = LoginRequest(
            1, "1",
            20000, "MD1", "1", "abcd", "123456789", "", 1
        )
        whenever(authManager.login(loginRequest, false))
            .thenReturn(authResponse)

        viewModel.requestToLoginWithNumber("123456789", "abcd")
        verify(navManager).popSignInScreens()
    }

    @Test
    fun test_request_loginWithNumber_failureState() = runTest {
        val loginRequest = LoginRequest(
            1, "1",
            20000, "MD1", "1", "abcd", "123456789", "", 1
        )
        whenever(authManager.login(loginRequest, false)).thenThrow(RuntimeException("Error"))
        viewModel.requestToLoginWithNumber("123456789", "abcd")
        Assert.assertEquals(
            viewModel.phoneState.value,
            PhoneVerificationState.FAILURE("Error")
        )
    }

    @Test
    fun test_onPhoneNoChange() {
        viewModel.onPhoneNoChange("123456")
        Assert.assertEquals("123456", viewModel.phoneNumberState.value)
    }

    @Test
    fun test_onCountrySelected() {
        val country = Country("India", "+91", "IN")
        viewModel.onCountrySelected(country)
        Assert.assertEquals(country, viewModel.selectedCountry.value)
    }
}
