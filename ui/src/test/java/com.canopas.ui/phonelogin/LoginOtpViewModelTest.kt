package com.canopas.ui.phonelogin

import androidx.core.app.NotificationManagerCompat
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthResponse
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.auth.ServiceError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.utils.Device
import com.canopas.data.utils.TestUtils.Companion.user3
import com.canopas.data.utils.TestUtils.Companion.user4
import com.canopas.data.utils.TestUtils.Companion.user5
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.home.settings.buypremiumsubscription.DEVICE_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class LoginOtpViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val authManager = mock<AuthManager>()
    private val navManager = mock<NavManager>()
    private val device = mock<Device>()
    private val appAnalytics = mock<AppAnalytics>()
    private val firebaseCrashlytics = mock<FirebaseCrashlytics>()
    private lateinit var viewModel: LoginOtpViewModel
    private val testDispatcher = AppDispatcher(IO = UnconfinedTestDispatcher())
    private val notificationManagerCompat = mock<NotificationManagerCompat>()

    @Before
    fun setUp() {
        viewModel = LoginOtpViewModel(
            authManager,
            appAnalytics,
            firebaseCrashlytics,
            device,
            appDispatcher = testDispatcher,
            navManager,
            notificationManagerCompat,
        )
        whenever(device.getId()).thenReturn("1")
        whenever(device.getAppVersionCode()).thenReturn(20000)
        whenever(device.deviceModel()).thenReturn("MD1")
        whenever(device.getDeviceOsVersion()).thenReturn("1")
    }

    @Test
    fun test_on_start() {
        viewModel.onStart("123456", "1234567890")
        Assert.assertEquals("123456", viewModel.storedVerificationId.value)
        Assert.assertEquals("1234567890", viewModel.phoneNumber.value)
    }

    @Test
    fun test_request_loginWithNumber_loadingState() = runTest {
        val loadingState = OtpVerificationState.LOADING
        val authResponse = AuthResponse(AuthState.VERIFIED(mock()), ServiceError.NONE)
        val loginRequest = LoginRequest(
            1, "1",
            20000, "MD1", "1", "abcd", "123456789", "", 1
        )
        whenever(authManager.login(loginRequest, false)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            authResponse
        }
        viewModel.requestToLoginWithNumber("123456789", "abcd")
        Assert.assertEquals(loadingState, viewModel.otpState.value)
    }

    @Test
    fun test_request_loginWithNumber_successState() = runTest {
        val authResponse = AuthResponse(AuthState.VERIFIED(mock()), ServiceError.NONE)
        val loginRequest = LoginRequest(
            1, "1",
            20000, "MD1", "1", "abcd", "123456789", "", 1
        )
        whenever(authManager.login(loginRequest, false))
            .thenReturn(authResponse)

        viewModel.requestToLoginWithNumber("123456789", "abcd")
        verify(navManager).popSignInScreens()
    }

    @Test
    fun test_navigateToProfileScreen_when_required_data_is_empty() = runTest {
        whenever(authManager.currentUser).thenReturn(user3)
        viewModel.proceedLoginSuccessNavigation()
        verify(navManager).popSignInScreens()
        verify(navManager).navigateToProfileScreen(showBackArrow = false, promptPremium = true)
    }

    @Test
    fun test_navigateToBuyPremiumScreen_after_login() = runTest {
        whenever(authManager.currentUser).thenReturn(user4)
        viewModel.proceedLoginSuccessNavigation()
        verify(navManager).navigateToBuyPremiumScreen(showBackArrow = false, showDismissButton = true)
    }

    @Test
    fun test_navigateToBuyPremiumScreen_for_device_limitation() = runTest {
        whenever(authManager.currentUser).thenReturn(user5)
        viewModel.proceedLoginSuccessNavigation()
        verify(navManager).navigateToBuyPremiumScreen(showBackArrow = false, textType = DEVICE_LIMIT_TEXT_TYPE)
    }

    @Test
    fun test_request_loginWithNumber_failureState() = runTest {
        val loginRequest = LoginRequest(
            1, "1",
            20000, "MD1", "1", "abcd", "123456789", "", 1
        )
        whenever(authManager.login(loginRequest, false)).thenThrow(RuntimeException("Error"))
        viewModel.requestToLoginWithNumber("123456789", "abcd")
        Assert.assertEquals(
            viewModel.otpState.value,
            OtpVerificationState.FAILURE("Error")
        )
    }

    @Test
    fun test_popBack() {
        viewModel.popBackStack()
        verify(navManager).popBack()
    }

    @Test
    fun test_resetState() {
        viewModel.resetState()
        val state = OtpVerificationState.START
        Assert.assertEquals(state, viewModel.otpState.value)
    }

    @Test
    fun test_true_status_OfNotification() {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        Assert.assertEquals(true, viewModel.isNotificationOn())
    }

    @Test
    fun test_false_status_OfNotification() {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(false)
        Assert.assertEquals(false, viewModel.isNotificationOn())
    }

    @Test
    fun test_navigateToNotificationScreen() = runTest {
        viewModel.isAboveAndroidS.value = true
        viewModel.proceedLoginSuccessNavigation()
        verify(navManager).navigateToNotificationPermissionView()
    }
}
