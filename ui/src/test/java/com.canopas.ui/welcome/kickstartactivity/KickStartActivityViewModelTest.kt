package com.canopas.ui.welcome.kickstartactivity

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.utils.Device
import com.canopas.data.model.SubscribedResponse
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.activityData
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.welcome.kickstart.KickStartActivityViewModel
import com.canopas.ui.welcome.kickstart.KickStartState
import com.canopas.ui.welcome.kickstart.SubscribeActivityState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class KickStartActivityViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: KickStartActivityViewModel
    private val noLonelySharePreferences = mock<NoLonelyPreferences>()
    private val device = mock<Device>()
    private val service = mock<NoLonelyService>()
    private val analytics = mock<AppAnalytics>()
    private val testDispatcher = AppDispatcher(IO = UnconfinedTestDispatcher())
    private var resources = mock<Resources>()
    @Before
    fun setUp() {
        viewModel = KickStartActivityViewModel(noLonelySharePreferences, testDispatcher, service, device, analytics, resources)
        whenever(device.timeZone()).thenReturn("Asia/Kolkata")
    }

    @Test
    fun test_toggleSelectedActivity() {
        viewModel.toggleSelectedActivity(1)
        Assert.assertEquals(1, viewModel.selectedActivityIndex.value)
    }

    @Test
    fun test_loadingState_fetchFeaturedActivities() = runTest {
        val loadingState = KickStartState.LOADING
        whenever(service.getFeaturedActivities()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.fetchFeaturedActivities()
        Assert.assertEquals(loadingState, viewModel.kickStartState.value)
    }

    @Test
    fun test_failureState_fetchFeaturedActivities() = runTest {
        whenever(service.getFeaturedActivities()).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.fetchFeaturedActivities()
        val failureState = KickStartState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.kickStartState.value)
    }

    @Test
    fun test_successState_fetchFeaturedActivities() = runTest {
        whenever(service.getFeaturedActivities()).thenReturn(Result.success(listOf(activityData)))
        viewModel.fetchFeaturedActivities()
        val successState = KickStartState.SUCCESS(listOf(activityData))
        Assert.assertEquals(successState, viewModel.kickStartState.value)
    }

    @Test
    fun test_loadingState_onContinueBtnClick() = runTest {
        val loadingState = SubscribeActivityState.LOADING
        whenever(service.subscribeActivity(any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.onContinueBtnClick("08:30")
        Assert.assertEquals(loadingState, viewModel.subscribeActivityState.value)
    }

    @Test
    fun test_failureState_onContinueBtnClick() = runTest {
        whenever(service.subscribeActivity(any())).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.featuredActivities.value = listOf(activityData)
        viewModel.selectedActivityIndex.value = 0
        viewModel.onContinueBtnClick("08:30")
        val failureState = SubscribeActivityState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.subscribeActivityState.value)
    }

    @Test
    fun test_successState_onContinueBtnClick() = runTest {
        whenever(service.subscribeActivity(any())).thenReturn(
            Result.success(
                SubscribedResponse(0)
            )
        )
        viewModel.featuredActivities.value = listOf(activityData)
        viewModel.selectedActivityIndex.value = 0
        viewModel.onContinueBtnClick("08:30")
        val successState = SubscribeActivityState.SUCCESS
        Assert.assertEquals(successState, viewModel.subscribeActivityState.value)
    }
}
