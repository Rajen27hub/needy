package com.canopas.ui.welcome

import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthResponse
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.auth.ServiceError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.utils.Device
import com.canopas.ui.MainCoroutineRule
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class OnboardViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: OnboardViewModel

    private val authManager = mock<AuthManager>()
    private val noLonelySharePreferences = mock<NoLonelyPreferences>()
    private val device = mock<Device>()
    private val loginRequest = LoginRequest(
        1, "1",
        20000, "MD1", "1"
    )
    private val analytics = mock<AppAnalytics>()
    private val testDispatcher = AppDispatcher(IO = UnconfinedTestDispatcher())
    private val authResponse = AuthResponse(AuthState.ANONYMOUS(mock()), ServiceError.NONE)

    @Before
    fun setUp() {
        viewModel = OnboardViewModel(authManager, device, testDispatcher, analytics, noLonelySharePreferences)
        whenever(device.getId()).thenReturn("1")
        whenever(device.getAppVersionCode()).thenReturn(20000)
        whenever(device.deviceModel()).thenReturn("MD1")
        whenever(device.getDeviceOsVersion()).thenReturn("1")
    }

    @Test
    fun `show loading on anonymous login`() = runTest {
        whenever(authManager.login(loginRequest)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            authResponse
        }
        viewModel.anonymousLogin()
        verify(analytics).logEvent("tap_intro_1_button", null)
        Assert.assertEquals(viewModel.loginState.value, LoginState.LOADING)
    }

    @Test
    fun `success state should works on anonymous login`() = runTest {
        whenever(authManager.login(loginRequest)).thenReturn(authResponse)
        viewModel.anonymousLogin()
        Assert.assertEquals(viewModel.loginState.value, LoginState.SUCCESS)
    }

    @Test
    fun `failure state should works on anonymous login`() = runTest {
        val authResponse = AuthResponse(AuthState.ANONYMOUS(mock()), ServiceError.NETWORKERROR)
        whenever(authManager.login(loginRequest)).thenReturn(authResponse)
        viewModel.anonymousLogin()
        Assert.assertEquals(
            viewModel.loginState.value,
            LoginState.FAILURE("No internet connection!")
        )
    }
}
