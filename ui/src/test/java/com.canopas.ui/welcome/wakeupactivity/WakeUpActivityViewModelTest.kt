package com.canopas.ui.welcome.wakeupactivity

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.utils.Device
import com.canopas.data.rest.NoLonelyService
import com.canopas.ui.MainCoroutineRule
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class WakeUpActivityViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: WakeUpActivityViewModel
    private val noLonelySharePreferences = mock<NoLonelyPreferences>()
    private val device = mock<Device>()
    private val service = mock<NoLonelyService>()
    private val analytics = mock<AppAnalytics>()
    private val testDispatcher = AppDispatcher(IO = UnconfinedTestDispatcher())
    private var resources = mock<Resources>()
    @Before
    fun setUp() {
        viewModel = WakeUpActivityViewModel(noLonelySharePreferences, testDispatcher, service, device, analytics, resources)
        whenever(device.timeZone()).thenReturn("Asia/Kolkata")
    }

    @Test
    fun test_set_wake_up_time() {
        viewModel.setWakeUpTime("10:00")
        Assert.assertEquals("10:00", viewModel.selectedWakeUpTime.value)
    }

    @Test
    fun test_loadingState_onContinueBtnClick() = runTest {
        val loadingState = WakeUpState.LOADING
        whenever(service.subscribeDefaultActivity(any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.onContinueBtnClick()
        Assert.assertEquals(loadingState, viewModel.wakeUpState.value)
    }

    @Test
    fun test_failureState_onContinueBtnClick() = runTest {
        whenever(service.subscribeDefaultActivity(any())).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.onContinueBtnClick()
        val failureState = WakeUpState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.wakeUpState.value)
    }

    @Test
    fun test_successState_onContinueBtnClick() = runTest {
        val jsonElement = mock<JsonElement>()
        whenever(service.subscribeDefaultActivity(any())).thenReturn(Result.success(jsonElement))
        viewModel.onContinueBtnClick()
        val successState = WakeUpState.SUCCESS
        Assert.assertEquals(successState, viewModel.wakeUpState.value)
    }
}
