package com.canopas.ui.editsubscription

import android.content.res.Resources
import androidx.core.app.NotificationManagerCompat
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.utils.Device
import com.canopas.data.model.CustomActivityData
import com.canopas.data.model.SubscribeBody
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.util.Calendar

@ExperimentalCoroutinesApi
class EditSubscriptionViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: EditSubscriptionViewModel
    private val device = mock<Device>()

    private val service = mock<NoLonelyService>()
    private val calendar = mock<Calendar>()
    private val notificationManagerCompat = mock<NotificationManagerCompat>()
    private val navManager = mock<NavManager>()
    private val analytics = mock<AppAnalytics>()
    private val resources = mock<Resources>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        whenever(calendar.get(Calendar.HOUR_OF_DAY)).thenReturn(12)
        whenever(calendar.get(Calendar.MINUTE)).thenReturn(0)
        whenever(calendar.get(Calendar.AM_PM)).thenReturn(Calendar.AM)
        whenever(calendar.get(Calendar.HOUR)).thenReturn(12)

        viewModel = EditSubscriptionViewModel(analytics, calendar, navManager, notificationManagerCompat, device, testDispatcher, service, resources)
        whenever(device.timeZone()).thenReturn("Asia/Kolkata")
    }

    @Test
    fun test_loadingState_fetchSubscriptionData() = runTest {
        val loadingState = FetchSubscriptionState.LOADING
        whenever(service.retrieveUserSubscriptionById(any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(TestUtils.subscription)
        }
        viewModel.onStart(TestUtils.subscription.id)
        Assert.assertEquals(loadingState, viewModel.fetchSubscriptionState.value)
    }

    @Test
    fun test_successState_fetchSubscriptionData() = runTest {
        val successState = FetchSubscriptionState.SUCCESS(TestUtils.subscription)
        whenever(service.retrieveUserSubscriptionById(any())).thenReturn(Result.success(TestUtils.subscription))
        viewModel.onStart(TestUtils.subscription.id)
        Assert.assertEquals(successState, viewModel.fetchSubscriptionState.value)
    }

    @Test
    fun test_failureState_fetchSubscriptionData() = runTest {
        val failureState = FetchSubscriptionState.FAILURE("Error")
        whenever(service.retrieveUserSubscriptionById(any())).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.onStart(5)
        Assert.assertEquals(failureState, viewModel.fetchSubscriptionState.value)
    }

    @Test
    fun test_loadingState_updateSubscription() = runTest {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        whenever(service.retrieveUserSubscriptionById(TestUtils.subscription5.id)).thenReturn(
            Result.success(
                TestUtils.subscription5
            )
        )
        val subscriptionBody = SubscribeBody(
            activity_id = TestUtils.activityData.id,
            timezone = device.timeZone(),
            is_prompted = false,
            repeat_type = 1,
            repeat_on = "[1,2,3]",
            activity_time = "12:0",
            activity_duration = viewModel.durationTabType.value.duration,
            reminders = listOf(),
            show_note_on_complete = viewModel.promptForNote.value
        )
        whenever(service.updateSubscriptionBodyBySubscriptionId(TestUtils.subscription.id, subscriptionBody)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.onStart(TestUtils.subscription5.id)
        viewModel.handleUpdateSubscriptionBtn()
        Assert.assertEquals(true, viewModel.showLoader.value)
    }

    @Test
    fun test_successState_updateSubscription() = runTest {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        whenever(service.retrieveUserSubscriptionById(TestUtils.subscription5.id)).thenReturn(
            Result.success(
                TestUtils.subscription5
            )
        )
        val successData = EditSubscriptionState.CONFIGURED
        viewModel.setActivityTime("13:10")
        val subscriptionBody = SubscribeBody(
            activity_id = TestUtils.subscription5.activity.id,
            timezone = device.timeZone(),
            is_prompted = false,
            repeat_type = 1,
            repeat_on = "[1,2,3]",
            activity_time = "12:0",
            activity_duration = viewModel.durationTabType.value.duration,
            reminders = listOf(),
            show_note_on_complete = viewModel.promptForNote.value
        )
        val json = mock<JsonElement>()
        whenever(service.updateSubscriptionBodyBySubscriptionId(TestUtils.subscription5.id, subscriptionBody)).thenReturn(
            Result.success(json)
        )
        viewModel.onStart(TestUtils.subscription5.id)
        viewModel.handleUpdateSubscriptionBtn()
        Assert.assertEquals(successData, viewModel.editSubscriptionState.value)
    }

    @Test
    fun test_loadingState_updateCustomSubscription() = runTest {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        whenever(service.retrieveUserSubscriptionById(TestUtils.subscription5.id)).thenReturn(
            Result.success(
                TestUtils.subscription5
            )
        )
        val subscriptionBody = SubscribeBody(
            activity_id = TestUtils.activityData.id,
            timezone = device.timeZone(),
            is_prompted = false,
            repeat_type = 1,
            repeat_on = "[1,2,3]",
            activity_time = "12:0",
            activity_duration = viewModel.durationTabType.value.duration,
            reminders = listOf(),
            show_note_on_complete = viewModel.promptForNote.value
        )
        whenever(service.updateSubscriptionBodyBySubscriptionId(TestUtils.subscription.id, subscriptionBody)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.onStart(TestUtils.subscription5.id)
        viewModel.handleUpdateSubscriptionBtn()
        Assert.assertEquals(true, viewModel.showLoader.value)
    }

    @Test
    fun test_successState_updateCustomSubscription() = runTest {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        whenever(service.retrieveUserSubscriptionById(TestUtils.subscription5.id)).thenReturn(
            Result.success(
                TestUtils.subscription5
            )
        )
        viewModel.setActivityTime("08:30")
        val subscriptionBody = SubscribeBody(
            activity_id = TestUtils.activityData.id,
            timezone = device.timeZone(),
            is_prompted = false,
            repeat_type = 1,
            repeat_on = "[1,2,3]",
            activity_time = "12:0",
            activity_duration = viewModel.durationTabType.value.duration,
            reminders = listOf(),
            show_note_on_complete = viewModel.promptForNote.value,
            custom_activity = CustomActivityData(viewModel.title.value)
        )
        val json = mock<JsonElement>()
        whenever(service.updateSubscriptionBodyBySubscriptionId(TestUtils.subscription5.id, subscriptionBody)).thenReturn(
            Result.success(json)
        )
        val successData = EditSubscriptionState.CONFIGURED
        viewModel.onStart(TestUtils.subscription5.id)
        viewModel.onTitleChange("Title")
        viewModel.handleUpdateSubscriptionBtn()
        Assert.assertEquals(successData, viewModel.editSubscriptionState.value)
    }
}
