package com.canopas.ui.goals.addgoals

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.GOAL_WALL_VISIBILITY_NEVER
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.goal
import com.canopas.data.utils.TestUtils.Companion.goalBody
import com.canopas.data.utils.TestUtils.Companion.subscription
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class AddOrUpdateGoalViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    lateinit var viewModel: AddOrUpdateGoalViewModel
    private val navManager = mock<NavManager>()
    private val appAnalytics = mock<AppAnalytics>()
    private val service = mock<NoLonelyService>()
    private val resources = mock<Resources>()
    val testDispatcher = UnconfinedTestDispatcher()

    private val appDispatcher = AppDispatcher(
        IO = testDispatcher
    )

    @Before
    fun setup() {
        viewModel = AddOrUpdateGoalViewModel(navManager, appDispatcher, service, resources, appAnalytics)
    }

    @Test
    fun fetch_goal_loadingState() = runTest {
        val loadingState = GoalState.LOADING
        whenever(service.getGoalById(goal.id)).doSuspendableAnswer {
            withContext(appDispatcher.IO) { delay(5000) }
            null
        }
        viewModel.onStart(goal.id)
        assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun fetch_goal_successState() = runTest {
        val successState = GoalState.SUCCESS(goal = goal)
        whenever(service.getGoalById(goal.id)).thenReturn(Result.success(goal))
        viewModel.onStart(goal.id)
        assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun fetch_goal_failureState() = runTest {
        val successState = GoalState.FAILURE("Error")
        whenever(service.getGoalById(goal.id)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.onStart(goal.id)
        assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun empty_goal_state() = runTest {
        viewModel.onStart(null)
        assertEquals(GoalState.START, viewModel.state.value)
    }

    @Test
    fun test_add_goal_loading_state() = runTest {
        whenever(service.createGoal(any())).doSuspendableAnswer {
            withContext(appDispatcher.IO) { delay(5000) }
            null
        }
        viewModel.onDoneButtonClick(subscription.id, null)
        assertEquals(true, viewModel.showActionLoader.value)
    }

    @Test
    fun test_add_goal_success_state() = runTest {
        val json = mock<JsonElement>()
        whenever(service.createGoal(goalBody = goalBody)).thenReturn(Result.success(json))
        whenever(service.updateGoal(goal.id, goalBody = goalBody)).thenReturn(Result.success(json))
        viewModel.onDoneButtonClick(subscription.id, null)
        assertEquals(false, viewModel.showActionLoader.value)
    }

    @Test
    fun test_onTitleChange() {
        viewModel.onTitleChange("title")
        assertEquals("title", viewModel.goalTitle.value)
    }

    @Test
    fun test_onDescriptionChange() {
        viewModel.onDescriptionChange("title")
        assertEquals("title", viewModel.goalDescription.value)
    }

    @Test
    fun test_set_goals_due_date() {
        viewModel.setGoalDueDate(1679482814000)
        assertEquals("Mar 22, 2023", viewModel.dueDate.value)
    }

    @Test
    fun test_visibility_selection() {
        viewModel.toggleVisibilitySelection(GOAL_WALL_VISIBILITY_NEVER)
        assertEquals(GOAL_WALL_VISIBILITY_NEVER, viewModel.selectedVisibilityType.value)
    }

    @Test
    fun test_open_delete_alert_dialog() {
        viewModel.openDeleteGoalDialog()
        assertEquals(true, viewModel.openDeleteDialog.value.getContentIfNotHandled())
    }

    @Test
    fun test_close_delete_alert_dialog() {
        viewModel.closeDeleteGoalDialog()
        assertEquals(false, viewModel.openDeleteDialog.value.getContentIfNotHandled())
    }

    @Test
    fun test_loadingState_onDeleteGoal() = runTest {
        whenever(service.deleteGoal(any())).doSuspendableAnswer {
            withContext(appDispatcher.IO) { delay(5000) }
            null
        }
        viewModel.deleteGoal(any())
        assertEquals(GoalState.LOADING, viewModel.state.value)
    }

    @Test
    fun test_failureState_onDeleteGoal() = runTest {
        val failureState = GoalState.FAILURE("Error")
        whenever(service.deleteGoal(any())).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.deleteGoal(any())
        assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_successState_onDeleteGoal() = runTest {
        whenever(service.deleteGoal(goal.id)).thenReturn(Result.success(mock()))
        viewModel.deleteGoal(goal.id)
        verify(navManager).popBack()
    }

    @Test
    fun test_popBack() {
        viewModel.popBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_resetState() {
        viewModel.resetState()
        assertEquals(GoalState.START, viewModel.state.value)
    }
}
