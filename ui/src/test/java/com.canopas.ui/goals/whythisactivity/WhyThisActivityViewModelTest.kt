package com.canopas.ui.goals.whythisactivity

import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify

@ExperimentalCoroutinesApi
class WhyThisActivityViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    lateinit var viewModel: WhyThisActivityViewModel
    private val navManager = mock<NavManager>()
    private val appAnalytics = mock<AppAnalytics>()

    @Before
    fun setup() {
        viewModel = WhyThisActivityViewModel(appAnalytics, navManager)
    }

    @Test
    fun test_titleText_change() {
        viewModel.onTitleChange("title")
        Assert.assertEquals("title", viewModel.title.value)
    }

    @Test
    fun test_managePopBack() {
        viewModel.popBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_onContinueBtnClick_for_customActivity() {
        viewModel.onContinueButtonClick(0, "title", "", "", "", 0, "")
        verify(navManager).navigateToConfigureCustomActivity("title", 0)
    }

    @Test
    fun test_onContinueBtnClick_for_curatedActivity() {
        viewModel.onContinueButtonClick(0, "", "", "", "0", 0, "")
        verify(navManager).navigateToConfigureActivity(0, "", "", 0, 0, "")
    }
}
