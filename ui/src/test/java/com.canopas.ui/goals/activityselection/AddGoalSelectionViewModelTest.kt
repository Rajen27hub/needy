package com.canopas.ui.goals.activityselection

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.subscription
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class AddGoalSelectionViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    lateinit var viewModel: AddGoalSelectionViewModel
    private val navManager = mock<NavManager>()
    private val appAnalytics = mock<AppAnalytics>()
    private val service = mock<NoLonelyService>()
    private val resources = mock<Resources>()
    val testDispatcher = UnconfinedTestDispatcher()

    private val appDispatcher = AppDispatcher(
        IO = testDispatcher
    )

    @Before
    fun setup() {
        viewModel = AddGoalSelectionViewModel(service, appDispatcher, navManager, appAnalytics, resources)
    }

    @Test
    fun fetch_subscriptions_loadingState() = runTest {
        val loadingState = SubscriptionState.LOADING
        whenever(service.getUserSubscriptions()).doSuspendableAnswer {
            withContext(appDispatcher.IO) { delay(5000) }
            null
        }
        viewModel.getUserSubscriptions()
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun fetch_subscriptions_successState() = runTest {
        val successState = SubscriptionState.SUCCESS(subscriptions = listOf(subscription))
        whenever(service.getUserSubscriptions()).thenReturn(Result.success(listOf(subscription)))
        viewModel.getUserSubscriptions()
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun fetch_subscriptions_failureState() = runTest {
        val successState = SubscriptionState.FAILURE("Error")
        whenever(service.getUserSubscriptions()).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getUserSubscriptions()
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_onActivity_selected() {
        viewModel.onActivitySelected(0)
        verify(navManager).navigateToAddGoal(0)
        Assert.assertEquals(true, viewModel.shouldPopBack.value)
    }

    @Test
    fun test_popBack() {
        viewModel.popBack()
        verify(navManager).popBack()
    }
}
