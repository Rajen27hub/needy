package com.canopas.ui.goals.mygoals

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.DueDateGoals
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.subscription
import com.canopas.data.utils.TestUtils.Companion.subscription3
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class MyGoalsViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    lateinit var viewModel: MyGoalsViewModel
    private val navManager = mock<NavManager>()
    private val appAnalytics = mock<AppAnalytics>()
    private val service = mock<NoLonelyService>()
    private val resources = mock<Resources>()
    val testDispatcher = UnconfinedTestDispatcher()
    private val appDispatcher = AppDispatcher(
        IO = testDispatcher
    )

    @Before
    fun setUp() {
        viewModel = MyGoalsViewModel(appAnalytics, service, navManager, appDispatcher, resources)
    }

    @Test
    fun test_loadData_loadingState() = runTest {
        val loadingState = GoalState.LOADING
        whenever(service.getUserSubscriptions()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.loadData(2)
        assertEquals(loadingState, viewModel.state.value)
    }
    @Test
    fun test_loadDate_success_fetchGoal_loadingState() = runTest {
        val loadingState = GoalState.LOADING
        whenever(service.getUserSubscriptions()).thenReturn(
            Result.success(
                listOf(
                    subscription,
                    subscription3
                )
            )
        )
        whenever(service.getAllGoals()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.fetchGoals(1)
        assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_loadData_onSuccessState() = runTest {
        val goalList = DueDateGoals(emptyMap(), emptyMap())
        val successState = GoalState.SUCCESS(
            1, listOf(subscription, subscription3), listOf(goalList, goalList, goalList)
        )
        whenever(service.getUserSubscriptions()).thenReturn(
            Result.success(
                listOf(
                    subscription,
                    subscription3
                )
            )
        )
        whenever(service.getAllGoals()).thenReturn(Result.success(emptyList()))
        viewModel.loadData(1)
        assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_loadDate_success_fetchGoal_failure() = runTest {
        val failureState = GoalState.FAILURE("Error", false)
        whenever(service.getUserSubscriptions()).thenReturn(
            Result.success(
                listOf(
                    subscription,
                    subscription3
                )
            )
        )
        whenever(service.getAllGoals()).thenReturn(Result.failure(RuntimeException("Error")))

        viewModel.loadData(1)
        assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_loadData_onFailureState() = runTest {
        val failureState = GoalState.FAILURE("Error", false)
        whenever(service.getUserSubscriptions()).thenReturn(Result.failure(RuntimeException("Error")))
        whenever(service.getGoalsBySubscriptionId(1)).thenReturn(Result.failure(RuntimeException("Error")))

        viewModel.loadData(1)
        assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_onStart() {
        viewModel.onStart(123)
        verify(appAnalytics).logEvent("view_my_goals_screen")
        assertEquals(123, viewModel.currentSelectedTab.value)
    }

    @Test
    fun test_navigateToUpdateGoals() {
        viewModel.navigateToUpdateGoals(1, 2)
        verify(navManager).navigateToEditGoal(1, 2)
    }

    @Test
    fun test_navigateToAddGoals() {
        viewModel.navigateToAddGoals(1)
        verify(navManager).navigateToAddGoal(1)
    }

    @Test
    fun test_managePopBack() {
        viewModel.managePopBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_handleTabItemSelection() {
        viewModel.handleTabItemSelection(123)
        assertEquals(123, viewModel.currentSelectedTab.value)
        verify(appAnalytics).logEvent("tap_my_goals_activity_tab")
    }

    @Test
    fun test_onTabUpdate() {
        viewModel.onTabUpdate(123)
        assertEquals(123, viewModel.currentSelectedTab.value)
    }

    @Test
    fun test_onStop() {
        viewModel.onStop()
        assertEquals(GoalState.LOADING, viewModel.state.value)
    }
}
