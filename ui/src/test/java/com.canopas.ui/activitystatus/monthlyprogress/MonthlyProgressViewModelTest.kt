package com.canopas.ui.activitystatus.monthlyprogress

import android.content.res.Resources
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.activitystatus.ProgressHistoryState
import com.canopas.ui.home.explore.CurrentDate
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class MonthlyProgressViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private lateinit var viewModel: MonthlyProgressViewModel
    private val date = mock<CurrentDate>()
    private val authManager = mock<AuthManager>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )
    private val resources = mock<Resources>()
    private val service = mock<NoLonelyService>()

    @Before
    fun setup() {
        viewModel = MonthlyProgressViewModel(date, authManager, testDispatcher, service, resources)
    }

    @Test
    fun test_get_progress_history_loadingState() = runTest {
        val loadingData = ProgressHistoryState.LOADING
        whenever(service.retrieveUserSubscriptionById(1))
            .thenReturn(Result.success(TestUtils.subscription))
        whenever(service.getSubscriptionsProgressHistory(1, 1, 10000L, 10000L)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.getProgressHistory(10000L, 10000L, 1, "")
        Assert.assertEquals(loadingData, viewModel.progressState.value)
    }

    @Test
    fun test_get_progress_history_successState() = runTest {
        val successState = ProgressHistoryState.PROCESSED
        whenever(authManager.currentUser).thenReturn(TestUtils.user)
        whenever(service.retrieveUserSubscriptionById(1))
            .thenReturn(Result.success(TestUtils.subscription))
        whenever(service.getSubscriptionsProgressHistory(1, 1, 10000L, 10000L)).thenReturn(
            Result.success(
                listOf()
            )
        )
        viewModel.getProgressHistory(10000L, 10000L, 1, "December 2022")
        Assert.assertEquals(successState, viewModel.progressState.value)
    }

    @Test
    fun test_get_progress_history_failureState() = runTest {
        val failureState = ProgressHistoryState.FAILURE("Error")
        whenever(authManager.currentUser).thenReturn(TestUtils.user)
        whenever(service.retrieveUserSubscriptionById(1))
            .thenReturn(Result.success(TestUtils.subscription))
        whenever(service.getSubscriptionsProgressHistory(1, 1, 0L, 10000L)).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.getProgressHistory(0L, 10000L, 1, "")
        Assert.assertEquals(failureState, viewModel.progressState.value)
    }
}
