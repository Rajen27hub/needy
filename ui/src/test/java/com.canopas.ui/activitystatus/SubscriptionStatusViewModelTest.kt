package com.canopas.ui.activitystatus

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.utils.Device
import com.canopas.base.ui.model.ProgressTabType
import com.canopas.data.model.RestoreActivityBody
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.goal
import com.canopas.data.utils.TestUtils.Companion.subscription
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class SubscriptionStatusViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: SubscriptionStatusViewModel
    private val device = mock<Device>()
    private val service = mock<NoLonelyService>()
    private val eventBus = mock<EventBus>()
    private val navManager = mock<NavManager>()
    private val appAnalytics = mock<AppAnalytics>()
    private val authManager = mock<AuthManager>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    private val resources = mock<Resources>()
    @Before
    fun setup() {
        initViewModel()
    }

    private fun initViewModel() {
        whenever(device.timeZone()).thenReturn("Asia/Kolkata")
        viewModel = SubscriptionStatusViewModel(
            testDispatcher,
            service,
            eventBus,
            navManager,
            appAnalytics,
            authManager,
            resources
        )
    }

    @Test
    fun test_defaultSubscriptionStatus() = runTest {
        val loadingData = SubscriptionState.LOADING
        whenever(service.retrieveUserSubscriptionById(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(subscription)
        }
        whenever(service.getGoalsBySubscriptionId(subscription.id)).thenReturn(Result.success(listOf(goal)))
        viewModel.displayStatus(
            subscription.id.toString(),
            ""
        )
        verify(appAnalytics).logEvent("view_subscription_detail", null)
        Assert.assertEquals(loadingData, viewModel.state.value)

        whenever(service.retrieveUserSubscriptionById(1)).thenReturn(
            Result.success(subscription)
        )
        whenever(service.getGoalsBySubscriptionId(subscription.id)).thenReturn(Result.success(listOf(goal)))
        viewModel.displayStatus(
            subscription.id.toString(),
            ""
        )
        val successState = SubscriptionState.SUBSCRIBED(subscription, listOf(goal))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_loading_SubscriptionStatus() = runTest {
        val loadingData = SubscriptionState.LOADING
        whenever(service.retrieveUserSubscriptionById(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(subscription)
        }
        viewModel.displayStatus(
            subscription.id.toString(),
            ""
        )
        Assert.assertEquals(loadingData, viewModel.state.value)
    }

    @Test
    fun test_success_SubscriptionStatus() = runTest {
        whenever(service.retrieveUserSubscriptionById(1))
            .thenReturn(Result.success(subscription))
        whenever(service.getGoalsBySubscriptionId(subscription.id)).thenReturn(Result.success(listOf(goal)))
        viewModel.displayStatus(
            subscription.id.toString(),
            ""
        )
        val successState = SubscriptionState.SUBSCRIBED(subscription, listOf(goal))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_success_SubscriptionStatus_byActivityId() = runTest {
        whenever(service.retrieveUserSubscriptionByActivityId(1))
            .thenReturn(Result.success(subscription))
        whenever(service.getGoalsBySubscriptionId(subscription.id)).thenReturn(Result.success(listOf(goal)))
        viewModel.displayStatus(
            "",
            subscription.id.toString()
        )
        val successState = SubscriptionState.SUBSCRIBED(subscription, listOf(goal))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_Failure_SubscriptionStatus_byActivityId() = runTest {
        val failureState = SubscriptionState.FAILURE("Error", false)
        whenever(service.retrieveUserSubscriptionByActivityId(1))
            .thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.displayStatus(
            "",
            subscription.activity.id.toString()
        )
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_Failure_SubscriptionStatus() = runTest {
        val failureState = SubscriptionState.FAILURE("Error", false)
        whenever(service.retrieveUserSubscriptionById(1)).thenReturn(
            Result.failure(RuntimeException("Error"))
        )
        viewModel.displayStatus(
            subscription.id.toString(), ""
        )
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_unsubscribe_activity_loading_state() = runTest {
        val loadingData = SubscriptionState.LOADING
        whenever(service.retrieveUserSubscriptionById(1))
            .thenReturn(Result.success(subscription))
        whenever(service.unsubscribeActivity(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.displayStatus(
            subscription.id.toString(),
            ""
        )

        viewModel.onUnsubscribeClicked()
        Assert.assertEquals(loadingData, viewModel.state.value)
    }

    @Test
    fun test_success_unsubscribe_activity() = runTest {
        val successData = SubscriptionState.UNSUBSCRIBED
        val data = mock<JsonElement>()

        whenever(service.retrieveUserSubscriptionById(1))
            .thenReturn(Result.success(subscription))

        whenever(service.unsubscribeActivity(1))
            .thenReturn(Result.success(data))
        viewModel.displayStatus(
            subscription.id.toString(),
            ""
        )

        viewModel.onUnsubscribeClicked()
        Assert.assertEquals(successData, viewModel.state.value)
    }

    @Test
    fun test_Failure_unsubscribe_activity() = runTest {
        val failureState = SubscriptionState.FAILURE("Error", false)
        whenever(service.retrieveUserSubscriptionById(1))
            .thenReturn(Result.success(subscription))
        whenever(service.unsubscribeActivity(1)).thenReturn(
            Result.failure(RuntimeException("Error"))
        )
        viewModel.displayStatus(
            subscription.id.toString(), ""
        )

        viewModel.onUnsubscribeClicked()
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_onTabChange_showRecentProgressView() {
        viewModel.handleProgressTabSelection(ProgressTabType.PROGRESS_7D)
        verify(appAnalytics).logEvent("tap_subscription_detail_7_days", null)
        Assert.assertEquals(ProgressTabType.PROGRESS_7D, viewModel.progressSelectedTab.value)
    }

    @Test
    fun test_onTabChange_showMonthlyProgressView() {
        viewModel.handleProgressTabSelection(ProgressTabType.PROGRESS_M)
        verify(appAnalytics).logEvent("tap_subscription_detail_monthly", null)
        Assert.assertEquals(ProgressTabType.PROGRESS_M, viewModel.progressSelectedTab.value)
    }

    @Test
    fun test_onTabChange_show_6MonthsProgressView() {
        viewModel.handleProgressTabSelection(ProgressTabType.PROGRESS_6M)
        verify(appAnalytics).logEvent("tap_subscription_detail_6_months", null)
        Assert.assertEquals(ProgressTabType.PROGRESS_6M, viewModel.progressSelectedTab.value)
    }

    @Test
    fun test_onTabChange_showYearlyProgressView() {
        viewModel.handleProgressTabSelection(ProgressTabType.PROGRESS_Y)
        verify(appAnalytics).logEvent("tap_subscription_detail_yearly", null)
        Assert.assertEquals(ProgressTabType.PROGRESS_Y, viewModel.progressSelectedTab.value)
    }

    @Test
    fun test_onTabChange_showAllProgressView() {
        viewModel.handleProgressTabSelection(ProgressTabType.PROGRESS_ALL)
        verify(appAnalytics).logEvent("tap_subscription_detail_all", null)
        Assert.assertEquals(ProgressTabType.PROGRESS_ALL, viewModel.progressSelectedTab.value)
    }

    @Test
    fun test_toggleUnsubscribeActivityAlert() {
        viewModel.toggleUnsubscribeActivityAlert()
        verify(appAnalytics).logEvent("tap_subscription_unsubscribe", null)
        Assert.assertEquals(true, viewModel.showUnsubscribeAlert.value)
    }

    @Test
    fun test_managePopBack() {
        viewModel.managePopBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_navigate_to_notes() {
        viewModel.navigateToNotesView("activity1", "1", 1, false)
        verify(navManager).navigateToAddOrUpdateNotes("activity1", "1", "1", false)
    }

    @Test
    fun test_navigate_to_notesListActivity() {
        viewModel.navigateToNotesListView(1, false, "activity1")
        verify(navManager).navigateToNoteListView(1, false, "activity1")
    }

    @Test
    fun test_get_subscription_onSuccess() = runTest {
        val successState = UserSubscriptionsState.SUCCESS
        whenever(service.getUserSubscriptions()).thenReturn(Result.success(listOf(subscription)))
        viewModel.getUserSubscriptions()
        Assert.assertEquals(successState, viewModel.userSubscriptionState.value)
        val listOfId = listOf(subscription.activity.id)
        Assert.assertEquals(listOfId, viewModel.subscriptionIdList.value)
    }

    @Test
    fun test_get_subscription_onFailure() = runTest {
        val failureState = UserSubscriptionsState.FAILURE("Error")
        whenever(service.getUserSubscriptions()).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.getUserSubscriptions()
        Assert.assertEquals(failureState, viewModel.userSubscriptionState.value)
    }

    @Test
    fun test_get_subscription_loadingState() = runTest {
        val loadingState = UserSubscriptionsState.LOADING
        whenever(service.getUserSubscriptions()).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(500) }
            null
        }
        viewModel.getUserSubscriptions()
        Assert.assertEquals(loadingState, viewModel.userSubscriptionState.value)
    }

    @Test
    fun test_restore_and_delete_activity_OnSuccess() = runTest {
        val successState = ArchivedState.SUCCESS
        val jsonElement = mock<JsonElement>()
        val subId = RestoreActivityBody(1)

        whenever(service.restoreArchivedActivity(subId)).thenReturn(Result.success(jsonElement))
        viewModel.restoreOrDeleteArchivedActivity(1, 1)
        Assert.assertEquals(successState, viewModel.archivedState.value)
    }
    @Test
    fun test_restore_and_delete_activity_OnFailure() = runTest {
        val failureState = ArchivedState.FAILURE("Error")
        val subId = RestoreActivityBody(1)
        whenever(service.restoreArchivedActivity(subId)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.restoreOrDeleteArchivedActivity(1, 1)
        Assert.assertEquals(failureState, viewModel.archivedState.value)
    }

    @Test
    fun test_restore_and_delete_activity_loading_state() = runTest {
        val loadingState = ArchivedState.LOADING
        val subId = RestoreActivityBody(1)
        whenever(service.restoreArchivedActivity(subId)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(500) }
            null
        }
        viewModel.restoreOrDeleteArchivedActivity(subscriptionId = 1, 1)
        Assert.assertEquals(loadingState, viewModel.archivedState.value)
    }
    @Test
    fun test_navigateToBuyPremiums() {
        viewModel.navigateToBuyPremiumView()
        verify(navManager).navigateToBuyPremiumScreen()
    }

    @Test
    fun test_navigate_to_add_goal() {
        viewModel.navigateToAddGoal(subscription.id)
        verify(navManager).navigateToAddGoal(subscription.id)
    }

    @Test
    fun test_navigate_to_edit_goal() {
        viewModel.navigateToEditGoal(goal.id, subscription.id)
        verify(navManager).navigateToEditGoal(subscription.id, goal.id)
    }
}
