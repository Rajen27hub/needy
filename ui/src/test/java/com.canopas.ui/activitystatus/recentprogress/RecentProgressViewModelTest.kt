package com.canopas.ui.activitystatus.recentprogress

import android.content.res.Resources
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.activitystatus.PastCompletionState
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class RecentProgressViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private lateinit var viewModel: RecentProgressViewModel
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    private val resources = mock<Resources>()
    private val service = mock<NoLonelyService>()

    @Before
    fun setup() {
        viewModel = RecentProgressViewModel(appDispatcher = testDispatcher, service, resources)
    }

    @Test
    fun test_on_pastCompletion_Clicked_loading_state() = runTest {
        val loadingData = PastCompletionState.LOADING
        whenever(service.retrieveUserSubscriptionById(10676))
            .thenReturn(Result.success(TestUtils.subscriptionData))
        whenever(service.savePastCompletionActivity(any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.onStart(TestUtils.subscriptionData)
        viewModel.onPastCompletionClicked(TestUtils.recent.date)
        Assert.assertEquals(loadingData, viewModel.pastCompletionState.value)
    }

    @Test
    fun test_SuccessState_whenClick_onPastHabitCompletion() = runTest {
        val successData = PastCompletionState.SUCCESS
        val jsonElement = mock<JsonElement>()
        whenever(service.retrieveUserSubscriptionById(10676))
            .thenReturn(Result.success(TestUtils.subscriptionData))
        whenever(service.savePastCompletionActivity(any())).thenReturn(
            Result.success(jsonElement)
        )
        viewModel.onStart(TestUtils.subscriptionData)
        viewModel.onPastCompletionClicked(TestUtils.recent.date)
        Assert.assertEquals(
            successData, viewModel.pastCompletionState.value
        )
    }

    @Test
    fun test_FailureState_whenClick_onPastHabitCompletion() = runTest {
        val failureState = PastCompletionState.FAILURE("Error")
        whenever(service.retrieveUserSubscriptionById(10676))
            .thenReturn(Result.success(TestUtils.subscriptionData))
        whenever(service.savePastCompletionActivity(any())).thenReturn(
            Result.failure(RuntimeException("Error"))
        )
        viewModel.onStart(TestUtils.subscriptionData)
        viewModel.onPastCompletionClicked(TestUtils.recent.date)
        Assert.assertEquals(
            failureState, viewModel.pastCompletionState.value
        )
    }
}
