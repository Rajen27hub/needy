package com.canopas.ui.notes.managetemplates

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.emptyTemplate
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class ManageTemplatesViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private val services = mock<NoLonelyService>()
    private val appAnalytics = mock<AppAnalytics>()
    private val navManager = mock<NavManager>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )
    private var resources = mock<Resources>()
    private lateinit var viewModel: ManageTemplatesViewModel

    @Before
    fun setup() {
        viewModel = ManageTemplatesViewModel(testDispatcher, services, resources, navManager, appAnalytics)
    }

    @Test
    fun test_fetch_templates_loadingState() = runTest {
        val loadingState = ManageTemplatesState.LOADING
        whenever(services.getNoteTemplates()).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.onStart()
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_fetch_templates_successState() = runTest {
        val successState = ManageTemplatesState.SUCCESS(listOf(emptyTemplate))
        whenever(services.getNoteTemplates()).thenReturn(Result.success(listOf()))
        viewModel.onStart()
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_fetch_templates_failureState() = runTest {
        val failureState = ManageTemplatesState.FAILURE("Error")
        whenever(services.getNoteTemplates()).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.onStart()
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_reset_state() {
        viewModel.resetState()
        Assert.assertEquals(ManageTemplatesState.START, viewModel.state.value)
    }

    @Test
    fun test_popBack() {
        viewModel.popBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_navigateTo_AddTemplate() {
        viewModel.navigateToEditTemplate(emptyTemplate)
        verify(navManager).navigateToAddNoteTemplate()
    }

    @Test
    fun test_navigateTo_EditTemplate() {
        viewModel.navigateToEditTemplate(emptyTemplate.copy(id = 5, title = "title", createdAt = " "))
        verify(navManager).navigateToEditNoteTemplate(5)
    }
}
