package com.canopas.ui.notes.addnotes

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.AddOrUpdateNoteBody
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.emptyTemplate
import com.canopas.data.utils.TestUtils.Companion.note
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class AddNotesViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private val services = mock<NoLonelyService>()
    private val appAnalytics = mock<AppAnalytics>()
    private val navManager = mock<NavManager>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )
    private lateinit var viewModel: AddNotesViewModel
    private var resources = mock<Resources>()
    @Before
    fun setup() {
        viewModel = AddNotesViewModel(testDispatcher, services, navManager, appAnalytics, resources)
    }

    @Test
    fun test_fetch_notes_data_loading_state() = runTest {
        val loadingState = NotesState.LOADING
        whenever(services.getNote(note.id.toString())).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.onStart(notesId = note.id.toString())
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_fetch_notes_data_success_state() = runTest {
        val successState = NotesState.SUCCESS(note)
        whenever(services.getNote(note.id.toString())).thenReturn(Result.success(note))
        viewModel.onStart(notesId = note.id.toString())
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_fetch_notes_data_failure_state() = runTest {
        val successState = NotesState.FAILURE("Error")
        whenever(services.getNote("0")).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.onStart(notesId = "0")
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_fetch_notes_template_loading_state() = runTest {
        val loadingState = NotesState.LOADING
        whenever(services.getNoteTemplateById(any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.onStart(notesId = "", templateId = 0)
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_fetch_notes_template_success_state() = runTest {
        whenever(services.getNoteTemplateById(0)).thenReturn(Result.success(emptyTemplate))
        viewModel.onStart(notesId = "", templateId = 0)
        Assert.assertEquals("Sample Format", viewModel.content.value)
    }

    @Test
    fun test_fetch_notes_template_failure_state() = runTest {
        val failureState = NotesState.FAILURE("Error")
        whenever(services.getNoteTemplateById(0)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.onStart(notesId = "", templateId = 0)
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_on_content_change_value() {
        viewModel.onContentChange("Notes")
        Assert.assertEquals("Notes", viewModel.content.value)
        Assert.assertEquals(true, viewModel.showSaveButton.value)
    }

    @Test
    fun test_reset_state() {
        val resetState = NotesState.START
        viewModel.resetState()
        Assert.assertEquals(resetState, viewModel.state.value)
    }

    @Test
    fun test_popBack() {
        viewModel.popBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_handle_save_button_add_note_loadingState() = runTest {
        val loadingState = NotesState.LOADING
        val notesBody = AddOrUpdateNoteBody(2, "Note", false)
        whenever(services.addNote(notesBody)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.handleSaveButtonClick("2", null, false)
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_handle_save_button_add_note_successState() = runTest {
        val notesBody = AddOrUpdateNoteBody(2, "Note", false)
        val json = mock<JsonElement>()
        whenever(services.addNote(notesBody)).thenReturn(Result.success(json))
        viewModel.handleSaveButtonClick("2", null, false)
        verify(navManager).popBack()
    }

    @Test
    fun test_handle_save_button_add_note_failureState() = runTest {
        val state = NotesState.FAILURE("Error")
        val notesBody = AddOrUpdateNoteBody(0, "", false)
        whenever(services.addNote(notesBody)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.handleSaveButtonClick("0", null, false)
        Assert.assertEquals(state, viewModel.state.value)
    }

    @Test
    fun test_handle_save_button_update_note_loadingState() = runTest {
        val loadingState = NotesState.LOADING
        val notesBody = AddOrUpdateNoteBody(2, "Notes", false)
        whenever(services.updateNote(note.id.toString(), notesBody)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.handleSaveButtonClick("2", note, false)
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_handle_save_button_update_note_successState() = runTest {
        val notesBody = AddOrUpdateNoteBody(2, "Notes", false)
        whenever(services.updateNote(note.id.toString(), notesBody)).thenReturn(Result.success(note))
        viewModel.handleSaveButtonClick("2", note, false)
        verify(navManager).popBack()
    }

    @Test
    fun test_handle_save_button_update_note_failureState() = runTest {
        val state = NotesState.FAILURE("Error")
        val notesBody = AddOrUpdateNoteBody(0, "", false)
        whenever(services.updateNote(note.id.toString(), notesBody)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.handleSaveButtonClick("0", note, false)
        Assert.assertEquals(state, viewModel.state.value)
    }

    @Test
    fun test_toggle_delete_alert_dialog() {
        viewModel.toggleDeleteAlert()
        Assert.assertEquals(true, viewModel.showDeleteAlert.value)
    }

    @Test
    fun test_delete_note_loadingState() = runTest {
        val loadingState = NotesState.LOADING
        whenever(services.deleteNote(note.id.toString())).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.deleteNote(note)
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_delete_note_successState() = runTest {
        whenever(services.deleteNote(note.id.toString())).thenReturn(Result.success(mock()))
        viewModel.deleteNote(note)
        verify(navManager).popBack()
    }

    @Test
    fun test_delete_note_failureState() = runTest {
        val state = NotesState.FAILURE("Error")
        whenever(services.deleteNote(note.id.toString())).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.deleteNote(note)
        Assert.assertEquals(state, viewModel.state.value)
    }
}
