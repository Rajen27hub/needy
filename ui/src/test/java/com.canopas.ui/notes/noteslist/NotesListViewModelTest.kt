package com.canopas.ui.notes.noteslist

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.model.Notes
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.note
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class NotesListViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val services = mock<NoLonelyService>()
    private val appAnalytics = mock<AppAnalytics>()
    private val navManager = mock<NavManager>()
    private var resources = mock<Resources>()
    private lateinit var viewModel: NotesListViewModel
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    private fun initViewModel() {
        viewModel = NotesListViewModel(services, navManager, testDispatcher, appAnalytics, resources)
    }

    @Before
    fun setup() {
        initViewModel()
    }

    @Test
    fun test_popBack() {
        viewModel.onBackClick()
        verify(navManager).popBack()
    }

    @Test
    fun test_reset_state() {
        val resetState = NotesListState.START
        viewModel.resetState()
        assertEquals(resetState, viewModel.state.value)
    }

    @Test
    fun test_LoadingState_fetchNotesList() = runTest {
        val loadingData = NotesListState.LOADING
        whenever(services.getActivityNotes(1, false, skip = 0, limit = 30)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(emptyList())
        }
        viewModel.onViewResume(1, false, 0)
        assertEquals(loadingData, viewModel.state.value)
    }

    @Test
    fun test_fetch_notesList_LoaderSuccessState() = runTest {
        val loadingState = LoaderState.LOADING
        withContext(Dispatchers.IO) {
            delay(2000)
        }
        whenever(
            services.getActivityNotes(
                1,
                false,
                skip = 10,
                limit = 30
            )
        ).thenReturn(Result.success(listOf(note)))
        val notes = emptyList<Notes>()
        viewModel.onViewResume(1, false, 10)
        initViewModel()
        assertEquals(loadingState, viewModel.pageLoader.value)
        assertEquals(notes, viewModel.notes.value)
    }

    @Test
    fun test_FailureState_fetchNotesList() = runTest(dispatchTimeoutMs = 10000) {
        initViewModel()
        whenever(services.getActivityNotes(1, false, skip = 0, limit = 30)).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.onViewResume(1, false, 0)
        val failureState = NotesListState.FAILURE("Error")
        assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_navigate_to_notes() {
        viewModel.navigateToNotesView("activity1", "1", 1, false)
        verify(navManager).navigateToAddOrUpdateNotes("activity1", "1", "1", false)
    }

    @Test
    fun test_fetch_notesList_SuccessState() = runTest {
        val finishState = LoaderState.FINISH
        val successState = NotesListState.SUCCESS(listOf(note))
        whenever(
            services.getActivityNotes(
                1,
                false,
                skip = 10,
                limit = 30
            )
        ).thenReturn(Result.success(listOf(note)))
        viewModel.onViewResume(1, false, 10)
        assertEquals(successState, viewModel.state.value)
        assertEquals(finishState, viewModel.pageLoader.value)
    }

    @Test
    fun test_loading_state_lastItem_visible_false() = runTest {
        val loadingState = LoaderState.LOADING
        whenever(
            services.getActivityNotes(
                1,
                false,
                skip = 10,
                limit = 30
            )
        ).thenReturn(Result.success(listOf(note)))
        viewModel.onViewResume(1, false, 10)
        viewModel.onLastItemVisible(1, 1, false, 10)
        initViewModel()
        assertEquals(loadingState, viewModel.pageLoader.value)
        assertEquals(true, viewModel.hasMoreItems.value)
    }

    @Test
    fun test_loading_state_lastItem_visible_true() = runTest {
        whenever(
            services.getActivityNotes(
                1,
                false,
                skip = 10,
                limit = 30
            )
        ).thenReturn(Result.success(emptyList()))
        viewModel.onViewResume(1, false, 10)
        viewModel.onLastItemVisible(1, 1, false, 10)
        val notes = emptyList<Notes>()
        assertEquals(notes, viewModel.notes.value)
        assertEquals(false, viewModel.hasMoreItems.value)
    }
}
