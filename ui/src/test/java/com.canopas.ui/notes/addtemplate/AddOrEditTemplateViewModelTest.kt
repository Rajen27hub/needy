package com.canopas.ui.notes.addtemplate

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.addOrUpdateTemplateBody
import com.canopas.data.utils.TestUtils.Companion.emptyTemplate
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class AddOrEditTemplateViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private val services = mock<NoLonelyService>()
    private val appAnalytics = mock<AppAnalytics>()
    private val navManager = mock<NavManager>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )
    private var resources = mock<Resources>()
    private lateinit var viewModel: AddOrEditTemplateViewModel

    @Before
    fun setup() {
        viewModel = AddOrEditTemplateViewModel(
            testDispatcher,
            services,
            navManager,
            appAnalytics,
            resources
        )
    }

    @Test
    fun test_fetchNoteTemplate_loadingState() = runTest {
        val loadingState = TemplateState.LOADING
        whenever(services.getNoteTemplateById(any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.onStart(1)
        assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_fetchNoteTemplate_successState() = runTest {
        val successState = TemplateState.FETCH_SUCCESS(emptyTemplate.copy(id = 1))
        whenever(services.getNoteTemplateById(any())).thenReturn(Result.success(emptyTemplate.copy(id = 1)))
        viewModel.onStart(1)
        assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_fetchNoteTemplate_failureState() = runTest {
        val failure = TemplateState.FAILURE("Error", false)
        whenever(services.getNoteTemplateById(any())).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.onStart(1)
        assertEquals(failure, viewModel.state.value)
    }

    @Test
    fun test_addNoteTemplate_loadingState() = runTest {
        whenever(services.createNoteTemplate(any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.addNoteTemplate()
        assertEquals(true, viewModel.showActionLoader.value)
    }

    @Test
    fun test_addNoteTemplate_successState() = runTest {
        whenever(services.createNoteTemplate(any())).thenReturn(Result.success(mock()))
        viewModel.addNoteTemplate()
        verify(navManager).popBack()
    }

    @Test
    fun test_addNoteTemplate_failureState() = runTest {
        val failure = TemplateState.FAILURE("Error", false)
        whenever(services.createNoteTemplate(any())).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.addNoteTemplate()
        assertEquals(failure, viewModel.state.value)
    }

    @Test
    fun test_editNoteTemplate_loadingState() = runTest {
        whenever(services.updateNoteTemplate(1, addOrUpdateTemplateBody)).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.editNoteTemplate(1)
        assertEquals(true, viewModel.showActionLoader.value)
    }

    @Test
    fun test_editNoteTemplate_successState() = runTest {
        whenever(services.updateNoteTemplate(1, addOrUpdateTemplateBody)).thenReturn(Result.success(mock()))
        viewModel.editNoteTemplate(1)
        verify(navManager).popBack()
    }

    @Test
    fun test_editNoteTemplate_failureState() = runTest {
        val failure = TemplateState.FAILURE("Error", false)
        whenever(services.updateNoteTemplate(1, addOrUpdateTemplateBody)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.editNoteTemplate(1)
        assertEquals(failure, viewModel.state.value)
    }

    @Test
    fun test_on_title_changed() {
        viewModel.onTemplateTitleChange("title")
        assertEquals("title", viewModel.templateTitle.value)
    }

    @Test
    fun test_on_format_changed() {
        viewModel.onTemplateFormatChange("title")
        assertEquals("title", viewModel.templateFormat.value)
    }

    @Test
    fun test_popBack() {
        viewModel.popBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_openDeleteDialog() {
        viewModel.openDeleteTemplateDialog()
        assertEquals(true, viewModel.openDeleteDialog.value.getContentIfNotHandled())
    }

    @Test
    fun test_closeDeleteDialog() {
        viewModel.closeDeleteTemplateDialog()
        assertEquals(false, viewModel.openDeleteDialog.value.getContentIfNotHandled())
    }

    @Test
    fun test_deleteNoteTemplate_loadingState() = runTest {
        val loadingState = TemplateState.LOADING
        whenever(services.deleteNoteTemplate(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            null
        }
        viewModel.deleteTemplate(1)
        assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_deleteNoteTemplate_successState() = runTest {
        whenever(services.deleteNoteTemplate(1)).thenReturn(Result.success(mock()))
        viewModel.deleteTemplate(1)
        verify(navManager).popBack()
    }

    @Test
    fun test_deleteNoteTemplate_failureState() = runTest {
        val failure = TemplateState.FAILURE("Error", false)
        whenever(services.deleteNoteTemplate(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.deleteTemplate(1)
        assertEquals(failure, viewModel.state.value)
    }
}
