package com.canopas.ui.notificationPermission

import androidx.core.app.NotificationManagerCompat
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.utils.TestUtils
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.home.settings.buypremiumsubscription.DEVICE_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class NotificationPermissionViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()
    private lateinit var viewModel: NotificationPermissionViewModel
    private val analytics = mock<AppAnalytics>()
    private val testDispatcher = AppDispatcher(IO = UnconfinedTestDispatcher())
    private val navManager = mock<NavManager>()
    private val authManager = mock<AuthManager>()
    private val notificationManagerCompat = mock<NotificationManagerCompat>()

    @Before
    fun setUp() {
        viewModel = NotificationPermissionViewModel(
            testDispatcher,
            analytics,
            navManager,
            notificationManagerCompat,
            authManager
        )
    }

    @Test
    fun test_open_notification_dialog() = runTest {
        viewModel.openNotificationDialog()
        Assert.assertEquals(true, viewModel.openNotificationDialog.value)
    }

    @Test
    fun test_onAllowButtonClick() = runTest {
        viewModel.onRedirectToSettings()
        Assert.assertEquals(true, viewModel.navigateToAppSettings.value.getContentIfNotHandled())
        Assert.assertEquals(false, viewModel.openNotificationDialog.value)
    }

    @Test
    fun test_onLaterButtonClick() = runTest {
        viewModel.closeNotificationDialog()
        Assert.assertEquals(false, viewModel.openNotificationDialog.value)
        verify(navManager).popSignInScreens()
    }

    @Test
    fun test_true_status_OfNotification() {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        Assert.assertEquals(true, viewModel.isNotificationOn())
    }

    @Test
    fun test_false_status_OfNotification() {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(false)
        Assert.assertEquals(false, viewModel.isNotificationOn())
    }

    @Test
    fun test_onContinueButtonClick() {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(false)
        viewModel.onContinueButtonClick()
        Assert.assertEquals(true, viewModel.openNotificationDialog.value)
    }

    @Test
    fun test_onNotNowButtonClick() {
        viewModel.onNotNowButtonClicked()
        verify(navManager).popSignInScreens()
    }

    @Test
    fun test_popBack() {
        viewModel.popBack()
        verify(navManager).popSignInScreens()
    }

    @Test
    fun test_navigateToProfileScreen_when_required_data_is_empty() = runTest {
        whenever(authManager.currentUser).thenReturn(TestUtils.user3)
        viewModel.popBack()
        verify(navManager).navigateToProfileScreen(showBackArrow = false, promptPremium = true)
    }

    @Test
    fun test_navigateToBuyPremiumScreen_after_notificationPermission() = runTest {
        whenever(authManager.currentUser).thenReturn(TestUtils.user4)
        viewModel.popBack()
        verify(navManager).popSignInScreens()
        verify(navManager).navigateToBuyPremiumScreen(showBackArrow = false, showDismissButton = true)
    }

    @Test
    fun test_navigateToBuyPremiumScreen_for_device_limitation() = runTest {
        whenever(authManager.currentUser).thenReturn(TestUtils.user5)
        viewModel.popBack()
        verify(navManager).navigateToBuyPremiumScreen(showBackArrow = false, textType = DEVICE_LIMIT_TEXT_TYPE)
    }
}
