package com.canopas.ui.signinmethods

import androidx.core.app.NotificationManagerCompat
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthResponse
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.auth.ServiceError
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.model.LOGIN_TYPE_GOOGLE
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.utils.Device
import com.canopas.data.utils.TestUtils.Companion.user3
import com.canopas.data.utils.TestUtils.Companion.user4
import com.canopas.data.utils.TestUtils.Companion.user5
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.home.settings.buypremiumsubscription.DEVICE_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import com.google.android.gms.auth.api.identity.BeginSignInRequest
import com.google.android.gms.auth.api.identity.SignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.crashlytics.FirebaseCrashlytics
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class SignInMethodsViewModelTest {
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val authManager = mock<AuthManager>()
    private val device = mock<Device>()
    private val testDispatcher = AppDispatcher(IO = UnconfinedTestDispatcher())
    private val navManager = mock<NavManager>()
    private val firebaseCrashlytics = mock<FirebaseCrashlytics>()
    private val appAnalytics = mock<AppAnalytics>()
    private val oneTapClient = mock<SignInClient>()
    private val signInRequest = mock<BeginSignInRequest>()
    private val notificationManagerCompat = mock<NotificationManagerCompat>()
    private val signInClient = mock<GoogleSignInClient>()
    private lateinit var viewModel: SignInMethodsViewModel

    @Before
    fun setUp() {
        viewModel = SignInMethodsViewModel(
            authManager,
            device,
            testDispatcher,
            navManager,
            firebaseCrashlytics,
            appAnalytics,
            oneTapClient,
            notificationManagerCompat,
            signInRequest,
            signInClient
        )
        whenever(device.getId()).thenReturn("1")
        whenever(device.getAppVersionCode()).thenReturn(20000)
        whenever(device.deviceModel()).thenReturn("MD1")
        whenever(device.getDeviceOsVersion()).thenReturn("1")
    }

    @Test
    fun test_toggle_loading_state() {
        viewModel.toggleLoadingState()
        Assert.assertEquals(SignInState.LOADING, viewModel.state.value)
    }

    @Test
    fun test_reset_state() {
        viewModel.resetState()
        Assert.assertEquals(SignInState.START, viewModel.state.value)
    }

    @Test
    fun test_loadingState_signInWithToken() = runTest {
        val loadingState = SignInState.LOADING
        val authResponse = AuthResponse(AuthState.VERIFIED(mock()), ServiceError.NONE)
        val loginRequest = LoginRequest(
            1, "1",
            20000, "MD1", "1", provider_firebase_id_token = "abcd", login_type = LOGIN_TYPE_GOOGLE
        )
        whenever(authManager.login(loginRequest, false)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            authResponse
        }
        viewModel.signInWithToken("abcd", LOGIN_TYPE_GOOGLE)
        Assert.assertEquals(loadingState, viewModel.state.value)
    }

    @Test
    fun test_successState_signInWithToken() = runTest {
        val successState = SignInState.SUCCESS
        val authResponse = AuthResponse(AuthState.VERIFIED(mock()), ServiceError.NONE)
        val loginRequest = LoginRequest(
            1, "1",
            20000, "MD1", "1", provider_firebase_id_token = "abcd", login_type = LOGIN_TYPE_GOOGLE
        )
        whenever(authManager.login(loginRequest, false))
            .thenReturn(authResponse)

        viewModel.signInWithToken("abcd", LOGIN_TYPE_GOOGLE)
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_true_status_OfNotification() {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        Assert.assertEquals(true, viewModel.isNotificationOn())
    }

    @Test
    fun test_false_status_OfNotification() {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(false)
        Assert.assertEquals(false, viewModel.isNotificationOn())
    }

    @Test
    fun test_navigateToNotificationPermissionView() = runTest {
        viewModel.isAboveAndroidS.value = true
        viewModel.proceedLoginSuccessNavigation()
        verify(navManager).navigateToNotificationPermissionView()
    }

    @Test
    fun test_navigateToProfileScreen_when_required_data_is_empty() = runTest {
        whenever(authManager.currentUser).thenReturn(user3)
        viewModel.proceedLoginSuccessNavigation()
        verify(navManager).popSignInScreens()
        verify(navManager).navigateToProfileScreen(showBackArrow = false, promptPremium = true)
    }

    @Test
    fun test_navigateToBuyPremiumScreen_after_login() = runTest {
        whenever(authManager.currentUser).thenReturn(user4)
        viewModel.proceedLoginSuccessNavigation()
        verify(navManager).navigateToBuyPremiumScreen(showBackArrow = false, showDismissButton = true)
    }

    @Test
    fun test_navigateToBuyPremiumScreen_for_device_limitation() = runTest {
        whenever(authManager.currentUser).thenReturn(user5)
        viewModel.proceedLoginSuccessNavigation()
        verify(navManager).navigateToBuyPremiumScreen(showBackArrow = false, textType = DEVICE_LIMIT_TEXT_TYPE)
    }

    @Test
    fun test_failureState_signInWithToken() = runTest {
        val failureState = SignInState.FAILURE(ServiceError.SERVERERROR.errorMessage)
        val loginRequest = LoginRequest(
            1, "1",
            20000, "MD1", "1", provider_firebase_id_token = "abcd", login_type = LOGIN_TYPE_GOOGLE
        )
        whenever(authManager.login(loginRequest, false)).thenThrow(RuntimeException(ServiceError.SERVERERROR.errorMessage))

        viewModel.signInWithToken("abcd", LOGIN_TYPE_GOOGLE)
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_popBack() {
        viewModel.popBackStack()
        verify(navManager).popBack()
    }

    @Test
    fun test_navigateToSignInWithPhone() {
        viewModel.navigateToPhoneLogin()
        verify(navManager).navigateToPhoneLogin()
    }

    @Test
    fun test_navigateToCreateAccount() {
        viewModel.navigateToPhoneLogin()
        verify(navManager).navigateToPhoneLogin()
    }
}
