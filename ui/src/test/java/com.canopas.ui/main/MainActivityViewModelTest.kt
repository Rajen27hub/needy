import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.main.MainActivityViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock

class MainActivityViewModelTest {

    @ExperimentalCoroutinesApi
    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: MainActivityViewModel
    private val noLonelySharePreferences = mock<NoLonelyPreferences>()
    private val authManager = mock<AuthManager>()

    @Before
    fun setUp() {
        viewModel = MainActivityViewModel(noLonelySharePreferences, authManager)
    }

    @Test
    fun test_manageSplashScreen() {
        viewModel.completeSplashScreen()
        Assert.assertEquals(false, viewModel.showSplashView.value)
    }
}
