package com.canopas.ui.habit.configureActivity

import android.content.res.Resources
import androidx.core.app.NotificationManagerCompat
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.utils.Device
import com.canopas.base.ui.model.DurationTabType
import com.canopas.base.ui.model.RepeatTabStyle
import com.canopas.data.model.SubscribeBody
import com.canopas.data.model.SubscribedResponse
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.activityData
import com.canopas.data.utils.TestUtils.Companion.customActivityRequest
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.habit.configureactivity.ConfigureActivityState
import com.canopas.ui.habit.configureactivity.ConfigureActivityViewModel
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.greenrobot.eventbus.EventBus
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.whenever
import java.util.Calendar

@ExperimentalCoroutinesApi
class ConfigureActivityViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: ConfigureActivityViewModel

    private val device = mock<Device>()

    private val service = mock<NoLonelyService>()
    private val eventBus = mock<EventBus>()
    private val calendar = mock<Calendar>()
    private val notificationManagerCompat = mock<NotificationManagerCompat>()
    private val navManager = mock<NavManager>()
    private val analytics = mock<AppAnalytics>()
    private val resources = mock<Resources>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        whenever(calendar.get(Calendar.HOUR_OF_DAY)).thenReturn(12)
        whenever(calendar.get(Calendar.MINUTE)).thenReturn(0)
        whenever(calendar.get(Calendar.AM_PM)).thenReturn(Calendar.AM)
        whenever(calendar.get(Calendar.HOUR)).thenReturn(12)

        viewModel =
            ConfigureActivityViewModel(analytics, calendar, navManager, notificationManagerCompat, device, testDispatcher, service, eventBus, resources)
        whenever(device.timeZone()).thenReturn("Asia/Kolkata")
    }

    @Test
    fun test_subscribeActivity_whenOnStartJourney_loading() = runTest {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        val processing = ConfigureActivityState.PROCESSING
        whenever(service.subscribeActivity(any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.handleDoneBtnClick(activityData.id, "", "", "")
        Assert.assertEquals(processing, viewModel.configureActivityState.value)
    }

    @Test
    fun test_success_subscribeActivity_whenOnStartJourney() = runTest {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        val successData = ConfigureActivityState.CONFIGURED
        viewModel.setActivityTime("08:30")
        val subscription = SubscribeBody(
            activity_id = activityData.id,
            timezone = device.timeZone(),
            is_prompted = false,
            repeat_type = 1,
            repeat_on = "[1,2,3]",
            activity_time = "12:0",
            activity_duration = viewModel.durationTabType.value.duration,
            reminders = listOf(),
            show_note_on_complete = viewModel.promptForNote.value
        )
        whenever(service.subscribeActivity(subscription)).thenReturn(
            Result.success(SubscribedResponse(0))
        )
        viewModel.handleDoneBtnClick(activityData.id, "", "", "")
        Assert.assertEquals(successData, viewModel.configureActivityState.value)
    }

    @Test
    fun test_Failure_subscribeActivity_whenOnStartJourney() = runTest {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        val failureState = ConfigureActivityState.FAILURE("Error")
        viewModel.setActivityTime("08:30")
        whenever(service.subscribeActivity(any())).thenReturn(
            Result.failure(RuntimeException("Error"))
        )
        viewModel.handleDoneBtnClick(activityData.id, "", "", "")
        Assert.assertEquals(failureState, viewModel.configureActivityState.value)
    }

    @Test
    fun test_loading_onAddCustomActivity() = runTest {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        val loadingState = ConfigureActivityState.PROCESSING
        viewModel.setActivityTime("08:30")
        val data = SubscribedResponse(0)
        whenever(service.createCustomActivity(any())).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(5000)
            }
            Result.success(data)
        }
        viewModel.handleDoneBtnClick(0, "title", "description", "goal")
        Assert.assertEquals(loadingState, viewModel.configureActivityState.value)
    }

    @Test
    fun test_successState_onAddCustomActivity() = runTest {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        viewModel.setActivityTime("08:30")
        val successState = ConfigureActivityState.CONFIGURED
        val data = SubscribedResponse(0)
        whenever(service.createCustomActivity(customActivityRequest)).thenReturn(
            Result.success(
                data
            )
        )
        viewModel.handleDoneBtnClick(0, "title", "description", "goal")
        Assert.assertEquals(successState, viewModel.configureActivityState.value)
    }

    @Test
    fun test_open_notification_dialog_ifPermissionNotAllowed_whenUserStartJourney() = runTest {
        viewModel.openNotificationDialog()
        Assert.assertEquals(true, viewModel.openNotificationDialog.value)
    }

    @Test
    fun test_open_notification_dialog_ifPermissionNotAllowed_whenUserHandleStartJourney() =
        runTest {
            whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(false)
            viewModel.handleDoneBtnClick(
                activityData.id,
                "",
                "",
                ""
            )
            analytics.logEvent("tap_habit_configure_subscribe", null)
            Assert.assertEquals(true, viewModel.openNotificationDialog.value)
        }

    @Test
    fun test_close_notification_dialog_whenDismissDialog() = runTest {
        viewModel.closeNotificationDialog()
        Assert.assertEquals(false, viewModel.openNotificationDialog.value)
    }

    @Test
    fun test_addNotificationPermission_onConfirmButtonClicked() = runTest {
        viewModel.addNotificationPermission()
        Assert.assertEquals(true, viewModel.navigateToAppSettings.value.getContentIfNotHandled())
        Assert.assertEquals(false, viewModel.openNotificationDialog.value)
    }

    @Test
    fun test_true_status_OfNotification() {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(true)
        Assert.assertEquals(true, viewModel.isNotificationOn())
    }

    @Test
    fun test_false_status_OfNotification() {
        whenever(notificationManagerCompat.areNotificationsEnabled()).thenReturn(false)
        Assert.assertEquals(false, viewModel.isNotificationOn())
    }

    @Test
    fun test_toggle_remindMeValue() {
        viewModel.setActivityTimeSelector(true)
        Assert.assertEquals(true, viewModel.showActivityTimeSelector.value)
    }

    @Test
    fun test_handleDurationTabSelection() {
        viewModel.handleDurationTabSelection(DurationTabType.DURATION_1_HOUR)
        Assert.assertEquals(DurationTabType.DURATION_1_HOUR, viewModel.durationTabType.value)
        Assert.assertEquals(60, viewModel.durationTabType.value.duration)
    }

    @Test
    fun test_handleRepeatTabSelection() {
        viewModel.handleRepeatTabSelection(RepeatTabStyle.REPEAT_WEEKLY)
        Assert.assertEquals(RepeatTabStyle.REPEAT_WEEKLY, viewModel.repeatTabType.value)
    }

    @Test
    fun test_toggleReminderSelection() {
        viewModel.toggleReminderSelection(2)
        Assert.assertEquals(mutableListOf(1, 0, 1), viewModel.selectedReminderTypes.value)
    }

    @Test
    fun test_toggleNotesPrompt() {
        viewModel.toggleNotesPrompt(false)
        Assert.assertEquals(false, viewModel.promptForNote.value)
    }

    @Test
    fun test_toggleWeeklySelection() {
        viewModel.selectedWeeklyDays.value = List(7) { 0 }
        viewModel.toggleWeeklySelection(2)
        Assert.assertEquals(mutableListOf(0, 0, 1, 0, 0, 0, 0), viewModel.selectedWeeklyDays.value)
    }
}
