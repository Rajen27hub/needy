package com.canopas.ui.habit.successstories

import android.content.res.Resources
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.successStory
import com.canopas.data.utils.TestUtils.Companion.user
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class SuccessStoriesViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: SuccessStoriesViewModel

    private val service = mock<NoLonelyService>()
    private val navManager = mock<NavManager>()

    private val authManager = mock<AuthManager>()
    private val resources = mock<Resources>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel =
            SuccessStoriesViewModel(testDispatcher, service, navManager, authManager, resources)
    }

    @Test
    fun test_defaultSuccessStoryStatus_byActivityId() = runTest {
        val loadingData = (SuccessStoryState.LOADING)
        whenever(service.retrieveSuccessStoriesByActivityId(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(listOf(successStory))
        }
        viewModel.retrieveSuccessStory("1")
        Assert.assertEquals(loadingData, viewModel.successStoryState.value)

        whenever(service.retrieveSuccessStoriesByActivityId(1)).thenReturn(
            Result.success(
                listOf(
                    successStory
                )
            )
        )
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        whenever(service.retrieveSuccessStoriesByUserId())
            .thenReturn(Result.success(listOf(successStory)))
        viewModel.retrieveSuccessStory("1")
        val successState = SuccessStoryState.SUCCESS(listOf(successStory))
        Assert.assertEquals(successState, viewModel.successStoryState.value)
    }

    @Test
    fun test_loadingSuccessStoryStatus_retrieveSuccessStory_byActivityId() = runTest {
        val loadingData = (SuccessStoryState.LOADING)
        whenever(service.retrieveSuccessStoriesByActivityId(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.retrieveSuccessStory("1")
        Assert.assertEquals(loadingData, viewModel.successStoryState.value)
    }

    @Test
    fun test_FailureSuccessStoryStatus_retrieveSuccessStories_byActivityId() = runTest {
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(mock()))
        whenever(service.retrieveSuccessStoriesByActivityId(1)).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.retrieveSuccessStory("1")
        val failureState = SuccessStoryState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.successStoryState.value)
    }

    @Test
    fun test_SuccessStatus_retrieveSuccessStories_byActivityId() = runTest {
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(mock()))
        whenever(service.retrieveSuccessStoriesByActivityId(1))
            .thenReturn(Result.success(listOf(successStory)))
        viewModel.retrieveSuccessStory("1")
        val successState = SuccessStoryState.SUCCESS(listOf(successStory))
        Assert.assertEquals(successState, viewModel.successStoryState.value)
    }

    @Test
    fun test_SuccessStatus_retrieveUserSuccessStoriesByUserId() = runTest {
        whenever(authManager.authState).thenReturn(AuthState.VERIFIED(user))
        whenever(service.retrieveSuccessStoriesByUserId()).thenReturn(
            Result.success(
                listOf(
                    successStory
                )
            )
        )
        viewModel.retrieveSuccessStory("")
        val successState = SuccessStoryState.SUCCESS(listOf(successStory))
        Assert.assertEquals(successState, viewModel.successStoryState.value)
    }

    @Test
    fun test_Success_retrieveUserSuccessStoriesByUserId_withEmptyList() = runTest {
        whenever(authManager.authState).thenReturn(AuthState.ANONYMOUS(user))
        whenever(service.retrieveSuccessStoriesByUserId()).thenReturn(Result.success(emptyList()))
        viewModel.retrieveSuccessStory("")
        val successState = SuccessStoryState.SUCCESS(emptyList())
        Assert.assertEquals(successState, viewModel.successStoryState.value)
    }

    @Test
    fun test_popBackStack() = runTest {
        viewModel.managePopBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_onSuccessStoryClicked_openStoryDetail_NotAllow_EditStory() {
        viewModel.onSuccessStoryClicked(1, false)
        verify(navManager).navigateToSuccessStoryDetail(1, false)
    }

    @Test
    fun test_onSuccessStoryClicked_openStoryDetail_Allow_EditStory() {
        viewModel.onSuccessStoryClicked(1, true)
        verify(navManager).navigateToSuccessStoryDetail(1, true)
    }

    @Test
    fun test_isEditModeEnable_false() {
        viewModel.isEditModeEnable(false)
        Assert.assertEquals(false, viewModel.isEditModeOption)
    }

    @Test
    fun test_isEditModeEnable_true() {
        viewModel.isEditModeEnable(true)
        Assert.assertEquals(true, viewModel.isEditModeOption)
    }
}
