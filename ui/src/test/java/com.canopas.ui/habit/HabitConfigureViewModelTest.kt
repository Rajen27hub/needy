package com.canopas.ui.habit

import android.content.res.Resources
import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.model.AppDispatcher
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.activityData
import com.canopas.data.utils.TestUtils.Companion.activityData2
import com.canopas.data.utils.TestUtils.Companion.successStory
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.home.settings.buypremiumsubscription.ACTIVITY_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class HabitConfigureViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var viewModel: HabitConfigureViewModel

    private val service = mock<NoLonelyService>()
    private val navManager = mock<NavManager>()
    private val authManager = mock<AuthManager>()
    private val analytics = mock<AppAnalytics>()
    private val preferences = mock<NoLonelyPreferences>()
    private val resources = mock<Resources>()

    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel =
            HabitConfigureViewModel(
                testDispatcher, service, authManager, navManager, analytics, preferences, resources
            )
    }

    @Test
    fun test_defaultHabitStatus() = runTest {
        val loadingData = (HabitState.LOADING)
        whenever(service.getActivityById(1)).thenReturn(Result.success(activityData))

        whenever(service.retrieveSuccessStoriesByActivityId(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(listOf(successStory))
        }

        viewModel.onStart(1)
        analytics.logEvent("view_habit_configure", null)
        Assert.assertEquals(loadingData, viewModel.state.value)

        whenever(service.getActivityById(1)).thenReturn(Result.success(activityData))
        whenever(service.retrieveSuccessStoriesByActivityId(1)).thenReturn(
            Result.success(
                listOf(
                    successStory
                )
            )
        )
        viewModel.onStart(1)
        analytics.logEvent("view_habit_configure", null)
        val successState = HabitState.SUCCESS(activityData, listOf(successStory))
        Assert.assertEquals(successState, viewModel.state.value)
    }

    @Test
    fun test_loadingHabitStatus_getActivityById_fetchData() = runTest {
        val loadingData = (HabitState.LOADING)
        whenever(service.getActivityById(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.onStart(1)
        Assert.assertEquals(loadingData, viewModel.state.value)
    }

    @Test
    fun test_loadingHabitStatus_retrieveSuccessStoryByActivityId_fetchData() = runTest {
        val loadingData = (HabitState.LOADING)
        whenever(service.retrieveSuccessStoriesByActivityId(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            null
        }
        viewModel.onStart(1)
        Assert.assertEquals(loadingData, viewModel.state.value)
    }

    @Test
    fun test_successHabitStatus_fetchData() = runTest {
        val successState = setSuccessState()
        Assert.assertEquals(successState, viewModel.state.value)
    }

    private suspend fun setSuccessState(): HabitState.SUCCESS {
        whenever(service.getActivityById(1)).thenReturn(Result.success(activityData))
        whenever(service.retrieveSuccessStoriesByActivityId(1)).thenReturn(
            Result.success(
                listOf(
                    successStory
                )
            )
        )
        viewModel.onStart(1)
        return HabitState.SUCCESS(activityData, listOf(successStory))
    }

    @Test
    fun test_FailureHabitStatus_getActivityById_fetchData() = runTest {
        whenever(service.getActivityById(1)).thenReturn(Result.failure(RuntimeException("Error")))
        viewModel.onStart(1)
        val failureState = HabitState.FAILURE("Error", false)
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_FailureHabitStatus_retrieveSuccessStoriesByActivityId_fetchData() = runTest {
        whenever(service.getActivityById(1)).thenReturn(Result.success(activityData))
        whenever(service.retrieveSuccessStoriesByActivityId(1)).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.onStart(1)
        val failureState = HabitState.FAILURE("Error", false)
        Assert.assertEquals(failureState, viewModel.state.value)
    }

    @Test
    fun test_unAuthorized_user_navigateToLogin_whenOnHandleStartJourney() = runTest {
        setSuccessState()
        whenever(authManager.authState).thenReturn(AuthState.UNAUTHORIZED)
        viewModel.handleStartJourney()
        analytics.logEvent("tap_habit_configure_subscribe", null)
        verify(navManager).navigateToSignInMethods()
    }

    @Test
    fun test_onStartJourney() {
        viewModel.isGoalsEnabled.value = false
        viewModel.onStartJourney(
            activityData.id,
            activityData.name,
            activityData.default_activity_time!!,
            activityData.default_activity_duration!!
        )
        verify(navManager).navigateToConfigureActivity(
            activityData.id,
            activityData.name,
            activityData.default_activity_time!!,
            activityData.default_activity_duration!!
        )
    }

    @Test
    fun test_navigateToSuccessStory() {
        viewModel.navigateToSuccessStory(1)
        verify(navManager).navigateToSuccessStory(1)
    }

    @Test
    fun test_onSuccessStoryClicked_navigateToStoryDetail() = runTest {
        viewModel.onSuccessStoryClicked(1)
        verify(navManager).navigateToSuccessStoryDetail(1, false)
    }

    @Test
    fun test_NavigateToActivityStatus_onHandleStartJourney() = runTest {
        whenever(service.getActivityById(activityData2.id)).thenReturn(Result.success(activityData2))
        whenever(service.retrieveSuccessStoriesByActivityId(2)).thenReturn(
            Result.success(
                listOf(
                    successStory
                )
            )
        )
        viewModel.onStart(activityData2.id)
        viewModel.handleStartJourney()
        analytics.logEvent("tap_habit_configure_subscribe", null)
        verify(navManager).navigateToActivityStatusByActivityId(activityData2.id)
    }

    @Test
    fun test_updateVideoAutoPlayDelay() {
        viewModel.updateVideoAutoplayDelay(10000)
        Assert.assertEquals(10000, viewModel.videoAutoplayDelayInMillis.value)
    }

    @Test
    fun test_NavigateToLoginScreen() {
        viewModel.navigateToLogin()
        verify(navManager).navigateToSignInMethods()
    }

    @Test
    fun test_NavigateToBuyPremiumScreen() {
        viewModel.openBuyPremiumScreen()
        verify(navManager).navigateToBuyPremiumScreen(
            showBackArrow = true,
            showDismissButton = false, textType = ACTIVITY_LIMIT_TEXT_TYPE
        )
    }

    @Test
    fun test_managePopBack() {
        viewModel.managePopBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_navigateToConfigureScreenOnSubscribingActivity() {
        viewModel.navigateToConfigureActivity(
            activityData.id,
            activityData.color2,
            activityData.default_activity_time!!,
            activityData.default_activity_duration!!
        )
        verify(navManager).navigateToConfigureActivity(
            activityData.id,
            activityData.color2,
            activityData.default_activity_time!!,
            activityData.default_activity_duration!!
        )
    }
}
