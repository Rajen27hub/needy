package com.canopas.ui.customactivity

import com.canopas.base.data.analytics.AppAnalytics
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.auth.AuthState
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.home.settings.buypremiumsubscription.ACTIVITY_LIMIT_TEXT_TYPE
import com.canopas.ui.navigation.NavManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class CustomActivityViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    lateinit var viewModel: CustomActivityViewModel
    private val navManager = mock<NavManager>()
    private val sharePreferences = mock<NoLonelyPreferences>()
    private val authManager = mock<AuthManager>()
    private val appAnalytics = mock<AppAnalytics>()

    @Before
    fun setup() {
        viewModel = CustomActivityViewModel(
            navManager,
            authManager,
            sharePreferences,
            appAnalytics
        )
    }

    @Test
    fun test_titleText_change() {
        viewModel.onTitleChange("title")
        Assert.assertEquals("title", viewModel.title.value)
    }

    @Test
    fun test_managePopBack() {
        viewModel.managePopBack()
        verify(navManager).popBack()
    }

    @Test
    fun test_unAuthorized_user_navigateToLogin_whenClickOnStartJourney() {
        whenever(authManager.authState).thenReturn(AuthState.UNAUTHORIZED)
        viewModel.onTitleChange("Dummy")
        viewModel.onStartJourneyButtonClicked(0)
        verify(navManager).navigateToSignInMethods()
    }

    @Test
    fun test_enableSaveButton() {
        viewModel.onTitleChange("Custom Activity")
        Assert.assertEquals("Custom Activity", viewModel.title.value)
    }

    @Test
    fun test_NavigateToBuyPremiumScreen() {
        viewModel.openBuyPremiumScreen()
        verify(navManager).navigateToBuyPremiumScreen(textType = ACTIVITY_LIMIT_TEXT_TYPE)
    }
}
