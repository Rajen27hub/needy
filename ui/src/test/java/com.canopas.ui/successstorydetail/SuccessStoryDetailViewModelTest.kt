package com.canopas.ui.successstorydetail

import android.content.res.Resources
import com.canopas.base.data.auth.AuthManager
import com.canopas.base.data.model.AppDispatcher
import com.canopas.data.rest.NoLonelyService
import com.canopas.data.utils.TestUtils.Companion.activityData
import com.canopas.data.utils.TestUtils.Companion.successStory
import com.canopas.ui.MainCoroutineRule
import com.canopas.ui.navigation.NavManager
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.withContext
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class SuccessStoryDetailViewModelTest {

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private val navManager = mock<NavManager>()

    private lateinit var viewModel: SuccessStoryDetailViewModel
    private val service = mock<NoLonelyService>()
    private val authManager = mock<AuthManager>()
    private var resources = mock<Resources>()
    private val testDispatcher = AppDispatcher(
        IO = UnconfinedTestDispatcher()
    )

    @Before
    fun setup() {
        viewModel =
            SuccessStoryDetailViewModel(
                navManager,
                service,
                testDispatcher,
                authManager,
                resources
            )
    }

    @Test
    fun test_popBackStack() = runTest {
        viewModel.managePopBackStack()
        verify(navManager).popBack()
    }

    @Test
    fun test_loadingStoryDetailState_onFetchSuccessStoryDetail() = runTest {
        val loadingData = (StoryDetailState.LOADING)
        whenever(service.retrieveSuccessStoryDetailById(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(successStory)
        }
        viewModel.fetchSuccessStoryDetail(1)
        Assert.assertEquals(loadingData, viewModel.storyDetailState.value)
    }

    @Test
    fun test_successStoryDetailState_onFetchSuccessStoryDetail() = runTest {
        whenever(service.retrieveSuccessStoryDetailById(1)).thenReturn(Result.success(successStory))
        viewModel.fetchSuccessStoryDetail(1)
        val successState = StoryDetailState.SUCCESS(successStory)
        Assert.assertEquals(successState, viewModel.storyDetailState.value)
    }

    @Test
    fun test_failureStoryDetailState_onFetchSuccessStoryDetail() = runTest {
        whenever(service.retrieveSuccessStoryDetailById(1)).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.fetchSuccessStoryDetail(1)
        val failureState = StoryDetailState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.storyDetailState.value)
    }

    @Test
    fun test_loadingStoryDetailState_onDeleteSuccessStory() = runTest {
        val loadingData = (StoryDetailState.LOADING)
        val jsonElement = Mockito.mock(JsonElement::class.java)
        whenever(service.deleteSuccessStory(1)).doSuspendableAnswer {
            withContext(Dispatchers.IO) { delay(5000) }
            Result.success(jsonElement)
        }
        viewModel.deleteSuccessStory(1)
        Assert.assertEquals(loadingData, viewModel.storyDetailState.value)
    }

    @Test
    fun test_successStoryDetailState_onDeleteSuccessStory() = runTest {
        val jsonElement = Mockito.mock(JsonElement::class.java)
        whenever(service.deleteSuccessStory(1)).thenReturn(Result.success(jsonElement))
        viewModel.deleteSuccessStory(1)
        val successState = StoryDetailState.DELETE
        Assert.assertEquals(successState, viewModel.storyDetailState.value)
    }

    @Test
    fun test_failureStoryDetailState_onDeleteSuccessStory() = runTest {
        whenever(service.deleteSuccessStory(1)).thenReturn(
            Result.failure(
                RuntimeException("Error")
            )
        )
        viewModel.deleteSuccessStory(1)
        val failureState = StoryDetailState.FAILURE("Error")
        Assert.assertEquals(failureState, viewModel.storyDetailState.value)
    }

    @Test
    fun test_open_delete_dialog_whenDeleteSuccessStory() {
        viewModel.openStoryDeleteDialog()
        Assert.assertEquals(true, viewModel.openDeleteDialog.value)
    }

    @Test
    fun test_close_delete_dialog_whenDismissDialog() {
        viewModel.closeStoryDeleteDialog()
        Assert.assertEquals(false, viewModel.openDeleteDialog.value)
    }

    @Test
    fun test_onEditButtonClick() {
        viewModel.onEditButtonClick(activityData, 1)
        verify(navManager).navigateToUpdateSuccessStory(1, "yoga", 1)
    }

    @Test
    fun test_onReportButtonClick() {
        viewModel.openSuccessStoryFeedbackScreen("1")
        verify(navManager).navigateToFeedback("1")
    }
}
