package com.canopas.base.data.model

const val DEVICE_TYPE = 1

data class LoginRequest(
    var device_type: Int = DEVICE_TYPE,
    var device_id: String,
    var app_version: Long,
    var device_name: String,
    var os_version: String,
    var firebase_id_token: String? = null,
    var phone: String? = null,
    var provider_firebase_id_token: String? = null,
    var login_type: Int = LOGIN_TYPE_PHONE
)
