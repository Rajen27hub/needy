package com.canopas.base.data.model

import androidx.compose.ui.graphics.Color

const val GREEN_PROGRESS = 80
const val DEFAULT_RECENT_PROGRESS = 0
const val UNIX_TO_DATE_MULTIPLIER = 1000

data class SubscriptionCardDetails(
    var recentProgress: Int,
    var recentDaysCount: String,
    var cardBgColor: Color,
    var progressTextColor: Color,
    var chartTitleHeading: String,
    var chartTitleBody: String
)

data class RecentDaysHistory(
    var day: String,
    var isCompleted: Boolean,
    var isCurrentDay: Boolean,
    var icon: Int,
    var tint: Color
)

data class Progress(
    var recent: List<Recent>,
    var total: Total
)

data class Total(
    var completed_days: Int,
    var remaining_days: Int,
    var missed_days: Int,
    var progress: Float
)

data class Recent(
    var date: Long,
    var completed: Boolean
)
