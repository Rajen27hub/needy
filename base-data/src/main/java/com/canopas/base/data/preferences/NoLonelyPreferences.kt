package com.canopas.base.data.preferences

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.booleanPreferencesKey
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.longPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import com.canopas.base.data.model.UserAccount
import com.google.gson.Gson
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

const val PREF_NO_LONELY = "no_lonely_preferences"

@Singleton
class NoLonelyPreferences @Inject constructor(
    @Named(PREF_NO_LONELY) private val preferencesDataStore: DataStore<Preferences>
) {

    companion object {
        val KEY_NO_LONELY_SHOW_WELCOME_SCREEN = booleanPreferencesKey("show_welcome_screen")
        val KEY_NO_LONELY_SHOW_WAKE_UP_SCREEN = booleanPreferencesKey("show_wake_up_screen")
        val KEY_NO_LONELY_SHOW_KICK_START_SCREEN = booleanPreferencesKey("show_kick_start_screen")

        val KEY_NO_LONELY_IS_FCM_REGISTERED = booleanPreferencesKey("is_fcm_registered")

        val KEY_NO_LONELY_USER_TOTAL_SUBSCRIPTIONS =
            intPreferencesKey("users_total_number_of_subscriptions")

        val KEY_NO_LONELY_COMPLETED_SUBSCRIPTIONS_COUNT =
            intPreferencesKey("completed_subscriptions_count")

        val KEY_NO_LONELY_RATING_PROMPT_SHOWN_TIME = longPreferencesKey("rating_prompt_shown_time")

        val KEY_NO_LONELY_USER_POINTS = intPreferencesKey("user_points")
        val KEY_USER_JSON = stringPreferencesKey("user_account")

        val KEY_NO_LONELY_IS_NOTES_TIPS_SHOWN = booleanPreferencesKey("is_notes_tips_shown")
        val KEY_NO_LONELY_IS_DARK_MODE_ENABLED = booleanPreferencesKey("is_dark_mode_enabled")
        val KEY_NO_LONELY_IS_IN_APP_REVIEW_PROMPT_SHOWN =
            booleanPreferencesKey("is_in_app_review_prompt_shown")

        val KEY_NO_LONELY_SHOW_HEADER_FIRST_COMPLETION =
            booleanPreferencesKey("is_first_completion_header_show")
        val KEY_NO_LONELY_SHOW_HEADER_FIRST_ACTIVITY =
            booleanPreferencesKey("is_first_activity_header_show")
        val KEY_NO_LONELY_SHOW_HEADER_EMPTY_ACTIVITY =
            booleanPreferencesKey("is_empty_activity_header_show")

        private val RATING_PROMPT_INTERVAL =
            TimeUnit.MILLISECONDS.convert(60, TimeUnit.DAYS)

        const val SUBSCRIPTIONS_COUNT_FOR_RATING_PROMPT = 21
        const val SUBSCRIPTIONS_COUNT_FOR_IN_APP_RATING_PROMPT = 11
        const val SUBSCRIPTIONS_COUNT_FOR_NOTES_TIPS = 3
    }

    var currentUser: UserAccount?
        get() = runBlocking {
            preferencesDataStore.data.first()[KEY_USER_JSON]?.let {
                Gson().fromJson(it, UserAccount::class.java)
            }
        }
        set(newUser) = runBlocking {
            if (newUser == null) {
                preferencesDataStore.edit {
                    it.remove(KEY_USER_JSON)
                }
            } else {
                preferencesDataStore.edit { preferences ->
                    preferences[KEY_USER_JSON] = Gson().toJson(newUser)
                }
            }
        }

    suspend fun setIsWelcomeScreenShown(isScreenShow: Boolean) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_SHOW_WELCOME_SCREEN] = isScreenShow
        }
    }

    suspend fun setIsWakeUpScreenShown(isScreenShow: Boolean) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_SHOW_WAKE_UP_SCREEN] = isScreenShow
        }
    }

    suspend fun setIsKickStartScreenShown(isScreenShow: Boolean) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_SHOW_KICK_START_SCREEN] = isScreenShow
        }
    }

    suspend fun setIsFCMRegistered(isFCMRegistered: Boolean) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_IS_FCM_REGISTERED] = isFCMRegistered
        }
    }

    suspend fun setUserSubscriptionsCount(userSubscriptions: Int) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_USER_TOTAL_SUBSCRIPTIONS] = userSubscriptions
        }
    }

    suspend fun setCompletedActivitiesCount(completedActivities: Int) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_COMPLETED_SUBSCRIPTIONS_COUNT] = completedActivities
        }
    }

    suspend fun setUserPoints(points: Int) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_USER_POINTS] = points
        }
    }

    suspend fun setInAppReviewPromptShown(isPrompted: Boolean) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_IS_IN_APP_REVIEW_PROMPT_SHOWN] = isPrompted
        }
    }

    suspend fun getUserPoints(): Int {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_USER_POINTS] ?: 11
    }

    suspend fun setRatingPromptShownTime(millis: Long) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_RATING_PROMPT_SHOWN_TIME] = millis
        }
    }

    private suspend fun getRatingPromptShownTime(): Long {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_RATING_PROMPT_SHOWN_TIME] ?: 0L
    }

    suspend fun shouldShowAppRatingAlert(): Boolean {
        val initialDialogRemaining = (getRatingPromptShownTime() == 0L) &&
            (getCompletedActivitiesCount() >= SUBSCRIPTIONS_COUNT_FOR_RATING_PROMPT)

        val timeToShowAgain =
            (getRatingPromptShownTime() != 0L) && (getRatingPromptShownTime() + RATING_PROMPT_INTERVAL < System.currentTimeMillis())

        return initialDialogRemaining || timeToShowAgain
    }

    suspend fun isWelcomeScreenShown(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_SHOW_WELCOME_SCREEN] ?: false
    }

    suspend fun isWakeUpScreenShown(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_SHOW_WAKE_UP_SCREEN] ?: false
    }

    suspend fun isKickStartScreenShown(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_SHOW_KICK_START_SCREEN] ?: false
    }

    suspend fun isFCMRegistered(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_IS_FCM_REGISTERED] ?: false
    }

    suspend fun getUserSubscriptionCount(): Int {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_USER_TOTAL_SUBSCRIPTIONS] ?: 0
    }

    suspend fun getCompletedActivitiesCount(): Int {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_COMPLETED_SUBSCRIPTIONS_COUNT] ?: 0
    }

    suspend fun setIsNotesTipsShown(isNotesTipsShown: Boolean) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_IS_NOTES_TIPS_SHOWN] = isNotesTipsShown
        }
    }

    suspend fun setShowFirstActivityHeader(show: Boolean) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_SHOW_HEADER_FIRST_ACTIVITY] = show
        }
    }

    suspend fun shouldShowFirstActivityHeader(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_SHOW_HEADER_FIRST_ACTIVITY] ?: true
    }

    suspend fun setShowFirstCompletionHeader(show: Boolean) {
        preferencesDataStore.edit { preference ->
            preference[KEY_NO_LONELY_SHOW_HEADER_FIRST_COMPLETION] = show
        }
    }

    suspend fun shouldShowFirstCompletionHeader(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_SHOW_HEADER_FIRST_COMPLETION] ?: true
    }

    suspend fun shouldShowEmptyActivityHeader(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_SHOW_HEADER_EMPTY_ACTIVITY] ?: true
    }

    suspend fun setShowEmptyActivityHeader(show: Boolean) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_SHOW_HEADER_EMPTY_ACTIVITY] = show
        }
    }

    suspend fun setIsDarkModeEnabled(isDarkModeEnabled: Boolean) {
        preferencesDataStore.edit { preferences ->
            preferences[KEY_NO_LONELY_IS_DARK_MODE_ENABLED] = isDarkModeEnabled
        }
    }

    suspend fun getIsDarkModeEnabled(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_IS_DARK_MODE_ENABLED] ?: false
    }

    suspend fun getIsDarkModeSet(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_IS_DARK_MODE_ENABLED] != null
    }

    suspend fun getIsInAppReviewPromptShown(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_IS_IN_APP_REVIEW_PROMPT_SHOWN]
            ?: false
    }

    suspend fun shouldShowInAppReview(): Boolean {
        return !(getIsInAppReviewPromptShown()) &&
            (getCompletedActivitiesCount() >= SUBSCRIPTIONS_COUNT_FOR_IN_APP_RATING_PROMPT)
    }

    suspend fun isNotesTipsShown(): Boolean {
        return preferencesDataStore.data.first()[KEY_NO_LONELY_IS_NOTES_TIPS_SHOWN] ?: false
    }

    suspend fun shouldShowNotesTipsView(): Boolean {
        return getCompletedActivitiesCount() >= SUBSCRIPTIONS_COUNT_FOR_NOTES_TIPS &&
            !isNotesTipsShown()
    }
}
