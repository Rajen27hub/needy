package com.canopas.base.data.exception

import android.content.res.Resources
import com.canopas.base.data.R
import com.canopas.base.data.auth.AUTH_REQUEST_CODE_UNAUTHORIZED
import okhttp3.Request
import okio.Timeout
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber
import java.io.IOException

const val LIMITED_ACCESS = "limited_access"
const val MAX_LIMIT_REACHED = "max_limit_reached"
const val ALREADY_COMPLETED = "already_completed"
const val ALREADY_SUBSCRIBED = "already_subscribed"
const val LINK_CREATED_ALREADY = "link_created_already"
const val COMMUNITY_ADMIN_AUTHORISED = "community_admin_authorised"
const val NO_INTERNET_CONNECTION = "no_internet_connection"

class ResultCall<T>(val delegate: Call<T>) :
    Call<Result<T>> {

    override fun enqueue(callback: Callback<Result<T>>) {
        delegate.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    callback.onResponse(
                        this@ResultCall,
                        Response.success(
                            response.code(),
                            Result.success(response.body()!!)
                        )
                    )
                } else {
                    Timber.e("ResultCall unsuccessful with code:${response.code()}, body:${response.errorBody()}")
                    if (response.code() == 422) {
                        val errorMessage = ErrorResponse(response).reason
                        callback.onResponse(
                            this@ResultCall,
                            Response.success(
                                Result.failure(
                                    when (errorMessage) {
                                        LIMITED_ACCESS -> APIError.LimitedAccess
                                        MAX_LIMIT_REACHED -> APIError.MaxLimitReached
                                        ALREADY_COMPLETED -> APIError.AlreadyCompleted
                                        ALREADY_SUBSCRIBED -> APIError.AlreadySubscribed
                                        LINK_CREATED_ALREADY -> APIError.LinkCreatedAlready
                                        COMMUNITY_ADMIN_AUTHORISED -> APIError.CommunityAdminAuthorised
                                        else -> APIError.UnknownError(response.raw(), null)
                                    }
                                )
                            )
                        )
                    } else {
                        callback.onResponse(
                            this@ResultCall,
                            Response.success(
                                Result.failure(
                                    APIError.UnknownError(
                                        response.raw(),
                                        null
                                    )
                                )
                            )
                        )
                    }

                    if (response.code() == AUTH_REQUEST_CODE_UNAUTHORIZED) {
                        delegate.cancel()
                        return
                    }
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                Timber.e(t, "Result Call Error - onFailure")
                callback.onResponse(
                    this@ResultCall,
                    Response.success(
                        Result.failure(
                            if (t is IOException) {
                                APIError.NetworkError
                            } else {
                                APIError.UnknownError(error = t)
                            }
                        )
                    )
                )
            }
        })
    }

    override fun isExecuted(): Boolean {
        return delegate.isExecuted
    }

    override fun execute(): Response<Result<T>> {
        return Response.success(Result.success(delegate.execute().body()!!))
    }

    override fun cancel() {
        delegate.cancel()
    }

    override fun isCanceled(): Boolean {
        return delegate.isCanceled
    }

    override fun clone(): Call<Result<T>> {
        return ResultCall(delegate.clone())
    }

    override fun request(): Request {
        return delegate.request()
    }

    override fun timeout(): Timeout {
        return delegate.timeout()
    }
}

sealed class APIError(message: String?) : Throwable(message) {
    object NetworkError : APIError(NO_INTERNET_CONNECTION)
    object LimitedAccess : APIError(LIMITED_ACCESS)
    object MaxLimitReached : APIError(MAX_LIMIT_REACHED)
    object AlreadyCompleted : APIError(ALREADY_COMPLETED)
    object AlreadySubscribed : APIError(ALREADY_SUBSCRIBED)
    object LinkCreatedAlready : APIError(LINK_CREATED_ALREADY)
    object CommunityAdminAuthorised : APIError(COMMUNITY_ADMIN_AUTHORISED)
    data class UnknownError(val response: okhttp3.Response? = null, val error: Throwable?) : APIError(error?.localizedMessage)

    fun getMessage(resources: Resources): String {
        return when (this) {
            is NetworkError -> {
                resources.getString(R.string.no_internet_connection)
            }
            is UnknownError -> {
                resources.getString(R.string.unknown_error)
            }
            is LimitedAccess -> {
                resources.getString(R.string.limited_access)
            }
            is MaxLimitReached -> {
                resources.getString(R.string.max_limit_reached)
            }
            is AlreadyCompleted -> {
                resources.getString(R.string.already_completed)
            }
            is AlreadySubscribed -> {
                resources.getString(R.string.already_subscribed)
            }
            is LinkCreatedAlready -> {
                resources.getString(R.string.link_already_created)
            }
            is CommunityAdminAuthorised -> {
                resources.getString(R.string.community_admin_authorised)
            }
        }
    }
}

fun Throwable.toUserError(resources: Resources): String {
    return if (this is APIError) {
        getMessage(resources)
    } else {
        localizedMessage!!
    }
}

class ErrorResponse(response: Response<*>) {
    val reason: String? = response.errorBody()?.string()?.let { json ->
        try {
            val jsonObject = JSONObject(json)
            jsonObject.getString("reason")
        } catch (e: Exception) {
            null
        }
    }
}
