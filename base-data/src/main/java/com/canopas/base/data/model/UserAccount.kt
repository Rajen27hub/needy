package com.canopas.base.data.model

import com.canopas.base.data.utils.Utils
import com.google.gson.annotations.SerializedName

const val GENDER_MALE = 1
const val GENDER_FEMALE = 2
const val GENDER_NONE = 0
const val LOGIN_TYPE_PHONE = 1
const val LOGIN_TYPE_GOOGLE = 2
const val LOGIN_TYPE_FACEBOOK = 3

const val FREE_USER = 0
const val PREMIUM_USER = 1

data class UserAccount(

    @SerializedName("id")
    var id: Int,

    @SerializedName("user_id")
    var user_id: Int,

    @SerializedName("community_id")
    var community_id: Int,

    @SerializedName("role")
    var role: Int,

    @SerializedName("first_name")
    var firstName: String?,

    @SerializedName("last_name")
    var lastName: String?,

    @SerializedName("username")
    var userName: String?,

    @SerializedName("phone")
    var phone: String?,

    @SerializedName("email")
    var email: String?,

    @SerializedName("profile_image_url")
    var profileImageUrl: String? = null,

    @SerializedName("is_active")
    var isActive: Boolean = true,

    @SerializedName("is_anonymous")
    var isAnonymous: Boolean = true,

    @SerializedName("created_at")
    var createdAt: String,

    @SerializedName("updated_at")
    var updatedAt: String,

    @SerializedName("gender")
    var gender: Int = GENDER_NONE,

    @SerializedName("user_type")
    var userType: Int,

    @SerializedName("login_type")
    var loginType: Int = LOGIN_TYPE_PHONE,

    @SerializedName("is_verified")
    var isVerified: Boolean = true,

    @SerializedName("product_id")
    var productId: String,

    @SerializedName("session")
    var session: Session?,

    @SerializedName("notification_settings")
    var notificationsSetting: Notifications,

    @SerializedName("leader_board_visibility")
    var leaderBoardVisibility: Boolean = true,

    var activity_share_prompted: Boolean = false,

    @SerializedName("firebase_access_key")
    var firebaseAccessKey: String = "",

    var timezone: String? = null,

    @SerializedName("invited_to_community")
    var invitedToCommunity: Boolean = false
) {
    override fun equals(other: Any?): Boolean {
        if (other == null || javaClass != other.javaClass) return false
        val user = other as UserAccount
        return when {
            id != user.id -> false
            profileImageUrl != user.profileImageUrl -> false
            userName != user.userName -> false
            firstName != user.firstName -> false
            lastName != user.lastName -> false
            phone != user.phone -> false
            email != user.email -> false
            gender != user.gender -> false
            isActive != user.isActive -> false
            isAnonymous != user.isAnonymous -> false
            else -> true
        }
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + (firstName?.hashCode() ?: 0)
        result = 31 * result + (userName?.hashCode() ?: 0)
        result = 31 * result + (phone?.hashCode() ?: 0)
        result = 31 * result + (email?.hashCode() ?: 0)
        result = 31 * result + (profileImageUrl?.hashCode() ?: 0)
        result = 31 * result + isActive.hashCode()
        result = 31 * result + isAnonymous.hashCode()
        result = 31 * result + createdAt.hashCode()
        result = 31 * result + updatedAt.hashCode()
        result = 31 * result + gender
        return result
    }

    fun hasFirstname() = !firstName.isNullOrEmpty()
    fun hasValidEmail() = Utils.isValidEmail(email)
    fun allowPhoneEdit() = loginType != LOGIN_TYPE_PHONE

    fun isPremium(): Boolean {
        return userType == PREMIUM_USER
    }

    val fullName get() = "$firstName ${lastName ?: ""}".trim()

    fun profileImageChar(): String {
        return if (fullName.isNotEmpty()) {
            fullName.trim().first().uppercase()
        } else {
            "?"
        }
    }
}

data class Notifications(
    @SerializedName("push_notification")
    val pushNotification: Boolean,
    @SerializedName("mail")
    val mail: Boolean,
    @SerializedName("inactivity_notification")
    val inactivityNotification: Boolean,
    @SerializedName("inactivity_mail")
    val inactivityMail: Boolean
)
