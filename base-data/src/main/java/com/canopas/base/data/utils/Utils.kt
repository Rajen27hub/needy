package com.canopas.base.data.utils

import android.util.Base64
import java.io.UnsupportedEncodingException

class Utils {

    companion object {
        fun isValidEmail(text: CharSequence?): Boolean {
            return (
                !(text == null || text.isEmpty()) &&
                    text.matches(EMAIL_ADDRESS.toRegex())
                )
        }

        const val EMAIL_ADDRESS = "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
            "\\@" +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
            "(" +
            "\\." +
            "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
            ")+"

        fun encodeToBase64(input: String): String {
            return Base64.encodeToString(input.toByteArray(), Base64.NO_WRAP)
        }

        fun decodeFromBase64(description: String): String {
            val dataDec = Base64.decode(description, Base64.DEFAULT)
            var decodedString = ""
            try {
                decodedString = String(dataDec)
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
            }
            return decodedString
        }
    }
}
