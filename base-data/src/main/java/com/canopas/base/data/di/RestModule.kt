package com.canopas.base.data.di

import com.canopas.base.data.Config.BASE_URL
import com.canopas.base.data.exception.ResultCallAdapterFactory
import com.canopas.base.data.interceptor.AuthRequestInterceptor
import com.canopas.base.data.interceptor.LoggingInterceptor
import com.canopas.base.data.rest.AuthServices
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class RestModule {

    @Named("AuthHttpClient")
    @Singleton
    @Provides
    fun provideAuthHttpClient(
        apiRequestInterceptor: AuthRequestInterceptor,
        loggingInterceptor: LoggingInterceptor
    ): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(apiRequestInterceptor)
            .addInterceptor(loggingInterceptor)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()

    @Named("AuthRetrofit")
    @Singleton
    @Provides
    fun provideAuthRetrofit(@Named("AuthHttpClient") okHttpClient: OkHttpClient): Retrofit =
        Retrofit.Builder().client(okHttpClient)
            .addCallAdapterFactory(ResultCallAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(BASE_URL)
            .build()

    @Singleton
    @Provides
    fun provideAuthService(@Named("AuthRetrofit") retrofit: Retrofit): AuthServices =
        retrofit.create(AuthServices::class.java)
}
