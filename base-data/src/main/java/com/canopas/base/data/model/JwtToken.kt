package com.canopas.base.data.model

import com.google.gson.annotations.SerializedName

data class JwtToken(
    @SerializedName("access_token")
    var accessToken: String? = null,
    @SerializedName("refresh_token")
    var refreshToken: String? = null
)
