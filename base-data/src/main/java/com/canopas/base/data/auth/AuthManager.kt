package com.canopas.base.data.auth

import android.content.SharedPreferences
import com.canopas.base.data.model.DEVICE_TYPE
import com.canopas.base.data.model.LOGIN_TYPE_PHONE
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.model.UserAccount
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.rest.AuthServices
import com.canopas.base.data.utils.Device
import dagger.Lazy
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

const val PREF_AUTH = "auth_preferences"
const val AUTH_REQUEST_CODE_UNAUTHORIZED = 401
const val DEFAULT_STATUS_CODE = 0
const val DEFAULT_USER_POINTS = 11

const val ACCESS_TOKEN = "access-token"
const val REFRESH_TOKEN = "refresh-token"

@Singleton
class AuthManager @Inject constructor(
    private val authServices: Lazy<AuthServices>,
    private val noLonelyPreferences: NoLonelyPreferences,
    @Named(PREF_AUTH) private val sharedPreferences: SharedPreferences,
    private val device: Device,
) {

    private val mutex = Mutex()
    private var isRefreshingToken = false

    private val authStateChangeListeners = HashSet<AuthStateChangeListener>()

    private val hasRefreshToken: Boolean
        get() = refreshToken != null

    val authState: AuthState
        get() {
            val user = currentUser ?: return AuthState.UNAUTHORIZED
            if (refreshToken.isNullOrEmpty()) {
                return AuthState.UNAUTHORIZED
            }

            return if (user.isAnonymous) AuthState.ANONYMOUS(user) else AuthState.VERIFIED(user)
        }

    fun addListener(authStateChangeListener: AuthStateChangeListener) {
        this.authStateChangeListeners.add(authStateChangeListener)
    }

    fun removeListener(authStateChangeListener: AuthStateChangeListener) {
        this.authStateChangeListeners.remove(authStateChangeListener)
    }

    var accessToken: String?
        get() = sharedPreferences.getString(ACCESS_TOKEN, "")
        set(value) {
            sharedPreferences.edit()
                .putString(ACCESS_TOKEN, value).apply()
        }

    var refreshToken: String?
        get() = sharedPreferences.getString(REFRESH_TOKEN, "")
        set(value) {
            sharedPreferences.edit()
                .putString(REFRESH_TOKEN, value).apply()
        }

    var currentUser: UserAccount?
        get() {
            return noLonelyPreferences.currentUser
        }
        set(newUser) {
            noLonelyPreferences.currentUser = newUser
        }

    suspend fun login(loginRequest: LoginRequest, anonymous: Boolean = true): AuthResponse {
        try {
            val response = if (anonymous) authServices.get().anonymousLogin(loginRequest)
            else authServices.get().loginV2(loginRequest)
            if (!response.isSuccessful) {
                Timber.e("Service request error %s ", response)
            }
            when (response.code()) {
                200 -> {
                    val refreshToken = response.headers()[REFRESH_TOKEN]
                    val accessToken = response.headers()[ACCESS_TOKEN]
                    if (!refreshToken.isNullOrEmpty() && !accessToken.isNullOrEmpty()) {
                        this@AuthManager.refreshToken = refreshToken
                        this@AuthManager.accessToken = accessToken
                    }
                    currentUser = response.body()
                    notifyAuthState()
                    return AuthResponse(authState, ServiceError.NONE, response.code())
                }
                AUTH_REQUEST_CODE_UNAUTHORIZED -> {
                    notifyAuthState()
                    return AuthResponse(authState, ServiceError.UNAUTHORIZED, response.code())
                }
                else -> {
                    notifyAuthState()
                    return AuthResponse(authState, ServiceError.SERVERERROR, response.code())
                }
            }
        } catch (e: Exception) {
            Timber.e(e, "Network error")
            return if (e is IOException) {
                notifyAuthState()
                AuthResponse(authState, ServiceError.NETWORKERROR)
            } else {
                notifyAuthState()
                AuthResponse(authState, ServiceError.SERVERERROR)
            }
        }
    }

    private fun notifyAuthState() {
        authStateChangeListeners.forEach { listener -> listener.onAuthStateChanged(authState) }
    }

    fun refreshAccessToken() = runBlocking {
        var shouldRefresh = false
        if (!isRefreshingToken) {
            isRefreshingToken = true
            shouldRefresh = true
            Timber.d("ShouldRefresh true")
        } else {
            Timber.d("Waiting for token refresh")
        }

        mutex.withLock {
            if (shouldRefresh && hasRefreshToken) {
                try {
                    val response = authServices.get().refreshAccessToken(refreshToken!!)
                    when {
                        response.code() == AUTH_REQUEST_CODE_UNAUTHORIZED -> {
                            logOutUser()
                            Timber.d("Auth logout %s", currentUser)
                        }
                        response.isSuccessful -> {
                            response.body()?.let {
                                refreshToken = it.refreshToken
                                accessToken = it.accessToken
                            }
                            Timber.d("Access token refreshed")
                        }
                        else -> {
                            Timber.w("Access token refresh error %s", response)
                        }
                    }
                } catch (e: Exception) {
                    Timber.w(e, "Access token refresh error")
                }
            }
            isRefreshingToken = false
        }
    }

    fun logOutUser() {
        noLonelyPreferences.currentUser = null
        accessToken = null
        refreshToken = null
    }

    suspend fun resetSession() {
        logOutUser()
        anonymousLogin()
    }

    suspend fun anonymousLogin() {
        noLonelyPreferences.setUserPoints(DEFAULT_USER_POINTS)
        noLonelyPreferences.setIsFCMRegistered(false)
        val loginRequest = LoginRequest(
            DEVICE_TYPE,
            device.getId(),
            device.getAppVersionCode(),
            device.deviceModel(),
            device.getDeviceOsVersion(),
            login_type = LOGIN_TYPE_PHONE
        )
        login(loginRequest, true)
    }
}

sealed class AuthState {
    object UNAUTHORIZED : AuthState()
    data class ANONYMOUS(val userAccount: UserAccount) : AuthState()
    data class VERIFIED(val userAccount: UserAccount) : AuthState()

    val isAuthorised: Boolean
        get() = this !is UNAUTHORIZED

    val isVerified: Boolean
        get() = this is VERIFIED

    val isPremium: Boolean
        get() = this is VERIFIED && this.userAccount.isPremium()

    val isFree: Boolean
        get() = !isPremium
}

enum class ServiceError(var errorMessage: String = "") {
    NONE,
    UNAUTHORIZED("You are an unauthorised user."),
    SERVERERROR("Something went wrong!"),
    NETWORKERROR("No internet connection!");
}

data class AuthResponse constructor(
    val authState: AuthState,
    val error: ServiceError,
    val statusCode: Int = DEFAULT_STATUS_CODE
) {
    fun hasError(): Boolean {
        return error != ServiceError.NONE
    }

    val errorMessage = error.errorMessage
}

interface AuthStateChangeListener {
    fun onAuthStateChanged(authState: AuthState)
}
