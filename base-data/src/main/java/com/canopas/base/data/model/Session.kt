package com.canopas.base.data.model

import com.google.gson.annotations.SerializedName

data class Session(

    @SerializedName("id")
    val id: Int,

    @SerializedName("user_id")
    val userId: Int,

    @SerializedName("device_type")
    val deviceType: Int = DEVICE_TYPE,

    @SerializedName("device_id")
    val deviceId: String,

    @SerializedName("device_name")
    val deviceName: String,

    @SerializedName("device_token")
    val deviceToken: Any? = null,

    @SerializedName("app_version")
    val appVersion: Long,

    @SerializedName("os_version")
    val osVersion: String,

    @SerializedName("refresh_token")
    val refreshToken: String,

    @SerializedName("old_refresh_token")
    val oldRefreshToken: String,

    @SerializedName("limited_access")
    var limitedAccess: Boolean,

    @SerializedName("last_accessed_on")
    val lastAccessedOn: Long
)
