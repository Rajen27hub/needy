package com.canopas.base.data.interceptor

import com.canopas.base.data.auth.ACCESS_TOKEN
import com.canopas.base.data.auth.AuthManager
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AuthRequestInterceptor @Inject constructor(private val authManager: AuthManager) :
    Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val accessToken = authManager.accessToken

        if (authManager.authState.isAuthorised && !accessToken.isNullOrEmpty()) {
            request = request.newBuilder().cacheControl(CacheControl.FORCE_NETWORK)
                .addHeader(ACCESS_TOKEN, accessToken).build()
        }

        return chain.proceed(request)
    }
}
