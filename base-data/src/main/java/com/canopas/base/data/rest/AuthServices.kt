package com.canopas.base.data.rest

import com.canopas.base.data.auth.REFRESH_TOKEN
import com.canopas.base.data.model.JwtToken
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.model.RegisterDeviceRequest
import com.canopas.base.data.model.UserAccount
import com.google.gson.JsonElement
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Path

interface AuthServices {

    @POST("user/v1/anonymous")
    suspend fun anonymousLogin(@Body request: LoginRequest): Response<UserAccount>

    @GET("user/v1/access-token")
    suspend fun refreshAccessToken(@Header(REFRESH_TOKEN) token: String): Response<JwtToken>

    @POST("user/v1/logout")
    suspend fun logOutUser(@Header(REFRESH_TOKEN) refreshToken: String?): Result<JsonElement>

    @DELETE("user/v1/users/{id}")
    suspend fun deleteUser(@Path("id") userId: Int): Result<JsonElement>

    @POST("user/v1/device")
    suspend fun registerDevice(@Body registerDeviceRequest: RegisterDeviceRequest): Result<JsonElement>

    @POST("user/v2/login")
    suspend fun loginV2(@Body request: LoginRequest): Response<UserAccount>

    @DELETE("user/v1/logout/all")
    suspend fun logOutFromAllDevices(): Result<JsonElement>
}
