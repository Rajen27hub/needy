package com.canopas.base.data.di

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.canopas.base.data.auth.PREF_AUTH
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DataModule {

    @Singleton
    @Provides
    @Named(PREF_AUTH)
    fun provideAuthPreferences(application: Application): SharedPreferences {
        return application.getSharedPreferences(PREF_AUTH, Context.MODE_PRIVATE)
    }
}
