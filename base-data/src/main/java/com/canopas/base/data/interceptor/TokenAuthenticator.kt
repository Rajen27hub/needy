package com.canopas.base.data.interceptor

import com.canopas.base.data.auth.ACCESS_TOKEN
import com.canopas.base.data.auth.AuthManager
import okhttp3.Authenticator
import okhttp3.CacheControl
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class TokenAuthenticator @Inject constructor(
    private val authManager: AuthManager
) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? {
        Timber.d("Authenticate %s: %s", response.code, response.request.url)
        val originalRequest = response.request

        authManager.refreshAccessToken()
        val accessToken = authManager.accessToken
        if (accessToken.isNullOrEmpty()) return null

        return originalRequest.newBuilder().cacheControl(CacheControl.FORCE_NETWORK)
            .removeHeader(ACCESS_TOKEN)
            .addHeader(ACCESS_TOKEN, accessToken).build()
    }
}
