package com.canopas.base.data.model

import com.google.gson.annotations.SerializedName

data class RegisterDeviceRequest(
    @SerializedName("device_token")
    var deviceToken: String,

    @SerializedName("app_version")
    var appVersion: Long,

    @SerializedName("device_name")
    var deviceName: String,

    @SerializedName("os_version")
    var osVersion: String,
)
