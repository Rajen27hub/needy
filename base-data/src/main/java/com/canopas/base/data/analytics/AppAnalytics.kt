package com.canopas.base.data.analytics

import android.os.Bundle
import androidx.annotation.Size
import com.google.firebase.analytics.FirebaseAnalytics
import com.mixpanel.android.mpmetrics.MixpanelAPI
import org.json.JSONException
import org.json.JSONObject

open class AppAnalytics(
    private val firebaseAnalytics: FirebaseAnalytics,
    private val mixpanelAPI: MixpanelAPI
) {

    open fun logEvent(@Size(min = 1L, max = 40L) name: String, params: Bundle? = null) {
        // Firebase logging
        firebaseAnalytics.logEvent(name, params)

        // Mixpanel logging
        val json = params?.let {
            val json = JSONObject()
            for (key in it.keySet()) {
                try {
                    json.put(key, JSONObject.wrap(it.get(key)))
                } catch (e: JSONException) {
                    // Ignore exception
                }
            }
            return@let json
        }

        if (json == null) {
            mixpanelAPI.track(name)
        } else {
            mixpanelAPI.track(name, json)
        }
    }

    open fun setAnalyticsCollectionEnabled(enabled: Boolean) {
        firebaseAnalytics.setAnalyticsCollectionEnabled(enabled)

        val optOut = !enabled
        if (optOut != mixpanelAPI.hasOptedOutTracking()) {
            if (optOut) mixpanelAPI.optOutTracking() else mixpanelAPI.optInTracking()
        }
    }

    open fun setUserProperty(userType: String, userId: String?) {
        firebaseAnalytics.setUserProperty("user_type", userType)
        firebaseAnalytics.setUserId(userId)

        val props = JSONObject()
        props.put("User Type", userType)
        mixpanelAPI.registerSuperProperties(props)

        if (userId != null) {
            mixpanelAPI.identify(userId)
        }
    }
}
