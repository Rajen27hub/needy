package com.canopas.base.data.model

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

const val MONTH1 = "January"
const val MONTH2 = "February"
const val MONTH3 = "March"
const val MONTH4 = "April"
const val MONTH5 = "May"
const val MONTH6 = "June"
const val MONTH7 = "July"
const val MONTH8 = "August"
const val MONTH9 = "September"
const val MONTH10 = "October"
const val MONTH11 = "November"
const val MONTH12 = "December"

data class StreaksData(
    var start_date: Long,
    var end_date: Long,
    var count: Int,
    var is_current: Boolean
)

fun getFormattedDayDate(time: Long): Triple<String, String, String> {
    val formatter = SimpleDateFormat("dd MMMM yyyy", Locale.getDefault())
    val date = Date(time * UNIX_TO_DATE_MULTIPLIER)
    formatter.format(date)
    val formattedDate = when {
        date.toString().substring(8, 9) == "0" -> {
            when {
                date.toString().substring(9, 10) == "1" -> {
                    "${date.toString().substring(9, 10)}st ${
                    getMonthName(
                        date.toString().substring(4, 7)
                    )
                    }"
                }
                date.toString().substring(9, 10) == "2" -> {
                    "${date.toString().substring(9, 10)}nd ${
                    getMonthName(
                        date.toString().substring(4, 7)
                    )
                    }"
                }
                date.toString().substring(9, 10) == "3" -> {
                    "${date.toString().substring(9, 10)}rd ${
                    getMonthName(
                        date.toString().substring(4, 7)
                    )
                    }"
                }
                else -> {
                    "${date.toString().substring(9, 10)}th ${
                    getMonthName(
                        date.toString().substring(4, 7)
                    )
                    }"
                }
            }
        }
        date.toString().substring(8, 9) == "2" -> {
            when {
                date.toString().substring(9, 10) == "1" -> {
                    "${date.toString().substring(8, 10)}st ${
                    getMonthName(
                        date.toString().substring(4, 7)
                    )
                    }"
                }
                date.toString().substring(9, 10) == "2" -> {
                    "${date.toString().substring(8, 10)}nd ${
                    getMonthName(
                        date.toString().substring(4, 7)
                    )
                    }"
                }
                date.toString().substring(9, 10) == "3" -> {
                    "${date.toString().substring(8, 10)}rd ${
                    getMonthName(
                        date.toString().substring(4, 7)
                    )
                    }"
                }
                else -> {
                    "${date.toString().substring(8, 10)}th ${
                    getMonthName(
                        date.toString().substring(4, 7)
                    )
                    }"
                }
            }
        }
        date.toString().substring(8, 10) == "31" -> {
            "${date.toString().substring(8, 10)}st ${getMonthName(date.toString().substring(4, 7))}"
        }
        else -> {
            "${date.toString().substring(8, 10)}th ${getMonthName(date.toString().substring(4, 7))}"
        }
    }
    val formattedDay = SimpleDateFormat("EEEE", Locale.ENGLISH).format(time * 1000)
    return Triple(formattedDate, formattedDay, date.toString().substring(8, 10))
}

fun getMonthName(month: String): String {
    val monthList = listOf(
        MONTH1,
        MONTH2,
        MONTH3,
        MONTH4,
        MONTH5,
        MONTH6,
        MONTH7,
        MONTH8,
        MONTH9,
        MONTH10,
        MONTH11,
        MONTH12
    )
    monthList.forEachIndexed { _, data ->
        if (data.contains(month)) {
            return data
        }
    }
    return month
}
