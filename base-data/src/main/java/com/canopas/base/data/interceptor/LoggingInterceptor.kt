package com.canopas.base.data.interceptor

import com.canopas.base.data.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody.Companion.toResponseBody
import timber.log.Timber
import java.nio.charset.Charset
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoggingInterceptor @Inject constructor() : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        Timber.d("⚡Okhttp-> Request Started: ${request.url}")
        if (BuildConfig.DEBUG) {
            request.body?.let { body ->
                val buffer = okio.Buffer()
                body.writeTo(buffer)
                val requestBodyContent = buffer.readString(Charset.forName("UTF-8"))
                Timber.d("⚡Okhttp-> Request Body Data: $requestBodyContent")
            } ?: Timber.d("⚡Okhttp-> Request Body Data: None")
        }

        val response = chain.proceed(request)

        Timber.d("⚡Okhttp-> Response: ${response.code} -> ${response.request.url}")
        if (BuildConfig.DEBUG) {
            response.body?.let { body ->
                val responseBodyContent = body.string()
                Timber.d("⚡Okhttp-> Response Body Data: $responseBodyContent")

                // Re-create response body after consuming it to log the content
                val newResponseBody = responseBodyContent.toResponseBody(body.contentType())
                return response.newBuilder().body(newResponseBody).build()
            } ?: Timber.d("⚡Okhttp-> Response Body Data: None")
        }

        return response
    }
}
