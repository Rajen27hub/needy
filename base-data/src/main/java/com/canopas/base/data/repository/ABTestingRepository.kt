package com.canopas.base.data.repository

import android.content.Context
import android.content.SharedPreferences

class ABTestingRepository {

    private var sharedPreferences: SharedPreferences? = null

    fun init(context: Context) {
        sharedPreferences = context.applicationContext.getSharedPreferences("pref_ab_testing", Context.MODE_PRIVATE)
    }

    fun get(expName: String): Variant {
        val preferences = sharedPreferences ?: throw IllegalStateException("ABTestingRepository Instance is not initialized")

        var value = preferences.getInt(expName, -1)

        if (value == -1) {
            value = (0..1).random()
            preferences.edit().putInt(expName, value).apply()
        }
        return when (value) {
            0 -> Variant.Base
            1 -> Variant.V1
            else -> throw AssertionError("ABTestingRepository: Variant value $value is not possible")
        }
    }

    enum class Variant {
        Base, V1
    }

    companion object {
        val INSTANCE = ABTestingRepository()
    }
}

object FeatureFlag
