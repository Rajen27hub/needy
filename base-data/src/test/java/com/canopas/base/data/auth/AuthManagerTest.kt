package com.canopas.base.data.auth

import com.canopas.base.data.model.JwtToken
import com.canopas.base.data.model.LoginRequest
import com.canopas.base.data.model.Notifications
import com.canopas.base.data.model.Session
import com.canopas.base.data.model.UserAccount
import com.canopas.base.data.preferences.NoLonelyPreferences
import com.canopas.base.data.rest.AuthServices
import com.canopas.base.data.utils.Device
import com.canopas.base.data.utils.MemorySharedPreference
import dagger.Lazy
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.Headers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.doSuspendableAnswer
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import retrofit2.Response

class AuthManagerTest {

    private val authServicesLazy = mock<Lazy<AuthServices>>()
    private val authServices = mock<AuthServices>()
    private val device = mock<Device>()

    private val noLonelyPreferences = mock<NoLonelyPreferences>()

    private val authPreferences =
        MemorySharedPreference()

    private lateinit var authManager: AuthManager

    private val loginRequest = LoginRequest(
        1, "1",
        20000, "MD1", "1"
    )

    val session = Session(
        1, 14, deviceType = 1, "1", "MD1",
        "", 20000, "", "1", oldRefreshToken = "",
        limitedAccess = false, lastAccessedOn = 1641987105
    )
    val notifications = Notifications(
        pushNotification = true, mail = true, inactivityMail = true, inactivityNotification = false
    )

    private val anonymousUser = UserAccount(
        1, 14, 1, 1, "abc", "",
        "userName", "12345", "dummy@gmail.com", "",
        isActive = true, isAnonymous = true,
        createdAt = "123", updatedAt = "456", gender = 1, userType = 0, loginType = 1,
        isVerified = true, productId = "", session, notifications
    )

    val verifiedUser = UserAccount(
        1, 14, 1, 1, "abc", "",
        "userName", "12345", "dummy@gmail.com", "url",
        isActive = true, isAnonymous = false,
        createdAt = "123", updatedAt = "456", gender = 0, userType = 0, loginType = 1,
        isVerified = true, productId = "", session, notifications
    )

    @Before
    fun setup() {
        authManager =
            AuthManager(
                authServicesLazy,
                noLonelyPreferences,
                authPreferences,
                device
            )
        whenever(authServicesLazy.get()).thenReturn(authServices)
    }

    @Test
    fun `authManager should return updated accessToken`() {
        authPreferences.edit().putString(ACCESS_TOKEN, "123").commit()
        val accessToken = authManager.accessToken
        assertEquals(accessToken, "123")
    }

    @Test
    fun `login should return anonymous authState on succeed`() = runBlocking {
        authPreferences.edit().putString(REFRESH_TOKEN, "123").commit()

        val response = Response.success(anonymousUser)
        whenever(authServices.anonymousLogin(loginRequest)).thenReturn(response)
        whenever(noLonelyPreferences.currentUser).thenReturn(anonymousUser)

        val authResponse = authManager.login(loginRequest)
        assertEquals(authResponse, AuthResponse(AuthState.ANONYMOUS(anonymousUser), ServiceError.NONE, 200))
        verify(noLonelyPreferences).currentUser = anonymousUser
    }

    @Test
    fun `verify tokens on login succeed`() = runBlocking {
        val response = mock<Response<UserAccount>>()
        val header = mock<Headers>()

        whenever(response.isSuccessful).thenReturn(true)
        whenever(response.code()).thenReturn(200)
        whenever(response.headers()).thenReturn(header)
        whenever(header[ACCESS_TOKEN]).thenReturn("123")
        whenever(header[REFRESH_TOKEN]).thenReturn("123Refresh")

        whenever(authServices.anonymousLogin(loginRequest)).thenReturn(response)

        authManager.login(loginRequest, true)

        assertEquals(authManager.accessToken, "123")
        assertEquals(authManager.refreshToken, "123Refresh")
    }

    @Test
    fun `login should return verified authState on succeed`() = runBlocking {
        authPreferences.edit().putString(REFRESH_TOKEN, "123").commit()

        val response = Response.success(verifiedUser)
        whenever(authServices.loginV2(loginRequest)).thenReturn(response)
        whenever(noLonelyPreferences.currentUser).thenReturn(verifiedUser)

        val authResponse = authManager.login(loginRequest, false)
        assertEquals(authResponse, AuthResponse(AuthState.VERIFIED(verifiedUser), ServiceError.NONE, 200))
        verify(noLonelyPreferences).currentUser = verifiedUser
    }

    @Test
    fun `login should return unAuthorized authState when login returns unauthorized code`() =
        runBlocking {
            val response = mock<Response<UserAccount>>()
            whenever(response.code()).thenReturn(AUTH_REQUEST_CODE_UNAUTHORIZED)

            whenever(authServices.loginV2(loginRequest))
                .thenReturn(response)

            val authResponse = authManager.login(loginRequest, false)
            assertEquals(
                authResponse,
                AuthResponse(AuthState.UNAUTHORIZED, ServiceError.UNAUTHORIZED, 401)
            )
        }

    @Test
    fun `login should return unAuthorized authState with server error on invalid response`() =
        runBlocking {
            val response = mock<Response<UserAccount>>()
            whenever(response.code()).thenReturn(401)

            whenever(authServices.loginV2(loginRequest))
                .thenReturn(response)

            val authResponse = authManager.login(loginRequest, false)
            assertEquals(
                authResponse,
                AuthResponse(AuthState.UNAUTHORIZED, ServiceError.UNAUTHORIZED, 401)
            )
        }

    @Test
    fun `login should return unAuthorized authState with server error on failure`() = runBlocking {

        whenever(authServices.loginV2(loginRequest))
            .doSuspendableAnswer {
                throw RuntimeException()
            }

        val authResponse = authManager.login(loginRequest, false)
        assertEquals(
            authResponse,
            AuthResponse(AuthState.UNAUTHORIZED, ServiceError.SERVERERROR)
        )
    }

    @Test
    fun `refresh access token should logout on response code unauthorized`() = runBlocking {
        authPreferences.edit().putString(REFRESH_TOKEN, "123").commit()

        val response = mock<Response<JwtToken>>()
        whenever(response.code()).thenReturn(AUTH_REQUEST_CODE_UNAUTHORIZED)
        whenever(authServices.refreshAccessToken("123")).thenReturn(response)

        authManager.refreshAccessToken()
        assertEquals(null, authPreferences.getString(REFRESH_TOKEN, ""))
        assertEquals(null, authPreferences.getString(ACCESS_TOKEN, ""))
        assertEquals(null, noLonelyPreferences.currentUser)
    }

    @Test
    fun `verify refresh & access token on api call succeed`() = runBlocking {
        authPreferences.edit().putString(REFRESH_TOKEN, "123").commit()

        val jwtToken = JwtToken("1234", "asdfg")
        val response = Response.success(jwtToken)

        whenever(authServices.refreshAccessToken("123")).thenReturn(response)

        authManager.refreshAccessToken()

        assertEquals(jwtToken.accessToken, authPreferences.getString(ACCESS_TOKEN, ""))
        assertEquals(jwtToken.refreshToken, authPreferences.getString(REFRESH_TOKEN, ""))
    }

    @Test
    fun `verify refresh token api call on multiple refreshAccessToken call`() = runBlocking {
        authPreferences.edit().putString(REFRESH_TOKEN, "123").commit()

        val jwtToken = JwtToken("1234", "asdfg")
        val response = Response.success(jwtToken)

        whenever(authServices.refreshAccessToken("123")).doSuspendableAnswer {
            withContext(Dispatchers.IO) {
                delay(100)
                response
            }
        }

        val call1 = async(Dispatchers.IO) {
            authManager.refreshAccessToken()
        }
        val call2 = async(Dispatchers.IO) {
            authManager.refreshAccessToken()
        }

        call1.await()
        call2.await()

        verify(authServices, times(1)).refreshAccessToken("123")
        assertEquals(jwtToken.accessToken, authPreferences.getString(ACCESS_TOKEN, ""))
        assertEquals(jwtToken.refreshToken, authPreferences.getString(REFRESH_TOKEN, ""))
    }
}
