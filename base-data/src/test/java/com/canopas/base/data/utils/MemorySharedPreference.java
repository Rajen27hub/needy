package com.canopas.base.data.utils;

import android.content.SharedPreferences;

import androidx.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class MemorySharedPreference implements SharedPreferences {
    HashMap<String, Object> memoryMap;
    MockSharedPreferenceEditor editor;

    public MemorySharedPreference() {
        memoryMap = new HashMap<>();
        editor = new MockSharedPreferenceEditor(memoryMap);
    }

    @Override
    public Map<String, ?> getAll() {
        return memoryMap;
    }

    @Nullable
    @Override
    public String getString(String key, @Nullable String value) {
        return (String) memoryMap.getOrDefault(key, value);
    }

    @Nullable
    @Override
    public Set<String> getStringSet(String key, @Nullable Set<String> set) {
        return (Set<String>) memoryMap.getOrDefault(key, set);
    }

    @Override
    public int getInt(String key, int value) {
        return (int) memoryMap.getOrDefault(key, value);
    }

    @Override
    public long getLong(String key, long value) {
        return (long) memoryMap.getOrDefault(key, value);
    }

    @Override
    public float getFloat(String key, float value) {
        return (float) memoryMap.getOrDefault(key, value);
    }

    @Override
    public boolean getBoolean(String key, boolean value) {
        return (boolean) memoryMap.getOrDefault(key, value);
    }

    @Override
    public boolean contains(String key) {
        return memoryMap.containsKey(key);
    }

    @Override
    public Editor edit() {
        return editor;
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    private static class MockSharedPreferenceEditor implements Editor {
        HashMap<String, Object> memoryMap;

        public MockSharedPreferenceEditor(HashMap<String, Object> memoryMap) {
            this.memoryMap = memoryMap;
        }

        @Override
        public Editor putString(String s, @Nullable String s1) {
            memoryMap.put(s, s1);
            return this;
        }

        @Override
        public Editor putStringSet(String s, @Nullable Set<String> set) {
            memoryMap.put(s, set);
            return this;
        }

        @Override
        public Editor putInt(String s, int i) {
            memoryMap.put(s, i);
            return this;
        }

        @Override
        public Editor putLong(String s, long l) {
            memoryMap.put(s, l);
            return this;
        }

        @Override
        public Editor putFloat(String s, float v) {
            memoryMap.put(s, v);
            return this;
        }

        @Override
        public Editor putBoolean(String s, boolean b) {
            memoryMap.put(s, b);
            return this;
        }

        @Override
        public Editor remove(String s) {
            memoryMap.remove(s);
            return this;
        }

        @Override
        public Editor clear() {
            memoryMap.clear();
            return this;
        }

        @Override
        public boolean commit() {
            return true;
        }

        @Override
        public void apply() {

        }
    }
}
